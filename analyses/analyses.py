import argparse
import sacrebleu
import sys
import os
from tqdm import tqdm
from mosestokenizer import *
from pprint import pprint
import json

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

parser = argparse.ArgumentParser(description='Analyse un corpus de travail.')


parser.add_argument('--models', type=str, nargs="+", default=os.listdir('../xml_generator/models/'),
                   help='Models list')
parser.add_argument('--max', type=int, default=0,
                   help='Nombre maximal d\'exemples à traiter (0 == tous)')
parser.add_argument('--refresh', action='store_true', help='Rafraîchit le recalcul')
parser.add_argument('--force', action='store_true', help='Force le recalcul même si tout a déjà été traité')

all_log_rep = "analyses.json"
lemmas = "../xml_generator/lemmas.json"

if os.path.isfile(all_log_rep):
    with open(all_log_rep, 'r') as outfile:
        all_log = json.load(outfile)
    pprint(all_log)


if os.path.isfile(lemmas):
    with open(lemmas, 'r') as outfile:
        lemmas = json.load(outfile)
else:
    lemmas = {}

for forme in lemmas.keys():
    lemmas[forme] = set(lemmas[forme])

args = parser.parse_args()

for model in args.models[:]:
    if not os.path.isfile(os.path.abspath(os.path.join(pathname, "../xml_generator/models/" + model + "/pred.txt"))):
        args.models.remove(model)

all_log = {}

for model in args.models:

    all_log[model] = {}

    rep = os.path.abspath(os.path.join(pathname, "../xml_generator/models/" + model))
    print(rep)

    inputs = rep + '/inputs' + '-test.txt'
    outputs = rep + '/outputs' + '-test.txt'
    preds = rep + '/pred.txt'
    log_analyse = rep + '/log_analyse.json'

    take = True

    if not os.path.exists(inputs):
        print(model + " inputs to built, please")
        take = False
    if not os.path.exists(outputs):
        print(model + " outputs to built, please")
        take = False
    if not os.path.exists(preds):
        print(model + " predictions to realize, please")
        take = False

    with open(inputs) as f:
        inputs = f.readlines()
        f.close()
    with open(outputs) as f:
        outputs = f.readlines()
        f.close()
    with open(preds) as f:
        preds = f.readlines()
        f.close()

    if len(inputs) != len(outputs):
        print("inputs et outputs have differents length.")
        take = False

    elif len(inputs) != len(preds):
        print("inputs et preds have differents length. Respectively " + len(inputs).__str__() + " and " + len(preds).__str__()  + ".")
        if len(preds) == 0:
            print("no predictions")
            take = False
        else:
            if len(preds) > len(inputs) or len(preds) == 0:
                take = False
            if not (len(inputs) < len(preds) or  len(preds) == 0 ) and  ('len preds' not in all_log[model].keys() or all_log[model]['len preds'].__str__() == len(preds).__str__()):
                if  args.refresh and  args.force:
                    take = True

            all_log[model]['len inputs'] = len(inputs).__str__()
            all_log[model]['len preds'] = len(preds).__str__()

            if os.path.isfile(log_analyse):
                with open(log_analyse, 'r') as outfile:
                    all_log[model] = json.load(outfile)
                    if int(all_log[model]['len preds']) >= len(preds) and not args.refresh:
                        print(model + " allready done.")
                        take = False
                    elif int(all_log[model]['len preds']) >= len(preds):
                        print(model + " à rafraichir.")
                outfile.close()

    if take:
        nb_max = args.max
        ratio = -1
        if args.max <= 0:
            nb_max = len(preds)
        # ratio = nb_max/len(preds)

        taken_indexes = [k for k in range(len(preds))]

        if 'random' in globals():
            del random
        import random

        taken_indexes = random.sample(taken_indexes, nb_max)
        all_log[model]['len preds'] = nb_max # nombre de réf pour le calcul.

        score_moyen = {'contrainte':[], 'structure':[], 'P':[], 'R':[]}

        for input, output, pred, itqdm in zip(inputs, outputs, preds, tqdm(range(len(preds)-1))):
            if itqdm in taken_indexes:
                with MosesDetokenizer('fr') as detokenize:
                    hypothèse_lex_retok = detokenize(pred.strip().split(" ")).replace("_", " ")
                    reference_lex_retok = detokenize(output.strip().split(" ")).replace("_", " ")

                score = sacrebleu.corpus_bleu(hypothèse_lex_retok, reference_lex_retok).score
                if "contrainte_" in input:
                    score_moyen['contrainte'].append(score)
                    # evaluation lemmas
                    # on est sur des contraintes
                    input_words = input.split()
                    if "proposition_" in input_words[0]:
                        contrainte = input_words.pop(0).replace("contrainte_", "")
                    else:
                        contrainte = input_words.pop(0).replace("contrainte_", "")
                        support_contrainte = input_words.pop(0).replace("contrainte_", "")
                    input_words = set(input_words)
                    output_words = set(output.split())
                    Word_Input_And_Output = sum([1 for word in output_words if word in lemmas.keys() and not set(lemmas[word]).isdisjoint(input_words)])
                    if len(input_words) == 0:
                        if len(output_words)> 0:
                            input_words = output_words
                            # ie in:contrainte_coordination_verbale contrainte_être
                            #   out:Tu es amoureux , mais tu es fou .
                    P = Word_Input_And_Output / len(set(input_words))
                    # P = count(Word_Input_And_Output) / count(Words_Input) ie proportion of words in the output that are in the input
                    R = Word_Input_And_Output / len(set(output_words))
                    # R = count(Word_Input_And_Output) / count(Words_Ouput)
                    score_moyen['P'].append(P)
                    score_moyen['R'].append(R)
                else:
                    score_moyen['structure'].append(score)

        all_log[model]['score moyen contrainte'] = sum(score_moyen['contrainte'])/len(score_moyen['contrainte'])
        all_log[model]['P'] = sum(score_moyen['P'])/len(score_moyen['P'])
        all_log[model]['R'] = sum(score_moyen['R'])/len(score_moyen['R'])
        all_log[model]['nombre lignes contrainte'] = len(score_moyen['contrainte'])
        all_log[model]['score moyen structure'] = sum(score_moyen['structure'])/len(score_moyen['structure'])
        all_log[model]['nombre lignes structure'] = len(score_moyen['structure'])

        try:
            fjson = open(log_analyse, "w")
            fjson.write(json.dumps(all_log[model], indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        except KeyboardInterrupt:
            if not fjson.closed:
                fjson.close()
                while not fjson.closed:
                    True

try:
    fjson = open(all_log_rep, "w")
    fjson.write(json.dumps(all_log, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
except KeyboardInterrupt:
    if not fjson.closed:
        fjson.close()
        while not fjson.closed:
            True

pprint(all_log)


# TODO
# - run model on training data
# - compute the following metrics
#
#   BLEU
#
#   P = count(Word_Input_And_Output) / count(Words_Input) ie proportion of words in the output that are in the input
#   .e.g.,
#   chat manger souris	le chat mange un rat
#   Words_Input_And_Output = {chat, mange} count(Words_Input_And_Output) = 2
#   count(Words_Input) = 3
#   ratio 2/3
#
#
#   R = count(Word_Input_And_Output) / count(Words_Output)
#   .e.g.,
#   chat manger souris	le chat mange un rat
#   Words_Input_And_Output = {chat, mange} count(Words_Input_And_Output) = 2
#   count(Words_Ouput) = 5
#   ratio 2/5
#
# P gives the proportion of input words that appear in the generated text
# R gives an idea of how many additional words are in the generated text
#
# N.B. this suppose that you can relate lemmas (manger) to word forms (mange)
