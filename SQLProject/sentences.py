import os
import sqlite3

debug = True
version =("v 0.9.6322")

if debug:
    sql_file = ":memory:"
elif debug:
    if os.path.isfile("../SQLProject/sentences_debug"+version+".db"):
        os.remove("../SQLProject/sentences_debug"+version+".db")
    sql_file = "../SQLProject/sentences_debug"+version+".db"
else:
    if os.path.isfile("../SQLProject/sentences"+version+".db"):
        os.remove("../SQLProject/sentences"+version+".db")
    sql_file = "../SQLProject/sentences"+version+".db"

connexion = sqlite3.connect(sql_file)
connexion.execute("PRAGMA foreign_keys = 1")  # Récupération d'un curseur
curseur = connexion.cursor()  # Récupération d'un curseur


curseur.execute("SELECT name FROM sqlite_master WHERE type='table';")
rws = curseur.fetchall()

for row in rws:
    curseur.execute('SELECT * FROM ' + row[0] + ' LIMIT 1')
    print(curseur.fetchone())

