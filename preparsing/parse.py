import glob
import os, sys
from shutil import copyfile
from tqdm import tqdm
import argparse
from pathlib import Path

tqdm.monitor_interval = 0 # bug multithreading #https://github.com/tqdm/tqdm/issues/481



parser = argparse.ArgumentParser(description='Lance les analyses avec trois parseurs classiques.')

parser.add_argument('--stanford', action='store_true',
                   help='stanford parser\nprefer\njava -mx8g -cp \'*\'  edu.stanford.nlp.pipeline.StanfordCoreNLP -annnotators tokenize,ssplit,pos,lemma,parse,depparse,udfeats  -props StanfordCoreNLP-french.properties -outputFormat conllu -filelist ../../outputs/list.txt -outputDirectory ../../outputs/chunked_5000.',)

parser.add_argument('--grew', action='store_true',
                   help='grew.',)

parser.add_argument('--void', action='store_true',
                   help='aucun action, sort les erreurs si grew.',)

parser.add_argument('--grew_id', type=str, default="1",
                   help='spécifie un identifiant pour le noms des fichiers temporaires de grew',)

parser.add_argument('--talismane', action='store_true',
                   help='talismane.',)

args = parser.parse_args()

DIR = "outputs/chunked_5000"

sentences_files = glob.glob(DIR + "/*aligned_tokens.txt")
melt_files = glob.glob(DIR + "/*aligned_tokens.txt")

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

errors_grew = []
orig_grew = []

for sentences_file, i_file in zip(sentences_files, tqdm(range(len(sentences_files)))):

    melt_file = sentences_file.replace('aligned_tokens.txt', 'analyses.txt')
    tqdm.write(i_file.__str__(), end='', file=sys.stderr)

    # corenlp

    if args.stanford:
        os.chdir(fullpath)
        os.chdir("tools/")
        os.chdir("stanford-corenlp-full-2018-02-27")
        if not os.path.isfile(os.path.join(fullpath, sentences_file.replace("txt", "stanford.conllu"))) and not args.void:
            copyfile(os.path.join(fullpath, sentences_file), "test.txt")
            comd = 'java -mx8g -cp \'*\'  edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators tokenize,ssplit,pos,lemma,ner,parse,depparse,udfeats  -ssplit.eolonl -props StanfordCoreNLP-french.properties -outputFormat conllu -file test.txt '
            print(comd)
            os.system(comd)
            os.rename( "test.txt.conllu",  os.path.join(fullpath, sentences_file.replace("txt", "stanford.conllu")) )
            os.remove("test.txt")


    # grew
    if args.grew and (not os.path.isfile(os.path.join(fullpath, sentences_file.replace("txt", "grew.conll"))) or args.void):
    #
        if not args.void:
            Path(os.path.join(fullpath, sentences_file.replace("txt", "grew.conll"))).touch()
        os.chdir(fullpath)
        data_grew = open(os.path.join(fullpath, melt_file), "r").readlines()
        data_texte = open(os.path.join(fullpath, sentences_file), "r").readlines()
        for line, orig in zip(data_grew, data_texte):
            if " {" in line or "œ" in line or "{p" in line:
                if line != '' and line not in errors_grew:
                    errors_grew.append(line.strip())
                    orig_grew.append(orig.strip())
        data_grew = "\n".join(data_grew).replace(" { ", "{").replace(" {", "{").replace("œ", "oe").replace("{pg 5", '{pg_5')
        if not args.void:
            f = open("test" + args.grew_id + ".melt", 'w')
        data_grew = data_grew.replace("\n\n","\n").split("\n")

        for i, orig in zip(range(len(data_grew)), data_texte):
            line = data_grew[i].split(" ")
            if "//P/sur frac/NC/frac{m ( m + 3)}{/ADJ/*{ 2}/ADJ" in data_grew[i] or "#/ET/*#" in data_grew[i]:
                data_grew[i] = ""
            for word in line:
                if word.count("/") != 2 or "*#" in word or len(word.split("/")) != 3 or "=" in word:
                    if data_grew[i] != '' and data_grew[i] not in errors_grew:
                        errors_grew.append(data_grew[i])
                        orig_grew.append(orig.strip())
                    data_grew[i] = ""

        data_grew = "\n".join(data_grew)

        if not args.void:
            f.write(data_grew)
            f.close()
            while not f.closed:
                pass

        # copyfile(os.path.join(fullpath, melt_file), "test.melt")
        if not args.void:
            comd = 'grew transform -grs "tools/POStoSSQ/grs/surf_synt_main.grs" -i "test' + args.grew_id + '.melt" -o "test' + args.grew_id + '.txt.grew"'
            os.system(comd)
            os.remove(os.path.join(fullpath, sentences_file.replace("txt", "grew.conll")))
            os.rename("test" + args.grew_id + ".txt.grew",   os.path.join(fullpath, sentences_file.replace("txt", "grew.conll")))
            os.remove("test" + args.grew_id + ".melt")

    # talismane
    if args.talismane and not os.path.isfile(os.path.join(fullpath, sentences_file.replace("txt", "talismane"))) and not args.void:
        os.chdir(fullpath)
        os.chdir("tools")
        os.chdir("talismane-distribution-5.1.2-bin/")
        os.system('/usr/bin/java -Xmx5G -Dconfig.file=talismane-fr-5.0.4.conf -jar talismane-core-5.1.2.jar --analyse --sessionId=fr --encoding=UTF8 ' + \
                                    '--inFile="' + os.path.join(fullpath, sentences_file) + '"' + \
                                    ' --outFile="' + os.path.join(fullpath, sentences_file.replace("txt", "talismane")) + '"')


if args.void:
    errors_grew = "\n".join(errors_grew)
    f = open("errors.melt", 'w')
    f.write(errors_grew)
    f.close()
    while not f.closed:
        pass
    orig_grew = "\n".join(orig_grew)
    f = open("errors_orig.melt", 'w')
    f.write(orig_grew)
    f.close()
    while not f.closed:
        pass