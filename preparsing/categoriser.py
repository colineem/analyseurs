import argparse
import json
import sys
import os
from tqdm import tqdm
from pprint import pprint
import glob

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)


parser = argparse.ArgumentParser(description="""Permet la catégorisation des verbes de la base""")


parser.add_argument("-d", "--dir", type=str, default=os.path.join(fullpath, 'outputs/'),
                    help='chemin des fichiers')

parser.add_argument('--debug', action='store_true',
                    help='limite les traitements aux fichiers générés en debuggage')

parser.add_argument('--refresh', action='store_true',
                    help='réinitialise les catégories de base')
                    
parser.add_argument('--liste', action='store_true',
                    help='sort les verbes non gérés')

parser.add_argument('--shorted', action='store_true',
                    help='limité à 70 tokens')


args = parser.parse_args()

verbs = {}
def makeTable(rangement:{}, words:[], étiquette:str):
    for word in words:
        word = word.lower()
        if word not  in rangement.keys():
            rangement[word] = []
        if étiquette not in rangement[word]:
            rangement[word].append(étiquette)

if not os.path.isdir(args.dir):
    print("Pas de répertoire " + args.dir)
    exit(1)

if args.debug:
   sep_debug = "_debug"
else:
   sep_debug = ""

index_pos = 4
index_dep = 7
index_lemme_talismane = 2
index_cat_pos_grew = 3

if args.shorted:
    sentences_dbs = glob.glob(os.path.join(fullpath, 'outputs/results_70' + sep_debug + '/', "*_analyses.txt"))
else:
    sentences_dbs = glob.glob(os.path.join(fullpath, 'outputs/results' + sep_debug + '/kepts/', "*_analyses.txt"))

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'categories_verbales.json')
categories_verbales = {}
categories_verbales_lexique = []
out = []

def save(data, lexique:bool = False):
    if lexique:
        with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'categories_verbales_lexique.json'), 'w') as out:
            json.dump(data, out, indent=2, ensure_ascii=False)
    else:
        with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'categories_verbales.json'), 'w') as out:
            json.dump(data, out, indent=2, ensure_ascii=False)




if args.refresh:

    Possession = ["accorder", "accueillir", "adresser", "aider", "aider", "attribuer", "bénéficier", "confier",
                  "conseiller", "contribuer", "délivrer", "demander", "dépenser", "distribuer", "échanger", "écouter",
                  "égarer", "emprunter", "enseigner", "fournir", "gâter", "inviter", "livrer", "louer", "partager",
                  "payer", "prévenir", "procurer", "prodiguer", "profiter", "promettre", "proposer", "ravir",
                  "recevoir", "redonner", "remercier", "remettre", "restituer", "embrasser", "sauver", "procurer",
                  "servir", "solliciter", "vendre"]
    makeTable(verbs, Possession, "Possession")
    Mouvement = ["Accélérer", "accourir", "aller", "approcher", "arriver", "atteindre", "bondir", "bouger", "cheminer",
                 "circuler", "conduire", "contourner", "courir", "débarquer", "dépasser", "descendre", "devancer",
                 "éloigner", "embarquer", "enfuir", "entrer", "errer", "explorer", "foncer", "fouler", "frayer",
                 "galoper", "gravir", "grimper", "longer", "marcher", "mener", "monter", "mouvoir", "nager", "naviguer",
                 "orienter", "partir", "parvenir", "passer", "pénétrer", "planer", "poursuivre", "progresser",
                 "promener", "provenir", "ralentir", "ramper", "rattraper", "rebondir", "rechercher", "reculer",
                 "redescendre", "rejoindre", "remuer", "rentrer", "repartir", "ressortir", "retrouver", "revenir",
                 "roder", "rouler", "acheminer", "amener", "avancer", "aventurer", "élancer", "éloigner", "embarquer",
                 "enfuir", "envoler", "introduire", "orienter", "sauter", "balancer", "bouger", "croiser", "dégager",
                 "diriger", "dresser", "frayer", "glisser", "grouiller", "guider", "hâter", "hisser", "mouvoir",
                 "pencher", "perdre", "pointer", "presser", "ramener", "remuer", "rendre", "rouler", "sauver", "situer",
                 "suivre", "tirer", "transporter", "sortir", "suivre", "surgir", "tourner", "trotter", "visiter",
                 "voyager"]
    makeTable(verbs, Mouvement, "Mouvement")
    Déplacement = ["Amener", "apporter", "charger", "décharger", "enlever", "envoyer", "hisser", "importer", "lever",
                   "prendre", "ramener", "rapporter", "reporter", "encombrer", "encombrer", "soulever", "transporter"]
    makeTable(verbs, Déplacement, "Déplacement")
    Agression = ["Affaiblir", "affronter", "armer", "assiéger", "assommer", "blesser", "bousculer", "chasser", "cogner",
                 "commander", "confronter", "conquérir", "contester", "désarmer", "disputer", "effrayer", "empoigner",
                 "envahir", "esquiver", "étrangler", "forcer", "fouetter", "fusiller", "gagner", "gronder", "heurter",
                 "infliger", "liquider", "menacer", "parer", "protester", "provoquer", "punir", "régner", "résister",
                 "riposter", "risquer", "acharner", "armer", "attaquer", "empoigner", "battre", "blesser", "cogner",
                 "débattre", "déchaîner", "défendre", "disputer", "heurter", "mesurer", "venger", "survivre",
                 "survivre", "taper", "torturer", "triompher", "triompher", "tuer", "venger", "frapper"]
    makeTable(verbs, Agression, "Agression")
    Destruction = ["Abattre", "altérer", "anéantir", "assassiner", "casser", "consumer", "craquer", "crever",
                   "déchirer", "découper", "effacer", "effondrer", "éliminer", "jeter", "pourrir", "raser",
                   "empoisonner", "saigner", "casser", "couper", "rompre", "séparer", "supprimer", "tuer", "supprimer",
                   "tailler"]
    makeTable(verbs, Destruction, "Destruction")
    Assemblage = ["Entasser", "rassembler", "rattacher", "superposer", "grouper", "classer", "organiser", "accrocher",
                  "relier", "appliquer", "réparer", "concentrer", "fixer", "dresser", "enrouler", "envelopper",
                  "accumuler", "constituer", "ranger", "disposer", "rattacher", "enfiler", "fourrer", "encadrer",
                  "rassembler", "établir", "ficher", "enchaîner", "joindre", "serrer"]
    makeTable(verbs, Assemblage, "Assemblage")
    Modification = ["Accentuer", "accroître", "ajouter", "allonger", "colorer", "convertir", "corriger", "courber",
                    "croître", "développer", "dilater", "diminuer", "dissoudre", "durcir", "éparpiller", "étirer",
                    "gonfler", "grossir", "hausser", "modifier", "multiplier", "plier", "rectifier", "redresser",
                    "réduire", "resserrer", "adapter", "corriger", "multiplier", "tordre", "transformer", "transformer",
                    "voiler"]
    makeTable(verbs, Modification, "Modification")

    Activités = ["Aboutir", "accomplir", "achever", "agir", "arrêter", "cesser", "commettre", "débuter", "effectuer",
                 "entamer", "entreprendre", "essayer", "faire", "coïncider", "fonder", "germer", "pratiquer",
                 "procéder", "produire", "réaliser", "recommencer", "refaire", "relire", "apprêter", "dispenser",
                 "limiter", "préparer", "relire", "terminer", "succéder", "tenter"]
    makeTable(verbs, Activités, "Activités")
    Activitésb = ["Aboyer", "absorber", "amuser", "appeler", "applaudir", "avaler", "bailler", "bavarder", "boire",
                  "bouffer", "chuchoter", "communiquer", "consommer", "cracher", "dévorer", "dîner", "dormir",
                  "embrasser", "endormir", "enivrer", "entendre", "exprimer", "faire marrer", "frissonner", "fumer", "",
                  "goûter à ", "grogner", "grommeler", "gueuler", "haleter", "hurler", "jouer", "mâcher", "manifester",
                  "murmurer", "nourrir", "nourrir", "palpiter", "plaisanter", "prier", "raconter", "railler", "réciter",
                  "renifler", "répondre", "respirer", "ricaner", "rigoler", "ronfler", "rougir", "alimenter", "amuser",
                  "écrier", "endormir", "enivrer", "essuyer", "exclamer", "exprimer", "sangloter", "savourer",
                  "baigner", "déguiser", "distraire", "gratter", "laver", "rafraîchir", "reposer", "réveiller",
                  "saluer", "soulager", "siffler", "sommeiller", "souffler", "taire", "téter", "tousser", "trembler",
                  "vomir"]
    makeTable(verbs, Activitésb, "Activités de base")
    Possession = ["Abandonner", "accepter", "acheter", "acquérir", "acquitter", "adopter", "affecter", "alimenter",
                  "caresser", "collaborer", "consacrer", "correspondre", "débiter", "dérober", "discuter", "dispenser",
                  "distraire", "encourager", "engendrer", "épargner", "épouser", "expliquer", "favoriser", "féliciter",
                  "flatter", "garder", "indiquer", "informer", "laisser", "libérer", "obtenir", "parier", "participer",
                  "posséder", "prescrire", "professer", "profiter", "publier", "racheter", "ranimer", "réclamer",
                  "réconcilier", "regagner", "renseigner", "reprendre", "ruiner", "acquitter", "adresser", "enrichir",
                  "étreindre", "excuser", "saluer", "communiquer", "dépouiller", "fournir", "gâter", "livrer", "marier",
                  "prêter", "regarder", "revoir", "servir", "transmettre", "soigner", "témoigner", "transmettre",
                  "voler"]
    makeTable(verbs, Possession, "Possession")
    Changement_de_localisation = ["Abaisser", "accéder", "accompagner", "acheminer", "affluer", "agiter", "attirer",
                                  "attraper", "baisser", "balancer", "balayer", "bercer", "buter", "cacher", "camper",
                                  "capter", "charrier", "chercher", "croiser", "cueillir", "danser", "débarrasser",
                                  "défiler", "déposer", "dériver", "dérober", "dérouler", "détenir", "détourner",
                                  "diriger", "disparaître", "disperser", "doubler", "ébranler", "élancer", "émerger",
                                  "enfermer", "entourer", "entraîner", "évacuer", "éviter", "évoluer", "filer",
                                  "flotter", "fonctionner", "frôler", "fuir", "glisser", "grouiller", "guider",
                                  "incliner", "inspecter", "jaillir", "lâcher", "lancer", "manoeuvrer", "mobiliser",
                                  "osciller", "peser", "piétiner", "plonger", "pousser", "précéder", "précipiter",
                                  "presser", "projeter", "propager", "puiser", "quitter", "ramasser", "rapprocher",
                                  "reconduire", "recueillir", "réfugier", "regagner", "relever", "remporter",
                                  "rencontrer", "renverser", "renvoyer", "repasser", "replonger", "reposer",
                                  "repousser", "reprendre", "retirer", "retomber", "retourner", "révéler", "abriter",
                                  "arracher", "asseoir", "échapper", "écraser", "élever", "emparer", "empresser",
                                  "enfoncer", "engager", "esquiver", "immobiliser", "ôter", "saisir", "baisser",
                                  "cacher", "charger", "couvrir", "défiler", "déranger", "dérober", "détacher",
                                  "dissimuler", "fourrer", "hausser", "jeter", "mettre", "poser", "pousser", "ramasser",
                                  "ranger", "rasseoir", "rattraper", "reculer", "réfugier", "rejoindre", "relever",
                                  "rencontrer", "renseigner", "renverser", "replier", "retirer", "retourner",
                                  "retrouver", "saisir", "servir", "tailler", "transmettre", "trouver", "secouer",
                                  "situer", "soutenir", "supporter", "surmonter", "sursauter", "suspendre", "tenir",
                                  "tirer", "tomber", "tournoyer", "tracer", "traîner", "transmettre", "trébucher",
                                  "vaciller", "verser", "vider", "virer", "voler"]
    makeTable(verbs, Changement_de_localisation, "Changement de localisation")
    Action_objets = ["Abaisser", "accommoder", "adoucir", "affecter", "afficher", "améliorer", "approfondir", "appuyer",
                     "articuler", "assombrir", "attacher", "baisser", "border", "borner", "boucher", "bouillir",
                     "bourrer", "brouiller", "cacher", "camper", "cerner", "chauffer", "choisir", "combler", "comparer",
                     "compenser", "compléter", "compter", "contenir", "copier", "correspondre", "coter", "couronner",
                     "couvrir", "cramponner", "creuser", "cuire", "cultiver", "dater", "déboucher", "débrider",
                     "déchiffrer", "définir", "déguiser", "démêler", "déplier", "déposer", "dérouler", "dessiner",
                     "détendre", "détourner", "dévoiler", "disperser", "dissimuler", "dissiper", "distinguer",
                     "éclaircir", "écrouler", "égaler", "emplir", "employer", "enchaîner", "encombrer", "enfoncer",
                     "enregistrer", "enrichir", "entourer", "épouser", "esquisser", "essuyer", "étendre", "évoluer",
                     "exposer", "fausser", "figurer", "filtrer", "fleurir", "fondre", "former", "fortifier", "fouiller",
                     "froisser", "froncer", "frotter", "glacer", "grandir", "guetter", "habiller", "illuminer",
                     "illustrer", "imprimer", "incliner", "inoculer", "inonder", "instaurer", "introduire", "inventer",
                     "laver", "limiter", "loger", "maintenir", "manoeuvrer", "marier", "marquer", "masquer", "mentir",
                     "mobiliser", "mouiller", "nettoyer", "opérer", "orner", "ouvrir", "parer", "peindre", "pencher",
                     "pendre", "percer", "peupler", "piquer", "planter", "pointer", "poser", "presser", "prolonger",
                     "propager", "rabattre", "raccrocher", "raidir", "rallier", "rapprocher", "rayonner", "réconcilier",
                     "reconstituer", "reconstruire", "recouvrir", "redevenir", "relever", "remplacer", "remplir",
                     "renfermer", "renforcer", "renouveler", "répandre", "replier", "rétablir", "retirer", "retrancher",
                     "ronger", "accrocher", "allonger", "altérer", "arracher", "arranger", "attacher", "écarter",
                     "élever", "enrichir", "entasser", "entourer", "envelopper", "éparpiller", "établir", "étendre",
                     "habiller", "incliner", "insérer", "isoler", "organiser", "ôter", "ouvrir", "unir", "boucher",
                     "changer", "classer", "contracter", "courber", "couronner", "crisper", "défaire", "démêler", "",
                     "déshabiller", "détacher", "développer", "différencier", "grouper", "hausser", "marier", "mettre",
                     "nouer", "parer", "plier", "poser", "raccrocher", "ranger", "redresser", "rejoindre", "remplir",
                     "renforcer", "replier", "reproduire", "réunir", "suspendre", "tenir", "voiler", "sécher", "semer",
                     "séparer", "serrer", "simplifier", "situer", "souligner", "soustraire", "suspendre", "tacher",
                     "tenir", "tordre", "tracer", "traiter", "travailler", "tremper", "tricoter", "troubler", "user",
                     "utiliser", "varier", "vider", "viser", "frapper"]
    makeTable(verbs, Action_objets, "Action sur les objets")
    atteinte = ["Abaisser", "abuser", "assujettir", "aveugler", "barrer", "bouillir", "brouiller", "buter", "cerner",
                "claquer", "contredire", "couler", "crisper", "débarrasser", "débattre", "débiter", "déchaîner",
                "défaillir", "dénoncer", "dépouiller", "disperser", "ébranler", "écrouler", "efforcer", "empêcher",
                "empoisonner", "enchaîner", "enterrer", "épuiser", "étouffer", "éviter", "exclure", "faillir",
                "fausser", "fendre", "flamber", "fléchir", "froisser", "fuir", "imposer", "inoculer", "inonder",
                "interrompre", "irriter", "isoler", "lâcher", "lancer", "libérer", "mobiliser", "mordiller", "mourir",
                "paralyser", "parer", "pécher", "pendre", "percer", "piétiner", "pincer", "piquer", "pousser", "priver",
                "projeter", "protéger", "quitter", "ranimer", "réagir", "refouler", "refuser", "rejeter", "remporter",
                "renier", "renverser", "renvoyer", "répliquer", "repousser", "revendiquer", "ronger", "accrocher",
                "affaiblir", "affranchir", "altérer", "arracher", "échauffer", "écraser", "écrouler", "effondrer",
                "empêcher", "enterrer", "entraîner", "épuiser", "éteindre", "étrangler", "exposer", "incliner",
                "infliger", "sacrifier", "crever", "débarrasser", "défiler", "démêler", "dépouiller", "dérober",
                "jeter", "monter", "moquer", "parer", "pendre", "plier", "ramasser", "retrancher", "risquer", "saigner",
                "taire", "secouer", "séparer", "sombrer", "succomber", "succomber", "tirer", "tomber", "tordre",
                "trancher", "tressaillir", "tromper", "user", "vaciller", "violer", "viser", "frapper"]
    makeTable(verbs, atteinte, "Atteinte à l'intégrité")
    Activités = ["Abandonner", "aborder", "allumer", "amorcer", "apparaître", "approfondir", "attendre", "borner",
                 "causer", "choisir", "compléter", "contrôler", "dater", "déchiffrer", "décliner", "dessiner", "durer",
                 "écrire", "émerger", "employer", "engendrer", "envisager", "esquisser", "éteindre", "étreindre",
                 "étudier", "évoluer", "examiner", "exercer", "exploiter", "faciliter", "faillir", "fermer",
                 "fonctionner", "former", "improviser", "insister", "instaurer", "interrompre", "intervenir",
                 "inventer", "limiter", "manoeuvrer", "mourir", "opérer", "ouvrir", "persister", "précéder",
                 "présenter", "prolonger", "quitter", "rallumer", "rater", "reconduire", "reconstruire", "redevenir",
                 "redoubler", "refermer", "renouveler", "replonger", "reproduire", "rétablir", "revoir", "affranchir",
                 "animer", "appliquer", "attarder", "éteindre", "éveiller", "infliger", "inscrire", "interrompre",
                 "ouvrir", "unir", "charger", "faire", "produire", "ranger", "replonger", "sécher", "simplifier",
                 "succomber", "tacher", "tenter", "tracer", "travailler", "tricoter", "utiliser", "vieillir"]
    makeTable(verbs, Activités, "Activités")

    Activitésb = ["Alimenter", "animer", "annoncer", "appeler", "attendrir", "baigner", "bercer", "bourdonner",
                  "commenter", "comporter", "conter", "crisper", "danser", "distraire", "émettre", "éprouver",
                  "féliciter", "flairer", "flatter", "implorer", "interpeller", "lire", "nommer", "observer", "percher",
                  "proclamer", "prononcer", "rayonner", "réclamer", "redire", "regarder", "reparler", "retentir",
                  "revendiquer", "revivre", "animer", "étouffer", "éveiller", "excuser", "expliquer", "saluer",
                  "communiquer", "crisper", "manifester", "moquer", "remplir", "reproduire", "soigner", "taire",
                  "signaler", "trembloter", "tressaillir", "vivre"]

    inf = ["Accoucher", "cligner", "écouler", "fatiguer", "ordonner", "oser", "profiter", "ressusciter", "retenir",
           "extraire", "cramponner", "planter", "présenter", "vider", "téléphoner", "veiller", "voir", "naître"]
    makeTable(verbs, inf, "Inférieur au critère faible")
    horsc = ["Abriter", "adhérer", "affranchir", "apprêter", "chanceler", "conformer", "conserver", "contracter",
             "démontrer", "déshabiller", "dicter", "douter", "éblouir", "échouer", "éclairer", "entretenir", "épier",
             "feindre", "fréquenter", "gratter", "hocher", "impliquer", "maîtriser", "manquer", "mesurer", "réagir",
             "réfléchir", "réveiller", "accouder", "agenouiller", "agiter", "annoncer", "appuyer", "effacer",
             "efforcer", "embarrasser", "enrouler", "entretenir", "épargner", "exercer", "informer", "inviter",
             "observer", "coucher", "laisser", "loger", "mouiller", "qualifier", "rabattre", "raidir", "raser",
             "remettre", "retenir", "réveiller", "révéler", "ronger", "sécher", "toucher", "subsister", "suppléer",
             "surveiller", "survenir", "tinter", "trouver", "vanter", "voter"]
    makeTable(verbs, horsc, "Hors catégorie")
    save(verbs)
    categories_verbales = verbs
    for verb in verbs.keys():
        for cat in verbs[verb]:
            if cat not in categories_verbales_lexique:
                categories_verbales_lexique.append(cat)
        categories_verbales_lexique = sorted(categories_verbales_lexique)
    save(categories_verbales_lexique, True)



else:
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            categories_verbales = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)


    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'categories_verbales_lexique.json')

    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            categories_verbales_lexique = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)


    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'out_verbs.json')

    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            out = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)


verbes = {}
vues = {"know":0, "unknow":0}

for sentences_db, itdqm in zip(sentences_dbs, tqdm(range(len(sentences_dbs)))):
    tqdm.write(itdqm.__str__(), end='', file=sys.stderr)
    len_ok_tmp = 0

    dir_json = sentences_db
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            try:
                sentences_db = json.load(outfile)
            except Exception as ex:
                print(dir_json + "...", file=sys.stderr)
                sentences_db = []
    else:
        print("Chargement impossible, " + dir_json + " inexistant", file=sys.stderr)
        exit(2)

    for sentence_dic in sentences_db:
        sentence = sentence_dic["sentence"]
        if "metas" not in sentence_dic.keys():
            os.remove(dir_json)
            break
        metas = sentence_dic["metas"]
        analyse_grew = sentence_dic["grew"]
        analyse_talismane = sentence_dic["talismane"]
        analyse_stanford = sentence_dic["stanford"]

        take = True
        for analyzed_token_grew, analyzed_token_talismane, analyzed_token_stanford  in zip(analyse_grew, analyse_talismane, analyse_stanford):
            pos = analyzed_token_grew[index_pos]
            cat = analyzed_token_grew[index_cat_pos_grew]
            dep = analyzed_token_grew[index_dep]
            lemme = analyzed_token_talismane[index_lemme_talismane]
            word = analyzed_token_talismane[1]

            if cat == "V" and lemme not in categories_verbales.keys():
                if not args.liste:
                    print()
                    print("*"*20)
                    print(lemme)
                if lemme not in verbes.keys():
                    verbes[lemme] = 1
                    vues["unknow"] += 1
                else:
                    verbes[lemme] += 1
                if lemme not in out and not args.liste:
                    a = input("exclure [o/n] ? ").upper()
                    if a == 'O':
                        out.append(lemme)
                        with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'out_verbs.json'), 'w') as f:
                            json.dump(out, f, indent=2, ensure_ascii=False)
                    else:
                        categories_verbales[lemme] = []
                        print(sentence)
                        for catég in sorted(categories_verbales_lexique):
                            a = input(catég + " [o/n] ? ").upper()
                            if a == "O":
                                categories_verbales[lemme].append(catég)
                        print(categories_verbales[lemme])
                        a = ""
                        if categories_verbales[lemme] != []:
                            a = input("sauver [o/n] ? ").upper()
                        if a == 'O':
                            save(categories_verbales)
                        else:
                            del categories_verbales[lemme]
                            a = input("exclure [o/n] ? ").upper()
                            if a == 'O':
                                out.append(lemme)
                                with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'out_verbs.json'), 'w') as f:
                                    json.dump(out, f, indent=2, ensure_ascii=False)
            else:
                if cat == "V" and lemme in categories_verbales.keys():
                    vues["know"] += 1

for key in list(verbes.keys()):
    if (len(key) <2 or "_" in key or key.count(",") > 1) and key !=  "il, y, avoir":  
        del verbes[key]
    elif key[-2]+key[-1] not in ("er", "ir", "re"):
        del verbes[key]

pprint(verbes)
print(len(verbes.keys()))
pprint(vues)

with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'verbes.json'), 'w') as out:
    json.dump(verbes, out, indent=2, ensure_ascii=False)
