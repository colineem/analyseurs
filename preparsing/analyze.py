import glob
from pprint import pprint
import os, sys
import string
from tqdm import tqdm
import argparse
from nltk import FreqDist
import copy
import nltk
from math import fsum
import json
import gc

sent_tokenize = nltk.data.load('tokenizers/punkt/french.pickle')
from mosestokenizer import MosesTokenizer

tokenizer = MosesTokenizer(lang='fr')

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

parser = argparse.ArgumentParser(description="""Lance les analyses les sorties de trois parseurs classiques : talismane, grew, stanford.

Les fichiers doivent être dans un même répertoire.

Les fichiers stanford ont été généré avec la commande suivante :
\n\tjava -mx5g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP -annnotators tokenize,pos,lemma,parse,depparse,udfeats  -props StanfordCoreNLP-french.properties -outputFormat conllu -filelist ../../outputs/' + sep_debug + '/list.txt -outputDirectory ../../outputs/chunked_5000
Grew a été utilisé avec les valeurs par défaut :
\n\tgrew transform -grs "tools/POStoSSQ/grs/surf_synt_main.grs" -i "test.melt" -o "test.txt.grew"'
Talismane aussi :
\n\t/usr/bin/java -Xmx5G -Dconfig.file=talismane-fr-5.0.4.conf -jar talismane-core-5.1.2.jar --analyse --sessionId=fr --encoding=UTF8 --inFile=x.txt --outFile=y.txt     
""")

# Stanford, sur books gutemberg
# Annotation pipeline timing information:
# TokenizerAnnotator: 115.7 sec.
# WordsToSentencesAnnotator: 116.7 sec.
# POSTaggerAnnotator: 3497.0 sec.
# DependencyParseAnnotator: 99168.8 sec.
# TOTAL: 102898.1 sec. for 117468862 tokens at 1141,6 tokens/sec.
# Pipeline setup: 29.9 sec.
# Total time for StanfordCoreNLP pipeline: 104904.2 sec.


parser.add_argument("-d", "--dir", type=str, default=os.path.join(fullpath, 'outputs/chunked_5000'),
                    help='chemin des fichiers')

parser.add_argument("-s", "--seuil_others", type=int, default=10,
                    help='pourcentage en-dessous duquel on ne considère pas représentatif l\'échantillon (d\'une étiquette pos ou dep d\'un parser pour une autre)')

parser.add_argument("-c", "--seuil_compte", type=int, default=90, help='nombre d\'items pour que le tag soit pris en compte')

parser.add_argument('--texte', type=str, default='txt',
                    help='suffixe texte')

parser.add_argument('--stanford', type=str, default='txt.conllu',
                    help='suffixe stanford')

parser.add_argument('--grew', type=str, default='grew.conll',
                    help='suffixe grew')

parser.add_argument('--talismane', type=str, default='talismane',
                    help='suffixe talismane')

parser.add_argument('--debug', action='store_true',
                    help='limite les traitements au nombre de fichiers nécessaires pour obtenir au moins 1000 lignes.')

parser.add_argument('--refresh', action='store_true',
                    help='renouvelle les json stockant les traitements')

args = parser.parse_args()

if not os.path.isdir(args.dir):
    print("Pas de répertoire " + args.dir)
    exit(3)

files_talismane = glob.glob(os.path.join(args.dir, "*." + args.talismane))
files_stanford = glob.glob(os.path.join(args.dir, "*." + args.stanford))
files_grew = glob.glob(os.path.join(args.dir, "*." + args.grew))

if len(files_talismane) == 0:
    print("Pas de fichers *." + args.talismane + " dans le répertoire.")
if len(files_stanford) == 0:
    print("Pas de fichers *." + args.stanford + " dans le répertoire.")
if len(files_grew) == 0:
    print("Pas de fichers *." + args.grew + " dans le répertoire.")

if len(files_grew) == 0 or len(files_stanford) == 0 or len(files_talismane) == 0:
    exit(2)

if args.debug:
   sep_debug = "_debug"
else:
   sep_debug = ""

etiquettes = ('talismane', 'grew', 'stanford')

POS_TALISMANE = {}
POS_GREW = {}
POS_STANFORD = {}

POS_T_EXEMPLE = {}
POS_G_EXEMPLE = {}
POS_S_EXEMPLE = {}

DEP_TALISMANE = {}
DEP_GREW = {}
DEP_STANFORD = {}

index_pos_stanford = 4
index_pos_talismane = 4
index_pos_grew = 4

index_dep_stanford = 7
index_dep_talismane = 7
index_dep_grew = 7

sent_ok = 0
tokens_ok = 0
sents = 0

references_db = {}

if not args.refresh:

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/analyse' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            seuil_others, seuil_compte, sents, sent_ok, tokens_ok = json.load(outfile)
            args.seuil_others = seuil_others
            args.seuil_compte = seuil_compte
            print(dir_json + " chargé.", file=sys.stderr)
        sents = int(sents)
        sent_ok = int(sent_ok)
        tokens_ok = int(tokens_ok)    
        
        print("\nnombre de phrases : " + ("{0:,}".format(sents)).replace(",", " ") + "\nsentences ok : " + (
        "{0:,}".format(sent_ok)).replace(",", " ") + "\ntokens évalués : " + ("{0:,}".format(tokens_ok)).replace(","," "), file=sys.stderr)
        print("",file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_STANFORD' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            POS_STANFORD = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_TALISMANE' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            POS_TALISMANE = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_GREW' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            POS_GREW = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_STANFORD' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            DEP_STANFORD = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_TALISMANE' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            DEP_TALISMANE = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_GREW' + '.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            DEP_GREW =json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)
    else:
        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
        exit(1)
        

#    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/sentences_db' + '.json')
#    if os.path.exists(dir_json):
#        with open(dir_json, 'r') as outfile:
#            sentences_db = json.load(outfile)
#            print(dir_json + " chargé.", file=sys.stderr)
#    else:
#        print("refresh impossible, " + dir_json + " inexistant",file=sys.stderr)
#        exit(1)

else:
    exclude = set(string.punctuation)


    def stripPonctuation(string_):
        '''
        Supprime la ponctuation dans une chaîne, utile pour rapprocher deux textes, afin que la ponctuation ne participe pas au poids des ressemblances.
        :param string_: chaîne à dé-ponctualiser
        :return: chaîne dé-ponctualisée
        '''
        global exclude
        for c in string.punctuation:
            string_ = string_.replace(c, " ")
        return string_.replace("  ", " ")


    def getSet(line):
        '''
        Vise la création d'un set des mots d'une analyse type connll
        :param line: texte grosso modo au format connll, dont on retourne l'ensemble des mots
        :return: ensemble des mots d'une analyse type connll
        '''
        return set(k[1] for k in line)


    def getTab(line):
        '''
        Vise la création d'un tableau ordonné des mots d'une analyse type connll
        :param line: texte grosso modo au format connll, dont on retourne l'ensemble des mots
        :return: ensemble des mots d'une analyse type connll
        '''
        return [k[1] for k in line]


    def calculPivot(tab_words_talism, tab_words_grew, tab_words_stanford):
        '''
        Crée un dictionnaire des mots communs à trois analyses théoriquement distinctes contenant les indices rangés
        "{mot=indices_talismane, indices_grew,indices_stanford}"
        :param tab_words_talism: mots ordonnés arbitrairement désigné ici pour correspondre à ceux d'une analyse talismane
        :param tab_words_grew: mots ordonnés arbitrairement désigné ici pour correspondre à ceux d'une analyse grew
        :param tab_words_stanford: mots ordonnés arbitrairement désigné ici pour correspondre à ceux d'une analyse stanford
        :return: ensemble de triplets contenant table des indices des mots à décompte identique dans les trois ensembles
        '''
        words_talism = set(tab_words_talism)
        words_grew = set(tab_words_grew)
        words_stanford = set(tab_words_stanford)
        pivots = {}
        for word in words_talism.intersection(words_grew).intersection(words_stanford):
            if tab_words_talism.count(word) == tab_words_grew.count(word) == 1 and tab_words_stanford.count(
                    word) == tab_words_talism.count(word):
                indices_t = [i for i, x in enumerate(tab_words_talism) if x == word]
                indices_g = [i for i, x in enumerate(tab_words_grew) if x == word]
                indices_s = [i for i, x in enumerate(tab_words_stanford) if x == word]

                pivots[word] = (indices_t, indices_g, indices_s)
        return pivots


    def getRestructuredData(tab_of_words, tokens_sentence, pivots):
        '''
        Regroupe les items inter-pivots intercalés autour des dits pivots
        :param tab_of_words: mots ordonnés issus d'une analyse type connll
        :param tokens_sentence: analyse type connll : tableau de tableaux, chaque colonne étant une entrée d'analyse
        :param pivots: voir @pivots
        :return:
        '''
        new_tab = []
        start = True
        for i_w, word in enumerate(tab_of_words):
            if word in pivots.keys():
                new_tab.append([tokens_sentence[i_w]])
                start = True
            else:
                if start:
                    new_tab.append([])
                    start = False
                new_tab[-1].append(tokens_sentence[i_w])
        return new_tab


    def fusion(tabs):
        '''
        Crée une simili analyse à partir d'un ensemble d'analyses restructurées
        :param tabs: tableaux restructurés (voir @getRestructuredData)
        :return: tableau au contenu lissé : les entrées multiples (hors pivots) sont regroupées.
        '''
        tab_of_tabs = []
        for i in range(len(tabs)):
            if tabs[i] != []:
                tab_of_tabs.append(tabs[i])
        tab = []
        for indice in range(len(tab_of_tabs[0])):
            if indice == 1:
                sep = "_"
            else:
                sep = ", "
            value = []
            for tableau in tab_of_tabs:
                if tableau[indice] not in value:
                    value.append(tableau[indice])
            tab.append(sep.join(value))
        return tab

    for f_grew, i_tqdm in zip(files_grew, tqdm(range(len(files_grew)))):
        sentences_db = []
        f_talismane = f_grew.replace(args.grew, args.talismane)
        f_stanford = f_grew.replace(args.grew, args.stanford)
        f_texte = f_grew.replace(args.grew, args.texte)
        f_metas = f_texte.replace("aligned_tokens", "metas")


        if os.path.isfile(f_talismane) and os.path.isfile(f_stanford) and os.path.isfile(f_texte) and os.path.isfile(f_metas):
            # on a trois fichiers, on peut travailler
            data_talismane = open(f_talismane, "r").readlines()
            data_grew = open(f_grew, "r").readlines()
            data_stanford = open(f_stanford, "r").readlines()
            data_texte = open(f_texte, "r").readlines()
            data_metas = open(f_metas, "r").readlines()
            start_tokens = {'talismane': [], 'grew': [], 'stanford': []}
            tmp_sentences_t = []
            tmp_sentences_g = []
            tmp_sentences_s = []

            variables = (data_talismane, data_grew, data_stanford)
            tokens = {'talismane': {}, 'grew': {}, 'stanford': {}}

            # on construit les blocs
            for iv, variable in enumerate(variables):
                for i_var in range(len(variable)):
                    data = variable[i_var].strip().split("\t")
                    if data[0].isdigit():
                        if int(data[0]) == 1:
                            start_tokens[etiquettes[iv]].append([])
                        start_tokens[etiquettes[iv]][-1].append(data)

            for il, line in enumerate(data_texte):
                for key in tokens.keys():
                    tokens[key][line.strip()] = []

            il = 0
            for il, line in enumerate(data_texte):
                set_ref_sentence = set(stripPonctuation(line).split())
                for key in tokens.keys():
                    for i in range(len(start_tokens[key][0:15])):
                        sentence = start_tokens[key][i]
                        set_sentence = set(stripPonctuation(" ".join(getTab(sentence))).split(" "))
                        if (len(set_ref_sentence - set_sentence) / len(set_ref_sentence)) < 0.3:
                            tokens[key][line.strip()] = sentence
                            start_tokens[key] = start_tokens[key][i + 1:]
                            break

            keys_metas = {}
            for sentence, metas in zip(data_texte, data_metas):
                keys_metas[sentence.strip()] = metas.strip()
            # on fait du nettoyage

            sents_set = set(tokens['talismane'].keys()).union(set(tokens['stanford'].keys())).union(set(tokens['grew'].keys()))

            for key in tokens.keys():
                for sent in sents_set:
                    if tokens[key][sent] == []:
                        del tokens[key][sent]
            sents += len(sents_set)

            sentences_used = set(tokens['talismane'].keys()).intersection(set(tokens['stanford'].keys())).intersection(
                set(tokens['grew'].keys()))
            sent_ok += len(sentences_used)

            # go
            sent_courante = len(sentences_used)

            for sentence_used in sentences_used:
                
                sentence_talism = tokens['talismane'][sentence_used]
                sentence_grew = tokens['grew'][sentence_used]
                sentence_stanford = tokens['stanford'][sentence_used]

                sent_courante = 1
                # ras le bol, on va faire une table de fusion
                tab_words_talism = getTab(sentence_talism)
                tab_words_grew = getTab(sentence_grew)
                tab_words_stanford = getTab(sentence_stanford)

                pivots = {}

                pivots = calculPivot(tab_words_talism, tab_words_grew, tab_words_stanford)

                for indice in (-1, 0):
                    indice_ref = -2 if indice == -1 else 1
                    while tab_words_talism[indice] not in pivots.keys() and tab_words_talism[
                        indice_ref] in pivots.keys() and (
                            tab_words_grew[indice] == tab_words_talism[indice_ref] or tab_words_stanford[indice] ==
                            tab_words_talism[indice_ref]):
                        sentence_talism.pop(indice)
                        tab_words_talism = getTab(sentence_talism)
                        tab_words_grew = getTab(sentence_grew)
                        tab_words_stanford = getTab(sentence_stanford)
                        pivots = calculPivot(tab_words_talism, tab_words_grew, tab_words_stanford)
                    while tab_words_stanford[indice] not in pivots.keys() and tab_words_stanford[
                        indice_ref] in pivots.keys() and (
                            tab_words_grew[indice] == tab_words_stanford[indice_ref] or tab_words_talism[indice] ==
                            tab_words_stanford[indice_ref]):
                        sentence_stanford.pop(indice)
                        tab_words_talism = getTab(sentence_talism)
                        tab_words_grew = getTab(sentence_grew)
                        tab_words_stanford = getTab(sentence_stanford)
                        pivots = calculPivot(tab_words_talism, tab_words_grew, tab_words_stanford)
                    while tab_words_grew[indice] not in pivots.keys() and tab_words_grew[indice_ref] in pivots.keys() and (
                            tab_words_stanford[indice] == tab_words_grew[indice_ref] or tab_words_talism[indice] ==
                            tab_words_grew[indice_ref]):
                        sentence_grew.pop(indice)
                        tab_words_talism = getTab(sentence_talism)
                        tab_words_grew = getTab(sentence_grew)
                        tab_words_stanford = getTab(sentence_stanford)
                        pivots = calculPivot(tab_words_talism, tab_words_grew, tab_words_stanford)

                new_talism = getRestructuredData(tab_words_talism, sentence_talism, pivots)
                new_stanford = getRestructuredData(tab_words_stanford, sentence_stanford, pivots)
                new_grew = getRestructuredData(tab_words_grew, sentence_grew, pivots)

                if not (len(new_talism) == len(new_stanford)and len(new_talism) == len(new_grew)):
                    # la découpe stanford craint, à moins que ce soit mon fichier (deuxième option sans doute :=/)
                    # Au cumul du problème précédent on perd la phrase
                    if len(new_stanford) == len(new_talism) + 1 and  len(new_stanford) == len(new_grew) + 1:
                        if new_stanford[-2][0][1] == new_grew [-1][0][1]:
                            new_stanford.pop(-1)
                    # grew regroupe parfois en colonne 2 entre accolades des spécifications alors qu'il y a split de tokens
                    # le token avant l'accolade est placé seul en colonne 1, ce qui le rend potentiellement pivot
                    # vu le split sur les deux autres parseurs (ex:1^er). Deux pivots consécutifs d'un côté, la restructuration
                    # à besoin d'un coup de main. Sinon len(grew) < [len(stanford|len(talismane)]
                    if len(new_grew) < len(new_stanford) and  len(new_stanford) == len(new_talism):
                        s_pivots = set(pivots.keys())
                        group_ids = []
                        for i_new_stanford in range(len(new_stanford)-(len(new_stanford)-len(new_grew))-1):
                            # on est entre deux pivots
                            i_step = i_new_stanford
                            word_t = new_talism[i_step][0][1]
                            word_s = new_stanford[i_step][0][1]
                            word_g = new_grew[i_step][0][1]
                            if len(set([word_t, word_s, word_g]) - s_pivots) == 0:
                                word_t_1 = new_talism[i_step+1][0][1]
                                word_s_1 = new_stanford[i_step+1][0][1]
                                word_g_1 = new_grew[i_step+1][0][1]
                                if word_t_1 == word_s_1 and word_s_1 not in s_pivots and word_g_1 in s_pivots:
                                    group_ids.append(i_step)
                                if (len(new_stanford)-len(new_grew)) == len(group_ids):
                                    break
                        for group_id in sorted(group_ids, reverse=True):
                            new_talism[group_id] += new_talism[group_id+1]
                            del new_talism[group_id+1]
                            new_stanford[group_id] += new_stanford[group_id+1]
                            del new_stanford[group_id+1]

                if not (len(new_talism) == len(new_stanford) and len(new_talism) == len(new_grew) and len(new_stanford[-1])<3):
                    del new_talism
                    del new_stanford
                    del new_grew
                else:
                    if args.debug:
                        print(sentence_used, file=sys.stderr)
                    sentence_grew = []
                    sentence_stanford = []
                    sentence_talism = []
                    for i in range(len(new_talism)):
                        if len(new_talism[i]) == len(new_stanford[i]) and len(new_grew[i]) == len(new_stanford[i]):
                            for k in range(len(new_talism[i])):
                                sentence_grew.append(new_grew[i][k])
                                sentence_stanford.append(new_stanford[i][k])
                                sentence_talism.append(new_talism[i][k])
                        else:
                            steps = 0
                            for k in range(min(len(new_talism[i]), len(new_stanford[i]), len(new_grew[i])) - 1):
                                if new_grew[i][k][1] == new_stanford[i][k][1] and new_talism[i][k][1] == new_stanford[i][k][
                                    1]:
                                    sentence_grew.append(new_grew[i][k])
                                    sentence_stanford.append(new_stanford[i][k])
                                    sentence_talism.append(new_talism[i][k])
                                    steps += 1
                                else:
                                    break
                            if steps > 0:
                                for step in range(steps):
                                    new_grew[i].pop(0)
                                    new_stanford[i].pop(0)
                                    new_talism[i].pop(0)

                            sentence_grew.append(fusion(new_grew[i]))
                            sentence_stanford.append(fusion(new_stanford[i]))
                            sentence_talism.append(fusion(new_talism[i]))

                    # on est théoriquement sur les mêmes tokens
                    if not sentence_stanford[-1][1].count("_") < 3:
                        del sentence_grew
                        del sentence_stanford
                        del sentence_talism
                    else:
                        tokens_ok += len(sentence_grew)
                        sent_courante = 0
                        metas = keys_metas[sentence_used].split("\t")
                        if len(metas) == 1:
                            metas = [":", ":", metas[0]]
                        try:
                            metas[2] = 'source:' + metas[2]
                            if metas[1].strip() == '':
                                metas[1] = ':'
                            if args.debug:
                                print(metas, file=sys.stderr)
                        except:
                            print(metas, file=sys.std.err)
                            exit(5)
                        metas = {k.split(':')[0].strip():k.split(':')[1].strip() for k in metas}
                        sentences_db.append({'sentence':sentence_used,'talismane':sentence_talism, 'grew':sentence_grew,
                                             'stanford':sentence_stanford, 'metas':metas})
                        for tokens_sentence_talism, tokens_sentence_grew, tokens_sentence_stanford in zip(sentence_talism,
                                                                                                          sentence_grew,
                                                                                                          sentence_stanford):                                          
                            # gestion des part of speech
                                                                                                                                      
                            pos_tali = tokens_sentence_talism[index_pos_talismane]                                                             
                            pos_grew = tokens_sentence_grew[index_pos_grew]                                                       
                            pos_stan = tokens_sentence_stanford[index_pos_stanford]
                            
                            if pos_tali not in POS_TALISMANE.keys():
                                POS_TALISMANE[pos_tali] = {'stanford': {}, 'grew': {}}
                            
                            if pos_grew not in POS_GREW.keys():
                                POS_GREW[pos_grew] = {'stanford': {}, 'talismane': {}}
                            
                            if pos_stan not in POS_STANFORD.keys():
                                POS_STANFORD[pos_stan] = {'grew': {}, 'talismane': {}}
                            
                            # on s'occupe de talismane
                            i_p = 0
                            for pos in (pos_grew, pos_stan):
                                if i_p == 0:
                                    if pos not in POS_TALISMANE[pos_tali]['grew'].keys():
                                        POS_TALISMANE[pos_tali]['grew'][pos] = 0
                                    POS_TALISMANE[pos_tali]['grew'][pos] += 1
                                else:
                                    if pos not in POS_TALISMANE[pos_tali]['stanford'].keys():
                                        POS_TALISMANE[pos_tali]['stanford'][pos] = 0
                                    POS_TALISMANE[pos_tali]['stanford'][pos] += 1
                                i_p += 1
                            
                            # on s'occupe de grew
                            i_p = 0
                            for pos in (pos_stan, pos_tali):
                                if i_p == 0:
                                    if pos not in POS_GREW[pos_grew]['stanford'].keys():
                                        POS_GREW[pos_grew]['stanford'][pos] = 0
                                    POS_GREW[pos_grew]['stanford'][pos] += 1 
                                else:
                                    if pos not in POS_GREW[pos_grew]['talismane'].keys():
                                        POS_GREW[pos_grew]['talismane'][pos] = 0
                                    POS_GREW[pos_grew]['talismane'][pos] += 1
                                i_p += 1
                            
                            # on s'occupe de stanford
                            i_p = 0
                            for pos in (pos_grew, pos_tali):
                                if i_p == 0:
                                    if pos not in POS_STANFORD[pos_stan]['grew'].keys():
                                        POS_STANFORD[pos_stan]['grew'][pos] = 0
                                    POS_STANFORD[pos_stan]['grew'][pos] += 1
                                else:
                                    if pos not in POS_STANFORD[pos_stan]['talismane'].keys():
                                        POS_STANFORD[pos_stan]['talismane'][pos] = 0
                                    POS_STANFORD[pos_stan]['talismane'][pos] +=1
                                i_p += 1
                                    
                                            
                            # gestion des dépendances
                                                        
                            dep_tali = tokens_sentence_talism[index_dep_talismane]                                                             
                            dep_grew = tokens_sentence_grew[index_dep_grew]                                                       
                            dep_stan = tokens_sentence_stanford[index_dep_stanford]
                            
                            if dep_tali not in DEP_TALISMANE.keys():
                                DEP_TALISMANE[dep_tali] = {'stanford': {}, 'grew': {}}
                            
                            if dep_grew not in DEP_GREW.keys():
                                DEP_GREW[dep_grew] = {'stanford': {}, 'talismane': {}}
                            
                            if dep_stan not in DEP_STANFORD.keys():
                                DEP_STANFORD[dep_stan] = {'grew': {}, 'talismane': {}}
                            
                            # on s'occupe de talismane
                            i_p = 0
                            for dep in (dep_grew, dep_stan):
                                if i_p == 0:
                                    if dep not in DEP_TALISMANE[dep_tali]['grew'].keys():
                                        DEP_TALISMANE[dep_tali]['grew'][dep] = 0
                                    DEP_TALISMANE[dep_tali]['grew'][dep] += 1
                                else:
                                    if dep not in DEP_TALISMANE[dep_tali]['stanford'].keys():
                                        DEP_TALISMANE[dep_tali]['stanford'][dep] = 0
                                    DEP_TALISMANE[dep_tali]['stanford'][dep] += 1
                                i_p += 1
                            
                            # on s'occupe de grew
                            i_p = 0
                            for dep in (dep_stan, dep_tali):
                                if i_p == 0:
                                    if dep not in DEP_GREW[dep_grew]['stanford'].keys():
                                        DEP_GREW[dep_grew]['stanford'][dep] = 0
                                    DEP_GREW[dep_grew]['stanford'][dep] += 1 
                                else:
                                    if dep not in DEP_GREW[dep_grew]['talismane'].keys():
                                        DEP_GREW[dep_grew]['talismane'][dep] = 0
                                    DEP_GREW[dep_grew]['talismane'][dep] += 1
                                i_p += 1
                            
                            # on s'occupe de stanford
                            i_p = 0
                            for dep in (dep_grew, dep_tali):
                                if i_p == 0:
                                    if dep not in DEP_STANFORD[dep_stan]['grew'].keys():
                                        DEP_STANFORD[dep_stan]['grew'][dep] = 0
                                    DEP_STANFORD[dep_stan]['grew'][dep] += 1
                                else:
                                    if dep not in DEP_STANFORD[dep_stan]['talismane'].keys():
                                        DEP_STANFORD[dep_stan]['talismane'][dep] = 0
                                    DEP_STANFORD[dep_stan]['talismane'][dep] +=1
                                i_p += 1       
                del tokens['talismane'][sentence_used]
                del tokens['grew'][sentence_used]
                del tokens['stanford'][sentence_used]   
                                
        f_name = f_texte[f_texte.rindex('/')+1:]
        f_name = f_name[0:f_name.index("_")]
        dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/analyses/sentences_db_' + f_name + '.json')
        if args.refresh or not os.path.exists(dir_json):
            with open(dir_json, 'w') as outfile:
                json.dump(sentences_db, outfile, indent=2, ensure_ascii=False)
                
        if args.debug and sent_ok > 1000:
            print(("{0:,}".format(sent_ok)).replace(",", " "), file=sys.stderr)
            break

seuil_others = args.seuil_others  # pourcentage en-dessous duquel on ne considère pas représentatif l'échantillon
seuil_compte = args.seuil_compte  # nombre d'items pour que le tag soit pris en compte

allowed_pos_associations = {}
allowed_dep_associations = {}

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_STANFORD.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(POS_STANFORD, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_TALISMANE' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(POS_TALISMANE, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/POS_GREW' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(POS_GREW, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_STANFORD' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(DEP_STANFORD, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_TALISMANE' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(DEP_TALISMANE, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/DEP_GREW' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(DEP_GREW, outfile, indent=2, ensure_ascii=False)

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/analyse' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump((seuil_others, seuil_compte, sents, sent_ok, tokens_ok), outfile, indent=2, ensure_ascii=False)


# TODO : refaire les calculs
# [k, value for]

résultats = {}

for étiquette in etiquettes:
    résultats[étiquette] = {}
    if étiquette == 'stanford':
        repère = POS_STANFORD
    elif étiquette == 'talismane':
        repère = POS_TALISMANE
    elif étiquette == 'grew':
        repère = POS_GREW
    for pos in repère.keys():
        résultats[étiquette][pos] = {'compte': 0}
        for other in repère[pos].keys():
            résultats[étiquette][pos]['compte'] += len(repère[pos][other].keys())
        for other in repère[pos].keys():
            résultats[étiquette][pos][other] = [(k, v) for k, v in repère[pos][other].items()]
print("\n"
      "\documentclass[10pt,a4paper]{article}\n"
      "\\usepackage[utf8]{inputenc}\n"
      "\\usepackage[french]{babel}\n"
      "\\usepackage[T1]{fontenc}\n"
      "\\usepackage{amsmath}\n"
      "\\usepackage{amsfonts}\n"
      "\\usepackage{longtable}\n"
      "\\usepackage{tabu}\n"
      "\\usepackage{amssymb}\n"
      "\\usepackage{graphicx}\n"
      "\\usepackage{lmodern}\n"
      "\\usepackage{listingsutf8}\n"
      "\\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}\n"
      "\\author{Emilie Colin}\n"
      "\\begin{document}\n\\tableofcontents\n")
print("""
\lstset{%
  breaklines=true,
  basicstyle=\\normalfont\\ttfamily,
  columns=flexible,
  frame=single,
  inputencoding=utf8/latin1,
}
""")

print(
    "\paragraph{Vue d'ensemble}\n\\noindent\nnombre de phrases : " + ("{0:,}".format(sents)).replace(",", " ") + "\\\\"
                                                                                                                 "sentences ok : " + (
        "{0:,}".format(sent_ok)).replace(",", " ") + "\\\\"
                                                     "tokens évalués : " + ("{0:,}".format(tokens_ok)).replace(",",
                                                                                                               " "))
print(
    "\paragraph{Petites explications préalables}\nLes phrases perdues sont celles qui ont été splittées par un des analyseurs, éventuellement en deux ou trois morceaux, et pas dans les autres.\\\\C'est utile car des tags d'un parser donné ont du être regroupés pour y associer un tag d'un autre parser (ie: de le pour du). Certaines combinaisons sont tellement rares qu'elles doivent être liées à des erreurs d'alignement, mais sur (" + (
        "{0:,}".format(tokens_ok)).replace(",",
                                           " ") + " tokens * 3 parsers), il y a  beaucoup de petites choses apparaissant rendant le tex inexploitable pour la sélection d'association/la vue d'ensemble.")
print(
    "\\\\A été utilisé un seuil en-deça duquel les étiquettes n'ont pas été prises en compte(tag présenté dans une correspondance inter-parsers en nombre inférieur à " + seuil_compte.__str__() + "), et un pourcentage de représentativité éliminatoires (moins de " + seuil_others.__str__() + " pour cent des correspondances pour un tag donné, \\textit{others\#\\textbf{count}}  en précisant à chaque fois le nombre de ces autres non listés. )\n")
print(
    "\paragraph{Précision}Les items composites n'ont pas été sortis (items composite : créés de manière \\textit{ad hoc} quand les splits sur mots sont différents).")
next_page = ""
print("\section{POS}")

references_db['pos'] = {}
references_db['dep'] = {}

for étiquette, itqdm in zip(sorted(etiquettes), tqdm(range(len(etiquettes)))):
    if étiquette not in allowed_pos_associations.keys():
        allowed_pos_associations[étiquette] = {}
        references_db['pos'][étiquette] = allowed_pos_associations[étiquette]

    size_pos = len([k for k in résultats[étiquette] if ", " not in k])
    print("\subsection{" + étiquette + "}")

    print("\\begin{lstlisting}")
    print(sorted([k for k in résultats[étiquette] if ", " not in k]))
    print("\end{lstlisting}")
    print("\\begin{longtabu}{|X|c|c|}\n\caption{part of speech " + étiquette + "}\\\\")
    next_page = "\\newpage\n"
    print("\hline")
    end = " & "
    print(étiquette, end=end)
    for étiquette2 in sorted(etiquettes):
        if étiquette2 != étiquette:
            print(étiquette2, end=end)
            end = "\\\\\n\hline\n"
    print('\endfirsthead')
    print("""\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{part of speech for """ + étiquette + """ continued on next page}} \\\\
    \hline""")
    end = " & "
    print(étiquette, end=end)
    for étiquette2 in sorted(etiquettes):
        if étiquette2 != étiquette:
            print(étiquette2, end=end)
            end = "\\\\\n\hline\n"
    print("""\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{part of speech for """ + étiquette + """ continued on next page}} \\\\
    \endfoot
    \hline
    \endlastfoot""")

    dispersions = {}
    dispersions_percent = {}

    for pos_étiquette in sorted([k for k in résultats[étiquette] if ", " not in k]):
        if pos_étiquette not in allowed_pos_associations[étiquette].keys():
            allowed_pos_associations[étiquette][pos_étiquette] = {}
        values = [pos_étiquette]
        values_next_1 = []
        values_next_2 = []
        data = ""
        for étiquette2 in sorted(etiquettes):
            if étiquette2 != étiquette:
                if étiquette2 not in allowed_pos_associations[étiquette][pos_étiquette].keys():
                    allowed_pos_associations[étiquette][pos_étiquette][étiquette2] = {}
                if étiquette2 not in dispersions.keys():
                    dispersions[étiquette2] = []
                    dispersions_percent[étiquette2] = []
                if data == "":
                    data = sum([k[1] for k in résultats[étiquette][pos_étiquette][étiquette2] if k[1] > seuil_compte])
                    if data > 0:
                        calcul_other = [(décompte / data) * 100 for k, décompte in
                                        résultats[étiquette][pos_étiquette][étiquette2] if
                                        round((décompte / data) * 100, 2) <= seuil_others and décompte > seuil_compte]
                        values_next_1 = [(k, décompte, (round((décompte / data) * 100, 2)).__str__() + " \%") for
                                         k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]
                        dispersions[étiquette2].append(len([décompte  for
                                         k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        dispersions_percent[étiquette2].append(fsum([(décompte / data) * 100 for k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        allowed_pos_associations[étiquette][pos_étiquette][étiquette2] = {k:round((décompte / data) * 100, 2)for
                                         k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte}
                        if len(calcul_other) > 0:
                            values_next_1.append("\\textit{others (\#" + len(calcul_other).__str__() + ")} : " + (round(sum(calcul_other), 2)).__str__() + "\%")
                    else:
                        values_next_1 = []
                else:
                    data = sum([k[1] for k in résultats[étiquette][pos_étiquette][étiquette2] if k[1] > seuil_compte])
                    if data > 0:
                        calcul_other = [(décompte / data) * 100 for k, décompte in
                                        résultats[étiquette][pos_étiquette][étiquette2] if
                                        round((décompte / data) * 100, 2) <= seuil_others and décompte > seuil_compte]
                        values_next_2 = [(k, décompte, (round((décompte / data) * 100, 2)).__str__() + " \%") for
                                         k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]
                        dispersions[étiquette2].append(len([décompte for k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        dispersions_percent[étiquette2].append(fsum([(décompte / data) * 100 for k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        allowed_pos_associations[étiquette][pos_étiquette][étiquette2] = {k:round((décompte / data) * 100, 2) for
                                         k, décompte in résultats[étiquette][pos_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte}

                        if len(calcul_other) > 0:
                            values_next_2.append("\\textit{others (\#" + len(calcul_other).__str__() + ")} : " + (round(sum(calcul_other), 2)).__str__() + "\%")
                    else:
                        values_next_1 = []
        if not values_next_1 == [] and not values_next_2 == []:
            while len(values_next_1) != len(values_next_2) or len(values_next_1) != len(values):
                if len(values) < len(values_next_1) or len(values) < len(values_next_2):
                    values.append("~")
                if len(values_next_2) < len(values_next_1):
                    values_next_2.append("~")
                if len(values_next_1) < len(values_next_2):
                    values_next_1.append("~")

            for value, next_1, next_2 in zip(values, values_next_1, values_next_2):
                print("{0} & {1} & {2} \\\\".format(value, next_1, next_2).replace('\\\\%', '\\%'))
            print("\hline\n")

    print("\end{longtabu}")

    print(étiquette + " offre " + size_pos.__str__() + " étiquettes différentes, hors regroupement.")

    for key_p in dispersions.keys():
        print("\paragraph{Dispersion " + key_p + "} : ")
        print("Pour " + (len(dispersions[key_p])).__str__() + " POS différents : " + int(fsum(dispersions[key_p])).__str__() + " occurrences (en moyenne " + (round((fsum(dispersions[key_p])/len(dispersions[key_p])), 2)).__str__() + ").")
        print("\paragraph{Couverture " + key_p + " par " + étiquette + "} : moyenne : " + (round(fsum(dispersions_percent[key_p])/len(dispersions_percent[key_p]), 2)).__str__() + "\\%.")

# dépendances


résultats = {}
for étiquette in etiquettes:
    résultats[étiquette] = {}
    if étiquette == 'stanford':
        repère = DEP_STANFORD
    elif étiquette == 'talismane':
        repère = DEP_TALISMANE
    elif étiquette == 'grew':
        repère = DEP_GREW
    for dep in repère.keys():
        résultats[étiquette][dep] = {'compte': 0}
        for other in repère[dep].keys():
            résultats[étiquette][dep]['compte'] += len(repère[dep][other])
        for other in repère[dep].keys():
            résultats[étiquette][dep][other] = FreqDist(repère[dep][other]).most_common(len(repère[dep][other]))
next_page = ""

print("\section{Dependencies}")

for étiquette, itqdm in zip(sorted(etiquettes), tqdm(range(len(etiquettes)))):
    if étiquette not in allowed_dep_associations.keys():
        allowed_dep_associations[étiquette] = {}
        references_db['dep'][étiquette] = allowed_dep_associations[étiquette]
    size_dep = len([k for k in résultats[étiquette] if ", " not in k])
    print(next_page)
    print("\subsection{" + étiquette + "}")
    print("\\begin{lstlisting}")
    print(sorted([k for k in résultats[étiquette] if ", " not in k]))
    print("\end{lstlisting}")
    print("\\begin{longtabu}{|X|c|c|}\n\caption{dependencies " + étiquette + "}\\\\")
    next_page = "\\newpage\n"
    print("\hline")
    end = " & "
    print(étiquette, end=end)
    for étiquette2 in sorted(etiquettes):
        if étiquette2 != étiquette:
            print(étiquette2, end=end)
            end = "\\\\\n\hline\n"
    print('\endfirsthead')
    print("""\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{dependencies for """ + étiquette + """ continued on next page}} \\\\
    \hline""")
    end = " & "
    print(étiquette, end=end)
    for étiquette2 in sorted(etiquettes):
        if étiquette2 != étiquette:
            print(étiquette2, end=end)
            end = "\\\\\n\hline\n"
    print("""\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{dependencies for """ + étiquette + """ continued on next page}} \\\\
    \endfoot
    \hline
    \endlastfoot""")
    dispersions = {}
    dispersions_percent = {}

    for dep_étiquette in sorted([k for k in résultats[étiquette] if ", " not in k]):
        if dep_étiquette not in allowed_dep_associations[étiquette].keys():
            allowed_dep_associations[étiquette][dep_étiquette] = {}
        values = [dep_étiquette]
        values_next_1 = []
        values_next_2 = []
        data = ""
        for étiquette2 in sorted(etiquettes):
            if étiquette2 != étiquette:
                if étiquette2 not in allowed_dep_associations[étiquette][dep_étiquette].keys():
                    allowed_dep_associations[étiquette][dep_étiquette][étiquette2] = {}
                if étiquette2 not in dispersions.keys():
                    dispersions[étiquette2] = []
                    dispersions_percent[étiquette2] = []
                if data == "":
                    data = sum([k[1] for k in résultats[étiquette][dep_étiquette][étiquette2] if k[1] > seuil_compte])
                    if data > 0:
                        calcul_other = [(décompte / data) * 100 for k, décompte in
                                        résultats[étiquette][dep_étiquette][étiquette2] if
                                        round((décompte / data) * 100, 2) <= seuil_others and décompte > seuil_compte]
                        values_next_1 = [(k, décompte, (round((décompte / data) * 100, 2)).__str__() + " \%") for
                                         k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]
                        dispersions[étiquette2].append(len([décompte for k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        dispersions_percent[étiquette2].append(fsum([(décompte / data) * 100 for k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        allowed_dep_associations[étiquette][dep_étiquette][étiquette2] = {k:round((décompte / data) * 100, 2)  for
                                             k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                             round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte}
                        if len(calcul_other) > 0:
                            values_next_1.append("\\textit{others (\#" + len(calcul_other).__str__() + ")} : " + (round(sum(calcul_other), 2)).__str__() + "\%")
                    else:
                        values_next_1 = []
                else:
                    data = sum([k[1] for k in résultats[étiquette][dep_étiquette][étiquette2] if k[1] > seuil_compte])
                    if data > 0:
                        calcul_other = [(décompte / data) * 100 for k, décompte in
                                        résultats[étiquette][dep_étiquette][étiquette2] if
                                        round((décompte / data) * 100, 2) <= seuil_others and décompte > seuil_compte]
                        values_next_2 = [(k, décompte, (round((décompte / data) * 100, 2)).__str__() + " \%") for
                                         k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]
                        dispersions[étiquette2].append(len([décompte for k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        dispersions_percent[étiquette2].append(fsum([(décompte / data) * 100 for k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                         round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte]))
                        allowed_dep_associations[étiquette][dep_étiquette][étiquette2] = {k:round((décompte / data) * 100, 2) for
                                             k, décompte in résultats[étiquette][dep_étiquette][étiquette2] if
                                             round((décompte / data) * 100, 2) > seuil_others and décompte > seuil_compte}
                        if len(calcul_other) > 0:
                            values_next_2.append("\\textit{others (\#" + len(calcul_other).__str__() + ")} : " + (
                                round(sum(calcul_other), 2)).__str__() + "\%")
                    else:
                        values_next_1 = []
        if not values_next_1 == [] and not values_next_2 == []:
            while len(values_next_1) != len(values_next_2) or len(values_next_1) != len(values):
                if len(values) < len(values_next_1) or len(values) < len(values_next_2):
                    values.append("~")
                if len(values_next_2) < len(values_next_1):
                    values_next_2.append("~")
                if len(values_next_1) < len(values_next_2):
                    values_next_1.append("~")

            for value, next_1, next_2 in zip(values, values_next_1, values_next_2):
                print("{0} & {1} & {2} \\\\".format(value, next_1, next_2).replace('\\\\%', '\\%').replace("_", "\_"))
            print("\hline\n")

    print("\end{longtabu}")

    print(étiquette + " offre " + size_dep.__str__() + " étiquettes différentes, hors regroupement.")

    for key_p in dispersions.keys():
        print("\paragraph{Dispersion " + key_p + "} : ")
        print("Pour " + (len(dispersions[key_p])).__str__() + " DEP différentes : " + int(fsum(dispersions[key_p])).__str__() + " occurences (moyenne : " + (round((fsum(dispersions[key_p])/len(dispersions[key_p])), 2)).__str__() + ").")
        print("\paragraph{Couverture " + key_p + " par " + étiquette + "} : moyenne : " + (round(fsum(dispersions_percent[key_p])/len(dispersions_percent[key_p]), 2)).__str__() + "\\%.")

print("\end{document}\n")

print("\nsents : " + ("{0:,}".format(sents)).replace(",", " ") + "\n"
                                                                 "sents ok : " + ("{0:,}".format(sent_ok)).replace(",",
                                                                                                                   " ") + "\n"
                                                                                                                          "tokens ok : " + (
          "{0:,}".format(tokens_ok)).replace(",", " ") + "\n", file=sys.stderr)


dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/references_db' + '.json')
if args.refresh or not os.path.exists(dir_json):
    with open(dir_json, 'w') as outfile:
        json.dump(references_db, outfile, indent=2, ensure_ascii=False)



if args.debug:
    print(allowed_dep_associations, file=sys.stderr)
