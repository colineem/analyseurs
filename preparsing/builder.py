import argparse
import json
import sys
import os
from tqdm import tqdm
from math import fsum
import glob

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

parser = argparse.ArgumentParser(description="""Génère les fichiers de travail""")


parser.add_argument("-d", "--dir", type=str, default=os.path.join(fullpath, 'outputs/'),
                    help='chemin des fichiers')

parser.add_argument('--debug', action='store_true',
                    help='limite les traitements aux fichiers générés en debuggage')

parser.add_argument('--dep', action='store_true',
                    help='filtre sur les dépendances')

parser.add_argument('--shorted', action='store_true',
                    help='limité à 70 tokens')

parser.add_argument('--filter', action='store_true',
                    help='filtre les verbes')

parser.add_argument('--pos', action='store_true',
                    help='filtre sur les part of speach')

args = parser.parse_args()

if not os.path.isdir(args.dir):
    print("Pas de répertoire " + args.dir)
    exit(1)

if args.debug:
   sep_debug = "_debug"
else:
   sep_debug = ""

dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/references_db' + '.json')
if os.path.exists(dir_json):
    with open(dir_json, 'r') as outfile:
        references_db = json.load(outfile)
        print(dir_json + " chargé.", file=sys.stderr)
else:
    print("Chargement impossible, " + dir_json + " inexistant", file=sys.stderr)
    exit(2)

sentences_dbs = glob.glob(os.path.join(fullpath, 'outputs/results' + sep_debug + '/analyses/', "sentences_db_*.json"))

index_pos = 4
index_dep = 7
index_lemme = 2
index_lemme_talismane = 2
index_cat_pos_grew = 3

len_ok = 0
sentences_count = 0
kepts = []

others_verbes = {}
categories_verbales = {}


# categoriser.py permet de générer les ficheir catégoriels.
if os.path.exists(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'verbes.json')):
    with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'verbes.json'), 'r') as outfile:
        verbes = json.load(outfile)

    dir_json = os.path.join(fullpath, 'outputs/results' + sep_debug + '/', 'categories_verbales.json')
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            categories_verbales = json.load(outfile)
            print(dir_json + " chargé.", file=sys.stderr)

compteur = 0
def save(data):
    global compteur
    global args
    compteur += 1
    
    if args.shorted:
        with open(os.path.join(fullpath, 'outputs/results_70/', compteur.__str__().zfill(3) +'_analyses.txt'), 'w') as out:
            json.dump(data, out, indent=2, ensure_ascii=False)
    else:
        with open(os.path.join(fullpath, 'outputs/results' + sep_debug + '/kepts/', compteur.__str__().zfill(3) +'_analyses.txt'), 'w') as out:
            json.dump(data, out, indent=2, ensure_ascii=False)

for sentences_db, itdqm in zip(sentences_dbs, tqdm(range(len(sentences_dbs)-1))):
    tqdm.write(itdqm.__str__(), end='', file=sys.stderr)
    len_ok_tmp = 0

    dir_json = sentences_db
    if os.path.exists(dir_json):
        with open(dir_json, 'r') as outfile:
            try:
                sentences_db = json.load(outfile)
            except Exception as ex:
                print(dir_json + "...", file=sys.stderr)
                sentences_db = []
    else:
        print("Chargement impossible, " + dir_json + " inexistant", file=sys.stderr)
        exit(2)

    for sentence_dic in sentences_db:
        sentences_count += 1
        sentence = sentence_dic["sentence"]
        if "metas" not in sentence_dic.keys():
            os.remove(dir_json)
            break
        metas = sentence_dic["metas"]
        analyse_grew = sentence_dic["grew"]
        analyse_talismane = sentence_dic["talismane"]
        analyse_stanford = sentence_dic["stanford"]

        take = (not args.shorted) or len(analyse_grew) < 70


        if take:
            for analyzed_token_grew, analyzed_token_talismane, analyzed_token_stanford  in zip(analyse_grew, analyse_talismane, analyse_stanford):

                take = True
                if args.filter:

                    cat = analyzed_token_grew[index_cat_pos_grew]
                    lemme = analyzed_token_talismane[index_lemme_talismane]

                    if cat == "V":
                        if lemme not in categories_verbales.keys() and (lemme not in verbes.keys()):
                            take = False
                        else:
                            if lemme in verbes.keys() and verbes[lemme] < 40:
                                take = False

                if take :
                    if analyzed_token_grew[index_lemme] != analyzed_token_talismane[index_lemme] and \
                            analyzed_token_grew[index_lemme] not in ('cln', 'le', 'de', 'celui-ci', "que", "ne", "reine|roi",  'cln|ilimp', 'cld|cll', 'cla', 'cla|cld', 'quiAcc?|quiNom?') and \
                            "*" not in analyzed_token_grew[index_lemme] and \
                            analyzed_token_grew[3] != "CL" and \
                            not len(analyzed_token_grew[index_pos].split(",")) == 1 and \
                            not (analyzed_token_grew[index_lemme].count("_") == analyzed_token_talismane[index_lemme].count(",")) and  \
                            analyzed_token_talismane[index_lemme] not in ('la', 'des', 'de', 'les', 'une', "ses", "_", "ma"):
                        take = False
                        break

                    if args.pos:
                        if analyzed_token_grew[index_pos] not in references_db['pos']['grew'].keys():
                            take = False
                            break
                        # on test les pos à fort consensus
                        if fsum(references_db['pos']['grew'][analyzed_token_grew[index_pos]]['talismane'].values()) > 95:
                            if analyzed_token_talismane[index_pos] not in references_db['pos']['grew'][analyzed_token_grew[index_pos]]['talismane'].keys():
                                take = False
                                break
                        if fsum(references_db['pos']['grew'][analyzed_token_grew[index_pos]]['stanford'].values()) > 95:
                            if analyzed_token_stanford[index_pos] not in references_db['pos']['grew'][analyzed_token_grew[index_pos]]['stanford'].keys():
                                take = False
                                break

                    if analyzed_token_talismane[index_pos].startswith("V"):
                        if not analyzed_token_grew[index_pos].startswith("V") or analyzed_token_talismane[index_lemme_talismane] in ("voilà", "borne"):
                            take = False
                            break
                        if not (analyzed_token_stanford[index_pos].startswith("V") or analyzed_token_stanford[index_pos] == "AUX"):
                            take = False
                            break
                        if analyzed_token_grew[index_pos] == "VINF" and not  analyzed_token_talismane[index_pos] == "VINF":
                            take = False
                        if analyzed_token_talismane[index_pos] == "VINF" and not analyzed_token_grew[index_pos] == "VINF":
                            take = False

                    if args.dep:
                        if analyzed_token_grew[index_dep] not in references_db['dep']['grew'].keys():
                            take = False
                            break
                        # on test les dep à fort consensus, on exclut les grew "_"
                        if fsum(references_db['dep']['grew'][analyzed_token_grew[index_dep]]['talismane'].values()) > 94:
                            if analyzed_token_grew[index_dep] != "_":
                                if analyzed_token_talismane[index_dep] not in references_db['dep']['grew'][analyzed_token_grew[index_dep]]['talismane'].keys():
                                    take = False
                                    break
                        if fsum(references_db['dep']['grew'][analyzed_token_grew[index_dep]]['stanford'].values()) > 94:
                            if analyzed_token_grew[index_dep] != "_":
                                if analyzed_token_stanford[index_dep] not in references_db['dep']['grew'][analyzed_token_grew[index_dep]]['stanford'].keys():
                                    take = False
                                    break
        if take:
            kepts.append(sentence_dic)
            len_ok += 1
            len_ok_tmp += 1
            if len(kepts) == 5000:
                save(kepts)
                kepts = []

if len(kepts) > 0:
    save(kepts)
    kepts = []

print("", file=sys.stderr)
print("", file=sys.stderr)
print(sentences_count, end=" traités, ", file=sys.stderr)
print(len_ok, end=" sélectionnés.", file=sys.stderr)

