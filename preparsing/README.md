## détails des fichiers
getBooks.py : se connecte sur gutemberg et récupère les livres en français.
separateur.py : génère un corpus de phrases françaises à partir de livres théoriquement en français au format texte (gère encodage, non accentuation, et type d'enregistrement (fichier binaire ou non)
parse.py : lance les analyses des sorties de trois parseurs classiques : talismane, grew, stanford. 
analyze.py : explore les données de travail,  aligne les analyses des trois parseurs et sort à des fins de redirection vers fichier tex un tableau d'analyses des données traitées.
categoriser.py : génère les données d'analyse touchant aux verbes, permettant l'exclusion des phrases où des verbes socle n'ont pas été correctement codifiés.
builder.py : génère les fichiers de travail fonction des résultats d'analyses (correspondance inter-parseurs et verbes).


## les informations si après sont plus de l'aide au développement, gardé en tant que trace ##
cd ~/Dropbox/local/git/analyseurs/preparsing/tools

#### lancement analyse talismane : ####
(cd talismane-distribution-5.1.2-bin/ && java -Xmx1G -Dconfig.file=talismane-fr-5.0.4.conf -jar talismane-core-5.1.2.jar --analyse --sessionId=fr --encoding=UTF8 --inFile=data/frTest.txt --outFile=data/frTest.tal)

###### le fichier généré est à éditer. ######
###### --> prep à remplacer par obj.p ######
###### --> étiquettes première colonne nature mot : juste les initiales sauf P+D et PONCT ######
###### --> 2 dernières colonnes à videer pour des underscores ######
###### enregister sous test.surf.conll ######
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i talismane-distribution-5.1.2-bin/data/test.surf.conll -o test_talismane.ud2

#### conversion talismane ud2 ####
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i POStoSSQ/test.surf.conll -o test_grew.ud2

#### analyse grew : on envoie du texte vers melt
cat stanford-corenlp-full-2018-02-27/test.txt | MElt -T -L > POStoSSQ/test.melt 
grew transform -grs "POStoSSQ/grs/surf_synt_main.grs" -i POStoSSQ/test.melt -o test.surf.conll
##### conversion grew ud2 #####
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i POStoSSQ/test.surf.conll -o test_grew.ud2

#### analyse stanford ####
( cd stanford-corenlp-full-2018-02-27 && java -mx8g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP -outputFormat conllu -file test.txt  -props StanfordCoreNLP-french.properties)
( cd stanford-corenlp-full-2018-02-27 && java -mx8g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP   -annotators tokenize,ssplit,pos,lemma,ner,parse,depparse,udfeats  -props StanfordCoreNLP-french.properties -outputFormat conllu -file test.txt)

##### éditer fichier : punct par PONCT, NOUN par NC, ADP par P+D #####
##### conversion stanford grew ud2 #####
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i stanford-corenlp-full-2018-02-27/test.txt.conll -o test_stanford.ud2

####--- commandes lancées sur les cinq premiers livres####
  cd ~/Dropbox/Non\ synchronisé\ portable/git/analyseurs/
  cp outputs/aligned_tokens.txt tools/talismane-distribution-5.1.2-bin/data/frTest.txt
  cd tools
  cp outputs/analyses.txt tools/POStoSSQ/test.melt 
  cp outputs/aligned_tokens.txt tools/stanford-corenlp-full-2018-02-27/test.txt 
  cd ~/Dropbox/Non\ synchronisé\ portable/git/analyseurs/tools
  (cd talismane-distribution-5.1.2-bin/ && java -Xmx1G -Dconfig.file=talismane-fr-5.0.4.conf -jar talismane-core-5.1.2.jar --analyse --sessionId=fr --encoding=UTF8 --inFile=data/frTest.txt --outFile=data/frTest.tal)
  grew transform -grs "/home/colineem/Dropbox/Non synchronisé portable/git/tools/POStoSSQ/grs/surf_synt_main.grs" -i POStoSSQ/test.melt -o test.surf.conll
  ( cd stanford-corenlp-full-2018-02-27 && java -mx8g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP   -annotators tokenize,pos,lemma,ner,parse,depparse,udfeats  -props StanfordCoreNLP-french.properties -outputFormat conllu -file test.txt)

#####résultats : ##### 
ls -alh outputs/aligned_tokens.txt tools/talismane-distribution-5.1.2-bin/data/frTest.tal # talismane
ls -alh ~/Dropbox/Non\ synchronisé\ portable/git/analyseurs/tools/test.surf.conll # grew
ls -alh ~/Dropbox/Non\ synchronisé\ portable/git/analyseurs/tools/stanford-corenlp-full-2018-02-27/test.txt.conllu # stanford


