import os
import string
import subprocess
import argparse

from pprint import pprint

from tqdm import tqdm
tqdm.monitor_interval = 0 # bug multithreading #https://github.com/tqdm/tqdm/issues/481

import nltk
sent_tokenize = nltk.data.load('tokenizers/punkt/french.pickle')
from mosestokenizer import MosesTokenizer, MosesDetokenizer

from langdetect import detect
from langdetect import DetectorFactory
DetectorFactory.seed = 0

tokenizer = MosesTokenizer(lang='fr')

extra_abbreviations = ['mm', 'º', 'etc', "dr", "m", "mr"]
sent_tokenize._params.abbrev_types.update(extra_abbreviations)

detokenizer = MosesDetokenizer(lang='fr')

files = []
import glob, sys
import numpy

files = glob.glob("books/*/*.txt")
files += glob.glob("books/*.txt")

parser = argparse.ArgumentParser(description='Génération d\'un corpus à partir de livres français au format texte.')

parser.add_argument('--log', action='store_true',
                   help='Active les logs.',)

parser.add_argument('--debug', action='store_true',
                   help='Limite le nombre de livres à traiter.',)

args = parser.parse_args()

STDOUT = sys.stdout
DEBUG_COUNT = 3 # nombre de livres à prendre en debug

if args.log:
    sys.stdout = open('separateur.log', 'w') # TODO répertoire dates...

sentences_set = set()
not_in_french =  open('french errors.log', 'w')
are_french = [] if not os.path.isfile('are_french.log') else set(open('are_french.log','r').readlines())
english_words_selection = ('produced', 'Distributed', 'previous', "copying", "easy", "derivative", "practically",
                           "especially", "Section", "works", "about", "links", "located", "requirements", "keep",
                           "work", "electronic", "agree", "agreement", "below", "received", "limited", "provided",
                           "you", "purpose", "allow", "including", "hundreds", "associated", "directory", "filename",
                           "including", "renamed", "paragraph", "laws", "copy", "alternate", "owed", "volunteers",
                           "provide", "Foundation", "posted", "Contributions", "maintaining", "committed", "where",
                           "methods", "confirmed", "replaced", "identical", "corresponding", "em.", "corresponding",
                           "number", "succeeded", "unfailing", "lightness", "he", "him", "she", "and", "be", "the",
                           "to", "of", "was", 'this', "her", "all", "with", "but", "us", "their", "in", "here", "they",
                           "me", "study", "been", "written", "by")
maybe_french = set()

sentences = []
kept = []
align_tok = []
meta = []
ponctuation_finale = (".", "!", "?")

print("Traitement fichiers")
for fichier, iq in zip(sorted(files), tqdm(range(len(files)))):

    if args.debug and iq > DEBUG_COUNT:
        break

    encoding = subprocess.Popen("file -i \"" + fichier + "\" | cut -d '=' -f 2", shell=True,
                  stdout=subprocess.PIPE).stdout.read().decode('utf8').strip()

    print(encoding)
    source = ''
    author = ''
    if encoding.strip() not in ("unknown-8bit"):
        wrk_sentences = []
        if encoding == "binary":
            f = open(fichier, 'rb')
        else:
            f = open(fichier, 'r', encoding = encoding, errors='replace')
        take = False
        b_start = False
        b_title_start = False
        b_title_end= False
        b_author_start = False
        b_author_end = False
        b_start = False
        for sentence in f:
            if encoding == "binary":
                sentence = sentence.decode('latin-1')
            sentence = sentence.lstrip()
            wrk_sentences.append(sentence)

            if b_start and not take and "é" in sentence or "è" in sentence or "ç" in sentence:
                take = True

            if not b_start and "START" in sentence:
                wrk_sentences = []
                b_start = True

                print(fichier)
                print(source)
                print(author)
            elif not b_start:
                if sentence.startswith("Title"):
                    source = sentence.rstrip()
                    b_title_start = True
                elif b_title_start and not b_title_end:
                    if sentence.strip() == "":
                        b_title_end = True
                    else:
                        source += " " + sentence.rstrip()

                if sentence.startswith("Author"):
                    author = sentence.rstrip()
                    b_author_start = True
                elif b_author_start and not b_author_end:
                    if sentence.strip() == "":
                        b_author_end = True
                    else:
                        author += " " + sentence.rstrip()

        if take:
            wrk_sentences ="\n".join(wrk_sentences).replace("--", "\n--")
            wrk_sentences  = sent_tokenize.tokenize(wrk_sentences.replace("\n", " "))

            for sentence in wrk_sentences:
                retokenized = detokenizer.detokenize(tokenizer.tokenize(sentence), return_str=True)
                if retokenized[-1].endswith(ponctuation_finale) and "_" not in retokenized and "[" not in retokenized and "]" not in retokenized and retokenized[0].isalpha() and retokenized[0].upper() == retokenized[0]   and not retokenized[0:2].upper() == retokenized[0:2]  and not retokenized[0:5].upper() == retokenized[0:5]  and not retokenized[0].isdigit():
                    if retokenized not in sentences_set:
                        in_french = (detect(retokenized) == 'fr' or retokenized.strip() + '\n' in are_french) and retokenized.count('"') %2 == 0
                        # check balancing
                        if in_french:
                            lefts, rights = [], []
                            lefts.extend(["«", "(", "[", "{"])
                            rights.extend(["»", ")", "]", "}"])
                            for symboll, symbolr in zip(lefts, rights):
                                if retokenized.count(symboll) != retokenized.count(symbolr) :
                                    in_french = False
                                    break

                            if in_french:
                                sentences_set.add(retokenized)
                                align_tok.append(retokenized)
                                sentences.append(align_tok[-1][:])
                                while "  " in sentence:
                                    sentence = sentence.replace("  ", " ")
                                kept.append(sentence)
                                meta.append(source.replace("\t",' ') + "\t" + author.replace("\t",' ') + "\t" + fichier.replace("\t",' '))
                        else:
                            if set(retokenized.lower().split(" ")).isdisjoint(english_words_selection):
                                if retokenized not in maybe_french:
                                    not_in_french.write(retokenized + '\n')
                                maybe_french.add(retokenized)

not_in_french.close()

nb_sentences = len(sentences)

CHUNK_SIZE = 1000

sentences = [sentences[i:i + CHUNK_SIZE] for i in range(0, len(sentences), CHUNK_SIZE)]
align_tok = [align_tok[i:i + CHUNK_SIZE] for i in range(0, len(align_tok), CHUNK_SIZE)]
kept = [kept[i:i + CHUNK_SIZE] for i in range(0, len(kept), CHUNK_SIZE)]
meta = [meta[i:i + CHUNK_SIZE] for i in range(0, len(meta), CHUNK_SIZE)]

if not os.path.exists('tmp/'):
    os.makedirs('tmp/')
    
wrk_sentences = []
len_chunked_tab = len(sentences)
for i in tqdm(range(len_chunked_tab)):
    in_progress = sentences[i]
    wrk_sentences.append([])
    try:
        fm = open('tmp/lot_'+ i.__str__(), "w")
        to_do = "\n".join(in_progress)
        fm.write(to_do)
        fm.close()
        while not fm.closed:
            True
        results = (subprocess.Popen("cat \"" + 'tmp/lot_'+ i.__str__() + "\" | MElt -T -L  2>/dev/null", shell=True,
                      stdout=subprocess.PIPE).stdout.read().decode('utf8'))
        wrk_sentences[-1] = results.split("\n")
    except:
        CHUNK_SIZE = 200
        tab_p = [sentences[i][j:j + CHUNK_SIZE] for j in range(0, len(sentences[i]), CHUNK_SIZE)]
        for j in range(len(tab_p)):
            to_do = "\n".join(tab_p[j])
            fm = open('tmp/lot_'+ i.__str__() + "_" +  j.__str__(), "w")
            fm.write(to_do)
            fm.close()
            while not fm.closed:
                True
            results = (subprocess.Popen("echo \"" + 'tmp/lot_'+ i.__str__() + "_" +  j.__str__() + "\" | MElt -T -L  2>/dev/null ", shell=True,
                                        stdout=subprocess.PIPE).stdout.read().decode('utf8'))
            wrk_sentences[-1].extend(results.split("\n"))

analyses = []
sentences = []
align_tokens = []
metas = []
print("Traitement sentences")
for i in tqdm(range(len_chunked_tab)):
    for analyse, sentence, at, m in zip(wrk_sentences[i], kept[i], align_tok[i], meta[i]):
        if "/V/" in analyse and "/NC/" in analyse and not analyse.split("/")[1] == "CC":
            # on ne va pas garder ce qui démarre par une conjonction de coordination
            analyses.append(analyse)
            sentences.append(sentence)
            align_tokens.append(at)
            metas.append(m)


if not os.path.exists('outputs/'):
    os.makedirs('outputs/')

with open('outputs/analyses.txt', 'w') as out:
    out.write("\n".join(analyses) + '\n')

with open('outputs/sentences.txt', 'w') as out:
    out.write("\n".join(sentences) + '\n')

with open('outputs/aligned_tokens.txt', 'w') as out:
    out.write("\n".join(align_tokens) + '\n')

with open('outputs/metas.txt', 'w') as out:
    out.write("\n".join(metas) + '\n')


CHUNK_SIZE = 5000

if not os.path.exists('outputs/chunked_' + CHUNK_SIZE.__str__() + '/'):
    os.makedirs('outputs/chunked_' + CHUNK_SIZE.__str__() + '/')

if (len(analyses)) > CHUNK_SIZE:
    analyses = [analyses[i:i + CHUNK_SIZE] for i in range(0, len(analyses), CHUNK_SIZE)]
    sentences = [sentences[i:i + CHUNK_SIZE] for i in range(0, len(sentences), CHUNK_SIZE)]
    align_tokens = [align_tokens[i:i + CHUNK_SIZE] for i in range(0, len(align_tokens), CHUNK_SIZE)]
    metas = [metas[i:i + CHUNK_SIZE] for i in range(0, len(metas), CHUNK_SIZE)]
else:
    analyses = [analyses]
    sentences = [sentences]
    align_tokens = [align_tokens]
    metas = [metas]


print(len(analyses))
print("Traitement sauvegardes")
for i in tqdm(range(len(analyses))):
    compteur = i.__str__().zfill(10)

    with open('outputs/chunked_' + CHUNK_SIZE.__str__() + '/'+ compteur +'_analyses.txt', 'w') as out:
        out.write("\n".join(analyses[i]) + '\n')

    with open('outputs/chunked_' + CHUNK_SIZE.__str__() + '/'+ compteur +'_sentences.txt', 'w') as out:
        out.write("\n".join(sentences[i]) + '\n')

    with open('outputs/chunked_' + CHUNK_SIZE.__str__() + '/'+ compteur +'_aligned_tokens.txt', 'w') as out:
        out.write("\n".join(align_tokens[i]) + '\n')

    with open('outputs/chunked_' + CHUNK_SIZE.__str__() + '/'+ compteur +'_metas.txt', 'w') as out:
        out.write("\n".join(metas[i]) + '\n')


if args.log:
    sys.stdout = STDOUT
