import os
import regex as re
import argparse
import networkx as nx
import sys
import subprocess
from pattern.fr import conjugate
from pattern.fr import INFINITIVE
from pattern.fr import INDICATIVE, IMPERATIVE, CONDITIONAL, SUBJUNCTIVE, PARTICIPLE
from pattern.fr import IMPERFECTIVE, PROGRESSIVE, PERFECTIVE, PRESENT, PAST, FUTURE
from pattern.fr import SG, PL
from pprint import pprint
import glob
import sqlite3
import random
import inspect
import json
import itertools
import re

from tqdm import tqdm
__MAISON__ = False
__PERCENT_DEBUG_MODE__ = 10
__outed__ = {}
tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481

sentences_treated = set()

sql_file_greffes = ":memory:"
sql_file_greffes = "greffes.db"

print(sql_file_greffes, end=" used.\nFile exists: ")
greffe_create =  not os.path.isfile(sql_file_greffes)

print(os.path.isfile(sql_file_greffes))
connexion = sqlite3.connect(sql_file_greffes)
curseur = connexion.cursor()  # Récupération d'un curseur
curseurSecondaire = connexion.cursor()  # Récupération d'un curseur

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481

parser = argparse.ArgumentParser(description="Jonction LVF data")

parser.add_argument('--source', dest='source', type=str, default="train_socle.txt")
parser.add_argument('--pred', dest='pred', type=str, default="train.txt")
parser.add_argument('--debug', dest='debug', action='store_true')
parser.add_argument('--ask_small', dest='ask_small', action='store_true')
parser.add_argument('--percent', dest='percent', default=__PERCENT_DEBUG_MODE__, type=int)

args = parser.parse_args()

class Trait():
    def __init__(self, traits: str):
        values = traits.split("￨")
        self._value = values.pop(0)
        self._desc = ", ".join([k for k in values if k not in ("", "-") and not k.isdigit()])
        self._universal_pos = values.pop(0)
        self._gender = values.pop(0)
        self._number = values.pop(0)
        self._temps = values.pop(0)
        self._mode = values.pop(0)
        self._person = values.pop(0)
        self._arg_0 = values.pop(0)
        self._arg_1 = values.pop(0)
        self._arg_2 = values.pop(0)
        self._arg_3 = values.pop(0)
        self._type_arg_0 = values.pop(0)
        self._type_arg_1 = values.pop(0)
        self._type_arg_2 = values.pop(0)
        self._type_arg_3 = values.pop(0)
        if self._type_arg_0 != "":
            self._desc = self._desc.replace(", " + self._type_arg_0, "")
        if self._type_arg_1 != "":
            self._desc = self._desc.replace(", " + self._type_arg_1, "")
        if self._type_arg_2 != "":
            self._desc = self._desc.replace(", " + self._type_arg_2, "")
        if self._type_arg_3 != "":
            self._desc = self._desc.replace(", " + self._type_arg_3, "")
        self._dep = values.pop(0)
        self._nature = values.pop(0)
        self._events = []
        self._aux = []

    def add_event(self, event_node_id, position, nature):
        self._events.append((event_node_id, position, nature))

    def add_aux(self, trait):
        self._aux.append(trait)

    def getTrait(self):
        values = []

        values.append(self._value)
        values.append(self._universal_pos)
        values.append(self._gender)
        values.append(self._number)
        values.append(self._temps)
        values.append(self._mode)
        values.append(self._person)
        values.append(self._arg_0)
        values.append(self._arg_1)
        values.append(self._arg_2)
        values.append(self._arg_3)
        values.append(self._type_arg_0)
        values.append(self._type_arg_1)
        values.append(self._type_arg_2)
        values.append(self._type_arg_3)
        values.append(self._dep)
        values.append(self._nature)
        return "￨".join(values)

    @property
    def mode(self):
        return self._mode

    @property
    def temps(self):
        return self._temps

    @property
    def value(self):
        return self._value

    @property
    def universal_pos(self):
        return self._universal_pos

    @property
    def nature(self):
        return self._nature

    def __str__(self) -> str:
        informations = [self.universal_pos, "dep " + self._dep, "arg_0 " + self._arg_0, "arg_1 " + self._arg_1,
                        "arg_2 " + self._arg_2, "arg_3 " + self._arg_3]
        return self.value + " (" + ", ".join([k for k in informations if k.strip() == k]) + ")"

if greffe_create:
    if not greffe_create :
        os.remove(sql_file_greffes)
        connexion.close()
        connexion = sqlite3.connect(sql_file_greffes)
        curseur = connexion.cursor()  # Récupération d'un curseur
        curseurSecondaire = connexion.cursor()  # Récupération d'un curseur

    __SMALL__ = False
    curseur.execute('''DROP TABLE IF EXISTS greffes_support ''')
    sujs_voice = {}
    curseur.execute('''CREATE TABLE asked_sentences (
        ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        sent_id TEXT,
        specif TEXT,
        in_ TEXT,
        out_ TEXT,
        time_event_1 TEXT,
        structure_event_1 TEXT,
        event_1 TEXT,
        event_1_ask TEXT,
        sujet_event_1 TEXT,
        sujet_event_1_properties TEXT,
        sujet_event_1_ask TEXT)
        ''')

    curseur.execute("CREATE INDEX asked_sentences_data_sent_id ON asked_sentences (sent_id);")
    curseur.execute("CREATE INDEX asked_sentences_data_sent_id_det ON asked_sentences (sent_id, event_1, event_1_ask );")
    curseur.execute("CREATE INDEX asked_sentences_data_sent_id_det_tme ON asked_sentences (sent_id, time_event_1, sujet_event_1_ask );")

    curseur.execute('''CREATE TABLE greffes_data (
        ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        sent_id TEXT,
        token_id TEXT,
        traits_args TEXT,  
        traits_complets TEXT,  
        cut TEXT,  
        universal_pos TEXT,  
        gender TEXT,  
        number TEXT,  
        person TEXT,  
        dep TEXT,  
        nature TEXT,  
        level TEXT,  
        prep TEXT,  
        categorie TEXT,  
        sentence TEXT,  
        verbs_count TEXT,  
        args TEXT,  
        sujet_event_1_properties TEXT,  
        type_lvf TEXT,  
        used_verbs TEXT)''')
    curseur.execute("CREATE INDEX greffes_data_sent_id ON greffes_data (traits_args);")
    curseur.execute("CREATE INDEX greffes_data_upos ON greffes_data (universal_pos);")
    curseur.execute("CREATE INDEX greffes_data_gender ON greffes_data (gender);")
    curseur.execute("CREATE INDEX greffes_data_number ON greffes_data (number);")
    curseur.execute("CREATE INDEX greffes_data_person ON greffes_data (person);")
    curseur.execute("CREATE INDEX greffes_data_nature ON greffes_data (nature);")
    curseur.execute("CREATE INDEX greffes_data_prep ON greffes_data (prep);")
    curseur.execute("CREATE INDEX greffes_data_level ON greffes_data (level);")
    curseur.execute("CREATE INDEX greffes_data_prep_level ON greffes_data (level, prep);")
    curseur.execute("CREATE INDEX greffes_data_all ON greffes_data (universal_pos, gender , number , person , dep , nature , categorie, level, prep);")

    def get_out(message:str=''):
        """Store the current line number in our program."""
        ligne = inspect.currentframe().f_back.f_lineno
        index = "l." + ligne.__str__() + ((" "  + message) if message != "" else "")
        if index not in __outed__:
            __outed__[index] = 1
        else:
            __outed__[index] += 1
        if message == "":
            print()
            print(ligne)
            raise Exception

    ok_accord = 0
    to_ok_accord = 0

    file_output = False

    if args.debug or file_output:
        __PERCENT_DEBUG_MODE__ = args.percent
        __SMALL__ = 500

    if not os.path.exists(args.source):
        print(args.source + " does not exist.")
        exit(1)

    if not os.path.exists(args.pred):
        print(args.pred + " does not exist.")
        exit(2)

    if not args.source.endswith(".txt"):
        print(args.source + " must be a .txt file.")
        exit(2)

    if not args.pred.endswith(".txt"):
        print(args.source + " must be a .txt file.")
        exit(2)

    if not args.source.count(".txt") == 1:
        print(args.source + " must have only one .txt.")
        exit(2)

    if not os.path.exists(args.source.replace('.txt', '.json.txt')):
        print(args.source.replace('.txt', '.json.txt') + " must exist.")
        exit(3)

    try:
        import pygraphviz
        from networkx.drawing.nx_agraph import write_dot
        # print("using package pygraphviz", file=sys.stderr)
    except ImportError:
        try:
            import pydot
            from networkx.drawing.nx_pydot import write_dot

            print("using package pydot", file=sys.stderr)
        except ImportError:
            print("", file=sys.stderr)
            print("Both pygraphviz and pydot were not found ", file=sys.stderr)
            print("see  https://networkx.github.io/documentation/latest/reference/drawing.html", file=sys.stderr)
            print("", file=sys.stderr)
            raise

    in_features = []
    __ref_dir__ = "../../corpusGenerator/features_IO_pro_pers/buildCoNLL-U-with_traits-join/data"
    def loadFeaturesIn():
        global __ref_dir__, in_features
        fichiers = sorted(glob.glob(__ref_dir__ + '/test.source_feature_*.*'), key=lambda fichier:int(fichier.split("_")[-1].split(".")[0]))
        for fichier in fichiers:
            in_features.append(set())
            for line in open(fichier):
                if line.startswith(" "):
                    in_features[-1].add("")
                else:
                    in_features[-1].add(line.split()[0])
    loadFeaturesIn()

    if __MAISON__:
        num_lines_pred = 2289518
    else:
        num_lines_pred = sum(1 for line in open(args.pred))
        print(num_lines_pred, end=" lines for pred.\n")

    if __MAISON__:
        num_lines_source = num_lines_pred
    else:
        num_lines_source = sum(1 for line in open(args.source))
        print(num_lines_source, end=" lines for source.\n")

    num_lines_info = num_lines_source
    if num_lines_pred > num_lines_source:
        exit(4)
    if num_lines_pred > num_lines_info:
        exit(4)

    source_file = open(args.source, 'r')
    info_file_orig = args.source.replace('.txt', '.json.txt')
    info_file = open(info_file_orig, 'r')
    # info_file += [""] * num_lines_info
    suivi_file_orig = args.source.replace('.txt', '.suivi.txt')
    suivi_file = open(suivi_file_orig, 'r')
    # suivi_file += [""] * num_lines_info

    pred_file = open(args.pred, 'r')

    print("source :", end=args.source+"\n")
    print("pred :", end=args.pred+"\n")
    print("info_file :", end=info_file_orig+"\n")
    print("suivi_file :", end=suivi_file_orig+"\n")

    source_file_filtred = open(args.source.replace('.txt', '_filtred.txt'), 'w')
    info_file_filtred = open(info_file_orig.replace('.json.txt', '_filtred.json.txt'), 'w')
    pred_file_filtred = open(args.pred.replace('.txt', '_filtred.txt'), 'w')
    suivi_file_filtred = open(suivi_file_orig.replace('.txt', '_filtred.txt'), 'w')

    def verif(asked, obtained):
        return True

    verbes = {}
    verbes_générés = {}
    aux_tps = {}
    passifs = {}
    event_1_allowed_times = {'conditionnel': {'passé': {'participe': {'passé'}},
                                      'présent': {'conditionnel': {'présent'}}},
     'indicatif': {'futur': {'indicatif': {'futur'}},
                   'futur-antérieur': {'participe': {'passé'}},
                   'imparfait': {'indicatif': {'imparfait'}},
                   'passé-antérieur': {'participe': {'passé'}},
                   'passé-composé': {'participe': {'passé'}},
                   'passé-simple': {'indicatif': {'passé-simple'}},
                   'plus-que-parfait': {'participe': {'passé'}},
                   'présent': {'indicatif': {'présent'}}}}

    event_1_allowed_times_sub = event_1_allowed_times.copy()
    event_1_allowed_times_sub['participe'] = {'présent': {'participe': {'présent'}},
                                               'passé': {'participe': {'passé'}}}
    event_1_allowed_times_sub['subjonctif'] = {'présent': {'subjonctif': {'présent'}},
                                               'passé': {'participe': {'passé'}},
                                               'imparfait': {'subjonctif': {'imparfait'}},
                                               'plus-que-parfait': {'participe': {'passé'}}}
    event_1_allowed_times_sub['infinitif'] = {'na': {'infinitif': {'na'}}}
    count_kept = 0
    num_lines_debug = 0

    accords = {"no_wait":{}, "wait":{}}

    temps_auxiliaire = {}
    args_data = {"arg_0", "arg_1", "arg_2", "arg_3"}

    pronouns_arg_0 = {'': {'': {'person:3': {'': {"qu'": 42, 'que': 7}}},
          'gender:masc': {'person:3': {'': {'ce': 226, 'qui': 4},
                                       'actualisation:indefini': {'aucun': 1}}}}, 'number:plur': {'': {'person:1': {'': {'nous': 42651,
                                            'nôtres': 10},                                  'actualisation:defini': {'nous': 1,
                                                                'nôtres': 4086}},
                          'person:2': {'': {'-vous': 5,
                                            'vous': 32707},
                                       'actualisation:defini': {'vous': 2},
                                       'actualisation:indefini': {'vous': 536}},
                          'person:3': {'': {'autres': 1131,
                                            'cent': 1,
                                            'dix': 4,
                                            'elles-mêmes': 1,
                                            'plusieurs': 1,
                                            'quatre': 22,
                                            'six': 2,
                                            'trois': 19},
                                       'actualisation:defini': {'autres': 11935,
                                                                'cent': 143,
                                                                'dix': 710,
                                                                'quatre': 2346,
                                                                'six': 45,
                                                                'trois': 18},
                                       'actualisation:indefini': {'autres': 7478,
                                                                  'dix': 9,
                                                                  'plusieurs': 2}}},
                     'gender:fem': {'person:3': {'': {'-elles': 712,
                                                      'celles': 11,
                                                      'celles-ci': 87,
                                                      'celles-là': 9,
                                                      'elles': 12886,
                                                      'quelques-unes': 12,
                                                      'unes': 9},
                                                 'actualisation:defini': {'unes': 5150},
                                                 'actualisation:indefini': {'elles': 2,
                                                                            'unes': 72}}},
                     'gender:masc': {'person:3': {'': {'-ils': 1815,
                                                       'certains': 28,
                                                       'ceux': 1140,
                                                       'ceux-ci': 11720,
                                                       'deux': 309,
                                                       'eux-mêmes': 15,
                                                       'ils': 14224,
                                                       'plusieurs': 5049,
                                                       'quelques-uns': 153,
                                                       'tous': 187,
                                                       'uns': 179},
                                                  'actualisation:defini': {'-ils': 50,
                                                                           'deux': 6567,
                                                                           'eux-mêmes': 1,
                                                                           'quelques-uns': 7,
                                                                           'siens': 7,
                                                                           'tous': 2,
                                                                           'uns': 9409},
                                                  'actualisation:indefini': {'-ils': 63,
                                                                             'certains': 13,
                                                                             'eux-mêmes': 2,
                                                                             'ils': 2,
                                                                             'plusieurs': 177,
                                                                             'sien': 4,
                                                                             'uns': 4260}}}},
     'number:sing': {'': {'person:1': {'': {'-je': 3,
                                            "j'": 3236,
                                            'je': 46563,
                                            "m'": 3,
                                            'me': 3,
                                            'moi': 6,
                                            'nôtre': 30},
                                       'actualisation:defini': {"j'": 1,
                                                                'je': 3,
                                                                'moi': 614,
                                                                'nôtre': 2533},
                                       'actualisation:indefini': {"j'": 448,
                                                                  'je': 755,
                                                                  'moi': 44,
                                                                  'nôtre': 133}},
                          'person:2': {'': {'-tu': 2,
                                            "t'": 7,
                                            'te': 1,
                                            'toi': 3,
                                            'tu': 33937},
                                       'actualisation:defini': {}},
                          'person:3': {'': {'-ce': 198,
                                            '13': 21,
                                            'aucun': 1,
                                            'autre': 449,
                                            'nul': 7,
                                            'un': 6},
                                       'actualisation:defini': {'-ce': 9,
                                                                '13': 766,
                                                                'autre': 6711},
                                       'actualisation:indefini': {'aucun': 3,
                                                                  'autre': 4036}}},
                     'gender:fem': {'person:1': {'': {},
                                                 'actualisation:defini': {'mienne': 4170,
                                                                          'nôtre': 36}},
                                    'person:2': {'': {}},
                                    'person:3': {'': {'-elle': 1010,
                                                      '-t-elle': 597,
                                                      'aucune': 7,
                                                      'autre': 6,
                                                      'celle-ci': 11565,
                                                      'celle-là': 1,
                                                      'chacune': 4,
                                                      'elle': 17433,
                                                      'elle-même': 1,
                                                      'sienne': 79,
                                                      'une': 13},
                                                 'actualisation:defini': {'-elle': 30,
                                                                          '-t-elle': 31,
                                                                          'sienne': 3697,
                                                                          'une': 2043},
                                                 'actualisation:indefini': {'-elle': 24,
                                                                            '-t-elle': 14,
                                                                            'autre': 1023,
                                                                            'celle': 3,
                                                                            'celle-ci': 6,
                                                                            'celle-là': 2,
                                                                            'sienne': 643}}},
                     'gender:masc': {'person:1': {'': {},
                                                  'actualisation:defini': {},
                                                  'actualisation:indefini': {}},
                                     'person:2': {'': {},
                                                  'actualisation:defini': {},
                                                  'actualisation:indefini': {}},
                                     'person:3': {'': {'-il': 982,
                                                       '-on': 111,
                                                       '-t-il': 402,
                                                       '-t-on': 291,
                                                       "c'": 98,
                                                       'ce': 10,
                                                       'cela': 20,
                                                       'celui': 10,
                                                       'celui-ci': 12193,
                                                       'celui-là': 566,
                                                       'il': 16733,
                                                       'lequel': 5,
                                                       'lui': 855,
                                                       'lui-même': 28,
                                                       'on': 5541,
                                                       'personne': 8364,
                                                       'rien': 68,
                                                       'sien': 194,
                                                       'tout': 1212,
                                                       'un': 317,
                                                       'ça': 21},
                                                  'actualisation:defini': {'-il': 104,
                                                                           '-on': 1,
                                                                           '-t-il': 41,
                                                                           'ce': 158,
                                                                           'celui': 1,
                                                                           'il': 520,
                                                                           'on': 36,
                                                                           'personne': 23,
                                                                           'rien': 1,
                                                                           'sien': 4087,
                                                                           'tout': 24,
                                                                           'un': 5530,
                                                                           'ça': 4},
                                                  'actualisation:indefini': {'-il': 5,
                                                                             'sien': 17}}}}}

    pronouns_arg_x = {'': {'': {'person:3': {'': {'-moi': 8,
                                 'cinq': 10,
                                 'où': 8,
                                 "qu'": 31,
                                 'quatre': 33,
                                 'que': 219,
                                 'quoi': 4,
                                 'y': 3,
                                 'étienne': 2}}},
          'gender:masc': {'person:3': {'': {'autrui': 2,
                                            'ce': 46,
                                            'ceci': 36,
                                            'en': 8234,
                                            'qui': 484}}}},
     'number:plur': {'': {'person:1': {'': {'nous': 5901,
                                            'nous-mêmes': 2,
                                            'vous': 8},
                                       'actualisation:defini': {'nôtres': 9},
                                       'actualisation:indefini': {'nôtres': 1}},
                          'person:2': {'': {'les': 2, 'vous': 5184},
                                       'actualisation:defini': {'trois': 2},
                                       'actualisation:indefini': {'trois': 2,
                                                                  'vous': 2}},
                          'person:3': {'': {'autres': 12,
                                            'les': 12283,
                                            'six': 1,
                                            'vous': 3},
                                       'actualisation:defini': {'autres': 147,
                                                                'quatre': 1},
                                       'actualisation:indefini': {'autres': 17,
                                                                  'dix': 1,
                                                                  'douze': 1}}},
                     'gender:fem': {'person:3': {'': {'-elles': 1,
                                                      'celles': 80,
                                                      'elles': 27,
                                                      'elles-mêmes': 5,
                                                      'quelques-unes': 71},
                                                 'actualisation:defini': {'unes': 28}}},
                     'gender:masc': {'person:2': {'': {'deux': 10,
                                                       'quittes': 7,
                                                       'tous': 13}},
                                     'person:3': {'': {'-ils': 10,
                                                       'certains': 6,
                                                       'ceux': 60,
                                                       'deux': 938,
                                                       'eux': 75,
                                                       'eux-mêmes': 58,
                                                       'quels': 4,
                                                       'tous': 61},
                                                  'actualisation:defini': {'deux': 6,
                                                                           'uns': 71},
                                                  'actualisation:indefini': {'deux': 3}}}},
     'number:sing': {'': {'person:1': {'': {'je': 4,
                                            "m'": 8754,
                                            'me': 8506,
                                            'moi': 106,
                                            'moi-même': 62}},
                          'person:2': {'': {"t'": 152,
                                            'te': 1069,
                                            'toi': 3,
                                            'vous-même': 5}},
                          'person:3': {'': {"l'": 25138,
                                            'leur': 2,
                                            'se': 11,
                                            'soi': 1},
                                       'actualisation:defini': {'autre': 14},
                                       'actualisation:indefini': {'autre': 2}}},
                     'gender:fem': {'person:1': {'actualisation:defini': {'mienne': 11,
                                                                          'nôtre': 2}},
                                    'person:3': {'': {'aucune': 203,
                                                      'celle': 1,
                                                      'celle-ci': 63,
                                                      'chacune': 14,
                                                      'elle': 143,
                                                      'elle-même': 5,
                                                      'la': 8294,
                                                      'quelle': 14,
                                                      'une': 2},
                                                 'actualisation:defini': {'sienne': 32},
                                                 'actualisation:indefini': {'autre': 1,
                                                                            'sienne': 1}}},
                     'gender:masc': {'person:2': {'': {}},
                                     'person:3': {'': {'cela': 1,
                                                       'celui': 18,
                                                       'le': 18947,
                                                       'lui': 595,
                                                       'lui-même': 26,
                                                       'sien': 7,
                                                       'tout': 12,
                                                       'un': 621},
                                                  'actualisation:defini': {'sien': 5,
                                                                           'un': 513}}}}}
    num_lines_real = 0
    for in_, out_, info_, suivi_, count_sent in zip(source_file, pred_file, info_file, suivi_file, tqdm(range(num_lines_pred))):

        if out_ in sentences_treated:
            get_out("sentence already seen")
            continue

        sentences_treated.add(out_)
        if args.debug:
            value = random.randint(0, 99)
            if  value >= __PERCENT_DEBUG_MODE__:
                continue
            num_lines_debug += 1
            if isinstance(__SMALL__, int) and __SMALL__> 0:
                if num_lines_debug > __SMALL__:
                    break

        num_lines_real += 1
        in_ = in_.strip()
        out_ = out_.strip()
        info_ = info_.strip()
        suivi_ = suivi_.strip()
        source = { k.split("￨")[0] for k in  in_.split()  if "argevent_" in k or "event_" in k or "mod_" in k}
        full_in_items = { k.split("￨")[0]:k for k in  in_.split()  if "argevent_" in k or "event_" in k or "mod_" in k}

        prédiction = {k for k in re.split(r'(\W+)', out_) if "argevent_" in k or "event_" in k or "mod_" in k}
        # {k.split("￨")[0] for k in out_.split() if "_" in k.split("￨")[0]}
        verbe_support = None
        aux_tps_support = {}
        aux_tps_passif = {}
        event_1_allowed_time = {}
        verbes_gen = {}
        if source - args_data == prédiction:

            # vérification des traits d'input
            inputs_type = {}
            outputs_type = {}
            for word in in_.split():
                features = word.split("￨")
                inputs_type[features.pop(0)] = features[3]
                for i_f, feature in enumerate(in_features):
                    if features[i_f] not in feature:
                        print()
                        print(i_f)
                        print( features[i_f])
                        print(feature)
                        print(word)
                        pass
            take = True

            if False:
                outputs_type = {}
                for word in out_.split():
                    features = word.split("￨")
                    word_value = features.pop(0)
                    if word_value in inputs_type.keys():
                        if features[0] == inputs_type[word_value]:
                            a = 1
                        else:
                            take = False
                            break

                if not take:
                    get_out("unexpected item " + features[0]  + " for "  + inputs_type[word_value])
                    continue

            in_verbs = {verb for verb in in_.split() if "voice" in verb}
            out_verbs = {verb for verb in out_.split() if "infinitif" in verb and verb not in in_verbs}

            in_items = {item.split("￨")[0]+"￨"+item.split("￨")[3] for item in in_.split() if item.split("￨")[0] in prédiction and item not in out_verbs and "￨pron￨" not in item}
            in_pron_items = {item.split("￨")[0]:item.split("￨")[1:] for item in in_.split() if "￨pron￨" in item}
            out_items = {"￨".join(item.split("￨")[0:2]) for item in out_.split() if item.split("￨")[0] in prédiction and item not in out_verbs}

            if len(in_items-out_items) > 0:
                tmp_items = in_items.copy()
                in_items = in_items - out_items
                out_items = out_items - tmp_items
                if len(in_items) != len(out_items):
                    get_out("UPOS expected items")
                    continue
                tmp_items = {}
                for item in in_items:
                    tmp_items[item] = ''
                    for item_out in out_items:
                        if item_out.startswith(item):
                            tmp_items[item] = item_out
                            break
                    if tmp_items[item] != '':
                        out_items.remove(tmp_items[item])
                if len(out_items) > 0:
                    take = False
                    get_out("UPOS expected items")
                    continue

            used_verbs = {}
            verbs_infos = "\nasked "
            sep = ""
            special = False
            modes_ = {}
            temps_ = {}
            for in_verb in in_verbs:
                in_verb = in_verb.split("￨")
                used_verbs[in_verb[0]] = {}
                used_verbs[in_verb[0]]['elision'] = in_verb.pop(1)
                used_verbs[in_verb[0]]['polarity'] = in_verb.pop(1)
                used_verbs[in_verb[0]]['temps'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['mode'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['aux'] = in_verb.pop(9)
                used_verbs[in_verb[0]]['voice'] = in_verb.pop(9)
                verbs_infos += sep + in_verb[0] + ": " + used_verbs[in_verb[0]]['elision'] + ", " + \
                                used_verbs[in_verb[0]]['polarity'] + ", " + \
                                used_verbs[in_verb[0]]['temps'] + ", " + \
                                used_verbs[in_verb[0]]['mode'] + ", " + \
                                used_verbs[in_verb[0]]['voice']
                used_verbs[in_verb[0]]['type_verb'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['sujet'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['objet'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['préposition'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['complément'] = in_verb.pop(4)
                for key_lvf in ("sujet", "objet", "préposition", "complément"):
                    if used_verbs[in_verb[0]][key_lvf] in ("0", "na", "z"):
                        used_verbs[in_verb[0]][key_lvf] = ""

                if in_verb[0].startswith("event_") or trait_word._value in used_verbs.keys():
                    modes_[in_verb[0]] = used_verbs[in_verb[0]]['mode']
                    temps_[in_verb[0]] = used_verbs[in_verb[0]]['temps']
                    if modes_[in_verb[0]] not in verbes:
                        verbes[modes_[in_verb[0]]] = {}
                    if temps_[in_verb[0]] not in verbes[modes_[in_verb[0]]].keys():
                        verbes[modes_[in_verb[0]]][temps_[in_verb[0]]] = {'not kept':0, 'kept':0, 'kept values':{}}
                    if in_verb[0] == 'event_1':
                        if modes_[in_verb[0]] not in event_1_allowed_times.keys():
                            take = False
                            get_out("mode not authorized for the base verb ("+ modes_[in_verb[0]] + ")")
                            break
                        if temps_[in_verb[0]] not in event_1_allowed_times[modes_[in_verb[0]]].keys():
                            take = False
                            get_out("time not authorized for the base verb ("+ temps_[in_verb[0]] + ")")
                            break
                        event_1_allowed_time[in_verb[0]] = event_1_allowed_times[modes_[in_verb[0]]][temps_[in_verb[0]]]
                    else:
                        event_1_allowed_time[in_verb[0]] = event_1_allowed_times_sub[modes_[in_verb[0]]][temps_[in_verb[0]]]
                    verbe_support = verbes[modes_[in_verb[0]]][temps_[in_verb[0]]]

                sep = ", "
            for out_verb_ in out_verbs:
                if not take:
                    break
                out_verb = Trait(out_verb_)

                if out_verb._nature == "bruit":
                    take = False
                    break
                if "aux" in out_verb._nature:
                    continue
                if len([verb for verb in in_.split() if verb.startswith(out_verb._value)]) == 0:
                    take = False # noise without flag
                    get_out("noise detected")
                    break

                in_verb = [verb for verb in in_.split() if verb.startswith(out_verb._value)][0]
                in_verb = in_verb.split("￨")

                used_verbs[in_verb[0]] = {}
                used_verbs[in_verb[0]]['elision'] = in_verb.pop(1)
                used_verbs[in_verb[0]]['polarity'] = in_verb.pop(1)
                used_verbs[in_verb[0]]['temps'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['mode'] = in_verb.pop(4)
                used_verbs[in_verb[0]]['aux'] = in_verb.pop(9)
                used_verbs[in_verb[0]]['voice'] = in_verb.pop(9)
                verbs_infos += sep + in_verb[0] + ": " + used_verbs[in_verb[0]]['elision'] + ", " + \
                                used_verbs[in_verb[0]]['polarity'] + ", " + \
                                used_verbs[in_verb[0]]['temps'] + ", " + \
                                used_verbs[in_verb[0]]['mode'] + ", " + \
                                used_verbs[in_verb[0]]['voice']

                sep = ", "

                if in_verb[0].startswith("event_") or trait_word._value in used_verbs.keys():
                    try:
                        if used_verbs[in_verb[0]]['mode'] not in verbes.keys():
                            verbes[used_verbs[in_verb[0]]['mode']] = {}
                        if used_verbs[in_verb[0]]['temps'] not in verbes[used_verbs[in_verb[0]]['mode']].keys():
                            # update 201908232250
                            verbes[used_verbs[in_verb[0]]['mode']][used_verbs[in_verb[0]]['temps']] = {'not kept':0, 'kept':0, 'kept values':{}}
                        modes_[in_verb[0]] = used_verbs[in_verb[0]]['mode']
                        temps_[in_verb[0]] = used_verbs[in_verb[0]]['temps']
                        if modes_[in_verb[0]] not in verbes:
                            verbes[modes_[in_verb[0]]] = {}
                        if temps_[in_verb[0]] not in verbes[modes_[in_verb[0]]].keys():
                            verbes[modes_[in_verb[0]]][temps_[in_verb[0]]] = {'not kept':0, 'kept':0, 'kept values':{}}
                        if in_verb[0] == 'event_1':
                            event_1_allowed_time[in_verb[0]] = event_1_allowed_times[modes_[in_verb[0]]][temps_[in_verb[0]]]
                        else:
                            event_1_allowed_time[in_verb[0]] = event_1_allowed_times_sub[modes_[in_verb[0]]][temps_[in_verb[0]]]
                        if modes_[in_verb[0]] not in event_1_allowed_times_sub.keys():
                            event_1_allowed_times_sub[modes_[in_verb[0]]] = {}
                        if temps_[in_verb[0]] not in event_1_allowed_times_sub[modes_[in_verb[0]]].keys() :
                            event_1_allowed_times_sub[modes_[in_verb[0]]][temps_[in_verb[0]]]= {modes_[in_verb[0]]:temps_[in_verb[0]]}
                            event_1_allowed_time[in_verb[0]] = event_1_allowed_times_sub[modes_[in_verb[0]]][temps_[in_verb[0]]]

                    except:
                        take = False
                    break
            if not take:
                continue


            if out_[-1] == "!" and "exclamation" not in in_:
                get_out("unexpected exclamation")
                continue
            if out_[-1] == "?" and "question" not in in_:
                get_out("unexpected question")
                continue
            if out_[-1] == "." and ("question" in in_ or "exclamation" not in in_) :
                get_out("unexpected affirmation")
                continue

            if not take:
                get_out()
                continue

            name = count_sent.__str__().zfill(7)
            labeldict = {}
            types_word = {} # label dic + simple
            words = out_.split()
            nodes = {}
            tab_nodes = {}
            old_data = ""
            for node_id, word in enumerate(words):

                if (word.startswith("personne") or "nul" in word.lower() or "aucun" in word.lower()) and not "|person:0|" in word:
                    get_out("bruit")
                    take = False
                    break

                if False:
                    if "￨punct￨" in word and node_id< len(words)-1:
                        get_out("bruit")
                        take = False
                        break

                trait_word = Trait(word)


                if node_id > 0:
                    if old_data in ("je", "me", "le", "te", "que", "ce"):
                        if trait_word._value[0] in ("aeiou") and not "event" in trait_word._value:
                            get_out("souci basique élision")
                            take = False
                            break

                    old_data = trait_word._value

                if trait_word.value.startswith('event_') or trait_word.value in used_verbs.keys():
                    if trait_word.value not in used_verbs.keys():
                        take = False
                        get_out("event problem, incomplete modifier")
                        break
                    mode_initial = used_verbs[trait_word.value]['mode']
                    temps_initial = used_verbs[trait_word.value]['temps']
                    if mode_initial not in verbes_gen.keys():
                        verbes_gen[mode_initial] = {}
                    if temps_initial not in verbes_gen[mode_initial].keys():
                        verbes_gen[mode_initial][temps_initial] = {}
                    verbe_gen = verbes_gen[mode_initial][temps_initial]
                    if trait_word.mode not in verbe_gen.keys():
                        verbe_gen[trait_word.mode] = {}
                    if trait_word._temps not in verbe_gen[trait_word._mode].keys():
                        verbe_gen[trait_word._mode][trait_word._temps] = 0
                    verif_time = event_1_allowed_times[mode_initial][temps_initial] if trait_word.value == 'event_1' else event_1_allowed_times_sub[mode_initial][temps_initial]
                    if trait_word._mode in verif_time.keys():
                        if trait_word._temps in verif_time[trait_word._mode]:
                            verbe_gen[trait_word._mode][trait_word._temps] += 1
                        elif used_verbs[trait_word.value]['voice'] == 'voice:pass':
                            if not (trait_word._mode == "participe" and trait_word._temps == "passé"):
                                take = False
                                get_out("passive voice asked but no past participle")
                                break
                        else:
                            take = False
                            get_out("misfit time")
                            break
                    if trait_word._value.startswith('event_') or trait_word._value in used_verbs.keys():
                        if used_verbs[trait_word._value]['voice'] == 'voice:pass':
                            if trait_word._mode != 'participe':
                                take = False
                                get_out("unauthorized mode for the ask (passive) ")
                                break
                            if trait_word._temps  != 'passé':
                                take = False
                                get_out("unauthorized couple mode/time for the ask (passive)")
                                break
                            aux_tps_support[trait_word._value] = (modes_[trait_word._value], temps_[trait_word._value], used_verbs[trait_word.value]['voice'] )
                        elif trait_word._value == 'event_1':
                            if trait_word._mode not in verif_time.keys():
                                take = False
                                get_out("unauthorized mode for the ask")
                                break
                            if trait_word._temps not in verif_time[trait_word._mode]:
                                take = False
                                get_out("unauthorized couple mode/time for the ask")
                                break
                            if trait_word.mode != used_verbs[trait_word._value]['mode'] or trait_word.temps != used_verbs[trait_word._value]['temps']:
                                aux_tps_support[trait_word._value] = (modes_[trait_word._value], temps_[trait_word._value], used_verbs[trait_word.value]['voice'] )
                        else:
                            if trait_word._mode not in verif_time.keys():
                                take = False
                                get_out("unauthorized mode for the ask")
                                break
                            if trait_word._temps not in verif_time[trait_word._mode]:
                                take = False
                                get_out("unauthorized couple mode/time for the ask")
                                break
                            if trait_word.mode != used_verbs[trait_word._value]['mode'] or trait_word.temps != used_verbs[trait_word._value]['temps']:
                                aux_tps_support[trait_word._value] = (modes_[trait_word._value], temps_[trait_word._value], used_verbs[trait_word.value]['voice'] )

                nodes[(node_id + 1).__str__()] = trait_word
                tab_nodes[trait_word.value] = (node_id + 1).__str__()
                if trait_word.nature == "bruit":
                    take = False
                    get_out("noise")
                    break

            values = set()
            for trait_word in nodes.values():
                if trait_word.value in prédiction:
                    if trait_word.value not in values:
                        values.add(trait_word.value)
                    else:
                        take = False
                        get_out("unexpected duplicates")
                        break

            if not take:
                continue

            sentence = []
            event_1_node_id = None
            for node_id in range(len(words)):
                node_id = (node_id + 1).__str__()

                trait_word = nodes[node_id]
                word = trait_word._value
                if trait_word._value == "event_1":
                    event_1_node_id = node_id
                sentence.append(word)

            G = nx.MultiDiGraph(data=None, label="sent : " + name + "\n" + " ".join(sentence) + verbs_infos)
            aux_data_information = {}
            clr_ref = []
            clr_data = {}
            for node_id in range(len(words)-1):
                node_id = (node_id + 1).__str__()
                avoir = False
                être = False
                trait_word = nodes[node_id]
                word = trait_word._value
                if trait_word.universal_pos == "det":
                    if not trait_word._dep.isdigit() or  not trait_word._dep in nodes.keys():
                        get_out("det without dependencies")
                        take = False
                        break
                    dependency = nodes[trait_word._dep]
                    if trait_word.value[0].lower() == nodes[trait_word._dep].value[0].lower():
                        get_out("det in not allowed relation (ie sa sienne)")
                        take = False
                        break

                labeldict[node_id] = trait_word._value + "\n" + trait_word._desc
                if node_id not in types_word.keys():
                    types_word[node_id] = {"value": trait_word._value , 'desc':trait_word._desc}
                else:
                    types_word[node_id]["value"] = trait_word._value
                    types_word[node_id]['desc'] = trait_word._desc
                if trait_word._dep.isdigit():
                    if trait_word._dep not in nodes.keys():
                        take = False
                        get_out("dep outside of the scope")
                        break
                    if not G.has_edge(node_id, trait_word._dep):
                        if not G.has_edge(trait_word._dep, node_id):
                            G.add_edge(node_id, trait_word._dep, color='red')
                    if trait_word.value == "event_1": # c'est le support, pas censé avoir de dépendances
                        trait_word._dep = ""
                    if trait_word.universal_pos == "aux":
                        nodes[trait_word._dep].add_aux(trait_word)

                if trait_word.nature == "CLR":
                    clr_data[node_id] = None
                    clr_ref.append(node_id)
                if trait_word._value.startswith("event_") or trait_word._value in used_verbs.keys():
                    if len(clr_ref) > 0:
                        if used_verbs[trait_word._value]['type_verb'] == 'p':
                            if clr_data[clr_ref[-1]] == None:
                                clr_data[clr_ref[-1]] = node_id
                                valeur_clr = nodes[clr_ref[-1]].value
                                if trait_word._person == 'person:3':
                                    if trait_word._number == 'number:sing':
                                        if valeur_clr[0] in ("s", ):
                                            pass
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    elif trait_word._number == 'number:plur':
                                        if valeur_clr[0] in ("s", ):
                                            pass
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    else:
                                        take = False
                                        get_out("no CLR adapted for pron verb")
                                        break
                                elif trait_word._person == 'person:2':
                                    if trait_word._number == 'number:sing':
                                        if valeur_clr[0] in ("t", ):
                                            pass
                                        elif valeur_clr[0] in "vnsm":
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    elif trait_word._number == 'number:plur':
                                        if valeur_clr[0] in ("v", ):
                                            pass
                                        elif valeur_clr[0] in "tsm":
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    else:
                                        raise Exception
                                        take=False
                                        break
                                elif trait_word._person == 'person:1':
                                    if trait_word._number == 'number:sing':
                                        if valeur_clr[0] in ("m", ):
                                            pass
                                        elif valeur_clr[0] in "tsn":
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    elif trait_word._number == 'number:plur':
                                        if valeur_clr[0] in ("n", ):
                                            pass
                                        elif valeur_clr[0] in "stm":
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                        else:
                                            take = False
                                            get_out("no CLR adapted for pron verb")
                                            break
                                    else:
                                        raise Exception
                                        take=False
                                        break
                                else:
                                    take = False
                                    get_out("no CLR adapted for pron verb (uncertain input, person unaffacted)")
                                    break
                                    #if not valeur_clr[0] == "m":
                                G.add_edge(clr_ref[-1], node_id, color='green', label="refl")
                            else:
                                take = False
                                get_out("no CLR from pron verb")
                                break
                    elif used_verbs[trait_word._value]['type_verb'] == 'p':
                        take = False
                       # print(out_)
                        get_out("no CLR from pron verb")
                        break


                if trait_word._value in ("étienne", "élisabeth"):
                    take = False
                    get_out("bruit")
                    break
                if trait_word._value == "event_1":
                    G.add_edge((len(words)).__str__(), node_id, color='red')
                    trait_final = Trait(words[-1])
                    labeldict[(len(words)).__str__()] = trait_final._value +"\n"+ trait_final._desc
                    nodes[(len(words)).__str__()].dep = node_id

                if trait_word._value in used_verbs.keys():

                    splitted_input = []
                    for i_, sub_i in enumerate(in_.strip().split(" ")):
                        if i_ % 5 == 0:
                            splitted_input.append([])
                        splitted_input[-1].append(sub_i)
                    triples = {}
                    for i_sub in range(len(splitted_input)):
                        splitted_input[i_sub].pop(0)
                        splitted_input[i_sub].pop(-1)
                        sujet = splitted_input[i_sub][0].split("￨")[0]
                        prédicat = splitted_input[i_sub][1].split("￨")[0]
                        objet = splitted_input[i_sub][2].split("￨")[0]
                        if prédicat.startswith("arg_"):
                            if sujet not in triples.keys():
                                triples[sujet] = {}
                            if prédicat not in triples.keys():
                                triples[sujet][prédicat] = objet

                    if used_verbs[trait_word._value]["mode"].startswith("subj"):
                        special = True

                    if used_verbs[trait_word._value]["voice"] == "voice:pass":
                        if len(trait_word._aux) == 0:
                            take = False
                            get_out("no auxiliary for a passive voice")
                            break
                        else:
                            if len(trait_word._aux) > 1:
                                print()
                                print(out_)
                                print(trait_word._aux)
                                print(used_verbs[trait_word._value])
                                print()
                            take  = False
                            for aux in trait_word._aux: # nous sommes sur des phrases simples, auxiliaire avant verbe
                                aux_ = conjugate(aux._value, INFINITIVE)
                                if aux._nature != 'aux.pass':
                                    break
                                elif aux_ == "être":
                                    take = True
                                    être = True
                                elif aux_ == "avoir":
                                    avoir = True
                                else:
                                    take = False
                                    break
                            if not take:
                                get_out("not the expected auxiliary for the passive voice")
                                break
                    else:
                        for aux in trait_word._aux:  # nous sommes sur des phrases simples, auxiliaire avant verbe
                            aux_ = conjugate(aux._value, INFINITIVE)
                            if aux._nature != 'aux.tps':
                                take = False
                                break
                            if aux_ == "être":
                                être = True
                            elif aux_ == "avoir":
                                avoir = True
                    for aux  in trait_word._aux:
                        if aux._mode == "":
                            if aux._value in ("fus", "eus"):
                                aux._mode = "indicatif"
                                aux._number = 'number:sing'
                                aux._temps = 'passé-simple'
                            elif aux._value in ("étais", "avais"):
                                aux._mode = "indicatif"
                                aux._number = 'number:sing'
                                aux._temps = 'imparfait'
                            elif aux._value in ("auriez", "seriez"):
                                aux._mode = "conditionnel"
                                aux._number = 'number:plur'
                                aux._person = 'person:2'
                                aux._temps = 'présent'
                            elif aux._value in ("aurez", "serez"):
                                aux._mode = "indicatif"
                                aux._number = 'number:plur'
                                aux._person = 'person:2'
                                aux._temps = 'futur'
                            elif aux._value in ("aurons", "serons"):
                                aux._mode = "indicatif"
                                aux._number = 'number:plur'
                                aux._person = 'person:1'
                                aux._temps = 'futur'
                            elif aux._value in ("aurai", "serai"):
                                aux._mode = "indicatif"
                                aux._number = 'number:sing'
                                aux._person = 'person:1'
                                aux._temps = 'futur'
                            elif aux._value in ("aurions", "serions"):
                                aux._mode = "conditionnel"
                                aux._number = 'number:plur'
                                aux._person = 'person:1'
                                aux._temps = 'présent'
                            elif aux._value in ("ayez", "seriez"):
                                aux._mode = "subjonctif"
                                aux._number = 'number:plur'
                                aux._person = 'person:2'
                                aux._temps = 'présent'
                            elif aux._value in ("ayons", "serions"):
                                aux._mode = "subjonctif"
                                aux._number = 'number:plur'
                                aux._person = 'person:1'
                                aux._temps = 'présent'
                            elif aux._value in ("serais", "aurais"):
                                aux._mode = "conditionnel"
                                aux._number = 'number:sing'
                                aux._temps = 'présent'
                            elif aux._value in ("soit", "ait"):
                                aux._mode = "subjonctif"
                                aux._number = 'number:sing'
                                aux._person = 'person:3'
                                aux._temps = 'présent'
                            elif aux._value in ("aie","aies", "sois"):
                                aux._mode = "subjonctif"
                                aux._number = 'number:sing'
                                if aux._value == "aie":
                                    aux._person = 'person:1'
                                elif aux._value == "aies":
                                    aux._person = 'person:2'
                                else :
                                    aux._person = ""
                                aux._temps = 'présent'
                            elif aux._value in ("faire", "faire"):
                                aux._mode = "infinitif"
                                aux._temps = ''

                        verbe_affecté = conjugate(aux._value, INFINITIVE)
                        if  aux._mode == "indicatif" :
                            mode = INDICATIVE
                        elif aux._mode == "impératif" :
                            mode = IMPERATIVE
                        elif aux._mode == "conditionnel" :
                            mode = CONDITIONAL
                        elif aux._mode == "subjonctif" :
                            mode = SUBJUNCTIVE
                        elif aux.mode == "infinitif" :
                            mode = INFINITIVE
                        elif aux.mode == "infinitif" :
                            mode = INFINITIVE
                        elif aux.mode == "participe" :
                            mode = PARTICIPLE
                        else:
                            print()
                            print(verbe_affecté)
                            print(aux.value)
                            print(aux._mode)
                            raise Exception
                        try:
                            sujet_aux = nodes[trait_word._arg_0] if used_verbs[trait_word.value]['voice'] == "voice:act" else nodes[trait_word._arg_1]
                        except Exception as ex:
                            take = False
                            get_out("no voice subject")
                            break
                        person_aux = sujet_aux._person
                        if aux._person == "":
                            aux._person = person_aux
                        elif aux._person != person_aux:
                            if person_aux == "" and sujet_aux.value[0].lower() not in "jtvn" and 'person:3' == aux._person:
                                sujet_aux._person = aux._person
                            elif sujet_aux.value[0].lower() in "jtvn" and 'person:3' == aux._person:
                                get_out("bad subject")
                                take = False
                                break
                            elif sujet_aux.value[0].lower() in "tv" and 'person:2' == aux._person:
                                sujet_aux._person = aux._person
                            elif sujet_aux.value[0].lower() in "jn" and 'person:1' == aux._person:
                                sujet_aux._person = aux._person
                            else:
                                get_out("bad subject")
                                take = False
                                break
                        number_aux = sujet_aux._number
                        if aux._number == "":
                            aux._number = number_aux
                        elif aux._number != number_aux:
                            if number_aux == "" and sujet_aux.value[0].lower() in "jt" and 'number:sing' == aux._number:
                                sujet_aux._person = aux._person
                            elif number_aux == "" and sujet_aux.value[0].lower() in "nv" and 'number:plur' == aux._number:
                                sujet_aux._person = aux._person
                            else:
                                get_out("bad subject")
                                take = False
                                break
                        if number_aux == "":
                            sujet_aux._number = aux._number
                            number_aux = aux._number
                        if person_aux == "":
                            sujet_aux._person = aux._person
                            person_aux = aux._person
                        if person_aux == "":
                            get_out("bad subject")
                            take = False
                            break
                        if number_aux == "":
                            get_out("bad subject")
                            take = False
                            break

                        person_aux = 3 if "3" in person_aux else 2 if "2" in person_aux else 1
                        number_aux = SG if "sing" in number_aux else PL

                        if not verbe_affecté == aux._value and mode != PARTICIPLE:
                            verbes_possibles =[]

                            for tense_possible in ( PRESENT, PAST, FUTURE):
                                if aux._temps in ('passé-simple',"imparfait"):
                                    if tense_possible in (PRESENT, FUTURE):
                                        continue
                                elif aux._temps in ('présent', ):
                                    if tense_possible in (PAST, FUTURE):
                                        continue
                                elif aux._temps in ('futur', ):
                                    if tense_possible in (PAST, PRESENT):
                                        continue
                                else:
                                    print()
                                    print("##############")
                                    print(mode )
                                    print(aux._mode )
                                    print(aux._temps )
                                    print(aux._value )
                                    raise Exception
                                for aspect_possible in (IMPERFECTIVE, PROGRESSIVE, PERFECTIVE):
                                    verbe_possible = conjugate(verbe_affecté, tense=tense_possible, person=person_aux, number=number_aux, mood=mode, aspect=aspect_possible)
                                    if verbe_possible is not None:
                                        if isinstance(verbe_possible, str):
                                            verbes_possibles.append(verbe_possible)
                                        else:
                                            verbes_possibles.append += verbe_possible
                            if not aux._value in verbes_possibles:
                                get_out("bad auxiliary")
                                take = False
                                break

                    if trait_word.value.startswith("event") or trait_word._value in used_verbs.keys():
                        used_verbs[trait_word.value]['traits'] = trait_word.getTrait()
                        used_verbs[trait_word.value]['types_args'] = []
                        for arg in range(4):
                            if eval("trait_word._type_arg_" + arg.__str__()) != "":
                                used_verbs[trait_word.value]['types_args'].append(eval("trait_word._arg_"  + arg.__str__()) + "-" + eval("trait_word._type_arg_" + arg.__str__()))

                    if trait_word._arg_0.isdigit():
                        G.add_edge(node_id, trait_word._arg_0, color='blue', label="arg 0")
                        if trait_word._arg_0 not in nodes.keys() or ("argem" in nodes[trait_word._arg_0]._value and nodes[trait_word._arg_0]._universal_pos == "pron"):
                            take = False
                            if trait_word._arg_0 not in nodes.keys():
                                get_out("arg 0 out of the scope " + used_verbs[trait_word._value]["voice"].__str__())
                            else:
                                get_out("arg 0 isn't an argument " + used_verbs[trait_word._value]["voice"].__str__())
                            break

                        if used_verbs[trait_word._value]["voice"] == "voice:pass": #si je suis au passif
                            if nodes[trait_word._arg_0]._person == 'person:3':
                                if nodes[trait_word._arg_0]._universal_pos == "pron":
                                    if nodes[trait_word._arg_0].value not in ("le", "l'", "lui", "se", "s'", "la", "l'", "se", "les"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break

                                if nodes[trait_word._arg_0]._number == 'number:plur':
                                    if nodes[trait_word._arg_0].value not in ("se", "s'", "les"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                if nodes[trait_word._arg_0]._number == 'number:sing':
                                    if nodes[trait_word._arg_0].value in ("les",):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                if nodes[trait_word._arg_0]._gender == 'gender:fem':
                                    if nodes[trait_word._arg_0].value not in ("la", "l'", "se", "s'", "les"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                elif nodes[trait_word._arg_0]._gender == 'gender:masc':
                                    if nodes[trait_word._arg_0].value not in ("le", "l'", "se", "s'", "les"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break

                            if nodes[trait_word._arg_0]._person == 'person:2':
                                if nodes[trait_word._arg_0]._universal_pos == "pron":
                                    if nodes[trait_word._arg_0].value not in ("te", "t'", "vous"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                if nodes[trait_word._arg_0]._number == 'number:plur':
                                    if nodes[trait_word._arg_0].value not in ("te", "t'"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                            if nodes[trait_word._arg_0]._person == 'person:1':
                                if nodes[trait_word._arg_0]._universal_pos == "pron":
                                    if nodes[trait_word._arg_0].value not in ("me", "m'", "nous"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                if nodes[trait_word._arg_0]._number == 'number:plur':
                                    if nodes[trait_word._arg_0].value not in ("nous",):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break
                                    elif nodes[trait_word._arg_0].value not in ("me", "m'"):
                                        get_out("arg 0 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                        take = False
                                        break

                        if ("_" in nodes[trait_word._arg_0]._value and  nodes[trait_word._arg_0]._universal_pos == "pron"  ) :
                            get_out("arg 0 shouldn't be a pronoun with this name " + used_verbs[trait_word._value]["voice"].__str__())
                            take = False
                            break
                        else:
                            if not trait_word._value in triples.keys() or not trait_word._arg_0 in nodes.keys():
                                get_out("arg 0 not detected " + used_verbs[trait_word._value]["voice"].__str__())
                                take = False
                                break

                            if 'arg_0' not in triples[trait_word._value].keys():
                                get_out("arg 0 should not be")
                                take = False
                                break

                            if 'arg_0' not in triples[trait_word._value].keys() or not triples[trait_word._value]['arg_0'] in in_pron_items.keys() and nodes[trait_word._arg_0]._universal_pos == "pron" :
                                get_out("arg 0 should be a pronoun " + used_verbs[trait_word._value]["voice"].__str__())
                                take = False
                                break

                            if triples[trait_word._value]['arg_0'] in in_pron_items.keys() and nodes[trait_word._arg_0]._universal_pos == "pron" :
                                if in_pron_items[triples[trait_word._value]['arg_0'] ][14] != nodes[trait_word._arg_0]._gender:
                                    val = {in_pron_items[triples[trait_word._value]['arg_0'] ][14], nodes[trait_word._arg_0]._gender}
                                    if not val - {'', 'gender:masc'} == set():
                                        get_out("arg 0 should be a " + nodes[trait_word._arg_0]._gender )
                                        take = False
                                        break
                                if in_pron_items[triples[trait_word._value]['arg_0'] ][15] != nodes[trait_word._arg_0]._number:
                                    val = {in_pron_items[triples[trait_word._value]['arg_0'] ][15], nodes[trait_word._arg_0]._number}
                                    if not val - {'', 'number:sing'} == set():
                                        get_out("arg 0 should be a " + nodes[trait_word._arg_0]._number )
                                        take = False
                                        break
                                if True:
                                    number =  nodes[trait_word._arg_0]._number
                                    gender =  nodes[trait_word._arg_0]._gender
                                    person = in_pron_items[triples[trait_word._value]['arg_0'] ][16]
                                    actualisation = in_pron_items[triples[trait_word._value]['arg_0'] ][17]
                                    value = nodes[trait_word._arg_0].value
                                    if value in ("étienne", ):
                                        take = False
                                        break
                                    if args.debug:
                                        if number not in pronouns_arg_0.keys():
                                            pronouns_arg_0[number] = {}
                                        if gender not in pronouns_arg_0[number].keys():
                                            pronouns_arg_0[number][gender] = {}
                                        if person not in pronouns_arg_0[number][gender].keys():
                                            pronouns_arg_0[number][gender][person] = {}
                                        if actualisation not in pronouns_arg_0[number][gender][person].keys():
                                            pronouns_arg_0[number][gender][person][actualisation] = {}
                                        if value not in pronouns_arg_0[number][gender][person][actualisation].keys():
                                            pronouns_arg_0[number][gender][person][actualisation][value] = 0
                                        pronouns_arg_0[number][gender][person][actualisation][value] += 1
                                    else:
                                        if number not in pronouns_arg_0.keys():
                                            take = False
                                        elif gender not in pronouns_arg_0[number].keys():
                                            take = False
                                        elif person not in pronouns_arg_0[number][gender].keys():
                                            take = False
                                        elif actualisation not in pronouns_arg_0[number][gender][person].keys():
                                            take = False
                                        elif value not in pronouns_arg_0[number][gender][person][actualisation].keys():
                                            take = False
                                        if not take:
                                            get_out("pronoun not accepted arg 0" )
                                            take = False
                                            break

                        if trait_word._arg_0 in (trait_word._arg_1, trait_word._arg_2, trait_word._arg_3):
                            take = False
                            get_out("arg 0 used for others arguments (same event) " + used_verbs[trait_word._value]["voice"].__str__())
                            break
                        if "arg_0" not in triples[trait_word._value].keys():
                            take = False
                            get_out("arg 0 wasn't asked. " + used_verbs[trait_word._value]["voice"].__str__())
                            break
                        if triples[trait_word._value]["arg_0"] != nodes[trait_word._arg_0]._value:
                            if triples[trait_word._value]["arg_0"].startswith("argem_pronoun_"):
                                if nodes[trait_word._arg_0].universal_pos != "pron":
                                    take = False
                                else:
                                    take = verif(triples[trait_word._value]["arg_0"], nodes[trait_word._arg_0])
                            else:
                                take = False
                            if not take:
                                get_out("arg 0 isn't the expected argument " + used_verbs[trait_word._value]["voice"].__str__())
                                break
                        nodes[trait_word._arg_0].add_event(node_id, "arg 0", trait_word._type_arg_0)
                    elif trait_word._value in triples.keys() and 'arg_0' in triples[trait_word._value].keys():
                        take = False
                        get_out("arg 0 was ask but no found " + used_verbs[trait_word._value]["voice"].__str__())
                        break
                    if trait_word._arg_1.isdigit() : # J'AI UN ARGUMENT 1
                        if used_verbs[trait_word._value]["voice"] == "voice:pass": #si je suis au passif
                            aux_data_information[trait_word._value] = {"node_id":node_id, "accord_sujet":trait_word._arg_1, "accord_participe":trait_word._arg_1, 'accord':True, "aux":(avoir, être)}
                            if trait_word._arg_1 in nodes.keys():
                                if nodes[trait_word._arg_1].value in ("nada", ): #pas d'idée pour l'instant :D... lui est aimé....
                                    get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                    take = False
                                    break
                        else:
                            if int(trait_word._arg_1) < int(node_id): # sir arg_1 avant le verbe
                                if trait_word._temps == "passé" and   trait_word._mode == "participe":
                                    aux_data_information[trait_word._value] = {"node_id":node_id, "accord_sujet":trait_word._arg_0, "accord_participe":trait_word._arg_1, 'accord':avoir, "aux":(avoir, être)}
                            else:
                                aux_data_information[trait_word._value] = {"node_id": node_id, "accord_sujet":trait_word._arg_0,  "accord_participe": "masc_sing", 'accord': avoir, "aux":(avoir, être)}

                            if trait_word._arg_1 in nodes.keys() :
                                if nodes[trait_word._arg_1].value.lower() in ("elles", "elle", "il", 'ils',"on", "je", "tu", "j'"):
                                    get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                    take = False
                                    break
                                if nodes[trait_word._arg_1]._person == 'person:3' and  nodes[trait_word._arg_1].value != "en":
                                    if nodes[trait_word._arg_1]._universal_pos == "pron":
                                        if nodes[trait_word._arg_1].value not in ("le", "l'", "lui", "se", "s'", "la", "l'", "se", "les", "uns"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break

                                    if nodes[trait_word._arg_1]._number == 'number:plur':
                                        if nodes[trait_word._arg_1].value not in ( "se", "s'", "les", "uns"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                    if nodes[trait_word._arg_1]._number == 'number:sing':
                                        if nodes[trait_word._arg_1].value in ("les", ):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                    if nodes[trait_word._arg_1]._gender == 'gender:fem':
                                        if nodes[trait_word._arg_1].value not in ("la", "l'", "se", "s'", "les"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                    elif nodes[trait_word._arg_1]._gender == 'gender:masc':
                                        if nodes[trait_word._arg_1].value not in ("le", "l'", "se", "s'", "les", "uns"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break

                                if nodes[trait_word._arg_1]._person == 'person:2':
                                    if nodes[trait_word._arg_1]._universal_pos == "pron":
                                        if nodes[trait_word._arg_1].value not in ("te", "t'", "vous"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                    if nodes[trait_word._arg_1]._number == 'number:plur':
                                        if nodes[trait_word._arg_1].value not in ("te", "t'"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                if nodes[trait_word._arg_1]._person == 'person:1':
                                    if nodes[trait_word._arg_1]._universal_pos == "pron":
                                        if nodes[trait_word._arg_1].value not in ("me", "m'", "nous"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                    if nodes[trait_word._arg_1]._number == 'number:plur':
                                        if nodes[trait_word._arg_1].value not in ("nous", ):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break
                                        elif  nodes[trait_word._arg_1].value not in ("me", "m'"):
                                            get_out("arg 1 has a form not available " + used_verbs[trait_word._value]["voice"].__str__())
                                            take = False
                                            break



                        G.add_edge(node_id, trait_word._arg_1, color='blue', label="arg 1")
                        if trait_word._arg_1 not in nodes.keys() or ("argem" in nodes[trait_word._arg_1]._value and nodes[trait_word._arg_1]._universal_pos == "pron"):
                            take = False
                            if trait_word._arg_1 not in nodes.keys():
                                get_out("arg 1 out of the scope " + used_verbs[trait_word._value]["voice"].__str__())
                            else:
                                get_out("arg 1 isn't an argument " + used_verbs[trait_word._value]["voice"].__str__())
                            break
                        if ("_" in nodes[trait_word._arg_1]._value and  nodes[trait_word._arg_1]._universal_pos == "pron"  ) :
                            get_out("arg_1 shouldn't be a pronoun with this name " + used_verbs[trait_word._value]["voice"].__str__())
                            take = False
                            break
                        else:
                            if trait_word._value not in triples.keys():
                                take = False
                                get_out("incomplete triples. ")
                                break
                            elif "arg_1" not in triples[trait_word._value].keys():
                                take = False
                                get_out("arg 1 wasn't asked. " + used_verbs[trait_word._value]["voice"].__str__())
                                break
                            if not triples[trait_word._value]['arg_1'] in in_pron_items.keys() and nodes[trait_word._arg_1]._universal_pos == "pron" :
                                get_out("arg_1 should be a pronoun " + used_verbs[trait_word._value]["voice"].__str__())
                                take = False
                                break

                            if triples[trait_word._value]['arg_1'] in in_pron_items.keys() and nodes[trait_word._arg_1]._universal_pos == "pron" :
                                if in_pron_items[triples[trait_word._value]['arg_1'] ][14] != nodes[trait_word._arg_1]._gender:
                                    get_out("arg_1 should be a " + nodes[trait_word._arg_1]._gender )
                                    take = False
                                    break
                                if in_pron_items[triples[trait_word._value]['arg_1'] ][15] != nodes[trait_word._arg_1]._number:
                                    get_out("arg_1 should be a " + nodes[trait_word._arg_1]._number )
                                    take = False
                                    break
                                if True:
                                    number =  nodes[trait_word._arg_1]._number
                                    gender =  nodes[trait_word._arg_1]._gender
                                    person = in_pron_items[triples[trait_word._value]['arg_1'] ][16]
                                    actualisation = in_pron_items[triples[trait_word._value]['arg_1'] ][17]
                                    value = nodes[trait_word._arg_1].value
                                    if value in ("étienne", ):
                                        take = False
                                        break
                                    if args.debug:
                                        if number not in pronouns_arg_x.keys():
                                            pronouns_arg_x[number] = {}
                                        if gender not in pronouns_arg_x[number].keys():
                                            pronouns_arg_x[number][gender] = {}
                                        if person not in pronouns_arg_x[number][gender].keys():
                                            pronouns_arg_x[number][gender][person] = {}
                                        if actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            pronouns_arg_x[number][gender][person][actualisation] = {}
                                        if value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            pronouns_arg_x[number][gender][person][actualisation][value] = 0
                                        pronouns_arg_x[number][gender][person][actualisation][value] += 1
                                    else:
                                        if number not in pronouns_arg_x.keys():
                                            take = False
                                        elif gender not in pronouns_arg_x[number].keys():
                                            take = False
                                        elif person not in pronouns_arg_x[number][gender].keys():
                                            take = False
                                        elif actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            take = False
                                        elif value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            take = False
                                        if not take:
                                            get_out("pronoun not accepted arg 1" )
                                            take = False
                                            break


                            elif triples[trait_word._value]["arg_1"] != nodes[trait_word._arg_1]._value:
                                if triples[trait_word._value]["arg_1"].startswith("argem_pronoun_"):
                                    if nodes[trait_word._arg_1].universal_pos != "pron":
                                        take = False
                                    else:
                                        take = verif(triples[trait_word._value]["arg_1"], nodes[trait_word._arg_1])
                                else:
                                    take = False
                                if not take:
                                    get_out("arg 1 isn't the expected argument " + used_verbs[trait_word._value]["voice"].__str__())
                                    break
                        if trait_word._arg_1 in (trait_word._arg_0, trait_word._arg_2, trait_word._arg_3):
                            take = False
                            get_out("arg 1 used for others arguments (same event)")
                            break

                        nodes[trait_word._arg_1].add_event(node_id, "arg 1", trait_word._type_arg_1)

                    elif trait_word._value in triples.keys() and  'arg_1' in triples[trait_word._value].keys():
                        take = False
                        get_out("arg 1 was ask but no found " + used_verbs[trait_word._value]["voice"].__str__())
                        break
                    if trait_word._arg_2.isdigit():
                        G.add_edge(node_id, trait_word._arg_2, color='blue', label="arg 2")
                        # l'arg2 n'est même pas dans les noeuds ou j'ai demandé un pronom et je n'en ai pas, déjà ce n'est pas bon.
                        if trait_word._arg_2 not in nodes.keys() or ("argem" in nodes[trait_word._arg_2]._value and nodes[trait_word._arg_2]._universal_pos == "pron"):
                            take = False
                            if trait_word._arg_2 not in nodes.keys():
                                get_out("arg 2 out of the scope " + used_verbs[trait_word._value]["voice"].__str__())
                            else:
                                get_out("arg 2 isn't an argument " + used_verbs[trait_word._value]["voice"].__str__())
                            break
                        if ("_" in nodes[trait_word._arg_2]._value and  nodes[trait_word._arg_2]._universal_pos == "pron"  ) :
                            get_out("arg_2 shouldn't be a pronoun with this name " + used_verbs[trait_word._value]["voice"].__str__())
                            take = False
                            break
                        else:

                            if "arg_2" not in triples[trait_word._value].keys():
                                take = False
                                get_out("arg 2 wasn't asked. " + used_verbs[trait_word._value]["voice"].__str__())
                                break

                            if not triples[trait_word._value]['arg_2'] in in_pron_items.keys() and nodes[trait_word._arg_2]._universal_pos == "pron" :
                                get_out("arg_2 should be a pronoun " + used_verbs[trait_word._value]["voice"].__str__())
                                take = False
                                break

                            if triples[trait_word._value]['arg_2'] in in_pron_items.keys() and nodes[trait_word._arg_2]._universal_pos == "pron" :
                                if in_pron_items[triples[trait_word._value]['arg_2'] ][14] != nodes[trait_word._arg_2]._gender:
                                    get_out("arg_2 should be a " + nodes[trait_word._arg_2]._gender )
                                    take = False
                                    break
                                if in_pron_items[triples[trait_word._value]['arg_2'] ][15] != nodes[trait_word._arg_2]._number:
                                    get_out("arg_2 should be a " + nodes[trait_word._arg_2]._number )
                                    take = False
                                    break
                                if True:
                                    number =  nodes[trait_word._arg_2]._number
                                    gender =  nodes[trait_word._arg_2]._gender
                                    person = in_pron_items[triples[trait_word._value]['arg_2'] ][16]
                                    actualisation = in_pron_items[triples[trait_word._value]['arg_2'] ][17]
                                    value = nodes[trait_word._arg_2].value
                                    if value in ("étienne", ):
                                        take = False
                                        break
                                    if args.debug:
                                        if number not in pronouns_arg_x.keys():
                                            pronouns_arg_x[number] = {}
                                        if gender not in pronouns_arg_x[number].keys():
                                            pronouns_arg_x[number][gender] = {}
                                        if person not in pronouns_arg_x[number][gender].keys():
                                            pronouns_arg_x[number][gender][person] = {}
                                        if actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            pronouns_arg_x[number][gender][person][actualisation] = {}
                                        if value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            pronouns_arg_x[number][gender][person][actualisation][value] = 0
                                        pronouns_arg_x[number][gender][person][actualisation][value] += 1
                                    else:
                                        if number not in pronouns_arg_x.keys():
                                            take = False
                                        elif gender not in pronouns_arg_x[number].keys():
                                            take = False
                                        elif person not in pronouns_arg_x[number][gender].keys():
                                            take = False
                                        elif actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            take = False
                                        elif value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            take = False
                                        if not take:
                                            get_out("pronoun not accepted arg 2" )
                                            take = False
                                            break
                            elif triples[trait_word._value]["arg_2"] != nodes[trait_word._arg_2]._value:
                                if triples[trait_word._value]["arg_2"].startswith("argem_pronoun_"):
                                    if nodes[trait_word._arg_2].universal_pos != "pron":
                                        take = False
                                    else:
                                        take = verif(triples[trait_word._value]["arg_2"], nodes[trait_word._arg_2])
                                else:
                                    take = False
                                if not take:
                                    get_out("arg 2 isn't the expected argument " + used_verbs[trait_word._value]["voice"].__str__())
                                    break
                        if trait_word._arg_2 in (trait_word._arg_0, trait_word._arg_1, trait_word._arg_3):
                            take = False
                            get_out("arg 2 used for others arguments (same event) " + used_verbs[trait_word._value]["voice"].__str__())
                            break


                        nodes[trait_word._arg_2].add_event(node_id, "arg 2", trait_word._type_arg_2)

                    elif trait_word._value in triples.keys() and  'arg_2' in triples[trait_word._value].keys():
                        take = False
                        get_out("arg 2 was ask but no found " + used_verbs[trait_word._value]["voice"].__str__())
                        break
                    if trait_word._arg_3.isdigit():
                        G.add_edge(node_id, trait_word._arg_3, color='blue', label="arg 3")
                        if trait_word._arg_3 not in nodes.keys() or ("argem" in nodes[trait_word._arg_3]._value and nodes[trait_word._arg_3]._universal_pos == "pron"):
                            take = False
                            if trait_word._arg_3 not in nodes.keys():
                                get_out("arg 3 out of the scope " + used_verbs[trait_word._value]["voice"].__str__())
                            else:
                                get_out("arg 3 isn't an argument " + used_verbs[trait_word._value]["voice"].__str__())
                            break
                        if ("_" in nodes[trait_word._arg_3]._value and  nodes[trait_word._arg_3]._universal_pos == "pron"  ) :
                            get_out("arg_3 shouldn't be a pronoun with this name " + used_verbs[trait_word._value]["voice"].__str__())
                            take = False
                            break
                        else:
                            if "arg_3" not in triples[trait_word._value].keys():
                                take = False
                                get_out("arg 3 wasn't asked. " + used_verbs[trait_word._value]["voice"].__str__())
                                break

                            if not triples[trait_word._value]['arg_3'] in in_pron_items.keys() and nodes[trait_word._arg_3]._universal_pos == "pron" :
                                get_out("arg_3 should be a pronoun " + used_verbs[trait_word._value]["voice"].__str__())
                                take = False
                                break

                            if triples[trait_word._value]['arg_3'] in in_pron_items.keys() and nodes[trait_word._arg_3]._universal_pos == "pron" :
                                if in_pron_items[triples[trait_word._value]['arg_3'] ][14] != nodes[trait_word._arg_3]._gender:
                                    get_out("arg_3 should be a " + nodes[trait_word._arg_3]._gender )
                                    take = False
                                    break
                                if in_pron_items[triples[trait_word._value]['arg_3'] ][15] != nodes[trait_word._arg_3]._number:
                                    get_out("arg_3 should be a " + nodes[trait_word._arg_3]._number )
                                    take = False
                                    break
                                if True:
                                    number =  nodes[trait_word._arg_3]._number
                                    gender =  nodes[trait_word._arg_3]._gender
                                    person = in_pron_items[triples[trait_word._value]['arg_3'] ][16]
                                    actualisation = in_pron_items[triples[trait_word._value]['arg_3'] ][17]
                                    value = nodes[trait_word._arg_3].value
                                    if value in ("étienne", ):
                                        take = False
                                        break
                                    if args.debug:
                                        if number not in pronouns_arg_x.keys():
                                            pronouns_arg_x[number] = {}
                                        if gender not in pronouns_arg_x[number].keys():
                                            pronouns_arg_x[number][gender] = {}
                                        if person not in pronouns_arg_x[number][gender].keys():
                                            pronouns_arg_x[number][gender][person] = {}
                                        if actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            pronouns_arg_x[number][gender][person][actualisation] = {}
                                        if value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            pronouns_arg_x[number][gender][person][actualisation][value] = 0
                                        pronouns_arg_x[number][gender][person][actualisation][value] += 1
                                    else:
                                        if number not in pronouns_arg_x.keys():
                                            take = False
                                        elif gender not in pronouns_arg_x[number].keys():
                                            take = False
                                        elif person not in pronouns_arg_x[number][gender].keys():
                                            take = False
                                        elif actualisation not in pronouns_arg_x[number][gender][person].keys():
                                            take = False
                                        elif value not in pronouns_arg_x[number][gender][person][actualisation].keys():
                                            take = False
                                        if not take:
                                            get_out("pronoun not accepted arg 3" )
                                            take = False
                                            break

                            elif triples[trait_word._value]["arg_3"] != nodes[trait_word._arg_3]._value:
                                if triples[trait_word._value]["arg_3"].startswith("argem_pronoun_"):
                                    if nodes[trait_word._arg_3].universal_pos != "pron":
                                        take = False
                                    else:
                                        take = verif(triples[trait_word._value]["arg_3"], nodes[trait_word._arg_3])
                                else:
                                    take = False
                                if not take:
                                    get_out("arg 3 isn't the expected argument " + used_verbs[trait_word._value]["voice"].__str__())
                                    break
                        if trait_word._arg_3 in (trait_word._arg_0, trait_word._arg_1, trait_word._arg_2):
                            take = False
                            get_out("arg 3 used for others arguments (same event) " + used_verbs[trait_word._value]["voice"].__str__())
                            break


                        nodes[trait_word._arg_3].add_event(node_id, "arg 3", trait_word._type_arg_3)

                    elif trait_word._value in triples.keys() and  'arg_3' in triples[trait_word._value].keys():
                        take = False
                        get_out("arg 3 was ask but no found " + used_verbs[trait_word._value]["voice"].__str__())
                        break
                if not take:
                    break

            if not take:
                continue

            # on vérifie les accords:

            if len(aux_data_information.keys()) > 0:
                for event_ in aux_data_information.keys():
                    to_ok_accord += 1
                    event_node_id = aux_data_information[event_]["node_id"]
                    arg_node_id = aux_data_information[event_]["accord_participe"]
                    accord = aux_data_information[event_]["accord"]
                    if nodes[tab_nodes[event_]]._gender == "":
                        nodes[tab_nodes[event_]]._gender = 'gender:masc'
                    if nodes[tab_nodes[event_]]._number == "":
                        nodes[tab_nodes[event_]]._number = 'number:sing'
                    if accord and len(nodes[tab_nodes[event_]]._aux)> 0:
                        if arg_node_id == "masc_sing":
                            if not nodes[tab_nodes[event_]]._number in ('number:sing', ''):
                                take = False
                                get_out("verbal agreement on number error : not required")
                                if "no_accord" not in  accords['no_wait'].keys():
                                    accords['no_wait']['no_accord'] = {"number":{}, "gender":{}}
                                expri = nodes[tab_nodes[event_]]._number
                                if not expri in accords['no_wait']['no_accord']["number"].keys():
                                    accords['no_wait']['no_accord']["number"][expri] = 0
                                accords['no_wait']['no_accord']["number"][expri] += 1
                            else:
                                nodes[tab_nodes[event_]]._number = 'number:sing'
                            if not nodes[tab_nodes[event_]]._gender in ('', 'gender:masc', 'gender:com'):
                                if take:
                                    get_out("verbal agreement on gender error : not required")
                                take = False
                                if "no_accord" not in  accords['no_wait'].keys():
                                    accords['no_wait']['no_accord'] = {"number":{}, "gender":{}}
                                expri = nodes[tab_nodes[event_]]._gender
                                if not expri in accords['no_wait']['no_accord']["gender"].keys():
                                    accords['no_wait']['no_accord']["gender"][expri] = 0
                                accords['no_wait']['no_accord']["gender"][expri] += 1
                            else:
                                nodes[tab_nodes[event_]]._gender = 'gender:masc'
                                ok_accord += 1
                            if take == False:
                                break
                        else:
                            if nodes[arg_node_id]._number != nodes[tab_nodes[event_]]._number:
                                if not (nodes[arg_node_id]._number == "" and nodes[tab_nodes[event_]]._number == "number:sing" ):
                                    get_out("verbal agreement on number, required but false")
                                    if "no_accord" not in  accords['wait'].keys():
                                        accords['wait']['no_accord'] = {"number":{}, "gender":{}}
                                    expri = nodes[tab_nodes[event_]]._number + " for arg " +  nodes[arg_node_id]._number
                                    if not expri in accords['wait']['no_accord']["number"].keys():
                                        accords['wait']['no_accord']["number"][expri] = 0
                                    accords['wait']['no_accord']["number"][expri] += 1
                                    take = False

                            if nodes[arg_node_id]._gender != nodes[tab_nodes[event_]]._gender:
                                if not (nodes[arg_node_id]._gender == "" and nodes[tab_nodes[event_]]._gender == "gender:masc" ):
                                    get_out("verbal agreement on gender, required but false")
                                    if "no_accord" not in  accords['wait'].keys():
                                        accords['wait']['no_accord'] = {"number":{}, "gender":{}}
                                    expri = nodes[tab_nodes[event_]]._gender + " for arg " +  nodes[arg_node_id]._gender
                                    if not expri in accords['wait']['no_accord']["gender"].keys():
                                        accords['wait']['no_accord']["gender"][expri] = 0
                                    accords['wait']['no_accord']["gender"][expri] += 1
                                    take = False

                            if not take :
                                break

                        ok_accord += 1
            if not take :
                continue
            # note : deux temps par génération ne seraient sans doute pas mal
            # au passif, c'est la conjugaison de l'auxiliaire être qui va compter.
            for aux_tps_supp in aux_tps_support.keys():
                if len(nodes[tab_nodes[aux_tps_supp]]._aux) == 0:
                    take = False
                    get_out("auxiliary required but none")
                    continue
                else:
                    aux_mem = aux_tps if used_verbs[aux_tps_supp]['voice'] == 'voice:act' else passifs
                    mode_origine =  aux_tps_support[aux_tps_supp][0]
                    temps_origine =  aux_tps_support[aux_tps_supp][1]
                    # mon verbe est passé au participe passé.
                    if mode_origine not in aux_mem.keys():
                        aux_mem[mode_origine] = {}
                    if temps_origine not in aux_mem[mode_origine].keys():
                        aux_mem[mode_origine][temps_origine] = {}
                    if len(nodes[tab_nodes[aux_tps_supp]]._aux) > 1 :
                        take = False
                        get_out("to much auxiliaries for the form")
                        break
                    aux_courant = nodes[tab_nodes[aux_tps_supp]]._aux[0]
                    if aux_courant._mode not in aux_mem[mode_origine][temps_origine].keys():
                        aux_mem[mode_origine][temps_origine][aux_courant._mode] = {}
                    if aux_courant._temps not in aux_mem[mode_origine][temps_origine][aux_courant._mode].keys():
                        aux_mem[mode_origine][temps_origine][aux_courant._mode][aux_courant._temps] = 0
                    aux_mem[mode_origine][temps_origine][aux_courant._mode][aux_courant._temps] += 1
                    if used_verbs[aux_tps_supp]['voice'] == 'voice:act':
                        pass
                    else:
                        pass
                    voice = used_verbs[aux_tps_supp]['voice']
                    if voice not in temps_auxiliaire.keys():
                        temps_auxiliaire[voice] = {}
                    if mode_origine not in temps_auxiliaire[voice].keys():
                        temps_auxiliaire[voice][mode_origine] = {}
                    if temps_origine not in temps_auxiliaire[voice][mode_origine].keys():
                        temps_auxiliaire[voice][mode_origine][temps_origine] = {}
                    if aux_courant._mode not in temps_auxiliaire[voice][mode_origine][temps_origine].keys():
                        temps_auxiliaire[voice][mode_origine][temps_origine][aux_courant._mode]  = {}
                    if aux_courant._temps not in temps_auxiliaire[voice][mode_origine][temps_origine][aux_courant._mode].keys():
                        temps_auxiliaire[voice][mode_origine][temps_origine][aux_courant._mode][aux_courant._temps] = 0
                    temps_auxiliaire[voice][mode_origine][temps_origine][aux_courant._mode][aux_courant._temps] += 1

                    # l'auxiliaire doit s'accorder ave le sujet
                    sujet = nodes[tab_nodes[aux_tps_supp]]._arg_0 if used_verbs[aux_tps_supp]['voice'] == 'voice:act' else nodes[tab_nodes[aux_tps_supp]]._arg_1
                    sujet = nodes[sujet]
                    # on se contrefixhe du genre, nombre et personne par contre...
                    if sujet._number != aux_courant._number:
                        take = False
                        get_out("bad number for auxiliary")
                        break
                    if aux_courant._number == "":
                        get_out("no number for auxiliary")
                        take = False
                        break

                    if not (aux_courant._person== 'person:3' and sujet._person == ''):
                        if sujet._person != aux_courant._person :
                            get_out("bad person for auxiliary")
                            take = False
                            break
                        if aux_courant._person == "":
                            get_out("no person for auxiliary")
                            take = False
                            break

            d = list(nx.connected_component_subgraphs(G.to_undirected()))
            # update 201908260026
            if len(d) > 1:
                get_out("subgraphs not connected")
                continue

            for key in labeldict.keys(): # on est passé dessus mais rien ne le lie
                if key not in G.nodes():
                    take = False
                    get_out("unused node")
                    break
                if labeldict[key].startswith("argevent"):
                    if len(nodes[key]._events) == 0:
                        take = False
                        get_out("argument of event without event")
                        break
                    elif len(nodes[key]._events) > 1:
                        special = True
                # update 201908231113
                for event in nodes[key]._events:
                    labeldict[key]+= ("\n" + event[2]) if event[2] not in labeldict[key] else ""
                    if event[2] not in types_word.keys():
                        types_word[event[2]] = {}
                    if "categorie" not in types_word[event[2]].keys():
                        types_word[event[2]]['categorie'] = []
                    types_word[event[2]]['categorie'].append(event[0])

            if not take:
                continue

            if count_sent % 10000 == 0 or special:
                nx.set_node_attributes(G, labeldict, 'label')

                lcoal_path = os.path.join(".", "debug_dots" if args.debug else "dots")
                if not os.path.exists(lcoal_path):
                    os.makedirs(lcoal_path)
                path = os.path.join(".", "debug_dots" if args.debug else "dots", name + ".dot")
                write_dot(G, path)

                subprocess.run("cd " + lcoal_path + " && dot -Tpng " + name + ".dot > " + name + ".png && rm " + name + ".dot",
                           shell=True, check=True)
            # subprocess.run ("cd " + lcoal_path + " && dot -Tpng " + name + ".dot > " + name + ".png", shell=True, check=True )

            source_file_filtred.write(in_ + "\n")
            pred_file_filtred.write(out_ + "\n")
            info_file_filtred.write(info_ + "\n")
            suivi_ = suivi_.split()

            suivi_.insert(1, "conj:"+used_verbs['event_1']['mode']+"/"+used_verbs['event_1']['temps'])

            suivi_file_filtred.write(" ".join(suivi_) + "\n")
            count_kept += 1
            for m in verbes_gen.keys():
                if m not in verbes_générés.keys(): verbes_générés[m] = {}
                for n in verbes_gen[m].keys():
                    if n not in verbes_générés[m].keys():
                        verbes_générés[m][n] = {}
                    for m2 in verbes_gen[m][n].keys():
                        if m2 not in verbes_générés[m][n].keys(): verbes_générés[m][n][m2] = {}
                        for n2 in verbes_gen[m][n][m2].keys():
                            if n2 not in verbes_générés[m][n][m2].keys():
                                verbes_générés[m][n][m2][n2] = 0
                            verbes_générés[m][n][m2][n2] += 1

            from_start_item_greff = None
            from_start_greff = None
            token_id = -1
            for node_id, word in enumerate(words):
                if "￨pron￨" in word :
                    if from_start_item_greff is not None:
                        break
                    from_start_item_greff = ((node_id + 1).__str__(), word)
                    from_start_greff = " ".join(words[node_id + 1:])
                    token_id = (node_id + 1).__str__()
                elif word.startswith("argevent"):
                    from_start_item_greff = ((node_id + 1).__str__(), word)
                    from_start_greff = " ".join(words[node_id + 1:])
                    token_id = (node_id + 1).__str__()
                elif "clr" in word.lower() and from_start_item_greff is not None:
                    break
                elif word.startswith("ne￨") and from_start_item_greff is not None:
                    break
                elif word.startswith("n'￨") and from_start_item_greff is not None:
                    break
                elif word.startswith("event") and from_start_item_greff is not None:
                    break
                elif "￨aux￨" in word and from_start_item_greff is not None:
                    break
                elif "￨det￨" in word and from_start_item_greff is not None:
                    from_start_item_greff = None
                    from_start_greff = None
                    break
                elif "￨det￨" in word and from_start_item_greff is None:
                    pass
                else:
                    from_start_item_greff = None
                    from_start_greff = None
                    break

            prep = None
            from_end_item_greff = None
            from_end_greff = None
            token_id_end = -1
            if ("￨pron￨" in words[-2] or words[-2].startswith("argevent")) and "￨punct￨" in words[-1]:
                for node_id, word in reversed([(i, k) for i, k in enumerate(words[0:-1])]):
                    if (word.startswith("argevent") or "￨pron￨" in word) and from_end_item_greff is None:
                        token_id_end = (node_id + 1).__str__()
                    if word.startswith('-') or (word.endswith("event") and from_end_item_greff is None ):
                        prep = None
                        from_end_item_greff = None
                        from_end_greff = None
                        break
                    if from_end_item_greff is None :
                        from_end_item_greff = ((node_id + 1).__str__(), word)
                    elif "￨det￨" in word :
                        if prep is not None:
                            prep = None
                            from_end_item_greff = None
                            from_end_greff = None
                        pass
                    elif "￨adp￨" in word:
                        prep = ((node_id + 1).__str__(), word)
                        from_end_greff = " ".join(words[:node_id])
                        break
                    elif (not from_end_item_greff is None) and (word.endswith("event") or word.startswith("pas￨")):
                        from_end_greff = " ".join(words[:node_id+1])
                        break
                    else:
                        prep = None
                        from_end_item_greff = None
                        from_end_greff = None
                        break
            # suivi_ = <class 'list'>: ['2', 'conj:indicatif/passé-simple', 'P1300|humain|chose||', 'DIV', 'voice:act']
            base_dico = {}
            base_dico['sent_id'] = suivi_[0]
            base_dico['specif'] = " ".join(suivi_[-2:])
            base_dico['in_'] = in_
            base_dico['out_'] = out_
            base_dico['time_event_1'] = suivi_[1]
            base_dico['structure_event_1'] = suivi_[2]
            base_dico['event_1'] = used_verbs['event_1']['traits']
            base_dico['sujet_event_1'] = nodes[Trait(used_verbs['event_1']['traits'])._arg_0].getTrait()
            base_dico['event_1_ask'] = in_.split()[1]
            base_dico['sujet_event_1_ask'] = in_.split()[3]
            base_dico['sujet_event_1_properties'] = "￨".join(base_dico['sujet_event_1_ask'].split("￨")[4:])

            d_k = list(base_dico.keys())
            keys = ','.join(d_k)
            question_marks = ','.join(list('?' * len(base_dico)))
            values = tuple(base_dico[k].__str__() for k in d_k)
            a = curseur.execute('INSERT INTO asked_sentences (' + keys + ') VALUES (' + question_marks + ')', values)
            a = base_dico['sujet_event_1_ask'].split("￨")
            a = a[4:]
            a = "￨".join(a)
            group_by = base_dico['sent_id'] + a
            if (group_by) not in sujs_voice.keys():
                sujs_voice[group_by] = []
            sujs_voice[group_by].append((base_dico['specif'] +  suivi_[-2], base_dico['time_event_1'], out_))

            dico = {}
            if from_start_item_greff is not None:
                # item avant greffable
                trait_word = Trait(from_start_item_greff[1])
                dico['sent_id'] = suivi_[0]
                dico['level'] = "start"
                dico['prep'] = ""
                dico['token_id'] = token_id
                dico['traits_args'] = from_start_item_greff[1].split("￨")[2:-2]
                dico['traits_complets'] = from_start_item_greff[1]
                dico['universal_pos'] = trait_word.universal_pos.__str__()
                dico['gender'] = trait_word._gender.__str__()
                dico['number'] = trait_word._number.__str__()
                dico['person'] = trait_word._person.__str__()
                dico['dep'] = trait_word._dep.__str__()
                dico['nature'] = trait_word._nature.__str__()
                dico['categorie'] = types_word[from_start_item_greff[0]]
                dico['sentence'] = out_
                dico['cut'] = from_start_greff
                dico['verbs_count'] = len(used_verbs)
                type_arguments = []
                for verb in used_verbs.keys():
                    type_arguments += used_verbs[verb]['types_args']

                values_lvf = [k.split("-")[1] for k in type_arguments if k.startswith(token_id)]

                for value_lvf in values_lvf:
                    dico['type_lvf'] = value_lvf
                    dico['args'] = json.dumps(type_arguments, ensure_ascii=False)
                    dico['used_verbs'] = json.dumps(used_verbs, ensure_ascii=False)
                    dico['sujet_event_1_properties'] = "￨".join(base_dico['sujet_event_1_ask'].split("￨")[4:])

                    d_k = list(dico.keys())
                    keys = ','.join(d_k)
                    question_marks = ','.join(list('?' * len(dico)))
                    values = tuple(dico[k].__str__() for k in d_k)
                    a = curseur.execute('INSERT INTO greffes_data (' + keys + ') VALUES (' + question_marks + ')', values)

            dico = {}
            if from_end_item_greff is not None:
                # item avant greffable

                trait_word = Trait(from_end_item_greff[1])
                dico['level'] = "end"
                dico['prep'] = "" if prep is None else prep[1]
                dico['sent_id'] = suivi_[0]
                dico['token_id'] = token_id_end
                dico['traits_args'] = from_end_item_greff[1].split("￨")[2:-2]
                dico['traits_complets'] = from_end_item_greff[1]
                dico['universal_pos'] = trait_word.universal_pos.__str__()
                dico['gender'] = trait_word._gender.__str__()
                dico['number'] = trait_word._number.__str__()
                dico['person'] = trait_word._person.__str__()
                dico['dep'] = trait_word._dep.__str__()
                dico['nature'] = trait_word._nature.__str__()
                dico['categorie'] = types_word[from_end_item_greff[0]]
                dico['cut'] = from_end_greff
                type_arguments = []
                for verb in used_verbs.keys():
                    type_arguments += used_verbs[verb]['types_args']

                values_lvf = [k.split("-")[1] for k in type_arguments if k.startswith(token_id_end)]

                for value_lvf in values_lvf:
                    dico['type_lvf'] = value_lvf
                    dico['sentence'] = out_
                    dico['args'] = json.dumps(type_arguments, ensure_ascii=False)
                    dico['verbs_count'] = len(used_verbs)
                    dico['used_verbs'] = json.dumps(used_verbs, ensure_ascii=False)
                    dico['sujet_event_1_properties'] = "￨".join(base_dico['sujet_event_1_ask'].split("￨")[4:])

                    d_k = list(dico.keys())
                    keys = ','.join(d_k)
                    question_marks = ','.join(list('?' * len(dico)))
                    values = tuple(dico[k].__str__() for k in d_k)
                    a = curseur.execute('INSERT INTO greffes_data (' + keys + ') VALUES (' + question_marks + ')', values)

        else:
            get_out("expected items")
            continue

        if args.ask_small: #todo eject
            req = "select count(*), substr(prep,1, 3) from greffes_data where prep !='' group by  substr(prep,1, 3)"
            rows = curseurSecondaire.execute(req, {}).fetchall()
            if len(rows) >= 5:
                req = "select count(*) from greffes_data where prep !=''"
                row = curseurSecondaire.execute(req, {}).fetchone()
                if row[0] >= 590:
                    break
    i_count_var = 0
    in_var = open(args.source.replace('.txt', '_train_var_in.txt'), 'w')
    out_var = open(args.source.replace('.txt', '_train_var_out.txt'), 'w')
    for key in sujs_voice.keys():
        if len(sujs_voice[key]) > 1:
            i_count_var += 1
            for a, b in itertools.permutations(sujs_voice[key], 2):
                if 'PRON_EXPRESS' in a[0] and not 'PRON_EXPRESS' in b[0]:
                    continue
                if a[-1] == b[-1]:
                    continue

                if 'voice:pass' in b[0] and not 'voice:pass' in a[0]:
                    if 'PRON_EXPRESS' in a[0] and not 'PRON_EXPRESS' in b[0]:
                        continue
                    if 'DIV' in a[0] and not 'DIV' in b[0]:
                        continue
                    print("%variation% " + a[-1], file=in_var)
                    print("%passif% " + b[-1], file=out_var)
                if 'voice:pass' in a[0] and not 'voice:pass' in b[0]:
                    if 'PRON_EXPRESS' in a[0] and not 'PRON_EXPRESS' in b[0]:
                        continue
                    if 'DIV' in a[0] and not 'DIV' in b[0]:
                        continue
                    print("%variation% " + a[-1], file=in_var)
                    print("%actif% " + b[-1], file=out_var)

                if 'PRON_EXPRESS' in b[0] and not 'PRON_EXPRESS' in a[0]:
                    kind_of = "%pronominalisation%"
                else:
                    if b[1] != a[1]:
                        kind_of = "%" + a[1] + "%" + b[1] + '%'
                    else:
                        continue

                print("%variation% " + a[-1], file=in_var)
                print(kind_of + " " + b[-1], file=out_var)

    print(i_count_var)
    print(i_count_var, file=sys.stderr, end="basic variations.\n")

    source_file.close()
    pred_file.close()

    suivi_file_filtred.flush()
    suivi_file_filtred.close()
    while not suivi_file_filtred.closed:
        True

    source_file_filtred.flush()
    source_file_filtred.close()
    while not source_file_filtred.closed:
        True

    info_file_filtred.flush()
    info_file_filtred.close()
    while not info_file_filtred.closed:
        True

    pred_file_filtred.flush()
    pred_file_filtred.close()
    while not pred_file_filtred.closed:
        True

    print()
    print(count_kept, end=" lines kept.\n")
    print(num_lines_debug - count_kept if args.debug else num_lines_source - count_kept, end=" lines loose.\n")
    print(num_lines_debug - count_kept if args.debug else num_lines_real - count_kept, end=" real lines loose.\n")
    print(round(count_kept / (num_lines_debug - count_kept if args.debug else num_lines_real - count_kept) * 100, 2),
          end=" ratio lines kept/lines lost.\n")

    # pprint(verbes)

    max = max([len(key) for key in __outed__.keys()])
    somme_perte = 0
    for key in sorted(__outed__.keys()):
        print(key.__str__().rjust(max), end=" -> ")
        print(__outed__[key])
        somme_perte += __outed__[key]
    print(somme_perte, end=" pertes.")
    print()
    print()
    print('accord : ' + ok_accord.__str__() + "/", end="")
    print(to_ok_accord)
    print("décompte par temps")
    somme = 0

    for mode in verbes_générés.keys():
        for temps in verbes_générés[mode].keys():
            for mode_gen in verbes_générés[mode][temps].keys():
                if mode not in event_1_allowed_times.keys():
                    continue
                if temps not in event_1_allowed_times[mode].keys():
                    continue
                if mode_gen in event_1_allowed_times[mode][temps].keys():
                    for temps_gen in verbes_générés[mode][temps][mode_gen].keys():
                        if temps_gen in event_1_allowed_times[mode][temps][mode_gen]:
                            print(mode, end="\t")
                            print(temps, end="\t")
                            print(verbes_générés[mode][temps][mode_gen][temps_gen])
                            somme += verbes_générés[mode][temps][mode_gen][temps_gen]
    print("soit :", end=somme.__str__())
    print(" verbes principaux.")
    print()

    print("auxiliaires par temps auxiliarisé actifs")
    pprint(aux_tps)
    print("auxiliaires par temps auxiliarisé passifs")
    pprint(passifs)
    print("erreurs d'accords")
    pprint(accords)
    print("temps des auxiliaires")
    pprint(temps_auxiliaire)

    print('pronouns_arg_0_autorized = ', end="\\\n")
    pprint(pronouns_arg_0)

    print('pronouns_arg_x_autorized = ', end="\\\n")
    pprint(pronouns_arg_x)
    genders = {}
    persons = {}
    numbers = {}

    for dicto in (pronouns_arg_0, pronouns_arg_x):
        for number in dicto.keys():
            for gender in dicto[number].keys():
                for person in dicto[number][gender].keys():
                    for actualisation in dicto[number][gender][person].keys():
                        for value in dicto[number][gender][person][actualisation].keys():
                            if value not in numbers.keys():
                                numbers[value] = {}
                            if number not in numbers[value].keys():
                                numbers[value][number] = 0
                            numbers[value][number] += dicto[number][gender][person][actualisation][value]
                            if value not in genders.keys():
                                genders[value] = {}
                            if gender not in genders[value].keys():
                                genders[value][gender] = 0
                            genders[value][gender] += dicto[number][gender][person][actualisation][value]
                            if value not in persons.keys():
                                persons[value] = {}
                            if person not in persons[value].keys():
                                persons[value][person] = 0
                            persons[value][person] += dicto[number][gender][person][actualisation][value]
    pprint(numbers)
    pprint(genders)
    pprint(persons)

connexion.commit()

if True:
    in_var = open(args.source.replace('.txt', '_var_in_synt.txt'), 'w')
    out_var = open(args.source.replace('.txt','_var_out_synt.txt'), 'w')
    p = re.compile('￨(\S)*￨\S+')


    def rename_traits_values(tab_traits_1, tab_traits_2, kept_name_1, unkept_name_1):
        args_1 = set()
        kept_name = kept_name_1.split('￨')[0]
        unkept_name = unkept_name_1.split('￨')[0]
        renamed_data = {}
        for trait in tab_traits_1:
            if trait._value.startswith("arge") or trait._value.startswith("mod_") or trait._value.startswith("event_"):
                args_1.add(trait._value)
        if kept_name in args_1:
            args_1.remove(kept_name)
        for trait in tab_traits_2:
            if trait._value == unkept_name:
                args_1.add(kept_name)
                renamed_data[trait._value] = kept_name
                trait._value = kept_name
            elif trait.value in renamed_data.keys():
                pass
            elif "_" in trait._value and trait._value.split("_")[1].isdigit():
                values = trait._value.split("_")
                value =  0
                while trait._value in args_1 or trait._value == kept_name or value == 0:
                    value +=1
                    trait._value = values[0] + "_" + value.__str__()
                args_1.add(trait._value)
                renamed_data["_".join(values)] = trait._value
        return tab_traits_2, renamed_data


    """
    Le cheval qui se trouve dans cette prairie, appartient à mon oncle.
    Le pronom relatif « qui » s'emploie comme sujet, en parlant des personnes, des animaux ou des choses.
    L'enfant à qui tu as donné ce livre en est tout heureux.
    
    "qui" peut être également complément d'objet indirect. Dans ce cas, il est précédé d'une préposition et ne remplace que des personnes.
    La pomme que tu manges vient directement du verger.
    Le pronom relatif « que » est complément d'objet direct. Il remplace des personnes, des animaux ou des choses.
    """
    traits_qui = Trait("qui￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨5￨relatif") #dep = argument repris (qui sera le sujet du verbe)
    traits_que = Trait("que￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")

    """
    Voici l'ami dont je vous ai parlé.
    Je vous présente mon travail, dont je suis fier.
    Le pronom relatif « dont » peut avoir différentes fonctions. Il remplace des personnes, des animaux ou des choses.
    Je vis dans une __MAISON__ dont les murs tombaient en ruines.
    Il peut être complément du nom (les murs de la __MAISON__),
    Cette jolie table dont je t'ai parlé est à vendre.
    ou complément d'objet indirect  (je t'ai parlé de cette jolie table)
    Les rennes découvrent sous la neige des lichens dont ils sont friands.
        ou encore complément de l'adjectif (les rennes sont friands de lichens)
    """
    traits_dont = Trait("dont￨pron￨￨￨￨￨￨￨￨￨￨￨￨￨￨12￨relatif")
    """
    L'endroit où je me trouve est ombragé.
    Le pays où je vis est le plus beau !
    Le plus souvent, on emploie le pronom relatif « où » pour remplacer  « lequel, précédé d'une préposition ». Ce pronom relatif est toujours complément circonstanciel de lieu ou de temps.
    """

    traits_où = Trait("où￨pron￨￨￨￨￨￨￨￨￨￨￨￨￨￨5￨relatif")

    """
    Marivaux a donné son nom à quelque chose à quoi il n'avait jamais pensé.
    C'est ce à quoi j'ai pensé toute la journée.
    Le pronom relatif « quoi » ne s'emploie qu'en parlant des choses ; il est toujours complément. Il représente souvent un antécédent de sens vague comme "rien" / "ce" / "chose" / "quelque chose".
    """
    traits_quoi = Trait("quoi￨pron￨￨￨￨￨￨￨￨￨￨￨￨￨￨5￨relatif")
    traits_chez = Trait("chez￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨5￨indéfini") #dep = verbe concerné
    traits_chez = Trait("à￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨5￨indéfini") #dep = verbe concerné
    """
    variables
    """
    """"
    J'ai couru chez mon oncle et ma tante, laquelle m'a remis une lettre pour vous.
    "lequel" s'emploie comme sujet quand on craint l'équivoque ou que l'on désire simplement mettre l'antécédent en valeur. Attention : cette tournure est teintée d'archaïsme.
    
    Vous avez voulu donner à l'illustre compagnie à laquelle j'ai l'honneur d'appartenir, une marque de confiance.
    "lequel" s'emploie comme complément ; il remplace le plus souvent des choses et des animaux.
    """
    traits_dont= Trait("dont￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨5￨relatif") #dep = argument repris (qui sera le sujet du verbe)
    traits_lequel= Trait("lequel￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨5￨relatif") #dep = argument repris (qui sera le sujet du verbe)
    traits_laquelle = Trait("laquelle￨pron￨￨￨￨￨￨￨￨￨￨￨￨￨￨12￨relatif")
    traits_lesquels= Trait("lesquels￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨5￨relatif") #dep = argument repris (qui sera le sujet du verbe)
    traits_lesquelles= Trait("lesquelles￨pron￨￨￨￨￨￨￨￨￨￨￨￨￨￨12￨relatif")
    traits_duquel = Trait("duquel￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")
    traits_delaquelle = [Trait("de￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨6￨relatif"), Trait("laquelle￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")]
    traits_desquels = Trait("desquels￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")
    traits_desquelles = Trait("desquelles￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")
    traits_auquel = Trait("auquel￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")
    traits_alaquelle = [Trait("à￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨6￨relatif"), Trait("laquelle￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")]
    traits_auxquels = Trait("auxquels￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")
    traits_auxquelles = Trait("auxquelles￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨6￨relatif")

    req = "select * from greffes_data  ORDER BY RANDOM()"
    rows = curseur.execute(req, {}).fetchall()
    sents_id = set()
    backud_id = set()
    for row, _ in zip(rows, tqdm(range(len(rows)))):
        row = list(row)
        ID_1 = row.pop(0)
        if ID_1 in backud_id:
            continue
        sent_id_1 = row.pop(0)
        token_id_1 = row.pop(0)
        traits_args_1 = row.pop(0)
        traits_complets_1 = row.pop(0)
        cut_1 = row.pop(0)
        universal_pos_1 = row.pop(0)
        gender_1 = row.pop(0)
        number_1 = row.pop(0)
        person_1 = row.pop(0)
        dep_1 = row.pop(0)
        nature_1 = row.pop(0)
        level_1 = row.pop(0)
        prep_1 = row.pop(0)
        backud_id.add(ID_1)
        if sent_id_1 + prep_1 in sents_id:
            continue
        sents_id.add(sent_id_1 + prep_1)
        categorie_1 = eval(row.pop(0))
        sentence_1 = row.pop(0)
        verbs_count_1 = row.pop(0)
        args_1 = eval(row.pop(0))
        sujet_event_1_properties_1 = row.pop(0)
        type_lvf_1 = row.pop(0)
        used_verbs_1 = eval(row.pop(0))
        sentence_depart_1 = p.sub("", sentence_1)
        type_arg_sent_prem_1 = [arg for arg in args_1 if arg.startswith(token_id_1)][0][2:]
        traits_sentences_depart_1 = [Trait(word) for word in sentence_1.split()]
        locked = Trait(traits_complets_1)
        locked._value = "%"
        locked._dep = "%"
        locked_pron = Trait(traits_complets_1)
        locked_pron._value = "%"
        locked_pron._dep = "%"
        locked_pron._universal_pos = "pron"
        comp = " and (traits_complets like '" + locked.getTrait() + "' or traits_complets like '" + locked_pron.getTrait() + "') "
        req = "select * from greffes_data where type_lvf like '%"+type_lvf_1+"%'  and level != 'end' " + comp + " and   sent_id != :sent_id   ORDER BY RANDOM() "
        rows_secondaire = curseurSecondaire.execute(req, {'sent_id':sent_id_1})#,'traits_args':traits_args_1})
        #rows_secondaire = rows_secondaire.fetchall()
        rows_news = []
        for row_2 in rows_secondaire:
            row_2 = list(row_2)
            if row_2[0] in backud_id:
                continue
            if len(rows_news) == 5:
                break
            rows_news.append(row_2)
            backud_id.add(row_2[0])
        for row_2 in rows_news:
            ins_ , outs_, contraintes_ = [], [], []

            # on fait des copies de nos variables
            sent_id = sent_id_1[:]
            token_id = token_id_1[:]
            traits_args = eval(traits_args_1)
            traits_complets = traits_complets_1[:]
            cut = cut_1[:]
            universal_pos = universal_pos_1[:]
            gender = gender_1[:]
            number = number_1[:]
            person = person_1[:]
            dep = dep_1[:]
            nature = nature_1[:]
            level = level_1[:]
            prep = prep_1[:]
            categorie = categorie_1.copy()
            sentence = sentence_1[:]
            verbs_count = verbs_count_1[:]
            args_used = args_1[:]
            sujet_event_1_properties = sujet_event_1_properties_1[:]
            type_lvf = type_lvf_1[:]
            used_verbs = used_verbs_1.copy()
            sentence_depart = sentence_depart_1[:]
            type_arg_sent_prem = type_arg_sent_prem_1[:]
            traits_sentences_depart = [Trait(word) for word in sentence_1.split()]

            row = row_2
            ID_2 = row.pop(0)
            sent_id_2 = row.pop(0)
            token_id_2 = row.pop(0)
            traits_args_2 = eval(row.pop(0))
            traits_complets_2 = row.pop(0)
            cut_2 = row.pop(0)
            universal_pos_2 = row.pop(0)
            gender_2 = row.pop(0)
            number_2 = row.pop(0)
            person_2 = row.pop(0)
            dep_2 = row.pop(0)
            nature_2 = row.pop(0)
            level_2 = row.pop(0)
            prep_2 = row.pop(0)
            categorie_2 = eval(row.pop(0))
            sentence_2 = row.pop(0)
            verbs_count_2 = row.pop(0)
            args_2 = eval(row.pop(0))
            type_arg_sent_sec = [arg for arg in args_2 if arg.startswith(token_id_2)][0][2:]
            sujet_event_1_properties_2 = row.pop(0)
            type_lvf = row.pop(0)
            used_verbs_2 = eval(row.pop(0))
            sentence_next = p.sub("", sentence_2)
            cut_depart = p.sub("", cut)
            cut_depart_inversed = sentence_depart.replace(cut_depart, "").strip()
            cut_next = p.sub("", cut_2)
            cut_next_inversed = sentence_next.replace(cut_next, "").strip()
            cut_inversed = sentence.replace(cut, "").strip()
            cut_2_inversed = sentence_2.replace(cut_2, "").strip()

            traits_sentences_next = [Trait(word) for word in sentence_2.split()]
            sent_1 = random.choice((traits_sentences_depart, traits_sentences_next))

            kept_name = sorted((traits_complets_1, traits_complets_2) , key=lambda x:x.split("_")[0])[-1]
            un_kept = traits_complets_1 if traits_complets_2 == kept_name else traits_complets_2
            if traits_complets_1 != traits_complets_2:
                if kept_name == traits_complets_1:
                    sent_1 = traits_sentences_depart
                else:
                    sent_1 = traits_sentences_next
            if sent_1 == traits_sentences_depart:
                sent_2 = traits_sentences_next
                traits_sentences_next, renamed_data = rename_traits_values(sent_1, sent_2, kept_name, un_kept)    # kept_name in sent_1
                if traits_complets_1 == traits_complets_2:
                    del renamed_data[traits_complets_1.split("￨")[0]]
                dones = sorted(list(renamed_data.keys()), key=lambda x:len(x), reverse=True)
                dones_args = [a for a in dones if "arg" in a]
                dones_vent = [a for a in dones if a not in dones_args]
                for key in renamed_data.keys():
                    if key in dones_args:
                        renamed_data[key] = renamed_data[key].replace("_", "£")
                i_get_out = 0
                for dones in (dones_args, dones_vent):
                    while len(dones) > 0:
                        if i_get_out == 25:
                            break
                        for old_etiquette in dones:
                            if i_get_out == 25:
                                break
                            i_get_out += 1
                            if renamed_data[old_etiquette] not in sentence_2:
                                dones.remove(old_etiquette)
                                cut_2 = cut_2.replace(old_etiquette, renamed_data[old_etiquette])
                                sentence_2 = sentence_2.replace(old_etiquette, renamed_data[old_etiquette])
                                cut_next = cut_next.replace(old_etiquette, renamed_data[old_etiquette])
                                cut_next_inversed = cut_next_inversed.replace(old_etiquette, renamed_data[old_etiquette])
                                sentence_next = sentence_next.replace(old_etiquette, renamed_data[old_etiquette])
                                traits_complets_2 = traits_complets_2.replace(old_etiquette, renamed_data[old_etiquette])
                                if old_etiquette.startswith("event"):
                                    used_verbs_2[renamed_data[old_etiquette]] = used_verbs_2[old_etiquette]
                                    del used_verbs_2[old_etiquette]
                                break
                            elif renamed_data[old_etiquette] == old_etiquette:
                                dones.remove(old_etiquette)
                                break
                            else:
                                replaced_char = "£" if "arg" in  old_etiquette else "_"
                                renamed_data[renamed_data[old_etiquette].replace(replaced_char, "%")] = renamed_data[old_etiquette]
                                renamed_data[old_etiquette] = renamed_data[old_etiquette].replace(replaced_char, "%")
                                dones.append(renamed_data[old_etiquette].replace(replaced_char, "%"))
                                break

                if i_get_out == 20:
                    continue
                cut_2 = cut_2.replace("£", "_")
                sentence_2 = sentence_2.replace("£", "_")
                cut_next = cut_next.replace("£", "_")
                cut_next_inversed = cut_next_inversed.replace("£", "_")
                sentence_next = sentence_next.replace("£", "_")
                traits_complets_2 = traits_complets_2.replace("£", "_")
            else:
                sent_2 = traits_sentences_depart
                traits_sentences_depart, renamed_data = rename_traits_values(sent_1, sent_2, kept_name, un_kept)
                if traits_complets_1 == traits_complets_2:
                    del renamed_data[traits_complets_1.split("￨")[0]]
                dones = sorted(list(renamed_data.keys()), key=lambda x:len(x), reverse=True)
                dones_args = [a for a in dones if "arg" in a]
                dones_vent = [a for a in dones if a not in dones_args]
                for key in renamed_data.keys():
                    if key in dones_args:
                        renamed_data[key] = renamed_data[key].replace("_", "£")
                i_get_out = 0
                for dones in (dones_args, dones_vent):
                    while len(dones) > 0:
                        if i_get_out == 25:
                            break
                        for old_etiquette in dones:
                            if i_get_out == 25:
                                break
                            i_get_out += 1
                            if renamed_data[old_etiquette] not in sentence:
                                dones.remove(old_etiquette)
                                cut = cut.replace(old_etiquette, renamed_data[old_etiquette])
                                cut_depart = cut_depart.replace(old_etiquette, renamed_data[old_etiquette])
                                cut_depart_inversed = cut_depart_inversed.replace(old_etiquette, renamed_data[old_etiquette])
                                sentence = sentence.replace(old_etiquette, renamed_data[old_etiquette])
                                sentence_depart = sentence_depart.replace(old_etiquette, renamed_data[old_etiquette])
                                traits_complets = traits_complets.replace(old_etiquette, renamed_data[old_etiquette])
                                if old_etiquette.startswith("event") :
                                    used_verbs[renamed_data[old_etiquette]] = used_verbs[old_etiquette]
                                    del used_verbs[old_etiquette]
                                break # nécessaire pour l'ordonnancement, sinon le +1 est yieldé
                            elif renamed_data[old_etiquette] == old_etiquette:
                                dones.remove(old_etiquette)
                                break
                            else:
                                replaced_char = "£" if "arg" in  old_etiquette else "_"
                                renamed_data[renamed_data[old_etiquette].replace(replaced_char, "%")] = renamed_data[old_etiquette]
                                renamed_data[old_etiquette] = renamed_data[old_etiquette].replace(replaced_char, "%")
                                dones.append(renamed_data[old_etiquette].replace(replaced_char, "%"))
                                break

                if i_get_out == 20:
                    continue
                cut = cut.replace("£", "_")
                cut_depart = cut_depart.replace("£", "_")
                cut_depart_inversed = cut_depart_inversed.replace("£", "_")
                sentence = sentence.replace("£", "_")
                sentence_depart = sentence_depart.replace("£", "_")
                traits_complets = traits_complets.replace("£", "_")

            traits_sentences_next[-1]._dep = traits_sentences_depart[-1]._dep
            traits_sentences_depart[-1]._value = random.choice((",", ';'))
            traits_sentences_next[0]._value = traits_sentences_next[0]._value.lower()
            k = len(traits_sentences_depart)
            for i_traits in range(len(traits_sentences_next)-1):
                if traits_sentences_next[i_traits]._dep.isdigit():
                    traits_sentences_next[i_traits]._dep = (int(traits_sentences_next[i_traits]._dep) +k).__str__()
                if traits_sentences_next[i_traits]._arg_0.isdigit():
                    traits_sentences_next[i_traits]._arg_0 = (int(traits_sentences_next[i_traits]._arg_0) + k).__str__()
                if traits_sentences_next[i_traits]._arg_1.isdigit():
                    traits_sentences_next[i_traits]._arg_1 = (int(traits_sentences_next[i_traits]._arg_1) + k).__str__()
                if traits_sentences_next[i_traits]._arg_2.isdigit():
                    traits_sentences_next[i_traits]._arg_2 = (int(traits_sentences_next[i_traits]._arg_2) + k).__str__()
                if traits_sentences_next[i_traits]._arg_3.isdigit():
                    traits_sentences_next[i_traits]._arg_3 = (int(traits_sentences_next[i_traits]._arg_3) + k).__str__()

            if sentence_depart.endswith("?") and not sentence_next.endswith("?"):
                tmp = sentence_depart
                sentence_depart = sentence_next
                sentence_next = tmp
                tmp = traits_sentences_depart.copy()
                traits_sentences_depart = traits_sentences_next.copy()
                traits_sentences_next = tmp.copy()

            ins_.append(sentence_depart + " " + sentence_next)

            outs_.append([" ".join(k.getTrait().lower() for k in traits_sentences_depart + traits_sentences_next), "%coordination%"])
            if random.choice([1, 2]) == 1:
                outs_[-1][0] = outs_[-1][0].replace(';', ".")
            traits_complets_2 = Trait(traits_complets_2)
            traits_complets = Trait(traits_complets)

            old_id_toks = {}
            for i_t, _ in enumerate(traits_sentences_depart):
                old_id_toks[(i_t+1).__str__()] = -1
            old_id_toks_2 = {}
            cut_2_values = cut_2.split(" ")[0:-1]
            for i_t, _ in enumerate(traits_sentences_next):
                old_id_toks_2[(i_t+1).__str__()] = -1

            old_dep_punct = len(traits_sentences_next).__str__()
            diff_next_new = len(traits_sentences_next) - len(cut_2_values)

            if traits_complets_2 == traits_complets:#nous sommes sur du pronom
                a=1
            elif traits_args_2 == traits_args:
                a=1
            def reformule(value_cut):
                if "￨" in value_cut:
                    if "tu￨pron" in value_cut:
                        value_cut = value_cut.replace("tu￨pron", "toi￨pron")
                    if "je￨pron" in value_cut or "j'￨pron" in cut_inversed:
                        value_cut = value_cut.replace("je￨pron", "moi￨pron")
                        value_cut = value_cut.replace("j'￨pron", "moi￨pron")
                    if "ils￨pron" in value_cut:
                        value_cut = value_cut.replace("ils￨pron", "eux￨pron")
                    if "il￨pron" in value_cut:
                        value_cut = value_cut.replace("il￨pron", "lui￨pron")
                else:
                    for value, new_value in zip(("tu", "je", "j'", "il", "ils"), ("toi", "moi", "moi", "lui", "eux")):
                        if value_cut.startswith(value+ " "):
                            if value == "ils":
                                value_cut = new_value + value_cut[3:]
                            else:
                                value_cut = new_value + value_cut[2:]
                        if value_cut == value:
                            value_cut = new_value
                        if " " + value + " " in value_cut:
                            value_cut = value_cut.replace(" " + value + " " , " " + new_value + " " )
                return value_cut

            if ("humain" in type_arg_sent_prem and "humain" in type_arg_sent_sec) or ("animal" in type_arg_sent_prem and "animal" in type_arg_sent_sec) or ("chose" in type_arg_sent_prem and "chose" in type_arg_sent_sec) :

                traits_qui._dep = token_id # --> quand sujet était token_id_2 sujet sera token_id
                traits_qui._person = traits_complets._person
                traits_qui._number = traits_complets._number
                len_start = (" ".join((cut_depart_inversed, "qui"))).count(" ") + 1
                cut_depart_inversed = reformule(cut_depart_inversed)
                cut_inversed = reformule(cut_inversed)
                if "on￨pron"  in cut_depart_inversed :
                    continue

                if level == "end":
                    nouveaux_traits = " ".join((cut, *cut_inversed.split(" ")[0:-1], traits_qui.getTrait(), *cut_2_values, cut_inversed.split(" ")[-1]))
                else:
                    nouveaux_traits = " ".join((cut_inversed, traits_qui.getTrait(), *cut_2_values, cut))

                nouveaux_traits_instanciés = [Trait(word) for word in nouveaux_traits.split()]
                sentence_built = " ".join([k.value for k in nouveaux_traits_instanciés])

                new_dep_punct = (len(nouveaux_traits_instanciés)).__str__()
                if level == "start":
                    for i_l in range(len(nouveaux_traits_instanciés)):
                        if i_l < len_start or i_l >= len_start+len(cut_2_values) :
                            #début
                            for val in ("_dep", "_arg_0", "_arg_1", "_arg_2", "_arg_3"):
                                var = eval("nouveaux_traits_instanciés["+i_l.__str__()+"]."+val)
                                if var.isdigit() and int(var)>=len_start:
                                    result = (int(var) + 1 + len(cut_2_values)).__str__()
                                    if val == "_dep":
                                        nouveaux_traits_instanciés[i_l]._dep = result
                                    elif val == "_arg_0":
                                        nouveaux_traits_instanciés[i_l]._arg_0 = result
                                    elif val == "_arg_1":
                                        nouveaux_traits_instanciés[i_l]._arg_1 = result
                                    elif val == "_arg_2":
                                        nouveaux_traits_instanciés[i_l]._arg_2 = result
                                    elif val == "_arg_3":
                                        nouveaux_traits_instanciés[i_l]._arg_3 = result
                        elif i_l < len_start+len(cut_2_values) :
                            a = "1"
                            for val in ("_dep", "_arg_0", "_arg_1", "_arg_2", "_arg_3"):
                                var = eval("nouveaux_traits_instanciés["+i_l.__str__()+"]."+val)
                                result = None
                                if var == token_id_2:
                                    result = token_id
                                elif var == old_dep_punct:
                                    result = new_dep_punct
                                elif var.isdigit():
                                    result = (int(var) - len_start + diff_next_new +1 ).__str__()
                                if result is not None:
                                    if val == "_dep":
                                        nouveaux_traits_instanciés[i_l]._dep = result
                                    elif val == "_arg_0":
                                        nouveaux_traits_instanciés[i_l]._arg_0 = result
                                    elif val == "_arg_1":
                                        nouveaux_traits_instanciés[i_l]._arg_1 = result
                                    elif val == "_arg_2":
                                        nouveaux_traits_instanciés[i_l]._arg_2 = result
                                    elif val == "_arg_3":
                                        nouveaux_traits_instanciés[i_l]._arg_3 = result
                            a = "1"
                        else:
                            # la fin
                            a = 1
                else:
                    len_start = len(sentence_next.split())
                    for i_l in range(len(nouveaux_traits_instanciés)):
                        if i_l < len_start: # le qui sera embarqué
                            #début
                            for val in ("_dep", "_arg_0", "_arg_1", "_arg_2", "_arg_3"):
                                var = eval("nouveaux_traits_instanciés["+i_l.__str__()+"]."+val)
                                if var == len_start.__str__():
                                    result = new_dep_punct
                                    if val == "_dep":
                                        nouveaux_traits_instanciés[i_l]._dep = result
                                    elif val == "_arg_0":
                                        nouveaux_traits_instanciés[i_l]._arg_0 = result
                                    elif val == "_arg_1":
                                        nouveaux_traits_instanciés[i_l]._arg_1 = result
                                    elif val == "_arg_2":
                                        nouveaux_traits_instanciés[i_l]._arg_2 = result
                                    elif val == "_arg_3":
                                        nouveaux_traits_instanciés[i_l]._arg_3 = result
                        else:
                            a = "1"
                            for val in ("_dep", "_arg_0", "_arg_1", "_arg_2", "_arg_3"):
                                var = eval("nouveaux_traits_instanciés["+i_l.__str__()+"]."+val)
                                result = None
                                if var == token_id_2:
                                    result = token_id
                                elif var == old_dep_punct:
                                    result = new_dep_punct
                                elif var.isdigit():
                                    result = (int(var) - len_start + diff_next_new +1 ).__str__()
                                if result is not None:
                                    if val == "_dep":
                                        nouveaux_traits_instanciés[i_l]._dep = result
                                    elif val == "_arg_0":
                                        nouveaux_traits_instanciés[i_l]._arg_0 = result
                                    elif val == "_arg_1":
                                        nouveaux_traits_instanciés[i_l]._arg_1 = result
                                    elif val == "_arg_2":
                                        nouveaux_traits_instanciés[i_l]._arg_2 = result
                                    elif val == "_arg_3":
                                        nouveaux_traits_instanciés[i_l]._arg_3 = result
                            a = "1"
                a = 1
                ins_.append(sentence_built)

                outs_.append((" ".join(k.getTrait() for k in nouveaux_traits_instanciés), "%relativeSujet%"))
            else:
                a=1
                pass #todo do it
                #raise Exception


            if prep_2 == "" :
                if level == "end":
                    for verb in used_verbs.keys():
                        if token_id == used_verbs[verb]["objet"] and 'voice:act' == used_verbs[verb]["voice"]:
                            if ("chose" in type_arg_sent_prem and "chose" in  type_arg_sent_sec) or ("humain" in type_arg_sent_prem and "humain" in  type_arg_sent_sec) or ("animal" in type_arg_sent_prem and "animal" in  type_arg_sent_sec):
                                if cut_depart_inversed[-1] != "?":

                                    starting_part = []
                                    verbalized_t = []
                                    punct_id_start = (len(traits_sentences_depart)).__str__()
                                    punct_id_start_2 = (len(traits_sentences_next)).__str__()
                                    complément = [Trait(k) for k in cut_2.split(" ")]

                                    trait_starts = cut_2_inversed.split()
                                    trait_starts = [Trait(l.strip()) for l in trait_starts]
                                    old_punct = len(sentence.split())
                                    for i_start, trait in enumerate([Trait(word) for word in cut.split()]):
                                        if trait._dep == punct_id_start:
                                            trait._dep = "k"

                                        if trait._dep == token_id:
                                            trait._dep = token_id_2
                                        elif trait._dep.isdigit():
                                            trait._dep = (int(trait._dep) + 1 + len(trait_starts)).__str__()
                                        if trait._arg_0 == token_id:
                                            trait._arg_0 = token_id_2
                                        elif trait._arg_0.isdigit():
                                            trait._arg_0 = (int(trait._arg_0) + 1 + len(trait_starts)).__str__()
                                        if trait._arg_1 == token_id:
                                            trait._arg_1 = token_id_2
                                        elif trait._arg_1.isdigit():
                                            trait._arg_1 = (int(trait._arg_1) + 1 + len(trait_starts)).__str__()
                                        if trait._arg_2 == token_id:
                                            trait._arg_2 = token_id_2
                                        elif trait._arg_2.isdigit():
                                            trait._arg_2 = (int(trait._arg_2) + 1 + len(trait_starts)).__str__()
                                        if trait._arg_3 == token_id:
                                            trait._arg_3 = token_id_2
                                        elif trait._arg_3.isdigit():
                                            trait._arg_3 = (int(trait._arg_3) + 1 + len(trait_starts)).__str__()
                                        starting_part.append(trait)
                                        verbalized_t.append(trait.value)
                                        if (i_start + 1).__str__() == token_id:
                                            break

                                    for traitComplément in complément + trait_starts:
                                        if traitComplément._dep.isdigit() and int(traitComplément._dep) > int(token_id_2):
                                            traitComplément._dep = (
                                                        int(traitComplément._dep) + 1 + len(starting_part)).__str__()
                                        if traitComplément._arg_0.isdigit() and int(traitComplément._arg_0) > int(
                                                token_id_2):
                                            traitComplément._arg_0 = (
                                                        int(traitComplément._arg_0) + 1 + len(starting_part)).__str__()
                                        if traitComplément._arg_1.isdigit() and int(traitComplément._arg_1) > int(
                                                token_id_2):
                                            traitComplément._arg_1 = (
                                                        int(traitComplément._arg_1) + 1 + len(starting_part)).__str__()
                                        if traitComplément._arg_2.isdigit() and int(traitComplément._arg_2) > int(
                                                token_id_2):
                                            traitComplément._arg_2 = (
                                                        int(traitComplément._arg_2) + 1 + len(starting_part)).__str__()
                                        if traitComplément._arg_3.isdigit() and int(traitComplément._arg_3) > int(
                                                token_id_2):
                                            traitComplément._arg_3 = (
                                                        int(traitComplément._arg_3) + 1 + len(starting_part)).__str__()

                                    traits_que._dep = token_id_2
                                    traits_que._value = "qu'" if cut[0] in "eiaou" and cut[1] != "r" else "que"
                                    new_punct = len([k.getTrait().lower() for k in trait_starts + [
                                        traits_que] + starting_part + complément]).__str__()
                                    for trait in trait_starts:
                                        if trait._dep == punct_id_start_2:
                                            trait._dep = new_punct
                                    for trait in starting_part:
                                        if trait._dep == "k":
                                            trait._dep = new_punct
                                    for trait in complément:
                                        if trait._dep == punct_id_start_2:
                                            trait._dep = new_punct
                                    aux = None
                                    for i_e, element in enumerate(starting_part):
                                        if element._nature == "aux.pass":
                                            aux_ = conjugate(aux._value, INFINITIVE)
                                            if aux_ == "avoir":
                                                aux = element
                                        if element._value == verb and element._mode == "participe" and element._temps == "passé" and aux is not None:
                                            if aux._dep ==  (i_e+1).__str__():
                                                element._gender = traits_complets._gender
                                                element._number = traits_complets._number
                                        elif element._value == verb:
                                            break
                                    ins_.append(" ".join([k.value for k in trait_starts + [traits_que] + starting_part + complément]))
                                    outs_.append((" ".join([k.getTrait().lower() for k in
                                                            trait_starts + [traits_que] + starting_part + complément]),
                                                      "%relativeObjet%"))


            if prep != "":

                text_prep = prep.split('￨')[0]

                fusion = False
                if level == "end" and level_2 == "start":
                    if type_arg_sent_sec == type_arg_sent_prem or  type_arg_sent_prem in type_arg_sent_sec :
                        starting_part = []
                        verbalized_t = []
                        punct_id_start = (len(traits_sentences_depart)).__str__()
                        complément = [Trait(k) for k in cut_2.split(" ")]
                        if complément[0].universal_pos in ('adp', 'prep') or complément[0].nature == "relatif":
                            fusion = True

                        for i_start, trait in enumerate( [Trait(word) for word in sentence.split()]):
                            if trait._dep == punct_id_start:
                                trait._dep = (int(token_id) + len(complément) ).__str__()
                            starting_part.append(trait)
                            verbalized_t.append(trait.value)
                            if (i_start+1).__str__() == token_id:
                                break

                        for traitComplément in complément:
                            if traitComplément._dep == token_id_2:
                                traitComplément._dep = token_id
                            elif  traitComplément._dep.isdigit():
                                traitComplément._dep = (int(traitComplément._dep) - int(token_id) + int(token_id_2)).__str__()
                            if traitComplément._arg_0 == token_id_2:
                                traitComplément._arg_0 = token_id
                            elif  traitComplément._arg_0.isdigit():
                                traitComplément._arg_0 = (int(traitComplément._arg_0) - int(token_id) + int(token_id_2) ).__str__()
                            if traitComplément._arg_1 == token_id_2:
                                traitComplément._arg_1 = token_id
                            elif  traitComplément._arg_1.isdigit():
                                traitComplément._arg_1 = (int(traitComplément._arg_1) - int(token_id) + int(token_id_2) ).__str__()
                            if traitComplément._arg_2 == token_id_2:
                                traitComplément._arg_2 = token_id
                            elif  traitComplément._arg_2.isdigit():
                                traitComplément._arg_2 = (int(traitComplément._arg_2) - int(token_id) + int(token_id_2)).__str__()
                            if traitComplément._arg_3 == token_id_2:
                                traitComplément._arg_3 = token_id
                            elif  traitComplément._arg_3.isdigit():
                                traitComplément._arg_3 = (int(traitComplément._arg_3) - int(token_id) + int(token_id_2) ).__str__()
                        if fusion:
                            ins_.append(" ".join([k.value for k in starting_part + complément]))
                            outs_.append((" ".join([k.getTrait().lower() for k in starting_part + complément]), "%fusion%"))

                        if fusion:
                            continue
                        if cut_depart_inversed[-1] == "?":
                            continue

                        starting_part = []
                        verbalized_t = []
                        punct_id_start = (len(traits_sentences_depart)).__str__()
                        punct_id_start_2 = (len(traits_sentences_next)).__str__()
                        complément = [Trait(k) for k in cut_2.split(" ")]

                        trait_starts = cut_2_inversed.split()
                        trait_starts = [Trait(l.strip()) for l in trait_starts]
                        old_punct = len(sentence.split())
                        for i_start, trait in enumerate( [Trait(word) for word in cut.split()]):
                            if trait._dep == punct_id_start:
                                trait._dep ="k"

                            if trait._dep == token_id:
                                trait._dep = token_id_2
                            elif  trait._dep.isdigit():
                                trait._dep = (int( trait._dep) + 1 + len(trait_starts)).__str__()
                            if trait._arg_0 == token_id:
                                trait._arg_0 = token_id_2
                            elif  trait._arg_0.isdigit():
                                trait._arg_0 = (int( trait._arg_0) + 1 + len(trait_starts)).__str__()
                            if trait._arg_1 == token_id:
                                trait._arg_1 = token_id_2
                            elif  trait._arg_1.isdigit():
                                trait._arg_1 = (int( trait._arg_1) + 1 + len(trait_starts)).__str__()
                            if trait._arg_2 == token_id:
                                trait._arg_2 = token_id_2
                            elif  trait._arg_2.isdigit():
                                trait._arg_2 = (int( trait._arg_2) + 1 + len(trait_starts)).__str__()
                            if trait._arg_3 == token_id:
                                trait._arg_3 = token_id_2
                            elif  trait._arg_3.isdigit():
                                trait._arg_3 =(int( trait._arg_3) + 1 + len(trait_starts)).__str__()
                            starting_part.append(trait)
                            verbalized_t.append(trait.value)
                            if (i_start+1).__str__() == token_id:
                                break


                        for traitComplément in complément + trait_starts:
                            if traitComplément._dep.isdigit() and int(traitComplément._dep) > int(token_id_2):
                                traitComplément._dep = (int(traitComplément._dep) + 1 + len(starting_part) ).__str__()
                            if traitComplément._arg_0.isdigit() and int(traitComplément._arg_0 ) > int(token_id_2):
                                traitComplément._arg_0 = (int(traitComplément._arg_0)  + 1 + len(starting_part) ).__str__()
                            if traitComplément._arg_1.isdigit() and int(traitComplément._arg_1 ) > int(token_id_2):
                                traitComplément._arg_1 = (int(traitComplément._arg_1)  + 1 + len(starting_part)).__str__()
                            if traitComplément._arg_2.isdigit() and int(traitComplément._arg_2 ) > int(token_id_2):
                                traitComplément._arg_2 = (int(traitComplément._arg_2)  + 1 + len(starting_part)).__str__()
                            if traitComplément._arg_3.isdigit() and int(traitComplément._arg_3) > int(token_id_2):
                                traitComplément._arg_3 = (int(traitComplément._arg_3)  + 1 + len(starting_part)).__str__()

                        if text_prep == "sous":
                            a =1
                            pass
                        elif text_prep in ("de", 'des'):
                            traits_dont._dep = token_id_2
                            new_punct = len([k.getTrait().lower() for k in trait_starts + [traits_dont] + starting_part + complément]).__str__()
                            for trait in trait_starts:
                                if trait._dep == punct_id_start_2:
                                    trait._dep = new_punct
                            for trait in starting_part:
                                if trait._dep == "k":
                                    trait._dep = new_punct
                            for trait in complément:
                                if trait._dep == punct_id_start_2:
                                    trait._dep = new_punct
                            ins_.append(" ".join([k.value for k in trait_starts + [traits_dont] + starting_part + complément]))
                            outs_.append((" ".join([k.getTrait().lower() for k in trait_starts + [traits_dont] + starting_part + complément]), "%dont%"))
                        elif text_prep in ("sur", "vers"):
                            a=1
                            pass
                        else:
                            a = 2
                            pass #raise Exception


            if len(outs_) > 1:
                for a, b in itertools.permutations(outs_, 2):
                    in_var.write("%variation% " + a[0].lower() + "\n")
                    out_var.write(b[1] + " " + b[0].lower()+ "\n")