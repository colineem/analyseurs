#!/usr/bin/env python3
# coding=utf-8
i_version = 6
ressources_adress = "https://test.pypi.org/simple/"

import importlib
import sys
import json
import os
from tqdm import tqdm
import random
import glob
import tracemalloc
import linecache

__version__ = 1.09

i_sent = 0
clé = 0

def display_top(snapshot, key_type='lineno', limit=10):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))

tracemalloc.start()

# les analyses discordantes amènent à de gros soucis sur les passif composés
event_1_allowed_times_passif = {'conditionnel': ['présent'],
 'indicatif': ['futur',
               'imparfait',
               'passé-simple',
               'présent']}

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481

loader = importlib.find_loader('pip')
if loader is None:
    print("please install pip", file=sys.stderr)
    exit(1)

loader = importlib.find_loader('ressources')

if loader is None:

    print("please install ressources", file=sys.stderr)
    print(" sudo -H pip" + sys.version_info.major.__str__() + "."+  sys.version_info.minor.__str__() +  " install -i "+ressources_adress + " ressources ")
    print(" sudo -H pip" + sys.version_info.major.__str__() +  " install -i "+ressources_adress + " ressources ")
    exit(1)

import sqlite3
from ressources import *
import argparse

# check the version
def __check_version__():
    return # nothing yet
    __version_min__ = "0.0.152"
    for version_ref, version in zip(__version_min__.split("."), ressources.__version__.split(".")):
        if int(version_ref)<int(version):
            print("please update ressources", file=sys.stderr)
            print(" sudo -H pip" + sys.version_info.major.__str__() + "."+  sys.version_info.minor.__str__() +  " install -i "+ressources_adress + " ressources -U ")
            print(" sudo -H pip" + sys.version_info.major.__str__() +  " install -i "+ressources_adress + " ressources -U ")
            exit(1)

__database__ = "../../SQLProject/consolidated_args_buildCoNLL-U-sqlite-anonymise_accord-elision-full-reveal_adverbe-with_traits-without_contraintes-without_structurev 0.9.107.db"
__database__ = "../../SQLProject/consolidated_args_buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structurev 0.9.107.db"
__database__ = "../../SQLProject/soft.db"
__database_ref__ = "soft"


in_features = []

__ref_dir__ = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/data"
__ref_dir__ = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/data"

def loadFeaturesIn():
    global __ref_dir__, in_features
    fichiers = sorted(glob.glob(__ref_dir__ + '/test.source_feature_*.*'), key=lambda fichier:int(fichier.split("_")[-1].split(".")[0]))
    for fichier in fichiers:
        in_features.append(set())
        for line in open(fichier):
            if line.startswith(" "):
                in_features[-1].add("")
            else:
                in_features[-1].add(line.split()[0])
loadFeaturesIn()


class Word_Instance():

    def __init_(self, id):
        self._lemma = {}
        pass

class Traits():

    SEP_TRAITS = "￨"
    CLEAR_TRAITS = "￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨"
    def __init__(self, long_traits, shorted_traits=None):
        self._traits = long_traits.split(Traits.SEP_TRAITS)
        if not shorted_traits is None:
            self._shorted_traits = shorted_traits.split(Traits.SEP_TRAITS)
        assert shorted_traits is None or len(self._shorted_traits) == 16
        if self.UPOS != UPOS.verb:
            self.code_type = "0"
        self._type_arg_0 = None
        self._type_arg_1 = None
        self._type_arg_2 = None
        self._type_arg_3 = None
        self._gender = None
        self._person = None
        self._number = None

        if not shorted_traits is None and len(self._shorted_traits) == 16:
            self._type_arg_0 = self._shorted_traits[10]
            self._type_arg_1 = self._shorted_traits[11]
            self._type_arg_2 = self._shorted_traits[12]
            self._type_arg_3 = self._shorted_traits[14]
            try:
                if self._shorted_traits[1] is not None:
                    self._gender = Features.Gender.from_name(self._shorted_traits[1].replace(":", "=")) # hidden --> attention, voice.active ou passive : arg_1 ou 0
            except:
                pass

            try:
                if self._shorted_traits[5] is not None:
                    self._person = Features.Person.from_name(self._shorted_traits[5].replace(":", "="))
            except:
                pass

            try:
                if self._shorted_traits[2] is not None:
                    self._number = Features.Number.from_name(self._shorted_traits[2].replace(":", "="))
            except:
                pass

    @property
    def type_arg_0(self):
        return self._type_arg_0

    @property
    def type_arg_1(self):
        return self._type_arg_1

    @property
    def type_arg_2(self):
        return self._type_arg_2

    @property
    def type_arg_3(self):
        return self._type_arg_3

    @property
    def UPOS(self)->UPOS:
        return UPOS.from_name(self._traits[2])

    @UPOS.setter
    def UPOS(self, upos:UPOS):
        assert isinstance(upos, UPOS)
        self._traits[2] = upos.__str__()

    @property
    def polarity(self)->Features.Polarity:
        return Features.Polarity.from_name(self._traits[1])

    @polarity.setter
    def polarity(self, value:Features.Polarity):
        assert isinstance(value, Features.Polarity)
        self._traits[1] = value.__str__()

    @property
    def gender(self)->Features.Gender:
        return Features.Gender.from_name(self._traits[14])

    @gender.setter
    def gender(self, value:Features.Gender):
        if value is None or value == "None":
            value = ""
        assert isinstance(value, Features.Gender) or value == '' or "gender" in value.__str__().lower()
        self._traits[14] = value.__str__()

    @property
    def temps(self)->str:
        return self._traits[5]

    @temps.setter
    def temps(self, value:str):
        assert value in ("", "présent", "0", "passé-composé", "na", "passé-simple", "imparfait", "plus-que-parfait", "passé", "futur", "futur-antérieur", "passé-antérieur")
        self._traits[5] = value.__str__()

    @property
    def temps(self)->str:
        return self._traits[5]

    @temps.setter
    def temps(self, value:str):
        assert value in ("", "présent", "0", "passé-composé", "na", "passé-simple", "imparfait", "plus-que-parfait", "passé", "futur", "futur-antérieur", "passé-antérieur")
        self._traits[5] = value.__str__()
        if value == "na":
            self._mode = "infinitif"
            self._traits[6] = "infinitif" # mode trait 6
        if value in ("0", ''):
            self._traits[6] = value

    @property
    def mode(self)->str:
        return self._traits[6]

    @mode.setter
    def mode(self, value:str):
        assert value in ("", "indicatif", "0", "infinitif", "conditionnel", "participe", "subjonctif")
        if value == "infinitif":
            self._temps = "na"
            # update 201908231045
            self._traits[5] = "na" # temps trait 5
        self._traits[6] = value.__str__()
        if value in ("0", ''):
            self._traits[5] = value

    @property
    def number(self)->Features.Number:
        return Features.Number.from_name(self._traits[15])

    @number.setter
    def number(self, value:Features.Number):
        if value == 'None':
            value = ""
        assert isinstance(value, Features.Number) or value == '' or "number" in value.__str__().lower()
        self._traits[15] = value.__str__()

    @property
    def person(self)->Features.Person:
        return Features.Person.from_name(self._traits[16])

    @person.setter
    def person(self, value:Features.Person):
        assert isinstance(value, Features.Person) or value == '' or "person" in value.__str__().lower()
        self._traits[16] = value.__str__()


    @property
    def actualisation(self)->Features.Actualisation:
        return Features.Actualisation.from_name(self._traits[17])

    @actualisation.setter
    def actualisation(self, value:Features.Actualisation):
        assert isinstance(value, Features.Actualisation) or value == '' or "actualisation" in value.__str__().lower()
        self._traits[17] = value.__str__()


    @property
    def masse(self)->Features.Masse:
        return Features.Masse.from_name(self._traits[18])

    @masse.setter
    def masse(self, value:Features.Masse):
        assert isinstance(value, Features.Masse) or value == '' or "masse" in value.__str__().lower()
        self._traits[18] = value.__str__()

    @property
    def voice(self)->Features.Voice:
        return Features.Voice.from_name(self._traits[1])

    @voice.setter
    def voice(self, value:Features.Voice):
        assert isinstance(value, Features.Voice)
        self._traits[13] = value.__str__()

    @property
    def auxiliaire(self)->str:
        return self._traits[12]

    @auxiliaire.setter
    def auxiliaire(self, value:str):
        value = value.replace(" ", "-")
        assert value in ("avoir-(sauf-si-pronominal-ou-entrée-en-être)", "être","être-ou-avoir", '')
        self._traits[12] = value.__str__()

    @property
    def code_type(self)->str:
        return self._traits[7]

    @code_type.setter
    def code_type(self, value:str)->str:
        if self.UPOS == UPOS.verb:
            assert value in ("A", "N", "T", '0', 'Z')
        else :
            assert value in ("", "0") #vide pour parenthèses et complémentation prédicat, zéro pour items non verbaux
        self._traits[7] = value.__str__()

    @property
    def sujet(self)->str:
        return self._traits[8]

    @sujet.setter
    def sujet(self, value:str)->str:
        assert isinstance(value, str)
        assert len(value) == 1 or value == ""
        if self.UPOS != UPOS.verb:
            assert value in ("0", '')
        self._traits[8] = value.__str__()

    @property
    def objet(self)->str:
        return self._traits[9]

    @objet.setter
    def objet(self, value:str)->str:
        assert isinstance(value, str)
        assert len(value) == 1 or value == "na" or value == ""
        if self.UPOS != UPOS.verb:
            assert value in ("0", '')
        self._traits[9] = value.__str__()

    @property
    def préposition(self)->str:
        return self._traits[10]

    @préposition.setter
    def préposition(self, value:str)->str:
        assert isinstance(value, str)
        assert len(value) == 1 or value == "na" or value == ""
        if self.UPOS != UPOS.verb:
            assert value in ("0", '')
        self._traits[10] = value.__str__()

    @property
    def circonstant(self)->str:
        return self._traits[11]

    @circonstant.setter
    def circonstant(self, value:str)->str:
        assert isinstance(value, str)
        assert len(value) == 1 or value == "na" or value == ""
        if self.UPOS != UPOS.verb:
            assert value in ("0", '')
        self._traits[11] = value.__str__()

    @property
    def traits(self)->str:
        return Traits.SEP_TRAITS.join(self._traits)

    @traits.setter
    def traits(self, long_traits: str):
        self._traits = long_traits.split(Traits.SEP_TRAITS)

class Conjugaison:

    def __init__(self, short=False, subordonnée=False):
        self._subordonnée = subordonnée
        if short:
            self._tab = (("indicatif", "présent"), )
        else:
            self._tab = [("indicatif", "présent" ), ("indicatif", "passé-composé"), ("indicatif", "imparfait"),
                     ("indicatif", "plus-que-parfait"), ("indicatif", "passé-simple"), ("indicatif", "passé-antérieur"),
                     ("indicatif", "futur"), ("indicatif", "futur-antérieur"), ("conditionnel", "présent"),
                     ("conditionnel", "passé"), ("subjonctif", "présent"), ("subjonctif", "passé"),
                     ("subjonctif", "imparfait"), ("subjonctif", "plus-que-parfait")]
            random.shuffle(self._tab)

    def next(self):
        for mode, temps in self._tab:
            if self._subordonnée or (mode != "subjonctif"):
                yield mode, temps
        raise StopIteration

    def choice(self):
        stop = False
        i = 0
        while not stop:
            mode, temps = random.choice(self._tab)
            if self._subordonnée or (mode != "subjonctif"):
                yield mode, temps
                if i == 1:
                    stop = True
                i += 1
        raise StopIteration

    def all_variations(self):
        for mode, temps in self._tab:
            yield mode, temps
        raise StopIteration

traits_args_disponibles = ["e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Indefini￨￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨possession:0",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨possession:1",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨possession:2",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨possession:3",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "e￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨￨￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨￨￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨￨￨",
                           "e￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨￨￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "x￨0￨NOUN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:2￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:2￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:1￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:1￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨￨￨",
                           "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨￨￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "x￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:3￨￨￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:0",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:1",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:2",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Defini￨masse:Totale￨possession:3",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨masse:Totale￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨actualisation:Indefini￨￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:3￨￨￨",
                           "e￨0￨PROPN￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:3￨actualisation:Indefini￨masse:Partielle￨",]

traits_args_disponibles = set(traits_args_disponibles)

lib_traits = []
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:2￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(8):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:2￨￨￨")
for i_trait in range(26):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:1￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(5):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:1￨￨￨")
for i_trait in range(2):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Sing￨person:2￨￨￨")
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Plur￨person:2￨￨￨")
for i_trait in range(3):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:1￨￨￨")
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:2￨￨￨")
for i_trait in range(19):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Plur￨person:1￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(max(15180, 15071)):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Plur￨person:1￨￨￨")
for i_trait in range(7):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Plur￨person:2￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(4):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Plur￨person:2￨actualisation:Indefini￨masse:Partielle￨")
for i_trait in range(15180):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Plur￨person:2￨￨￨")
for i_trait in range(13):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨actualisation:Defini￨masse:Totale￨possession:0")
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨actualisation:Defini￨masse:Totale￨possession:1")
for i_trait in range(3):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨actualisation:Indefini￨masse:Partielle￨")
for i_trait in range(2):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨actualisation:Indefini￨￨")
for i_trait in range(min(15180, 50044)):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨￨￨")
for i_trait in range(12):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:2￨actualisation:Defini￨masse:Totale￨")
for i_trait in range(1):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:2￨actualisation:Indefini￨masse:Partielle￨")
for i_trait in range(max(15180, 5406)):
    lib_traits.append("x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:2￨￨￨")
for i_trait in range(1):
    lib_traits.append("x￨polarity:Neg￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:1￨￨￨")
for i_trait in range(1):
    lib_traits.append("x￨polarity:Neg￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨number:Sing￨person:2￨￨￨")

# simplification:
for trait in list(traits_args_disponibles):
    if "PROPN" in trait or "PRON" in trait:
        if "actualisation" in trait:
            traits_args_disponibles.remove(trait)
        elif "masse" in trait:
            traits_args_disponibles.remove(trait)
        elif "possession" in trait:
            traits_args_disponibles.remove(trait)

for _ in range(1):
    for trait in list(traits_args_disponibles):
        traits_args_disponibles.add(trait.replace("Masc", "Fem"))
    for trait in list(traits_args_disponibles):
        traits_args_disponibles.add(trait.replace("Sing", "Plur"))
    for trait in list(traits_args_disponibles):
        traits_args_disponibles.add(trait.replace("Fem", "Masc"))
    for trait in list(traits_args_disponibles):
        traits_args_disponibles.add(trait.replace("Plur", "Sing"))
traits_args_disponibles = list(traits_args_disponibles)
random.shuffle(traits_args_disponibles)


req_save = {}

def main():
    global  __database__, i_sent, traits_args_disponibles, req_save

    parser = argparse.ArgumentParser(description="Jonction LVF data")

    parser.add_argument('--debug', dest='debug', action='store_true')
    parser.add_argument('--output_file', dest='output_file', type=str, default="train_socle.txt")

    dones_structs = {}
    dones_sents = set()
    not_dones_sents = {}

    args = parser.parse_args()

    if args.debug:
        print(__database__)
        args.output_file = "debug_" + args.output_file

    connexion = sqlite3.connect(Word._database, isolation_level=None)
    connVerbs = sqlite3.connect(__database__ ) # , isolation_level=None désactive l'autocommit
    connVerbs.execute("PRAGMA cache_size=20000")  # Récupération d'un curseur
    connVerbs.execute("PRAGMA temp_store = MEMORY")  # Récupération d'un curseur
    connexion.execute("PRAGMA locking_mode = NORMAL")  # Récupération d'un curseur
    connexion.execute("PRAGMA foreign_keys = 1")  # Récupération d'un curseur

    curseurLVF = connexion.cursor()  # Récupération d'un curseur
    curseurData = connVerbs.cursor()  # Récupération d'un curseur
# Récupération d'un curseur

    # on va updater la table
    req = """SELECT sql FROM sqlite_master WHERE name = 'arguments';"""
    curseurData.execute(req, {})
    rows = curseurData.fetchall()

    if not "suj_short_traits" in rows[0][0]:

        req = """ALTER TABLE arguments ADD suj_short_traits TEXT;"""
        curseurData.execute(req)
        req = """ALTER TABLE arguments ADD obj_short_traits TEXT;"""
        curseurData.execute(req)


        req = """select count(*) from arguments  where suj_short_traits is null  ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        nb = rows[0][0]
        print(nb)
        req = """select count(*) from arguments  where obj_short_traits is null  ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        if rows[0][0] > nb:
            nb = rows[0][0]
        if args.debug:
            print(nb)

        for _ in tqdm(range(int(nb/1000)+2)):
            req = """update arguments set suj_short_traits = (select shorted_traits from argument_details ad 
                    where arguments.sent_id = ad.sent_id and arguments.sujet = ad.item and shorted_traits != '' limit 1) where suj_short_traits is null limit 1000;"""
            curseurData.execute(req)
            req = """update arguments set obj_short_traits = (select shorted_traits from argument_details ad 
                    where arguments.sent_id = ad.sent_id and arguments.objet = ad.item and shorted_traits != '' limit 1) where obj_short_traits is null limit 1000;"""
            curseurData.execute(req)

        req = """CREATE INDEX arguments_suj_short_traits ON arguments (suj_short_traits);"""
        curseurData.execute(req)
        req = """CREATE INDEX arguments_obj_short_traits ON arguments (obj_short_traits);"""
        curseurData.execute(req)

        print("saving tables")
        connVerbs.commit()
        while connVerbs.in_transaction:
            pass

    # on va updater la table
    req = """SELECT sql FROM sqlite_master WHERE name = 'arguments';"""
    curseurData.execute(req, {})
    rows = curseurData.fetchall()

    if not "suj_traits" in rows[0][0]:
        req = """ALTER TABLE arguments ADD suj_traits TEXT;"""
        curseurData.execute(req)
        req = """ALTER TABLE arguments ADD obj_traits TEXT;"""
        curseurData.execute(req)
        print("tables argument process")
        req = """CREATE INDEX arguments_partial_obj_traits ON arguments (obj_traits)  WHERE obj_traits IS NULL;"""
        curseurData.execute(req)
        req = """CREATE INDEX arguments_partial_suj_traits ON arguments (suj_traits) WHERE suj_traits IS NULL;"""
        curseurData.execute(req)
        req = """CREATE INDEX argument_details_sent_item ON argument_details (sent_id, item);"""
        curseurData.execute(req)
        req = "CREATE INDEX argument_details_full ON argument_details (lvf_code, lvf_sujet, lvf_objet, lvf_cp, lvf_circ, item);"
        curseurData.execute(req)
        req = "CREATE INDEX argument_details_shorted ON argument_details (item, shorted_traits);"
        curseurData.execute(req)
        req = "CREATE INDEX argument_details_long ON argument_details (item, long_traits);"
        curseurData.execute(req)
        req = "CREATE INDEX arguments_pred_suj ON arguments (sent_id, prédicat, sujet);"
        curseurData.execute(req, {})

        req = """select count(*) from arguments  where suj_traits is null  ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        nb = rows[0][0]
        print(nb)
        req = """select count(*) from arguments  where obj_traits is null  ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        if rows[0][0] > nb:
            nb = rows[0][0]
        if args.debug:
            print(nb)


        for i_tqdm in tqdm(range(int(nb/1000)+2)):
            req = """update arguments set suj_traits = (select long_traits from argument_details ad 
                    where arguments.sent_id = ad.sent_id and arguments.sujet = ad.item and long_traits != '' limit 1) where suj_traits is null limit 1000;"""
            curseurData.execute(req)
            req = """update arguments set obj_traits = (select long_traits from argument_details ad 
                    where arguments.sent_id = ad.sent_id and arguments.objet = ad.item and long_traits != '' limit 1) where obj_traits is null limit 1000;"""
            curseurData.execute(req)

        req = "CREATE INDEX arguments_obj_traits ON arguments (obj_traits);"
        curseurData.execute(req)
        req = "CREATE INDEX arguments_suj_traits ON arguments (suj_traits);"
        curseurData.execute(req)

        print("saving tables")
        connVerbs.commit()
        while connVerbs.in_transaction:
            pass
    old_nb = 1
    nb = 0
    curseurData.close()
    curseurData = connVerbs.cursor()
    while nb != old_nb:

        old_nb = nb
        req = """select shorted_traits, long_traits, arguments.id from arguments , argument_details 
        where arguments.sent_id = argument_details.sent_id and objet = item and (arguments.obj_traits is null or arguments.obj_short_traits is null ) 
        and argument_details.long_traits is not null  limit 100000;"""

        curseurData.execute(req, {})
        rows = curseurData.fetchall()

        for row in rows:
            req = "update arguments set obj_short_traits = ?, obj_traits = ?  WHERE id = ?"
            curseurData.execute(req, row)

        if len(rows)>0:
            print("saving tables")
            connVerbs.commit()
            while connVerbs.in_transaction:
                pass

        req = """select count(*) from arguments  where ( obj_traits is null or  obj_short_traits is null )  ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        nb = rows[0][0]
        print(nb)

    old_nb = 0
    nb = 1

    while nb != old_nb:

        old_nb = nb
        req = """select shorted_traits, long_traits, arguments.id from arguments , argument_details where arguments.sent_id = argument_details.sent_id and sujet = item and (arguments.suj_traits is null or arguments.suj_short_traits is null) and argument_details.long_traits is not null  limit 100000;"""

        curseurData.execute(req, {})
        rows = curseurData.fetchall()

        for row in rows:
            req = "update arguments set suj_short_traits = ?, suj_traits = ?  WHERE id = ?"
            curseurData.execute(req, row)

        if len(rows)>0:
            print("saving tables")
            connVerbs.commit()
            while connVerbs.in_transaction:
                pass

        req = """select count(*) from arguments  where (suj_traits is null  or suj_short_traits is null  )   ;"""
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        nb = rows[0][0]
        print(nb)


    nb = 1
    curseurData.close()
    curseurData = connVerbs.cursor()

    connVerbs.close()
    connVerbs = sqlite3.connect(__database__, isolation_level=None)
    connVerbs2 = sqlite3.connect(__database__, isolation_level=None)
    connVerbs.execute("PRAGMA cache_size=20000")  # Récupération d'un curseur
    connVerbs.execute("PRAGMA temp_store = MEMORY")  # Récupération d'un curseur
    connVerbs.execute("PRAGMA cache_size=20000")  # Récupération d'un curseur
    connVerbs.execute("PRAGMA temp_store = MEMORY")  # Récupération d'un curseur

    curseurData = connVerbs.cursor()  # Récupération d'un curseur
    curseurData2 = connVerbs2.cursor()  # Récupération d'un curseur
    curseurData_verb = connVerbs.cursor()

    def gestion_event_annexe(traits_ref, détails_traits, sent_id_ref, sujet_event, sujet_initial, traits_sujet_initial):
        req_verb = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                   "and sujet = :sujet  "

        curseurData_verb.execute(req_verb,
                                 {'sent_id': sent_id_ref,  #,
                                  'sujet': sujet_event})
        info_complémentaire_event = []
        for row in curseurData_verb:
            prédicat_verb = row[1]
            sujet_verb = row[0]
            objet_verb = row[2]
            if objet_verb == "":
                continue
            traits_sujet_verb = Traits.CLEAR_TRAITS[1:] if row[3] is None else row[3]
            traits_objet_verb = Traits.CLEAR_TRAITS[1:] if row[4] is None else row[4]
            if sujet_verb not in détails_traits.keys() or détails_traits[sujet_verb] is None : # pour éviter de réécrire le sujet (à discuter)
                détails_traits[sujet_verb] = traits_sujet_verb
            if objet_verb not in détails_traits.keys() or détails_traits[objet_verb] is None :
                détails_traits[objet_verb] = traits_objet_verb
            if sujet_verb not in traits_ref.keys():
                if row[3] is None:
                    traits_ref[sujet_verb] = sujet_verb
                else:
                    traits_ref[sujet_verb] = "argevent_" + (len([k for k in traits_ref.keys() if k.startswith("argevent_")]) + (1 if "argevent_1" in traits_ref.values() else 2)).__str__()
            if objet_verb not in traits_ref.keys():
                if row[4] is None:
                    traits_ref[objet_verb] = objet_verb
                else:
                    traits_ref[objet_verb] = "argevent_" + (len([k for k in traits_ref.keys() if k.startswith("argevent_")]) + (1 if "argevent_1" in traits_ref.values() else 2)).__str__()
            info_complémentaire_event.append(" ".join(["(" + Traits.CLEAR_TRAITS, traits_ref[sujet_verb]  + Traits.SEP_TRAITS + détails_traits[sujet_verb], prédicat_verb + Traits.CLEAR_TRAITS,
                 traits_ref[objet_verb]  + Traits.SEP_TRAITS + détails_traits[objet_verb] , ")" + Traits.CLEAR_TRAITS]))
        return "" if len(info_complémentaire_event) == 0 else " ", info_complémentaire_event

    if args.debug:
        print(ressources.database)

    _nature = {}
    req = "SELECT code, value FROM lvf_natures;"
    curseurLVF.execute(req, {})
    rows = curseurLVF.fetchall()
    for row in rows:
        _nature[row[0]] = [k.replace(" ", "_") for k in row[1].split(" ou ")]

    _complément = {}
    req = "SELECT code, value FROM lvf_compléments;"
    curseurLVF.execute(req, {})
    rows = curseurLVF.fetchall()
    for row in rows:
        _complément[row[0]] = [k.replace(" ", "_") for k in row[1].split(" ou ")]


    _préposition = {}
    req = "SELECT code, value FROM lvf_prépositions;"
    curseurLVF.execute(req, {})
    rows = curseurLVF.fetchall()
    for row in rows:
        _préposition[row[0]] = [k.replace(" ", "_") for k in row[1].split(" ou ")]

    req = "select count(*), construction, type, sujet, objet, préposition, circonstant, auxiliaire, classe_sem_synt from  lvf group by construction, type, sujet, objet, préposition, circonstant, auxiliaire, classe_sem_synt "
    req = "select count(*), construction, type, sujet, objet, préposition, circonstant, auxiliaire from  lvf group by construction, type, sujet, objet, préposition, circonstant, auxiliaire "
    curseurLVF.execute(req, {})
    rows = curseurLVF.fetchall()
    lvf_data = []
    columns_lvf = ["count" , "construction" , "type" , "sujet" , "objet" , "préposition" , "circonstant" , "auxiliaire"] # , classe_sem_synt]

    for row in rows:
        lvf_data.append({})
        for column_lvf in columns_lvf:
            lvf_data[-1][column_lvf] = column_lvf

    verbs = {}
    ids = {}
    used_id = {}

    sujets_par_structure = {}
    constructions_par_structure = {}
    if os.path.exists('verbs_' + __database_ref__ + '_.json'):
        print("load verbs")
        a = open('verbs_' + __database_ref__ + '_.json', "r")
        verbs, used_id = json.load(a)
        if args.debug:
            print("verbs loaded")
    else:
        def save_data(rows, construction, auxiliaire, where=verbs, what=ids, step=0, used=used_id):
            for row in rows:
                sent_id = row[0]
                item = row[1]
                form = row[2]
                place = row[3]
                long_traits = row[4]
                if long_traits is None:
                    continue
                shorted_traits = row[5]
                elision = row[6]
                id = row[7]
                if auxiliaire is None:
                    auxiliaire = ''
                if construction not in ids.keys():
                    ids[construction] = {}
                if auxiliaire not in ids[construction].keys():
                    ids[construction][auxiliaire] = set()

                if auxiliaire == '':
                    if "participe" in shorted_traits and not ("passé￨participe" in long_traits or "présent￨participe" in long_traits):
                        continue
                    if auxiliaire == '':

                        if not 'avoir-(sauf-si-pronominal-ou-entrée-en-être)' in long_traits:
                            continue
                        if not "voice:Act"  in long_traits:
                            continue
                        if not "infinitif" in long_traits:
                            if not "gender:Masc"  in shorted_traits:
                                continue
                            if not "number:Sing"  in shorted_traits:
                                continue
                            if not "person:3"  in shorted_traits:
                                continue

                elif 'avoir (sauf si pronominal ou entrée en être)' == auxiliaire and not 'avoir-(sauf-si-pronominal-ou-entrée-en-être)' in long_traits:
                    continue

                elif auxiliaire=="être":
                    take = True
                    for value in ('￨être-ou-avoir￨', '￨avoir-(sauf-si-pronominal-ou-entrée-en-être),être-ou-avoir￨', "￨avoir-(sauf-si-pronominal-ou-entrée-en-être)￨"):
                        if value in long_traits:
                            take = False
                            break
                    if not take:
                        continue
                elif auxiliaire=='être ou avoir':
                    take = True
                    for value in ("￨avoir-(sauf-si-pronominal-ou-entrée-en-être)￨", "￨être￨", "￨avoir-(sauf-si-pronominal-ou-entrée-en-être),être￨",):
                        if value in long_traits:
                            take = False
                            break
                    if not take:
                        continue
                if id not in ids[construction][auxiliaire] :
                    ids[construction][auxiliaire].add(id)
                    used[id] = {'sent_id': sent_id, 'item': item, 'form': form, 'place': place,
                                'long_traits': long_traits, 'shorted_traits': shorted_traits,
                                'elision': elision}
                    where[construction][auxiliaire].append((id, step))

        def make_verbs(nombre, construction, auxiliaire, type_verbe, sujet, objet, préposition, circonstant, verbs=verbs, step=0):
            type_verbe = construction[0]
            if sujet == "":
                return
            if objet in ("", "0"):
                objet = None
            if préposition == "":
                préposition = None
            if circonstant == "":
                circonstant = None

            req = "select " \
                  "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                  "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                  "from argument_details ad_1 " \
                  " where lvf_code IS :type_verbe and lvf_sujet IS :sujet " \
                  "and  lvf_objet IS :objet " \
                  "and  lvf_cp IS :préposition " \
                  "and  lvf_circ IS :circonstant "
            if sujet == "7":
                req = req.replace("lvf_sujet IS :sujet", " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
            if sujet == "8":
                req = req.replace("lvf_sujet IS :sujet", " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

            dictionnaire = {"type_verbe":type_verbe, "sujet":sujet, "objet":objet, "préposition":préposition, "circonstant":circonstant, }
            curseurData.execute(req, dictionnaire)
            rows = curseurData.fetchall()
            save_data(rows, construction, auxiliaire, step=step)

            reqSuj = ""
            reqObj = ""


            for item in _nature[sujet]:
                reqSuj += "or ((type_arg_0 = '" + item + "'"
                reqSuj += " or type_arg_0 like '%/" + item + "'"
                reqSuj += " or type_arg_0 like '" + item + "/%'"
                reqSuj += " or type_arg_0 like '%/" + item + "/%') and lvf_sujet = 'Z') "
            if objet is not None:
                for item in _nature[sujet]:
                    reqObj += "or ((type_arg_1 = '" + item + "'"
                    reqObj += " or type_arg_1 like '%/" + item + "'"
                    reqObj += " or type_arg_1 like '" + item + "/%'"
                    reqObj += " or type_arg_1 like '%/" + item + "/%') and lvf_objet = 'Z') "

            req = "select " \
                  "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                  "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                  "from argument_details ad_1 " \
                  " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                  "and  (lvf_objet IS :objet " + reqObj +") " \
                  "and  lvf_cp IS :préposition " \
                  "and  lvf_circ IS :circonstant "

            if sujet == "7":
                req = req.replace("lvf_sujet IS :sujet", " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
            if sujet == "8":
                req = req.replace("lvf_sujet IS :sujet", " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

            dictionnaire = {"type_verbe":type_verbe, "sujet":sujet, "objet":objet, "préposition":préposition, "circonstant":circonstant, }
            curseurData.execute(req, dictionnaire)
            rows = curseurData.fetchall()
            save_data(rows, construction, auxiliaire,step=step)

            if circonstant is None and préposition is None:
                req = "select " \
                      "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                      "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                      "from argument_details ad_1 " \
                      " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                      "and  (lvf_objet IS :objet " + reqObj +") " \
                      "and  lvf_cp IS null " \
                      "and  lvf_circ IS null"
                if sujet == "7":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                if sujet == "8":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")
                dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet,
                               }
                curseurData.execute(req, dictionnaire)
                rows = curseurData.fetchall()
                save_data(rows, construction, auxiliaire,step=step)
                if len(verbs[construction]) == 0:
                    req = "select " \
                          "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                          "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                          "from argument_details ad_1 " \
                          " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                          "and  (lvf_objet IS :objet " + reqObj +") " \
                          "and  lvf_cp IS null and type_arg_2 is null "
                    if sujet == "7":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                    if sujet == "8":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

                    dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet,
                                   }
                    curseurData.execute(req, dictionnaire)
                    rows = curseurData.fetchall()
                    save_data(rows, construction, auxiliaire,step=step)

            if circonstant is not None and préposition is None:

                liste_natures = []
                if circonstant in _complément.keys():
                    vars_circonstant = _complément[circonstant]
                elif circonstant in _préposition.keys():
                    vars_circonstant = _préposition[circonstant]
                for item in vars_circonstant:
                    liste_natures.append('(')
                    tmp = "(type_arg_3 = '" + item + "'"
                    sep = " or "
                    tmp += " or type_arg_3 like '%/" + item + "'"
                    tmp += " or type_arg_3 like '" + item + "/%'"
                    tmp += " or type_arg_3 like '%/" + item + "/%')"

                    liste_natures[-1] += "("+ tmp + ' and type_arg_2 is null))'
                liste_natures = " or ".join(liste_natures)

                req = "select " \
                      "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                      "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                      "from argument_details ad_1 " \
                      " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                      "and  (lvf_objet IS :objet " + reqObj +") " \
                      "and " + liste_natures

                if sujet == "7":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                if sujet == "8":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")
                dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet }
                curseurData.execute(req, dictionnaire)
                rows = curseurData.fetchall()
                save_data(rows, construction, auxiliaire,step=step)

            if préposition is not None and circonstant is None:

                liste_natures = []
                if préposition in _complément.keys():
                    vars_préposition = _complément[préposition]
                elif préposition in _préposition.keys():
                    vars_préposition = _préposition[préposition]
                for item in vars_préposition:
                    liste_natures.append('(')
                    tmp = "(type_arg_2 = '" + item + "'"
                    sep = " or "
                    tmp += " or type_arg_2 like '%/" + item + "'"
                    tmp += " or type_arg_2 like '" + item + "/%'"
                    tmp += " or type_arg_2 like '%/" + item + "/%')"

                    liste_natures[-1] += "("+ tmp + ' and type_arg_3 is null))'
                liste_natures = " or ".join(liste_natures)


                req = "select " \
                      "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                      "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                      "from argument_details ad_1 " \
                      " where lvf_code IS :type_verbe and lvf_sujet IS :sujet " \
                      "and  lvf_objet IS :objet " \
                      "and " + liste_natures
                if sujet == "7":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                if sujet == "8":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

                dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet }
                curseurData.execute(req, dictionnaire)
                rows = curseurData.fetchall()
                save_data(rows, construction, auxiliaire,step=step)

            if préposition is None and not circonstant and len(verbs[construction])==0:
                req = "select " \
                      "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                      "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                      "from argument_details ad_1 " \
                      " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                      "and  (lvf_objet IS :objet " + reqObj +") " \
                      "and  lvf_cp IS :préposition " \
                      "and  lvf_circ IS null "
                if sujet == "7":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                if sujet == "8":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

                dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet,
                                "préposition": circonstant, }
                curseurData.execute(req, dictionnaire)
                rows = curseurData.fetchall()
                save_data(rows, construction, auxiliaire,step=step)

            if circonstant is not None and préposition is None and len(verbs[construction])==0:
                req = "select " \
                      "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                      "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                      "from argument_details ad_1 " \
                      " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj +") " \
                      "and  (lvf_objet IS :objet " + reqObj +") " \
                      "and  lvf_cp IS null " \
                      "and  lvf_circ IS :circonstant"
                if sujet == "7":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                if sujet == "8":
                    req = req.replace("lvf_sujet IS :sujet",
                                      " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")
                dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet,
                                "circonstant": préposition, }
                curseurData.execute(req, dictionnaire)
                rows = curseurData.fetchall()
                save_data(rows, construction, auxiliaire,step=step)

            if len(verbs[construction])==0:
                if circonstant is not None and préposition is None:

                    liste_natures = []
                    if circonstant in _complément.keys():
                        vars_circonstant = _complément[circonstant]
                    elif circonstant in _préposition.keys():
                        vars_circonstant = _préposition[circonstant]
                    for item in vars_circonstant:
                        liste_natures.append('(')
                        tmp = "(type_arg_3 = '" + item + "'"
                        sep = " or "
                        tmp += " or type_arg_3 like '%/" + item + "'"
                        tmp += " or type_arg_3 like '" + item + "/%'"
                        tmp += " or type_arg_3 like '%/" + item + "/%')"

                        liste_natures[-1] += "(" + tmp + ' and type_arg_2 is null)'
                        liste_natures[-1] += ' or (' + tmp.replace('_3', "_2") + " and type_arg_3 is null ))"
                    liste_natures = " or ".join(liste_natures)

                    req = "select " \
                          "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                          "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                          "from argument_details ad_1 " \
                          " where lvf_code IS :type_verbe and (lvf_sujet IS :sujet " + reqSuj + ") " \
                                                                                                "and  (lvf_objet IS :objet " + reqObj + ") " \
                                                                                                                                        "and " + liste_natures
                    if sujet == "7":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                    if sujet == "8":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

                    dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet}
                    curseurData.execute(req, dictionnaire)
                    rows = curseurData.fetchall()
                    save_data(rows, construction, auxiliaire,step=step)

                if préposition is not None and circonstant is None:

                    liste_natures = []
                    if préposition in _complément.keys():
                        vars_préposition = _complément[préposition]
                    elif préposition in _préposition.keys():
                        vars_préposition = _préposition[préposition]
                    for item in vars_préposition:
                        liste_natures.append('(')
                        tmp = "(type_arg_2 = '" + item + "'"
                        sep = " or "
                        tmp += " or type_arg_2 like '%/" + item + "'"
                        tmp += " or type_arg_2 like '" + item + "/%'"
                        tmp += " or type_arg_2 like '%/" + item + "/%')"

                        liste_natures[-1] += "(" + tmp + ' and type_arg_3 is null)'
                        liste_natures[-1] += ' or (' + tmp.replace('_2', "_3") + " and type_arg_2 is null ))"
                    liste_natures = " or ".join(liste_natures)

                    req = "select " \
                          "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
                          "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, id " \
                          "from argument_details ad_1 " \
                          " where lvf_code IS :type_verbe and lvf_sujet IS :sujet " \
                          "and  lvf_objet IS :objet " \
                          "and " + liste_natures
                    if sujet == "7":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='1'))")
                    if sujet == "8":
                        req = req.replace("lvf_sujet IS :sujet",
                                          " (lvf_sujet IS :sujet or (shorted_traits like '%number:Plur%' and lvf_sujet='3'))")

                    dictionnaire = {"type_verbe": type_verbe, "sujet": sujet, "objet": objet}
                    curseurData.execute(req, dictionnaire)
                    rows = curseurData.fetchall()
                    save_data(rows, construction, auxiliaire,step=step)


        for nombre, construction, type_verbe, sujet, objet, préposition, circonstant, auxiliaire in rows:
            if construction not in verbs.keys():
                verbs[construction] = {}
            verbs[construction][auxiliaire] = []
            print(nombre, end=' dans le lvf pour :')
            print((type_verbe, sujet, objet, préposition, circonstant, auxiliaire), end="\t")

            make_verbs(nombre, construction, auxiliaire, type_verbe, sujet, objet, préposition, circonstant, step=1)
            if len(verbs[construction][auxiliaire]) < 1000:
                combi_sujet = [sujet]
                combi_objet = [objet]

                if sujet in ("1", "2"):
                    combi_sujet = ["1", "2"]
                if objet in ("1", "2"):
                    combi_objet = ["1", "2"]
                if sujet == "4":
                    combi_sujet = ["3", "4"]
                if objet == "4":
                    combi_objet = ["3", "4"]
                if sujet == "9":
                    combi_sujet = ["9", "1", "3"]
                if objet == "9":
                    combi_objet = ["9", "1", "3"]
                step = 1
                for subj in combi_sujet:
                    for obj in combi_objet:
                        if subj == sujet and obj == objet:
                            continue
                        else:
                            step+=1
                            make_verbs(nombre, construction, auxiliaire, type_verbe, subj, obj, préposition, circonstant, step=step)

            print(len(verbs[construction][auxiliaire]), end="\n")

        print(len(ids.keys()))
        print(len(used_id.keys()))

        fjson = open('verbs_' + __database_ref__ + '_.json', "w")
        fjson.write(json.dumps((verbs, used_id), ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        if args.debug:
            print("reload verbs")
        a = open('verbs_' + __database_ref__ + '_.json', "r")
        verbs, used_id = json.load(a)
        if args.debug:
            print("verbs reloaded")


    if os.path.exists('adjs_' + __database_ref__ + '_.json'):
        if args.debug:
            print("load adjs")
        a = open('adjs_' + __database_ref__ + '_.json', "r")
        adjs = json.load(a)
        rows = adjs
        if args.debug:
            print("adjs loaded")
    else:
        req = "select " \
              "ad_1.sent_id, ad_1.item , ad_1.form , ad_1.place , " \
              "ad_1.long_traits, ad_1.shorted_traits, ad_1.elision, ad_1.place, " \
              "ad_2.item , ad_2.form , ad_2.long_traits, ad_2.shorted_traits, ad_2.elision " \
              "from argument_details ad_1 " \
              "left join arguments " \
                "on arguments.sent_id = ad_1.sent_id and (prédicat='modifie' or prédicat like 'impact%' ) and sujet = ad_1.item " \
              "left join argument_details ad_2 " \
                "on ad_1.sent_id = ad_2.sent_id " \
                "and objet = ad_2.item " \
              "where ad_1.item like 'mod%' and ad_1.shorted_traits like 'ADJ￨%' " \
              "and ad_2.shorted_traits like 'NOUN￨%' "

        if args.debug:
            print('load adjs')
        curseurData.execute(req, {})
        rows = curseurData.fetchall()
        if args.debug:
            print('adjs loaded, now saving them')
        fjson = open('adjs_' + __database_ref__ + '_.json', "w")
        fjson.write(json.dumps(rows, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        if args.debug:
            print('adjs saved')

        if args.debug:
            print("reload adjs")
        a = open('adjs_' + __database_ref__ + '_.json', "r")
        adjs = json.load(a)
        rows = adjs
        if args.debug:
            print("adjs reloaded")

       # traitement
    used_verbs_for_structure = {}
    file = open(args.output_file, "w")
    file_flat_json = open(args.output_file.replace(".txt", ".json.txt"), "w")
    file_suivi = open(args.output_file.replace(".txt", ".suivi.txt"), "w")

    data_writed = set()
    def write(input_, new_verb,  verb, sentence_data, round, traits, voice, traits_alternatifs, structure):
        global clé

        if round == 0:
            return False
        traits_du_verbe = traits[:]
        inputs_ = [input_, ]
        if traits_alternatifs[1] is not None:
            inputs_.append(input_.replace(traits_alternatifs[0], traits_alternatifs[1]))
        if len(inputs_) > 1:
            if inputs_[1] == inputs_[0]:
                raise Exception
        for i_in_count, input_ in enumerate(inputs_):
            i_in_count = "DIV" if i_in_count == 0 else "PRON_EXPRESS"
            traits = traits_du_verbe[:]
            ssives_ = {}
            last = ""
            for input_str in input_.lower().strip().split(" "):
                if "voice=pass" in input_str and input_str not in ssives_.keys():
                    if input_str not in ssives_.keys():
                        ssives_[input_str] = False if input_str.startswith("event") else last
                if last in ssives_.keys() and input_str == 'arg_1￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨':
                    ssives_[last] = True
                last = input_str if input_str.startswith("event") else last

            if False in ssives_.values():
                input_ = input_.lower().replace("=", ":").replace("voice:pass", "voice:act")
                voice = [Features.Voice.ACTIVE]

            # rafistolage suite à modif prno not in output
            if "pron￨" in input_.lower():
                count_argument = 0
                count_argem = 0
                set_inputs = set()
                for input_str in input_.lower().strip().split(" "):
                    if input_str not in set_inputs:
                        set_inputs.add(input_str)
                        count_argument += 1 if "argevent_" in input_str and "￨pron￨" not in input_str else 0
                        count_argem += 1 if "￨pron￨" in input_str else 0
                args_i = [i + 1 for i in range(count_argument)]
                arge_i = [i + 1 for i in range(count_argem)]
                random.shuffle(args_i)
                random.shuffle(arge_i)
                args_keys = {}
                arge_keys = {}
                input_ = input_.lower().strip().split(" ")
                for i_in, input_str in enumerate(input_):
                    if "argevent_" in input_str  and "￨pron￨" not in input_str:
                        input_str = input_str.split("￨")
                        if input_str[0] not in args_keys.keys():
                            args_keys[input_str[0]] = "argevent_"  + args_i.pop().__str__()
                        input_str[0] = args_keys[input_str[0]]
                        input_str = "￨".join(input_str)
                    elif "￨pron￨" in input_str :
                        input_str = input_str.split("￨")
                        if input_str[0] not in arge_keys.keys():
                            arge_keys[input_str[0]] = "argem_pronoun_"  + arge_i.pop().__str__()
                        input_str[0] = arge_keys[input_str[0]]
                        input_str = "￨".join(input_str)
                    #on a les sentences_data non d'équerre, pour l'instant on laisse
                    input_[i_in] = input_str

                input_ = " ".join(input_)

            splitted_input = []
            for i_, sub_i in enumerate(input_.strip().split(" ")):
                if i_%5 == 0:
                    splitted_input.append([])
                splitted_input[-1].append(sub_i)
                if sub_i.count("￨") != 20:
                    return False

            if args.debug:
                for sub_i in splitted_input:
                    if len(sub_i)< 5:
                        print(input_)
                        print(sub_i)
                        raise Exception
            sorted(splitted_input, key=lambda x: x[2] + x[1] + x[3]) # même ordre que pendant l'apprentissage

            for x in range(len(splitted_input)):
                splitted_input[x] = " ".join(splitted_input[x])

            input_ = " ".join(splitted_input)

            input_hash = input_.__hash__()
            if input_hash in data_writed:
                return False
            data_writed.add(input_hash)

            input_ += "\n"

            #update 201908222054
            verbes_passifs = []
            input_ = input_.lower().replace("=", ":").replace("avoir-(sauf-si-pronominal-ou-entrée-en-être)","avoir-sauf-si-pronominal-ou-entrée-en-être")
            traits = traits.lower().replace("=", ":").replace("avoir-(sauf-si-pronominal-ou-entrée-en-être)","avoir-sauf-si-pronominal-ou-entrée-en-être")
            if traits.replace( "voice:pass", "voice:act" ) in input_:
                traits = traits.replace("voice:pass", "voice:act")
                v_str = "voice:act"
            else:
                if traits not in input_:
                    raise Exception
                v_str = "voice:pass"

                 #  (￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ argevent_4￨x￨0￨x￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨￨￨￨￨￨ par￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ argevent_5￨x￨0￨noun￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:fem￨number:sing￨person:3￨actualisation:defini￨masse:totale￨ )￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨

            if input_.count("￨") % 20 == 0:
                file.write(input_.lower())
                annexe_informations = {"verb":new_verb,"reference verb":(verb, v_str), "sentence_data":sentence_data}
                file_flat_json.write(annexe_informations.__str__()+"\n") # faudra un eval, mais comme cela, les lignes sont face à face. Bon cela se discute ^^'
                file_suivi.write(clé.__str__() + " " + structure  + " " + i_in_count.__str__() + " " + v_str + " " + "\n")

                if Features.Voice.PASSIVE in voice:
                    un = "voice:pass"
                    deux = "voice:act"
                    if v_str == "voice:act":
                        trait = traits.lower().replace(deux, un)
                        v_str = "voice:pass"
                    else:
                        trait = traits.lower().replace(un, deux)
                        v_str = "voice:act"
                    if traits not in input_:
                        print()
                        print(traits)
                        print(input_)
                        raise Exception

                    inpu_1 = input_.lower().replace("=", ":").replace(traits, trait)
                    if inpu_1.count("￨") % 20 == 0:
                        if inpu_1 == input_:
                            raise Exception
                        str_comp = []
                        if "voice:pass" in inpu_1:
                            for value_pass in inpu_1.split(")"):
                                if " arg_0￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ " in value_pass:
                                    if "voice:pass" in value_pass and " event_" in value_pass :
                                        value_pass = value_pass[value_pass.index(" event_"):]
                                        value_pass = value_pass.split(" ")[1:]
                                        value_pass[1] = "par￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨"
                                        value_pass.insert(0,"(￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨")
                                        value_pass.append(")￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨")
                                        str_comp.append(" ".join(value_pass))
                                    elif "voice:pass" in value_pass :
                                        continue
                        if len(str_comp) > 0:
                            str_comp = " " + " ".join(str_comp)
                        else:
                            str_comp = ""
                        file.write(inpu_1 + str_comp)
                        annexe_informations = {"verb": new_verb, "reference verb": (verb, v_str), "sentence_data": sentence_data}
                        file_flat_json.write(annexe_informations.__str__() + "\n")
                        file_suivi.write(clé.__str__()  + " " + structure  + " " + i_in_count.__str__() + " " + v_str + "\n")

        clé += 1
        return True

    print("prétraitement")

    if os.path.exists('ref_id_verb_' + __database_ref__ + '_.json'):
        print("load ref_id_verb")
        a = open('ref_id_verb_' + __database_ref__ + '_.json', "r")
        ref_id_verb = json.load(a)
        if args.debug:
            print("ref_id_verb loaded")
    else:
        ref_id_verb = {}
        for structure, _ in zip(verbs.keys(), tqdm(range(len(verbs.keys())))):
            for auxiliaire in verbs[structure].keys():
                for id_verb, step in verbs[structure][auxiliaire]:
                    if id_verb not in ref_id_verb.keys() :
                        ref_id_verb[id_verb] = 1
                    else:
                        ref_id_verb[id_verb] += 1
        fjson = open('ref_id_verb_' + __database_ref__ + '_.json', "w")
        fjson.write(json.dumps(ref_id_verb, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

    verbs_keys = list(verbs.keys())
    random.shuffle(verbs_keys)
    ref_id_verb_structure = {}
    for structure, _ in zip(verbs_keys, tqdm(range(len(verbs.keys())), mininterval=5)):
        sys.stderr.flush()
        ref_id_verb_structure[structure] = set()
        aux_keys = [random.choice(list(verbs[structure].keys()))] if args.debug else list(verbs[structure].keys())
        random.shuffle(aux_keys)
        for auxiliaire in aux_keys:
            random.shuffle(verbs[structure][auxiliaire])
            data_verbs = verbs[structure][auxiliaire]
            if len(data_verbs)  == 0:
                continue
            i_data = len(data_verbs)
            while len(data_verbs) < 200:
                data_verbs *= 2
            if i_data < 200:
                data_verbs = data_verbs[0:200]
            random.shuffle(data_verbs)
            # data_verbs = data_verbs[0:500]
            tq2 =  tqdm(range(len(data_verbs)), mininterval=0.9)
            for id_verb, step in data_verbs:
                tq2.update()
                traits = {}
                standard_elements = {}
                prépositions = {}
                modifiers_events = {}
                modifiers_elements = {}
                verb = used_id[id_verb.__str__()]
        #    if id_verb not in used_verbs_for_structure.keys():
                take = True
                objets = []
                # used_verbs_for_structure[id_verb] = None
                if args.debug:
                    req = "select sentence from sentences where sent_id = :sent_id ; "
                    curseurData.execute(req, {'sent_id':verb['sent_id']})
                    verb['sentence'] = curseurData.fetchone()
                req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                      "and prédicat like 'arg%' " \
                      "and sujet = :event"
                curseurData.execute(req, {'sent_id':verb['sent_id'], 'event':verb['item']})
                traits_verbs_lvf = verb['long_traits'].split(Traits.SEP_TRAITS)[7:11]
                test_traits = Traits(long_traits=verb['long_traits'], shorted_traits=verb['shorted_traits'])
                verbe_myabe = False                   #
                if "complétive" in test_traits.type_arg_1 or "inf" in  test_traits.type_arg_1:
                    verbe_myabe = True              #
                elif "complétive" in test_traits.type_arg_2 or "inf" in  test_traits.type_arg_2:
                    verbe_myabe = True              #
                elif "complétive" in test_traits.type_arg_3 or "inf" in  test_traits.type_arg_3:
                    verbe_myabe = True

                if "Z" in traits_verbs_lvf[2:]:
                    searchz = True
                else:
                    searchz = False
                if 'a' in traits_verbs_lvf[2:]:
                    searcha = True
                else:
                    searcha = False
                if 'b' in traits_verbs_lvf[2:]:
                    searchb = True
                else:
                    searchb = False
                if 'c' in traits_verbs_lvf[2:]:
                    searchc = True
                else:
                    searchc = False
                if 'd' in traits_verbs_lvf[2:]:
                    searchd = True
                else:
                    searchd = False
                if 'e' in traits_verbs_lvf[2:]:
                    searche = True
                else:
                    searche = False
                if 'g' in traits_verbs_lvf[2:]:
                    searchg = True
                else:
                    searchg = False
                if 'i' in traits_verbs_lvf[2:]:
                    searchi = True
                else:
                    searchi = False
                if 'j' in traits_verbs_lvf[2:]:
                    searchj = True
                else:
                    searchj = False
                if 'k' in traits_verbs_lvf[2:]:
                    searchk = True
                else:
                    searchk = False
                if 'l' in traits_verbs_lvf[2:]:
                    searchl = True
                else:
                    searchl = False
                if 'm' in traits_verbs_lvf[2:]:
                    searchm = True
                else:
                    searchm = False
                if 'n' in traits_verbs_lvf[2:]:
                    searchn = True
                else:
                    searchn = False
                if 'q' in traits_verbs_lvf[2:]:
                    searchq = True
                else:
                    searchq = False
                sujet_initial = ""
                traits_sujet_initial = ""
                for row in curseurData:
                    prédicat = row[1]
                    sujet = row[0]
                    objet = row[2]
                    if prédicat == "arg_0":
                        sujet_initial = objet[:]
                        traits_sujet_initial = row[4]
                    traits_sujet = Traits.CLEAR_TRAITS[1:] if (row[3] is None and verb['long_traits'] is None) else verb['long_traits'] if row[3] is None else verb['long_traits']
                    traits_objet = Traits.CLEAR_TRAITS[1:] if row[4] is None else row[4]
                    if prédicat not in standard_elements.keys() or objet == '':
                        if prédicat.startswith("arg"):
                            standard_elements[prédicat] = (sujet, prédicat, objet)
                            objets.append(objet)
                        else:
                            raise Exception
                    elif objet != '' and standard_elements[prédicat] ==(sujet, prédicat, objet):
                        pass
                    else:
                        take = False
                        break
                    traits[sujet] = traits_sujet
                    traits[objet] = traits_objet
                    a = 1
                if not take:
                    continue

                for argument in list(standard_elements.keys()):
                    #on cherche maintenant les propositions
                    if argument == verb['item']:
                        pass
                    req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                          "and sujet = :event " \
                          "and objet = :argument " \
                           "and prédicat not like 'arg%' " \
                           "and prédicat not like 'event%' " \
                          "and objet like 'arg£_%'  ESCAPE '£'"

                    curseurData2.execute(req, {'sent_id': verb['sent_id'], 'event': verb['item'], 'argument':argument})
                    traits_lvf = verb['long_traits'].split(Traits.SEP_TRAITS)
                    for detail_prép in curseurData2.fetchall():
                        prédicat = detail_prép[1]
                        sujet = detail_prép[0]
                        objet = detail_prép[2]
                        traits_sujet = detail_prép[3]
                        traits_objet = detail_prép[4]
                        if (searcha or searchz) and prédicat in ("à", "*au", "*aux"):
                            prédicat = "à"
                            if searchz:
                                if objet == "arg_2":
                                    traits_lvf[10] = "a"

                                elif objet == "arg_3":
                                    traits_lvf[11] = "a"

                            searcha = False
                            searchz = False
                        if (searchb or searchz) and prédicat in ("de", "*du", "*des"):
                            if searchz:
                                if objet == "arg_2":
                                    if "b" in in_features[9]:
                                        traits_lvf[10] = "b"

                                elif objet == "arg_3":
                                    if "b" in in_features[10]:
                                        traits_lvf[11] = "b"

                            prédicat = "de"
                            searchb = False
                            searchz = False
                        if (searchc or searchz) and prédicat == "avec":
                            prédicat = "avec"
                            if searchz:
                                if objet == "arg_2":
                                    traits_lvf[10] = "c"

                                elif objet == "arg_3":
                                    traits_lvf[11] = "c"

                            searchc = False
                            searchz = False
                        if (searchd or searchz) and prédicat == "contre":
                            prédicat = "contre"
                            if searchz:
                                if objet == "arg_2":
                                    traits_lvf[10] = "d"

                                elif objet == "arg_3":
                                    traits_lvf[11] = "d"

                            searchd = False
                            searchz = False
                        if (searche or searchz) and prédicat == "par":
                            prédicat = "par"
                            if objet == "arg_2":
                                traits_lvf[10] = "e"

                            elif objet == "arg_3":
                                traits_lvf[11] = "e"

                            searche = False
                            searchz = False
                        if (searchg or searchz) and prédicat in ('sur', 'vers'):
                            if objet == "arg_2":
                                traits_lvf[10] = "g"

                            elif objet == "arg_3":
                                traits_lvf[11] = "g"

                            searchg = False
                            searchz = False
                        if (searchi or searchz) and prédicat == "de":
                            if objet == "arg_2":
                                traits_lvf[10] = "i"

                            elif objet == "arg_3":
                                traits_lvf[11] = "i"

                            searchi = False
                            searchz = False
                        if (searchj or searchz) and prédicat == "dans":
                            if objet == "arg_2":
                                traits_lvf[10] = "j"

                            elif objet == "arg_3":
                                traits_lvf[11] = "j"

                            searchj = False
                            searchz = False
                        if (searchk or searchz) and prédicat == "pour":
                            if objet == "arg_2":
                                traits_lvf[10] = "k"

                            elif objet == "arg_3":
                                traits_lvf[11] = "k"

                            searchk = False
                            searchz = False
                        if (searchl or searchz) and prédicat == "auprès":
                            if objet == "arg_2":
                                traits_lvf[10] = "l"

                            elif objet == "arg_3":
                                traits_lvf[11] = "l"

                            searchl = False
                            searchz = False
                        if (searchm  or searchz)and prédicat == "devant":
                            if objet == "arg_2":
                                traits_lvf[10] = "m"

                            elif objet == "arg_3":
                                traits_lvf[11] = "m"

                            searchm = False
                            searchz = False
                        if (searchn or searchz) :
                            if objet == "arg_2":
                                traits_lvf[10] = "n"

                            elif objet == "arg_3":
                                traits_lvf[11] = "n"

                            searchn = False
                            searchz = False
                        if (searchq or searchz) and prédicat == "pour":
                            if objet == "arg_2":
                                traits_lvf[10] = "q"

                            elif objet == "arg_3":
                                traits_lvf[11] = "q"

                            searchq = False
                            searchz = False
                        for i_f, feature in enumerate(traits_lvf):
                            if feature.lower().replace("=",":") not in in_features[i_f]:
                                if "-(" in feature:
                                    traits_lvf[i_f] = traits_lvf[i_f].replace("avoir-(sauf-si-pronominal-ou-entrée-en-être)","avoir-sauf-si-pronominal-ou-entrée-en-être")
                                else:
                                    traits_lvf[i_f] = "z" if i_f in (10, 11) else ""
                        verb['long_traits'] = Traits.SEP_TRAITS.join(traits_lvf)
                        traits[verb['item']] = verb['long_traits']

                        if prédicat not in prépositions.keys() :
                            prépositions[prédicat] = [(sujet, prédicat, objet)]
                        else:
                            prépositions[prédicat].append((sujet, prédicat, objet))
                        if not sujet in traits.keys() or traits[sujet] is None:
                            traits[sujet] = traits_sujet
                        if not objet in traits.keys() or traits[objet] is None:
                            traits[objet] = traits_objet

                if searchz or searcha or searchb or searchc or searchd or searche or searchg or searchi or searchj or searchk or searchl or searchm or searchn or searchq:
                    take = False
                    continue

                req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                      "and prédicat not like 'arg%' and prédicat not in (\"juxtaposé\") " \
                      "and objet not like 'arg£_%'  ESCAPE '£' " \
                      "and sujet = :event and suj_short_traits is not null "
                curseurData2.execute(req, {'sent_id':verb['sent_id'], 'event':verb['item']})
                for row in curseurData2.fetchall():
                    prédicat = row[1]
                    sujet = row[0]
                    objet = row[2]
                    traits_sujet = row[3]
                    traits_objet = row[4]

                    if objet in objets:
                        # ce n'est pas lié à la strcuture du verbe, il n'empêche, l'un des arguments est introduit par une préposition
                        if prédicat not in prépositions.keys() :
                            prépositions[prédicat] = [(sujet, prédicat, objet)]
                        else:
                            prépositions[prédicat].append((sujet, prédicat, objet))
                    else:
                        if prédicat not in modifiers_events.keys() and traits_objet is not None:
                            modifiers_events[prédicat] = [(sujet, prédicat, objet)]
                        elif traits_objet is None:
                            take = False
                            break
                        else:
                            modifiers_events[prédicat].append((sujet, prédicat, objet))

                        # TODO vérifier
                    objets = "'" + "', '".join(objets) + "'"

                    if sujet not in traits.keys():
                        traits[sujet] = traits_sujet
                    if objet not in traits.keys() or traits[objet] is None:
                        traits[objet] = traits_objet

                    req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                          "and (prédicat = 'modifie'  or prédicat like 'impact%') " \
                          "and objet = :objet and sujet not in ('pas', 'ne') and " \
                          "sujet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car', 'si', 'si', 'que')  and " \
                          "objet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car', 'si', 'si', 'que')  and " \
                          "prédicat not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car', 'si', 'si', 'que')  and " \
                          "sujet not like 'grp%' and sujet not like 'jux%'  and sujet not like 'coord%'  and suj_short_traits is not null "
                    curseurData2.execute(req, {'sent_id':verb['sent_id'], 'objet':objet})
                    for row in curseurData2.fetchall():
                        prédicat = row[1]
                        sujet = row[0]
                        objet = row[2]
                        traits_sujet = Traits.CLEAR_TRAITS[1:] if  row[3] is None else row[3]
                        traits_objet = row[4]

                        if objet in traits.keys():
                            if prédicat not in modifiers_elements.keys() and traits_objet is not None:
                                modifiers_elements[prédicat] = [(sujet, prédicat, objet)]

                            if sujet not in traits.keys() or traits[sujet] is None:
                                traits[sujet] = traits_sujet
                            if objet not in traits.keys() or traits[objet] is None:
                                traits[objet] = traits_objet
                if not take:
                    continue
                else:
                    a = 1

                    éléments = {"standard":standard_elements,
                                                         "prépositions":prépositions, "modifiers_events":modifiers_events,
                                                         "modifiers_elements":modifiers_elements, "traits":traits }
                    try:
                        traits_verb = Traits(verb['long_traits'], shorted_traits=verb['shorted_traits'])
                    except:
                        continue

                    if structure not in constructions_par_structure.keys():
                        req = """select distinct temps, mode, person, number from verb_details where form in (select distinct lvf.verbe from lvf where construction = :structure and auxiliaire=:auxiliaire )  and  mode not in ("participe", "infinitif", "subjonctif")"""
                        curseurLVF.execute(req, {'structure':structure, "auxiliaire":auxiliaire})
                        constructions_par_structure[structure] = curseurLVF.fetchall()

                    # update 201908260136
                    #random.shuffle(traits_dispo)
                    # update 201908222034

                    traits_verb.auxiliaire = auxiliaire
                    if auxiliaire == "":
                        mode_arg_0 = Conjugaison(short=True)
                    else:
                        mode_arg_0 = Conjugaison()

                    # ce passage porte les stigmates de choix passés, mais l'idée est de conjuguer à chacun temps et de tirer au sort
                    for mode, temps in mode_arg_0.choice():
                        voices_arg_0 = [Features.Voice.ACTIVE]
                        upos_arg_0 = ["all"]
                        person_arg_0 = ["all"]
                        gender_arg_0 = ["all"]
                        number_arg_0 = ["all"]

                        if auxiliaire == "" or (traits_verb.sujet == "4"  ):
                            person_arg_0 = [Features.Person.THIRD]
                            if traits_verb.circonstant != "1":
                                upos_arg_0 = [UPOS.pronoun]
                                gender_arg_0 = [Features.Gender.MASCULINE]
                                number_arg_0 = [Features.Number.SINGULAR]

                        if structure not in sujets_par_structure.keys():
                            req = """select value from lvf_natures, lvf  where sujet = lvf_natures.code and construction = :structure """
                            curseurLVF.execute(req, {'structure':structure})
                            constructions_par_structure[structure] = curseurLVF.fetchone()[0]

                        nature = constructions_par_structure[structure]
                        if not (nature.replace(" ou ", "/") in traits_verb.type_arg_0 or  "/".join(sorted(nature.split(" ou "))) in traits_verb.type_arg_0):
                            continue

                        if not (auxiliaire == "" or (traits_verb.sujet == "4"  )):
                            if "pluriel" in nature:
                                number_arg_0 = [Features.Number.PLURAL]

                            if not "humain" in nature:
                                person_arg_0 = [Features.Person.THIRD]

                        if traits_verb.objet not in ("na", ""):
                            voices_arg_0.append(Features.Voice.PASSIVE)

                        if upos_arg_0 == ['all']: # dans les traits autorisés pour arg_0
                            upos_arg_0 = [UPOS.noun, UPOS.pronoun, UPOS.properNoun]
                        if person_arg_0 == ['all']:
                            person_arg_0 = [Features.Person(0), Features.Person(-1)]
                            for i in range(3):
                                person_arg_0.append(Features.Person(i+1))
                        if gender_arg_0 == ['all']:
                            gender_arg_0 = []
                            for i in range(6):
                                gender_arg_0.append(Features.Gender(i))
                        if number_arg_0 == ['all']:
                            number_arg_0 = []
                            for i  in range(2):
                                number_arg_0.append(Features.Number(i+1))
                        if voices_arg_0 == ['all']:
                            voices_arg_0 = []
                            for i in range(3):
                                voices_arg_0.append(Features.Voice(i-1))

                        if args.debug and random.randint(0, 99) > 10:
                            continue
                        person_arg_0 = [random.choice(person_arg_0)]

                        if "1" in person_arg_0[0].__str__() or "2" in person_arg_0[0].__str__():
                            upos_arg_0 = [UPOS.pronoun]
                            traits_dispo = []
                            # for trait in ["x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:2￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:2￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:1￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:1￨￨￨",]:
                            #    if  person_arg_0[0].__str__().replace("=", ":") in  trait:
                            #        traits_dispo.append(trait)
                        else:
                            upos_arg_0 = [random.choice(upos_arg_0)]
                            traits_dispo = traits_args_disponibles[:]
                            #for trait in  ["x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:2￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:2￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Fem￨number:Plur￨person:1￨￨￨", "x￨0￨PRON￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨0￨gender:Masc￨number:Sing￨person:1￨￨￨",]:
                             #   traits_dispo.remove(trait)

                        gender_arg_0 = [random.choice(gender_arg_0)]
                        number_arg_0 = [random.choice(number_arg_0)]

                        #traits_dispo = [random.choice(traits_dispo)] if args.debug else random.choices(traits_dispo, k=4)
                        __pers = person_arg_0[0].__str__().replace("=", ":")
                        __upos = upos_arg_0[0].__str__().replace("=", ":")
                        __gender = gender_arg_0[0].__str__().replace("=", ":")
                        __number = number_arg_0[0].__str__().replace("=", ":")

                        if __gender == 'gender:Neut':
                            __gender = "%"
                        if __pers == 'person:0':
                            __pers = "person:3"

                        traits_dispo = ['']
                        i_ibb = 0
                        suj_traits_extract = '￨'.join(traits_verb.traits.split('￨')[7:12])
                        while not 'person' in traits_dispo[0] and not 'number' in  traits_dispo[0] and not 'gender' in  traits_dispo[0] and i_ibb <3:
                            if suj_traits_extract == "":
                                suj_traits_extract = '%'
                            i_ibb += 1
                            if __upos == 'PRON' and not ( auxiliaire == "" or (traits_verb.sujet == "4"  )):
                                req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%' and suj_traits like '%:suj%'   and obj_traits like '%￨:number%' order by random()  """
                                req = req.replace(':upos', __upos).replace(':number', __number).replace(":suj", suj_traits_extract)
                                if req not in req_save.keys():
                                    req_save[req] = []
                                    curseurData2.execute(req)
                                    for rw_req in curseurData2.fetchall():
                                        req_save[req].append(rw_req)
                                traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None 
                                if traits_dispo is None:
                                    req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and suj_traits like '%:suj%'  and obj_traits like '%￨:upos￨%'   and obj_traits like '%￨:number%' order by random() """
                                    req = req.replace(':upos', __upos).replace(':number', __number).replace(":suj", suj_traits_extract)
                                    if req not in req_save.keys():
                                        req_save[req] = []
                                        curseurData2.execute(req)
                                        for rw_req in curseurData2.fetchall():
                                            req_save[req].append(rw_req)
                                    traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                            else:
                                req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and suj_traits like '%:suj%'  and obj_traits like '%￨:upos￨%' and obj_traits like '%:pers%' and obj_traits like '%:gender%'   and obj_traits like '%￨:number%'  order by random() """
                                req = req.replace(':upos', __upos).replace(':pers', __pers).replace(':gender', __gender).replace(':number', __number).replace(":suj", suj_traits_extract)
                                if req not in req_save.keys():
                                    req_save[req] = []
                                    curseurData2.execute(req)
                                    for rw_req in curseurData2.fetchall():
                                        req_save[req].append(rw_req)
                                traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                if traits_dispo is None:
                                    req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and suj_traits like '%:suj%'  and obj_traits like '%￨:upos￨%' and obj_traits like '%:pers%'   and obj_traits like '%￨:number%' order by random() """
                                    req = req.replace(':upos', __upos).replace(':pers', __pers).replace(':number', __number).replace(":suj", suj_traits_extract)
                                    if req not in req_save.keys():
                                        req_save[req] = []
                                        curseurData2.execute(req)
                                        for rw_req in curseurData2.fetchall():
                                            req_save[req].append(rw_req)
                                    traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                if traits_dispo is None:
                                    req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and suj_traits like '%:suj%'  and obj_traits like '%￨:upos￨%'   and obj_traits like '%￨:number%' order by random() """
                                    req = req.replace(':upos', __upos).replace(':number', __number).replace(":suj", suj_traits_extract)
                                    if req not in req_save.keys():
                                        req_save[req] = []
                                        curseurData2.execute(req)
                                        for rw_req in curseurData2.fetchall():
                                            req_save[req].append(rw_req)
                                    traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                            if traits_dispo is None:
                                if __upos == 'PRON' and not ( auxiliaire == "" or (traits_verb.sujet == "4"  )):
                                    req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%'   and obj_traits like '%￨:number%' order by random()  """
                                    req = req.replace(':upos', __upos).replace(':number', __number)
                                    if req not in req_save.keys():
                                        req_save[req] = []
                                        curseurData2.execute(req)
                                        for rw_req in curseurData2.fetchall():
                                            req_save[req].append(rw_req)
                                    traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                    if traits_dispo is None:
                                        req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%'  and obj_traits like '%￨:number%' order by random() """
                                        req = req.replace(':upos', __upos).replace(':number', __number)
                                        if req not in req_save.keys():
                                            req_save[req] = []
                                            curseurData2.execute(req)
                                            for rw_req in curseurData2.fetchall():
                                                req_save[req].append(rw_req)
                                        traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                else:
                                    req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%' and obj_traits like '%:pers%' and obj_traits like '%:gender%'   and obj_traits like '%￨:number%' order by random() """
                                    req = req.replace(':upos', __upos).replace(':pers', __pers).replace(':gender', __gender).replace(':number', __number)
                                    if req not in req_save.keys():
                                        req_save[req] = []
                                        curseurData2.execute(req)
                                        for rw_req in curseurData2.fetchall():
                                            req_save[req].append(rw_req)
                                    traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                    if traits_dispo is None:
                                        req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%' and obj_traits like '%:pers%'  and obj_traits like '%￨:number%' order by random() """
                                        req = req.replace(':upos', __upos).replace(':pers', __pers).replace(':number', __number)
                                        if req not in req_save.keys():
                                            req_save[req] = []
                                            curseurData2.execute(req)
                                            for rw_req in curseurData2.fetchall():
                                                req_save[req].append(rw_req)
                                        traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                    if traits_dispo is None:
                                        req = """select distinct obj_traits from arguments where prédicat = 'arg_0' and obj_traits like '%￨:upos￨%'  and obj_traits like '%￨:number%' order by random() """
                                        req = req.replace(':upos', __upos).replace(':number', __number)
                                        if req not in req_save.keys():
                                            req_save[req] = []
                                            curseurData2.execute(req)
                                            for rw_req in curseurData2.fetchall():
                                                req_save[req].append(rw_req)
                                        traits_dispo = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                            if traits_dispo is None:
                                suj_traits_extract = suj_traits_extract.split('￨')
                                suj_traits_extract.pop(-1)
                            suj_traits_extractsuj_traits_extract = '￨'.join(suj_traits_extract)
                        #number_arg_0 = [random.choice(number_arg_0)]
                        # voices_arg_0 = [random.choice(voices_arg_0)]
                        # update 201908231054
                        traits_alternatifs = None
                        if "￨PRON￨" not in traits_dispo[0]:
                            data_nom = '￨'.join(traits_dispo[0].split('￨')[-5:])
                            while traits_alternatifs is None or len(traits_alternatifs) == 0:
                                complément = ""
                                if "person" not in data_nom:
                                    complément = " and obj_traits like '%￨person:3￨%' "
                                req = "select distinct obj_traits from arguments where prédicat = 'arg_0' and " \
                                      "obj_traits like '%￨PRON￨%'  and obj_traits like '%:data%' " + complément + " order by random()  "
                                req = req.replace(':data', data_nom)
                                if req not in req_save.keys():
                                    req_save[req] = []
                                    curseurData2.execute(req)
                                    for rw_req in curseurData2.fetchall():
                                        req_save[req].append(rw_req)
                                traits_alternatifs = random.choice(req_save[req]) if len(req_save[req]) > 0 else None
                                if traits_alternatifs is not None:
                                    t_traits_alternatifs = Traits(traits_alternatifs[0])
                                    t_default = Traits(traits_dispo[0])
                                    t_traits_alternatifs._traits[15] = t_default._traits[15]
                                    t_traits_alternatifs._traits[16] = t_default._traits[16]
                                    t_traits_alternatifs._traits[17] = t_default._traits[17]
                                    t_traits_alternatifs._traits[18] = t_default._traits[18]
                                    t_traits_alternatifs._traits[19] = t_default._traits[19]
                                    if 'person:3' == t_traits_alternatifs._traits[16]:
                                        t_traits_alternatifs._traits[0] = "e"
                                    else:
                                        t_traits_alternatifs._traits[0] = "x"
                                    traits_alternatifs = (t_traits_alternatifs.traits, )
                                data_nom = data_nom.split('￨')
                                data_nom.pop(-1)
                                data_nom = '￨'.join(data_nom)
                                if len(data_nom) == 0:
                                    raise  Exception

                        # update 201908260134
                        traits_dispo = set(traits_dispo)
                        if "humain" in nature:
                            if "pluriel" in nature:
                                trait_arg = ""
                                while 'Plur' not in trait_arg:
                                    trait_arg = random.choice(lib_traits)
                                    traits_dispo.add(trait_arg)
                            else:
                                traits_dispo.add(random.choice(lib_traits))

                        for traits_arg in traits_dispo:
                            if traits_alternatifs is None:
                                traits_alternatifs = (traits_arg, None)
                            else:
                                traits_alternatifs = (traits_arg, traits_alternatifs[0])
                            for upos in upos_arg_0[0:1]:
                                for person in person_arg_0[0:1]:
                                    for gender in gender_arg_0[0:1]:
                                        for number in number_arg_0[0:1]:
                                            take_time = True
                                            take_voice = ()
                                            for voice in voices_arg_0: #
                                                if voice == Features.Voice.PASSIVE:
                                                    mode_available_for_passive = False
                                                    if mode in  event_1_allowed_times_passif.keys() :
                                                        mode_available_for_passive = True
                                                    if mode_available_for_passive:
                                                        if not temps in  event_1_allowed_times_passif[mode] :
                                                            mode_available_for_passive = False
                                                    if not mode_available_for_passive:
                                                        take_voice = (mode[:], temps[:])
                                                        modes = []
                                                        for key in event_1_allowed_times_passif.keys():
                                                            modes += [key] * len(event_1_allowed_times_passif[key])
                                                        mode = random.choice(modes)
                                                        temps = random.choice(event_1_allowed_times_passif[mode])
                                                if not take_time:
                                                    break
                                                is_written = False
                                                for round_ in range(2):
                                                    if round_ == 1:
                                                        for each in traits:
                                                            if traits[each] is not None and "VERB" in traits[each] and each != "event_1":# and verbe_myabe :
                                                                if False:
                                                                    if auxiliaire != '':
                                                                        nb_pos/=10
                                                                take_time = False
                                                                mode = test_traits.mode # on ne s'embête pas, on prend les temps d'origine
                                                                temps = test_traits.temps
                                                                # update 201908232240
                                                                if mode in ("infinitif", "subjonctif", "participe"):
                                                                    take_time = None
                                                                    break
                                                                elif len(take_voice)>0:
                                                                    voice = Features.Voice.ACTIVE
                                                                    # update 201908232245
                                                                    mode = take_voice[0]
                                                                    temps = take_voice[1]
                                                                break
                                                        if take_time is None:
                                                            take_time = False
                                                            break

                                                    arg_0 = "argevent_1" + Traits.SEP_TRAITS + traits_arg
                                                    traits_verb.mode = mode
                                                    traits_verb.temps = temps
                                                    traits_verb.voice = voice if Features.Voice.PASSIVE in voices_arg_0 else Features.Voice.ACTIVE
                                                    traits_verb.auxiliaire = auxiliaire
                                                    annexe = {verb["item"]:"event_1"}  # stockage des nouveaux noms
                                                    new_verb = "event_1" + Traits.SEP_TRAITS + traits_verb.traits

                                                    i_sent += 1
                                                    BASE_VERBALE = " ".join(["("+ Traits.CLEAR_TRAITS ,new_verb, "arg_0" + Traits.CLEAR_TRAITS , arg_0, ")"+Traits.CLEAR_TRAITS])
                                                    verbal_argument = False
                                                    take_sample = True
                                                    mod_annexe = {}
                                                    base_info = [ "("+ Traits.CLEAR_TRAITS ,new_verb, "arg_0" + Traits.CLEAR_TRAITS , arg_0, ")"+Traits.CLEAR_TRAITS ]
                                                    if len(standard_elements) <= 1:
                                                        _to_write = BASE_VERBALE + "\n"
                                                        is_written =  write(_to_write, new_verb, verb, base_info, round_, traits_verb.traits, voices_arg_0, traits_alternatifs,  "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))
                                                    else:
                                                        i = 1
                                                        i_event = 1
                                                        annexe = {}
                                                        complement = set()
                                                        for each in sorted(standard_elements.keys()):
                                                            if each == "arg_0":
                                                                if standard_elements[each][-1] not in annexe.keys():
                                                                    annexe[standard_elements[each][-1]] = "argevent_1"
                                                                continue

                                                            old_name = standard_elements[each][-1]
                                                            if old_name.startswith("event"):
                                                                i_event +=1
                                                                if old_name not in annexe.keys():
                                                                    annexe[old_name] ="event_" + i_event.__str__()
                                                                    sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], old_name, sujet_initial, traits_sujet_initial)
                                                                    BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                    base_info += infos_annexes_event
                                                            else:
                                                                i += 1
                                                                if old_name not in annexe.keys():
                                                                    if Traits.CLEAR_TRAITS[1:] == traits[old_name]:
                                                                        annexe[old_name] = old_name
                                                                    else:
                                                                        annexe[old_name] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + (1 if "argevent_1" in annexe.values() else 2)).__str__()
                                                            BASE_VERBALE = " ".join([BASE_VERBALE,
                                                                "("+ Traits.CLEAR_TRAITS ,new_verb, each + Traits.CLEAR_TRAITS, annexe[old_name] + Traits.SEP_TRAITS + traits[old_name], ")"+Traits.CLEAR_TRAITS])
                                                            base_info.append(("("+ Traits.CLEAR_TRAITS ,new_verb, each + Traits.CLEAR_TRAITS, annexe[old_name] + Traits.SEP_TRAITS + traits[old_name], ")"+Traits.CLEAR_TRAITS))
                                                            if "VERB" in traits[old_name]:
                                                                #TODO traiter les verbes
                                                                verbal_argument = True
                                                                SUBORDONNEE = ""
                                                                if "infinitif" in verb['long_traits'] or "participe" in verb['long_traits']:
                                                                    take_sample = False # on ne va se base que sur des phrases modèles conjuguées, c'est déjà assez compliqué comme ça
                                                                    break
                                                                valeur_arg = ""
                                                                if each == "arg_1":
                                                                    valeur_arg = traits_verb.type_arg_1
                                                                elif each == "arg_2":
                                                                    valeur_arg = traits_verb.type_arg_2
                                                                elif each == "arg_3":
                                                                    valeur_arg = traits_verb.type_arg_3
                                                                # if not "infinitif" in traits[old_name]:
                                                                #     if valeur_arg in ("chose", ):
                                                                #         take_sample = False
                                                                #         break
                                                                #     else:
                                                                #         complement.add(old_name)
                                                                # else:
                                                                complement.add(old_name)

                                                        for compl in complement:
                                                            req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                                                                  "and sujet = :event " \
                                                                  "and (objet like 'arg%' or objet like 'mod%' ) " \
                                                                  "and prédicat not like 'arg%' " \
                                                                  "and prédicat not like 'event%' " \
                                                                  "and objet like 'arg£_%'  ESCAPE '£' and suj_short_traits is not null "

                                                            if req + verb['sent_id'] + compl not in req_save.keys():
                                                                curseurData2.execute(req, {'sent_id': verb['sent_id'],
                                                                                           'event': compl})
                                                                req_save[req + verb['sent_id'] + compl] = []
                                                                for detail_prép in curseurData2:
                                                                    req_save[req + verb['sent_id'] + compl].append(detail_prép)

                                                            for detail_prép in req_save[ req + verb['sent_id'] + compl]:
                                                                prédicat = detail_prép[1]
                                                                sujet = detail_prép[0]
                                                                objet = detail_prép[2]

                                                                if sujet not in traits.keys():
                                                                    traits[sujet] = detail_prép[3]  if detail_prép[3] is not None and detail_prép[3] != "" else Traits.CLEAR_TRAITS[1:]
                                                                if sujet not in annexe.keys():
                                                                    if sujet.startswith("event_"):
                                                                        annexe[sujet] = "event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                            annexe[sujet] = sujet
                                                                        sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], sujet, sujet_initial, traits_sujet_initial)
                                                                        BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                        base_info += infos_annexes_event
                                                                    else:
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                            annexe[sujet] = sujet
                                                                        else:
                                                                            annexe[sujet] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()
                                                                if sujet not in mod_annexe.keys():
                                                                    mod_annexe[sujet] = annexe[sujet]
                                                                if "subj" in traits[sujet].lower():
                                                                    pass #raise Exception

                                                                if objet not in traits.keys():
                                                                    traits[objet] = detail_prép[4] if detail_prép[4] is not None and detail_prép[4] != "" else Traits.CLEAR_TRAITS[1:]
                                                                if objet not in annexe.keys():
                                                                    if objet.startswith("event_"):
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[objet]:
                                                                            annexe[objet] = objet
                                                                        else:
                                                                            annexe[objet] = "event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                        sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], objet, sujet_initial, traits_sujet_initial)
                                                                        BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                        base_info += infos_annexes_event
                                                                    else:
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[objet]:
                                                                            annexe[objet] = objet
                                                                        else:
                                                                            annexe[objet] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()
                                                                if objet not in mod_annexe.keys():
                                                                    mod_annexe[objet] = annexe[objet]
                                                                if "subj" in traits[objet].lower():
                                                                    pass #raise Exception

                                                                BASE_VERBALE = " ".join([BASE_VERBALE, "(" + Traits.CLEAR_TRAITS, annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet], prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS + traits[objet], ")" + Traits.CLEAR_TRAITS])
                                                                base_info.append(("(" + Traits.CLEAR_TRAITS, annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet], prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS + traits[objet], ")" + Traits.CLEAR_TRAITS))

                                                            req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                                                                  "and prédicat not like 'arg%' and prédicat not in (\"juxtaposé\") and " \
                                                                    "sujet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')  and " \
                                                                    "objet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')  and " \
                                                                    "prédicat not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')   " \
                                                                  "and objet not like 'arg£_%'  ESCAPE '£' " \
                                                                  "and sujet = :event and suj_short_traits is not null "
                                                            if req + verb['sent_id'] + compl not in req_save.keys():
                                                                curseurData2.execute(req, {'sent_id': verb['sent_id'],
                                                                                       'event': compl})
                                                                req_save[req + verb['sent_id'] + compl] = []
                                                                for row in curseurData2:
                                                                    req_save[req + verb['sent_id'] + compl].append(row)
                                                            for row in req_save[req + verb['sent_id'] + compl]:
                                                                prédicat = row[1]
                                                                sujet = row[0]
                                                                objet = row[2]

                                                                if sujet not in traits.keys():
                                                                    traits[sujet] = row[3] if row[3] is not None and row[3] != "" else Traits.CLEAR_TRAITS[1:]
                                                                if sujet not in annexe.keys():
                                                                    if sujet.startswith("event_"):
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                            annexe[sujet] = sujet
                                                                        else:
                                                                            annexe[sujet] = "event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                        sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], sujet, sujet_initial, traits_sujet_initial)
                                                                        BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                        base_info += infos_annexes_event
                                                                    else:
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                            annexe[sujet] = sujet
                                                                        else:
                                                                            annexe[sujet] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()

                                                                if "subj" in traits[sujet].lower():
                                                                    pass #raise Exception
                                                                if sujet not in mod_annexe.keys():
                                                                    mod_annexe[sujet] = annexe[sujet]

                                                                if objet not in traits.keys():
                                                                    traits[objet] = row[4] if row[4] is not None and row[4] != "" else Traits.CLEAR_TRAITS[1:]
                                                                if objet not in annexe.keys():
                                                                    if objet.startswith("event_"):
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[objet]:
                                                                            annexe[objet] = objet
                                                                        else:
                                                                            annexe[objet] = "event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                        sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], objet, sujet_initial, traits_sujet_initial)
                                                                        BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                        base_info += infos_annexes_event
                                                                    else:
                                                                        if Traits.CLEAR_TRAITS[1:] == traits[objet]:
                                                                            annexe[objet] = objet
                                                                        else:
                                                                            annexe[objet] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()
                                                                if "subj" in traits[objet].lower():
                                                                    pass #raise Exception
                                                                if objet not in mod_annexe.keys():
                                                                    mod_annexe[objet] = annexe[objet]

                                                                BASE_VERBALE = " ".join([BASE_VERBALE, "(" + Traits.CLEAR_TRAITS, annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet], prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS + traits[objet], ")" + Traits.CLEAR_TRAITS])

                                                                base_info.append(("(" + Traits.CLEAR_TRAITS, annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet], prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS + traits[objet], ")" + Traits.CLEAR_TRAITS))
                                                            req = "select sujet, prédicat, objet, suj_traits, obj_traits from arguments where sent_id = :sent_id " \
                                                                  "and prédicat not like 'arg%' and prédicat not in (\"juxtaposé\") and " \
                          "sujet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')  and " \
                          "objet not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')  and " \
                          "prédicat not in ('mais', 'ou', 'et', '&', 'donc', 'or', 'ni', 'car')   " \
                                                                  "and objet not like 'arg£_%'  ESCAPE '£' " \
                                                                  "and sujet = :event and suj_short_traits is not null "
                                                            if req + verb['sent_id'] + compl not in req_save.keys():
                                                                curseurData2.execute(req, {'sent_id': verb['sent_id'],
                                                                                       'event': compl})
                                                                req_save[req + verb['sent_id'] + compl] = []
                                                                for row in curseurData2:
                                                                    req_save[req + verb['sent_id'] + compl].append(row)
                                                            for row in req_save[req + verb['sent_id'] + compl]:
                                                                prédicat = row[1]
                                                                sujet = row[0]
                                                                objet = row[2]

                                                            if sujet not in traits.keys():
                                                                traits[sujet] = row[3] if row[3] is not None and row[3] != "" else Traits.CLEAR_TRAITS[1:]
                                                            if sujet not in annexe.keys():
                                                                if sujet.startswith("event_"):
                                                                    if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                        annexe[sujet] = sujet
                                                                    else:
                                                                        annexe[sujet] = "event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                    sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], sujet, sujet_initial, traits_sujet_initial)
                                                                    BASE_VERBALE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                    base_info += infos_annexes_event
                                                                else:
                                                                    if Traits.CLEAR_TRAITS[1:] == traits[sujet]:
                                                                        annexe[sujet] = sujet
                                                                    else:
                                                                        annexe[sujet] = "argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()
                                                            if "subj" in traits[sujet].lower():
                                                                pass #pass #raise Exception
                                                            if sujet not in mod_annexe.keys():
                                                                mod_annexe[sujet] = annexe[sujet]

                                                            if objet not in traits.keys():
                                                                traits[objet] = row[4] if row[4] is not None and row[4] != "" else Traits.CLEAR_TRAITS[1:]
                                                            if objet not in mod_annexe.keys():
                                                                mod_annexe[objet] = "mod_" + ((len(mod_annexe.keys())+1) *100 ).__str__()
                                                            if objet not in annexe.keys():
                                                                annexe[objet] = mod_annexe[objet]
                                                            if "VERB" in traits[objet] : #on va prendre les trucs simples
                                                                pass
                                                                # take_sample = False

                                                            BASE_VERBALE = " ".join([BASE_VERBALE, "(" + Traits.CLEAR_TRAITS,
                                                                                     annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet],
                                                                                     prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS +
                                                                                     traits[objet], ")" + Traits.CLEAR_TRAITS])
                                                            base_info.append(("(" + Traits.CLEAR_TRAITS,
                                                                                     annexe[sujet]+ Traits.SEP_TRAITS + traits[sujet],
                                                                                     prédicat + Traits.CLEAR_TRAITS, annexe[objet] + Traits.SEP_TRAITS +
                                                                                     traits[objet], ")" + Traits.CLEAR_TRAITS))
                                                    if not take_sample:
                                                        continue

                                                    if len(prépositions.keys())> 0:
                                                        for préposition in prépositions.keys():
                                                                # l'argument (arg_2, arg_3) est structurellement introduit par une préposition:
                                                            for sujet_prep, prep, objet_prep in prépositions[préposition]:
                                                                BASE_VERBALE = " ".join([BASE_VERBALE, "(" + Traits.CLEAR_TRAITS, new_verb,
                                                                     prep + Traits.CLEAR_TRAITS,
                                                                     objet_prep + Traits.CLEAR_TRAITS, ")" + Traits.CLEAR_TRAITS])

                                                                base_info.append(("(" + Traits.CLEAR_TRAITS, new_verb,
                                                                     prep + Traits.CLEAR_TRAITS,
                                                                     objet_prep + Traits.CLEAR_TRAITS, ")" + Traits.CLEAR_TRAITS))

                                                    if len(standard_elements) > 1 or len(prépositions.keys())> 0 :
                                                        _to_write = BASE_VERBALE + "\n"
                                                        is_written =  write(_to_write, new_verb, verb, base_info, round_, traits_verb.traits, voices_arg_0, traits_alternatifs, "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))

                                                    # annexe contient tous les traits sélectionnés pour les arguments de l'évènement
                                                    # traits contient tous les traits
                                                    BASE_ADVERBIALE = []
                                                    base_info_adverbiale = []
                                                    kept = {}
                                                    kept_additionnel = {}
                                                    if len(modifiers_elements.keys())> 0:
                                                        for modifieur in modifiers_elements.keys():
                                                            for sujet_mod, prédicat, objet_mod in modifiers_elements[modifieur]:
                                                                if objet_mod  in annexe.keys():
                                                                    if 'ADV' in traits[sujet_mod]:
                                                                        BASE_ADVERBIALE.append(" ".join(["(" + Traits.CLEAR_TRAITS, sujet_mod + Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                         prédicat + Traits.CLEAR_TRAITS,
                                                                         annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS]))
                                                                        base_info_adverbiale.append(("(" + Traits.CLEAR_TRAITS, sujet_mod + Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                         prédicat + Traits.CLEAR_TRAITS,
                                                                         annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS))
                                                                    else:
                                                                        if modifieur not in kept_additionnel.keys():
                                                                            kept_additionnel[modifieur] = []
                                                                        kept_additionnel[modifieur].append((sujet_mod, prédicat, objet_mod))
                                                                else:
                                                                    if modifieur not in kept.keys():
                                                                        kept[modifieur] = []
                                                                    kept[modifieur].append((sujet_mod, prédicat, objet_mod))
                                                    if len(BASE_ADVERBIALE) > 0:
                                                        _to_write = BASE_VERBALE +" " + " ".join(BASE_ADVERBIALE)+ "\n"
                                                        is_written =  write(_to_write, new_verb, verb, base_info + base_info_adverbiale, round_, traits_verb.traits, voices_arg_0, traits_alternatifs, "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))
                                                        if len(BASE_ADVERBIALE)> 1:
                                                            for adv in BASE_ADVERBIALE:
                                                                _to_write = BASE_VERBALE +" " + adv + "\n"
                                                                is_written =  write(_to_write, new_verb, verb, base_info + [(adv.split(" "))], round_, traits_verb.traits, voices_arg_0, traits_alternatifs, "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))

                                                    BASE_ADDITIONNELLE = []
                                                    base_info_additionnelle = []
                                                    for modifieur in kept_additionnel.keys():
                                                        for sujet_mod, prédicat, objet_mod in kept_additionnel[modifieur]:
                                                            if sujet_mod == "*sans":
                                                                BASE_ADDITIONNELLE.append(" ".join(["(￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ *sans￨x￨0￨adv￨0￨0￨￨￨￨￨￨￨￨￨￨0￨0￨0￨0￨0￨0 modifie￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨",
                                                                     annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS]))
                                                                base_info_additionnelle.append(("(￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ *sans￨x￨0￨adv￨0￨0￨￨￨￨￨￨￨￨￨￨0￨0￨0￨0￨0￨0 modifie￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨",
                                                                     annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS))
                                                            else:
                                                                if not sujet_mod.startswith("mod_"):
                                                                    pass
                                                                    #print(sujet_mod)
                                                                    #print("not sujet_mod.startswith(mod_)")
                                                                    #raise Exception
                                                                mod_annexe[sujet_mod] = "mod_" + ((len(mod_annexe.keys())+1) *100 ).__str__()
                                                                BASE_ADDITIONNELLE.append(" ".join(["(" + Traits.CLEAR_TRAITS, mod_annexe[sujet_mod]+ Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                     prédicat + Traits.CLEAR_TRAITS,
                                                                     annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS]))
                                                                base_info_additionnelle.append(("(" + Traits.CLEAR_TRAITS, mod_annexe[sujet_mod]+ Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                     prédicat + Traits.CLEAR_TRAITS, annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS))

                                                    for modifieur in modifiers_events.keys():
                                                        for sujet_mod, prédicat, objet_mod in modifiers_events[modifieur]:
                                                            if objet_mod  not in annexe.keys() and sujet_mod != verb["item"]: # un participe peut être modifié sans que ce soit exploitable pour l'utiliser en verbe
                                                                if not objet_mod.startswith("argevent_"):
                                                                    print(objet_mod)
                                                                    print("not objet_mod.startswith(argevent_)")
                                                                    pass
                                                                if objet_mod.startswith("event_"):
                                                                    annexe[objet_mod]="event_" + (len([k for k in annexe.keys() if k.startswith("event_")]) + 1).__str__()
                                                                    sep_info_complémentaire_event, infos_annexes_event = gestion_event_annexe(annexe, traits, verb['sent_id'], objet_mod, sujet_initial, traits_sujet_initial)
                                                                    BASE_ADDITIONNELLE += sep_info_complémentaire_event + " ".join(infos_annexes_event)
                                                                    base_info_additionnelle += infos_annexes_event
                                                                else:
                                                                    annexe[objet_mod]="argevent_" + (len([k for k in annexe.keys() if k.startswith("argevent_")]) + 1).__str__()
                                                                BASE_ADDITIONNELLE.append(" ".join(["(" + Traits.CLEAR_TRAITS, annexe[sujet_mod]+ Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                     prédicat + Traits.CLEAR_TRAITS,
                                                                     annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS]))
                                                                base_info_additionnelle(("(" + Traits.CLEAR_TRAITS, annexe[sujet_mod]+ Traits.SEP_TRAITS + traits[sujet_mod] ,
                                                                     prédicat + Traits.CLEAR_TRAITS,
                                                                     annexe[objet_mod]+ Traits.SEP_TRAITS + traits[objet_mod], ")" + Traits.CLEAR_TRAITS))

                                                        #pass #raise Exception

                                                    if len(BASE_ADDITIONNELLE) > 0:
                                                        _to_write = BASE_VERBALE +" " + " ".join(BASE_ADDITIONNELLE)+ "\n"
                                                        is_written =  write(_to_write, new_verb, verb, base_info + base_info_additionnelle, round_, traits_verb.traits, voices_arg_0, traits_alternatifs, "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))
                                                        _to_write = BASE_VERBALE +" " + " ".join(BASE_ADVERBIALE)+ " ".join(BASE_ADDITIONNELLE)+ "\n"
                                                        is_written =  write(_to_write, new_verb, verb, base_info + base_info_adverbiale + base_info_additionnelle, round_, traits_verb.traits, voices_arg_0, traits_alternatifs, "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))

                                                        if len(BASE_ADDITIONNELLE)> 1:
                                                            for mod in BASE_ADDITIONNELLE:
                                                                _to_write = BASE_VERBALE +" " + mod + "\n"
                                                                is_written =  write(_to_write, new_verb, verb, base_info+ [(mod.split(" "))], round_, traits_verb.traits, voices_arg_0, traits_alternatifs,  "|".join((structure, traits_verb.type_arg_0, traits_verb.type_arg_1, traits_verb.type_arg_2, traits_verb.type_arg_3)))
                                                # update 201909191359
            tq2.close()
    if False:
        for key in not_dones_sents.keys():
            for sent in not_dones_sents[key]:
                file.write(sent)
            file.write()
    print(i_sent)
    file.flush()
    file.close()
    file_flat_json.flush()
    file_flat_json.close()
    file_suivi.flush()
    file_suivi.close()

    while not file.closed:
        True
    while not file_flat_json.closed:
        True
    while not file_suivi.closed:
        True

if __name__ == "__main__":
    __check_version__()
    main()

print(i_sent)


"""
 (￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ event_2￨x￨polarity:pos￨verb￨0￨0￨passé￨participe￨p￨1￨na￨j￨na￨avoir-sauf-si-pronominal-ou-entrée-en-être￨voice:act￨0￨0￨0￨0￨0￨0 
 dans￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ arg_2￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨ )
"""

snapshot = tracemalloc.take_snapshot()
display_top(snapshot)
