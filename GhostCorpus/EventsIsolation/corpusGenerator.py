__version__ = 0.1
import sqlite3
import glob, sys, random
from itertools import *
from ressources import *
from tqdm import tqdm
import json
import os
from tqdm import tqdm

debug = False

out_features = []

__ref_dir__ = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-full-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/data"

def loadFeaturesOut():
    global __ref_dir__, out_features
    fichiers = sorted(glob.glob(__ref_dir__ + '/test.target_feature_*.*'), key=lambda fichier:int(fichier.split("_")[-1].split(".")[0]))
    for fichier in fichiers:
        out_features.append(set())
        for line in open(fichier):
            if line.startswith(" "):
                out_features[-1].add("")
            elif "<" not in line and ">" not in line:
                out_features[-1].add(line.split()[0])


loadFeaturesOut()
out_words = set()
out_words_details = {}

connexion = sqlite3.connect(Word._database, isolation_level=None)
print(Word._database)

curseurLVF = connexion.cursor()  # Récupération d'un curseur
curseurLVFdétails = connexion.cursor()  # Récupération d'un curseur

if os.path.exists("out_words.json"):
    a = open("out_words.json", "r")
    out_words = json.load(a)
    a = open("out_words_details.json", "r")
    out_words_details = json.load(a)
    out_words = set(out_words)
    out_words_details = set(out_words_details)
    a.close()
else:
    refused = set()
    for step in ("train", "test", "val"):
        priin = False
        print(step)
        fichier_sent = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_sentence_outputs.txt"
        fichier_toks =  "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_outputs.txt"

        num_lines = sum(1 for line in open(fichier_sent))
        fichier_sent = open(fichier_sent, 'r')
        fichier_toks = open(fichier_toks, 'r')

        for sent, toks, _ in zip(fichier_sent, fichier_toks, tqdm(range(num_lines))):
            sent = sent.split()
            toks = toks.split()
            for word, analyse in zip(sent, toks):
                analyse = analyse.split("￨")[1]
                if analyse in ("punct", "ponct"):
                    out_words.add(word)
                    if word not in out_words_details:
                        out_words_details[word] = 0
                    out_words_details[word] += 1
                elif word in refused:
                    pass
                elif word not in out_words:
                    req = "select word_details.form from word_details where form = :form " + \
                          " union "  + \
                          "select mot from dem  where mot = :form " + \
                          " union " + \
                          "select verb_details.form from verb_details  where verb_details.form = :form "
                    curseurLVF.execute(req, {'form': word.replace("_", " ")})
                    a = curseurLVF.fetchall()
                    if len(a) > 0:
                        out_words.add(word)
                        out_words.add(word.replace("_", " "))
                        if word not in out_words_details:
                            out_words_details[word] = 0
                    else:
                        refused.add(word)
                if word in out_words:
                    out_words_details[word] += 1
                elif word.lower() in out_words_details:
                    out_words_details[word.lower()] += 1

    fjson = open("out_words.json", "w")
    fjson.write(json.dumps(list(out_words), indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    fjson = open("out_words_details.json", "w")
    fjson.write(json.dumps(out_words_details, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

class Trait():

    _indexes = {}

    def __init__(self):
        self._value = ""
        self._desc = ""
        self._universal_pos = ""
        self._gender = ""
        self._number = ""
        self._temps = ""
        self._mode = ""
        self._person = ""
        self._arg_0 = ""
        self._arg_1 = ""
        self._arg_2 = ""
        self._arg_3 = ""
        self._type_arg_0 = ""
        self._type_arg_1 = ""
        self._type_arg_2 = ""
        self._type_arg_3 = ""
        self._dep = ""
        self._nature = ""
        self._events = ""
        self._aux = ""
        self._values = "￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨".split("￨")
        Trait._indexes["universal_pos"] = 0
        Trait._indexes["gender"] = 1
        Trait._indexes["number"] = 2
        Trait._indexes["temps"] = 3
        Trait._indexes["mode"] = 4
        Trait._indexes["person"] = 5
        Trait._indexes["arg_0"] = 6
        Trait._indexes["arg_1"] = 7
        Trait._indexes["arg_2"] = 8
        Trait._indexes["arg_3"] = 9
        Trait._indexes["type_arg_0"] = 10
        Trait._indexes["type_arg_1"] = 11
        Trait._indexes["type_arg_2"] = 12
        Trait._indexes["type_arg_3"] = 13
        Trait._indexes["dep"] = 14
        Trait._indexes["nature"] = 15

    def setTraits(self, traits:str):
        self._values = traits.split("￨")
        self.value = self._values[0]
        self.universal_pos = self._values[1]
        self.gender = self._values[2]
        self.number = self._values[3]
        self.temps = self._values[4]
        self.mode = self._values[5]
        self.person = self._values[6]
        self.arg_0 = self._values[7]
        self.arg_1 = self._values[8]
        self.arg_2 = self._values[9]
        self.arg_3 = self._values[10]
        self.type_arg_0 = self._values[11]
        self.type_arg_1 = self._values[12]
        self.type_arg_2 = self._values[13]
        self.type_arg_3 = self._values[14]
        self.dep = self._values[15]
        self.nature = self._values[16]

    def getTrait(self):
        self._values[0] = self.value
        self._values[1] = self.universal_pos
        self._values[2] = self.gender
        self._values[3] = self.number
        self._values[4] = self.temps
        self._values[5] = self.mode
        self._values[6] = self.person
        self._values[7] = self.arg_0
        self._values[8] = self.arg_1
        self._values[9] = self.arg_2
        self._values[10] = self.arg_3
        self._values[11] = self.type_arg_0
        self._values[12] = self.type_arg_1
        self._values[13] = self.type_arg_2
        self._values[14] = self.type_arg_3
        self._values[15] = self.dep
        self._values[16] = self.nature
        return "￨".join(self._values)


    @property 
    def value(self):
        return self._value

    @value.setter 
    def value(self, value):
        self._value = value


    @property 
    def desc(self):
        return self._desc

    @desc.setter 
    def desc(self, value):
        self._desc = value


    @property 
    def universal_pos(self):
        return self._universal_pos

    @universal_pos.setter 
    def universal_pos(self, value):
        if debug and value not in out_features[Trait._indexes["universal_pos"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["universal_pos"]], file=sys.stderr)
            raise Exception
        self._universal_pos = value


    @property 
    def gender(self):
        return self._gender

    @gender.setter 
    def gender(self, value):
        if debug and value not in out_features[Trait._indexes["gender"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["gender"]], file=sys.stderr)
            raise Exception
        self._gender = value


    @property 
    def number(self):
        return self._number

    @number.setter 
    def number(self, value):
        if debug and value not in out_features[Trait._indexes["number"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["number"]], file=sys.stderr)
            raise Exception
        self._number = value


    @property 
    def temps(self):
        return self._temps

    @temps.setter 
    def temps(self, value):
        if debug and value not in out_features[Trait._indexes["temps"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["temps"]], file=sys.stderr)
            raise Exception
        self._temps = value


    @property 
    def mode(self):
        return self._mode

    @mode.setter 
    def mode(self, value):
        if debug and value not in out_features[Trait._indexes["mode"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["mode"]], file=sys.stderr)
            raise Exception
        self._mode = value

    @property 
    def person(self):
        return self._person

    @person.setter 
    def person(self, value):
        if debug and value not in out_features[Trait._indexes["person"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["person"]], file=sys.stderr)
            raise Exception
        self._person = value


    @property 
    def arg_0(self):
        return self._arg_0

    @arg_0.setter 
    def arg_0(self, value):
        if debug and value not in out_features[Trait._indexes["arg_0"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["arg_0"]], file=sys.stderr)
            raise Exception
        self._arg_0 = value


    @property 
    def arg_1(self):
        return self._arg_1

    @arg_1.setter 
    def arg_1(self, value):
        if debug and value not in out_features[Trait._indexes["arg_1"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["arg_1"]], file=sys.stderr)
            raise Exception
        self._arg_1 = value


    @property 
    def arg_2(self):
        return self._arg_2

    @arg_2.setter 
    def arg_2(self, value):
        if debug and value not in out_features[Trait._indexes["arg_2"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["arg_2"]], file=sys.stderr)
            raise Exception
        self._arg_2 = value


    @property 
    def arg_3(self):
        return self._arg_3

    @arg_3.setter 
    def arg_3(self, value):
        if debug and value not in out_features[Trait._indexes["arg_3"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["arg_3"]], file=sys.stderr)
            raise Exception
        self._arg_3 = value


    @property 
    def type_arg_0(self):
        return self._type_arg_0

    @type_arg_0.setter 
    def type_arg_0(self, value):
        value = value.replace("_", "-")
        if debug and value not in out_features[Trait._indexes["type_arg_0"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["type_arg_0"]], file=sys.stderr)
            raise Exception
        self._type_arg_0 = value


    @property 
    def type_arg_1(self):
        return self._type_arg_1

    @type_arg_1.setter 
    def type_arg_1(self, value):
        value = value.replace("_", "-")
        if debug and value not in out_features[Trait._indexes["type_arg_1"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["type_arg_1"]], file=sys.stderr)
        self._type_arg_1 = value


    @property 
    def type_arg_2(self):
        return self._type_arg_2

    @type_arg_2.setter 
    def type_arg_2(self, value):
        value = value.replace("_", "-")
        value = value.replace("dans", "en,-dans")
        value = value.replace("_", "-")
        if debug and value not in out_features[Trait._indexes["type_arg_2"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["type_arg_2"]], file=sys.stderr)
        self._type_arg_2 = value

    @property 
    def type_arg_3(self):
        return self._type_arg_3

    @type_arg_3.setter 
    def type_arg_3(self, value):
        value = value.replace("_", "-")
        if debug and value not in out_features[Trait._indexes["type_arg_3"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["type_arg_3"]], file=sys.stderr)
        self._type_arg_3 = value

    @property 
    def dep(self):
        return self._dep

    @dep.setter 
    def dep(self, value):
        if debug and value not in out_features[Trait._indexes["dep"]]:
            print(value, file=sys.stderr)
            print(out_features[Trait._indexes["dep"]], file=sys.stderr)
            raise Exception
        self._dep = value


    @property 
    def nature(self):
        return self._nature

    @nature.setter 
    def nature(self, value):
        self._nature = value


    @property 
    def events(self):
        return self._events

    @property 
    def aux(self):
        return self._aux



_préposition = {}
req = "SELECT code, value FROM lvf_prépositions;"
curseurLVF.execute(req, {})
rows = curseurLVF.fetchall()
for row in rows:
    _préposition[row[0]] = "/".join(sorted([k.replace(" ", "_") for k in row[1].split(" ou ")]))

_nature = {}
req = "SELECT code, value FROM lvf_natures;"
curseurLVF.execute(req, {})
rows = curseurLVF.fetchall()
for row in rows:
    _nature[row[0]] = "/".join(sorted([k.replace(" ", "_") for k in row[1].split(" ou ")]))

_complément = {}
req = "SELECT code, value FROM lvf_compléments;"
curseurLVF.execute(req, {})
rows = curseurLVF.fetchall()
for row in rows:
    _complément[row[0]] = "/".join(sorted([k.replace(" ", "_") for k in row[1].split(" ou ")]))

print(_préposition)
print(_nature)

ne_elision = Trait()
ne = Trait()
réfléchi = Trait()
pas = Trait()
pronom = Trait()
punct = Trait()
ne_elision.setTraits("n'￨adv￨￨￨￨￨￨￨￨￨￨￨￨￨￨3￨indéfini")
ne.setTraits("ne￨adv￨￨￨￨￨￨￨￨￨￨￨￨￨￨23￨indéfini")
pas.setTraits("pas￨adv￨￨￨￨￨￨￨￨￨￨￨￨￨￨13￨indéfini")
réfléchi.setTraits('se￨pron￨￨￨￨￨person:3￨￨￨￨￨￨￨￨￨￨')
pronom.setTraits('je￨pron￨￨number:sing￨￨￨person:1￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass=[]
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('moi￨pron￨￨number:sing￨￨￨person:1￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('toi￨pron￨￨number:sing￨￨￨person:2￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('lui￨pron￨￨number:sing￨gender:masc￨￨person:3￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('elle￨pron￨￨number:sing￨gender:fem￨￨person:3￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('nous￨pron￨￨number:plur￨￨￨person:1￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('vous￨pron￨￨number:plur￨￨￨person:2￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('eux￨pron￨￨number:plur￨gender:masc￨￨person:3￨￨￨￨￨￨￨￨￨￨')
pronoms_arg_0_pass.append(Trait())
pronoms_arg_0_pass[-1].setTraits('elles￨pron￨￨number:plur￨gender:fem￨￨person:3￨￨￨￨￨￨￨￨￨￨')

punct.setTraits(".￨punct￨￨￨￨￨￨￨￨￨￨￨￨￨￨2￨")
être = Trait()
être.setTraits("être￨aux￨gender:com￨￨￨￨￨￨￨￨￨￨￨￨￨￨")
en = Trait()
en.setTraits("en￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨8￨indéfini")
par = Trait()
par.setTraits("par￨adp￨￨￨￨￨￨￨￨￨￨￨￨￨￨6￨indéfini")
consubj = Trait()
consubj.setTraits("que￨sconj￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨indéfini")
inputs = []
outputs = []

req = "select distinct verbe, construction, type, sujet, objet, préposition, circonstant, auxiliaire from  lvf group by verbe, construction, type, sujet, objet, préposition, circonstant, auxiliaire "
#               0       1           2       3   4       5               6           7

columns = ['verb_id', 'form', 'tense', 'mood', 'person', 'number', 'aspect', 'gender', 'mode', 'temps', 'elision']
en_tetes = {}
for i in range(len(columns)):
    en_tetes[i] = columns[i]

tab_verbs = {}
verbs = {}
wrtite_ae = False
if os.path.exists("être.json"):
    a = open("être.json", "r")
    verb_être = json.load(a)
    a.close()
else:
    verb_être = {"in":[], 'out':[]}
    verb_avoir = {"in":[], 'out':[]}
    wrtite_ae = True
if os.path.exists("avoir.json"):
    a = open("avoir.json", "r")
    verb_avoir = json.load(a)
    a.close()
else:
    verb_être = {"in":[], 'out':[]}
    verb_avoir = {"in":[], 'out':[]}
    wrtite_ae = True

if os.path.exists("tab_verbs.json"):
    print("load tab_verbs", file=sys.stderr)
    a = open("tab_verbs.json", "r")
    tab_verbs = json.load(a)
    a.close()
    print("tab_verbs loaded", file=sys.stderr)
else:
    tab_verbs = {}

def add(verbe, in_, out_):
    global verb_être
    global verb_avoir
    if verbe == "être":
        verb_être['in'] += in_
        verb_être['out'] += out_
    if verbe == "avoir":
        verb_avoir['in'] += in_
        verb_avoir['out'] += out_

pronoms = {'singular':{'1':"je", '2':"tu", '3':"il"},
           'plural':{'1':"nous", '2':"vous", '3':"ils"}}
ins_done = set()

def makeInOu(pronom_courant, type_verb, auxiliaire, verbe, negation, élision, impersonnel, lemme):
    global verb_être
    global verb_avoir
    global ins_done
    global tab_verbs
    if type_verb == "p" and not (lemme.startswith("s'") or lemme.startswith("se ") ) :
        if élision:
            lemme = "s'" + lemme
        else:
            lemme = "se " + lemme
    if lemme not in tab_verbs.keys():
        tab_verbs[lemme] = {}

    in_ = []
    out_ = []
    if verbe.mode not in tab_verbs[lemme].keys():
        tab_verbs[lemme][verbe.mode]={}
    if verbe.temps + negation.__str__() not in tab_verbs[lemme][verbe.mode].keys():
        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()]={}

    if pronom_courant is not None:
        if pronom_courant.value[0:2] == "il":
            if pronom_courant.value == "il":
                pronom = Trait()
                pronom.setTraits(pronom.getTrait())
                pronom.value = pronom_courant.value.replace("il", "on")
                pronom.gender = 'gender:masc'
                if impersonnel:
                   pronom.nature="impersonnel"
                in_2, ou_2 = makeInOu(pronom, type_verb, auxiliaire, verbe, negation, élision, impersonnel, lemme)
                in_ += in_2
                out_ += ou_2
                pronom.value = pronom_courant.value.replace("il", "elle")
                pronom.gender = 'gender:fem'
                in_2, ou_2 = makeInOu(pronom, type_verb, auxiliaire, verbe, negation, élision, impersonnel, lemme)
                in_ += in_2
                out_ += ou_2

    ne.value = "ne"
    punct.value = "."

    if verbe.mode in ("infinitif", "participe"):
        if verbe.mode == "participe":
            gérondif = Trait()
            gérondif.setTraits(verbe.getTrait())
            gérondif.mode = "gérondif"
            if gérondif.temps == "passé-composé":
                gérondif.temps = "passé"
        else:
            gérondif = None
        if negation:
            if type_verb != "p":
                if verbe.mode ==  "infinitif":
                    in_.append("ne pas " + verbe.value + " .")
                    ne.dep = "3"
                    pas.dep = "3"
                    punct.dep = "3"

                    out_.append(" ".join([ne.getTrait(), pas.getTrait(), verbe.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                elif verbe.mode == "participe" and verbe.temps == "présent":
                    if élision:
                        ne.value = "n'"
                    in_.append(ne.value + " " + verbe.value + " pas " + " .")
                    ne.dep = "2"
                    pas.dep = "2"
                    punct.dep = "2"
                    out_.append(" ".join([ne.getTrait(), verbe.getTrait(), pas.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))
                    # gérondif
                    if élision:
                        ne.value = "n'"
                    in_.append("en " + ne.value + " " + verbe.value + " pas " + " .")
                    en.dep = "3"
                    ne.dep = "3"
                    pas.dep = "3"
                    punct.dep = "3"
                    out_.append(" ".join([en.getTrait(), ne.getTrait(), gérondif.getTrait(), pas.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))
                elif verbe.mode == "participe" and verbe.temps == "passé":
                    ne.value = "non"
                    in_.append(ne.value + " " + verbe.value  + " .")
                    ne.dep = "2"
                    pas.dep = "2"
                    punct.dep = "2"
                    out_.append(" ".join([ne.getTrait(), verbe.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))
                else:
                    pass
            else:
                if verbe.mode == "infinitif":
                    ref = Trait()
                    ref.setTraits(réfléchi.getTrait())
                    if élision:
                        ref.value = "s'"
                    else:
                        ref.value = "se"
                    ref.dep = "4"
                    in_.append("ne pas " + ref.value + " " + verbe.value + " .")
                    ne.dep = "4"
                    pas.dep = "4"
                    punct.dep = "4"
                    out_.append(" ".join([ne.getTrait(), pas.getTrait(), ref.getTrait(), verbe.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))
                elif verbe.mode == "participe" and verbe.temps == "présent":
                    ref = Trait()
                    ref.setTraits(réfléchi.getTrait())
                    if élision:
                        ref.value = "s'"
                    else:
                        ref.value = "se"
                    ref.dep = "3"
                    in_.append("ne " + ref.value + " " + verbe.value + " pas .")
                    ne.dep = "3"
                    pas.dep = "3"
                    punct.dep = "3"
                    out_.append(" ".join([ne.getTrait(), ref.getTrait(), verbe.getTrait(), pas.getTrait(),  punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                    #gérondif
                    ref = Trait()
                    ref.setTraits(réfléchi.getTrait())
                    if élision:
                        ref.value = "s'"
                    else:
                        ref.value = "se"
                    en.dep = "4"
                    ref.dep = "4"
                    in_.append("en ne " + ref.value + " " + verbe.value + " pas .")
                    ne.dep = "4"
                    pas.dep = "4"
                    punct.dep = "4"
                    out_.append(" ".join([en.getTrait(), ne.getTrait(), ref.getTrait(), gérondif.getTrait(), pas.getTrait(),  punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                elif verbe.mode == "participe" and verbe.temps == "passé":
                    pass
                else:
                    pass

            if type_verb == "t" and auxiliaire != ""and verbe.type_arg_1 != "": # passif
                if verbe.mode ==  "infinitif":
                    in_.append("ne pas être " + verbe.value + " .")
                    ne.dep = "4"
                    pas.dep = "4"
                    punct.dep = "4"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "4"
                    aux_être.universal_pos = "aux"
                    aux_être.mode = "infinitif"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([ne.getTrait(), pas.getTrait(),  aux_être.getTrait(), verbe.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                elif verbe.mode == "participe" and verbe.temps == "passé":
                    in_.append("ne s' étant pas " + verbe.value + " .")
                    ref = Trait()
                    ref.setTraits(réfléchi.getTrait())
                    ref.value = "s'"
                    ne.dep = "5"
                    pas.dep = "5"
                    punct.dep = "5"
                    ref.dep = "3"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "5"
                    aux_être.universal_pos = "aux"
                    aux_être.value = "étant"
                    aux_être.mode = "participe"
                    aux_être.temps = "présent"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([ne.getTrait(),  ref.getTrait(), aux_être.getTrait(), pas.getTrait(),  verbe.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                    # gérondif
                    in_.append("en ne s' étant pas " + verbe.value + " .")
                    ref = Trait()
                    ref.setTraits(réfléchi.getTrait())
                    ref.value = "s'"
                    en.dep = "6"
                    ne.dep = "6"
                    pas.dep = "6"
                    punct.dep = "6"
                    ref.dep = "6"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "6"
                    aux_être.universal_pos = "aux"
                    aux_être.value = "étant"
                    aux_être.mode = "participe"
                    aux_être.temps = "présent"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([en.getTrait(), ne.getTrait(),  ref.getTrait(), aux_être.getTrait(), pas.getTrait(),  gérondif.getTrait(), punct.getTrait()]))

                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                elif verbe.mode == "participe" and verbe.temps == "présent":
                    pass
                else:
                    pass
        else:

            if type_verb != "p" and verbe.temps != "passé-composé":
                in_.append(verbe.value + " .")
                ne.dep = "1"
                pas.dep = "1"
                punct.dep = "1"
                out_.append(" ".join([verbe.getTrait(), punct.getTrait()]))

                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                if verbe.mode == "participe" and verbe.temps == "passé":
                    # gérondif
                    punct.dep = "3"
                    en.dep = "3"
                    aux_temps = Trait()
                    aux_temps.setTraits(être.getTrait())
                    aux_temps.universal_pos = "aux"
                    if aux == "être":
                        aux_temps.value = "étant"
                        in_.append("en étant " + verbe.value+ " .")
                    else:
                        aux_temps.value = "ayant"
                        in_.append("en ayant " + verbe.value+ " .")
                    aux_temps.nature = "aux.tmps"
                    aux_temps.mode = "participe"
                    aux_temps.temps = "présent"
                    aux_temps.dep = "3"
                    out_.append(" ".join([en.getTrait(), aux_temps.getTrait(), gérondif.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

            elif not (verbe.mode == "participe" and (verbe.temps == "passé" or verbe.temps == "passé-composé" )):
                ref = Trait()
                ref.setTraits(réfléchi.getTrait())
                if élision:
                    ref.value = "s'"
                else:
                    ref.value = "se"
                ref.dep = "2"
                in_.append(ref.value + " " + verbe.value+ " .")
                ne.dep = "2"
                pas.dep = "2"
                punct.dep = "2"
                out_.append(" ".join([ref.getTrait(), verbe.getTrait(), punct.getTrait()]))

                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

            if type_verb == "t" and auxiliaire != ""and verbe.type_arg_1 != "":  # passif
                if verbe.mode ==  "infinitif":
                    in_.append("être " + verbe.value+ " .")
                    punct.dep = "1"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "1"
                    aux_être.universal_pos = "aux"
                    aux_être.mode = "infinitif"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([ aux_être.getTrait(), verbe.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                elif verbe.mode ==  "participe" and verbe.temps == "passé":
                    in_.append("étant " + verbe.value+ " .")
                    punct.dep = "2"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "2"
                    aux_être.universal_pos = "aux"
                    aux_être.value = "étant"
                    aux_être.mode = "participe"
                    aux_être.temps = "présent"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([aux_être.getTrait(), verbe.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                    #gérondif
                    in_.append("en étant " + verbe.value+ " .")
                    en.dep = "3"
                    punct.dep = "3"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "3"
                    aux_être.universal_pos = "aux"
                    aux_être.value = "étant"
                    aux_être.mode = "participe"
                    aux_être.temps = "présent"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([en.getTrait(), aux_être.getTrait(), gérondif.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

                if verbe.mode ==  "participe" and verbe.temps == "passé-composé":
                    in_.append("en ayant été " + verbe.value+ " .")
                    en.dep = "4"
                    punct.dep = "4"
                    aux_temps = Trait()
                    aux_temps.setTraits(être.getTrait())
                    aux_temps.universal_pos = "aux"
                    aux_temps.value = "ayant"
                    aux_temps.nature = "aux.tmps"
                    aux_temps.mode = "participe"
                    aux_temps.temps = "présent"
                    aux_temps.dep = "3"
                    aux_être = Trait()
                    aux_être.setTraits(être.getTrait())
                    aux_être.dep = "4"
                    aux_être.universal_pos = "aux"
                    aux_être.mode = "participe"
                    aux_être.value = "été"
                    aux_être.temps = "passé"
                    aux_être.nature = "aux.pass"
                    out_.append(" ".join([en.getTrait(), aux_temps.getTrait(), aux_être.getTrait(), gérondif.getTrait(), punct.getTrait()]))


                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""] = {}
                    if "" not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][""][""][""].append(([in_[-1]], [out_[-1]]))

    if pronom_courant is not None:

        pronom = Trait()
        pronom.setTraits(pronom_courant.getTrait())
        pronom.number = verbe.number
        pronom.dep = ""
        # TODO
        if élision and not negation and "person:1" in pronom.getTrait() and "number:sing" in pronom.getTrait()  :
            pronom.value = "j'"

        if not negation and mode != "impératif":
            if mode == "subjonctif":
                consubj.dep = "-1"
                verbe.arg_0 = "2"
                punct.dep = "3"
                consubj.value = "qu'" if pronom.value[0] in ("eaiou") else "que"
                value_in = " ".join([consubj.value, pronom.value, verbe.value, punct.value])
                value_out = " ".join([consubj.getTrait(), pronom.getTrait(), verbe.getTrait(), punct.getTrait()])
                if value_in + value_out not in ins_done:
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)
                    if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                    if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                    if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))
            else:
                verbe.arg_0 = "1"
                punct.dep = "2"
                if pronom.value == "je" and élision:
                    pronom.value = "j'"
                value_in = " ".join([pronom.value, verbe.value, punct.value])
                value_out = " ".join([pronom.getTrait(), verbe.getTrait(), punct.getTrait()])
                if value_in + value_out not in ins_done:
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)
                    if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                    if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                    if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))
        elif not negation:

            verbe.arg_0 = "1"
            punct.dep = "1"
            punct.value = "!"
            value_in = " ".join([verbe.value, punct.value])
            value_out = " ".join([verbe.getTrait(), punct.getTrait()])
            if value_in + value_out not in ins_done:
                ins_done.add(value_in + value_out )
                in_.append(value_in)
                out_.append(value_out)
                if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))
        elif mode != "impératif":
            if mode == "subjonctif":
                consubj.dep = "-1"
                verbe.arg_0 = "2"
                if élision :
                    ne.value = "n'"
                else:
                    ne.value = "ne"
                ne.dep = "4"
                pas.dep = "4"
                punct.dep = "4"
                punct.value = "."
                consubj.value = "qu'" if pronom.value[0] in ("eaiou") else "que"
                value_in = " ".join([consubj.value, pronom.value,  ne.value, verbe.value, pas.value, punct.value])
                value_out = " ".join([consubj.getTrait(),  pronom.getTrait(),ne.getTrait(), verbe.getTrait(), pas.getTrait(), punct.getTrait()])
                if value_in + value_out not in ins_done:
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)
                    if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                    if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                    if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))
            else:
                verbe.arg_0 = "1"
                if élision :
                    ne.value = "n'"
                else:
                    ne.value = "ne"
                ne.dep = "3"
                pas.dep = "3"
                punct.value = "!"
                punct.dep = "3"
                value_in = " ".join([pronom.value, ne.value, verbe.value, pas.value, punct.value])
                value_out = " ".join([pronom.getTrait(), ne.getTrait(), verbe.getTrait(), pas.getTrait(), punct.getTrait()])
                if value_in + value_out not in ins_done:
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)
                    if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                    if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                    if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))
        else:
            verbe.arg_0 = "2"
            if élision :
                ne.value = "n'"
            else:
                ne.value = "ne"
            ne.dep = "2"
            pas.dep = "2"
            punct.value = "!"
            punct.dep = "2"
            value_in = " ".join([ne.value, verbe.value, pas.value, punct.value])
            value_out = " ".join([ne.getTrait(), verbe.getTrait(), pas.getTrait(), punct.getTrait()])
            if value_in + value_out not in ins_done:
                ins_done.add(value_in + value_out )
                in_.append(value_in)
                out_.append(value_out)
                if pronom.person not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person] = {}
                if pronom.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number] = {}
                if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number].keys():
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number] = []
                tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom.person][pronom.number][pronom.number].append(([in_[-1]], [out_[-1]]))

        if type_verb == "t" and auxiliaire != ""and verbe.type_arg_1 != "" and verbe.temps== "passé" and verbe.mode== "participe":  # passif


            for pronom_arg_0 in pronoms_arg_0_pass:

                if "humain" not in verbe.type_arg_0 :
                    if pronom_arg_0._person != "person:3":
                        continue
                if "pluriel" in verbe.type_arg_0 :
                    if pronom_arg_0._number != "number:plur":
                        continue
                pronom_arg_0.nature = verbe.type_arg_0
                for in_être, out_être in zip(verb_être['in'], verb_être['out']):
                    if mode != "impératif":
                        #if mode == "subjonctif":
                        if "j" in pronom.value and not "j" in in_être:
                            continue
                        if "tu " in pronom.value and not "tu " in in_être:
                            continue
                        if "il " in pronom.value and not "il " in in_être:
                            continue
                        if "elle " in pronom.value and not "elle " in in_être:
                            continue
                        if "on " in pronom.value and not "on " in in_être:
                            continue
                        if "nous " in pronom.value and not "nous " in in_être:
                            continue
                        if "vous " in pronom.value and not "vous " in in_être:
                            continue
                        if "ils " in pronom.value and not "ils " in in_être:
                            continue
                        if "elles " in pronom.value and not "elles " in in_être:
                            continue
                        if "eux " in pronom.value and not "eux " in in_être:
                            continue
                    if élision:
                        pronom_élidé = Trait()
                        pronom_élidé.setTraits(pronom.getTrait())
                        pronom_non_élidé = Trait()
                        pronom_non_élidé.setTraits(pronom.getTrait())
                        if pronom_non_élidé.value == "j'":
                            pronom_non_élidé.value = "je"
                    else:
                        pronom_élidé = Trait()
                        pronom_élidé.setTraits(pronom.getTrait())
                        pronom_non_élidé = Trait()
                        pronom_non_élidé.setTraits(pronom.getTrait())
                        if pronom_élidé.value == "je":
                            pronom_élidé.value = "j'"

                    value_in = in_être[:]

                    if pronom_élidé.getTrait() in out_être :
                        pronom_courant = Trait()
                        pronom_courant.setTraits(pronom_élidé.getTrait())
                        pronom_courant.gender = verbe.gender
                        value_out = out_être[:].replace(pronom_élidé.getTrait() , pronom_courant.getTrait())
                    elif pronom_non_élidé.getTrait() in out_être :
                        pronom_courant = Trait()
                        pronom_courant.setTraits(pronom_non_élidé.getTrait())
                        pronom_courant.gender = verbe.gender
                        value_out = out_être[:].replace(pronom_non_élidé.getTrait() , pronom_courant.getTrait())
                    else:
                        continue

                    if "humain" not in verbe.type_arg_1:
                        if pronom_élidé._person != "person:3":
                            continue
                    if "pluriel" in verbe.type_arg_1:
                        if pronom_élidé._number != "number:plur":
                            continue
                    if "fem" in verbe.gender:
                        if "masc" in pronom_élidé.gender:
                            continue
                    if "masc" in verbe.gender:
                        if "fem" in pronom_élidé.gender:
                            continue

                else:
                    value_in = in_être[:]
                    value_out = out_être[:]
                value_in = value_in.split()
                value_out = value_out.split()
                punct_final = Trait()
                punct_final.setTraits(value_out.pop(-1))
                old_dep = punct_final.dep[:]
                punct_final.dep = (len(value_in) + 1).__str__()
                verbe.arg_0 = ""
                for i_out in range(len(value_out)):
                    value_out[i_out] = value_out[i_out].split("￨")
                    if value_out[i_out][-2] == old_dep and  value_out[i_out][0] != "n'":
                        value_out[i_out][-2] = punct_final.dep
                    traits = Trait()
                    traits.setTraits(value_out[i_out])
                    if (traits.arg_0 != "" or mode == "impératif") and traits.universal_pos == "verb":
                        arg_0 = traits.arg_0
                        traits.nature = "aux.pass"
                        traits.universal_pos = "aux"
                        traits.dep = punct_final.dep
                        verbe.arg_1 = traits.arg_0
                        traits.arg_1 = ""
                        traits.arg_2 = ""
                        traits.arg_3 = ""
                        traits.type_arg_0 = ""
                        traits.type_arg_1 = ""
                        traits.type_arg_2 = ""
                        traits.type_arg_3 = ""
                        value_out[i_out] = traits.getTrait()
                    value_out[i_out] = "￨".join(value_out[i_out])
                value_in.pop(-1)

                par.dep = punct_final.dep
                verbe.arg_0 = (len(value_in) + 3).__str__()
                value_in = " ".join([" ".join(value_in), verbe.value, par.value, pronom_arg_0.value, punct_final.value])
                value_out = " ".join([" ".join(value_out), verbe.getTrait(), par.getTrait(), pronom_arg_0.getTrait(), punct_final.getTraits()])
                if value_in + value_out not in ins_done:
                    break
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)
                    if pronom_arg_0.value.person + pronom_courant.value not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value] = {}
                    if pronom_arg_0.value.number + pronom_courant.number not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value][pronom_arg_0.value.number + pronom_courant.number] = {}
                    if pronom.gender not in tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value][pronom_arg_0.value.number + pronom_courant.number].keys():
                        tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value][pronom_arg_0.value.number + pronom_courant.number][pronom_arg_0.value.number + pronom_courant.number] = []
                    tab_verbs[lemme][verbe.mode][verbe.temps + negation.__str__()][pronom_arg_0.value.person + pronom_courant.value][pronom_arg_0.value.number + pronom_courant.number][pronom_arg_0.value.number + pronom_courant.number].append(([in_[-1]], [out_[-1]]))

                par.dep = punct_final.dep
                verbe.arg_0 = ''
                value_in = " ".join([" ".join(value_in), verbe.value, punct_final.value])
                value_out = " ".join([" ".join(value_out), verbe.getTrait(), punct_final.getTraits()])
                if value_in + value_out not in ins_done:
                    break
                    ins_done.add(value_in + value_out )
                    in_.append(value_in)
                    out_.append(value_out)

    return in_, out_

curseurLVF.execute(req, {})
rows = curseurLVF.fetchall()

if not os.path.exists("tab_verbs.json"):
    progress = tqdm(range(len(rows)))
    for row in rows:
        progress.update()
        aux = row[-1]
        traits_verb_socle = Trait()
        traits_verb_socle.value = row[0]
        if traits_verb_socle.value not in out_words:
            continue
        traits_verb_socle.universal_pos = "verb"
        traits_verb_socle.type = row[2][0].lower()
        traits_verb_socle.type_arg_0 = _nature[row[3]]
        traits_verb_socle.type_arg_1 = "" if row[4] == "" else _nature[row[4]]
        if row[5] in _préposition.keys():
            traits_verb_socle.type_arg_2 = _préposition[row[5]]
        elif row[5] in _complément.keys():
            traits_verb_socle.type_arg_2 = _complément[row[5]]
        else:
            traits_verb_socle.type_arg_2 = row[5]
        if row[6] in _complément.keys():
            traits_verb_socle.type_arg_3 = _complément[row[6]]
        elif row[6] in _préposition.keys():
            traits_verb_socle.type_arg_3 = _préposition[row[6]]
        else:
            traits_verb_socle.type_arg_3 = row[6]
        traits_verb_socle.mode = "infinitif"

        if traits_verb_socle.value not in verbs:
            verbs[traits_verb_socle.value] = {}  # initialisation d'un type de verbe
        tmp_v = verbs[traits_verb_socle.value]
        if traits_verb_socle.type not in tmp_v.keys():
            tmp_v[traits_verb_socle.type] = {}
        tmp_v = tmp_v[traits_verb_socle.type]
        if traits_verb_socle.type_arg_0 not in tmp_v.keys():
            tmp_v[traits_verb_socle.type_arg_0] = {}
        tmp_v = tmp_v[traits_verb_socle.type_arg_0]
        if traits_verb_socle.type_arg_1 not in tmp_v.keys():
            tmp_v[traits_verb_socle.type_arg_1] = {}
        tmp_v = tmp_v[traits_verb_socle.type_arg_1]
        if traits_verb_socle.type_arg_2 not in tmp_v.keys():
            tmp_v[traits_verb_socle.type_arg_2] = {}
        tmp_v = tmp_v[traits_verb_socle.type_arg_2]
        if traits_verb_socle.type_arg_3 not in tmp_v.keys():
            tmp_v[traits_verb_socle.type_arg_3] = []
        tmp_v[traits_verb_socle.type_arg_3] = [traits_verb_socle]

        req = "select verb_details.verb_id, verb_details.form, verb_details.tense , verb_details.mood , verb_details.person , verb_details.number , verb_details.aspect , verb_details.gender , verb_details.mode , verb_details.temps , verb_details.elision, verb.elision from verb_details, verb where verb.id = verb_details.verb_id and verb.lemma = :lemma "

        détails = curseurLVFdétails.execute(req, {'lemma':traits_verb_socle.value})



        for détail in détails:
            values_verb = {}
            traits_verb = Trait()
            traits_verb.setTraits(traits_verb_socle.getTrait())


            for i in range(len(columns)):
                values_verb[en_tetes[i]] = détail[i]

            if values_verb["form"] not in out_words:
                continue

            traits_verb.gender = "gender:com" if values_verb["gender"] == 'all' else  "gender:"+ values_verb["gender"]
            if traits_verb.gender == "gender:feminine":
                traits_verb.gender = "gender:fem"
            if traits_verb.gender == "gender:masculine":
                traits_verb.gender = "gender:masc"

            if traits_verb_socle.type == "p":
                index_verb = 3
            else:
                index_verb = 2
            mode_1 = values_verb["mode"]
            temps_1 = values_verb["temps"]

            impersonnel = False
            if aux == "" or (traits_verb.type_arg_0 == "complétive ou chose"):
                impersonnel = True
                if values_verb["person"] != "3":
                    continue

            if traits_verb.type_arg_0 in ("complétive ou chose", "complétive ou inf", "pluriel humain", "pluriel chose"):
                if values_verb["number"] != "singular":
                    continue

            temps_modes = [(0, mode_1, temps_1)]

            if mode_1 == 'participe' and temps_1 == 'passé':
                temps_modes.append((len(temps_modes), 'conditionnel','passé'))
                temps_modes.append((len(temps_modes), 'indicatif','futur-antérieur'))
                temps_modes.append((len(temps_modes), 'indicatif','passé-antérieur'))
                temps_modes.append((len(temps_modes), 'indicatif','passé-composé'))
                temps_modes.append((len(temps_modes), 'indicatif','plus-que-parfait'))
                temps_modes.append((len(temps_modes), 'subjonctif','passé'))
                temps_modes.append((len(temps_modes), 'plus-que-parfait','passé'))
                temps_modes.append((len(temps_modes), 'gérondif','passé'))
                temps_modes.append((len(temps_modes), 'participe','passé-composé'))

            if values_verb["elision"] is None:
                values_verb["elision"] = détail[-1]
            values_verb["elision"] = eval(values_verb["elision"])

            for i, mode, temps in temps_modes:
                traits_verb.temps = temps
                traits_verb.mode = mode
                traits_verb.value = values_verb["form"]

                pronom_courant = None
                if traits_verb.mode not in ('infinitif', "participe"):
                    if i == 0:
                        pronom_courant = Trait()
                        pronom_courant.setTraits(pronom.getTrait())
                        pronom_courant.value = pronoms[values_verb["number"]][values_verb["person"]]
                        if values_verb["person"] in ("1", "2"):
                            pronom_courant.nature = "humain"
                            pronom_courant.person = "person:" + values_verb["person"]
                        else:
                            pronom_courant.nature = traits_verb.type_arg_0
                        if values_verb["number"] == "plural":
                            pronom_courant.number = "number:plur"
                        else:
                            pronom_courant.number = "number:sing"
                        in_, ou_ = makeInOu(pronom_courant, traits_verb_socle.type, None, traits_verb, True, values_verb["elision"], impersonnel, traits_verb_socle.value)
                        inputs += in_
                        outputs += ou_
                        add(traits_verb_socle.value, in_, ou_)
                        in_, ou_ = makeInOu(pronom_courant, traits_verb_socle.type, None, traits_verb, False, values_verb["elision"], impersonnel, traits_verb_socle.value)
                        inputs += in_
                        outputs += ou_
                        add(traits_verb_socle.value, in_, ou_)
                    else:
                        in_, ou_ = makeInOu(pronom_courant, traits_verb_socle.type,  aux, traits_verb, True, values_verb["elision"], impersonnel, traits_verb_socle.value)
                        inputs += in_
                        outputs += ou_
                        add(traits_verb_socle.value, in_, ou_)
                        in_, ou_ = makeInOu(pronom_courant, traits_verb_socle.type, aux, traits_verb, False, values_verb["elision"], impersonnel, traits_verb_socle.value)
                        inputs += in_
                        outputs += ou_
                        add(traits_verb_socle.value, in_, ou_)
                else:
                    in_, ou_ = makeInOu(None, traits_verb_socle.type, aux, traits_verb, True, values_verb["elision"], impersonnel, traits_verb_socle.value)
                    inputs += in_
                    outputs += ou_
                    add(traits_verb_socle.value, in_, ou_)
                    in_, ou_ = makeInOu(None, traits_verb_socle.type, aux, traits_verb, False, values_verb["elision"], impersonnel, traits_verb_socle.value)
                    inputs += in_
                    outputs += ou_
                    add(traits_verb_socle.value, in_, ou_)

    progress.__init_subclass__()
    if wrtite_ae:
        fjson = open("être.json", "w")
        fjson.write(json.dumps(verb_être, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        fjson = open("avoir.json", "w")
        fjson.write(json.dumps(verb_avoir, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

    print("save tab_verbs", file=sys.stderr)
    fjson = open("tab_verbs.json", "w")
    fjson.write(json.dumps(tab_verbs, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    print("tab_verbs saved", file=sys.stderr)
    print("save inputsCG", file=sys.stderr)
    with open("inputsCG.txt", 'w') as w_inputs, open("outputsCG.txt", 'w') as w_outputs:
        w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
        w_outputs.writelines([k+ "\n" for k in outputs if k != ""])
    print("inputsCG save", file=sys.stderr)

inputs = []
outputs = []

if os.path.exists("definitions.json"):
    print("load definitions", file=sys.stderr)
    a = open("definitions.json", "r")
    definitions = json.load(a)
    a.close()
    print("words_nature loaded", file=sys.stderr)
    print("load words_nature", file=sys.stderr)
    a = open("words_nature.json", "r")
    words_nature = json.load(a)
    a.close()
    print("words_nature loaded", file=sys.stderr)
else:
    definitions = {}
    words_nature = {}

    req = "select mot, type, elision from dem where type != '' and genre != '' and categorie = 'N' "
    curseurLVF.execute(req, {})
    words = []
    rows = curseurLVF.fetchall()
    for row in rows:
        mot_ = row[0]

        if mot_ not in out_words:
            continue
        type_ = row[1]
        elision_ = row[2]
        words.append((mot_, type_, elision_))

    dét_tr = Trait()
    dét_tr.setTraits("le￨det￨gender:masc￨number:sing￨￨￨person:3￨￨￨￨￨￨￨￨￨2￨défini")

    mot_tr = Trait()
    mot_tr.setTraits("argevent_1￨noun￨gender:masc￨number:sing￨￨￨￨￨￨￨￨￨￨￨￨￨")

    for word in words:
        req = "select distinct word_details.form, word_details.grammaticalGender, word_details.grammaticalNumber from word, word_details where grammaticalCategory = 'commonNoun' and lemma = :lemma and word_details.word_id = word.id and grammaticalGender != '' and grammaticalNumber != ''"
        req = "select distinct word_details.form, word_details.grammaticalGender, word_details.grammaticalNumber from word, word_details where lemma = :lemma and word_details.word_id = word.id and grammaticalGender != '' and grammaticalNumber != ''"
        # req += " union "
        # req += "select * from word_details where word_details.word_id = (select id from word where lemma = :lemma and grammaticalCategory='commonNoun') and grammaticalGender != '' and grammaticalNumber != '' "

        if word[0] not in out_words:
            continue
        curseurLVF.execute(req, {'lemma':word[0]})
        definitions[word[0]] = []
        type_ =  word[1]
        elision_ = word[2]
        rows = curseurLVF.fetchall()
        mot_lem = Trait()
        mot_lem.setTraits(mot_tr.getTrait())
        mot_lem.value = word[0]

        if mot_lem.value not in out_words:
            continue
        for row in rows:
            déterminant = Trait()
            déterminant.setTraits(dét_tr.getTrait())
            mot_traité = Trait()
            mot_traité.setTraits(mot_tr.getTrait())
            mot_ = row[0]

            if mot_ not in out_words:
                continue

            grammaticalGender = row[1]
            grammaticalNumber = row[2]
            if grammaticalGender == "masculine":
                déterminant.value = "le"
                déterminant.gender = "gender:masc"
            else:
                déterminant.value = "la"
                déterminant.gender = "gender:fem"
            mot_traité.gender = déterminant.gender
            if grammaticalNumber == "singular":
                mot_traité.number = "number:sing"
                déterminant.number = "number:sing"
                if elision_ == 'True' and not mot_[0] == "y":
                    déterminant.value = "l'"
            else:
                déterminant.value = "les"
                déterminant.number = "number:plur"
                mot_traité.number = "number:plur"
            mot_traité.gender = déterminant.gender
            mot_traité.number = déterminant.number
            mot_traité.value = mot_
            mot_traité.nature = type_
            if mot_traité.value not in words_nature.keys():
                words_nature[mot_traité.value] = []
            words_nature[mot_traité.value].append(mot_traité.getTrait())
            inputs.append(" ".join([déterminant.value, mot_traité.value]))
            outputs.append(" ".join([déterminant.getTrait(), mot_traité.getTrait()]))
            inputs.append(" ".join([mot_traité.value, ]))
            outputs.append(" ".join([mot_traité.getTrait(), ]))

            inputs.append(" ".join(("%form%", mot_lem.value)))
            outputs.append(" ".join((mot_traité.getTrait(), )))
            inputs.append(" ".join(("%lemma%", mot_traité.value)))
            outputs.append(" ".join((mot_lem.getTrait(), )))
            definitions[word[0]].append((mot_lem.getTrait(), mot_traité.value, mot_traité.getTrait()))

    print("save definitions", file=sys.stderr)
    fjson = open("definitions.json", "w")
    fjson.write(json.dumps(definitions, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    print("save words_nature", file=sys.stderr)
    fjson = open("words_nature.json", "w")
    fjson.write(json.dumps(words_nature, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    print("CGD saved", file=sys.stderr)
    print("save definitions", file=sys.stderr)
    with open("inputsCGD.txt", 'w') as w_inputs, open("outputsCGD.txt", 'w') as w_outputs:
        w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
        w_outputs.writelines([k+ "\n" for k in outputs if k != ""])
    print("CGD saved", file=sys.stderr)

inputs = []
outputs = []
if (not os.path.exists("inputsCV.txt") or not os.path.exists("inputsCGS.txt")):
    domaines = []

    if os.path.exists("tab_def.json"):
        print("load tab_def", file=sys.stderr)
        a = open("tab_def.json", "r")
        tab_def, domaines = json.load(a)
        a.close()
        print("tab_def loaded", file=sys.stderr)
    else:
        tab_def = {}

        for definition in definitions.keys():
            for lemme_, mot, traits in definitions[definition]:
                lemme = Trait()
                lemme.setTraits(lemme_)
                if len(lemme.value) == 1:
                    continue
                req = "select domaine, sens from dem where mot = :lemma and categorie= 'N'"

                if lemme.value not in out_words:
                    continue
                rows = curseurLVF.execute(req, {'lemma':lemme.value})
                for row in rows:
                    domaine = Trait()
                    domaine.value = '%'+row[0].replace(' ', '_')+'%'
                    domaine.nature = "domaine"
                    sens = row[1]
                    if mot +  domaine.value  not in domaines:
                        inputs.append(" ".join(("%domaine%", mot)))
                        outputs.append(" ".join((domaine.getTrait(), traits)))
                        domaines.append(mot + domaine.value)
                    if mot not in tab_def:
                        tab_def[mot] = []
                    valeurs = sens.split(',')
                    if " " in valeurs[0]:
                        for valeur in range(len(valeurs[1:])):
                            valeurs[valeur+1] = valeurs[0].split(" ") +[valeurs[valeur+1]]
                            valeurs[valeur + 1].pop(-2)
                            valeurs[valeur+1] = " ".join(valeurs[valeur+1])

                    tab_def[mot].append((domaine.getTrait(), valeurs))

        print("save tab_def", file=sys.stderr)
        fjson = open("tab_def.json", "w")
        fjson.write(json.dumps((tab_def, domaines), ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

    variations = []
    variations_2 = []
    tab_values = {}
    for definition, _ in zip(definitions.keys(),  tqdm(range(len(definitions.keys())))):
        for lemme_, mot, traits in definitions[definition]:
            lemme = Trait()
            lemme.setTraits(lemme_)
            if len(lemme.value) == 1:
                continue

            if lemme.value not in out_words:
                continue
            found_ = False
            for domaine_, sens_decoup in tab_def[mot]:
                domaine = Trait()
                domaine.setTraits(domaine_)
                dico = {'lemma':lemme.value}
                sens_complet = ",".join(sens_decoup)
                dico['sens'] = sens_complet
                req = "select distinct  mot, domaine, sens from dem where categorie= 'N' and mot != :lemma and  (%sens% or mot=:sens) "
                if len(sens_decoup) == 1:
                    req = req.replace("%sens%", "sens like '"+sens_decoup[0].replace("'", "_") + "'")
                else:
                    sens_ = "("
                    sep = ""
                    for s_sens in sens_decoup:
                        sens_ += sep + " sens like '%" + s_sens.replace("'", "_") + "%'"
                        sep = " or "
                    sens_ += ") " #pas de "!" dans les sens du dem
                    req = req.replace("%sens%", sens_)
                if req not in tab_values:
                    détails = curseurLVFdétails.execute(req, dico)
                    tab_values[req] = détails.fetchall()
                détails = tab_values[req]
                for détail in détails:
                    mot_2 = détail[0]

                    if mot_2 not in out_words:
                        continue
                    domaine_2 = détail[1]
                    sens_2 = détail[2]
                    if mot_2 in sens_decoup:
                        if mot_2 in definitions.keys():
                            for lemme_2_, mot_2, traits_2 in definitions[mot_2]:

                                traits_2_ = Trait()
                                traits_2_.setTraits(traits_2)
                                if traits_2_.value not in out_words:
                                    continue
                                traits_2_.nature = domaine.value
                                traits_ = Trait()
                                traits_.setTraits(traits)
                                if traits_2_.number == traits_.number:
                                    if traits_2_.gender == traits_.gender:

                                        if mot not in out_words:
                                            continue
                                        if traits_2_.value not in out_words:
                                            continue
                                        if (mot, traits_2_.getTrait()) in variations:
                                            found_ = True
                                            continue
                                        inputs.append(" ".join(("%variation%", mot)))
                                        outputs.append(" ".join((traits_2_.getTrait(), )))
                                        variations.append((mot, traits_2_.getTrait()))
                                        found_ = True
            if not found_ :

                if mot not in out_words:
                    continue
                if (mot, traits) in variations:
                    continue

                traits_tmp = Trait()
                traits_tmp.setTraits(traits)
                traits_tmp.nature = domaine.value
                variations_2.append((mot, traits_tmp.getTrait()))

    with open("inputsCV.txt", 'w') as w_inputs, open("outputsCV.txt", 'w') as w_outputs:
        w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
        w_outputs.writelines([k+ "\n" for k in outputs if k != ""])
    print("inputsCV outed")

    inputs = []
    outputs = []
    domaines_var = {}
    for variation in variations:
        domaine = variation[1].split("￨")[-1]
        if domaine not in domaines_var.keys():
            domaines_var[domaine] = []
        domaines_var[domaine].append(variation)
    domaines_var2 = {}
    for variation in variations_2:
        domaine = variation[1].split("￨")[-1]
        if domaine not in domaines_var2.keys():
            domaines_var2[domaine] = []
        domaines_var2[domaine].append(variation)

    print("shuffle variations")
    print(len(variations), end=" variations.")
    print(len(variations_2), end=" variations_2.")
    for _ in range(400000):
        domaine = random.choice(list(domaines_var.keys()))
        if domaine in domaines_var2.keys():
            a= random.choices(domaines_var[domaine], k=random.randint(5, 25))
            b= random.choices(domaines_var2[domaine], k=random.randint(1, 4))
        else:
            a= random.choices(domaines_var[domaine], k=random.randint(5, 29))
        x = [a_ for a_ in a] +  [b_ for b_ in b]
        random.shuffle(x)
        c =  [a_[0] for a_ in x]
        d =  [a_[1] for a_ in x]
        c.insert(0, "%variation%")
        inputs.append(" ".join(c))
        outputs.append(" ".join(d))

    with open("inputsCGS.txt", 'w') as w_inputs, open("outputsCGS.txt", 'w') as w_outputs:
        w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
        w_outputs.writelines([k+ "\n" for k in outputs if k != ""])


inputs = []
outputs = []

print("verbs variations", file=sys.stderr)


if not os.path.exists("inputsCGV.txt") or True:
    dones_v = set()
    tab_verbs_available = set()
    req = "select distinct mot, domaine, sens, categorie from dem where categorie like 'V%'"
    rows = curseurLVF.execute(req, {}).fetchall()
    sens_verbes = {}
    for row in rows:

        if row[0] not in out_words:
            continue
        if row[3] == "Vp":
            tab_verbs_available.add("se " + row[0] if "se " + row[0] in tab_verbs.keys() else "s'" + row[0] if "s'" + row[0] in tab_verbs.keys() else "s' " + row[0] if "s' " + row[0] in tab_verbs.keys() else None)
        else:
            tab_verbs_available.add(row[0])
    if None in tab_verbs_available:
        tab_verbs_available.remove(None)

    commons_meaning = {}
    for row in rows:


        if row[0] not in out_words:
            continue

        domaine = row[1]
        sens_ = row[2]
        catégorie = row[3]


        if catégorie != "Vp":
            verbe = row[0]
        else:
            verbe = "se " + row[0] if "se " + row[0] in tab_verbs.keys() else "s'" + row[0] if "s'" + row[0] in tab_verbs.keys() else "s' " + row[0] if "s' " + row[0] in tab_verbs.keys() else None

        if verbe is not None:
            if sens_ not in sens_verbes:
                sens_verbes[sens_] = []
            sens_verbes[sens_].append((verbe, domaine, catégorie))
            if verbe not in commons_meaning:
                commons_meaning[verbe] = []
            commons_meaning[verbe].append(sens_)
            for sens in sens_.split(","):
                if sens_ == sens:
                    continue
                if sens not in sens_verbes:
                    sens_verbes[sens] = []
                sens_verbes[sens].append((verbe, domaine, catégorie))
                commons_meaning[verbe].append(sens)

                if verbe not in tab_verbs.keys() and " " in verbe[3:]:
                    if sens in tab_verbs.keys():
                        for input_, output_ in tab_verbs[sens]["infinitif"]['False']['']['']['']:
                            input_ = input_[0]
                            output_ = output_[0]
                            if len(input_.split()) == 2:
                                if verbe.startswith("s'"):
                                    if verbe[2] != " ":
                                        verbe.insert(2, " ")
                                if " ".join(("%variation%", verbe + " .")) + output_ not in dones_v:
                                    inputs.append(" ".join(("%variation%", verbe + " .")))
                                    outputs.append(output_)
                                    dones_v.add(inputs[-1]+outputs[-1])

            if verbe not in tab_verbs.keys() and " " in verbe[3:]:
                if sens_ in tab_verbs.keys():
                    for input_, output_ in tab_verbs[sens_]["infinitif"]['False']['']['']['']:
                        input_ = input_[0]
                        output_ = output_[0]
                        if len(input_.split()) == 2:
                            if verbe.startswith("s'"):
                                if verbe[2] != " ":
                                    verbe.insert(2, " ")
                            if " ".join(("%variation%", verbe + " .")) + output_ not in dones_v:
                                inputs.append(" ".join(("%variation%", verbe + " .")))
                                outputs.append(output_)
                                dones_v.add(inputs[-1]+outputs[-1])


    progress = tqdm(range(len(list(tab_verbs.keys()))))
    for lemme in tab_verbs.keys():

        if lemme not in out_words:
            continue

        progress.update()
        if lemme not in commons_meaning.keys():
            continue
        lemme_ = lemme.split()
        for mode in tab_verbs[lemme].keys():
            for temps_neg in tab_verbs[lemme][mode].keys():
                for person in tab_verbs[lemme][mode][temps_neg].keys():
                    for number in tab_verbs[lemme][mode][temps_neg][person].keys():
                        for key in tab_verbs[lemme][mode][temps_neg][person][number].keys():
                            for input_, output_ in tab_verbs[lemme][mode][temps_neg][person][number][key]:
                                input_ = input_[0]
                                output_ = output_[0]

                                for sens in  commons_meaning[lemme]:
                                    for mot, domaine, catégorie in sens_verbes[sens]:
                                        if mot == lemme or mot.endswith(lemme) or lemme.endswith(mot):
                                            continue
                                        if mot not in tab_verbs.keys() :
                                            continue

                                        if mode not in tab_verbs[mot].keys():
                                            continue
                                        if temps_neg not in tab_verbs[mot][mode].keys():
                                            continue
                                        if person not in tab_verbs[mot][mode][temps_neg].keys():
                                            continue
                                        if number not in tab_verbs[mot][mode][temps_neg][person].keys():
                                            continue
                                        if key not in tab_verbs[mot][mode][temps_neg][person][number].keys():
                                            continue
                                        for input_2, output_2 in tab_verbs[mot][mode][temps_neg][person][number][key]:
                                            input_2 = input_2[0]
                                            output_2 = output_2[0]
                                            if "aux.pass" in output_ and not "aux.pass" in  output_2:
                                                continue
                                            if "aux.pass" in output_2 and not "aux.pass" in output_:
                                                continue
                                            if " ".join(("%variation%", input_)) + output_2 not in dones_v:
                                                inputs.append(" ".join(("%variation%", input_)))
                                                outputs.append(output_2)
                                                dones_v.add( " ".join(("%variation%", input_)) + output_2)

    with open("inputsCGV.txt", 'w') as w_inputs, open("outputsCGV.txt", 'w') as w_outputs:
        w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
        w_outputs.writelines([k+ "\n" for k in outputs if k != ""])
inputs = []
outputs = []

print("adjectifs")
req = "select mot, sens, domaine from dem where categorie = 'A' and (not mot like '% %') and sens not like '% %' and sens not like '% %'  and contenu != 'N q rési à' "

détails = curseurLVFdétails.execute(req, {})
détails = détails.fetchall()
adjs_nature = {}
adjectif = Trait()
adjectif.setTraits("rebelle￨adj￨gender:masc￨number:sing￨￨￨￨￨￨￨￨￨￨￨￨￨")
for détail in détails:
    mot = détail[0]

    if mot not in out_words:
        continue

    sens = détail[1]
    domaine = détail[2].replace(' ', "_")
    for sens_ in sens.split(","):
        if ")" in sens_:
            sens_ = sens_[sens_.index(")")+1:]
        req = "select mot, domaine from dem where categorie = 'A' and mot = :mot"
        synonymes = curseurLVFdétails.execute(req, {"mot":sens_})
        synonymes = synonymes.fetchall()
        req = "select word_details.form, word_details.grammaticalGender, word_details.grammaticalNumber from word_details, word " \
              "where word.grammaticalCategory='adjective' and word.id = word_details.word_id and lemma=:lemma "
        variations_1 = curseurLVFdétails.execute(req, {"lemma": mot})
        variations_1 = variations_1.fetchall()
        dones = set()
        for form, grammaticalGender, grammaticalNumber in variations_1:

            if form not in out_words:
                continue

            if form + grammaticalGender + grammaticalNumber  not in dones:
                dones.add(form + grammaticalGender + grammaticalNumber )
                for synonyme in synonymes:
                    req = "select word_details.form, word_details.grammaticalGender, word_details.grammaticalNumber from word_details, word where " \
                          "word.grammaticalCategory='adjective' and word.id = word_details.word_id and lemma=:lemma " \
                          "and grammaticalGender =:grammaticalGender and  grammaticalNumber = :grammaticalNumber"
                    variations_2 = curseurLVFdétails.execute(req, {"lemma": synonyme[0], 'grammaticalGender':grammaticalGender, 'grammaticalNumber':grammaticalNumber})
                    variations_2 = variations_2.fetchall()
                    for form_2, grammaticalGender_2, grammaticalNumber_2 in variations_2:

                        if form_2 not in out_words:
                            continue

                        inputs.append(" ".join(("%variation%", form)))
                        adjectif.value = form_2
                        adjectif.number = "number:plur"  if grammaticalNumber == "plural" else "number:sing"  if grammaticalNumber == "singular" else ""
                        adjectif.gender = "gender:fem"  if grammaticalGender == "feminine" else "gender:masc" if grammaticalGender == "masculine" else ""
                        adjectif.nature = synonyme[1]
                        outputs.append(adjectif.getTrait())

req = "select distinct mot, domaine from dem where categorie = 'A' and (not mot like '% %') and contenu != 'N q rési à' "

détails = curseurLVFdétails.execute(req, {})
détails = détails.fetchall()
adjs_nature = {}
adjectif = Trait()
adjectif.setTraits("rebelle￨adj￨gender:masc￨number:sing￨￨￨￨￨￨￨￨￨￨￨￨￨")
for détail in détails:
    mot = détail[0]

    if mot not in out_words:
        continue

    domaine = détail[1]
    req = "select word_details.form, word_details.grammaticalGender, word_details.grammaticalNumber from word_details, word " \
              "where word.grammaticalCategory='adjective' and word.id = word_details.word_id and lemma=:lemma "
    data = curseurLVFdétails.execute(req, {"lemma": mot})
    data = data.fetchall()
    for form, grammaticalGender, grammaticalNumber in data:

        if form not in out_words:
            continue
        adjectif.value = form
        adjectif.number = "number:plur" if grammaticalNumber == "plural" else "number:sing" if grammaticalNumber == "singular" else ""
        adjectif.gender = "gender:fem" if grammaticalGender == "feminine" else "gender:masc" if grammaticalGender == "masculine" else ""
        adjectif.nature = domaine.replace(' ', "_")
        inputs.append(form)
        outputs.append(adjectif.getTrait())
        if form not in adjs_nature.keys():
            adjs_nature[form] = []
        adjs_nature[form].append(adjectif.getTrait())
        inputs.append(" ".join(("%lemma%", form)))
        adjectif.value = mot
        outputs.append(" ".join((adjectif.getTrait(),)))

for form in adjs_nature.keys():

    if form not in out_words:
        continue
    adjs_nature[form] = set(adjs_nature[form] )
    adjs_nature[form] = list(adjs_nature[form] )

print("save adjs_nature", file=sys.stderr)
fjson = open("adjs_nature.json", "w")
fjson.write(json.dumps(adjs_nature, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

adv_nature = {}
ne.setTraits("ne￨adv￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨indéfini")
req = "select distinct mot, domaine from dem where categorie = 'Adv' and (not mot like '% %')  "

détails = curseurLVFdétails.execute(req, {})
détails = détails.fetchall()
adjs_nature = {}
for détail in détails:
    mot = détail[0]

    if mot not in out_words:
        continue
    domaine = détail[1]
    inputs.append(mot)
    ne.value = mot
    ne.nature = domaine.replace(' ', "_")
    outputs.append(ne.getTrait())
    if mot not in adv_nature.keys():
        adv_nature[mot] = []
    adv_nature[mot].append(ne.getTrait())

print("save adv_nature", file=sys.stderr)
fjson = open("adv_nature.json", "w")
fjson.write(json.dumps(adv_nature, ensure_ascii=False))
fjson.flush()
fjson.close()

while not fjson.closed:
    True
with open("inputsCGA.txt", 'w') as w_inputs, open("outputsCGA.txt", 'w') as w_outputs:
    w_inputs.writelines([k+ "\n" for k in inputs if k != ""])
    w_outputs.writelines([k+ "\n" for k in outputs if k != ""])
