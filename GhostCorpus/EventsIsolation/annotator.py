import os
import json
import sys

in_train_lines = []
in_test_lines = []
in_val_lines = []

out_train_lines = []
out_test_lines = []
out_val_lines = []
files = []
files.append(("inputsCG.txt", "outputsCG.txt") )
files.append(("inputsCGD.txt", "outputsCGD.txt"))
files.append(("inputsCV.txt", "outputsCV.txt"))
files.append(("inputsCGV.txt", "outputsCGV.txt"))
files.append(("inputsCGS.txt", "outputsCGS.txt"))
files.append(("inputsCGA.txt", "outputsCGA.txt"))

for in_f, out_ in files:
    priin = False
    if os.path.exists(in_f):
        print(in_f.replace("inputs", "").replace(".txt", ""))
        fichier_toks = open(in_f, 'r')
        fichier_sent = open(out_, 'r')
        priin = False
        for toks, sent in zip(fichier_toks, fichier_sent):
            toks = toks.strip()
            sent = sent.strip()
            if len(toks.split()) != len(sent.split()):
                continue
            if toks.startswith("%"):
                continue
            if sent.startswith("%"):
                continue
            in_train_lines.append(toks)
            out_train_lines.append(sent)
            if not priin:
                priin = True
                print(in_train_lines[-1])
                print(out_train_lines[-1])

print("load words_nature", file=sys.stderr)
a = open("words_nature.json", "r")
words_nature = json.load(a)
a.close()
print("words_nature loaded", file=sys.stderr)

adjs_nature = {}
if os.path.exists("adjs_nature.json"):
    print("load adjs_nature.json", file=sys.stderr)
    a = open("adjs_nature.json", "r")
    adjs_nature = json.load(a)
    a.close()
    print("adjs_nature.json loaded", file=sys.stderr)

adv_nature = {}
if os.path.exists("adv_nature.json"):
    print("load adv_nature.json", file=sys.stderr)
    a = open("adv_nature.json", "r")
    adv_nature = json.load(a)
    a.close()
    print("adv_nature.json loaded", file=sys.stderr)

for step in ("train", "test", "val"):
    priin = False
    print(step)#/home/colineem/Dropbox/local/git/emilie/analyseurs//compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/train_outputs.txt
    fichier_toks = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_outputs.txt"
    fichier_sent = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_sentence_outputs.txt"


    fichier_toks = open(fichier_toks, 'r')
    fichier_sent = open(fichier_sent, 'r')
    for toks, sent in zip(fichier_toks, fichier_sent):
        toks = toks.strip().split()
        sent = sent.strip().split()

        if len(toks) != len(sent):
            continue

        if False:
            events = []
            has_punt = False
            for i_, sent_ in zip(range(len(toks)), sent):
                if toks[i_].endswith("event"):
                    events.append((i_+1).__str__())
                    if has_punt and  toks[i_].split('￨')[-2] in ("", "-") and len(events) > 0:
                        if toks[i_].split('￨')[-2] in ("", "-") and len(events) > 0:
                            toks[i_] = toks[i_].split('￨')
                            toks[i_][-2] = events[-1]
                            toks[i_] = '￨'.join(toks[i_])
                if ("￨punct￨" in toks[i_] or "￨cconj￨" in toks[i_]):
                    has_punt = True
                    if toks[i_].split('￨')[-2] in ("", "-") and len(events) > 0:
                        toks[i_] = toks[i_].split('￨')
                        if sent_ in (".", "!", "?"):
                            toks[i_][-2] = events[0]
                        else:
                            toks[i_][-2] = events[-1]
                        toks[i_] = '￨'.join(toks[i_])
                toks[i_] = sent_ + toks[i_][toks[i_].index('￨'):]

        elts = {}
        adjs = {}
        advs = {}
        for i_tok in range(len(sent)):
            toks[i_tok] = sent[i_tok]+toks[i_tok][toks[i_tok].index('￨'):]
            if sent[i_tok] in words_nature.keys():
                for value in set(words_nature[sent[i_tok]]):
                    if i_tok not in elts.keys():
                        elts[i_tok] = []
                    elts[i_tok].append(value)
        for i_tok in range(len(sent)):
            toks[i_tok] = sent[i_tok]+toks[i_tok][toks[i_tok].index('￨'):]
            if sent[i_tok] in adjs_nature.keys():
                for value in set(adjs_nature[sent[i_tok]]):
                    if i_tok not in adjs.keys():
                        adjs[i_tok] = []
                    adjs[i_tok].append(value)
        for i_tok in range(len(sent)):
            toks[i_tok] = sent[i_tok]+toks[i_tok][toks[i_tok].index('￨'):]
            if sent[i_tok] in adv_nature.keys():
                for value in set(adv_nature[sent[i_tok]]):
                    if i_tok not in advs.keys():
                        advs[i_tok] = []
                    advs[i_tok].append(value)

        toks_base = {}
        for i_tok in elts.keys():
            if "￨noun￨" in toks[i_tok] :
                toks_base[toks[i_tok]] = {}
                toks_base[toks[i_tok]] = []
                for value in elts[i_tok]:
                    tokens = toks[i_tok].split('￨')
                    value = value.replace(' ', "_").split('￨')
                    tokens[-1] = value[-1]
                    tokens[2] = value[2]
                    tokens[3] = value[3]
                    tokens = '￨'.join(tokens)
                    toks_base[toks[i_tok]].append((i_tok, tokens))
        for i_tok in adjs.keys():
            if "￨adj￨" in toks[i_tok] :
                toks_base[toks[i_tok]] = {}
                toks_base[toks[i_tok]] = []
                for value in adjs[i_tok]:
                    tokens = toks[i_tok].split('￨')
                    value = value.replace(' ', "_").split('￨')
                    tokens[-1] = value[-1]
                    tokens[2] = value[2]
                    tokens[3] = value[3]
                    tokens = '￨'.join(tokens)
                    toks_base[toks[i_tok]].append((i_tok, tokens))
        for i_tok in advs.keys():
            if "￨adv￨" in toks[i_tok] :
                toks_base[toks[i_tok]] = {}
                toks_base[toks[i_tok]] = []
                for value in advs[i_tok]:
                    tokens = toks[i_tok].split('￨')
                    value = value.replace(' ', "_").split('￨')
                    tokens[-1] = value[-1]
                    tokens[2] = value[2]
                    tokens[3] = value[3]
                    tokens = '￨'.join(tokens)
                    toks_base[toks[i_tok]].append((i_tok, tokens))

        def replace_(string_out, dico):
            if len(dico.keys()) == 0:
                return
            string_retour = []
            keys = list(dico.keys())
            dico_2 = dico.copy()
            in_ = dico_2[keys[0]]
            del dico_2[keys[0]]
            for i_tok, out_ in in_:
                if len(dico_2.keys()) == 0:
                    retour = string_out.split()
                    retour[i_tok] = out_
                    string_retour.append(" ".join(retour))
                else:
                    retour = string_out.split()
                    retour[i_tok] = out_
                    string_retour += replace_(" ".join(retour), dico_2)
            return string_retour

        retours = replace_(" ".join(toks).lower(), toks_base)

        if retours is None or len(retours) == 0:
            while "en,-en,-dans" in toks:
                toks = toks.replace("en,-en,-dans", "en,-dans")
        else:
            for i_r in range(len(retours)):
                while "en,-en,-dans" in retours[i_r]:
                    retours[i_r] = retours[i_r].replace("en,-en,-dans", "en,-dans")

        if retours is None or len(retours) == 0:
            sent = " ".join(sent).lower()
            toks = " ".join(toks).lower()
            in_train_lines.append(sent)
            out_train_lines.append(toks)
        else:
            in_train_lines += [" ".join(sent).lower()]*len(retours)
            out_train_lines += retours
        if step == "test":
            if retours is None or len(retours) == 0:
                sent = " ".join(sent).lower()
                toks = " ".join(toks).lower()
                in_test_lines.append(sent)
                out_test_lines.append(toks)
            else:
                in_test_lines += [" ".join(sent).lower()]*len(retours)
                out_test_lines += retours
        elif step != "train":
            if retours is None or len(retours) == 0:
                sent = " ".join(sent).lower()
                toks = " ".join(toks).lower()
                in_val_lines.append(sent)
                out_val_lines.append(toks)
            else:
                in_val_lines += [" ".join(sent).lower()]*len(retours)
                out_val_lines += retours
        if not priin:
            priin = True
            print(in_train_lines[-1])
            print(out_train_lines[-1])

if not os.path.exists('annotator/data'):
    os.makedirs("annotator/data")
if not os.path.exists('annotator/logs'):
    os.makedirs("annotator/logs")
with open('annotator/data/train-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_train_lines)
with open('annotator/data/train-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_train_lines)
with open('annotator/data/valid-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_val_lines)
with open('annotator/data/valid-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_val_lines)
with open('annotator/data/test-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_test_lines)
with open('annotator/data/test-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_test_lines)