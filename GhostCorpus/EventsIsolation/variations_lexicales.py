__version__ = 2.8
import os
import json
import sys
import random
import glob

in_train_lines = []
in_test_lines = []
in_val_lines = []

out_train_lines = []
out_test_lines = []
out_val_lines = []

allowed_words = set()

__ref_dir__ = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-full-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/data"

fichiers = sorted(glob.glob(__ref_dir__ + '/test.tgt.dict'), key=lambda fichier:int(fichier.split("_")[-1].split(".")[0]))
for fichier in fichiers:
    for line in open(fichier):
        if not line.startswith(" ") and "<" not in line and ">" not in line:
            allowed_words.add(line.split()[0])

allowed_words = set()

files = []
#files.append(("inputsCG.txt", "outputsCG.txt") )
#files.append(("inputsCGD.txt", "outputsCGD.txt"))
#files.append(("inputsCV.txt", "outputsCV.txt"))
#files.append(("inputsCGS.txt", "outputsCGS.txt"))
files.append(("inputsCGV.txt", "outputsCGV.txt"))
files.append(("inputsCGA.txt", "outputsCGA.txt"))
#files.append(("train_filtred.txt", "train_filtred.txt"))
#files.append(("train_socle_train_var_in.txt", "train_socle_train_var_out.txt"))
#files.append(("train_socle_var_in_synt.txt", "train_socle_var_out_synt.txt"))
base = '￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide￨vide'
base_vide = '￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨￨'

for in_f, out_ in files:
    print(in_f.replace("inputs", "").replace(".txt", ""), end=" -> ")
    print(sum(1 for line in open(in_f)))

i_in = set()
variations = set()
for in_f, out_ in files:
    priin = False
    print(len(in_train_lines))
    if os.path.exists(in_f):
        print(in_f.replace("inputs", "").replace(".txt", ""))
        fichier_toks = open(in_f, 'r')
        fichier_sent = open(out_, 'r')
        for toks, sent in zip(fichier_toks, fichier_sent):
            toks = toks.strip()
            sent = sent.strip()
            if False:
                if "inputsCG.txt" == in_f:
                    value = random.randint(0, 99)
                    if  value >= 10:
                        continue
                if "inputsCGV.txt" == in_f:
                    value = random.randint(0, 99)
                    if value >= 2 and False:
                        continue

            if "train_socle_train_var_in.txt" != in_f and "train_socle_train_var_in_synt.txt" != in_f :
                if "%variation" in toks:
                    if sent.startswith(toks.split()[1]):
                        if len(toks.split()) == 2:
                            continue
                if "%variation" in toks:
                    sent = "%lexicale%"+ in_f.replace("inputs", "").replace(".txt", "") + "%" + base_vide + " " + sent
                elif (not sent.startswith("%")) and toks.startswith("%"):
                    sent = "%other%"+ in_f.replace("inputs", "").replace(".txt", "") + "%" + base_vide + " " + sent
            toks = toks.split()
            sents = sent.split()

            take = True

            for i in range(len(toks)):
                if "￨pron￨" in toks[i]:
                    toks[i] = toks[i].split("￨")[0] # on va apprendre la charge des pronoms... on ne l'aura pas en
                if "￨adv￨" in toks[i]:
                    toks[i] = toks[i].split("￨")[0] # on va apprendre la charge des pronoms... on ne l'aura pas en
                toks[i] = toks[i] if "￨" in toks[i] else toks[i]+base_vide if "%" in toks[i] else toks[i]+base
                toks[i] = toks[i].split("￨")
                for j in range(len(toks[i])):
                    if j == 0:
                        if toks[i][j].startswith("%"):
                            pass
                        elif toks[i][j] not in allowed_words and len(allowed_words) > 0:
                            if not "%lemm" in toks[i][0]:
                                take = False
                                break
                        else:
                            pass
                    elif "%" in toks[i][j]:
                        toks[i][j] = toks[i][j] .replace("%","")
                toks[i] = "￨".join(toks[i])

            for i in range(len(sents)):
                sents[i] = sents[i] if "￨" in sents[i] else sents[i]+base_vide if "%" in sents[i] else sents[i]+base
                sents[i] = sents[i].split("￨")
                for j in range(len(sents[i])):
                    if j == 0:
                        if sents[i][j].startswith("%"):
                            pass
                        elif sents[i][j] not in allowed_words and len(allowed_words) > 0:
                            take = False
                            break
                        else:
                            pass
                    elif "%" in sents[i][j]:
                        sents[i][j] = sents[i][j] .replace("%","")
                sents[i] = "￨".join(sents[i])

            if not take:
                continue

            if sents[0].startswith('%') :
                value = sents.pop(0)
            if toks[0].startswith('%') :
                toks.pop(0)
            value = "%lexicale%" + base_vide
            toks.insert(0, value)
            variations.add(value)

            toks = " ".join(toks)
            sents = " ".join(sents)

            if toks+sents not in i_in:

                i_in.add(toks+sents)
                in_train_lines.append(toks)
                out_train_lines.append(sents)
                if not priin:
                    print(in_train_lines[-1])
                    print(out_train_lines[-1])

                if "inputsCGV.txt" == in_f:
                    out_train_lines.append(sents[:])
                    sent_annexe = sents[:]
                    sents = []
                    for sent in sent_annexe.split():
                        sents.append(sent.split('￨')[0]+base)
                    sents = " ".join(sents)
                    in_train_lines.append(sents)
                    if not priin:
                        priin = True
                        print(in_train_lines[-1])
                        print(out_train_lines[-1])
                else:
                    priin = True


print("load words_nature", file=sys.stderr)
a = open("words_nature.json", "r")
words_nature = json.load(a)
a.close()
print("words_nature loaded", file=sys.stderr)

adjs_nature = {}
if os.path.exists("adjs_nature.json"):
    print("load adjs_nature.json", file=sys.stderr)
    a = open("adjs_nature.json", "r")
    adjs_nature = json.load(a)
    a.close()
    print("adjs_nature.json loaded", file=sys.stderr)

adv_nature = {}
if os.path.exists("adv_nature.json"):
    print("load adv_nature.json", file=sys.stderr)
    a = open("adv_nature.json", "r")
    adv_nature = json.load(a)
    a.close()
    print("adv_nature.json loaded", file=sys.stderr)

for step in ("train", "test", "val"):
    priin = False
    print(step)
    fichier_ins = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_inputs.txt"
    fichier_toks = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_outputs.txt"
    fichier_sent = "../../corpusGenerator/compref/buildCoNLL-U-sqlite-anonymise_accord-elision-reveal_adverbe-with_traits-without_contraintes-without_structure_wc/"+step+"_sentence_outputs.txt"

    if step == "test":
        continue

    fichier_toks = open(fichier_toks, 'r')
    fichier_sent = open(fichier_sent, 'r')
    for toks, sent in zip(fichier_toks, fichier_sent):
        toks = toks.strip()
        sent = sent.strip()
        tok_in = toks[:]
        tok_in = tok_in.split()
        sent = sent.split()

        if len(tok_in) != len(sent):
            continue
        take = True
        for i in range(len(tok_in)):
            if tok_in[i].endswith("bruit"):
                take = False
            if "￨pron￨" in tok_in[i]:
                tok_in[i] = tok_in[i].split("￨")[0]
            if "￨adv￨" in tok_in[i]:
                tok_in[i] = tok_in[i].split("￨")[0]  # on va apprendre la charge des pronoms... on ne l'aura pas en
            tok_in[i] = tok_in[i] if "￨" in tok_in[i] else tok_in[i] + base_vide if "%" in tok_in[i] else tok_in[i] + base
        if not take:
            continue
        tok_in = " ".join(tok_in)

        if step == "train":
            in_train_lines.append(tok_in)
            out_train_lines.append(toks)

        toks = toks.split()
        if not priin:
            print(in_train_lines[-1])
            print(out_train_lines[-1])

        retours = None

        for i_tok in range(len(sent)):
            toks[i_tok] = sent[i_tok] + toks[i_tok][toks[i_tok].index('￨'):]

        if step == "test":
            pass
        elif step == "val":
            pass
        elif step == "train":
            pass
        else:
            elts = {}
            adjs = {}
            advs = {}
            for i_tok in range(len(sent)):
                toks[i_tok] = sent[i_tok]+toks[i_tok][toks[i_tok].index('￨'):]
                if sent[i_tok] in words_nature.keys():
                    for value in set(words_nature[sent[i_tok]]):
                        if i_tok not in elts.keys():
                            elts[i_tok] = []
                        elts[i_tok].append(value)
            for i_tok in range(len(sent)):
                if sent[i_tok] in adjs_nature.keys():
                    for value in set(adjs_nature[sent[i_tok]]):
                        if i_tok not in adjs.keys():
                            adjs[i_tok] = []
                        adjs[i_tok].append(value)
            for i_tok in range(len(sent)):
                if sent[i_tok] in adv_nature.keys():
                    for value in set(adv_nature[sent[i_tok]]):
                        if i_tok not in advs.keys():
                            advs[i_tok] = []
                        advs[i_tok].append(value)

            toks_base = {}
            for i_tok in elts.keys():
                if "￨noun￨" in toks[i_tok] :
                    toks_base[toks[i_tok]] = {}
                    toks_base[toks[i_tok]] = []
                    for value in elts[i_tok]:
                        tokens = toks[i_tok].split('￨')
                        value = value.replace(' ', "_").split('￨')
                        tokens[-1] = value[-1]
                        tokens[2] = value[2]
                        tokens[3] = value[3]
                        tokens = '￨'.join(tokens)
                        toks_base[toks[i_tok]].append((i_tok, tokens))
            for i_tok in adjs.keys():
                if "￨adj￨" in toks[i_tok] :
                    toks_base[toks[i_tok]] = {}
                    toks_base[toks[i_tok]] = []
                    for value in adjs[i_tok]:
                        tokens = toks[i_tok].split('￨')
                        value = value.replace(' ', "_").split('￨')
                        tokens[-1] = value[-1]
                        tokens[2] = value[2]
                        tokens[3] = value[3]
                        tokens = '￨'.join(tokens)
                        toks_base[toks[i_tok]].append((i_tok, tokens))
            for i_tok in advs.keys():
                if "￨adv￨" in toks[i_tok] :
                    toks_base[toks[i_tok]] = {}
                    toks_base[toks[i_tok]] = []
                    for value in advs[i_tok]:
                        tokens = toks[i_tok].split('￨')
                        value = value.replace(' ', "_").split('￨')
                        tokens[-1] = value[-1]
                        tokens[2] = value[2]
                        tokens[3] = value[3]
                        tokens = '￨'.join(tokens)
                        toks_base[toks[i_tok]].append((i_tok, tokens))

            def replace_(string_out, dico):
                if len(dico.keys()) == 0:
                    return
                string_retour = []
                keys = list(dico.keys())
                dico_2 = dico.copy()
                in_ = dico_2[keys[0]]
                del dico_2[keys[0]]
                for i_tok, out_ in in_:
                    if len(dico_2.keys()) == 0:
                        retour = string_out.split()
                        retour[i_tok] = out_
                        string_retour.append(" ".join(retour))
                    else:
                        retour = string_out.split()
                        retour[i_tok] = out_
                        string_retour += replace_(" ".join(retour), dico_2)
                return string_retour

            retours = replace_(" ".join(toks).lower(), toks_base)

        if retours is None or len(retours) == 0:
            while "en,-en,-dans" in toks:
                toks = toks.replace("en,-en,-dans", "en,-dans")
        else:
            for i_r in range(len(retours)):
                while "en,-en,-dans" in retours[i_r]:
                    retours[i_r] = retours[i_r].replace("en,-en,-dans", "en,-dans")

        for i in range(len(sent)):
            sent[i] = sent[i] if "￨" in sent[i] else sent[i]+base_vide if "%" in sent[i] else sent[i]+base

        if retours is None or len(retours) == 0:
           # sents = [" ".join(sent).lower()]
            toks = " ".join(toks).lower()
        else:
            #sents = [" ".join(sent).lower()]*len(retours)
            toks = random.choice(retours)



        if step == "train":
            sents = " ".join(sent).lower()
            in_train_lines.append(sents)
            out_train_lines.append(toks)
            if not priin:
                priin = True
                print(in_train_lines[-1])
                print(out_train_lines[-1])
        elif step == "test":

            for variation in variations:
                if sent[0].startswith("%"):
                    sent.pop(0)
                sent.insert(0, variation)
                sents = " ".join(sent).lower()
                in_test_lines.append(sents)
                out_test_lines.append(toks)
                if not priin:
                    priin = True
                    print(in_test_lines[-1])
                    print(out_test_lines[-1])
        else:
            sents = " ".join(sent).lower()
            in_val_lines.append(sents)
            out_val_lines.append(toks)
            if not priin:
                priin = True
                print(in_val_lines[-1])
                print(out_val_lines[-1])

for step in ("test", ):
    priin = False
    print(step)
    fichier_ins = "phrases_LVF_corpus test.txt"
#

    fichier_ins = open(fichier_ins, 'r')
    for toks in fichier_ins:
        toks = toks.strip().split('"')
        toks = toks[5].strip()
        toks = toks.split()
        for i_ in range(len(toks)):
            toks[i_] = toks[i_]  +base
        for variation in variations:
            sents = toks.copy()
            if sents[0].startswith("%"):
                sents.pop(0)
            sents.insert(0, variation)
            sents = " ".join(sents).lower()
            in_test_lines.append(sents)
            out_test_lines.append(" ".join(toks).lower())
            if not priin:
                priin = True
                print(in_test_lines[-1])
                print(out_test_lines[-1])

if not os.path.exists('variationLexicales3/data'):
    os.makedirs("variationLexicales3/data")
if not os.path.exists('variationLexicales3/logs'):
    os.makedirs("variationLexicales3/logs")
with open('variationLexicales3/data/train-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_train_lines)
with open('variationLexicales3/data/train-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_train_lines)
with open('variationLexicales3/data/valid-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_val_lines)
with open('variationLexicales3/data/valid-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_val_lines)
with open('variationLexicales3/data/test-src.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in in_test_lines)
with open('variationLexicales3/data/test-tgt.txt', 'w') as ftxt:
    ftxt.writelines("%s\n" % l for l in out_test_lines)

#end