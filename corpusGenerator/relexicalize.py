# version 0.998
import glob
import os
import sacrebleu
import copy
import json
import re
import inspect
import random
import numpy as np
from pathlib import Path

from pprint import pprint
import argparse
import math
from tqdm import tqdm
import shutil
import sys

parser = argparse.ArgumentParser(description="Evaluation de corpus")

parser.add_argument('--debug', dest='debug', action='store_true')
parser.add_argument('--refresh', dest='refresh', action='store_true')

args = parser.parse_args()
debug = args.debug
refresh = args.refresh
print("debug: ", end=debug.__str__()+"\n")
print("refresh: ", end=refresh.__str__()+"\n")

best_models = []

if debug:
    out_log = './outputs/features_debug'
    text_outputs = 'tex_debug'
else:
    out_log = './features'
    text_outputs = 'tex'

fichiers_pred = glob.glob(out_log + '/*/pred.txt')
fichiers_log = glob.glob(out_log + '/*/logs/log.txt')
dirs_rep = glob.glob(out_log + '/buildCoNLL*')
fichiers = glob.glob(out_log + '/*/*onllup')
if refresh:
    created_files = ("tab_of_ref.json", "relex_info.json", "waited_relex.txt", "bleu.json", "stat_per_ref.json", "lemmas.json", "relex.json", "pred_relex.txt", "pred_relex.log", "contraintes.json")

    fichiers_socle = glob.glob(text_outputs+'/socles/*.*')
    for fichier_socle in fichiers_socle:
        os.remove(fichier_socle)

    for file_created in created_files[:]:
        if debug:
            created_files = glob.glob('./outputs/features_debug/*/'+file_created)
        else:
            created_files = glob.glob('./features/*/' + file_created)
        for fichier_socle in created_files:
            os.remove(fichier_socle)

if not os.path.exists(text_outputs+"/socles"):
    os.makedirs(text_outputs+"/socles")


f_log_inachevé = open(text_outputs+"/to_do.txt", "w")


tab_jobs = {}
tab_contrainte = {}
contraintes_names = set()


for dir_rep in dirs_rep:
    model = dir_rep[dir_rep.rindex("/")+1 :]
    tab_jobs[model] = {}

accuracies = {}
perplexities = {}

progress = tqdm(range(len(fichiers_log)))
rm_list = []
for fichier_log in fichiers_log:
    model = fichier_log.replace("/logs/log.txt", "")
    model = model[model.rindex("/")+1 :]
    apprentissages = open(fichier_log, "r")
    count_acc = 0
    log_perp = ""
    log_acc = ""
    vocab_source = ""
    vocab_target = ""
    accuracies[model] = []
    perplexities[model] = []
    tab_jobs[model]["local_dir"] = model[:]
    if os.path.exists(text_outputs+"/socles/"+model+".json"):
        with open(text_outputs+"/socles/"+model+".json", "r") as a:
            tab_jobs[model] = json.load(a)
            tab_jobs[model]["local_dir"] = model[:]
            accuracies[model] = tab_jobs[model]["accuracies"]
            perplexities[model] = tab_jobs[model]["perplexities"]
            vocab_source = tab_jobs[model]["vocab_source"]
            vocab_target = tab_jobs[model]["vocab_target"]
            count_acc = len(accuracies[model])

        if count_acc >= 10:
            count_acc = 10
            if os.path.exists(fichier_log.replace("/logs/log.txt", "/data")) and debug:
                shutil.rmtree(fichier_log.replace("/logs/log.txt", "/data"))
            fjson = open(text_outputs+"/socles/"+model+".json", "w")
            fjson.write(json.dumps(tab_jobs[model] , indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
    else:
        for apprentissage in apprentissages:
            if 'vocabulary size' in apprentissage:
                vocab = apprentissage.split('*')[1]
                vocab = vocab.split('.')[1].strip().split(";")
                vocab_source = vocab[0].split('=')[1].strip()
                vocab_target = vocab[1].split('=')[1].strip()
            elif 'src vocab size' in apprentissage:
                vocab = apprentissage.split('*')[1]
                vocab_source = vocab.split('=')[1].strip()
            elif 'tgt vocab size' in apprentissage:
                vocab = apprentissage.split('*')[1]
                vocab_target = vocab.split('=')[1].strip()

            if 'Validation perplexity' in apprentissage:
                lofichiers_logg_perp = round(float(apprentissage[apprentissage.rindex(':')+1:].strip()), 2)
                perplexities[model].append(float(apprentissage[apprentissage.rindex(':')+1:].strip()))
                if "first_perplexity" not in tab_jobs[model].keys():
                    tab_jobs[model]["first_perplexity"] = log_perp
            if 'Validation accuracy' in apprentissage:
                log_acc = round(float(apprentissage[apprentissage.rindex(':')+1:].strip()), 2)
                accuracies[model].append(float(apprentissage[apprentissage.rindex(':')+1:].strip()))
                if "first_accuracy" not in tab_jobs[model].keys():
                    tab_jobs[model]["first_accuracy"] = log_acc
                count_acc += 1 #{<tip text>}
        if count_acc >= 10:
            count_acc = 10
        tab_jobs[model]["epochs"] = count_acc.__str__()

        if count_acc < 10:
            print('# ' + count_acc.__str__() + ":", file=f_log_inachevé)
            print("# " + model, file=f_log_inachevé)
            print(fichier_log.replace("/logs/log.txt", "/cmd.bash"))
        tab_jobs[model]["local_dir"] = model[:]
        tab_jobs[model]["target_file"] =  model.replace("buildCoNLL-U-", "").replace("_wc", "").replace("inv_", "$\\neg$")\
            .replace("light_structure", "light").replace("without_structure", "ws")\
            .replace("-without_liaison", "").replace("without_liaison", "").replace("_", " ")
        tab_jobs[model]["perplexity"] = log_perp
        tab_jobs[model]["accuracy"] = log_acc
        tab_jobs[model]["perplexities"] = perplexities[model]
        tab_jobs[model]["accuracies"] = accuracies[model]
        tab_jobs[model]["vocab_source"] = vocab_source
        tab_jobs[model]["vocab_target"] = vocab_target

        if count_acc >= 10:
            if os.path.exists(fichier_log.replace("/logs/log.txt", "/data")) and debug:
                shutil.rmtree(fichier_log.replace("/logs/log.txt", "/data"))
            fjson = open(text_outputs+"/socles/"+model+".json", "w")
            fjson.write(json.dumps(tab_jobs[model] , indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True
        else:
            rm_list.append(fichier_log.replace("logs/log.txt", ""))
    progress.update()
progress.close()


f_log_inachevé.close()
stats_model_per_diff = {}

for fichier_log in rm_list:
    for fichier in fichiers_pred[:]:
        if fichier_log in fichier:
            fichiers_pred.remove(fichier)

progress = tqdm(range(len(fichiers_pred)))
for fichier in fichiers_pred:
    model =  fichier.replace("/pred.txt", "")
    model = model[model.rindex("/")+1 :]
    root = fichier[0:fichier.rindex("/")] + "/"
    reference_ano = root + "test_anonymise_inputs.txt"
    reference_out = root + "test_outputs.txt"
    anonymysed_data = root + "test_anonymise_outputs.txt"
    références_data = root + "test_outputs.txt"
    bleu_data = root + "bleu.json"
    log={"sentences":0, "relex":"-", "unexpected_token":"-", "not_relexicalisable":"-", 'bleu':[], 'lemmas':0}

    connll = root + 'CoNLLext.conllup'
    lemmas_file = root + 'lemmas.json'
    contraintes_file = root + 'contraintes.json'

    sentences_delex_info = {}
    sentences_delex = {}

    tmp_bleu = []
    if os.path.exists(bleu_data):
        with open(bleu_data, "r") as a:
            log['bleu'] = json.load(a)

        tmp_bleu = copy.deepcopy(log['bleu'] )

    i_count_treated = 0

    if os.path.exists(reference_ano) and os.path.exists(reference_out):
        with open(reference_out, "r") as input1:
            with open(reference_ano, "r") as input2:
                for sentence, line in zip(input1, input2):
                    i_count_treated += 1
                    line = eval(line)
                    sentence = sentence.strip()
                    sentence_delex = sentence[:]
                    for data in line.keys():
                        sentence = sentence.replace(data, line[data])
                    sentences_delex_info[sentence.strip().replace("|", " ").replace("  ", " ")] = line
                    sentences_delex[sentence.strip().replace("|", " ").replace("  ", " ")] = sentence_delex
    if os.path.exists(contraintes_file):
        with open(contraintes_file, "r") as a:
            contraintes = json.load(a)

    else:
        inputs = ('test_inputs.txt', 'train_inputs.txt', 'val_inputs.txt')
        contraintes = {}

        for input in inputs:

            with open(root + input, "r") as input:
                input = input.readlines()
                for line in input:
                    line = line.replace(">>", "#").split('>')
                    if len(line)> 1:
                        value = line[0].split()
                        if len(value) == 1:
                            value = value[0]
                        else:
                            value = value[1]
                        if value not in contraintes.keys():
                            contraintes[value] = 0
                        contraintes[value] += 1

        if len(contraintes)>0:
            fjson = open(contraintes_file, "w")
            fjson.write(json.dumps(contraintes, indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True

    tab_contrainte[model] = contraintes
    tab_contrainte[model]["\#all"] = 0
    for key in contraintes.keys():
        contraintes_names.add(key)
        tab_contrainte[model]["\#all"] += contraintes[key]

    if os.path.exists(lemmas_file):
        with open(lemmas_file, "r") as a:
            lemmas = json.load(a)
        log['lemmas'] = len(lemmas)

    elif os.path.exists(connll):
        lemmas = {}

        with open(connll, "r") as connll:
            connll = connll.readlines()

        for sentence in connll:
            if not sentence.strip().startswith("#") and sentence != '\n':
                line = sentence.strip().split('\t')
                if not (line[0].startswith('jux') or line[0].startswith('grp') or line[2]== "_"):
                    if line[2] not in lemmas.keys():
                        lemmas[line[2]] = 0
                    lemmas[line[2]] += 1

        if len(lemmas)>0:
            fjson = open(lemmas_file, "w")
            fjson.write(json.dumps(lemmas, indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True

        log['lemmas'] = len(lemmas)

    if os.path.exists(root + "train_inputs.txt"):
        with open(root + "train_inputs.txt", "r") as a:
            log['train sent.'] = len(a.readlines())
    else:
        print("error")
        print(root + "train_inputs.txt")

    stat_per_ref = {}
    tab_of_ref = {}

    if os.path.exists(root + "stat_per_ref.json"):
        try:
            with open(root + "stat_per_ref.json", "r") as a:
                stat_per_ref = json.load(a)
        except Exception as inst:
            stat_per_ref = {}

    if os.path.exists(root + "tab_of_ref.json"): # or
        try:
            with open(root + "tab_of_ref.json", "r") as a:
                tab_of_ref = json.load(a)
        except Exception as inst:
            tab_of_ref = {}
    else :
        tab_of_ref = {}

    save_bleu = False
    if (not os.path.exists(anonymysed_data)) or os.path.getsize(anonymysed_data) == 0:
        prédictions= open(fichier, "r")
        références = open(références_data, 'r')
        relex_lines_info = []
        count_sent = 0
        count_relex = 0

        for prédiction, référence in zip(prédictions, références):
            relex_lines_info.append({'miss':{},'present':{}, 'unexpected':[]})
            log["sentences"] += 1
            # c'est prédit dans l'ordre donc si nouvelles traductions..
            prédiction = prédiction.strip().replace("|", " ").replace("  ", " ")
            référence = référence.strip().replace("|", " ").replace("  ", " ")
            tab_of_ref[référence] = 1 if référence not in tab_of_ref.keys() else tab_of_ref[référence] + 1
            if prédiction+"|"+référence not in stat_per_ref.keys():
                stat_per_ref[prédiction + "|" + référence] = {'bleu':0, 'relex':0, 'contraintes count':0, 'looked for count':len(sentences_delex_info[référence]) if référence in sentences_delex_info.keys() else 0}
                score = sacrebleu.corpus_bleu(prédiction.replace("_", " "), référence.replace("_", " ")).score
                stat_per_ref[prédiction + "|" + référence]['bleu'] = score
            if len(log['bleu']) < log["sentences"] :
                log['bleu'].append(stat_per_ref[prédiction + "|" + référence]['bleu'])
                save_bleu = True
            stat_per_ref[prédiction + "|" + référence]['relex'] = 0
            if prédiction == référence:
                count_sent += 1
                count_relex += 1
                stat_per_ref[prédiction + "|" + référence]['relex'] = 1
            elif len(sentences_delex_info.keys())>0:
                if référence in sentences_delex_info.keys():
                    count_sent += 1
                    relex_info = sentences_delex_info[référence]
                    relex = True
                    for data in relex_info.keys():
                        if relex_info[data] not in prédiction.split():
                            relex = False
                            relex_lines_info[-1]['miss'][data]=relex_info[data]
                        else:
                            relex_lines_info[-1]['present'][data] = relex_info[data]
                    if relex:
                        count_relex+= 1
                        stat_per_ref[prédiction + "|" + référence]['relex'] = 1

        fjson = open(root + "relex_info.json", "w")
        fjson.write(json.dumps(relex_lines_info, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        fjson = open(root + "tab_of_ref.json", "w")
        fjson.write(json.dumps(tab_of_ref, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

        if count_sent == 0 or not os.path.exists(reference_ano):
            tab_jobs[model]["tx de relexicalisation"] = "na"
        else:
            tab_jobs[model]["tx de relexicalisation"] = round(count_relex / count_sent, 2).__str__() + "*"
    elif os.path.exists(anonymysed_data):
        if os.path.getsize(anonymysed_data) > 0:
            if os.path.getsize(fichier) == 0:
                print("#########################")
                print("#########################")
                print("ls -alh " + fichier.replace("pred.txt", "data/*"))
                print("rm " + fichier.replace("pred.txt", "data/*"))
                print("bash " + fichier.replace("pred.txt", "cmd_done.bash"))
                print("#########################")
                print("#########################")
            données = open(anonymysed_data, 'r')
            prédictions = open(fichier, "r")
            références = open(références_data, "r").readlines()
            relex_lines_info = []

            len_ref = len(références)
            len_rel = 0
            if os.path.exists(root + "pred_relex.txt"):
                relexicalisations = open(root + "pred_relex.txt", 'r').readlines()
                len_rel = len(relexicalisations)
            relex_file = root + "relex.json"

            tmp_log = {}
            if "lemmas" in log.keys():
                tmp_log["lemmas"] = log['lemmas']
            if "train sent." in log.keys():
                tmp_log["train sent."] = log['train sent.']
            if os.path.exists(relex_file):
                with open(relex_file, "r") as a:
                    relex_file = json.load(a)
                relex = relex_file['relex']
                log = relex_file['log']
                tab_jobs[model]["tx de relexicalisation"] = round(log["relex"] / log["sentences"], 2)

            if len_ref != len_rel or not os.path.exists(root + "relex.json") or not os.path.exists(root + "relex_info.json") or not os.path.exists(root + "tab_of_ref.json"):
                relex = []
                log["sentences"] = 0
                log["relex"] = 0
                #log["unexpected_token"] = 0
                log["not_relexicalisable"] = 0
                stat_per_ref = {}
                tab_of_ref = {}

                log['bleu'] = []
                if not os.path.exists(root + "waited_relex.txt"):
                    waited_ion = []
                    for donnée, référence in zip(données, références):
                        r_ref = référence.strip().replace("|", " ").replace("  ", " ")
                        donnée = eval(donnée)
                        référence = référence.strip().split(" ")
                        for i, token in enumerate(référence):
                            if token in donnée.keys():
                                référence[i] = donnée[token]
                        waited_ion.append(" ".join(référence))
                    with open(root + "waited_relex.txt", 'w') as ftxt:
                        ftxt.writelines("%s\n" % l for l in waited_ion)

                relex_change = False

                for prédiction, donnée, référence in zip(prédictions, données, références):
                    p_ref = prédiction.strip().replace("|", " ").replace("  ", " ")
                    r_ref = référence.strip().replace("|", " ").replace("  ", " ")
                    log["sentences"] += 1
                    tab_of_ref[r_ref] = 1 if r_ref not in tab_of_ref.keys() else tab_of_ref[r_ref] + 1

                    #if log["unexpected_token"] ==0:
                    #    relex_change =  True
                    relex_lines_info.append({'miss': {}, 'present': {}, 'unexpected':[]})
                    donnée = eval(donnée)
                    donnée_bak = list(donnée.keys())
                    référence = référence.strip().split(" ")
                    prédiction = prédiction.strip().split(" ")
                    for i, token in enumerate(référence):
                        if token in donnée.keys():
                            référence[i] = donnée[token]
                    for i, token in enumerate(prédiction):
                        if token in donnée.keys():
                            prédiction[i] = donnée[token]
                            if token in donnée_bak:
                                donnée_bak.remove(token)
                            relex_lines_info[-1]['present'][token] = donnée[token].replace('_', " ")
                    for token in set(donnée_bak):
                        relex_lines_info[-1]['miss'][token]=donnée[token]

                    relex.append(" ".join(prédiction))
                    save_bleu = True

                    if p_ref + "|" + r_ref not in stat_per_ref.keys():
                        stat_per_ref[p_ref + "|" + r_ref] = {'bleu': 0, 'relex': 0, 'contraintes count':0, 'looked for count':len(donnée.keys())}
                        score = sacrebleu.corpus_bleu(relex[-1].replace("_", " ").strip(), " ".join(référence).strip().replace("_", " ")).score
                        stat_per_ref[p_ref + "|" + r_ref]['bleu'] = score

                    log['bleu'].append(stat_per_ref[p_ref + "|" + r_ref]['bleu'] )

                    if len(donnée_bak) == 0:
                        log["relex"] += 1
                        stat_per_ref[p_ref + "|" + r_ref]['relex'] = 1

                if len(relex) > 0 and (save_bleu or not os.path.exists(root + "relex.json") or not os.path.exists(root + "relex_info.json")) or relex_change:
                    with open(root + "pred_relex.txt", 'w') as ftxt:
                        ftxt.writelines("%s\n" % l for l in relex)

                    with open(root + "pred_relex.log", 'w') as ftxt:
                        ftxt.write("sentences %s\n" % log["sentences"])
                        ftxt.write(" %s\n" % log["relex"])
                        ftxt.write("unexpected_token %s\n" % log["unexpected_token"])
                        ftxt.write("not_relexicalisable %s\n" % log["not_relexicalisable"])

                    tab_jobs[model]["tx de relexicalisation"] = round(log["relex"]/log["sentences"], 2)

                    fjson = open(root + "relex.json", "w")
                    fjson.write(json.dumps({'relex':relex, 'log':log}, indent=2, ensure_ascii=False))
                    fjson.flush()
                    fjson.close()
                    while not fjson.closed:
                            True

                    fjson = open(root + "relex_info.json", "w")
                    fjson.write(json.dumps(relex_lines_info, indent=2, ensure_ascii=False))
                    fjson.flush()
                    fjson.close()
                    while not fjson.closed:
                        True

                    fjson = open(root + "tab_of_ref.json", "w")
                    fjson.write(json.dumps(tab_of_ref, indent=2, ensure_ascii=False))
                    fjson.flush()
                    fjson.close()
                    while not fjson.closed:
                        True

            else:
                if not os.path.exists(root + "cmd_done.bash"):
                    if os.path.exists(root + "cmd.bash"):
                        os.rename(root + "cmd.bash", root + "cmd_done.bash")
                        Path(root + "cmd.bash").touch()

        if "lemmas" in tmp_log.keys():
            log["lemmas"] = tmp_log['lemmas']
        if "train sent." in tmp_log.keys():
            log["train sent."] = tmp_log['train sent.']
        del tmp_log

    for key in stat_per_ref.keys():
        stat_per_ref[key]['contraintes count'] = tab_of_ref[key.split("|")[1]]

    fjson = open(root + "stat_per_ref.json", "w")
    fjson.write(json.dumps(stat_per_ref, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True
    stats_model_per_diff[model] = stat_per_ref

    if len(log['bleu']) > 0 and save_bleu:
        fjson = open(bleu_data, "w")
        fjson.write(json.dumps(log['bleu'], indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

    for key in log.keys():
        tab_jobs[model][key] = log[key]

    progress.update()
    #break
progress.close()
# stats comparatives sur bleu

# 'relex',
columns = ['accuracy', 'vocab_source', 'vocab_target', 'train sent.', 'tx de relexicalisation', 'bleu', 'unexpected_token', 'lemmas',] # 'sentences',
columns.append('epochs')
backup_modeles_names = {}
bleus = {}
relexs = {}
print( "~ & " + " & " .join(columns).replace('_', "\_") + "\\\\ \hline")


for model in sorted(tab_jobs.keys()):
    if "wc" in model:
        modèle_part1 = "wc " + model.replace('wc', "").strip().strip("-_")

        modèle_part1 = modèle_part1.replace('_wc', "-wc")
        backup_modeles_names[modèle_part1.replace("_", " ")] = model
        if model not in accuracies.keys():
            accuracies[model] = [0]
            perplexities[model] = [0]
        accuracies[modèle_part1.replace("_", " ")] = accuracies[model]
        perplexities[modèle_part1.replace("_", " ")] = perplexities[model]
        if 'bleu' in tab_jobs[model].keys():
            bleus[modèle_part1.replace("_", " ")] = np.median(np.array(tab_jobs[model]['bleu']))
        if 'tx de relexicalisation' not in  tab_jobs[model].keys():
            tab_jobs[model]['tx de relexicalisation'] = 0
        relexs[modèle_part1.replace("_", " ")] = tab_jobs[model]['tx de relexicalisation']

        del accuracies[model]
        del perplexities[model]
        if model in tab_contrainte.keys():
            tab_contrainte[modèle_part1.replace("_", " ")] = tab_contrainte[model]
            del tab_contrainte[model]
        tab_jobs[modèle_part1.replace("_", " ")] = tab_jobs[model]
        del tab_jobs[model]

        if model in stats_model_per_diff.keys():
            stats_model_per_diff[modèle_part1.replace("_", " ")] = stats_model_per_diff[model]
            del stats_model_per_diff[model]

    else:
        if model in accuracies.keys():
            del accuracies[model]
            del perplexities[model]
        if model in tab_jobs.keys():
            del tab_jobs[model]
        if model in tab_contrainte.keys():
            del tab_contrainte[model]
        if model in stats_model_per_diff.keys():
            del stats_model_per_diff[model]

names_for_liste = {}
#backup_modeles_names --> clés pour les model
structures_keys = ("conll", "inv light structure", "light structure", 'insert global info', 'insert global contraintes')
id_keys = ('author id', 'book id', 'sent id')

infos = ('anonymise accord', 'time as parameter', 'reveal adverbe', 'simplify output', 'without contraintes', 'max tokens', 'max valued tokens', 'without lvf')

results = {}
columns_csv = []

for model in tab_jobs.keys():
    names_for_liste[model] = ""
    book_id = not 'no book id' in model.split("-")
    sent_id = not 'no sent id' in model.split("-")
    if ('without structure wc' in model.split("-") or "without structure" in model.split('-') )  and not "insert global info"  in  model.split("-") and not "insert global contraintes"  in  model.split("-") :
        book_id = False
        sent_id = False
    sep_var_name = ""
    tex_data = ""
    sep = ""
    for key in structures_keys:
        if key not in columns_csv:
            columns_csv.append(key)
        if "conll" == key and "ordered" in model:
            names_for_liste[model] += " ord"
            sep_var_name = " | "
            tex_data += sep + "o"
            sep = " & "
        elif "conll" == key and "join" in model:
            names_for_liste[model] += " join"
            sep_var_name = " | "
            if "doubled" in model:
                names_for_liste[model] += " join, da"
                tex_data += sep + "d"
            tex_data += sep + "j"
            sep = " & "
        elif "conll" == key and "doubled" in model:
            names_for_liste[model] += " da"
            sep_var_name = " | "
            tex_data += sep + "da"
            sep = " & "
        elif (key in model.split("-") or ("conll" == key and not ('without structure wc' in model.split("-") or "without structure" in model.split('-') ) )) and key != "reveal adverbe" :
            names_for_liste[model] += " " + key + sep_var_name.replace("without", "$\\neg$")
            tex_data += sep + "X"
            sep = " & "
            sep_var_name = " | "
        else:
            tex_data += sep +"~"
            sep = " & "
    if 'author id' in model.split("-"):
        if 'author id' not in columns_csv:
            columns_csv.append('author id')
        tex_data += sep + "X"
        names_for_liste[model] += " author id" + sep_var_name
        sep_var_name = " | "
    else:
        if 'author id' not in columns_csv:
            columns_csv.append('author id')
        tex_data +=  sep +"~"
    if book_id:
        if 'book id' not in columns_csv :
            columns_csv.append('book id')
        tex_data += sep + "X"
        names_for_liste[model] += " " + 'book id' + sep_var_name
        sep_var_name = " | "
    else:
        if 'book id' not in columns_csv:
            columns_csv.append('book id')
        tex_data +=  sep +"~"
    if sent_id:
        if 'sent id' not in columns_csv:
            columns_csv.append('sent id')
        tex_data += sep + "X"
        names_for_liste[model] += " " + 'sent id' + sep_var_name
        sep_var_name = " | "
    else:
        if 'sent id' not in columns_csv:
            columns_csv.append('sent id')
        tex_data +=  sep +"~"
    for key in infos:
        if key not in columns_csv:
            columns_csv.append(key)
        if key == "reveal adverbe" and "reveal adjectif" in model.split('-') and "reveal adverbe" in model.split('-'):
            names_for_liste[model] += " aa"
            tex_data += sep + "aa"
            sep = " & "
            sep_var_name = " | "
            sep = " & "
        elif key == "reveal adverbe" and "reveal adverbe" in model.split('-'):
            names_for_liste[model] += " adv"
            tex_data += sep + "adv"
            sep_var_name = " | "
            sep = " & "
        elif key == "reveal adverbe" and not "reveal adverbe" in model.split('-') and "reveal adjectif" in model.split(
                '-'):
            names_for_liste[model] += " adj"
            sep_var_name = " | "
            tex_data += sep + "adj"
            sep = " & "
        elif key == "reveal adverbe" and not "reveal adverbe" in model.split(
                '-') and not "reveal adjectif" in model.split('-'):
            sep_var_name = " | "
            tex_data += sep + "~"
            sep = " & "
        elif key in model.split("-"):
            tex_data += sep + "X"
            names_for_liste[model] += " " + key + sep_var_name.replace("without", "$\\neg$")
            sep_var_name
        elif key == "max valued tokens"  and len([k_ for k_ in model.split("-") if k_.startswith("max valued tokens")])>0:
            value_max_tokens_valuables = [k_ for k_ in model.split("-") if k_.startswith("max valued tokens")][0].split()[-1]
            tex_data += sep + value_max_tokens_valuables
            if "features" in model:
                tex_data += " fap"
                names_for_liste[model] += ' fap ' + sep_var_name
                sep_var_name = " | "
            if "trait" in model:
                tex_data += " t"
                names_for_liste[model] += ' traits ' + sep_var_name
                sep_var_name = " | "
            names_for_liste[model] += " maxVTk-" + value_max_tokens_valuables + sep_var_name
            sep_var_name = " | "
        elif key == "max valued tokens" :
            if "features" in model and not "trait" in model:
                names_for_liste[model] += "fap " + sep_var_name
                tex_data += sep + " fap"
                sep_var_name = " | "
            elif "trait" in model:
                names_for_liste[model] += "traits " + sep_var_name
                tex_data += sep + " t"
                sep_var_name = " | "
            elif "features" in model:
                names_for_liste[model] += "fap+t " + sep_var_name
                tex_data += sep + " fap+t"
                sep_var_name = " | "
            else:
                tex_data += sep +"~"
        elif key == "max tokens"  and len([k_ for k_ in model.split("-") if k_.startswith("max tokens")])>0:
            value_max_tokens_valuables = [k_ for k_ in model.split("-") if k_.startswith("max tokens")][0].split()[-1]
            tex_data += sep + value_max_tokens_valuables
            names_for_liste[model] += " maxTk-" + value_max_tokens_valuables + sep_var_name
            sep_var_name = " | "
        else:
            tex_data += sep +"~"
    results[model] = tex_data
    names_for_liste[model] = names_for_liste[model].strip()
    names_for_liste[model] = names_for_liste[model].strip("|")
    names_for_liste[model] = names_for_liste[model].strip()
columns_csv += columns

for model_name in names_for_liste.keys():
    names_for_liste[model_name] = names_for_liste[model_name].replace("contraintestraits", "contraintes, traits")
print( " & " .join(["~"]* (len(structures_keys)+ len(id_keys)+ len(infos))) + " & " + " & " .join(columns).replace('_', "\_") + "\\\\ \hline")
print( " & " .join(["~"]* (len(structures_keys)+ len(id_keys)+ len(infos))) + " & fichier\\\\ \hline")

fcsv = open(text_outputs+"/data.csv", "w")
ftrain = open(text_outputs+"/train.tex", "w")
ftrain2 = open(text_outputs+"/train2.tex", "w")

print("#model;" + ";".join(columns_csv).replace("~", "").replace("conll;inv light structure;light structure", "structure"), file=fcsv)
im = 1
numbers = {}
for model in sorted(tab_jobs.keys()):
    data_csv = [results[model]]
    values = [results[model]]
    values2 = []#[results[model]]
    if "epochs" in  tab_jobs[model].keys():
        values2.append(tab_jobs[model]["target_file"])
        for key in columns:
            if key in tab_jobs[model].keys():
                if key == 'bleu':
                    if len(tab_jobs[model][key]) > 0:
                        bleu = round(np.median(np.array(tab_jobs[model][key])), 2)
                    else:
                        bleu = "na"
                    values.append(bleu.__str__())
                    data_csv.append(bleu.__str__())
                else:
                    values.append(tab_jobs[model][key].__str__())
                    data_csv.append(tab_jobs[model][key].__str__().replace('-', 'na'))
            else:
                values.append("~")
                data_csv.append("na")
        values.insert(0, im.__str__().zfill(3))
        values2.insert(0, im.__str__().zfill(3))
        numbers[model] = im.__str__().zfill(3)
        im+=1
        print(" & ".join(values).replace("0.0*", "") + "\\\\ ", end='', file=ftrain)
        print(" & ".join(values2) + "\\\\ ", end='', file=ftrain2)
        target = out_log +  "/" + tab_jobs[model]["local_dir"]
        if not os.path.exists(target):
            print(target)
            print(model)
            raise Exception

        fcsv_local = open(target + "/data.csv", "w")
        print("#model;" + ";".join(columns_csv).replace("~", "").replace("conll;inv light structure;light structure",
                                                                        "structure"), file=fcsv_local)

        line_csv = ";".join(data_csv).replace("~", "Non").replace("X", "Oui").replace("&", ";").replace(" ;", ";") .replace("; ", ";").replace("*", "").replace("na", "").replace("0.0*", "").strip()
        if line_csv.startswith("Oui;Oui"):
            line_csv = "inv light" + line_csv[11:]
        elif line_csv.startswith("Oui;Non;Oui"):
            line_csv = "light" + line_csv[11:]
        elif line_csv.startswith("Oui;"):
            line_csv = "full" + line_csv[11:]
        else:
            line_csv = "none" + line_csv[11:]
        print(numbers[model]  + ";" + line_csv , file=fcsv)
        print(numbers[model]  + ";" + line_csv , file=fcsv_local)
        fcsv_local.close()
        print(" \hline", file=ftrain)
        print(" \hline", file=ftrain2)

print(" ", file=ftrain)
print(" ", file=ftrain2)
ftrain.close()
ftrain2.close()
fcsv.close()

try :
    def autodict(*args):
        get_rid_of = ['autodict(', ',', ')', '\n']
        calling_code = inspect.getouterframes(inspect.currentframe())[1][4][0]
        calling_code = calling_code[calling_code.index('autodict'):]
        for garbage in get_rid_of:
            calling_code = calling_code.replace(garbage, '')
        var_names, var_values = calling_code.split(), args
        dyn_dict = {var_name: var_value for var_name, var_value in
                    zip(var_names, var_values)}
        return dyn_dict

    def getDict(item):
        for key in item.keys():
            mean_k = np.mean(item[key])
            med_k = np.median(item[key])
            size = len(item[key])
            item[key] = {'mean': mean_k, 'median': med_k, 'size': size}
        return item

    for model in stats_model_per_diff.keys():
         #'bleu': 0, 'relex': 0, 'contraintes count':0, 'looked for count
        bleu_contraintes = {}
        bleu_par_nombre_tokens_demandés = {}
        relex_contraintes = {}
        relex_par_nombre_tokens_demandés = {}

        len_items = 0

        for key in stats_model_per_diff[model]:
            len_items += 1
            élément_ = stats_model_per_diff[model][key]
            cc_ = élément_['contraintes count']
            cl_ = élément_['looked for count']

            # bleu par nombre de contraintes et par tokens à générer
            if cc_ not in bleu_contraintes.keys():
                bleu_contraintes[cc_] = []
            if cl_ not in bleu_par_nombre_tokens_demandés.keys():
                bleu_par_nombre_tokens_demandés[cl_] = []
            bleu_contraintes[cc_].append(élément_['bleu'])
            bleu_par_nombre_tokens_demandés[cl_].append(élément_['bleu'])

            # taux de relexicalisation par nombre de contraintes et par tokens à générer
            if cc_ not in relex_contraintes.keys():
                relex_contraintes[cc_] = []
            if cl_ not in relex_par_nombre_tokens_demandés.keys():
                relex_par_nombre_tokens_demandés[cl_] = []
            relex_contraintes[cc_].append(élément_['relex'])
            relex_par_nombre_tokens_demandés[cl_].append(élément_['relex'])

        # ON REGROUPE ce qui est sous-représenté (disons -10%)
        for item in (bleu_contraintes, bleu_par_nombre_tokens_demandés, relex_contraintes, relex_par_nombre_tokens_demandés):
            for key in list(item.keys()):
                if len(item[key]) < len_items/10:
                    if "else" not in item.keys():
                        item["else"] = []
                    item["else"] += item[key]
                    del item[key]

            item = getDict(item)
        if True:
            print()
            print(model)
            if len(bleu_contraintes.keys())> 0:
                pprint(autodict(bleu_contraintes))
            if len(bleu_par_nombre_tokens_demandés.keys()) > 0:
                pprint(autodict(bleu_par_nombre_tokens_demandés))
            if len(relex_contraintes.keys()) > 0:
                pprint(autodict(relex_contraintes))
            if len(relex_par_nombre_tokens_demandés.keys()) > 0:
                pprint(autodict(relex_par_nombre_tokens_demandés))

except Exception:
    pass

print()

def make_tri(tab):
    tri_wc_ws = {}

    for model in sorted(tab.keys()):
        if model in numbers.keys():

            modele_name = model[:].replace('without_structure', " ws").replace('without structure', " ws")
            if "wc" in modele_name:
                if "ws" in modele_name:
                    structure = {"ws":{}}
                    modele_name = modele_name.replace("ws", "").strip().strip("-")
                    if "wc" in modele_name:
                        modele_name = modele_name.replace("wc", "").strip().strip("-")
                        structure["ws"]["wc"] = tab[model]
                    else:
                        structure["ws"]["no_wc"] = tab[model]
                else:
                    structure = {"no_ws": {}}
                    if "wc" in modele_name:
                        modele_name = modele_name.replace("wc", "").strip().strip("-")
                        structure["no_ws"]["wc"] = tab[model]
                    else:
                        structure["no_ws"]["no_wc"] = tab[model]
                modele_name = modele_name.strip('_-')
                if numbers[model] not in tri_wc_ws.keys():
                    tri_wc_ws[numbers[model]] = {}
                for key in structure.keys():
                    if key not in tri_wc_ws[numbers[model]].keys():
                        tri_wc_ws[numbers[model]] [key] = {}
                    for key_2 in structure[key].keys():
                        tri_wc_ws[numbers[model]] [key][key_2] = structure[key][key_2]
    return tri_wc_ws


tab_jobs = make_tri(tab_jobs)
sep = ""
old_titre = ""

print("\n"*3)
columns = sorted(contraintes_names)
columns1 = ['\\#all',
 'affirmation',
 'exclamation',
 'question', 'relativeObjet',
 'relativeSujet',
    'infinitif',
 'vpp',
 'vpa',
 'complément',
 'comparatif',
 'passive']

columns2 = []

#  'aucune',
# if self == Contrainte.RELATIVE_SUJET:
#     return "relativeSujet"
# elif self == Contrainte.RELATIVE_OBJET:
#     return "relativeObjet"
# elif self == Contrainte.INFINITIF:
#     return "infinitif"
# elif self == Contrainte.VPP:
#     return "vpp"
# elif self == Contrainte.VPA:
#     return "vpa"
# elif self == Contrainte.COMPLEMENT:
#     return "complément"
# elif self == Contrainte.COMPARATIF:
#     return "comparatif"
# elif self == Contrainte.JUXTAPOSITION:
#     return "juxtaposition"

i_count = 1
tab_contrainte = make_tri(tab_contrainte)

for columns in (columns1, columns2):
    fcont = open(text_outputs+"/contraintes" + i_count.__str__() + ".tex", "w")
    i_count += 1
    print( "~ & " + " & " .join(columns).replace('_', "\_") + "\\\\ \hline", file=fcont)
   # print( "~ & ws & wc & " + " & " .join(columns).replace('_', "\_") + "\\\\ \hline", file=fcont)

    sep = ""
    old_titre = ""

    for model in sorted(tab_contrainte.keys()):
        #model_name = model.replace('-without_liaison', "").replace('anonymise', " ano")\
        #    .replace('insert-global-info', "").replace('i.g.info', "")\
        #    .replace('time-as-parameter', "time").replace('  ', " ")\
        #    .replace('buildCoNLL-U-', "").replace('buildCoNLL-U', "").replace('-', " ").replace('  ', " ")\
        #    .replace('_', "-").strip().strip("-").strip()
        #if model_name == "":
        #    model_name = "all"
        model_name = model[:]
        if model_name == old_titre:
            titre_1 = ""
        else:
            titre_1 = model_name

        for column_1 in ("ws", 'no_ws'):
            for column_2 in ("wc", 'no_wc'):
                # les deux colonnes sont dispos pour le modèle, on sort la ligne
                if column_1 in tab_contrainte[model] and column_2 in tab_contrainte[model][column_1].keys():
                    if model_name != old_titre:
                        print(sep, file=fcont)
                        sep = " \hline"
                        print(titre_1, end=" & ", file=fcont)
                    else:
                        print("", file=fcont)
                        print("~", end=" & ", file=fcont)

                    old_titre = model_name
                    # if column_1 == "ws":
                    #     print("X", end=" & ", file=fcont)
                    #     if column_2 == "wc":
                    #         print("X", end=" & ", file=fcont)
                    #     else:
                    #         print("~", end=" & ", file=fcont)
                    # else:
                    #     print("~", end=" & ", file=fcont)
                    #     if column_2 == "wc":
                    #         print("X", end=" & ", file=fcont)
                    #     else:
                    #         print("~", end=" & ", file=fcont)

                    values = []
                    for key in columns:
                        if key in tab_contrainte[model][column_1][column_2]:
                            values.append(tab_contrainte[model][column_1][column_2][key].__str__())
                        else:
                            values.append("~")
                    print(" & " .join(values) + "\\\\ ", end='', file=fcont)

    print(" \hline", file=fcont)
    fcont.close()

# rechargement mémoire
min_acc = -1
max_acc = -1
min_perp = -1
max_perp = -1
max_epoch = 10

## pn va garder les mêmes marques
marks = {}

for model in accuracies.keys():
    if not isinstance(accuracies[model], float)  and len(accuracies[model])==0:
        accuracies[model].append(0)
        perplexities[model].append(100)
    if model not in names_for_liste.keys():
        names_for_liste[model] = model
    names_for_liste[model] = names_for_liste[model].replace("_", "\_")

# on ne va pas prendre les cinq meilleurs modèles, mais ceux utiles à l'études des phénomènes
titles_1 = [k for k in sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True) if k in bleus.keys()]
titles_2 = sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True)
for titles in (titles_1, titles_2):

    isNotFull = titles == titles_1
    if isNotFull:
        facc = open(text_outputs + "/accNotMaxed.tex", "w")
    else:
        facc = open(text_outputs + "/accperp.tex", "w")

        for i, k in enumerate(titles[:]):
            if (len(accuracies[k])<10) or  k  not in bleus.keys():
                titles.remove(k)
                del accuracies[k]
                if k in perplexities.keys():
                    del perplexities[k]
            if i > 0 and "insert global info" in k: #ok, iginf est super, voyons les autres
                titles.remove(k)
                del accuracies[k]
                if k in perplexities.keys():
                    del perplexities[k]
            if i > 2 and "author" in k and k in titles: #ok, iginf est super, voyons les autres
                titles.remove(k)
                del accuracies[k]
                if k in perplexities.keys():
                    del perplexities[k]

    #on va mettre en valeur le plus meilleur de tout ceux partageant une caractéristique tout en le laissant dans les graphes suivants
    all_titles = []
    nb_titres_to_folllow = 5
    def cleanTitles(ctitles, taille_liste=12):
        return ctitles[0:taille_liste]
        global all_titles
        kept = 0
        if len(ctitles)<= taille_liste or kept == taille_liste:
            return  ctitles
        motifs = []
        for title in ctitles[kept:nb_titres_to_folllow]:
            motifs += title.replace('wc buildCoNLL-U-', "").replace('-without liaison', "").split("-")
        change = True

        while change:
            change = False
            if len(ctitles)==taille_liste:
                return  ctitles
            found = False
            for motif in set(motifs):
                found = motifs.count(motif) == nb_titres_to_folllow
                if found :
                    break
            if not found :
                return  ctitles
            motif = sorted(set(motifs), reverse=True, key=lambda mot:motifs.count(motif))[0]

            if found:

                for title in ctitles[kept+1:]: #si le motif est dans le précédent est dans les cinq suivant, on le dit trop caractéristique
                    if motif in title:
                        ctitles.remove(title)
                        change = True
                        if len(ctitles)==taille_liste:
                            return  ctitles
                motifs = []
                for title in ctitles[kept:nb_titres_to_folllow+kept]:
                    motifs += title.replace('wc buildCoNLL-U-', "").replace('-without liaison', "").split("-")

                if len(ctitles)<=taille_liste:
                    return ctitles
            if kept < taille_liste-1 and not change:
                kept += 1
                change = True
            elif kept == taille_liste:
                change = False

        all_titles += ctitles
        return  ctitles[0:taille_liste]
    base_all_models_learned = cleanTitles(titles, 1000)
    titles = cleanTitles(titles, 12)


    for model in titles[:]:
        if model in accuracies.keys() and model in bleus.keys():
            min_acc = min(accuracies[model]) if min_acc == -1 else min(accuracies[model])  if min(accuracies[model]) < min_acc else min_acc
            max_acc = max(accuracies[model]) if max_acc == -1 else max(accuracies[model])  if max(accuracies[model]) > max_acc else max_acc
            min_perp = min(perplexities[model]) if min_perp == -1 else min(perplexities[model])  if min(perplexities[model]) < min_perp else min_perp
            max_perp = max(perplexities[model]) if max_perp == -1 else max(perplexities[model])  if max(perplexities[model]) > max_perp else max_perp
            max_epoch = len(accuracies) if len(accuracies) > max_epoch else max_epoch
        else:
            titles.remove(model)

    best_models.append("")
    best_models.append("")
    if isNotFull:
        best_models.append("# accuracies sur modèles sans contrainte de taille")#"", "# bleu", "# relex"
    else:
        best_models.append("# accuracies")  # "", "# bleu", "# relex"
    best_models.append("")
    for model in titles:
        if (names_for_liste[model], model) not in best_models:
            best_models.append((names_for_liste[model], model[:]))

    max_acc += 3
    max_perp += 1
    min_acc  -= 3
    min_perp = 0

    def nettoie(titre):
        titre = titre.replace("light structure", "light")
        titre = titre.replace("insert global info", "iginf")
        titre = titre.replace("insert global contraintes", "igcont")
        titre = titre.replace("anonymise accord", "ano")
        titre = titre.replace("reveal adverbe", "ra")
        titre = titre.replace("simplify output", "so")
        titre = titre.replace("time as parameter", "time")
        titre = titre.replace("  ", " ")
        titre = titre.replace(" id | book id", "+book id")
        titre = titre.replace(" id | sent id", "+sent id")
        titre = titre.replace(" id | sent id", "author+sent id")
        titre = titre.replace("|", ", ")
        titre = titre.replace(" ,", ", ")
        titre = titre.replace("  ", " ")
        if len(titre) > 95:
            titre = titre[0: 95] + " \ldots{}"
        else:
            while len(titre) != 99:
                titre += "~"
        return titre


    def getAxis(min_, max_, texte, max_x = 11):
        axis = """
    %\\begin{adjustbox}{angle=90}
    \\begin{tikzpicture}
        \\begin{axis}[	grid= major ,
                        width=0.45\\textwidth ,
                        xlabel = {epochs} ,
                        ylabel = {@text} ,
                        xmin = 0, xmax =  @max_x,
                        ymin = @min_y, ymax = @max_y,
                        legend entries={@curves},
                        legend style={at={(0.5,-0.1)},anchor=north},%legend style={at={(2.63,0.8)},anchor=east},%legend style={at={(0.5,-0.1)},anchor=north},
                        legend cell align={left},]
    """
        t_tiles = ", ".join(["{}{" + numbers[model] + "-" + nettoie(names_for_liste[model]) + "}" if model in numbers.keys() and model in names_for_liste.keys() else numbers[model] if model in numbers.keys()  else "~" +"-"+names_for_liste[model]  if model in names_for_liste.keys() else model for model in titles])
        t_tiles = t_tiles.replace("  ", " ").replace("without liaison", "")
        t_tiles = t_tiles.replace("-", " ").replace("wc", "")

        return axis.replace("@curves", t_tiles.replace("buildCoNLL U", " ").replace(",    ,", ", non anonymised,").replace(",     without structure,", ", non anonymised without structure,").strip())\
            .replace("@text", texte.__str__())\
            .replace("@max_x", (math.ceil(max_x)).__str__())\
            .replace("@min_y",(math.ceil(min_)).__str__())\
            .replace("@max_y", (math.ceil(max_)).__str__())

    print(getAxis(min_acc, max_acc, "accuracies"), file=facc)

    colors = ["black", "blue", "violet", "magenta", "teal", "brown", "cyan", "darkgray", "gray", "lime", "lightgray", "olive", "orange", "pink", "yellow", "red", "purple", "green", "white"]
    colors += colors
    marques_full = ["oplus*", "halfcircle*", "halfdiamond*", '*',
                    'square*', 'triangle*', 'diamond*', 'pentagon*', "otimes*",
                    "halfsquare*", "halfsquare right*", "halfsquare left*", '+', 'x']
    marques_full += reversed(marques_full)
    marques_empty = ['o', 'square', 'triangle', 'diamond', 'pentagon', "Mercedes star", "asterisk", "star", "10-pointed star", "oplus", "otimes", "|", "-", "Mercedes star flipped", "pentagon", "text", "halfcircle", "heart"]
    marques_full += reversed(marques_empty)
    marques_empty += reversed(marques_full)
    a = marques_empty[:]
    random.shuffle(a)
    marques_empty += a


    for model in titles:
        if model not in marks.keys():
            color = colors.pop(0)
            marks[model] = ["draw=" + color]
            marks[model].append("mark options={" + color + "}")
            if "without structure" in model:
                marks[model].append("dashed")
            if not "non ano" in model and "ano" in model:
                marks[model].append("mark=" + marques_full.pop(0))
            else:
                marks[model].append("mark=" + marques_empty.pop(0))


        print("			\\addplot[" + ",".join(marks[model]) + "] coordinates {", end="", file=facc)
        for i_val, value in enumerate(accuracies[model][0:10]):
            print("(" + (i_val+1).__str__() + "," + value.__str__() + ")", end=" ", file=facc)
        print("}; %"+model, file =facc)
    print("""\end{axis}
        \end{tikzpicture}
        %\end{adjustbox}
    """, file=facc)

    print("La précision (accuracy) est calculée sur les données de validation et est donc pleinement comparable : les éléments de structures ne se voient pas évalués.", file=facc)
    #print("\paragraph{}", file=facc)
    #print("Il est à noter que seul le couple (anonymisation avec information d'accord et informations structurelles) se dégage.\paragraph{}", file=facc)

    print(getAxis(min_perp, max_perp, "perplexities"), file=facc)

    for model in titles:
        print("			\\addplot[" + ",".join(marks[model]) + "] coordinates {", end="", file=facc)
        for i_val, value in enumerate(perplexities[model][0:10]):
            print("(" + (i_val+1).__str__() + "," + value.__str__() + ")", end=" ", file=facc)
        print("}; %"+model, file=facc)
    print("""\end{axis}
        \end{tikzpicture}
        %\end{adjustbox}
    """, file=facc)

    print("La perplexité (perplexity) est calculée sur les données de validation et est donc pleinement comparable : les éléments de structures ne se voient pas évalués.", file=facc)
    print("\\\\", file=facc)

    if isNotFull:
        facc.close()


titles_2 = [bleu for bleu in sorted(bleus.keys(), key=lambda x:float(bleus[x]), reverse=True) if not math.isnan(bleus[bleu])]
titles_1 = [bleu for bleu in sorted(bleus.keys(), key=lambda x:float(bleus[x]), reverse=True) if not math.isnan(bleus[bleu])]

for titles in (titles_1, titles_2):

    max_bleu = 100

    isNotFull = titles == titles_1
    if isNotFull:
        facc_2 = facc
        facc = open(text_outputs + "/bleuNotMaxed.tex", "w")
    else:
        facc.close()
        facc = facc_2

    if len(titles) > 0:
        max_bleu = int(math.ceil(max([bleus[bleu] for bleu in titles])/10)) * 10
    titles = cleanTitles(titles)

    if isNotFull:
        best_models.append("# bleu sur modèles sans contraintes de taille")# "# relex"
    else:
        best_models.append("# bleu")# "# relex"
    best_models.append("")
    all_models_learned = []
    for model in base_all_models_learned:
        if (names_for_liste[model], model) not in all_models_learned:
            all_models_learned.append((names_for_liste[model], model[:]))

    for model in titles:
        if (names_for_liste[model], model) not in best_models:
            best_models.append((names_for_liste[model], model[:]))

    for model in titles:
        if model not in marks.keys() and model  in bleus.keys():
            color = colors.pop(0)
            marks[model] = ["draw=" + color]
            marks[model].append("mark options={" + color + "}")
            if "without structure" in model:
                marks[model].append("dashed")
            if not "non ano" in model and "ano" in model:
                marks[model].append("mark=" + marques_full.pop(0))
            else:
                marks[model].append("mark=" + marques_empty.pop(0))

    print(getAxis(0, max_bleu, "bleu médian", 12), file=facc)

    i_val = 1
    for model in titles:
        if model not in bleus.keys():
            continue
        print("			\\addplot[" + ",".join(marks[model]) + "] coordinates {", end="", file=facc)
        print("(" + (i_val).__str__() + "," +  bleus[model].__str__() + ")", end=" ", file=facc)
        i_val += 1
        print("};", file=facc)
    print("""\end{axis}
        \end{tikzpicture}
        %\end{adjustbox}
    """, file=facc)


print("""
Le BLEU (N-Gram 4) est calculé sur données relexicalisées.
\paragraph{}

""", file=facc)

for k in list(relexs.keys()):
    if relexs[k] == "na":
        del relexs[k]

titles = sorted(relexs.keys(), key=lambda x:float(relexs[x].replace("*", "") if isinstance(relexs[x], str) else relexs[x]), reverse=True)
titles = cleanTitles(titles)
# petit copier coller du matin.
for key in relexs.keys():
    relexs[key] = float(relexs[key].replace("*", "")) if isinstance(relexs[key], str) else relexs[key]

if len(relexs) >0:
    max_relex = float(int(math.ceil(max([relexs[relex]*10 for relex in titles])))) / 10
else:
    max_relex = 0.
    
best_models.append("# relex")# "# "
best_models.append("")
for model in titles:
    if (names_for_liste[model], model) not in best_models:
        best_models.append((names_for_liste[model], model[:]))

for model in titles:
    if model not in marks.keys():
        color = colors.pop(0)
        marks[model] = ["draw=" + color]
        marks[model].append("mark options={" + color + "}")
        if "without structure" in model:
            marks[model].append("dashed")
        if not "non ano" in model and "ano" in model:
            marks[model].append("mark=" + marques_full.pop(0))
        else:
            marks[model].append("mark=" + marques_empty.pop(0))

print(getAxis(0, max_relex, "taux de relexicalisation", 12), file=facc)
i_val = 1
for model in titles:
    if model not in bleus.keys():
        continue
    print("			\\addplot[" + ",".join(marks[model]) + "] coordinates {", end="", file=facc)
    print("(" + (i_val).__str__() + "," + relexs[model].__str__().replace("*", "") + ")", end=" ", file=facc)
    i_val += 1
    print("}; %"+model, file=facc)
print("""\end{axis}
	\end{tikzpicture}
	%\end{adjustbox}
""", file=facc)


print("""


Une phrase est dite (re-)lexicalisée quand elle contient les objets demandés en entrée (verbes, leurs arguments, les attributs des verbes et arguments). Quand la phrase était anonymisée, une phrase ne sera pas comptabilisée comme correctement lexicalisée (donc non-(re)lexicalisée) si elle contient des éléme,nts inattendus.

""", file=facc)

facc.close()

if False:
    flearn = open(text_outputs+"/learningDetails.tex", "w")

    # on redéfinit la fonction, c'est plus simple
    def getAxis(min_, max_, labels_x, label_y, curves_titles, title):
        axis = """
    %\\begin{adjustbox}{angle=90}
    
    
    \\begin{tikzpicture}
        \\begin{axis}[
          y tick label style={/pgf/number format/.cd,%
              scaled y ticks = false,
              set thousands separator={},
              fixed},	grid= major ,
                        width=0.40\\textwidth ,
                        xtick={@int_i},
                        xticklabels={@labels_1},%<--Here
                        xlabel = {models} ,
                        ylabel = {@label_y} ,
                        xmin = @min_x, xmax =  @max_x,
                        %ymin = @min_y, 
                        ymax = @max_y,
                        legend entries={@curves},
                        title={@title},
                        legend style={at={(0.5,-0.1)},anchor=north},%legend style={at={(2.63,0.8)},anchor=east},%legend style={at={(0.5,-0.1)},anchor=north},
                        legend cell align={left}]
    """
        t_tiles = ", ".join(["{}{" + numbers[model] + "-" + nettoie(names_for_liste[model]) + "}" if model in numbers.keys() and model in names_for_liste.keys() else numbers[model] if model in numbers.keys() else "~" +"-"+names_for_liste[model]  if model in names_for_liste.keys() else model for model in curves_titles])
        t_tiles = t_tiles.replace("  ", " ").replace("without liaison", "")
        t_tiles = t_tiles.replace("-", " ").replace("wc", "")

        len_max= int(max_)
        len_max= len(max_.__str__())
        print(max_)
        if max_ == -1:
            max_ = 1
        else:
            max_ = int((int(max_)).__str__()[0]) +1
            max_ =  max_.__str__() + "0" * (len_max -1)

        return axis.replace("@curves", t_tiles.replace("buildCoNLL U", " ").strip().replace(",    ,", ", non anonymised,").replace(",     without structure,", ", non anonymised without structure,"))\
            .replace("@label_y", label_y.__str__())\
            .replace("@title", title.__str__())\
            .replace("@int_i", "0," + ",".join([(i+1).__str__() for i, label in enumerate(labels_x)]) + "," + (len(labels_x) +1 ).__str__() )\
            .replace("@labels_1", "~," + ",".join([label.__str__() for i, label in enumerate(labels_x)])  +  ",~" )\
            .replace("@min_x", "0") \
            .replace("@max_x", (len(labels_x) +1 ).__str__())\
            .replace("@min_y", "0")\
            .replace("@max_y", "1") # max_.__str__())
        # min([ for i, label in enumerate(labels_x)]).__str__())\

    #on ne va faire les rapports que sur les modèles non contraints pour plus de lisibilité

    titles = [k for k in sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True) ]

    graphes = {}
    columns_name = {}
    commentaires = {}
    min_value = {}
    max_value = {}
    #print("\subsection{Éléments manquants sur modèles étudiés}", file=flearn)

    graphe_name = 'Éléments manquants / nombre de prédictions'
    graphes[graphe_name] = {}
    columns_name[graphe_name] = set()
    commentaires[graphe_name] = ""
    min_value[graphe_name] = -1
    max_value[graphe_name] = -1
    for model in titles:
        graphes[graphe_name][model] = {}
        name = backup_modeles_names[model]
        path = os.path.join("outputs/features_debug" if debug else "features", name)
        print(os.path.join(path, "bleu.json"))
        if os.path.exists(os.path.join(path, "bleu.json")):
            bleu = json.load(open(os.path.join(path, "bleu.json"), "r"))
        else:
            bleu = ""
        if os.path.exists(os.path.join(path, "pred_relex.txt")):
            relex = open(os.path.join(path, "pred_relex.txt"), "r").readlines()
            pred = open(os.path.join(path, "pred.txt")).readlines()
        elif os.path.exists(os.path.join(path, "pred.txt")):
            relex = open(os.path.join(path, "pred.txt"), "r").readlines()
            pred = relex
        if os.path.exists(os.path.join(path, "test_inputs.txt")):
            source = open(os.path.join(path, "test_inputs.txt"), "r").readlines()
        # pred = open(os.path.join(path, "test_outputs.txt"), "r").readlines()
        relex_info = []
        if os.path.exists(os.path.join(path, "relex_info.json")):
            print(path)
            relex_info = json.load(open(os.path.join(path, "relex_info.json"), "r"))
        verbe = 0
        attribut = 0
        argument = 0
        for line in relex_info:
            data = line['miss']
            l_t = 0
            l_a = 0
            l_v = 0
            for anonymysed_data_key in data.keys():
                is_verbe = len(anonymysed_data_key.split("_")) == 2 or "verbe_" in anonymysed_data_key
                if is_verbe:
                    is_verbe = "Z" in anonymysed_data_key.split("_")[0] or "verbe_" in anonymysed_data_key
                    if not is_verbe:
                        regex = re.compile("(.)*\d(.)*")
                        is_verbe = regex.match(anonymysed_data_key.split("_")[0]) is not None
                else:
                    is_verbe = anonymysed_data_key.startswith('person_')
                if "infinitif" in anonymysed_data_key or \
                        "indicatif" in anonymysed_data_key or\
                        "conditionnel" in anonymysed_data_key or\
                        "subjonctif" in anonymysed_data_key or \
                        "participe" in anonymysed_data_key or is_verbe:
                    l_v += 1
                elif anonymysed_data_key.endswith("00"):
                    l_t += 1
                else:
                    l_a += 1
            if l_t>0:
                attribut += 1
            if l_a>0:
                argument += 1
            if l_v>0:
                verbe += 1

        if verbe == 0:
            del graphes[graphe_name][model]
            continue

        graphes[graphe_name][model]["verbe"] = verbe / len(pred)
        graphes[graphe_name][model]["attribut"] = attribut / len(pred)
        graphes[graphe_name][model]["argument"] = argument / len(pred)
        for item in (verbe / len(pred), attribut / len(pred), argument / len(pred)):
            if item < min_value[graphe_name] or min_value[graphe_name] == -1:
                min_value[graphe_name] = item
            if item > max_value[graphe_name] or max_value[graphe_name] == -1:
                max_value[graphe_name] = item

    commentaires[graphe_name] = """Quand une prédiction ramène au moins un verbe manque, elle est comptabilisée dans les phrases à verbes manquants. Quand un des verbes voit au moins un de des arguments manquer, elle est comptabilisée, idem pour les attributs. Le tout est ensuite divisé par le nombre total de phrases. \\\\~
    """
    columns_name[graphe_name].add('verbe')
    columns_name[graphe_name].add('attribut')
    columns_name[graphe_name].add('argument')

    graphe_name = 'Éléments manquants rapportés au nombre d\'élements présents'
    graphes[graphe_name] = {}
    columns_name[graphe_name] = set()
    commentaires[graphe_name] = ""
    min_value[graphe_name] = -1
    max_value[graphe_name] = -1
    titles = [k for k in sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True) ]
    for model in titles:
        graphes[graphe_name][model] = {}
        name = backup_modeles_names[model]
        path = os.path.join("outputs/features_debug" if debug else "features", name)
        print(os.path.join(path, "bleu.json"))
        if not os.path.exists(os.path.join(path, "bleu.json")):
            continue
        bleu = json.load(open(os.path.join(path, "bleu.json"), "r"))
        if os.path.exists(os.path.join(path, "pred_relex.txt")):
            relex = open(os.path.join(path, "pred_relex.txt"), "r").readlines()
            pred = open(os.path.join(path, "pred.txt")).readlines()
        else:
            waited = open(os.path.join(path, "test_outputs.txt")).readlines()
            relex = open(os.path.join(path, "pred.txt"), "r").readlines()
            pred = relex
        if os.path.exists(os.path.join(path, "waited_relex.txt")):
            if os.path.exists(os.path.join(path, "test_outputs.txt")):
                waited = open(os.path.join(path, "test_outputs.txt")).readlines()
                waited_relex = open(os.path.join(path, "waited_relex.txt")).readlines()
            else:
                waited = open(os.path.join(path, "test_outputs.txt")).readlines()
                waited_relex = waited[:]
        source = open(os.path.join(path, "test_inputs.txt"), "r").readlines()
        # pred = open(os.path.join(path, "test_outputs.txt"), "r").readlines()
        relex_info = json.load(open(os.path.join(path, "relex_info.json"), "r"))
        verbe = 0
        attribut = 0
        argument = 0
        verbe_in = 0
        attribut_in = 0
        argument_in = 0
        verbes_encoded = {}
        for line, input in zip(relex_info, source):
            data = line['miss']
            data_in = line['present']

            for anonymysed_data_key in data.keys():
                is_verbe = len(anonymysed_data_key.split("_")) == 2 or "verbe_" in anonymysed_data_key
                if is_verbe:
                    is_verbe = "Z" in anonymysed_data_key.split("_")[0] or "verbe_" in anonymysed_data_key
                    if not is_verbe:
                        regex = re.compile("(.)*\d(.)*")
                        is_verbe = regex.match(anonymysed_data_key.split("_")[0]) is not None
                else:
                    is_verbe = anonymysed_data_key.startswith('person_')
                if "infinitif" in anonymysed_data_key or \
                        "indicatif" in anonymysed_data_key or\
                        "conditionnel" in anonymysed_data_key or\
                        "subjonctif" in anonymysed_data_key or \
                        "participe" in anonymysed_data_key or is_verbe:
                    verbe += 1
                elif anonymysed_data_key.endswith("00"):
                    attribut += 1
                else:
                    argument += 1

            for anonymysed_data_key in data_in.keys():
                is_verbe = len(anonymysed_data_key.split("_")) == 2 or "verbe_" in anonymysed_data_key
                if is_verbe:
                    is_verbe = "Z" in anonymysed_data_key.split("_")[0] or "verbe_" in anonymysed_data_key
                    if not is_verbe:
                        regex = re.compile("(.)*\d(.)*")
                        is_verbe = regex.match(anonymysed_data_key.split("_")[0]) is not None
                else:
                    is_verbe = anonymysed_data_key.startswith('person_')
                if "infinitif" in anonymysed_data_key or \
                        "indicatif" in anonymysed_data_key or\
                        "conditionnel" in anonymysed_data_key or\
                        "subjonctif" in anonymysed_data_key or \
                        "participe" in anonymysed_data_key or is_verbe:
                    verbe_in += 1
                elif anonymysed_data_key.endswith("00"):
                    attribut_in += 1
                else:
                    argument_in += 1
        if verbe+verbe_in == 0 or attribut+attribut_in == 0 or argument+argument_in == 0:
            del graphes[graphe_name][model]
            continue
        else:
            graphes[graphe_name][model]["verbe"] = verbe / (verbe+verbe_in)
        graphes[graphe_name][model]["attribut"] = attribut / (attribut+attribut_in)
        graphes[graphe_name][model]["argument"] = argument / (argument+argument_in)

        for item in (verbe, attribut, argument):
            if item < min_value[graphe_name] or min_value[graphe_name] == -1:
                min_value[graphe_name] = item
            if item > max_value[graphe_name] or max_value[graphe_name] == -1:
                max_value[graphe_name] = item
    commentaires[graphe_name] = """Le taux d'éléments manquant est très faible pour le modèle anonymisation et structure, avec un intéressant pic pour les verbes, plus élevé que lorsqu'on traite les mêmes données et qu'on ne fournit pas d'information de structure. \\\\
    Coupler informations de structure et anonymisation est en tout  cas particulièrement efficace.
    """
    columns_name[graphe_name].add('verbe')
    columns_name[graphe_name].add('attribut')
    columns_name[graphe_name].add('argument')

    #print("\subsection{Éléments inattendus sur modèles étudiés}", file=flearn)
    print("L'anonymisation permet de repérer aisément la présence d'éléments (verbes, attributs de verbe, arguments).", file=flearn)
    graphe_name = 'Éléments inattendus / nombre de prédictions'
    graphes[graphe_name] = {}
    columns_name[graphe_name] = set()
    min_value[graphe_name] = min_value['Éléments manquants / nombre de prédictions']
    max_value[graphe_name] = max_value['Éléments manquants / nombre de prédictions']
    commentaires[graphe_name] = ""
    titles = [k for k in sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True)]
    for model in titles:
        graphes[graphe_name][model] = {}
        name = backup_modeles_names[model]
        path = os.path.join("outputs/features_debug" if debug else "features", name)
        if not os.path.exists(os.path.join(path, "bleu.json")):
            continue
        print(os.path.join(path, "bleu.json"))
        bleu = json.load(open(os.path.join(path, "bleu.json"), "r"))
        if os.path.exists(os.path.join(path, "pred_relex.txt")):
            relex = open(os.path.join(path, "pred_relex.txt"), "r").readlines()
            pred = open(os.path.join(path, "pred.txt")).readlines()
        else:
            relex = open(os.path.join(path, "pred.txt"), "r").readlines()
            pred = relex

        source = open(os.path.join(path, "test_inputs.txt"), "r").readlines()
        # pred = open(os.path.join(path, "test_outputs.txt"), "r").readlines()
        relex_info = json.load(open(os.path.join(path, "relex_info.json"), "r"))
        verbe = 0
        attribut = 0
        argument = 0
        for line in relex_info:
            data = line['unexpected']
            for anonymysed_data_key in data:
                if "infinitif" in anonymysed_data_key or \
                        "indicatif" in anonymysed_data_key or\
                        "conditionnel" in anonymysed_data_key or\
                        "subjonctif" in anonymysed_data_key or \
                        "participe" in anonymysed_data_key or anonymysed_data_key.startswith('person_')  or "verbe_" in anonymysed_data_key:
                    verbe += 1
                elif anonymysed_data_key.endswith("00"):
                    attribut += 1
                else:
                    argument += 1

        if verbe == 0:
            del graphes[graphe_name][model]
            continue

        graphes[graphe_name][model]["verbe"] = verbe / len(pred)
        graphes[graphe_name][model]["attribut"] = attribut / len(pred)
        graphes[graphe_name][model]["argument"] = argument / len(pred)
        for item in (verbe / len(pred), attribut / len(pred), argument / len(pred)):
            if item < min_value[graphe_name] or min_value[graphe_name] == -1:
                min_value[graphe_name] = item
            if item > max_value[graphe_name] or max_value[graphe_name] == -1:
                max_value[graphe_name] = item

    columns_name[graphe_name].add('verbe')
    columns_name[graphe_name].add('attribut')
    columns_name[graphe_name].add('argument')


    graphe_name = 'abs(expected-inattendus)'
    graphes[graphe_name] = {}
    columns_name[graphe_name] = set()
    min_value[graphe_name] = -1
    max_value[graphe_name] = -1
    commentaires[graphe_name] = ""
    columns_name[graphe_name].add('verbe')
    columns_name[graphe_name].add('attribut')
    columns_name[graphe_name].add('argument')
    titles = [k for k in sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True)]
    for model in graphes['Éléments inattendus / nombre de prédictions'].keys():
        graphes[graphe_name][model] = {}
        for key in graphes['Éléments inattendus / nombre de prédictions'][model].keys():
            graphes[graphe_name][model][key] = math.fabs(graphes['Éléments inattendus / nombre de prédictions'][model][key]-graphes['Éléments manquants / nombre de prédictions'][model][key])
            if graphes[graphe_name][model][key] < min_value[graphe_name] or min_value[graphe_name] == -1:
                min_value[graphe_name] = graphes[graphe_name][model][key]
            if graphes[graphe_name][model][key] > max_value[graphe_name] or max_value[graphe_name] == -1:
                max_value[graphe_name] = graphes[graphe_name][model][key]
    commentaires[graphe_name] = ""

    if len(titles) > 0:
        for graphe_name in graphes.keys():
            # min_, max_, labels_x, texte, curves_titles
            curves_titles = []
            for model in titles:
                if model in graphes[graphe_name].keys():
                    curves_titles.append(model)
            if len(curves_titles) > 0:
                graph_title = graphe_name
                y_label = " ".join(graphe_name.split(" ")[0:2])

                print(getAxis(min_value[graphe_name], max_value[graphe_name], sorted(columns_name[graphe_name]), y_label, curves_titles, graph_title), file=flearn)

                for model in curves_titles:
                    if model not in marks.keys():
                        color = colors.pop(0)
                        marks[model] = ["draw=" + color]
                        marks[model].append("mark options={" + color + "}")
                        if "without structure" in model:
                            marks[model].append("dashed")
                        if (not "non ano" in model and "ano" in model and (len(marques_full)>0)) or len(marques_empty)==0:
                            marks[model].append("mark=" + marques_full.pop(0))
                        else:
                            marks[model].append("mark=" + marques_empty.pop(0))
                    columns = sorted(graphes[graphe_name][model].keys())
                    print("			\\addplot[" + ",".join(marks[model]) + "] coordinates {", end="", file=flearn)
                    for i_val, value in enumerate(columns):
                        print("(" + (i_val+1).__str__() + "," + graphes[graphe_name][model][value].__str__() + ")", end=" ", file=flearn)
                    print("}; %"+model, file=flearn)
                print("""\end{axis}
                    \end{tikzpicture}
                %\end{adjustbox}
            """, file=flearn)
                print(commentaires[graphe_name]+"\paragraph{}", file=flearn)

    flearn.close()



titles = sorted(accuracies.keys(), key=lambda x:max(accuracies[x]), reverse=True)

oldi_stout = sys.stdout

for tab_tiltes in ([], best_models, all_models_learned):

    if len(tab_tiltes) > 0:
        if tab_tiltes == best_models:
            flog = open(out_log + "/best_results.log", 'w')
            # We redirect the 'sys.stdout' command towards the descriptor file
            sys.stdout = flog
        else:
            flog = open(out_log + "/all_results.log", 'w')
            # We redirect the 'sys.stdout' command towards the descriptor file
            sys.stdout = flog

    for best_model in tab_tiltes:
        if best_model not in ("# accuracies", "", "# bleu", "# relex"):
            model = best_model[1]
            best_model = best_model[0].strip().replace("conll", "").strip()

            if model in numbers.keys():
                print("# " +numbers[model] + " # "+ best_model)

            best_model = best_model.replace(" ", "_")
            best_model = "--" + best_model
            best_model = best_model.replace("_|__",  " True --") + " True"

            if "sent_id" not in best_model:
                best_model +=" --no_sent_id True"
            else:
                best_model = best_model.replace("--sent_id True",  "--no_sent_id False")
            if "book_id" not in best_model:
                best_model += " --no_book_id True"
            else:
                best_model= best_model.replace("--book_id True",  "--no_book_id False")

            if "without structure" in model:
                best_model += " --without_structure True"
            if best_model == "-- True":
                print("python3 buildCoNLL-U.py")
            else:

                if "_maxTk" in best_model:
                    value = best_model[best_model.index("_maxTk"):]
                    value = value[:value.index(" ")]
                    best_model = best_model.replace(value, "")
                    best_model += " --max_tokens " + value[value.index("-")+1:]
                if "--maxTk" in best_model:
                    value = best_model[best_model.index("--maxTk"):]
                    value = value[:value.index(" ")]
                    best_model = best_model.replace(value + " True", "")
                    value = value.replace("--", "")
                    best_model += " --max_tokens " + value[value.index("-")+1:]

                if "--maxVTk" in best_model:
                    value = best_model[best_model.index("--maxVTk"):]
                    value = value[:value.index(" ")]
                    best_model = best_model.replace(value + " True", "")
                    value = value.replace("--", "")
                    best_model += " --max_valued_tokens " + value[value.index("-")+1:]

                if "_maxVTk" in best_model:
                    value = best_model[best_model.index("_maxVTk"):]
                    value = value[:value.index(" ")]
                    best_model = best_model.replace(value, "")
                    best_model += " --max_valued_tokens " + value[value.index("-")+1:]
                best_model = best_model.replace("_with", " True --with")
                print("python3 buildCoNLL-U.py " + best_model)
        else:
            print(best_model)

    if len(tab_tiltes) > 0:
        flog.close()
    sys.stdout = oldi_stout

if debug:
    print('(cd tex_debug && pdflatex draft.tex && pdflatex draft.tex && bibtex draft && pdflatex draft.tex && pdflatex draftLight.tex && pdflatex draftLight.tex && bibtex draftLight && pdflatex draftLight)')
else:
    print('(cd tex && pdflatex draft.tex && pdflatex draft.tex && bibtex draft && pdflatex draft.tex && pdflatex draftLight.tex && pdflatex draftLight.tex && bibtex draftLight && pdflatex draftLight)')
