# version 0.5
import glob
import os
import subprocess
import math
import shutil
from pathlib import Path
import random

debug = True
colineem = True

if debug:
    fichiers = sorted(glob.glob('outputs/models_debug/*'))
    fichiers_graph = sorted(glob.glob('outputs/graphs_debug/*'))
else:
    fichiers = sorted(glob.glob('outputs/models/*'))
    fichiers_graph = sorted(glob.glob('outputs/graphs/*'))
sentences_count = 0

little = []
middle = []
big = []
liste = []
commandes = []

#for i_f, fichier in enumerate(fichiers_graph):
#    if not os.path.exists(fichier.replace("graphs_debug", "models_debug")):
#        shutil.rmtree(fichier)
#        continue

commande = "python3 buildCoNLL-U.py --debug True" #

for ano in (" --anonymise_accord False", " --anonymise_accord True"):
    for structure in (" --without_structure True", " --without_structure False", " --light_structure True", " --inv_light_structure True"):
        for global_info in (" --insert_global_info True", " --insert_global_info False"):
            for reveal_adverbe in (" --reveal_adverbe True", " --reveal_adverbe False"):
                for time_as_parameter in (" --time_as_parameter True", " --time_as_parameter False"):
                    for author_id in (" --author_id True", " --author_id False"):
                        for no_book_id in (" --no_book_id True", " --no_book_id False"):
                            for no_sent_id in (" --no_sent_id True", " --no_sent_id False"):
                                for wan in (" -wan True", " -wan False"):
                                    if ano == " --anonymise_accord False" and reveal_adverbe == " --reveal_adverbe True":
                                        continue
                                    if ano == " --anonymise_accord False" and structure == " --light_structure True":
                                        continue
                                    if ano == " --anonymise_accord False" and structure == " --inv_light_structure True":
                                        continue
                                    if global_info == " --insert_global_info True" and author_id == " --author_id False" and no_book_id == " --no_book_id True" and no_sent_id == " --no_sent_id True" :
                                        continue
                                    if global_info == " --insert_global_info True" and " --without_structure True" == structure:
                                        # si on veut author id et/ou book_id et/ou book_id
                                        if author_id == " --author_id False" and no_book_id == " --no_book_id True" and no_sent_id == " --no_sent_id True":
                                            # mais qu'on n'a pas d'information structurelle...
                                            continue
                                    for simplify_output in (" --simplify_output True", " --simplify_output False"):
                                        if ano == " --anonymise_accord False" and simplify_output == " --simplify_output True":
                                            continue
                                        commandes.append(commande + ano + structure + global_info +reveal_adverbe + time_as_parameter + author_id + no_book_id + no_sent_id + wan + simplify_output)
# commandes = []

for i_f, fichier in enumerate(fichiers):

    if os.path.isfile(os.path.join(fichier + "cmd_emilie_done.bash")):
        os.rename(os.path.join(fichier + "cmd_emilie_done.bash"), os.path.join(fichier, "cmd_emilie_done.bash"))
    if os.path.isfile(os.path.join(fichier + "cmd_emilie.bash")):
        os.rename(os.path.join(fichier + "cmd_emilie.bash"), os.path.join(fichier, "cmd_emilie.bash"))
    if os.path.isfile(os.path.join(fichier + "cmd_done.bash")):
        os.rename(os.path.join(fichier + "cmd_done.bash"), os.path.join(fichier, "cmd_done.bash"))

    if not "anonymise" in fichier:
        if "light_structure" in fichier and os.path.isdir(fichier):
            shutil.rmtree(fichier)
            continue

    if os.path.isdir(fichier):
        pt = sorted(glob.glob(fichier + '/*pt'))
        if os.path.isfile(os.path.join(fichier, "pred.txt")) and os.path.isfile(os.path.join(fichier, "cmd_emilie.bash")):
            os.rename(os.path.join(fichier, "cmd_emilie.bash"), os.path.join(fichier , "cmd_emilie_done.bash"))
        if os.path.isfile(os.path.join(fichier, "pred.txt")) and os.path.isfile(os.path.join(fichier, "cmd.bash")):
            size = os.path.getsize(os.path.join(fichier, "cmd.bash"))
            if size > 0:
                os.rename(os.path.join(fichier, "cmd.bash"), os.path.join(fichier, "cmd_done.bash"))
                Path(os.path.join(fichier, "cmd.bash")).touch()

        # and os.path.isdir(os.path.join(fichier, "data")):
        if os.path.isfile(os.path.join(fichier, "pred.txt")) and os.path.isdir(os.path.join(fichier, "data")) and os.path.getsize(os.path.join(fichier, "pred.txt")) > 0:
            sorted_pt = sorted(pt, key=lambda x:int(x.replace(".pt", "").split("_")[-1]))
            if len(sorted_pt) > 1:
                liste.append( os.path.abspath(os.path.join(fichier, "data")))
                sorted_pt.pop(-1)
                for f_pt in sorted_pt:
                    os.remove(f_pt)
        elif len(pt) == 0 and len(glob.glob(fichier + '/data/*')) == 0:
            # absolument pas fait
            if os.path.isfile(os.path.join(fichier, "train_inputs.txt")):
                # par contra les fichiers semblent générés
                size = os.path.getsize(os.path.join(fichier, "train_inputs.txt"))
                size_mb = size / math.pow(1024, 2)
                if size_mb < 800:
                    little.append(os.path.join(fichier, "cmd.bash"))
                elif size_mb < 900:
                    middle.append(os.path.join(fichier, "cmd.bash"))
                else:
                    big.append(os.path.join(fichier, "cmd.bash"))
        else:
            if os.path.isfile(os.path.join(fichier, "cmd.bash")):
                size = os.path.getsize(os.path.join(fichier, "cmd.bash"))
                if size > 0:
                    if not os.path.exists(os.path.join(fichier , "cmd_done.bash")):
                        if os.path.exists(os.path.join(fichier , "cmd.bash")):
                            os.rename(os.path.join(fichier, "cmd.bash"), os.path.join(fichier, "cmd_done.bash"))
                            Path(os.path.join(fichier, "cmd.bash")).touch()
                    if not os.path.exists(os.path.join(fichier , "cmd_locale.bash")):
                        if os.path.exists(os.path.join(fichier , "cmd.bash")):
                            os.rename(os.path.join(fichier, "cmd.bash"), os.path.join(fichier, "cmd_locale.bash"))
                            Path(os.path.join(fichier, "cmd.bash")).touch()

print("# exclusions dropbox")
for element in liste:
    print("dropbox exclude add '" + element + "'")
#subprocess.run( "dropbox exclude add '" + "' '".join(liste) + "'", shell=True, check=True)


print("# ####")
print("# ")
print("# big")
random.shuffle(big)
for l in big:
    print("mv " + l + " " + l.replace("cmd.bash", "cmd_done.bash"))
    print("mv " + l.replace("cmd.bash", "cmd_emilie.bash") + " " + l.replace("cmd.bash", "cmd_emilie_done.bash"))
    if colineem:
        print("bash " + l.replace("cmd.bash", "cmd_done.bash"))
    else:
        print("bash " + l.replace("cmd.bash", "cmd_emilie_done.bash"))

print("# little")
for l in little:
    print("mv " + l + " " + l.replace("cmd.bash", "cmd_done.bash"))
    print("mv " + l.replace("cmd.bash", "cmd_emilie.bash") + " " + l.replace("cmd.bash", "cmd_emilie_done.bash"))
    if colineem:
        print("bash " + l.replace("cmd.bash", "cmd_done.bash"))
    else:
        print("bash " + l.replace("cmd.bash", "cmd_emilie_done.bash"))
    print()
    print("# next")
    
print()
print("# ####")
print("# ")
print("# middle")
for l in middle:
    print("mv " + l + " " + l.replace("cmd.bash", "cmd_done.bash"))
    print("mv " + l.replace("cmd.bash", "cmd_emilie.bash") + " " + l.replace("cmd.bash", "cmd_emilie_done.bash"))
    if colineem:
        print("bash " + l.replace("cmd.bash", "cmd_done.bash"))
    else:
        print("bash " + l.replace("cmd.bash", "cmd_emilie_done.bash"))
print()
print("# ")
print("# commandes")

sep = ">"
random.shuffle(commandes)
# commandes = []
if len(commandes) > 0:
    print("# ####")
    print("# ")
    print("# options")
    for l in commandes:
        print(l + " |  grep emilie " + sep  + " fichier_bash.bash ")
        sep = ">>"
