
import glob
import os
import copy
import json
import numpy as np
from pathlib import Path

import argparse
import math
from tqdm import tqdm
import shutil
import sys

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481


fichiers = glob.glob('./models/*/data.csv')

model_keys = "#model;structure;insert global info;insert global contraintes;author id;book id;sent id;anonymise accord;time as parameter;reveal adverbe;simplify output;without contraintes;max tokens;max valued tokens;without lvf;accuracy;vocab_source;vocab_target;train sent.".split(";")
data_keys = ['bleu', 'original.length', 'original.delex.count', 'relex']

lines = []

lines.append(";".join(data_keys + model_keys).replace(" ", ""))

progress = tqdm(range(len(fichiers)))

for fichier in fichiers:
    root = fichier.replace("data.csv", "")
    f = open(fichier, "r")
    model = {}
    keys = f.readline().strip().split(";")
    values = f.readline().strip().split(";")
    for k, v in zip(keys, values):
        model[k] = v
    if not model['epochs'].isnumeric() :
        progress.update()
        continue
    if int(model['epochs'])<10:
        progress.update()
        continue

    with open(root + "pred.txt") as f: # TO USE
        preds = f.readlines()
    with open(root + "test_outputs.txt") as f: # TO USE
        silver = f.readlines()

    stats_ = json.load(open(root + "stat_per_ref.json", "r"))

    stats = {}
    for stat in stats_.keys():
        obtained, wanted = stat.split("|")
        stats[stat] = stats_[stat]
        stats[stat]['obtained'] = obtained
        stats[stat]['wanted'] = wanted
        stats[stat]['original.length'] = len(wanted.split())
        stats[stat]['original.delex.count'] = stats[stat]['looked for count']
        #stats[stat]['original.constraints.count'] = stats[stat]['contraintes count']
        del stats[stat]['looked for count']
        del stats[stat]['contraintes count']
        stats[stat]['pred.length'] = len(obtained.split())

        if stats[stat]['bleu'] > 100:
            stats[stat]['bleu'] = 100.

    del stats_

    # c'est une ridicule perte de temps, mais si je reviens pour augmenter la masse d'infos, j'aurai besoin deme situer
    # linéairement

    for pred, silv in zip(preds, silver):
        pred = pred.strip()
        silv = silv.strip()
        lines.append("")
        sep = ""
        for key in data_keys:
            lines[-1] += sep + stats[pred+"|"+silv.replace('|', "")][key].__str__()
            sep = ";"
        for key in model_keys:
            lines[-1] += sep + model[key]


    progress.update()
progress.close()

with open('./models/lines.csv', "w") as f:
    f.writelines("%s\n" % l for l in lines)