from pyforms.basewidget import BaseWidget
from pyforms.controls   import ControlFile
from pyforms.controls   import ControlText
from pyforms.controls   import ControlSlider
from pyforms.controls   import ControlPlayer
from pyforms.controls   import ControlButton
from pyforms.controls   import ControlCheckBox
from pyforms.controls   import ControlCombo
from pyforms.controls   import ControlEmptyWidget
from pyforms.controls   import ControlLabel
from pyforms.controls   import ControlTextArea

class CommandGenerator(BaseWidget):

    def __init__(self, *args, **kwargs):
        super().__init__('Commands generator')

        #Definition of the forms fields
        self._debug    = ControlCheckBox('Debug')
        self._structure     = ControlCombo('Structure')
        self._info_globale    = ControlCheckBox('Information globale')
        self._info_globale_contraintes    = ControlCheckBox('Information contraintes')
        self._author    = ControlCheckBox('Auteur')
        self._book    = ControlCheckBox('Livre')
        self._sent    = ControlCheckBox('Phrase')
        self._ano    = ControlCheckBox('Anonymisation')
        self._time    = ControlCheckBox('Temps')
        self._ra    = ControlCheckBox('Révélation adverbes')
        self._so    = ControlCheckBox('Simplification sortie')
        self._lvf = ControlCheckBox('Sans lvf')
        self._synt    = ControlCheckBox('Sans contraintes syntaxiques')
        self._size    = ControlCheckBox('Nombre de tokens')
        self._valuedT    = ControlCheckBox('Nb tokens valués')
        self._aide    = ControlTextArea('Ici s\'affiche la commande ')
        self._button        = ControlButton('=>')

        self._structure.add_item('Complète', 'full')
        self._structure.add_item('Légère (lignes touchant valeurs anonymisées)', 'light')
        self._structure.add_item('Inverse (lignes ne touchant pas les valeurs anonymisées)', 'inv')
        self._structure.add_item('Aucune', 'None')
        #Define the organization of the Form Controls
        controls = [['_structure', ["   ", "||",(['_debug', '_info_globale',    '_info_globale_contraintes',      '',     '', ''], "=",
                                    ['Identifiants: ', '_author', '_book', '_sent', '', ''],"=",
                                    ['Spécifications: ', '_ano', '_ra', '_time', '_so', '_lvf'], "=",
                                    ['Contraintes: ', '_synt', '_size', '_valuedT', ' ', ' ' ]
                                    ), '||', '_button', "  ", "||",
                    ('_aide', )]]]
        self._formset =  controls

        # Define the button action
        self._button.value = self.__buttonAction

    def __buttonAction(self):
        """Button action event"""
        olf= self._aide.value
        self._aide.value = 'python3 buildCoNLL-U.py '
        if self._debug.value:
            self._aide.value += " --debug True"
        if self._structure.value == "None":
            self._aide.value += " --without_structure True"
        elif self._structure.value == "light":
            self._aide.value += " --light_structure True"
        elif self._structure.value == "inv":
            self._aide.value += " --inv_light_structure True"
        if self._info_globale.value:
            self._aide.value += " --insert_global_info True"
        if self._info_globale_contraintes.value:
            self._aide.value += " --insert_global_contraintes True"
        if self._author.value:
            self._aide.value += " --author_id True"
        if self._sent.value:
            self._aide.value += " --no_sent_id False"
        else:
            self._aide.value += " --no_sent_id True"
        if self._book.value:
            self._aide.value += " --no_book_id False"
        else:
            self._aide.value += " --no_book_id True"
        if self._ano.value:
            self._aide.value += " --anonymise_accord True"
        if self._time.value:
            self._aide.value += " --time_as_parameter True"
        if self._ra.value:
            self._aide.value += " --reveal_adverbe True"
        if self._so.value:
            self._aide.value += " --simplify_output True"

        if self._lvf.value:
            self._aide.value += " --without_lvf True"
        else:
            self._aide.value += " --without_lvf False"
        if self._synt.value:
            self._aide.value += " --without_contraintes True"
        else:
            self._aide.value += " --without_contraintes False"
        if self._size.value:
            self._aide.value += " --max_tokens 28"
        if self._valuedT.value:
            self._aide.value += " --max_valued_tokens 6"

        self._aide.value += "\n" + olf
if __name__ == '__main__':

    from pyforms import start_app
    start_app(CommandGenerator)