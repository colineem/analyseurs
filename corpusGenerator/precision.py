# version 0.9963
import glob
import os
import json
import string
import argparse
import numpy
import regex as re


parser = argparse.ArgumentParser(description="Evaluation de corpus")

parser.add_argument('--refresh', dest='refresh', action='store_true')

args = parser.parse_args()
exclude = set(string.punctuation)
regex = re.compile('[%s]' % re.escape(string.punctuation))

gold = ["le", "sur", "une"]
gen = ["la", "dans", "dans", "une"]

count_attrib = 0
count_gold = 0
count_gen = 0
for each in gen:
    if each in gold:
        count_gen += 1
        count_attrib += 1
        count_gold += 1
        gold.remove(each)
    else:
        count_attrib += 1
count_gold += len(gold)
print(count_gen , end="/")
print(count_attrib )
print(count_gen , end="/")
print(count_gold )
dico = {"précision":count_gen/count_attrib, "recall":count_gen/count_gold}
print(dico)

fichiers_waited = glob.glob('./features/*/test_outputs.txt')
#pred_relex.txt

for fichier_waited in fichiers_waited:
    fichier_pred = fichier_waited.replace("test_outputs", "pred")
    fichier_json = fichier_waited.replace("test_outputs", "test_anonymise_outputs")
    fichier_stats = fichier_waited.replace("test_outputs", "stats")

    if not os.path.isfile(fichier_stats) or args.refresh:
        if not os.path.exists(fichier_pred):
            continue
        preds = open(fichier_pred, 'r').readlines()
        waiteds = open(fichier_waited, 'r').readlines()
        données = open(fichier_json, 'r').readlines()
        précisions = []
        recalls = []
        golds = []
        gens = []
        for pred, waited, data in zip(preds, waiteds, données):
            data = eval(data)

            gold = waited[:].split()
            gen = pred[:].split()
            for word in data.keys():
                if word in gold:
                    gold.remove(word)
                if word in gen:
                    gen.remove(word)

            gold = regex.sub('', " ".join(gold)).split()
            gen = regex.sub('', " ".join(gen)).split()
            dico = {}
            golds += gold
            gens += gen
            if sorted(gold)== sorted(gen):
                précision = 1
                recall = 1
            else:
                count_attrib = 0
                count_gold = 0
                count_gen = 0
                for each in gen:
                    if each in gold:
                        count_gen += 1
                        count_attrib += 1
                        count_gold += 1
                        gold.remove(each)
                    else:
                        count_attrib += 1
                count_gold += len(gold)

            précision = count_gen / count_attrib if count_attrib > 0 and count_gen > 0 else 1
            recall = count_gen / count_gold if count_gold > 0 and count_gen > 0 else 1
            dico["précision micro"] = précision
            dico["recall micro"] = recall
            précisions.append(dico["précision micro"])
            recalls.append(dico["recall micro"])

        count_attrib = 0
        count_gold = 0
        count_gen = 0
        for each in gens:
            if each in golds:
                count_gen += 1
                count_attrib += 1
                count_gold += 1
                golds.remove(each)
            else:
                count_attrib += 1
        count_gold += len(golds)

        précision = count_gen / count_attrib if count_attrib > 0 and count_gen > 0 else 1
        recall = count_gen / count_gold if count_gold > 0 and count_gen > 0 else 1


            #FW_gold = {le sur une}
            #FW_gen = {["la", "dans", "dans", "une"]}
            #correct = {une}
            #precision = 1 / 4
            #recall = 1 / 3

        dic= {"précision médiane":numpy.median(précisions), "précision moyenne":numpy.mean(précisions),
              "recall médian":numpy.median(recalls), "recall moyen":numpy.mean(recalls), "précision macro": précision, "recall macro":recall, "FW macro" :2*((précision*recall)/(précision+recall))}

        fjson = open(fichier_stats, "w")
        fjson.write(json.dumps(dic, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
    else:
        with open(fichier_stats, "r") as a:
            dic = json.load(a)

    print(fichier_pred)
    print(dic)
    for dick in dic.keys():
        print(dick, end=": ")
        print(round(dic[dick], 3))