__all__ = ['UPOS', 'Contrainte', 'Features']

import weakref
from enum import Enum

class FlyWeightMeta(type):

    def __new__(mcs, name, parents, dct):
        dct['pool'] = weakref.WeakValueDictionary()
        return super(FlyWeightMeta, mcs).__new__(mcs, name, parents, dct)

    @staticmethod
    def _serialize_params(cls, *args, **kwargs):
        args_list = map(str, args)
        args_list.extend([str(kwargs), cls.__name__])
        key = ''.join(args_list)
        return key

    def __call__(cls, *args, **kwargs):
        key = FlyWeightMeta._serialize_params(cls, *args, **kwargs)
        pool = getattr(cls, 'pool', {})

        instance = pool.get(key)
        if not instance:
            instance = super(FlyWeightMeta, cls).__call__(*args, **kwargs)
            pool[key] = instance
        return instance

class UPOS(Enum):
    __metaclass__ = FlyWeightMeta
    adjective = 1  #: adjective
    adposition = 2  #: adposition
    adverb = 3  #: adverb
    auxiliary = 4  #: auxiliary
    coordinatingConjunction = 5  #:
    determiner = 6  #: determiner
    interjection = 7  # interjection
    noun = 8  # noun
    numeral = 9  # numeral
    particle = 10  # particle
    pronoun = 11  # pronoun
    properNoun = 12  #
    punctuation = 13  # punctuation
    subordinatingConjunction = 14  #
    symbol = 15  #
    verb = 16  #
    X = 17  # other

    def invariables():
        return [ UPOS.adposition,  UPOS.adverb, UPOS.coordinatingConjunction, UPOS.interjection, UPOS.particle, UPOS.punctuation, UPOS.subordinatingConjunction ]

    def __str__(self):
        if not hasattr(self, '_value'):
            if self == UPOS.coordinatingConjunction:
                self._value = "CCONJ"
            elif self == UPOS.interjection:
                self._value = "INTJ"
            elif self == UPOS.properNoun:
                self._value = "PROPN"
            elif self == UPOS.subordinatingConjunction:
                self._value = "SCONJ"
            elif self == UPOS.punctuation:
                self._value = "PUNCT"
            elif self == UPOS.verb:
                self._value = "VERB"
            elif self == UPOS.particle:
                self._value = "PART"
            elif self == UPOS.noun:
                self._value = "NOUN"
            elif self == UPOS.pronoun:
                self._value = "PRON"
            elif self == UPOS.X:
                self._value = "X"
            else:
                self._value = self.name[0:3].upper()
        return self._value


class Contrainte(Enum):
    __metaclass__ = FlyWeightMeta
    NEUTRE = 0
    RELATIVE_SUJET = 1
    RELATIVE_OBJET = 2
    INFINITIF = 3
    VPP = 4
    VPA = 5
    COMPLEMENT = 6
    COMPARATIF = 7
    JUXTAPOSITION = 8

    def __str__(self):
        if not hasattr(self, '_value'):
            if self == Contrainte.RELATIVE_SUJET:
                self._value = "relativeSujet"
            elif self == Contrainte.RELATIVE_OBJET:
                self._value = "relative"
            elif self == Contrainte.INFINITIF:
                self._value = "infinitif"
            elif self == Contrainte.VPP:
                self._value = "vpp"
            elif self == Contrainte.VPA:
                self._value = "vpa"
            elif self == Contrainte.COMPLEMENT:
                self._value = "complément"
            elif self == Contrainte.COMPARATIF:
                self._value = "comparatif"
            elif self == Contrainte.JUXTAPOSITION:
                self._value = "juxtaposition"
            else:
                self._value = ""
        return self._value


class Features:
    __metaclass__ = FlyWeightMeta

    class Actualisation(Enum):
        __metaclass__ = FlyWeightMeta
        DEFINI = 1
        INDEFINI = 0
        INCONNU = -1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Actualisation.DEFINI:
                    self._value = "actualisation=Defini"
                elif self == Features.Actualisation.INDEFINI:
                    self._value = "actualisation=Indefini"
                else:
                    self._value = ""
            return self._value

    class Masse(Enum):
        __metaclass__ = FlyWeightMeta
        TOTALE = 1
        PARTIELLE = 0
        NULLE = -1
        INCONNUE = -2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Masse.TOTALE:
                    self._value = "masse=Totale"
                elif self == Features.Masse.PARTIELLE:
                    self._value = "masse=Partielle"
                elif self == Features.Masse.NULLE:
                    self._value = "masse=Nulle"
                else:
                    self._value = ""
            return self._value

    class Polarity(Enum):
        __metaclass__ = FlyWeightMeta
        NEG = -1
        NEUTRE = 0
        POS = 1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Polarity.NEG:
                    self._value = "polarity=Neg"
                elif self == Features.Polarity.POS:
                    self._value = "polarity=Pos"
                else:
                    self._value = ""
            return self._value

    class Gender(Enum):
        __metaclass__ = FlyWeightMeta
        COMMON = 0
        FEMININE = 1
        MASCULINE = 2
        NEUTER = 3
        INDEFINI = 4
        # il y a un genre, mais on ne sait lequel
        DEFINI = 5  # un déterminant est passé, on ne sait pas le genrer

        # ("la méthode l'" <- "l'" est déclaré det il ne faut pas qu'il écrase le déterminant passé avant

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Gender.COMMON:
                    self._value = "gender=Com"
                elif self == Features.Gender.FEMININE:
                    self._value = "gender=Fem"
                elif self == Features.Gender.MASCULINE:
                    self._value = "gender=Masc"
                elif self == Features.Gender.NEUTER:
                    self._value = "gender=Neut"
                elif self == Features.Gender.COMMON:
                    self._value = "gender=Com"
                else:
                    self._value = ""
            return self._value

    class Number(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = 0
        SINGULAR = 1
        PLURAL = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Number.SINGULAR:
                    self._value = "number=Sing"
                elif self == Features.Number.PLURAL:
                    self._value = "number=Plur"
                else:
                    self._value = ""
            return self._value

    class Mood(Enum):
        __metaclass__ = FlyWeightMeta
        INDICATIVE = 0
        SUBJUNCTIVE = 1
        CONDITIONAL = 2
        INDEFINI = -1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Mood.INDICATIVE:
                    self._value = "mood=Ind"
                elif self == Features.Mood.CONDITIONAL:
                    self._value = "mood=Cnd"
                elif self == Features.Mood.SUBJUNCTIVE:
                    self._value = "mood=Sub"
                else:
                    self._value = ""
            return self._value

    class Tense(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        PAST = 0
        PRESENT = 1
        FUTURE = 2
        IMPERFECT = 3
        PLUPERFECT = 4

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Tense.PAST:  # The past tense denotes actions that happened before a reference point
                    self._value = "tense=Past"
                elif self == Features.Tense.PRESENT:
                    self._value = "tense=Pres"
                elif self == Features.Tense.FUTURE:
                    self._value = "tense=Fut"
                elif self == Features.Tense.IMPERFECT:  # ne comporte pas de limitation temporelle, sauf à l’intervention d’une cause extérieure.
                    self._value = "tense=Imp"
                elif self == Features.Tense.PLUPERFECT:  # The pluperfect denotes action that happened before another action in past.
                    self._value = "tense=Pqp"
                else:
                    self._value = ""
            return self._value

    class Aspect(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        IMPERFECT = 0
        PERFECT = 1
        PROGRESSIVE = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Aspect.IMPERFECT:
                    self._value = "aspect=Imp"
                elif self == Features.Aspect.PERFECT:
                    self._value = "aspect=Perf"
                elif self == Features.Aspect.PROGRESSIVE:
                    self._value = "aspect=Prog"
                else:
                    self._value = ""
            return self._value

    class Voice(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ACTIVE = 0
        PASSIVE = 1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Voice.ACTIVE:
                    self._value = "voice=Act"
                elif self == Features.Voice.PASSIVE:
                    self._value = "voice=Pass"
                else:
                    self._value = ""
            return self._value

    class VerbForm(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        FINITE = 0
        INFINITIVE = 1
        PARTICIPLE = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.VerbForm.FINITE:
                    self._value = "verbForm=Fin"
                elif self == Features.VerbForm.INFINITIVE:
                    self._value = "verbForm=Inf"
                elif self == Features.VerbForm.PARTICIPLE:
                    self._value = "verbForm=Part"
                else:
                    self._value = ""
            return self._value

    class Person(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ZERO = 0
        FIRST = 1
        SECOND = 2
        THIRD = 3

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Person.ZERO:
                    self._value = "person=0"
                elif self == Features.Person.FIRST:
                    self._value = "person=1"
                elif self == Features.Person.SECOND:
                    self._value = "person=2"
                elif self == Features.Person.THIRD:
                    self._value = "person=3"
                else:
                    self._value = ""
            return self._value

    class Possession(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ZERO = 0
        FIRST = 1
        SECOND = 2
        THIRD = 3

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Possession.ZERO:
                    self._value = "possession=0"
                elif self == Features.Possession.FIRST:
                    self._value = "possession=1"
                elif self == Features.Possession.SECOND:
                    self._value = "possession=2"
                elif self == Features.Possession.THIRD:
                    self._value = "possession=3"
                else:
                    self._value = ""
            return self._value

