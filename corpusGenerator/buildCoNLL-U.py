print("v 0.9.32")
import argparse
import copy
import glob
import json
import os
import re
import shutil
import subprocess
import sys
from enum import Enum
from inspect import currentframe, getframeinfo
import getpass
# import gc
import time
import signal
# gc.disable()
import tracemalloc
import weakref
import linecache
from unidecode import unidecode



def display_top(snapshot, key_type='lineno', limit=10):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))


try:
    import pygraphviz
    from networkx.drawing.nx_agraph import write_dot
    # print("using package pygraphviz", file=sys.stderr)
except ImportError:
    try:
        import pydot
        from networkx.drawing.nx_pydot import write_dot

        print("using package pydot", file=sys.stderr)
    except ImportError:
        print("", file=sys.stderr)
        print("Both pygraphviz and pydot were not found ", file=sys.stderr)
        print("see  https://networkx.github.io/documentation/latest/reference/drawing.html", file=sys.stderr)
        print("", file=sys.stderr)
        raise
user = getpass.getuser()
debug_for_user = "emilie"

from tqdm import tqdm
import random
import math

i_write = 0  # compteur des fichiers sortis en graphe
max_input = 0
max_output = 0

take_table = {}


def log(lineNumber):
    take_table[lineNumber] = take_table[lineNumber] + 1 if lineNumber in take_table.keys() else 1

#￨(\S)*￨\S+ <-nettoyage feature
DEBUG_TEST_SENT_COUNT = 100
DEBUG_VAL_SENT_COUNT = 100
DEBUG_TRAIN_SENT_COUNT = 500

from pattern.fr import singularize, pluralize
import networkx as nx

count_not_take = 0
sentences_count = 0
manual_sentences_count = 0
count_take = 0

max_len_i = 0
max_len_o = 0
len_i = []
len_o = []

NA = ""
ZERO = "0"


class FlyWeightMeta(type):

    def __new__(mcs, name, parents, dct):
        dct['pool'] = weakref.WeakValueDictionary()
        return super(FlyWeightMeta, mcs).__new__(mcs, name, parents, dct)

    @staticmethod
    def _serialize_params(cls, *args, **kwargs):
        args_list = map(str, args)
        args_list.extend([str(kwargs), cls.__name__])
        key = ''.join(args_list)
        return key

    def __call__(cls, *args, **kwargs):
        key = FlyWeightMeta._serialize_params(cls, *args, **kwargs)
        pool = getattr(cls, 'pool', {})

        instance = pool.get(key)
        if not instance:
            instance = super(FlyWeightMeta, cls).__call__(*args, **kwargs)
            pool[key] = instance
        return instance


TRAIT_VIERGE = None
shorted_TRAIT_VIERGE = None

emnlp = True  # valeur de without structure et contraintes

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

parser = argparse.ArgumentParser(description="Création de corpus")

parser.add_argument('--debug', type=str2bool, default=False,
                    help='limite les traitements à un petit fichier')
parser.add_argument('--refresh_dic', type=str2bool, default=False,
                    help='Rafraîchit les dictionnaires de genre et nombre')
parser.add_argument('--log', type=str2bool, default=True,
                    help='redirige les sorties vers un log')
parser.add_argument('--force', type=str2bool, default=False,
                    help='recrée les données')

# limites sur phrases
parser.add_argument("-mav", "--max_valued_tokens", type=int, default=-1,
                    help='nombre maximal de tokens, -1 pour désactiver la limite')
parser.add_argument("-t", "--with_traits", type=str2bool, default=True,
                    help='les traits sortent')
parser.add_argument("-ws", "--without_structure", type=str2bool, default=emnlp,
                    help='les strcuture ne sortent pas dans les sorties d\'apprentissage')
parser.add_argument("-gi", "--insert_global_info", type=str2bool, default=False,
                    help='Insère une ligne supplémentaires de biais propres au corpus d\'apprentissage')
parser.add_argument("-gc", "--insert_global_contraintes", type=str2bool, default=False,
                    help='Insère une ligne supplémentaires de biais propres avec la liste des contraintes')
parser.add_argument("-wl", "--without_liaison", type=str2bool, default=True,
                    help='évacue les structures à liaison')
parser.add_argument("-a", "--anonymise", type=str2bool, default=False,
                    help='anonymisation des entrées')
parser.add_argument("-aa", "--anonymise_accord", type=str2bool, default=False,
                    help='anonymisation des entrées avec accords')
parser.add_argument("-wlvf", "--without_lvf", type=str2bool, default=False,
                    help='n\'utilise pas la classe lvf pour l\'anonymisation')
parser.add_argument("-ra", "--reveal_adverbe", type=str2bool, default=False,
                    help='ne pas anonymiser les adverbes')
parser.add_argument("-rj", "--reveal_adjectif", type=str2bool, default=False,
                    help='ne pas anonymiser les adjectifs')
parser.add_argument("-ls", "--light_structure", type=str2bool, default=False,
                    help='ne sortir que les structures recoupant les infos structurelles des entrées')
parser.add_argument("-ils", "--inv_light_structure", type=str2bool, default=False,
                    help='ne sortir pas les structures recoupant les infos structurelles des entrées')
parser.add_argument("-tap", "--time_as_parameter", type=str2bool, default=False,
                    help='temps passé en paramètre dans les sorties')
parser.add_argument("-fap", "--features_in_embeddings", type=str2bool, default=False,
                    help='features passé en paramètre dans les sorties')
# limites sur verbes
parser.add_argument("-da", "--doubled_with_author", type=str2bool, default=False,
                    help='sorties ordonnées des mots anonymisables')
parser.add_argument("-o", "--ordered", type=str2bool, default=False,
                    help='anonymisation ordonnées')
parser.add_argument("-oa", "--ordered_names_by_author", type=str2bool, default=False,
                    help='sorties ordonnées des mots anonymisables préfixées par le nom de l\'auteur')
parser.add_argument("-oo", "--ordered_names", type=str2bool, default=False,
                    help='sorties ordonnées des mots anonymisables')
parser.add_argument("-aid", "--author_id", type=str2bool, default=False,
                    help='identifiant d\'auteur en sortie')
parser.add_argument("-nbid", "--no_book_id", type=str2bool, default=False,
                    help='pas d\'identifiant de livre en sortie')
parser.add_argument("-nsid", "--no_sent_id", type=str2bool, default=False,
                    help='pas d\'identifiant de phrase en sortie')
parser.add_argument("-wan", "--without_contraintes", type=str2bool, default=emnlp,
                    help='ni contrainte, ni informations contextuelles : désactive tout argument touchant le contexte')
parser.add_argument("-mat", "--max_tokens", type=int, default=-1,
                    help='nombre maximal de tokens demandés, en mode anonyme, -1 pour désactiver la limite')
parser.add_argument("-p", "--path", type=str2bool, default=False,
                    help='Obtention du chemin')
#
# parser.add_argument("-mac", "--max_conj", type=int, default=-1,
#                     help='nombre maximal de verbes conjugués, -1 pour désactiver la limite')
# parser.add_argument("-mic", "--min_conj", type=int, default=-1,
#                     help='nombre minimal de verbes conjugués, -1 pour désactiver la limite')
# parser.add_argument("-mai", "--max_inf", type=int, default=-1,
#                     help='nombre maximal de verbes à l\infinitif, -1 pour désactiver la limite')
# parser.add_argument("-mii", "--min_inf", type=int, default=-1,
#                     help='nombre minimal de verbes à l\infinitif, -1 pour désactiver la limite')
# parser.add_argument("-mapa", "--max_vpa", type=int, default=-1,
#                     help='nombre maximal de verbes au participe présent, -1 pour désactiver la limite')
# parser.add_argument("-mipa", "--min_vpa", type=int, default=-1,
#                     help='nombre minimal de verbes au participe présent, -1 pour désactiver la limite')
# parser.add_argument("-map", "--max_vpp", type=int, default=-1,
#                     help='nombre maximal de verbes au participe passé, -1 pour désactiver la limite')
# parser.add_argument("-mip", "--min_vpp", type=int, default=-1,
#                     help='nombre minimal de verbes au participe passé, -1 pour désactiver la limite')
# parser.add_argument("-maa", "--max_all", type=int, default=-1,
#                     help='nombre maximal de verbes au participe passé, -1 pour désactiver la limite')
# parser.add_argument("-mia", "--min_all", type=int, default=-1,
#                     help='nombre minimal de verbes au participe passé, -1 pour désactiver la limite')

    
args = parser.parse_args()
if args.anonymise_accord:
    args.reveal_adverbe = True
    args.reveal_adjectif = True
else:
    args.reveal_adverbe = False
    args.reveal_adjectif = False
if not args.with_traits:
    args.with_traits = True

if args.ordered_names_by_author:
    args.ordered_names = True
    if args.author_id:
        print("args.author_id inutile, non implémenté avec ordered_names_by_author ")
        raise Exception

if not args.path:

    for arg in sorted(args.__dict__):
        if not arg in ("log", "path"):
            value = eval('args.' + arg)
            if (isinstance(value, bool)) or (isinstance(value, str)) or value > -1:
                for sortie in (sys.stderr,):
                    print("\t" + arg, end=": ", file=sortie)
                    print(value.__str__(), file=sortie)
    for sortie in (sys.stderr,):
        print("", file=sortie)

if args.author_id == False and args.no_book_id and args.no_sent_id and args.insert_global_info:
    print("args.author_id == False and args.no_book_id and args.no_sent_id and args.insert_global_info")
    print("il faut quelque chose à injecter")
    raise Exception

if args.author_id == False and args.no_book_id and args.no_sent_id and args.insert_global_contraintes:
    print("args.author_id == False and args.no_book_id and args.no_sent_id and args.insert_global_contraintes")
    print("il faut quelque chose à injecter pour lier les contraintes")
    raise Exception

if args.light_structure and args.inv_light_structure:
    print("args.light_structure et args.inv_light_structure : without-structure à False offre le cumul des deux.")
    raise Exception

if args.light_structure and args.without_structure:
    print("args.light_structure et args.without-structure incompatible")
    raise Exception

if args.anonymise_accord:
    args.elision = True
    print("elision en place")
else:
    args.elision = False

if not args.anonymise_accord and args.elision:
    print("elision seulement possible quand anonymisation")
    raise Exception

if not args.anonymise and not args.anonymise_accord and args.reveal_adverbe:
    print("argsreveal_adverbe seulement possible quand anonymisation")
    raise Exception

if not args.anonymise and not args.anonymise_accord and args.reveal_adjectif:
    print("reveal_adjectif seulement possible quand anonymisation")
    raise Exception

if args.with_traits and args.features_in_embeddings:
    print("features_in_embeddings seulement sans traits")
    raise Exception

# if not args.with_traits and not args.features_in_embeddings:
# if not args.debug:
# print("le temps des tests, on veut soit l'un soit l'autre")
# raise Exception

if not args.anonymise_accord and args.features_in_embeddings:
    print("features_in_embeddings seulement possible quand anonymisation")
    raise Exception

# je n'ai  pas l'id du livre, pas le n° de sentence et pas l'auteur, je n'ai rien pour préfixer la structure et l'insertion d'info globale.
if args.no_book_id == True and args.no_sent_id == True and args.author_id == False:
    if args.without_structure and (
            args.insert_global_info == False and args.light_structure == False and args.inv_light_structure == False):
        print("il faut une structure, quelque chose à préfixer pour utiliser un préfixe")
        raise Exception

if args.inv_light_structure and args.without_structure:
    print("args.inv_light_structure et args.without-structure incompatible")
    raise Exception

arbres_extraits = {}
if args.anonymise_accord:
    args.anonymise = False

if args.debug and False:
    fichiers = sorted(glob.glob(os.path.join(fullpath, '../decrypteur/arbres/simple/arbres_extraits_002*.json')))
else:
    fichiers = sorted(glob.glob(os.path.join(fullpath, '../decrypteur/arbres/random/arbres_extraits*.json')))
    if len(fichiers) != 10:
        args.debug = True
        fichiers = sorted(glob.glob(os.path.join(fullpath, '../decrypteur/arbres/simple/arbres_extraits_002*.json')))
assert len(fichiers) == 10

if args.debug:
    tracemalloc.start()

# on va ranger automatiquement les modèles dans des répertoires créés
model_dir = sys.argv[0][sys.argv[0].rindex("/") + 1:-3] if ("/") in sys.argv[0] else sys.argv[0][:-3]
conll_file = ""
for arg in sorted(args.__dict__):
    if not arg in ("log", "debug", "force"):
        value = eval('args.' + arg)
        if (value and value > -1) or value == 0 and not (isinstance(value, bool)):
            if (isinstance(value, bool)):
                model_dir += "/" + arg.__str__()
            else:
                model_dir += "/" + arg.__str__() + "_" + value.__str__()

if args.debug:
    conll_file = "outputs/feats_IO_debug/" + model_dir.replace("/", "-") + "_wc/CoNLLext.conllup"
    conll_log = "outputs/feats_IO_debug/" + model_dir.replace("/", "-") + "_wc/CoNLLext.log"
    model_dir = conll_file.replace("/CoNLLext.conllup", "")  # "features/" + model_dir
    graphs_dir = model_dir.replace("feats_IO_debug/", "graphs_debug/")
else:
    # if user == debug_for_user and not args.force:
    #    exit()
    conll_file = "feats_IO/" + model_dir.replace("/", "-") + "_wc/CoNLLext.conllup"
    conll_log = "feats_IO/" + model_dir.replace("/", "-") + "_wc/CoNLLext.log"
    model_dir = conll_file.replace("/CoNLLext.conllup", "")  # "feats_IO/" + model_dir
    graphs_dir = model_dir.replace("feats_IO/", "graphs/")

if args.path:
    print("# bash " + model_dir + '/cmd.bash')
    exit(0)

if os.path.exists(model_dir):
    if args.force:
        shutil.rmtree(model_dir)
    else:
        print("# " + model_dir + " déjà existant. Option --force True pour le recréer")

        fichier_log = glob.glob(model_dir + '/logs/log.txt')
        if len(fichier_log) == 0:
            print("# lancement apprentissage :")
            print(model_dir + '/cmd.bash')
        else:
            print("# un fichier de log historisant un apprentissage existe pour ce modèle.")
            print("# " + model_dir + '/logs/log.txt')
            print("# " + model_dir + '/cmd.bash')
        exit(0)
os.makedirs(model_dir)

stats = {
    'aucune contrainte': 0, "détails": {'moods': {}, 'verbsForm': {}},
    "size_token_corpus": {
        'all': {'inputs': {}, 'outputs': {}},
        'structure': {'inputs': {}, 'outputs': {}},
        'final': {'inputs': {}, 'outputs': {}}}
}

mega_print = False

if os.path.exists(graphs_dir):
    shutil.rmtree(graphs_dir)
os.makedirs(graphs_dir)

if args.log:
    flog = open(conll_log, 'w')
    # We redirect the 'sys.stdout' command towards the descriptor file

    old_stout = sys.stdout
    sys.stdout = flog

if args.log:
    print("lancement", file=flog)
    print("Local time:", time.ctime(time.time()), file=flog)
    flog.flush()

print("python3 " + " ".join(sys.argv), file=sys.stderr, end=". ")
print("python3 " + " ".join(sys.argv), file=sys.stdout)
print("Fichier d'info: " + conll_file, file=sys.stderr, end=". ")
print("Fichier d'info: " + conll_file, file=sys.stdout)
print("Fichiers du modèle: " + model_dir, file=sys.stderr, end=". ")
print("Fichiers du modèle: " + model_dir, file=sys.stdout)
print("", file=sys.stderr)
sys.stdout.flush()
sentences = []

train_inputs = []
train_outputs = []
train_anonymise_inputs = []
train_anonymise_outputs = []

test_inputs = []
test_outputs = []
test_anonymise_inputs = []
test_anonymise_outputs = []

val_inputs = []
val_outputs = []
val_anonymise_inputs = []
val_anonymise_outputs = []

progress = tqdm(range(len(fichiers)))

eval_values = {}

def evalued_value(param):
    if param not in eval_values.keys():
        eval_values[param] = eval(param)
    return eval_values[param]


lemmes_e = {}
h_aspirés_ = (
"habanera", "hâb", "hâblerie", "hâbleu", "hach", "hachisch", "hachoir", "hachure", "hack", "hackeur_-euse",
"hacquebute", "haquebute", "hacquebutier", "haquebutier", "hada", "haddock", "hadîth", "hadj", "hadji", "HADOPI",
"haguais", "Haguai", "Hague", "hagard", "ha_ha", "haha", "hahé", "haie", "haïe", "haïk", "haillon", "haine", "haine",
"haineusement", "haïr", "haïssable", "halage", "halbran", "HALDE", "hâle", "halecret", "haler", "hâler", "haleter",
"halètement", "hall", "halle", "hallebarde", "hallebardier", "hallier", "hallstatien", "halo", "haloir", "hâloir",
"halophile", "halot", "halte", "halva", "hamac", "hamada", "hamal", "hammal", "Hambourg", "hamburger", "hameau",
"hammal", "hamal", "hammam", "Hammerfest", "hammerless", "hampe", "hamster", "han", "hanap", "hanche", "hanchement",
"hancher", "hand", "handball", "handball", "handballeu", "handicap", "hangar", "hanneton", "hanse", "hanséatique",
"hant", "happe", "happelourde", "happ", "happening", "happement", "happy-end", "happy_end", "haquebute", "hacquebute",
"haquebutier", "hacquebutier", "haquenée", "haquet", "hara-kiri", "harangue", "haranguer", "harangueu", "haras",
"harassant", "harasser", "harassement", "harc", "harcèlement", "harceleu", "hachich", "Harald", "hard", "hardes",
"hardi", "hardiesse", "hardiment", "hardware", "harem", "hareng", "harengère", "haret", "harfang", "hargne", "hargne",
"hargneusement", "haricot", "haricoter", "haridelle", "harissa", "harka", "harki", "harle", "harlou", "harnach",
"harnais", "harnois", "Harold", "haro", "harp", "Harry", "hart", "Harvard", "hasard", "hasarde", "has-been", "haschich",
"hase", "hast", "hastaire", "haste", "Hastings", "hât", "hauban", "haubergeon", "haubert", "hauss", "haut", "hautain",
"hautai", "hautbois", "haut-de-chausses", "haut-de-forme", "haute-contre", "haute-forme", "hautement", "hautesse",
"hauteur", "hautes-contre", "hautes-formes", "haut-fond", "hautin", "haut-le-cœur", "haut-le-corps", "haut-le-pied",
"haut-parleur", "haut-parleurs", "haut-relief", "hauts-de-chausses", "hauts-de-forme", "hauts-fonds", "hauts-reliefs",
"hauturi", "havage", "havanais", "Havanai", "havane", "hâve", "haveneau", "havenet", "hav", "haveu", "havir", "havrais",
"Havrai", "havre", "havresac", "havre-sac", "havresacs", "havre-sacs", "hayon", "hé", "heaume", "heaumier", "hé_bien",
"heimatlos", "hein", "hélas", "hél", "hèl", "hel", "hem", "hum", "hemloc", "hemlock", "henné", "hennir", "hennissant",
"hennissement", "hennisseu", "Henri", "henry", "Henry", "hep", "héraut", "herchage", "herschage", "herch", "hersch",
"herche", "herscheu", "hère", "hériss", "hermitique", "herniaire", "hernie", "hernie", "héron", "héronni", "héros",
"herschage", "herchage", "hersch", "herch", "hersche", "hercheu", "herse", "herser", "hertz", "hertzien", "Hesse",
"hêtraie", "hêtre", "heu", "heulandite", "heurt", "heurtement", "heurtequin", "heurte", "heurteu", "heurtoir", "hi",
"hiata", "hibou", "hic", "hic_et_nunc", "hickory", "hideur", "hideusement", "hide", "hie", "hiement", "hier",
"hiéracocéphale", "hiérarch", "hiératique", "hiératiquement", "hiératisant", "hiératisé", "hiératisme", "hiérochromie",
"hiérocrate", "hiérocratisme", "hiérodrame", "hiérogamie", "hiérogamique", "hiéroglyphe", "hiéroglyphé", "hiéroglyphie",
"hiéroglyphié", "hiéroglyphique", "hiéroglyphiquement", "hiéroglyphisme", "hiéroglyphite", "hiérogramme",
"hiérogrammate", "hiérogrammatisme", "hiérographe", "hiéromancie", "hiéromoine", "hiérophanie", "hiéroscopie",
"hiéroscopique", "hi-fi", "highlandai", "Highlander", "Highlands", "high-life", "highlifer", "highlifeur", "hi-han",
"hi_han", "hilaire", "hile", "hiloire", "Hilbert", "Hildegarde", "hindi", "hindî", "hip_hip_hip", "hip-hop", "hippie",
"hipp", "hiragana", "Hiroshima", "hiss", "hit", "hit-parade", "hit-parades", "hittite", "Hittite", "HLM", "Hobart",
"hobb", "hobereau", "hobereautaille", "hoberelle", "hoc", "hoca", "hocco", "hocko", "hoche", "hochement", "hochepot",
"hochequeue", "hoche-queue", "hochequeues", "hoche-queues", "hoch", "hochet", "hockey", "hockeyeu", "hocko", "hocco",
"hodja", "hoffmannesque", "hoffmannien", "hognement", "hogn", "holà", "holding", "hôl", "hold-up", "hollandais",
"Hollandai", "hollandaisement", "hollande", "Hollande", "hollandé", "hollandis", "hollando-belge", "hollando-françai",
"hollando-norvégien", "hollando-saxon", "Hollywood", "hollywoodesque", "hollywoodien", "homard", "homarderie",
"homardier", "home", "home-cinema", "homespun", "hon", "Honduras", "hondurien", "Hondurien", "Hongkong", "Hongkongai",
"hongre", "hongreline", "hongrer", "hongreu", "Hongrie", "hongrois", "Hongrois", "hongroyage", "hongroy", "honnir",
"honnissement", "Honshu", "honte", "honte", "honteusement", "hooligan", "hop", "hoquet", "hoquèt", "hoqueton", "horde",
"horion", "hormis", "hornblende", "hors", "horsain", "hors-bord", "hors-bords", "hors-caste", "hors-castes",
"hors-d’œuvre", "hors_d’œuvre", "horseguard", "horse-guard", "horseguards", "horse-guards", "horse-pox", "hors-jeu",
"hors-la-loi", "hors-série", "horst", "hors-texte", "hosanna", "hosannah", "hosannière", "hotdog", "hot-dog", "hotdogs",
"hot-dogs", "hotte", "hottée", "hott", "hottentot", "hotteu", "hou", "houblon", "houblonneu", "houblonnière", "houdan",
"Houdan", "houe", "houhou", "houill", "hourdi", "hourdis", "houret", "houri", "hourque", "hourra_ouhurra", "hourvari",
"houseau", "houspill", "houss", "Houston", "houx", "hoyau", "huard", "hublot", "huche", "huchée", "huche", "huchet",
"huchier", "hue", "huée", "huerta", "huehau", "Hugo", "hugolâtre", "hugolâtrie", "hugolien", "hugotique", "hugotisme",
"Hugues", "huguenot", "huitain", "huitaine", "huitante", "huitième", "Hulk", "hulotte", "hulul", "hum", "humage",
"humement", "hum", "Hun", "humot", "hune", "hunier", "hunter", "huppe", "huppé", "huque", "hure", "hurl",
"huro-iroquois", "Huro-iroquois", "huron", "Huron", "Huronien", "huronien", "hurricane", "husk", "hussard", "hussite",
"hussitisme", "hutin", "hutinet", "hutt")
h_aspirés = set()
for h in h_aspirés_:
    h_aspirés.add(h.lower())
liste_voyelles = ["a", "e", "i", "o", "u", "y"]


def elisionAvailable(lemme: str, obj = None) -> bool:
    if lemme == "":
        raise Exception
    global lemmes_e, h_aspirés
    if not lemme in lemmes_e.keys():
        lemmes_e[lemme] = False
        if not lemme[0].upper() in (
        "B", "C", "Ç", "D", "F", "G", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Z", "*", "*",
        "-"):

            # h_aspirés = ("habanera", "hâb", "hâblerie", "hâbleu", "hach", "hachisch", "hachoir", "hachure", "hack", "hackeur_-euse", "hacquebute", "haquebute", "hacquebutier", "haquebutier", "hada", "haddock", "hadîth", "hadj", "hadji", "HADOPI", "haguais", "Haguai", "Hague", "hagard", "ha_ha", "haha", "hahé", "haie", "haïe", "haïk", "haillon", "haine", "haine", "haineusement", "haïr", "haïssable", "halage", "halbran", "HALDE", "hâle", "halecret", "haler", "hâler", "haleter", "halètement", "hall", "halle", "hallebarde", "hallebardier", "hallier", "hallstatien", "halo", "haloir", "hâloir", "halophile", "halot", "halte", "halva", "hamac", "hamada", "hamal", "hammal", "Hambourg", "hamburger", "hameau", "hammal", "hamal", "hammam", "Hammerfest", "hammerless", "hampe", "hamster", "han", "hanap", "hanche", "hanchement", "hancher", "hand", "handball", "handball", "handballeu", "handicap", "hangar", "hanneton", "hanse", "hanséatique", "hant", "happe", "happelourde", "happ", "happening", "happement", "happy-end", "happy_end", "haquebute", "hacquebute", "haquebutier", "hacquebutier", "haquenée", "haquet", "hara-kiri", "harangue", "haranguer", "harangueu", "haras", "harassant", "harasser", "harassement", "harc", "harcèlement", "harceleu", "hachich", "Harald", "hard", "hardes", "hardi", "hardiesse", "hardiment", "hardware", "harem", "hareng", "harengère", "haret", "harfang", "hargne", "hargne", "hargneusement", "haricot", "haricoter", "haridelle", "harissa", "harka", "harki", "harle", "harlou", "harnach", "harnais", "harnois", "Harold", "haro", "harp", "Harry", "hart", "Harvard", "hasard", "hasarde", "has-been", "haschich", "hase", "hast", "hastaire", "haste", "Hastings", "hât", "hauban", "haubergeon", "haubert", "hauss", "haut", "hautain", "hautai", "hautbois", "haut-de-chausses", "haut-de-forme", "haute-contre", "haute-forme", "hautement", "hautesse", "hauteur", "hautes-contre", "hautes-formes", "haut-fond", "hautin", "haut-le-cœur", "haut-le-corps", "haut-le-pied", "haut-parleur", "haut-parleurs", "haut-relief", "hauts-de-chausses", "hauts-de-forme", "hauts-fonds", "hauts-reliefs", "hauturi", "havage", "havanais", "Havanai", "havane", "hâve", "haveneau", "havenet", "hav", "haveu", "havir", "havrais", "Havrai", "havre", "havresac", "havre-sac", "havresacs", "havre-sacs", "hayon", "hé", "heaume", "heaumier", "hé_bien", "heimatlos", "hein", "hélas", "hél", "hèl", "hel", "hem", "hum", "hemloc", "hemlock", "henné", "hennir", "hennissant", "hennissement", "hennisseu", "Henri", "henry", "Henry", "hep", "héraut", "herchage", "herschage", "herch", "hersch", "herche", "herscheu", "hère", "hériss", "hermitique", "herniaire", "hernie", "hernie", "héron", "héronni", "héros", "herschage", "herchage", "hersch", "herch", "hersche", "hercheu", "herse", "herser", "hertz", "hertzien", "Hesse", "hêtraie", "hêtre", "heu", "heulandite", "heurt", "heurtement", "heurtequin", "heurte", "heurteu", "heurtoir", "hi", "hiata", "hibou", "hic", "hic_et_nunc", "hickory", "hideur", "hideusement", "hide", "hie", "hiement", "hier", "hiéracocéphale", "hiérarch", "hiératique", "hiératiquement", "hiératisant", "hiératisé", "hiératisme", "hiérochromie", "hiérocrate", "hiérocratisme", "hiérodrame", "hiérogamie", "hiérogamique", "hiéroglyphe", "hiéroglyphé", "hiéroglyphie", "hiéroglyphié", "hiéroglyphique", "hiéroglyphiquement", "hiéroglyphisme", "hiéroglyphite", "hiérogramme", "hiérogrammate", "hiérogrammatisme", "hiérographe", "hiéromancie", "hiéromoine", "hiérophanie", "hiéroscopie", "hiéroscopique", "hi-fi", "highlandai", "Highlander", "Highlands", "high-life", "highlifer", "highlifeur", "hi-han", "hi_han", "hilaire", "hile", "hiloire", "Hilbert", "Hildegarde", "hindi", "hindî", "hip_hip_hip", "hip-hop", "hippie", "hipp", "hiragana", "Hiroshima", "hiss", "hit", "hit-parade", "hit-parades", "hittite", "Hittite", "HLM", "Hobart", "hobb", "hobereau", "hobereautaille", "hoberelle", "hoc", "hoca", "hocco", "hocko", "hoche", "hochement", "hochepot", "hochequeue", "hoche-queue", "hochequeues", "hoche-queues", "hoch", "hochet", "hockey", "hockeyeu", "hocko", "hocco", "hodja", "hoffmannesque", "hoffmannien", "hognement", "hogn", "holà", "holding", "hôl", "hold-up", "hollandais", "Hollandai", "hollandaisement", "hollande", "Hollande", "hollandé", "hollandis", "hollando-belge", "hollando-françai", "hollando-norvégien", "hollando-saxon", "Hollywood", "hollywoodesque", "hollywoodien", "homard", "homarderie", "homardier", "home", "home-cinema", "homespun", "hon", "Honduras", "hondurien", "Hondurien", "Hongkong", "Hongkongai", "hongre", "hongreline", "hongrer", "hongreu", "Hongrie", "hongrois", "Hongrois", "hongroyage", "hongroy", "honnir", "honnissement", "Honshu", "honte", "honte", "honteusement", "hooligan", "hop", "hoquet", "hoquèt", "hoqueton", "horde", "horion", "hormis", "hornblende", "hors", "horsain", "hors-bord", "hors-bords", "hors-caste", "hors-castes", "hors-d’œuvre", "hors_d’œuvre", "horseguard", "horse-guard", "horseguards", "horse-guards", "horse-pox", "hors-jeu", "hors-la-loi", "hors-série", "horst", "hors-texte", "hosanna", "hosannah", "hosannière", "hotdog", "hot-dog", "hotdogs", "hot-dogs", "hotte", "hottée", "hott", "hottentot", "hotteu", "hou", "houblon", "houblonneu", "houblonnière", "houdan", "Houdan", "houe", "houhou", "houill", "hourdi", "hourdis", "houret", "houri", "hourque", "hourra_ouhurra", "hourvari", "houseau", "houspill", "houss", "Houston", "houx", "hoyau", "huard", "hublot", "huche", "huchée", "huche", "huchet", "huchier", "hue", "huée", "huerta", "huehau", "Hugo", "hugolâtre", "hugolâtrie", "hugolien", "hugotique", "hugotisme", "Hugues", "huguenot", "huitain", "huitaine", "huitante", "huitième", "Hulk", "hulotte", "hulul", "hum", "humage", "humement", "hum", "Hun", "humot", "hune", "hunier", "hunter", "huppe", "huppé", "huque", "hure", "hurl", "huro-iroquois", "Huro-iroquois", "huron", "Huron", "Huronien", "huronien", "hurricane", "husk", "hussard", "hussite", "hussitisme", "hutin", "hutinet", "hutt")

            for h_aspiré in h_aspirés:
                if lemme.lower().startswith(h_aspiré):
                    return False
            if lemme[0].lower() == "h":
                lemmes_e[lemme] = True
            elif unidecode(lemme[0].lower()) in liste_voyelles:
                lemmes_e[lemme] = True
    return lemmes_e[lemme]


# print("# global.columns = ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC")
print("# global.columns = ID\tFORM\tLEMMA\tUPOS\tFEATS\tHEAD\tMISC")


# https://universaldependencies.org/u/pos/ : UPOS
# XPOS : specific tag

def saveGraph(graph, subdir, name):
    global graphs_dir
    path = os.path.join(graphs_dir, subdir, name + ".dot")
    lcoal_path = os.path.join(graphs_dir, subdir)
    if not os.path.exists(lcoal_path):
        os.makedirs(lcoal_path)
    write_dot(graph, path)

    subprocess.run("cd " + lcoal_path + " && dot -Tpng " + name + ".dot > " + name + ".png && rm " + name + ".dot",
                   shell=True, check=True)
    # subprocess.run ("cd " + lcoal_path + " && dot -Tpng " + name + ".dot > " + name + ".png", shell=True, check=True )


# return l'index d'un label donné
def getTGIndex(label_index: str, grew: [], talismane: []) -> int:
    if label_index.isdigit() and int(label_index) < len(grew) and grew[int(label_index) - 1][0] == label_index:
        return int(label_index) - 1
    for i in range(len(grew)):
        if grew[i][0] == label_index:
            return i
    for i in range(len(grew)):
        if talismane[i][0] == label_index:
            return i
    return -1


class UPOS(Enum):
    __metaclass__ = FlyWeightMeta
    adjective = 1  #: adjective
    adposition = 2  #: adposition
    adverb = 3  #: adverb
    auxiliary = 4  #: auxiliary
    coordinatingConjunction = 5  #:
    determiner = 6  #: determiner
    interjection = 7  # interjection
    noun = 8  # noun
    numeral = 9  # numeral
    particle = 10  # particle
    pronoun = 11  # pronoun
    properNoun = 12  #
    punctuation = 13  # punctuation
    subordinatingConjunction = 14  #
    symbol = 15  #
    verb = 16  #
    X = 17  # other

    def invariables():
        return [ UPOS.adposition,  UPOS.adverb, UPOS.coordinatingConjunction, UPOS.interjection, UPOS.particle, UPOS.punctuation, UPOS.subordinatingConjunction ]

    def __str__(self):
        if not hasattr(self, '_value'):
            if self == UPOS.coordinatingConjunction:
                self._value = "CCONJ"
            elif self == UPOS.interjection:
                self._value = "INTJ"
            elif self == UPOS.properNoun:
                self._value = "PROPN"
            elif self == UPOS.subordinatingConjunction:
                self._value = "SCONJ"
            elif self == UPOS.punctuation:
                self._value = "PUNCT"
            elif self == UPOS.verb:
                self._value = "VERB"
            elif self == UPOS.particle:
                self._value = "PART"
            elif self == UPOS.noun:
                self._value = "NOUN"
            elif self == UPOS.pronoun:
                self._value = "PRON"
            elif self == UPOS.X:
                self._value = "X"
            else:
                self._value = self.name[0:3].upper()
        return self._value


def getUPOS(analyse: []) -> UPOS:
    tag = analyse[4]
    if tag == "ADJ":
        return UPOS.adjective
    elif tag in ("PRO", "CLO", 'PROWH', 'ADJWH') or tag.startswith("CL"):
        return UPOS.pronoun
    elif tag == "NC":
        return UPOS.noun
    elif tag == "ADV":
        return UPOS.adverb
    elif tag == "NPP":
        return UPOS.properNoun
    elif tag[0] == "V":
        return UPOS.verb
    elif tag == "CC":
        return UPOS.coordinatingConjunction
    elif tag == "ET" and analyse[1][0].upper() == analyse[1][0]:
        return UPOS.properNoun
    else:
        return UPOS.X
        # raise Exception


class Contrainte(Enum):
    __metaclass__ = FlyWeightMeta
    NEUTRE = 0
    RELATIVE_SUJET = 1
    RELATIVE_OBJET = 2
    INFINITIF = 3
    VPP = 4
    VPA = 5
    COMPLEMENT = 6
    COMPARATIF = 7
    JUXTAPOSITION = 8

    def __str__(self):
        if not hasattr(self, '_value'):
            if self == Contrainte.RELATIVE_SUJET:
                self._value = "relativeSujet"
            elif self == Contrainte.RELATIVE_OBJET:
                self._value = "relative"
            elif self == Contrainte.INFINITIF:
                self._value = "infinitif"
            elif self == Contrainte.VPP:
                self._value = "vpp"
            elif self == Contrainte.VPA:
                self._value = "vpa"
            elif self == Contrainte.COMPLEMENT:
                self._value = "complément"
            elif self == Contrainte.COMPARATIF:
                self._value = "comparatif"
            elif self == Contrainte.JUXTAPOSITION:
                self._value = "juxtaposition"
            else:
                self._value = ""
        return self._value


class Features:
    __metaclass__ = FlyWeightMeta

    class Actualisation(Enum):
        __metaclass__ = FlyWeightMeta
        DEFINI = 1
        INDEFINI = 0
        INCONNU = -1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Actualisation.DEFINI:
                    self._value = "actualisation=Defini"
                elif self == Features.Actualisation.INDEFINI:
                    self._value = "actualisation=Indefini"
                else:
                    self._value = ""
            return self._value

    class Masse(Enum):
        __metaclass__ = FlyWeightMeta
        TOTALE = 1
        PARTIELLE = 0
        NULLE = -1
        INCONNUE = -2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Masse.TOTALE:
                    self._value = "masse=Totale"
                elif self == Features.Masse.PARTIELLE:
                    self._value = "masse=Partielle"
                elif self == Features.Masse.NULLE:
                    self._value = "masse=Nulle"
                else:
                    self._value = ""
            return self._value

    class Polarity(Enum):
        __metaclass__ = FlyWeightMeta
        NEG = -1
        NEUTRE = 0
        POS = 1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Polarity.NEG:
                    self._value = "polarity=Neg"
                elif self == Features.Polarity.POS:
                    self._value = "polarity=Pos"
                else:
                    self._value = ""
            return self._value

    class Gender(Enum):
        __metaclass__ = FlyWeightMeta
        COMMON = 0
        FEMININE = 1
        MASCULINE = 2
        NEUTER = 3
        INDEFINI = 4
        # il y a un genre, mais on ne sait lequel
        DEFINI = 5  # un déterminant est passé, on ne sait pas le genrer

        # ("la méthode l'" <- "l'" est déclaré det il ne faut pas qu'il écrase le déterminant passé avant

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Gender.COMMON:
                    self._value = "gender=Com"
                elif self == Features.Gender.FEMININE:
                    self._value = "gender=Fem"
                elif self == Features.Gender.MASCULINE:
                    self._value = "gender=Masc"
                elif self == Features.Gender.NEUTER:
                    self._value = "gender=Neut"
                elif self == Features.Gender.COMMON:
                    self._value = "gender=Com"
                else:
                    self._value = ""
            return self._value

    class Number(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = 0
        SINGULAR = 1
        PLURAL = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Number.SINGULAR:
                    self._value = "number=Sing"
                elif self == Features.Number.PLURAL:
                    self._value = "number=Plur"
                else:
                    self._value = ""
            return self._value

    class Mood(Enum):
        __metaclass__ = FlyWeightMeta
        INDICATIVE = 0
        SUBJUNCTIVE = 1
        CONDITIONAL = 2
        INDEFINI = -1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Mood.INDICATIVE:
                    self._value = "mood=Ind"
                elif self == Features.Mood.CONDITIONAL:
                    self._value = "mood=Cnd"
                elif self == Features.Mood.SUBJUNCTIVE:
                    self._value = "mood=Sub"
                else:
                    self._value = ""
            return self._value

    class Tense(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        PAST = 0
        PRESENT = 1
        FUTURE = 2
        IMPERFECT = 3
        PLUPERFECT = 4

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Tense.PAST:  # The past tense denotes actions that happened before a reference point
                    self._value = "tense=Past"
                elif self == Features.Tense.PRESENT:
                    self._value = "tense=Pres"
                elif self == Features.Tense.FUTURE:
                    self._value = "tense=Fut"
                elif self == Features.Tense.IMPERFECT:  # ne comporte pas de limitation temporelle, sauf à l’intervention d’une cause extérieure.
                    self._value = "tense=Imp"
                elif self == Features.Tense.PLUPERFECT:  # The pluperfect denotes action that happened before another action in past.
                    self._value = "tense=Pqp"
                else:
                    self._value = ""
            return self._value

    class Aspect(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        IMPERFECT = 0
        PERFECT = 1
        PROGRESSIVE = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Aspect.IMPERFECT:
                    self._value = "aspect=Imp"
                elif self == Features.Aspect.PERFECT:
                    self._value = "aspect=Perf"
                elif self == Features.Aspect.PROGRESSIVE:
                    self._value = "aspect=Prog"
                else:
                    self._value = ""
            return self._value

    class Voice(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ACTIVE = 0
        PASSIVE = 1

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Voice.ACTIVE:
                    self._value = "voice=Act"
                elif self == Features.Voice.PASSIVE:
                    self._value = "voice=Pass"
                else:
                    self._value = ""
            return self._value

    class VerbForm(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        FINITE = 0
        INFINITIVE = 1
        PARTICIPLE = 2

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.VerbForm.FINITE:
                    self._value = "verbForm=Fin"
                elif self == Features.VerbForm.INFINITIVE:
                    self._value = "verbForm=Inf"
                elif self == Features.VerbForm.PARTICIPLE:
                    self._value = "verbForm=Part"
                else:
                    self._value = ""
            return self._value

    class Person(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ZERO = 0
        FIRST = 1
        SECOND = 2
        THIRD = 3

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Person.ZERO:
                    self._value = "person=0"
                elif self == Features.Person.FIRST:
                    self._value = "person=1"
                elif self == Features.Person.SECOND:
                    self._value = "person=2"
                elif self == Features.Person.THIRD:
                    self._value = "person=3"
                else:
                    self._value = ""
            return self._value

    class Possession(Enum):
        __metaclass__ = FlyWeightMeta
        INDEFINI = -1
        ZERO = 0
        FIRST = 1
        SECOND = 2
        THIRD = 3

        def __str__(self):
            if not hasattr(self, '_value'):
                if self == Features.Possession.ZERO:
                    self._value = "possession=0"
                elif self == Features.Possession.FIRST:
                    self._value = "possession=1"
                elif self == Features.Possession.SECOND:
                    self._value = "possession=2"
                elif self == Features.Possession.THIRD:
                    self._value = "possession=3"
                else:
                    self._value = ""
            return self._value


_dic_gender = {}
try:
    if os.path.exists('dic_gender.json'):
        a = open("dic_gender.json", "r")
        dic_gender = json.load(a)
        a.close()
        for m1 in list(dic_gender.keys()):
            for m2 in list(dic_gender[m1].keys()):
                if dic_gender[m1][m2] == Features.Gender.FEMININE.value:
                    dic_gender[m1][m2] = Features.Gender.FEMININE
                elif dic_gender[m1][m2] == Features.Gender.MASCULINE.value:
                    dic_gender[m1][m2] = Features.Gender.MASCULINE
                elif dic_gender[m1][m2] == Features.Gender.DEFINI.value:
                    dic_gender[m1][m2] = Features.Gender.DEFINI
                elif dic_gender[m1][m2] == Features.Gender.COMMON.value:
                    dic_gender[m1][m2] = Features.Gender.COMMON
                elif dic_gender[m1][m2] == Features.Gender.NEUTER.value:
                    dic_gender[m1][m2] = Features.Gender.NEUTER
                else:
                    del dic_gender[m1][m2]
            if len(dic_gender[m1]) == 0:
                del dic_gender[m1]
    else:
        dic_gender = {}
except Exception:
    dic_gender = {}
    print("dic_gender en erreur", file=sys.stderr)
    pass

_dic_plural = {}
try:
    if os.path.exists('dic_plural.json'):
        a = open("dic_plural.json", "r")
        dic_plural = json.load(a)
        for m1 in list(dic_plural.keys()):
            for m2 in list(dic_plural[m1].keys()):
                if dic_plural[m1][m2] == Features.Number.PLURAL.value:
                    dic_plural[m1][m2] = Features.Number.PLURAL
                elif dic_plural[m1][m2] == Features.Number.SINGULAR.value:
                    dic_plural[m1][m2] = Features.Number.SINGULAR
                else:
                    del dic_plural[m1][m2]
            if len(dic_plural[m1]) == 0:
                del dic_plural[m1]

        a.close()
    else:
        dic_plural = {}
except Exception:
    dic_plural = {}
    print("dic_plural en erreur", file=sys.stderr)
    pass


def setGenderNumber(objCoNll, form, lemma):
    if objCoNll.universal_pos in (UPOS.verb, UPOS.adverb, UPOS.adjective, UPOS.subordinatingConjunction,
                                  UPOS.coordinatingConjunction, UPOS.interjection, UPOS.X):
        return
    form = form.lower()
    lemma = lemma.lower()
    if objCoNll.universal_pos == UPOS.pronoun:
        if form in ("je", "nous", "m'", "m'", "moi", "nôtre"):
            objCoNll.person = Features.Person.FIRST
        elif form in ("tu", "vous", "t'", "t'", "toi", "vôtre"):
            objCoNll.person = Features.Person.SECOND
        elif form in ('on', "il", "elle", 'on', "ils", 'elles'):
            objCoNll.person = Features.Person.THIRD
        if form in (
        "je", "tu", "il", "elle", "nous", "vous", 'on', "m'", "t'", "ils", 'elles', 'on', "m'", "t'", "toi", "moi",
        "vôtre"):
            objCoNll.gender = Features.Gender.DEFINI
        else:
            pass
        if form in ("nous", 'vous', "ils", "elles"):
            objCoNll.number = Features.Number.PLURAL
        elif form in ("je", "tu", "il", "elle", 'on'):
            objCoNll.number = Features.Number.SINGULAR
        if form in ('il', "ils",):
            objCoNll.gender = Features.Gender.MASCULINE
        elif form in ('elles', "elle"):
            objCoNll.gender = Features.Gender.FEMININE
        if objCoNll.gender in (Features.Gender.MASCULINE, Features.Gender.FEMININE) and objCoNll.number in (Features.Number.SINGULAR, Features.Number.PLURAL):
            return
    done1 = False
    done2 = False

    if form in dic_gender.keys():
        if lemma in dic_gender[form].keys():
            objCoNll.gender = dic_gender[form][lemma]
            done1 = True

    if form in dic_plural.keys():
        if lemma in dic_plural[form].keys():
            objCoNll.number = dic_plural[form][lemma]
            done2 = True
            if done1:
                return

    if not done1 and done2 and objCoNll.number in (Features.Number.PLURAL, Features.Number.INDEFINI):
        if form not in ('amours', "délices", "orgues", "pâques"):
            if lemma in dic_gender.keys():
                if lemma in dic_gender[lemma].keys():
                    objCoNll.gender = dic_gender[lemma][lemma]
                    return


class conllItem():
    global stock_analyses

    def __init__(self, initialID: str, form: str, lemma: str, partOfSpeechTAG: UPOS = UPOS.X, head="not head",
                 real_form=""):
        assert isinstance(head, conllItem) or head == "not head"
        assert isinstance(partOfSpeechTAG, UPOS)
        self._ID = initialID
        self._polarité = Features.Polarity.NEUTRE
        self._actualisation = Features.Actualisation.INCONNU
        self._masse = Features.Masse.INCONNUE
        self._gender = Features.Gender.INDEFINI
        self._UPOS = partOfSpeechTAG
        self._form = form
        self._contrainte = []
        self._adPosition = []
        self._lemma = lemma
        self._mood = Features.Mood.INDEFINI
        self._tense = Features.Tense.INDEFINI
        self._number = Features.Number.INDEFINI
        self._aspect = Features.Aspect.INDEFINI
        self._voice = Features.Voice.INDEFINI
        self._verbForm = Features.VerbForm.INDEFINI
        self._person = Features.Person.INDEFINI
        self._possession = Features.Possession.INDEFINI
        self._head = []
        self.temps = None
        self.mode_fr = None
        self.lvf = None

        self._anonymous_value = ""
        if head != "not head":
            self.head = head
        self._child = []
        self._child_contrainte = []
        self._child_adPosition = []
        if lemma != "" and not initialID.startswith("grp"):
            if real_form == "" or (not "event" in form and not "copule" in form):
                real_form = lemma[:]
            self._elision = "e_" if elisionAvailable(real_form) else ""
        else:
            self._elision = ""
        self._string_value = ''
        self._valeur_encodée = None
        self._shorted_valeur_encodée = None
        # if self._form.startswith('var_'):
        #    self.head = stock_analyses.getLast().getItem(self._form)
        self._misc = []
        self.closure = ""
        self.real_form = real_form

        self._counterIDe = ""
        self._counterIDm = ""
        self._counterIDs = ""

    def setclosure(self, closure):
        self.closure = closure

    def clean(self):
        self._misc = None
        self._child_adPosition = None
        self._child_contrainte = None
        self._child = None
        self._head = None
        self._contrainte = None
        self._adPosition = None

    @property
    def child_adPosition(self) -> []:
        return self._ID

    @property
    def ID(self) -> str:
        return self._ID

    @ID.setter
    def ID(self, ID: str):
        self._ID = ID
        
    @property
    def counterIDe(self) -> str:
        return self._counterIDe 

    @counterIDe.setter
    def counterIDe(self, IDe: str):
        self._valeur_encodée = None
        self._shorted_valeur_encodée = None
        self._counterIDe = IDe
        
    @property
    def counterIDm(self) -> str:
        return self._counterIDm 

    @counterIDm.setter
    def counterIDm(self, IDm: str):
        self._valeur_encodée = None
        self._shorted_valeur_encodée = None
        self._counterIDm = IDm
        
    @property
    def counterIDs(self) -> str:
        return self._counterIDs 

    @counterIDs.setter
    def counterIDs(self, IDs: str):
        self._valeur_encodée = None
        self._shorted_valeur_encodée = None
        self._counterIDs = IDs

    @property
    def anonymous_value(self) -> str:
        if args.reveal_adverbe and self.universal_pos == UPOS.adverb:
            return ""
        if args.reveal_adjectif and self.universal_pos == UPOS.adjective:
            return ""
        return self._anonymous_value

    @property
    def elision(self) -> str:
        return self._elision

    @anonymous_value.setter
    def anonymous_value(self, anonymous_value: str):
        global TRAIT_VIERGE
        self._string_value = ''
        if args.reveal_adverbe and self.universal_pos == UPOS.adverb:
            self._anonymous_value = ""
        elif args.reveal_adjectif and self.universal_pos == UPOS.adjective:
            self._anonymous_value = ""
        else:
            self._anonymous_value = anonymous_value
        self.misc = "ano=" + anonymous_value
        assert("￨" not in anonymous_value or TRAIT_VIERGE is None or anonymous_value.count("￨") ==  TRAIT_VIERGE.count("￨")+1)

    def floatID(self):
        value = re.findall("\d+\.*\d*", self._ID)
        value = float(value[-1]) if len(value) > 0 else 1000
        return value if self._ID.startswith("var") else value * 100 if self._ID.startswith("mod") else value * 1000

    @property
    def person(self) -> Features.Person:
        return self._person

    @person.setter
    def person(self, person: Features.Person):
        self._person = person

    @property
    def possession(self) -> Features.Possession:
        return self._possession

    @possession.setter
    def possession(self, possession: Features.Possession):
        self._possession = possession

    @property
    def adPosition(self) -> list:
        return self._adPosition

    @adPosition.setter
    def adPosition(self, adPosition: tuple):
        assert len(adPosition) == 2
        assert isinstance(adPosition[0], str)
        assert isinstance(adPosition[1], conllItem)
        if adPosition not in self._adPosition:
            self._adPosition.append(adPosition)
            adPosition[1]._child_adPosition.append((adPosition[0], self))
            self._string_value = ''

    @property
    def child_adPosition(self) -> []:
        to_op = []
        #     for i_t, tuple_contraint in enumerate(copy.deepcopy(self._child_adPosition)):
        for i_t, tuple_contraint in enumerate(self._child_adPosition):
            adPosition, parent = tuple_contraint
            ok = False
            for adPosition_2, enfant in parent.adPosition:
                if adPosition == adPosition_2 and enfant.__str__() == self.__str__():
                    ok = True
            if not ok:
                to_op.append(i_t)
        for i_t in sorted(to_op, reverse=True):
            self._child_adPosition.pop(i_t)
            self._string_value = ''
        return self._child_adPosition

    @property
    def form(self) -> str:
        return self._form

    @form.setter
    def form(self, form: str):
        # if self.form.startswith('var_'):
        #    self.head = stock_analyses.getLast().getItem(form)
        self._form = form

    @property
    def head(self):
        list_head = []
        list_id = []
        for head in self._head:
            if head.ID not in list_head:
                list_id.append(head.ID)
                list_head.append(head)
        return list_head

    @head.setter
    def head(self, head):
        assert isinstance(head, conllItem)
        to_del = []
        for head_ in self._head:
            if head.ID == head_.ID:
                to_del.append(head_)
        for head_ in to_del:
            self._head.remove(head_)
        if head not in self._head:
            self._head.append(head)
            head.child = self
            self._string_value = ''

    @property
    def child(self):
        tab = {}
        count = {}
        for child_ in self._child:
            value = child_.__str__()
            id = child_.ID
            if len(self._child)==1:
                return self._child
            if id not in tab.keys():
                tab[id]= child_
                count[id]= 0
            count[id] += 1
            if len(value) < len(tab[id].__str__())  :
                tab[id]=child_
        keys = filter(lambda x : count[x] > 1, tab.keys())
        for child_ in keys:
            self._child.remove(tab[child_])
        return self._child

    @property
    def sorted_child(self):
        children = []
        children_ID = []
        for value in ('copule', 'event', 'event&modifier', 'copule&modifier', "sujet", 'objet', 'compl-prep', 'circonstant'):
            for child in self._child:
                if child.lemma == value:
                    if child.ID not in children_ID:
                        children.append(child)
                        children_ID.append(child.ID)
        for child in self._child:
            if child.lemma != "modifier" and child not in children:
                if child.ID not in children_ID:
                    children.append(child)
                    children_ID.append(child.ID)
        for child in self._child:
            if child.lemma == "modifier" and child not in children:
                if child.ID not in children_ID:
                    children.append(child)
                    children_ID.append(child.ID)
        return children

    @child.setter
    def child(self, child):
        assert isinstance(child, conllItem)
        if child not in self._child:
            self._child.append(child)
            self._string_value = ''

    def deleteHead(self, head):
        assert isinstance(head, conllItem)
        if head in self._head:
            self._head.remove(head)
            head._child.remove(self)
            self._string_value = ''

    @property
    def contrainte(self) -> Contrainte:
        return self._contrainte

    @contrainte.setter
    def contrainte(self, contrainte: tuple):
        assert len(contrainte) == 2
        assert isinstance(contrainte[0], Contrainte)
        assert isinstance(contrainte[1], conllItem)
        if contrainte not in self._contrainte:
            self._contrainte.append(contrainte)
            contrainte[1]._child_contrainte.append((contrainte[0], self))
            self._string_value = ''

    @property
    def child_contrainte(self) -> []:
        to_op = []
        # for i_t, tuple_contraint in enumerate(copy.deepcopy(self._child_contrainte)):
        for i_t, tuple_contraint in enumerate(self._child_contrainte):
            contrainte, parent = tuple_contraint
            ok = False
            for contrainte_2, enfant in parent.contrainte:
                if contrainte == contrainte_2 and enfant.__str__() == self.__str__():
                    ok = True
            if not ok:
                to_op.append(i_t)
        for i_t in sorted(to_op, reverse=True):
            self._child_contrainte.pop(i_t)
            self._string_value = ''
        return self._child_contrainte

    @property
    def actualisation(self) -> Features.Actualisation:
        return self._actualisation

    @actualisation.setter
    def actualisation(self, actualisation: Features.Actualisation):
        self._actualisation = actualisation
        self._string_value = ''

    @property
    def masse(self) -> Features.Masse:
        return self._masse

    @masse.setter
    def masse(self, masse: Features.Masse):
        self._masse = masse
        self._string_value = ''

    @property
    def lemma(self) -> str:
        return self._lemma

    @lemma.setter
    def lemma(self, lemma: str):
        self._lemma = lemma
        self._string_value = ''

    @property
    def polarité(self) -> Features.Polarity:
        return self._polarité

    @polarité.setter
    def polarité(self, polarité: Features.Polarity):
        assert isinstance(polarité, Features.Polarity)
        self._string_value = ''
        self._polarité = polarité

    @property
    def mood(self) -> Features.Mood:
        return self._mood

    @mood.setter
    def mood(self, mood: Features.Mood):
        assert isinstance(mood, Features.Mood)
        self._mood = mood

    @property
    def tense(self) -> Features.Tense:
        return self._tense

    @tense.setter
    def tense(self, tense: Features.Tense):
        assert isinstance(tense, Features.Tense)
        self._string_value = ''
        self._tense = tense

    @property
    def aspect(self) -> Features.Aspect:
        return self._aspect

    @aspect.setter
    def aspect(self, aspect: Features.Aspect):
        assert isinstance(aspect, Features.Aspect)
        self._string_value = ''
        self._aspect = aspect

    @property
    def number(self) -> Features.Number:
        return self._number

    @number.setter
    def number(self, number: Features.Number):
        assert isinstance(number, Features.Number)
        self._string_value = ''
        self._number = number

    @property
    def gender(self) -> Features.Gender:
        return self._gender

    @gender.setter
    def gender(self, gender: Features.Gender):
        assert isinstance(gender, Features.Gender)
        self._string_value = ''
        self._gender = gender

    @property
    def voice(self) -> Features.Voice:
        return self._voice

    @voice.setter
    def voice(self, voice: Features.Voice):
        assert isinstance(voice, Features.Voice)
        self._string_value = ''
        self._voice = voice

    @property
    def verbForm(self) -> Features.VerbForm:
        return self._verbForm

    @verbForm.setter
    def verbForm(self, verbForm: Features.VerbForm):
        assert isinstance(verbForm, Features.VerbForm)
        self._string_value = ''
        self._verbForm = verbForm

    @property
    def universal_pos(self) -> UPOS:
        return self._UPOS

    @universal_pos.setter
    def universal_pos(self, partOfSpeechTAG: UPOS):
        assert isinstance(partOfSpeechTAG, UPOS)
        self._string_value = ''
        self._UPOS = partOfSpeechTAG

    @property
    def misc(self) -> []:
        return self._misc

    def valeur_encodée(self, shorted=False) -> str:
        global TRAIT_VIERGE
        global shorted_TRAIT_VIERGE
        if self._valeur_encodée is None:

            if self.universal_pos == UPOS.adjective and "modifier" in self.form:
                if self.real_form[-1] in ("s", "x"):
                    if self.number.__str__() == "":
                        self.number = Features.Number.PLURAL
                else:
                    if self.number.__str__() == "":
                        self.number = Features.Number.SINGULAR
                if self.gender.__str__() == "":
                    if len(self.lemma)+2 == len(self.real_form):
                        self.number = Features.Number.PLURAL
                        self.gender = Features.Gender.FEMININE
                    if len(self.lemma)+1 == len(self.real_form):
                        if self.real_form[-1] == "e":
                            self.gender = Features.Gender.FEMININE
                            self.number = Features.Number.SINGULAR
                        else:
                            self.gender = Features.Gender.MASCULINE
                            self.number = Features.Number.PLURAL

                for head in self.head:
                    if head.universal_pos in (UPOS.noun, UPOS.properNoun):
                        if head.number.__str__() == "" :
                            if self.number.__str__() != "":
                                head.number = self.number
                                head._valeur_encodée = None
                            elif head.form[-1] in ("s", "x"):
                                head.number = Features.Number.PLURAL
                                head._valeur_encodée = None
                    if self.gender.__str__() == "" and head.gender.__str__() != "":
                        self.gender = head.gender
                    elif self.gender.__str__() != "" and head.gender.__str__() == "":
                        head.gender = self.gender
                    if self.number.__str__() == "" and head.number.__str__() != "":
                        self.number = head.number
                    elif self.number.__str__() != "" and head.number.__str__() == "":
                        head.number = self.number
                        head._valeur_encodée = None
                if self.gender.__str__() == "" and self.form.startswith(self.lemma) and self.form.endswith("e") and not self.lemma.endswith("e"):
                    self.gender = Features.Gender.FEMININE
                    self.number = Features.Number.SINGULAR

                    for head in self.head:
                        if head.gender.__str__() == "" and  head.universal_pos in (UPOS.adjective, UPOS.noun) :
                            head.gender = Features.Gender.FEMININE
                            head._valeur_encodée = None
                            self.number = Features.Number.SINGULAR


            for _shorted in (True, False):
                if args.without_lvf:
                    code, sujet, objet, cp, circ = ("", "", "", "", "")
                else:
                    code, sujet, objet, cp, circ = (ZERO, ZERO, ZERO, ZERO, ZERO) if self.lvf is not None else (NA, NA, NA, NA, NA)
                    if self.lvf is not None:
                        code = self.lvf[0]
                        sujet = self.lvf[1]
                        objet = self.lvf[2] if code in ("T", "P") else ""
                        cp = self.lvf[3] if code in ("T", "P") else self.lvf[2]
                        circ = self.lvf[4] if len(self.lvf) == 5 else NA if code in ("T", "P") else self.lvf[3] if len(
                            self.lvf) == 4 else NA

                if self.temps is None and self.mode_fr is not None:
                    self.temps = "na"

                traits = []

                if _shorted:
                    features_ = [self.universal_pos.__str__(), self._gender.__str__(), self._number.__str__()]
                    if self.temps is not None:
                        features_v = [self.temps, self.mode_fr, self._person.__str__()]
                        if "" in features_[1:]:
                            sujet = ""
                            if self.voice == Features.Voice.PASSIVE:
                                for arg in self.child:
                                    if arg.form == "ARG_1":
                                        sujet = arg
                                        break
                            else:
                                for arg in self.child:
                                    if arg.form == "ARG_0":
                                        sujet = arg
                                        break
                            if sujet != "":
                                value = ".".split(sujet.ID)[-1]
                                for child in sujet.child:
                                    if child.ID == value or child.ID == "mod_" + value:
                                        if features_[-1] == "":
                                            self.number = child.number
                                        elif child.number.__str__() == "":
                                            child.number = self.number
                                            child._valeur_encodée = None
                                        if features_[-2] == "":
                                            self.gender = child.gender
                                        elif child.gender.__str__() == "":
                                            child.gender = self.gender
                                            child._valeur_encodée = None
                                    features_ = [self.universal_pos.__str__(), self._gender.__str__(), self._number.__str__()]
                                    break
                        traits = ["" if k == "" else k for k in features_ + features_v ] + [self.counterIDe, self.counterIDm, self.counterIDs]
                    else:
                        features_v = ["", "", ""]
                        traits = ["" if k == "" else k for k in features_ + features_v ]+ [self.counterIDe, self.counterIDm, self.counterIDs]

                    if self.universal_pos in UPOS.invariables():
                        traits = ["" for k in features_ + features_v]+ [self.counterIDe, self.counterIDm, self.counterIDs]
                else:

                    features_v = [self.temps, self.mode_fr, code, sujet, objet, cp, circ, self._voice.__str__()]
                    features_c = [self._gender.__str__(), self._number.__str__(), self._person.__str__(),
                                  self.actualisation.__str__(), self.masse.__str__(), self.possession.__str__()]
                    features_ = [self._polarité.__str__(), self.universal_pos.__str__(), self.closure if self.closure != "" else ZERO]

                    if self.counterIDs == "" and self.counterIDe == "" and self.universal_pos not in (UPOS.noun, UPOS.properNoun, UPOS.pronoun):
                        traits = ["e" if elisionAvailable(self.real_form) else NA] + [ZERO if k == "" else k for k in features_] +\
                                 ["" for _ in features_v ] +[ZERO for _ in features_c ]
                    elif  self.counterIDs == "" and self.temps is not None and self.universal_pos not in (UPOS.noun, UPOS.properNoun, UPOS.pronoun):
                        traits = ["e" + self.lemma[0] if self.lemma in ("être", "aller") else "e" if elisionAvailable(self.real_form) else NA] + \
                                 [ZERO if k == "" else k for k in features_] + ["na" if k == "" else k for k in features_v ] + [ZERO for _ in features_c ]
                    elif self.temps is not None:
                        traits = ["e" + self.lemma[0] if self.lemma in ("être", "aller") else "e" if elisionAvailable(self.real_form) else NA] + \
                                 [ZERO if k == "" else k for k in features_] + ["na" if k == "" else k for k in features_v] + [ZERO for _ in features_c ]
                    else:
                        traits = ["e" if elisionAvailable(self.real_form, self) else NA] + [ZERO if k == "" else k for k in features_] +\
                                 [ZERO for _ in features_v] + [NA if k == "" else k for k in features_c]
                    traits += [self.counterIDe, self.counterIDm, self.counterIDs]

                if args.with_traits:
                    val = "￨".join([k.strip().replace("|", "-") for k in traits])
                else:
                    val = "_".join([k.strip().replace("|", "-") for k in traits if k not in (NA, ZERO)])
                val = val.replace("_", "-")
                if TRAIT_VIERGE is None and not _shorted:
                    if args.with_traits:
                        TRAIT_VIERGE = "￨".join([NA for _ in traits])
                    else:
                        TRAIT_VIERGE = ""
                if _shorted and shorted_TRAIT_VIERGE is None:
                    if args.with_traits:
                        shorted_TRAIT_VIERGE = "￨".join([NA for _ in traits])
                    else:
                        shorted_TRAIT_VIERGE = ""

                if _shorted:
                    self._shorted_valeur_encodée =  val.replace("=",  ":").strip()
                else:
                    self._valeur_encodée = val.replace("=", ":").strip()
        if shorted:
            return self._shorted_valeur_encodée
        else:
            return self._valeur_encodée

    @misc.setter
    def misc(self, misc: str):
        assert isinstance(misc, str)
        self._string_value = ''
        misc = misc.strip().strip("&")
        misc_2 = misc.split("&")
        for misc in misc_2:
            if misc.replace(" ", "_").replace("_ou_", "|") not in self.misc and misc != "":
                self._misc.append(misc.replace(" ", "_").replace("_ou_", "|"))

    def __str__(self):
        if self._string_value == '':
            retour = []
            if self.universal_pos == UPOS.adjective:
                if not self.ID.startswith("mod_"):
                    self.ID = "mod_" + self.ID
                    self.form = "modifier"

            retour.append(self.ID)
            retour.append(self.form)
            retour.append(self.lemma)
            retour.append(self.universal_pos.__str__())
            #
            # if self.form == "modifier" or self.ID.startswith("var"):
            #     self.gender = Features.Gender.INDEFINI
            if self.ID.startswith("var") and '.' in self.ID:
                count = 0
                for child in self._child:
                    if not child.ID.isdigit():
                        child._person = self._person
                        count += 1
                self._person = Features.Person.INDEFINI
                if self._number != Features.Number.INDEFINI:
                    if self._number == Features.Number.SINGULAR or count == 1:
                        for child in self._child:
                            if not child.ID.isdigit():
                                child._number = self._number
                self._number = Features.Number.INDEFINI
                self._gender = Features.Gender.INDEFINI

            features = [self._polarité, self._gender, self._number, self._mood, self._tense, self._aspect,
                        self._voice, self._verbForm, self._person]
            retour.append("&".join([feature.__str__() for feature in features if feature.__str__() != ""]))

            heads = set()
            heads.update(self.head)
            for contrainte in self._child_contrainte:
                heads.add(contrainte[1])
            for contrainte in self._adPosition:
                heads.add(contrainte[1])

            retour.append("&".join([head.ID for head in heads] if len(heads) > 0 else ZERO))
            if self._actualisation != Features.Actualisation.INCONNU:
                self.misc = self._actualisation.__str__()
            if self._masse != Features.Masse.INCONNUE:
                self.misc = self._masse.__str__()
            if self._possession != Features.Possession.INDEFINI:
                self.misc = self._possession.__str__()
            for contrainte, head in self._contrainte:
                if contrainte != Contrainte.NEUTRE:
                    self.misc = contrainte.__str__() + "=" + head.ID.__str__()
            for contrainte, head in self._adPosition:
                self.misc = contrainte.__str__() + "=" + head.ID
            retour.append("&".join(self._misc))

            for i in range(len(retour)):
                if retour[i] == "":
                    retour[i] = "_"
            self._string_value = "\t".join(retour)
        return self._string_value


class analyses():

    def __init__(self):
        # la liste de toutes les analyses
        self._analyses = []
        self._to_del = []
        self._to_treat = []

    @property
    def size(self):
        return len(self._analyses)

    def getLast(self):
        return self._analyses[-1]

    def getID(self, analyseObject):
        return self._analyses.index(analyseObject)

    def append(self, analyseObject):
        self._analyses.append(analyseObject)

    def remove(self, analyseObject):
        if analyseObject in self._analyses:
            self._analyses.remove(analyseObject)

    def gestLoose(self, item):
        self._to_del.append(item)

    def delLoose(self):
        for item in self._to_del:
            item.destroy()

    @property
    def printer(self):
        global mega_print
        global i_write
        global take

        sep = ''

        to_del = []
        global max_input, max_output
        progress = tqdm(range(int(len(self._analyses) / (1000 if args.debug else 10000))))
        for i_analyse, référence_item in enumerate(self._analyses):
            if i_analyse % (1000 if args.debug else 10000) == 0:
                progress.update()
            chaine = ''
            if référence_item._flushed_value != "":
                yield référence_item._flushed_value.replace("  ", " ")
            elif référence_item._flushable:
                take = True

                heads = référence_item.getHeads()
                i_head = 0

                if len(heads) > 1:
                    for head in référence_item.getHeads():
                        if head in référence_item.getHeads() and not head.ID.startswith("gr"):
                            token = head.ID
                            if "." in token:
                                token = token[token.rindex(".") + 1:]
                            if "_" in token:
                                index = -1
                            else:
                                analyses = référence_item._json_data["infos_origines"]["analyses"]
                                grew = analyses["grew"]
                                talismane = analyses["talismane"]
                                index = getTGIndex(token, grew, talismane)
                                if index > -1:
                                    grew_local = grew[index]
                                    tali_local = talismane[index]
                                    index = getTGIndex(grew_local[6] if grew_local[6] != "_" else tali_local[6], grew,
                                                       talismane)
                                    if index > -1:
                                        grew_dep = grew[index]
                                        tali_dep = talismane[index]
                                        if grew_dep[4] == "CC" or tali_dep[4] == "CC":
                                            référent_coodination = conllItem(initialID="grp_" + grew_dep[0],
                                                                             form=grew_dep[1],
                                                                             lemma="coordination", real_form="k")
                                            index = getTGIndex(grew_dep[6] if grew_dep[6] != "_" else tali_dep[6], grew,
                                                               talismane)
                                            if référence_item.isInside("var_" + grew[index][0]):
                                                coord = référence_item.getItem("var_" + grew[index][0])
                                            elif référence_item.isInside(grew[index][0]):
                                                coord = référence_item.getItem(grew[index][0])
                                            else:
                                                take = False
                                                break

                                            référence_item.addItem(référent_coodination)
                                            référent_coodination.head = coord
                                            head.head = référent_coodination

                                        elif grew_dep[4].startswith("P") or tali_dep[4].startswith("P"):
                                            index = getTGIndex(grew_dep[6] if grew_dep[6] != "_" else tali_dep[6], grew,
                                                               talismane)
                                            axe = grew_dep[1]
                                            index_support = 0
                                            if référence_item.isInside("var_" + grew[index][0]):
                                                linked = référence_item.getItem("var_" + grew[index][0])
                                                head.adPosition = (axe, linked)
                                                index_support = -1
                                            elif référence_item.isInside(grew[index][0]):
                                                linked = référence_item.getItem(grew[index][0])
                                                head.adPosition = (axe, linked)
                                                index_support = -1
                                            elif grew[index][4] == "CC":
                                                index_support = getTGIndex(
                                                    grew[index][6] if grew[index][6] != "_" else talismane[index][6],
                                                    grew,
                                                    talismane)
                                                if index_support > -1:
                                                    coord = conllItem(
                                                        initialID="grp_" + grew[index][1] + "_" + grew[index][0],
                                                        form=grew[index][1],
                                                        lemma="coordination", real_form="k")
                                                    if référence_item.isInside(grew[index_support][0]):
                                                        linked = référence_item.getItem(grew[index_support][0])
                                                    elif référence_item.isInside("var_" + grew[index_support][0]):
                                                        linked = référence_item.getItem("var_" + grew[index_support][
                                                            0])  # si on ne trouve pas le lien, il y aura justapositon ultérieur

                                                    elif grew[index_support][4].startswith("P"):
                                                        while grew[index_support][4].startswith("P"):
                                                            index_support = getTGIndex(
                                                                grew[index_support][6] if grew[index_support][
                                                                                              6] != "_" else
                                                                talismane[index_support][6], grew,
                                                                talismane)
                                                            if index_support > -1:
                                                                if référence_item.isInside(
                                                                        "var_" + grew[index_support][0]):
                                                                    linked = référence_item.getItem(
                                                                        "var_" + grew[index_support][0])
                                                                elif référence_item.isInside(grew[index_support][0]):
                                                                    linked = référence_item.getItem(
                                                                        grew[index_support][0])
                                                            elif args.debug:
                                                                raise Exception
                                                            else:
                                                                take = False
                                                                break
                                                    # elif args.debug:
                                                    #    raise Exception
                                                    else:
                                                        take = False
                                                        break

                                                    head.head = coord
                                                    head.adPosition = (axe, coord)
                                                    coord.head = linked

                                                    référence_item.addItem(coord)

                                                    index_support = -1
                                                    # head = coord
                                                else:
                                                    take = False
                                                    break
                                            else:
                                                take = False
                                                break
                                            if index_support > -1 and take:
                                                event_by_event = linked.form in (
                                                'copule', 'event', 'event&modifier', 'copule&modifier') \
                                                                 and head.form in (
                                                                 'copule', 'event', 'event&modifier', 'copule&modifier')
                                                for tete_linked in linked.head if len(
                                                        linked.head) > 0 or event_by_event else [linked]:
                                                    head.head = tete_linked
                                        else:
                                            if référence_item.isInside(grew_dep[0]):
                                                coord = référence_item.getItem(grew_dep[0])
                                            elif référence_item.isInside("var_" + grew_dep[0]):
                                                coord = référence_item.getItem("var_" + grew_dep[0])
                                            else:
                                                take = False
                                                break
                                            if head.form == "modifier" or head.universal_pos in (
                                            UPOS.adjective, UPOS.adverb):
                                                head.head = coord
                                                head.form = "modifier"
                                            else:

                                                event_by_event = coord.form in (
                                                'copule', 'event', 'event&modifier', 'copule&modifier') \
                                                                 and head.form in (
                                                                 'copule', 'event', 'event&modifier', 'copule&modifier')

                                                if len(coord.head) == 0 and event_by_event:
                                                    juxtaposition = conllItem(
                                                        initialID="juxt_ego_" + i_head.__str__() + "_" + heads_key,
                                                        form="groupe",
                                                        lemma="juxtaposé", real_form="k")
                                                    i_head += 1
                                                    coord.head = juxtaposition
                                                    head.head = juxtaposition
                                                elif len(coord.head) == 0:
                                                    for tete_coord in coord.head:
                                                        head.head = tete_coord
                                                else:
                                                    head.head = coord

                                else:
                                    take = False
                                    break

                    heads = référence_item.getHeads()

                    if not take:
                        self.gestLoose(référence_item)
                        take = True
                        continue

                heads_nature = {}

                if len(heads) > 1:
                    for head in heads:
                        key = head.universal_pos.__str__()
                        if head.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                            key = "event"
                        elif head.ID.startswith("var"):
                            key = "objet"
                        else:
                            key = "upos" + key
                        if key == UPOS.X.__str__():
                            key = set()
                            for child in head.child:
                                key.add(child.universal_pos)
                            n = ""
                            for u in sorted(key):
                                n += u
                            key = UPOS.X.__str__() + u
                        if key not in heads_nature.keys():
                            heads_nature[key] = []
                        heads_nature[key].append(head)

                for heads_key in heads_nature.keys():
                    if len(heads_nature[heads_key]) > 1:

                        juxtaposition = None
                        for head in heads_nature[heads_key]:
                            if head.lemma == "coordination":
                                juxtaposition = head

                        if juxtaposition is None:
                            juxtaposition = conllItem(initialID="juxt_ego_" + i_head.__str__() + "_" + heads_key,
                                                      form="groupe",
                                                      lemma="juxtaposé", real_form="k")
                            i_head += 1
                            juxtaposition.universal_pos = UPOS.X
                            référence_item.addItem(conllObject=juxtaposition)

                        for head in heads_nature[heads_key]:
                            if head != juxtaposition:
                                head.head = juxtaposition

                heads = référence_item.getHeads()
                if len(heads) > 1:
                    juxtaposition = None

                    for head in heads:
                        if head.lemma == "juxtaposé":
                            juxtaposition = head

                    if juxtaposition is None:
                        for head in heads:
                            if head.form == "modifier":
                                for head_2 in heads:
                                    if head_2 != head:
                                        if head_2.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                            juxtaposition = head_2

                    if juxtaposition is None:
                        juxtaposition = conllItem(initialID="juxt_" + i_head.__str__(), form="groupe",
                                                  lemma="juxtaposé", real_form="k")
                        juxtaposition.universal_pos = UPOS.X
                        référence_item.addItem(conllObject=juxtaposition)
                        i_head += 1

                    if not juxtaposition is None:
                        for head_2 in heads:
                            if head_2 != juxtaposition:
                                head_2.head = juxtaposition

                heads = référence_item.getHeads()
                while len(référence_item.getHeads()) > 1:

                    juxtaposition = None
                    heads = référence_item.getHeads()

                    for head in heads:
                        if "groupe" in head.ID or head.form.startswith("groupe"):
                            juxtaposition = head

                    if juxtaposition is None:
                        if head.form.startswith("objet"):
                            juxtaposition = head

                    for head in heads:
                        if "event" in head.ID or head.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                            juxtaposition = head

                    if not juxtaposition is None:
                        for head in heads:
                            if head != juxtaposition:
                                juxtaposition.contrainte = (Contrainte.COMPLEMENT, head)
                                break

                    heads = référence_item.getHeads()

                for node in référence_item._items.keys():
                    node = référence_item._items[node]
                    genders = set()
                    child_ad_posés = set()
                    # for adposition, child_ad_posé in node.adPosition:
                    # child_ad_posés.add( child_ad_posé)
                    for child in node._child:
                        if child not in child_ad_posés:
                            genders.add(child.gender)

                    if "." in node.ID and not node.ID.startswith("var_"):
                        if not Features.Gender.INDEFINI in genders and len(genders) == 1 and node.gender != (
                        Features.Gender.INDEFINI):
                            node.gender = genders.pop()

                labeldict = {}
                colordict = {}
                formdict = {}
                penwidth = {}
                bgcolor = {}
                fontcolor = {}
                styledict = {}

                contraintes = set()
                a_sentence = sentence_time_analyse()

                to_treat = []
                to_treat_passive = []
                for node in référence_item._items.keys():
                    node = référence_item._items[node]
                    if node.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                        if node.voice == Features.Voice.PASSIVE:
                            to_treat_passive.append([node, {}])
                            for child in node.child:
                                if child.form in ("sujet", "objet", "compl-prep", "circonstant"):
                                    to_treat_passive[-1][1][child.form] = child
                            for adn, child in node._child_adPosition:
                                to_treat_passive[-1][1][adn] = child
                        else:
                            for child in [child for child in node.child if
                                          child.form in ("sujet", "objet", "compl-prep", "circonstant")]:
                                to_treat.append((node, child))

                for values in to_treat:
                    node, child = values
                    if child.form == "compl-prep":
                        child.form = "ARG_2"
                    elif child.form == "circonstant":
                        child.form = "ARG_3"
                    elif child.form == "sujet":
                        child.form = "ARG_0"
                        if node.number != Features.Number.INDEFINI:
                            child.number = node.number
                        if node.person != Features.Person.INDEFINI:
                            child.number = node.number
                    elif child.form == "objet":
                        child.form = "ARG_1"

                arg_1 = False
                for head, values in to_treat_passive:
                    for key in values.keys():
                        node = values[key]
                        if key == node.form and node.form == "sujet":
                            node.form = "ARG_1"
                            if head.number != Features.Number.INDEFINI:
                                node.number = head.number
                            if head.person != Features.Person.INDEFINI:
                                node.number = head.number
                            arg_1 = True
                        elif key == node.form and node.form == "compl-prep":
                            node.form = "ARG_2"
                            arg_2 = True
                        elif key == node.form and node.form == "circonstant":
                            node.form = "ARG_3"
                            arg_3 = True

                arg_0 = False
                for key_searched in ("par", "objet", "de"):
                    for head, values in to_treat_passive:
                        for key in values.keys():
                            node = values[key]
                            if key == key_searched:
                                if arg_0 and key_searched == "objet":
                                    node.form = "ARG_3"
                                elif arg_0:
                                    break
                                node.form = "ARG_0"
                                arg_0 = True

                                if key in ("par", "de"):
                                    étiquette = head.ID + "." + node.ID
                                    objItem = conllItem(initialID=étiquette, form="ARG_0", lemma="",
                                                        partOfSpeechTAG=UPOS.X, real_form="k")
                                    for each in list(node.head):
                                        objItem.head = each
                                        each.deleteHead(each)
                                    node.head = objItem
                                    i_del = []
                                    for i, ad in enumerate(node._adPosition):
                                        adPosition, node_ = ad
                                        if adPosition == key and node_ == head:
                                            i_del.append(i)
                                    for i in reversed(i_del):
                                        node._adPosition.pop(i)
                                    i_del = []
                                    for i, ad in enumerate(node._child_adPosition):
                                        ad = adPosition, node_
                                        if adPosition == key and node_ == node:
                                            i_del.append(i)
                                    for i in reversed(i_del):
                                        node._child_adPosition.pop(i)

                                    for each in list(node.child):
                                        each.head = objItem
                                        each.deleteHead(node)
                                    break

                G = nx.MultiDiGraph(data=None, label=référence_item._sent_id.replace("/",
                                                                                     "-") + ":" + référence_item._tokenized_text)

                generation_data = []

                ad_positions_treated = []

                for node in référence_item._items.keys():
                    node = référence_item._items[node]
                    if node.form.lower() in ('m\'', 't\'', 'j\''):
                        node.form = node.form[0] + "e"
                    if node.form.startswith("juxt_ego_"):
                        contraintes.add("juxtaposition")
                    if node.ID.startswith("grp_") and node.lemma == "liaison":
                        contraintes.add("liaison")
                    if node.polarité == Features.Polarity.NEG:
                        bgcolor[node.ID] = "black"
                        fontcolor[node.ID] = "white"
                        styledict[node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                  node.ID] + ",filled"
                        formdict["neg" + node.ID] = 'note'
                        labeldict["neg" + node.ID] = 'négatif'
                        contraintes.add("négation")
                        G.add_edge(node.ID, "neg" + node.ID, style='dashed')
                    if node.form not in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                        text = ""
                        sep = ""
                        for misc in node.misc:
                            for misc_2 in misc.split('&'):
                                if misc_2.startswith("as"):
                                    labeldict["as_" + node.ID] = " ou \n".join(misc_2.replace("as=", "").split("|"))
                                    formdict["as_" + node.ID] = 'note'
                                    fontcolor["as_" + node.ID] = "green4"
                                    bgcolor["as_" + node.ID] = "gray7"
                                    styledict[
                                        "as_" + node.ID] = 'filled' if "as_" + node.ID not in styledict.keys() else \
                                    styledict["as_" + node.ID] + ",filled"
                                    G.add_edge(node.ID, "as_" + node.ID, style='dashed', color='gold1')

                    if node.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                        a_sentence.addEvent(node)
                        text = ""
                        sep = ""
                        for misc in node.misc:
                            for misc_2 in misc.split('&'):
                                text += sep + misc_2
                                sep = "\n"
                        if node.mood.__str__() != "":
                            text += sep + node.mood.__str__()
                            if sep == "\n":
                                sep = ", "
                            if text.count(',') == 1:
                                sep = "\n"
                        if node.aspect.__str__() != "":
                            text += sep + node.aspect.__str__()
                            if sep == "\n":
                                sep = ", "
                            if text.count(',') == 1:
                                sep = "\n"
                        if node.voice.__str__() != "":
                            text += sep + node.voice.__str__()
                            if sep == "\n":
                                sep = ", "
                            if text.count(',') == 1:
                                sep = "\n"
                            if node.voice == Features.Voice.PASSIVE:
                                contraintes.add('passive')
                        if node.verbForm.__str__() != "":
                            text += sep + node.verbForm.__str__()
                            if sep == "\n":
                                sep = ", "
                            if text.count(',') == 1:
                                sep = "\n"
                        if node.person.__str__() != "":
                            text += sep + node.person.__str__()
                        if text != "":
                            fontcolor["specif" + node.ID] = "green4"
                            labeldict["specif" + node.ID] = text
                            formdict["specif" + node.ID] = 'note'
                            styledict["specif" + node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                                 node.ID] + ",filled"
                            # formdict["specif" + node.ID] ="none"
                            G.add_node("specif" + node.ID)
                            G.add_edge(node.ID, "specif" + node.ID, color='gold1', style='dashed')
                        formdict[node.ID] = "polygon"
                        if node.form in ('event&modifier', 'copule&modifier'):
                            styledict[node.ID] = 'rounded' if node.ID not in styledict.keys() else styledict[
                                                                                                       node.ID] + ",rounded"
                        if node.voice == Features.Voice.ACTIVE:
                            formdict[node.ID] = "invtrapezium"
                        elif node.voice == Features.Voice.PASSIVE:
                            formdict[node.ID] = "trapezium"
                    elif node.form in ('modifier'):
                        formdict[node.ID] = "diamond"
                    else:
                        texte_node = ""
                        if node.actualisation != Features.Actualisation.INCONNU:
                            formdict["determination_" + node.ID] = 'note'
                            texte_node = node.actualisation.__str__()
                            if node.possession != Features.Possession.INDEFINI:
                                texte_node += "\n" + node.possession.__str__()
                        if node.masse != Features.Masse.INCONNUE:
                            formdict["determination_" + node.ID] = 'note'
                            if texte_node != "":
                                texte_node += "\n"
                            texte_node += node.masse.__str__()
                        if node.person.__str__() != '':
                            formdict["determination_" + node.ID] = 'note'
                            if texte_node != "":
                                texte_node += "\n"
                            texte_node += node.person.__str__()
                        if "determination_" + node.ID in formdict.keys():
                            labeldict["determination_" + node.ID] = texte_node
                            G.add_edge(node.ID, "determination_" + node.ID, style='dashed')
                    if node.number == Features.Number.PLURAL:
                        penwidth[node.ID] = 2
                    if node.gender == Features.Gender.MASCULINE:
                        colordict[node.ID] = "skyblue"
                        if node.ID in penwidth.keys():
                            colordict[node.ID] = "royalblue"
                        styledict[node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                  node.ID] + ",filled"
                    elif node.gender == Features.Gender.FEMININE:
                        colordict[node.ID] = "pink"
                        if node.ID in penwidth.keys():
                            colordict[node.ID] = "deeppink"
                        styledict[node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                  node.ID] + ",filled"
                    elif node.gender == Features.Gender.DEFINI:  # il y a un genre, mais on ne sait lequel
                        colordict[node.ID] = "gray"
                        styledict[node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                  node.ID] + ",filled"
                    if node.polarité == Features.Polarity.NEG:
                        bgcolor[node.ID] = "azure3"
                        styledict[node.ID] = 'filled' if node.ID not in styledict.keys() else styledict[
                                                                                                  node.ID] + ",filled"
                    G.add_node(node.ID)
                    label = ""
                    adpositions = {}
                    for adposition, head in node.adPosition:
                        data_adposée = head.ID + node.ID + adposition
                        if data_adposée not in ad_positions_treated:
                            adpositions[head.ID] = adposition
                            G.add_edge(head.ID, node.ID, label=adposition, color='blue')
                            ad_positions_treated.append(data_adposée)

                            # if not args.anonymise and not args.anonymise_accord:
                            #     texte_head = head.form.lower() if head.form not in ('copule', 'event', 'event&modifier', 'copule&modifier', 'modifier') else head.lemma.lower()
                            #    texte_node = node.form.lower() if node.form not in ('copule', 'event', 'event&modifier', 'copule&modifier', 'modifier') else node.lemma.lower()
                            # else:
                            if head.anonymous_value != "":
                                texte_head = head.anonymous_value.lower()
                            else:
                                texte_head = head.form.lower() if head.form not in (
                                'copule', 'event', 'event&modifier', 'copule&modifier',
                                'modifier') else head.lemma.lower()
                            if node.anonymous_value != "":
                                texte_node = node.anonymous_value.lower()
                            else:
                                texte_node = node.form.lower() if node.form not in (
                                'copule', 'event', 'event&modifier', 'copule&modifier',
                                'modifier') else node.lemma.lower()
                            generation_data.append("( " + texte_head + " " + adposition + " " + texte_node + " )")

                    if len(node.head) > 0:
                        for head in node.head:
                            if head.ID not in adpositions.keys():
                                G.add_edge(head.ID, node.ID)
                    labeldict[node.ID] = node.form + "\n" + node.lemma

                    if node.lemma == "coordination":
                        contraintes.add('coordination')
                    for contrainte, tête in node.contrainte:
                        G.add_edge(node.ID, tête.ID, label=contrainte.__str__(), style='dashed', color='red')
                        contraintes.add(contrainte.__str__())

                if référence_item._tokenized_text[-1] == "?":
                    label = "question"
                elif référence_item._tokenized_text[-1] == "!":
                    label = "exclamation"
                elif référence_item._tokenized_text[-1] == ".":
                    label = "affirmation"
                else:
                    raise Exception
                if label != "affirmation":
                    contraintes.add(label)
                    put = ""
                    for graph_node in G.nodes:
                        predec = False
                        for _ in G.predecessors(graph_node):
                            predec = True
                            break
                        if not predec:
                            G.add_edge(graph_node, référence_item._tokenized_text[-1], label=label.__str__(),
                                       style='dashed', color='red')
                            put = graph_node
                            break
                    if put in référence_item._items:
                        référence_item._items[put].setclosure(label)

                nx.set_node_attributes(G, labeldict, 'label')
                nx.set_node_attributes(G, colordict, 'color')
                nx.set_node_attributes(G, formdict, 'shape')
                nx.set_node_attributes(G, penwidth, 'penwidth')
                nx.set_node_attributes(G, penwidth, 'lwidth')
                nx.set_node_attributes(G, bgcolor, 'bgcolor')
                nx.set_node_attributes(G, styledict, 'style')
                nx.set_node_attributes(G, fontcolor, 'fontcolor')

                d = list(nx.connected_component_subgraphs(G.to_undirected()))

                for contrainte in contraintes:
                    if contrainte not in stats.keys():
                        stats[contrainte] = 0
                    stats[contrainte] += 1
                if len(contraintes) == 0:
                    stats['aucune contrainte'] += 1
                    contraintes.add("aucune")
                if a_sentence.moods not in stats["détails"]['moods'].keys():
                    stats["détails"]['moods'][a_sentence.moods] = 0
                stats["détails"]['moods'][a_sentence.moods] += 1
                if a_sentence.verbsForm not in stats["détails"]['verbsForm'].keys():
                    stats["détails"]['verbsForm'][a_sentence.verbsForm] = 0
                stats["détails"]['verbsForm'][a_sentence.verbsForm] += 1

                if len(d) > 1:
                    self.gestLoose(référence_item)
                elif len(d) == 1:

                    # def getValue(objet, features_treated, coupled=[], old_text_objet=""):
                    #     retour = ""
                    #     modifiers = []
                    #     if isinstance(objet, conllItem):
                    #         if objet.anonymous_value != "" or objet.ID in anonyme_names.keys():  # (args.anonymise or  args.anonymise_accord) and
                    #             texte_objet = objet.anonymous_value.lower() if objet.anonymous_value != "" else \
                    #             anonyme_names[objet.ID].lower()
                    #         else:
                    #             texte_objet = objet.form.lower() if objet.form not in ('copule', 'event', 'event&modifier', 'copule&modifier', 'modifier')  \
                    #                                                 and not (objet.universal_pos==UPOS.adjective and args.reveal_adjectif) else objet.lemma.lower()
                    #
                    #         sep = " "
                    #         for child in [k for k in objet.sorted_child if not (k.lemma == "coordination"or k.ID.startswith("jux")) or k.form == 'modifier']:
                    #             if child.anonymous_value != "" or child.ID in anonyme_names.keys():  # (args.anonymise or  args.anonymise_accord) and
                    #                 texte_child = child.anonymous_value.lower() if child.anonymous_value.lower() != "" else \
                    #                 anonyme_names[child.ID].lower()
                    #             else:
                    #                 texte_child = child.form.lower() if child.form not in ('copule', 'event', 'event&modifier', 'copule&modifier', 'modifier')  and not (child.universal_pos==UPOS.adjective and args.reveal_adjectif) else child.lemma.lower()
                    #
                    #
                    #             if child.universal_pos == UPOS.adjective and "modifier" in child.form:
                    #                 if child.gender.__str__() == "":
                    #                     child.gender = objet.gender
                    #                 if child.number.__str__() == "":
                    #                     child.number = objet.number
                    #
                    #             if child.form == "modifier" and not "modifier"  in objet.form and not "ARG" in child.form:
                    #                 modifiers.append(texte_child + ' ( ' + texte_objet + " ) ")
                    #             else:
                    #
                    #                 for child_2 in [k for k in child.sorted_child if not (k.lemma == "coordination"or k.ID.startswith("jux"))]:
                    #                     if child_2.anonymous_value != "" or child_2.ID in anonyme_names.keys():  # (args.anonymise or  args.anonymise_accord) and
                    #                         texte_child2 = child_2.anonymous_value.lower() if child_2.anonymous_value.lower() != "" else anonyme_names[child_2.ID].lower()
                    #                     else:
                    #                         texte_child2 = child_2.form.lower() if child_2.form not in ('copule', 'event', 'event&modifier', 'copule&modifier', 'modifier')  and not (child_2.universal_pos==UPOS.adjective and args.reveal_adjectif) else child_2.lemma.lower()
                    #                     retour += " ( " + texte_objet + " " + texte_child + " " + texte_child2 +  " ) "
                    #
                    #
                    #         modifiers.append(retour)
                    #         random.shuffle(modifiers)
                    #         return (" ".join(modifiers)).replace("  ", " ").replace('( )', " ").replace("  ",
                    #                                                                                     " ").strip()

                    data = référence_item.__str__()
                    take = len(référence_item._tokenized_text.split(" ")) == len(
                        référence_item._json_data['infos_origines']['analyses']["grew"])

                    if take:
                        infos_anonymisation = {}

                        infos_anonymisation_simplified = {}

                        if take:
                            new_line = référence_item._tokenized_text.split(" ")
                            lines = data.strip().split("\n")
                            sentence = lines[6]
                            lines = lines[7:]
                            i_count_event = 1
                            events = []
                            i_count_modifier = 1

                            modifiers = []
                            i_count_other = 1
                            others = []

                            numeraux_id = {'event':[], 'modifier':[], 'simple':[]}
                            for line in lines:
                                if line.startswith("#"):
                                    continue
                                ligne = line.strip()
                                line = ligne.split("\t")
                                if "event" in line[1] or "copule" in line[1]:
                                    events.append(i_count_event.__str__())
                                    i_count_event += 1
                                    numeraux_id['event'].append(len(numeraux_id['event']))
                                elif ("modifier" in line[1] or line[0].startswith("mod")) and (not args.reveal_adverbe or "ADV" not in ligne) and (not args.reveal_adjectif or "ADJ" not in ligne):
                                    modifiers.append((i_count_modifier * 100).__str__())
                                    i_count_modifier += 1
                                elif line[0].isdigit() and (not args.reveal_adverbe or "ADV" not in ligne):
                                    if (not args.reveal_adjectif or "ADJ" not in ligne) or "modifier" in ligne:
                                        others.append(i_count_other.__str__())
                                        i_count_other += 1
                                if "modifier" in line[1]:
                                    numeraux_id['modifier'].append(len(numeraux_id['modifier']))
                                elif line[0].isdigit():
                                    numeraux_id['simple'].append(len(numeraux_id['simple']))


                                random.shuffle(numeraux_id['simple'])
                                random.shuffle(numeraux_id['modifier'])
                                random.shuffle(numeraux_id['event'])
                                random.shuffle(events)
                                random.shuffle(modifiers)
                                random.shuffle(others)
                                grew = référence_item._json_data["infos_origines"]["analyses"]['grew']
                                talismane = référence_item._json_data["infos_origines"]["analyses"]['talismane']
                                infos_anonymisation = {}

                                infos_anonymisation_simplified = {}

                            if args.max_valued_tokens > 0:
                                if len(events) + len(modifiers) + len(others) > args.max_valued_tokens:
                                    continue
                            anonyme_names = {}


                            for line in lines:
                                if line.startswith("#"):
                                    continue
                                line = line.strip().split("\t")
                                anonyme_name = ''
                                if "event" in line[1] or "copule" in line[1]:

                                    if not (args.anonymise or args.anonymise_accord):
                                        anonyme_name = "$$$$$$$$"
                                    if référence_item.isInside(line[0]):
                                        node_event = référence_item.getItem(line[0])
                                    else:
                                        if not référence_item.isInside(line[0].replace("mod_", "")):
                                            found = False
                                            for node_testé in référence_item._items:
                                                if référence_item._items[node_testé].ID == line[0]:
                                                    node_event = référence_item._items[
                                                        node_testé]  # bon, je suis fatiguée, il est tard
                                                    node_event.ID = line[0]
                                                    found = True
                                                    break
                                            if found:
                                                référence_item._items[node_event.ID] = référence_item._items[node_testé]
                                                del référence_item._items[node_testé]
                                        else:
                                            node_event = référence_item.getItem(line[0].replace("mod_", ""))
                                            node_event.ID = line[0]

                                    anonyme_name = "EVENT_" + events.pop()
                                    if "event" in line[1] or "copule" in line[1]:
                                        node_event.counterIDe = "e" + numeraux_id['event'].pop().__str__()
                                    if "modifier" in line[1]:
                                        node_event.counterIDm = "m" + numeraux_id['modifier'].pop().__str__()
                                    elif line[0].isdigit():
                                        node_event.counterIDs = "s" + numeraux_id['simple'].pop().__str__()
                                elif "modifier" in line[1] or line[0].startswith("mod")  :

                                    if not (args.anonymise or args.anonymise_accord):
                                        anonyme_name = "$$$$$$$$"
                                    if référence_item.isInside(line[0]):
                                        node_event = référence_item.getItem(line[0])
                                    else:
                                        if not référence_item.isInside(line[0].replace("mod_", "")):
                                            found = False
                                            for node_testé in référence_item._items:
                                                if référence_item._items[node_testé].ID == line[0]:
                                                    node_event = référence_item._items[
                                                        node_testé]  # bon, je suis fatiguée, il est tard
                                                    node_event.ID = line[0]
                                                    found = True
                                                    break
                                            if found:
                                                référence_item._items[node_event.ID] = référence_item._items[node_testé]
                                                del référence_item._items[node_testé]
                                        else:
                                            node_event = référence_item.getItem(line[0].replace("mod_", ""))
                                            node_event.ID = line[0]
                                    if "event" in line[1] or "copule" in line[1]:
                                        node_event.counterIDe = "e" + numeraux_id['event'].pop().__str__()
                                    if "modifier" in line[1]:
                                        node_event.counterIDm = "m" + numeraux_id['modifier'].pop().__str__()
                                    elif line[0].isdigit():
                                        node_event.counterIDs = "s" + numeraux_id['simple'].pop().__str__()

                                    if ((not args.reveal_adverbe) or (
                                            args.reveal_adverbe and node_event.universal_pos != UPOS.adverb)):
                                        if ((not args.reveal_adjectif) or (
                                                args.reveal_adjectif and node_event.universal_pos != UPOS.adjective)):
                                            anonyme_name = "MOD_" + modifiers.pop()
                                        elif  not args.reveal_adjectif or (args.reveal_adjectif and node_event.universal_pos != UPOS.adjective):
                                            anonyme_name = ""
                                    elif  not args.reveal_adjectif or (args.reveal_adjectif and node_event.universal_pos != UPOS.adjective):
                                        anonyme_name = ""
                                    if args.reveal_adjectif and node_event.universal_pos == UPOS.adjective:
                                        anonyme_name = node_event.lemma
                                    if args.reveal_adverbe and node_event.universal_pos == UPOS.adverb:
                                        anonyme_name = node_event.lemma
                                elif line[0].isdigit():
                                    if not (args.anonymise or args.anonymise_accord):
                                        anonyme_name = "$$$$$$$$"

                                    node_event = référence_item.getItem(line[0])
                                    if (not args.reveal_adverbe) or (
                                            args.reveal_adverbe and node_event.universal_pos != UPOS.adverb):

                                        anonyme_name = "ARGEVENT_" + others.pop()

                                    else:
                                        anonyme_name = ""

                                    if "event" in line[1] or "copule" in line[1]:
                                        node_event.counterIDe = "e" + numeraux_id['event'].pop().__str__()
                                    if "modifier" in line[1]:
                                        node_event.counterIDm = "m" + numeraux_id['modifier'].pop().__str__()
                                    elif line[0].isdigit():
                                        node_event.counterIDs = "s" + numeraux_id['simple'].pop().__str__()
                                        
                                if anonyme_name != '':
                                    if (args.anonymise or args.anonymise_accord):
                                        if args.with_traits:
                                            anonyme_name = anonyme_name + "￨" + node_event.valeur_encodée()
                                        else:
                                            anonyme_name = node_event.valeur_encodée() + "_" + anonyme_name
                                    else:
                                        anonyme_name = node_event.lemma + "￨" +  node_event.valeur_encodée()

                                    if "_" in node_event.ID and not "." in node_event.ID:
                                        index = getTGIndex(node_event.ID[node_event.ID.rindex("_") + 1:], grew,
                                                           talismane)
                                    elif "." in node_event.ID:
                                        index = getTGIndex(node_event.ID[node_event.ID.rindex(".") + 1:], grew,
                                                           talismane)
                                    elif node_event.ID.isdigit():
                                        index = getTGIndex(node_event.ID, grew, talismane)
                                    else:
                                        raise Exception

                                    anonyme_name = anonyme_name.lower()
                                    node_event.anonymous_value = anonyme_name
                                    infos_anonymisation[anonyme_name] = new_line[index]
                                    simplified_value = ""
                                    if anonyme_name != "":
                                        anonyme_names[node_event.ID] = anonyme_name
                                        simplified_value = anonyme_name[:anonyme_name.index("￨")]
                                        simplified_value += "￨" + node_event.valeur_encodée(shorted=True)

                                        infos_anonymisation_simplified[simplified_value] = new_line[index]  # TODO tester
                                        new_line[index] = simplified_value if simplified_value != "" else new_line[index].replace("_", " ")
                                    else:
                                        new_line[index] = anonyme_name if anonyme_name != "" else new_line[index].replace("_", " ")
                                        # if ("event" in line[1] or "copule" in line[1]) and  args.elision:
                                        #       simplified_value =  node_event.elision + anonyme_name


                            référence_item.setAnonymized(" ".join(new_line).strip())

                            generation_data = []
                            ad_positions_treated = []
                            adpositions = {}
                            for node in référence_item._items.keys():
                                node = référence_item._items[node]
                                for adposition, head in node.adPosition:
                                    data_adposée = head.ID + node.ID + adposition
                                    if data_adposée not in ad_positions_treated:
                                        adpositions[head.ID] = adposition
                                        # G.add_edge(head.ID, node.ID, label=adposition, color='blue')
                                        ad_positions_treated.append(data_adposée)

                                        if head.anonymous_value != "":
                                            texte_head = head.anonymous_value
                                        else:
                                            texte_head = head.form.lower() if head.form not in (
                                            'copule', 'event', 'event&modifier', 'copule&modifier',
                                            'modifier') else head.lemma.lower()

                                        if node.anonymous_value != "":
                                            texte_node = node.anonymous_value
                                        else:
                                            texte_node = node.form.lower() if node.form not in (
                                            'copule', 'event', 'event&modifier', 'copule&modifier',
                                            'modifier') else node.lemma.lower()
                                        generation_data.append("( " + texte_head + " " + adposition + " " + texte_node + " )")
                            data = référence_item.__str__()

                    sep = "\n"

                    i_write += 1

                    if take and args.without_liaison and "liaison" in contraintes:
                        take = False

                    if take and ((mega_print and i_write % 5000 == 0) or (args.debug and i_write % 500 == 0)):
                        for sous_dossier in contraintes:
                            # sous_dossier = référence_item._json_data['fichier_corpus'][référence_item._json_data['fichier_corpus'].rindex("/")+1:référence_item._json_data['fichier_corpus'].rindex(".")]
                            try:
                                saveGraph(G, sous_dossier, référence_item._sent_id.replace("/", "-"))

                                json_data = copy.deepcopy(référence_item._json_data)
                                del json_data['infos_origines']['analyses']
                                fjson = open(os.path.join(graphs_dir, sous_dossier,
                                                          référence_item._sent_id.replace("/", "-") + ".json"), "w")
                                fjson.write(json.dumps(json_data, indent=2, ensure_ascii=False))
                                fjson.flush()
                                fjson.close()
                                while not fjson.closed:
                                    True
                            except:
                                pass
                            try:
                                f = open(os.path.join(graphs_dir, sous_dossier,
                                                      référence_item._sent_id.replace("/", "-") + ".txt"), "w")
                                f.write(data.strip())
                                f.flush()
                                f.close()
                                while not f.closed:
                                    True
                            except:
                                pass

                    if take:

                        if args.without_contraintes:
                            contraintes = ["nothing"]
                        else:
                            pack_of_contraintes = []
                            type_phrase = référence_item._text.strip()[-1]
                            if type_phrase == ".":
                                pack_of_contraintes.append("affirmation")
                            elif type_phrase == "!":
                                pack_of_contraintes.append("exclamation")
                            elif type_phrase == "?":
                                pack_of_contraintes.append("question")
                            else:
                                pack_of_contraintes.append("non_determiné")
                            for node__ in référence_item._items:
                                node_ = référence_item.getItem(node__)
                                if (args.anonymise or args.anonymise_accord) and node_.anonymous_value != "":
                                    self_texte = node_.anonymous_value
                                else:
                                    self_texte = node_.form.lower() if node_.form not in (
                                        'copule', 'event', 'event&modifier', 'copule&modifier',
                                        'modifier') else node_.lemma.lower()

                                if node_.voice == Features.Voice.PASSIVE:
                                    for child_ in node_.child:
                                        if child_.form == "ARG_0":
                                            pack_of_contraintes.append(" ".join([self_texte, "passive"]))
                                            break
                                for contrainte_, parent_ in node_._contrainte:
                                    if (args.anonymise or args.anonymise_accord) and parent_.anonymous_value != "":
                                        parent_texte = parent_.anonymous_value
                                    else:
                                        parent_texte = parent_.form.lower() if parent_.form not in (
                                            'copule', 'event', 'event&modifier', 'copule&modifier',
                                            'modifier') else parent_.lemma.lower()
                                    if parent_texte.startswith("arg_"):
                                        parent_texte += " ("
                                        for head_parent in parent_.head:
                                            if (
                                                    args.anonymise or args.anonymise_accord) and head_parent.anonymous_value != "":
                                                parent_texte += " " + head_parent.anonymous_value
                                            else:
                                                parent_texte += " " + head_parent.form.lower() if head_parent.form not in (
                                                    'copule', 'event', 'event&modifier', 'copule&modifier',
                                                    'modifier') else " " + head_parent.lemma.lower()
                                        parent_texte += " )"

                                    pack_of_contraintes.append(
                                        " ".join([self_texte, contrainte_.__str__(), parent_texte]))
                            if len(pack_of_contraintes) == 0:
                                pack_of_contraintes = ['aucune']
                            contraintes = pack_of_contraintes[:]

                        i = 0
                        for contrainte in contraintes:
                            i += 1
                            if take:

                                if référence_item._flat_tree == '':
                                    features_treated = []
                                    grp_ = {}
                                    grps = 0
                                    for line in [line.strip() for line in data.strip().split("\n") if
                                                 not line.startswith("#") and line.strip() != ""]:
                                        if line.startswith("#"):
                                            continue

                                        éléments = line.split("\t")
                                        référence_sentence = ''
                                        input = ''
                                        sep = ''

                                        if référence_item._is_train:
                                            if args.author_id:
                                                input += sep + référence_item._author.replace(" ", "_")
                                                sep = " "
                                            if not args.no_book_id:
                                                input += sep + référence_item._doc_id
                                                sep = " "
                                            if not args.no_sent_id:
                                                input += sep + référence_item._sent_id
                                                sep = " "
                                            référence_sentence = input[:]

                                        for élément in éléments:
                                            input += sep + "$ " + élément.replace("=", " = ").replace("&",
                                                                                                      " & ").replace(
                                                "|", " | ")
                                        info_ano = "ano =" in input

                                        take_stucture = not args.without_structure
                                        if take_stucture and référence_item._is_train and i == 1:
                                            if args.light_structure and not info_ano:
                                                take_stucture = False
                                            if args.inv_light_structure and info_ano:
                                                take_stucture = False

                                            if take_stucture:
                                                référence_item._inputs.append(input)
                                                len_input = len(référence_item._inputs[-1].split(" "))
                                                if len_input > max_input:
                                                    max_input = len_input
                                                référence_item._anonymise_inputs.append(infos_anonymisation)

                                                référence_item._anonymise_outputs.append(infos_anonymisation_simplified)

                                                # if (args.anonymise or args.anonymise_accord):
                                                référence_item._outputs.append(référence_item._anonymized_sentence)
                                                # else:
                                                #     référence_item._outputs.append(référence_item._tokenized_text)
                                                len_input = len(référence_item._outputs[-1].split(" "))
                                                if len_input > max_output:
                                                    max_output = len_input
                                        if référence_item.isInside(éléments[0]):
                                            lancement = référence_item.getItem(éléments[0])
                                        else:
                                            étiquette = éléments[0].replace("mod_", "")  # on a renommé à la volée des adjectifs
                                            if not référence_item.isInside(étiquette):
                                                for node_testé in référence_item._items:
                                                    if référence_item._items[node_testé].ID == éléments[0]:
                                                        lancement = référence_item._items[
                                                            node_testé]  # bon, je suis fatiguée, il est tard
                                                        break
                                            else:
                                                lancement = référence_item.getItem(étiquette)

                                        # if (not ((lancement.form.startswith('ARG') and lancement.ID.startswith("var"))
                                            #          or lancement.ID.startswith("mod") or len(lancement.child) == 0)) or 'modifier' in lancement.form:
                                            # generation_data.append(getValue(lancement, features_treated, [], "").strip())
                                        if lancement.form.startswith('ARG') :
                                            relation = lancement.form
                                            texte_objet = ""
                                            for head in lancement.head:
                                                if head.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                                    if (head.anonymous_value != "" or head.ID in anonyme_names.keys()) and (args.anonymise or args.anonymise_accord):
                                                        texte_objet = head.anonymous_value.lower() if head.anonymous_value.lower() != "" else  anonyme_names[head.ID].lower()
                                                if texte_objet == "":
                                                    texte_objet = head.lemma.lower() if not args.anonymise_accord and child.anonymous_value.lower() != "" else child.anonymous_value.lower()
                                                if "￨" not in texte_objet  and head.lemma != "":
                                                    texte_objet +=  "￨" + head.valeur_encodée()
                                                    anonyme_names[head.ID] = texte_objet
                                            if texte_objet == "":
                                                continue
                                            filtred = filter(lambda x : ("arg_2" in lancement.form.lower() or "arg_3" in lancement.form.lower()) and x.startswith("as="), lancement.misc)
                                            childs = lancement.child
                                            for f in filtred:
                                                # relation = lancement.form + " " + f
                                                if référence_item.isInside(lancement.ID.split(".")[-1]):
                                                    child = référence_item.getItem(lancement.ID.split(".")[-1])
                                                else:
                                                    étiquette = lancement.ID.split(".")[-1].replace("mod_", "")  # on a renommé à la volée des adjectifs
                                                    if not référence_item.isInside(étiquette):
                                                        for node_testé in référence_item._items:
                                                            if référence_item._items[node_testé].ID == éléments[0]:
                                                                child = référence_item._items[node_testé]  # bon, je suis fatiguée, il est tard
                                                                break
                                                    else:
                                                        child = référence_item.getItem(étiquette)
                                                childs = [child]
                                            if len(childs)== 0 and "." in lancement.ID:
                                                if référence_item.isInside(lancement.ID.split(".")[-1]):
                                                    child = référence_item.getItem(lancement.ID.split(".")[-1])
                                                else:
                                                    étiquette = lancement.ID.split(".")[-1].replace("mod_", "")  # on a renommé à la volée des adjectifs
                                                    if not référence_item.isInside(étiquette):
                                                        for node_testé in référence_item._items:
                                                            if référence_item._items[node_testé].ID == éléments[0]:
                                                                child = référence_item._items[node_testé]  # bon, je suis fatiguée, il est tard
                                                                break
                                                    else:
                                                        child = référence_item.getItem(étiquette)
                                                childs = []
                                            for child in childs:
                                                if child.ID.startswith("grp_") or (child.ID.startswith("jux") ):
                                                    continue
                                                relation_ = relation[:]
                                                texte_child = ""

                                                for ad_position_ in child.adPosition:
                                                    prep, var = ad_position_
                                                    if var.ID != lancement.ID:
                                                        continue
                                                    relation_ += " " + prep
                                                    if "( " + lancement.form.lower() + " " + prep + " " + child.anonymous_value.lower() + " )" in generation_data:
                                                        texte_child = child.anonymous_value.lower()
                                                        generation_data.remove("( " + lancement.form.lower() + " " + prep + " " + child.anonymous_value.lower() + " )")
                                                    elif "( " + lancement.form.lower() + " " + prep + " " + child.form.lower() + "￨"  + child.valeur_encodée() + " )" in generation_data:
                                                        texte_child = child.lemma.lower() + "￨"  + child.valeur_encodée()
                                                        generation_data.remove( "( " + lancement.form.lower() + " " + prep + " " + child.form.lower()  + "￨"  + child.valeur_encodée()+ " )")
                                                    elif "( " + lancement.form.lower() + " " + prep + " " + child.form.lower() + " )" in generation_data:
                                                        texte_child = "" #on refait la recherche
                                                        generation_data.remove("( " + lancement.form.lower() + " " + prep + " " + child.form.lower() + " )")
                                                    else:
                                                        texte_child = ""
                                                    break
                                                if texte_child == "":
                                                    if child.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                                        if (child.anonymous_value != "" or child.ID in anonyme_names.keys()) and (args.anonymise or args.anonymise_accord):
                                                            texte_child = child.anonymous_value.lower() if child.anonymous_value.lower() != "" else  anonyme_names[child.ID].lower()
                                                    if texte_child == "":
                                                        texte_child = child.lemma.lower() if not args.anonymise_accord and child.anonymous_value.lower() != "" else child.anonymous_value.lower()
                                                    if "￨" not in texte_child and not child.form.startswith('ARG'):
                                                        texte_child += "￨" + child.valeur_encodée()
                                                        anonyme_names[child.ID] = texte_child
                                                    elif  child.form.startswith('ARG') and  "￨" in texte_child :
                                                        texte_child = child.lemma.lower() if not args.anonymise_accord and child.anonymous_value.lower() != "" else child.anonymous_value.lower() if  "￨"  in child.anonymous_value.lower()  else child.anonymous_value.lower() + "￨" + child.valeur_encodée()
                                                    elif child.form.startswith('ARG') and relation_ == relation and child.ID == lancement.ID :
                                                        continue
                                                    elif "￨" not in texte_child and child.ID.isdigit() and child.form.startswith('ARG'):
                                                        texte_child = child.lemma.lower() if not args.anonymise_accord and child.anonymous_value.lower() != "" else child.anonymous_value.lower() if  "￨"  in child.anonymous_value.lower()  else child.anonymous_value.lower() + "￨" + child.valeur_encodée()
                                                    elif child.form.lower().startswith("arg_") and "var_" in child.ID and "." in child.ID:
                                                        texte_child = child.form.lower()
                                                        head_ref = child.ID.split(".")[0]
                                                        head_ref = référence_item.getItem(head_ref)
                                                        head_ref_text = ""
                                                        if head_ref.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                                            if (head_ref.anonymous_value != "" or head_ref.ID in anonyme_names.keys()) and (args.anonymise or args.anonymise_accord):
                                                                head_ref_text = head_ref.anonymous_value.lower() if head_ref.anonymous_value.lower() != "" else anonyme_names[head_ref.ID].lower()
                                                        if head_ref_text == "":
                                                            head_ref_text = head_ref.lemma.lower() if not args.anonymise_accord and child.anonymous_value.lower() != "" else child.anonymous_value.lower()
                                                        if "￨" not in head_ref_text:
                                                            head_ref_text += "￨" + head_ref.valeur_encodée()
                                                        texte_child += " " + head_ref_text
                                                    elif args.anonymise_accord and ("￨" in texte_child and (texte_child.startswith("event_") or texte_child.startswith("mod_") or texte_child.startswith("argevent_") or False)):
                                                        pass
                                                    else:
                                                        print(texte_child)
                                                        raise Exception
                                                generation_data.append(("( " + texte_objet + " " + relation_ +  " " + texte_child +" )").lower())
                                        if "modifier" in lancement.form :
                                            relation = lancement.anonymous_value.lower() if lancement.anonymous_value.lower() != "" else  anonyme_names[lancement.ID].lower()

                                            for child in lancement.head:
                                                texte_child = ""
                                                if child.ID.startswith("grp_") or (child.ID.startswith("jux") ):
                                                    continue
                                                if (child.anonymous_value != "" or child.ID in anonyme_names.keys()) and (args.anonymise or  args.anonymise_accord):  # (args.anonymise or  args.anonymise_accord) and
                                                    texte_child = child.anonymous_value.lower() if child.anonymous_value.lower() != "" else \
                                                    anonyme_names[child.ID].lower()
                                                else:
                                                    texte_child = child.form.lower() if child.form not in ('copule', 'event', 'event&modifier', 'copule&modifier',
                                                    'modifier') and not (
                                                                child.universal_pos == UPOS.adjective and args.reveal_adjectif) else child.lemma.lower()
                                                if "￨" not in texte_child:
                                                    #print(child.__str__(), file=sys.stderr)
                                                    if child.form.startswith('ARG') and "￨" in texte_child:
                                                        texte_child = child.lemma + "￨" + child.valeur_encodée()
                                                    elif child.form.startswith('ARG'):
                                                        pass
                                                    else:
                                                        texte_child += "￨" + child.valeur_encodée()
                                                if texte_child.startswith("arg_"):
                                                    if "var_" in child.ID and "." in child.ID:
                                                        head_ref = child.ID.split(".")[0]
                                                        head_ref = référence_item.getItem(head_ref)
                                                        head_ref_text = ""
                                                        if head_ref.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                                            if (head_ref.anonymous_value != "" or head_ref.ID in anonyme_names.keys()) and (args.anonymise or args.anonymise_accord):
                                                                head_ref_text = head_ref.anonymous_value.lower() if head_ref.anonymous_value.lower() != "" else anonyme_names[head_ref.ID].lower()
                                                        if head_ref_text == "":
                                                            head_ref_text = head_ref.lemma.lower()
                                                        if "￨" not in texte_objet:
                                                            head_ref_text += "￨" + head_ref.valeur_encodée()
                                                        texte_child += " " + head_ref_text
                                                generation_data.append(relation + " ( " + texte_child + "  )")

                                        elif lancement.ID.startswith("grp_") or lancement.ID.startswith("jux"):
                                            if lancement.ID not in grp_:
                                                grp_[lancement.ID] = grps
                                                grps += 1
                                            textes_child  = []
                                            if lancement.ID.startswith("grp_") and not lancement.ID.startswith("jux") :
                                                text_master = lancement.form
                                            else:
                                                text_master = lancement.ID[0:3] + ("" if grp_[lancement.ID] else grp_[lancement.ID].__str__())

                                            for child in lancement.head + lancement.child:
                                                if child.ID.startswith("jux") and child.ID not in grp_:
                                                    grp_[child.ID] = grps
                                                    grps += 1
                                                if child.ID in grp_.keys():
                                                    if child.ID[0:3] +  ("" if grp_[child.ID] else grp_[child.ID].__str__()) not in textes_child:
                                                        textes_child.append(child.ID[0:3] +  ("" if grp_[child.ID] else grp_[child.ID].__str__()))
                                                else:
                                                    if child.anonymous_value != "" or child.ID in anonyme_names.keys():  # (args.anonymise or  args.anonymise_accord) and
                                                        textes_child.append(child.anonymous_value.lower() if child.anonymous_value.lower() != "" else \
                                                            anonyme_names[child.ID].lower())
                                                    else:
                                                        textes_child.append(child.form.lower() if child.form not in ('copule', 'event', 'event&modifier', 'copule&modifier',
                                                        'modifier') else child.lemma.lower() if child.lemma != "" else child.form)
                                                    if "￨" not  in textes_child[-1] and not child.form.startswith('ARG'):
                                                        if child.valeur_encodée() not in textes_child[-1]:
                                                            indec = textes_child[-1].index("￨") if "￨" in textes_child[-1]  else len( textes_child[-1])
                                                            textes_child[-1] = textes_child[-1][0:indec] + "￨" + child.valeur_encodée()

                                                        if textes_child[-1].startswith("arg_"):
                                                            if "var_" in child.ID and "." in child.ID:
                                                                head_ref = child.ID.split(".")[0]
                                                                head_ref = référence_item.getItem(head_ref)
                                                                head_ref_text = ""
                                                                if head_ref.form in ('copule', 'event', 'event&modifier', 'copule&modifier'):
                                                                    if (head_ref.anonymous_value != "" or head_ref.ID in anonyme_names.keys()) and (args.anonymise or args.anonymise_accord):
                                                                        head_ref_text = head_ref.anonymous_value.lower() if head_ref.anonymous_value.lower() != "" else \
                                                                        anonyme_names[head_ref.ID].lower()
                                                                if texte_objet == "":
                                                                    head_ref_text = head_ref.lemma.lower()
                                                                if "￨" not in head_ref_text:
                                                                    head_ref_text += "￨" + head_ref.valeur_encodée()
                                                                textes_child[-1] += " " + head_ref_text
                                                                a = 1

                                            generation_data.append(text_master + " ( " + " , ".join(textes_child)+ " )")

                                    gen_dat = set()
                                    for gen in sorted(generation_data, key=lambda x: len(x), reverse=True):
                                        add_gen = True
                                        for gen_d in gen_dat:
                                            if gen in gen_d:
                                                add_gen = False
                                                break
                                        if add_gen:
                                            gen_dat.add(gen)
                                    generation_data = list(gen_dat)
                                    random.shuffle(generation_data)
                                    référence_item.setFlatTree(" ".join(generation_data[:]))
                                    yield référence_item.__str__().replace("  ", " ")

                                if args.insert_global_contraintes and référence_sentence.strip() != "" and référence_item._is_train and i == 1:
                                    # random.shuffle(generation_data) # --> on s'en fiche de faire le shuffle
                                    référence_item._inputs.append(référence_sentence.strip() + " " + " ".join(sorted(contraintes)))
                                    len_input = len(référence_item._inputs[-1].split(" "))
                                    if len_input > max_input:
                                        max_input = len_input
                                    référence_item._anonymise_inputs.append(infos_anonymisation)

                                    référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                    # if (args.anonymise or  args.anonymise_accord):
                                    référence_item._outputs.append(référence_item._anonymized_sentence)
                                    # else:
                                    #   référence_item._outputs.append(référence_item._tokenized_text)
                                    len_input = len(référence_item._outputs[-1].split(" "))
                                    if len_input > max_output:
                                        max_output = len_input

                                    # le booléen istrain n'est là que pour plus de clarté, référence_sentence n'est en version <=0.999 nourri que quand ._is_train est à True
                                if référence_item._is_train and référence_sentence.strip() != "" and args.insert_global_info and i == 1:
                                    random.shuffle(generation_data)
                                    référence_item._inputs.append(référence_sentence.strip() + " " + " ".join(generation_data[:]))
                                    len_input = len(référence_item._inputs[-1].split(" "))
                                    if len_input > max_input:
                                        max_input = len_input

                                    référence_item._anonymise_inputs.append(infos_anonymisation)
                                    référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                    # if (args.anonymise or  args.anonymise_accord):
                                    référence_item._outputs.append(référence_item._anonymized_sentence)
                                    # else:
                                    #    référence_item._outputs.append(référence_item._tokenized_text)
                                    len_input = len(référence_item._outputs[-1].split(" "))
                                    if len_input > max_output:
                                        max_output = len_input

                                random.shuffle(generation_data)
                                if args.without_contraintes:
                                    référence_item._inputs.append(" ".join(generation_data[:]))
                                    len_input = len(référence_item._inputs[-1].split(" "))
                                    if len_input > max_input:
                                        max_input = len_input
                                        # if (args.anonymise or  args.anonymise_accord):
                                    référence_item._anonymise_inputs.append(infos_anonymisation)

                                    référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                    référence_item._outputs.append(référence_item._anonymized_sentence)
                                    len_input = len(référence_item._outputs[-1].split(" "))
                                    if len_input > max_output:
                                        max_output = len_input
                                        
                                else:
                                    référence_item._inputs.append(" ".join([contrainte, ">"] + generation_data[:]))
                                    len_input = len(référence_item._inputs[-1].split(" "))
                                    if len_input > max_input:
                                        max_input = len_input
                                        # if (args.anonymise or  args.anonymise_accord):
                                    référence_item._anonymise_inputs.append(infos_anonymisation)

                                    référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                    référence_item._outputs.append(référence_item._anonymized_sentence)
                                    len_input = len(référence_item._outputs[-1].split(" "))
                                    if len_input > max_output:
                                        max_output = len_input
                                #
                                if référence_item._is_train:
                                    if args.author_id:
                                        # if (args.anonymise or  args.anonymise_accord):
                                        référence_item._inputs.append(référence_item._author.replace(" ", "_"))
                                        référence_item._anonymise_inputs.append(infos_anonymisation)
                                        len_input = len(référence_item._inputs[-1].split(" "))
                                        if len_input > max_input:
                                            max_input = len_input

                                        référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                        référence_item._outputs.append(référence_item._anonymized_sentence)
                                        len_input = len(référence_item._outputs[-1].split(" "))
                                        if len_input > max_output:
                                            max_output = len_input

                                    if args.ordered_names:
                                        keys = list(infos_anonymisation_simplified.keys())
                                        input_k = dict(map(reversed, infos_anonymisation.items()))
                                        ordered_infos_ = []
                                        ordered_infos = référence_item._anonymized_sentence.split()
                                        if args.ordered_names_by_author:
                                            ordered_infos_.append(référence_item._author.replace(" ", "_"))
                                        for ordered_info in ordered_infos:
                                            if ordered_info in keys:
                                                ordered_infos_.append(
                                                    input_k[infos_anonymisation_simplified[ordered_info]])

                                        référence_item._inputs.append(" ".join(ordered_infos_))
                                        référence_item._anonymise_inputs.append(référence_item._author.replace(" ", "_"))
                                        len_input = len(référence_item._inputs[-1].split(" "))
                                        if len_input > max_input:
                                            max_input = len_input


                                        référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                        référence_item._outputs.append(référence_item._anonymized_sentence)
                                        len_input = len(référence_item._outputs[-1].split(" "))
                                        if len_input > max_output:
                                            max_output = len_input

                                if référence_item._is_train:
                                    if args.doubled_with_author:
                                        référence_item._inputs.append(référence_item._author.replace(" ", "_") + " " + référence_item._inputs[-1])
                                        référence_item._anonymise_inputs.append(infos_anonymisation)
                                        len_input = len(référence_item._inputs[-1].split(" "))
                                        if len_input > max_input:
                                            max_input = len_input

                                        référence_item._anonymise_outputs.append(infos_anonymisation_simplified)
                                        référence_item._outputs.append(référence_item._anonymized_sentence)
                                        len_input = len(référence_item._outputs[-1].split(" "))
                                        if len_input > max_output:
                                            max_output = len_input

                                      
                                # else:
                                #     référence_item._outputs.append(référence_item._tokenized_text)
                                #     len_input = len(référence_item._outputs[-1].split(" "))
                                #     if len_input > max_input:
                                #         max_input = len_input

                    if not take:
                        référence_item.take = False
                        référence_item._flushed_value = ""
                        self.gestLoose(référence_item)
                        take = True
                        continue
                else:
                    référence_item.take = False
                    référence_item._flushed_value = ""
                    self.gestLoose(référence_item)
                    take = True
                    continue

            référence_item.flush()

            del référence_item
            self._analyses[i_analyse] = None
        progress.close()


stock_analyses = analyses()


class conllAnalyse(object):
    global stock_analyses

    def __init__(self, doc_id, sent_id, book, author, sentence, tokenized_sentence, json_data):
        self._items = {}
        self._json_data = json_data
        self._doc_id = doc_id
        self._book = book
        self._author = author
        self._sent_id = sent_id
        self._text = sentence
        self._tokenized_text = tokenized_sentence
        self._inputs = []
        self._outputs = []
        self._anonymise_inputs = []
        self._anonymise_outputs = []
        self._is_train = False
        self._anonymized_sentence = ""
        self._flat_tree = ""
        self._flushed_value = ''
        self._flushable = True
        self._cached_head = []
        stock_analyses.append(self)

    def setAnonymized(self, sentence):
        self._anonymized_sentence = sentence

    def setFlatTree(self, flat_tree):
        if self._flat_tree == "":
            self._flat_tree = flat_tree
            self._string_value = ''

    def getId(self):
        return self._sent_id

    def flush(self):
        self._flushable = False

    @property
    def flushable(self):
        return self._flushable

    def destroy(self):
        stock_analyses.remove(self)
        del self

    def deleteItem(self, conllObject: conllItem):
        if conllObject.ID not in self._items.keys():
            del self._items[conllObject.ID]

    def addItem(self, conllObject: conllItem):
        if conllObject.ID not in self._items.keys():
            self._items[conllObject.ID] = conllObject
        else:
            compare = self._items[conllObject.ID]
            if len(
                    compare.adPosition) > 0 and '.' in compare.ID:  # on est sur des adpositions modulant le mot par un adverbe
                index = getTGIndex(compare.ID[compare.ID.index('.') + 1:], grew, talismane)
                if grew[index - 2][4] == "ADV":
                    conllObject_intercalé = conllItem(initialID="mod_" + token + "." + grew[index - 2][0],
                                                      form="modifier",
                                                      lemma=grew[index - 2][2],
                                                      partOfSpeechTAG=UPOS.adverb, real_form=grew[index - 2][1])
                    for misc in compare.misc:
                        conllObject_intercalé.misc = misc
                    self.addItem(conllObject_intercalé)
                    self._items[conllObject.ID] = conllObject
                    conllObject_intercalé.head = conllObject

    def getHeads(self) -> []:
        if not self._flushable:
            return self._cached_head
        heads = []
        for head in sorted(self._items.values(), key=lambda x: x.floatID()):
            if len(head.head) == 0:
                if len(head.child_contrainte) == 0:
                    if len(head.adPosition) == 0:
                        heads.append(head)
                else:
                    for child in head.child_contrainte:
                        if child[1] in head.child and len(head.adPosition) == 0:
                            heads.append(head)
        self._cached_head = heads
        return heads

    def isInside(self, ID: str) -> bool:
        return ID in self._items.keys()

    def getItem(self, id: str) -> conllItem:
        return self._items[id]

    @property
    def setFeature(self, ID: str, feature: Enum):
        if ID not in self._items.keys():
            raise Exception
        if isinstance(feature, Features.Polarity):
            self._items[ID].polarité = feature
        elif isinstance(feature, Features.Mood):
            self._items[ID].mood = feature
        elif isinstance(feature, Features.Gender):
            self._items[ID].gender = feature
        elif isinstance(feature, Features.Number):
            self._items[ID].number = feature
        elif isinstance(feature, Features.Aspect):
            self._items[ID].aspect = feature
        elif isinstance(feature, Features.Voice):
            self._items[ID].voice = feature
        elif isinstance(feature, Features.VerbForm):
            self._items[ID].verbForm = feature
        elif isinstance(feature, Features.Person):
            self._items[ID].person = feature
        elif isinstance(feature, Features.Possession):
            self._items[ID].possession = feature
        else:
            raise Exception

    def __str__(self):  #
        if self._flushed_value == "" or self._flushable:
            id_c = stock_analyses.getID(self).__str__()
            retours = []
            retours.append('# doc_id = ' + self._doc_id)
            retours.append('# book = ' + self._book)
            retours.append('# author = ' + self._author)
            retours.append('# sent_id = ' + self._sent_id)
            retours.append('# newsent_id = ' + id_c)
            retours.append('# text = ' + self._text)
            retours.append('# tokenized_sentence = ' + self._tokenized_text)
            if self._flat_tree != "":
                retours.append('# flat_tree = ' + self._flat_tree)
            if self._anonymized_sentence != "":
                retours.append('# anonymized tokenized_sentence = ' + self._anonymized_sentence)
            retours += [value.__str__() for value in sorted(self._items.values(), key=lambda x: x.floatID())]
            self._flushed_value = "\n".join(retours)
        if not self._flushable:
            for item in list(self._items.keys()):
                self._items[item].clean()
                del self._items[item]
            del self._items
            del self._json_data
        return self._flushed_value


class sentence_time_analyse():

    def __init__(self):
        self._mood = []
        self._tense = []
        self._verbForm = []

    def addEvent(self, event: conllItem):
        self._mood.append(event.mood)
        self._tense.append(event.tense)
        self._verbForm.append(event.verbForm)

    @property
    def size(self):
        return len(self._mood).__str__()

    @property
    def indicativeSize(self):
        return self._mood.count(Features.Mood.INDICATIVE).__str__()

    @property
    def subjunctiveSize(self):
        return self._mood.count(Features.Mood.SUBJUNCTIVE).__str__()

    @property
    def conditionnelSize(self):
        return self._mood.count(Features.Mood.CONDITIONAL).__str__()

    @property
    def conjuguésSize(self):
        return self._verbForm.count(Features.VerbForm.FINITE).__str__()

    @property
    def infinitiveSize(self):
        return self._verbForm.count(Features.VerbForm.INFINITIVE).__str__()

    @property
    def participleSize(self):
        return self._verbForm.count(Features.VerbForm.PARTICIPLE).__str__()

    @property
    def moods(self):
        return "indicative:" + self.indicativeSize + "," + \
               "subjunctive:" + self.subjunctiveSize + "," + \
               "conditionnal:" + self.conditionnelSize

    @property
    def verbsForm(self):
        return "finite:" + self.conjuguésSize + "," + \
               "infinitive:" + self.infinitiveSize + "," + \
               "participle:" + self.participleSize


def getNumber(analyse_1_2: []):
    index_data_item = analyse_1_2[0]
    if index_data_item in liste_events.keys():
        event_relatif_nombre = \
        events[liste_events[index_data_item]][index_data_item]['constraints']['conjugaison']['informations']['nombre']
        if event_relatif_nombre == "plural":
            return Features.Number.PLURAL
        elif event_relatif_nombre == "singular":
            return Features.Number.SINGULAR
        elif "sujet" in events[liste_events[index_data_item]][index_data_item].keys():
            return getNumber(events[liste_events[index_data_item]][index_data_item]['sujet'][1][0:2])
        else:
            return Features.Number.INDEFINI
    elif objItem.number == Features.Number.INDEFINI:
        a = singularize(analyse_1_2[1])
        b = pluralize(analyse_1_2[1])
        if a != b:
            if a.lower() == analyse_1_2[1].lower():
                return Features.Number.SINGULAR
            else:
                return Features.Number.PLURAL
        else:
            return Features.Number.SINGULAR
    else:
        return objItem.number


def addModifiers(events, classe, token, référent, analyse_conll, form, lemma, ):
    global take
    for modifier in events[classe][token]["modifiers"]:
        # là il faut créer une variable modifieuse qui elle sera typée, sinon on ne comprend rien !
        if len (modifier) == 10 :
            if modifier[4] not in ("N", "NC", "NPP", "ADV", "ADJ", "V"):
                continue
            modifier = ('affirmation', modifier)
        if len(modifier) == 2:

            done = False
            axe = modifier[0].replace(" ", "_")
            for arg_modifiant in [modifier[1]] if len(modifier[1]) == 10 else modifier[1]:
                if len(arg_modifiant) != 10 and not isinstance(arg_modifiant[0], str):
                    raise Exception
                if arg_modifiant[4] == "CLR" or arg_modifiant[5] == 's=refl':
                    continue
                    log(getframeinfo(currentframe()).lineno)
                _form = arg_modifiant[1].replace(" ", "_")
                _value = arg_modifiant[2].replace(" ", "_")
                _étiquette = arg_modifiant[0]
                value = arg_modifiant[2].replace(" ", "_")
                étiquette = arg_modifiant[0]
                gender = None
                if arg_modifiant[4] in ("NC", "ADJ", "DET") or (arg_modifiant[4] in ("ET", "I") and arg_modifiant[1][0].lower() == arg_modifiant[1][0]):
                    partOfSpeech = UPOS.noun
                elif arg_modifiant[4] in ("PRO", "CLO", "PROWH", "PROREL", 'CLS', "CLR"):
                    partOfSpeech = UPOS.pronoun
                elif arg_modifiant[4] == "NPP" or (
                        arg_modifiant[4] in ("ET", "I") and arg_modifiant[1][0].upper() == arg_modifiant[1][0]):
                    partOfSpeech = UPOS.properNoun
                elif arg_modifiant[3][0] == "V":
                    partOfSpeech = UPOS.verb
                    if analyse_conll.isInside("var_" + étiquette):
                        # value = "var_" + étiquette
                        # analyse_conll.getItem("var_" + étiquette).head = référent
                        # for misc_item in misc:
                        #    analyse_conll.getItem("var_" + étiquette).misc = misc_item
                        # analyse_conll.getItem("var_" + étiquette).adPosition = (axe, référent)
                        analyse_conll.getItem("var_" + étiquette).form += "&" + "modifier"
                        done = True
                elif arg_modifiant[3] == "ADV":
                    partOfSpeech = UPOS.adverb
                elif axe == "négation":
                    référent.polarité = Features.Polarity.NEG
                    done = True
                elif arg_modifiant[3] in ("P", "P+D"):
                    return False
                else:
                    print()
                    print()
                    print()
                    print()
                    print(arg_modifiant[3] + " non identifié", file=sys.stderr)
                    print(arg_modifiant, file=sys.stderr)
                    print(sentence, file=sys.stderr)
                    take = False
                    return take
                if not done:
                    if analyse_conll.isInside(étiquette) or analyse_conll.isInside("var_" + étiquette):
                        if analyse_conll.isInside("var_" + étiquette):
                            objItem = analyse_conll.getItem("var_" + étiquette)
                        else:
                            objItem = analyse_conll.getItem(étiquette)
                            objItem.ID = "mod_" + token + "." + étiquette
                            objItem.form = _form  # "modifier"
                            objItem.lemma = value
                            objItem.universal_pos = partOfSpeech
                        # objItem.head = référent
                        setGenderNumber(objItem, _form, _form)
                        if axe == "affirmation":
                            objItem.head = référent
                        elif axe == "négation":
                            référent.polarité = Features.Polarity.NEG
                            objItem.head = référent
                        else:
                            objItem.adPosition = (axe, référent)
                            objItem.deleteHead(référent)
                    else:
                        if arg_modifiant[4][0] in ("N", "NC", "NPP", "ADV"):
                            objItem = conllItem("mod_" + token + "." + étiquette, form="modifier", #_form,
                                                lemma=value,
                                                partOfSpeechTAG=partOfSpeech, real_form=_form)  # ,head=référent)
                            setGenderNumber(objItem, _form, value)
                            analyse_conll.addItem(conllObject=objItem)
                            # for misc_item in misc:
                            if axe == "affirmation":
                                objItem.head = référent
                            elif axe == "négation":
                                référent.polarité = Features.Polarity.NEG
                                objItem.head = référent
                            else:
                                objItem.adPosition = (axe, référent)
                        else:
                            objItem = conllItem(token, form=_form, #_form,
                                                lemma=value,
                                                partOfSpeechTAG=partOfSpeech, real_form=_form)  # ,head=référent)
                            setGenderNumber(objItem, _form, value)
                            analyse_conll.addItem(conllObject=objItem)
                            # for misc_item in misc:
                            if axe == "affirmation":
                                objItem.head = référent
                            elif axe == "négation":
                                référent.polarité = Features.Polarity.NEG
                                objItem.head = référent
                            else:
                                objItem.adPosition = (axe, référent)
    return take


def makeDET(référent, analyse_conll, form, lemma, modifier):
    global take
    if len(modifier) == 10:
        nature = modifier[4]
        cible = modifier[6]
        if nature in ("ADJ",):  # on modifie un argument

            étiquette = modifier[0]
            objItem = conllItem(initialID=étiquette, form="modifier",
                                lemma=modifier[2],
                                partOfSpeechTAG=UPOS.adjective,
                                head=référent, real_form=modifier[1])
            analyse_conll.addItem(conllObject=objItem)
            # if analyse_conll.isInside("var_" + étiquette):
            #   analyse_conll.getItem("var_" + étiquette).head=étiquette
        elif nature in ("ADV",):  # on modifie un argument

            étiquette = modifier[0]
            objItem = conllItem(initialID=étiquette, form="modifier",
                                lemma=modifier[2],
                                partOfSpeechTAG=UPOS.adverb,
                                head=référent, real_form=modifier[1])
            analyse_conll.addItem(conllObject=objItem)
        elif nature in ("DET",):  # on modifie un argument
            if référent.gender == Features.Gender.INDEFINI:
                référent.gender = Features.Gender.DEFINI

            plural_possible = False

            for nombre_évalué in ("deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix",
                                  "onze", "douze", "treize", "quatorze", "quinze", "seize", "vingt", "trente",
                                  'octante', "nonante", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                                  "quarante", "cinquante", "soixante", "cent", "mille", "million", "milliard", '0'):
                if nombre_évalué in modifier[1].lower():
                    plural_possible = True
                    if modifier[1] == ZERO:
                        plural_possible = False
                    référent.actualisation = Features.Actualisation.INDEFINI
                    référent.masse = Features.Masse.PARTIELLE
                    référent.number = Features.Number.PLURAL
                    référent.person = Features.Person.THIRD
                    étiquette = référent.ID
                    if "_" in étiquette:
                        étiquette = étiquette[étiquette.rindex("_") + 1:]
                    if "." in étiquette:
                        étiquette = étiquette[étiquette.rindex(".") + 1:]
                    objItem = conllItem(initialID="mod_" + étiquette + "." + modifier[0], form="modifier",
                                        lemma=modifier[1], partOfSpeechTAG=UPOS.adjective, head=référent,
                                        real_form=modifier[1])
                    analyse_conll.addItem(objItem)
                    break

            if modifier[1].lower() in ('les', 'ses', "des", "leurs", "ces", "mes",
                                       "vos", "quelques", "plusieurs", "nos", "tes") or plural_possible \
                    or (modifier[2].lower() == "les" and modifier[1].lower().endswith('des')) or modifier[
                1].lower().endswith('_des') or modifier[1].lower().endswith('tres'):
                référent.number = Features.Number.PLURAL
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ('leur', "votre", "ton"):
                référent.number = Features.Number.SINGULAR
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ('chaque', 'quelque', 'quelqu\''):
                référent.number = Features.Number.SINGULAR
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ('la', 'sa', "une", "cette", "aucune", "mainte", "ladite",
                                         "ma", "ta", "quelle", "certaine", "toute", "nulle", "différente", "telle"):
                référent.gender = Features.Gender.FEMININE
                référent.number = Features.Number.SINGULAR
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ('diverses', "maintes", "certaines", "toutes", "nulles", "différentes",
                                         "quelles", "lesdites", "telles"):
                référent.gender = Features.Gender.FEMININE
                référent.number = Features.Number.PLURAL
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ('divers', "maints", "certains", "tous", "nuls", "quels", "tels",
                                         "différents", "lesdits", "aux") or modifier[1].lower().endswith('aux'):
                référent.gender = Features.Gender.MASCULINE
                référent.number = Features.Number.PLURAL
                référent.person = Features.Person.THIRD
            elif (modifier[1].lower() in (
            'ce', 'le', "un", "son", 'm\'', "beau", "certain", "hors_du", "nul", "au-dessus_du", "ledit", "tel",
            "différent",
            "cet", "mon", "notre", "de", "quel", "aucun", "maint", "tout", "du", "sou", "au", 'au_delà_du') or modifier[
                      1].lower().endswith('du') or modifier[1].lower().endswith('au')) \
                    and modifier[2].lower() in ('ce', 'le', "un", "son", "quel", "aucun", "tel", "*sou", "ledit",
                                                "tout", "maint", "certain", "nul", "du", "*cent", '*m\''):
                if modifier[1] == "Sou":
                    modifier[1] = "Son"
                    modifier[2] = "son"
                référent.gender = Features.Gender.MASCULINE
                référent.number = Features.Number.SINGULAR
                référent.person = Features.Person.THIRD
            elif modifier[1].lower() in ("çà", "coax", "l'", "d'"):
                pass
            else:
                #print("\n" + modifier[1], file=sys.stderr)
                #print(modifier[2], file=sys.stderr)
                if args.debug:
                    take = False
            if référent.gender == Features.Gender.MASCULINE:
                for head in référent.head:
                    head.gender = Features.Gender.MASCULINE
            if référent.number == Features.Number.PLURAL:
                for head in référent.head:
                    head.number = Features.Number.PLURAL
            if référent.person == Features.Person.THIRD:
                for head in référent.head:
                    head.person = Features.Person.THIRD
            if modifier[1].lower() in (
            "mon", "ma", "mes", "ton", "ta", "tes", "son", "sa", "ses", "notre", "nos", "votre", "vos", "leur", "leurs",
            "le", "la", "les", "l'", "ce", "cette", "cet", "ces", "aux", "au",) \
                    or modifier[1].lower() in (
            "tout", "tous", "toute", "toutes", "ledit", "ladite", "lesdits", "lesdites",):
                référent.actualisation = Features.Actualisation.DEFINI
                référent.masse = Features.Masse.TOTALE
                if modifier[1].lower() in ("mon", "ma", "mes", "notre", "nos",):
                    référent.possession = Features.Possession.FIRST
                elif modifier[1].lower() in ("ton", "ta", "tes", "votre", "vos"):
                    référent.possession = Features.Possession.SECOND
                elif modifier[1].lower() in ("son", "sa", "ses", "leur", "leurs"):
                    référent.possession = Features.Possession.THIRD
                elif modifier[1].lower() in ("ce", "cette", "cet", "ces"):
                    référent.possession = Features.Possession.ZERO
            else:
                référent.actualisation = Features.Actualisation.INDEFINI

                if modifier[1].lower() in (
                'chaque', 'quelque', 'quelqu\'', "mainte", "quelle", "certaine", "telle", 'diverses', "maintes",
                "certaines",
                "différentes", "quelque", "quelques",
                "quelles", "telles", 'divers', "maints", "certains", "quels", "tels",
                "différents", "aux", "un", "certain", "tel", "différent", "des",
                "de", "quel", "maint", "du", "quel", "tel", "maint", "certain", "*cent"):
                    référent.masse = Features.Masse.PARTIELLE
                if modifier[1].lower() in ('chaque'):
                    référent.masse = Features.Masse.TOTALE
                if modifier[1].lower() in ("aucune", "nulle", "nulles", "nuls", "nul", "aucun"):
                    référent.masse = Features.Masse.NULLE

            form = form.lower()
            lemma = lemma.lower()
            if référent.gender != Features.Gender.INDEFINI and référent.gender != Features.Gender.DEFINI:
                if form not in _dic_gender.keys():
                    _dic_gender[form] = {}
                if lemma not in _dic_gender[form].keys():
                    _dic_gender[form][lemma] = {}
                if len(_dic_gender[form][lemma]) == 0:
                    _dic_gender[form][lemma] = [référent.gender]
                elif référent.gender not in _dic_gender[form][lemma]:
                    _dic_gender[form][lemma].append(référent.gender)

            if référent.number != Features.Number.INDEFINI:
                if form not in _dic_plural.keys():
                    _dic_plural[form] = {}
                if lemma not in _dic_plural[form].keys():
                    _dic_plural[form][lemma] = {}
                if len(_dic_plural[form][lemma]) == 0:
                    _dic_plural[form][lemma] = [référent.number]
                elif référent.number not in _dic_plural[form][lemma]:
                    _dic_plural[form][lemma].append(référent.number)

        else:
            log(getframeinfo(currentframe()).lineno)

    else:
        Exception

    return take


isent_test = 0
isent_val = 0
isent_train = 0
# début des traitements
for i_f, fichier in enumerate(fichiers):
    a = None
    data = None

    train_file = False
    test_file = False
    val_file = False

    if i_f == 9:
        inputs = test_inputs
        outputs = test_outputs
        anonymise_inputs = test_anonymise_inputs
        anonymise_outputs = test_anonymise_outputs
        test_file = True
    elif i_f == 8:
        inputs = val_inputs
        outputs = val_outputs
        anonymise_inputs = val_anonymise_inputs
        anonymise_outputs = val_anonymise_outputs
        val_file = True
    else:
        inputs = train_inputs
        outputs = train_outputs
        anonymise_inputs = train_anonymise_inputs
        anonymise_outputs = train_anonymise_outputs
        train_file = True

    if i_f == 9:
        if args.debug:
            if isent_test == DEBUG_TEST_SENT_COUNT:
                continue
    elif i_f == 8:
        if args.debug:
            if isent_val == DEBUG_VAL_SENT_COUNT:
                continue
    else:
        if args.debug:
            if isent_train == DEBUG_TRAIN_SENT_COUNT:
                continue

    a = open(fichier, "r")
    data = json.load(a)
    a.close()
    sentences_count += len(data.keys())
    # on va y aller lentement, mais c'est juste pour que le code soit plus lisible
    # d'abord on comptabilise pour vérifier nos bornes
    progress2 = tqdm(range(int(len(data.keys()) / 1000)))
    i_slave = 0
    for sentence in data.keys():
        i_slave += 1
        s_infos = data[sentence]

        if args.max_tokens > 0 and len(s_infos['flat tokenized'].split()) > args.max_tokens or (
                args.max_valued_tokens <= 6 and len(s_infos['flat tokenized'].split()) > 28):
            continue

        if i_slave % 1000 == 0:
            progress2.update()

        doc_id = s_infos["source"]
        sent_id = s_infos['book number'].__str__() + "/" + s_infos['analyse number'].__str__()
        # if sent_id != "117/4866" or i_f not in (8, 9):
        #   continue
        manual_sentences_count += 1
        book = s_infos['titre']
        author = s_infos['auteur']
        tokenized_sentence = s_infos['flat tokenized']

        if i_f == 9:
            if args.debug:
                if isent_test == DEBUG_TEST_SENT_COUNT:
                    break
                isent_test += 1
        elif i_f == 8:
            if args.debug:
                if isent_val == DEBUG_VAL_SENT_COUNT:
                    break
                isent_val += 1
        else:
            if args.debug:
                if isent_train == DEBUG_TRAIN_SENT_COUNT:
                    break
                isent_train += 1

        analyse_conll = conllAnalyse(doc_id, sent_id, book, author, sentence, tokenized_sentence,
                                     {'sentences': sentence, 'fichier_corpus': os.path.relpath(fichier, start=fullpath),
                                      'infos_origines': s_infos})
        analyse_conll._inputs = inputs
        analyse_conll._outputs = outputs
        analyse_conll._anonymise_inputs = anonymise_inputs
        analyse_conll._anonymise_outputs = anonymise_outputs
        analyse_conll._is_train = train_file
        analyse_conll._is_test = test_file
        analyse_conll._is_val = val_file
        events = s_infos["events"]
        events_conj = 0
        events_inf = 0
        events_vpp = 0
        events_vpa = 0
        for classe in events.keys():
            for token in events[classe]:
                if not "event" in events[classe][token].keys():
                    break
                event = events[classe][token]["event"]
                conjugaison = events[classe][token]["constraints"]["conjugaison"]
                mode = conjugaison['mode']
                assert mode in ("conj", "infinitif", "vpp", "vpa")
                if mode == "conj":
                    events_conj += 1
                elif mode == "infinitif":
                    events_inf += 1
                elif mode == "vpp":
                    events_vpp += 1
                else:
                    events_vpa += 1

        take = False if "(" in sentence else False if "|" in sentence else True
        # for argument in ("conj", "inf", "vpp", "vpa", "all"):
        #     # test du maxima
        #     if argument == "all":
        #         valeur = events_conj + events_inf + events_vpa + events_vpp
        #     else:
        #         valeur = evalued_value("events_" + argument)
        #
        #     maxima = evalued_value("args.max_" + argument)
        #     if not maxima == -1: # -1 : la borne est désactivée
        #         if valeur > maxima :
        #             take = False
        #             break
        #     # test du minima
        #     minima = evalued_value("args.min_" + argument)
        #     if not minima == -1: # -1 : la borne est désactivée
        #         if valeur < minima :
        #             take = False
        #             break
        if take:
            # quelques variables utiles
            grew = s_infos["analyses"]["grew"]
            talismane = s_infos["analyses"]["talismane"]
            stanford = s_infos["analyses"]["stanford"]

            events_treated = []
            classes = []
            liste_events = {}
            for classe in list(events.keys()):
                make_event = True
                for token in events[classe]:
                    if not "event" in events[classe][token].keys():
                        make_event = False
                        break
                    liste_events[token] = classe

                if make_event:
                    classes.append(classe)

            # on va ordonner les ajouts:
            others = ["arguments", 'arguments sur events', 'arguments hors events', 'coordination libre',
                      "énumération d'arguments",
                      "coordination à objet d'event", "modifications tierces",
                      "arguments sur items events", "coordination d'events"]
            for other in others:
                if other in events.keys():
                    classes.append(other)
            for classe in events.keys():
                if classe not in classes:
                    print(classe)
                    raise Exception

            # création des évènements (var_*)
            for classe in events.keys():
                if not take:
                    break
                    log(getframeinfo(currentframe()).lineno)
                for token in events[classe]:
                    if not take:
                        break
                        log(getframeinfo(currentframe()).lineno)
                    if not "event" in events[classe][token].keys():
                        break

                    event = events[classe][token]
                    form_évènement = "event"
                    if event['event'][2].lower() == "être" or (
                            "objet" in event.keys() and len(event["objet"]) == 2 and len(event["objet"][1]) == 10 and
                            event["objet"][1][7] == "ats" and not event["objet"][1][4] == "ET"):
                        if event['event'][2].lower() == "être" and (
                                'circonstant' in event.keys() and len(event["circonstant"]) == 2 and len(
                                event["circonstant"][1]) == 10 and event["circonstant"][1][7] == "ats" and
                                event["circonstant"][1][4] == "ET"):
                            pass
                        else:
                            form_évènement = "copule"
                            if 'circonstant' in event.keys() and len(event["circonstant"]) == 2 and len(
                                    event["circonstant"][1]) == 10:
                                if not "objet" in event.keys():
                                    if "sujet" in event.keys():
                                        event["circonstant"] = [event["sujet"][0], event["circonstant"][1]]
                                    else:
                                        event["circonstant"] = ['from_copule', event["circonstant"][1]]
                                    event["objet"] = event["circonstant"]
                                    del event["circonstant"]

                    eventItem = conllItem(initialID="var_" + token, form=form_évènement,
                                          lemma=event['event'][2].lower(), partOfSpeechTAG=UPOS.verb,
                                          real_form=event['event'][1])
                    temps = events[classe][token]["constraints"]["conjugaison"]["informations"]["temps"]
                    mode = events[classe][token]['constraints']['conjugaison']["informations"]['mode']

                    socle = events[classe][token]["constraints"]["conjugaison"]["informations"]["socle"]
                    eventItem.number = Features.Number.SINGULAR if events[classe][token]['constraints']['conjugaison']["informations"]['nombre'] == "singular" else Features.Number.PLURAL if events[classe][token]['constraints']['conjugaison']["informations"]['nombre'] == "plural" else Features.Number.INDEFINI
                    if mode == "participe" or len(events[classe][token]['constraints']['conjugaison']["auxiliaires"]) > 0:
                        if event['event'][1].lower().endswith("e"):
                            eventItem.number = Features.Number.SINGULAR
                            eventItem.gender = Features.Gender.FEMININE
                        elif event['event'][1].lower().endswith("es"):
                            eventItem.number = Features.Number.PLURAL
                            eventItem.gender = Features.Gender.FEMININE
                        else:
                            eventItem.gender = Features.Gender.MASCULINE
                            if event['event'][1].lower().endswith("é") or  event['event'][1].lower().endswith("i") or  event['event'][1].lower().endswith("u") :
                                eventItem.number = Features.Number.SINGULAR
                            elif event['event'][1].lower().endswith("és") or event['event'][1].lower().endswith("us"):
                                eventItem.number = Features.Number.PLURAL

                    eventItem.mood = Features.Mood.INDICATIVE if mode == "indicatif" else Features.Mood.CONDITIONAL if mode == "conditionnel" else Features.Mood.SUBJUNCTIVE if mode == "subjonctif" else Features.Mood.INDEFINI
                    eventItem.polarité = Features.Polarity.NEG if "négation" in events[classe][token][
                        "constraints"].keys() else Features.Polarity.POS
                    eventItem.aspect = Features.Aspect.PERFECT if temps in (
                    "passé_simple", "futur_antérieur", "passé_antérieur") else Features.Aspect.IMPERFECT

                    if not temps is None:
                        eventItem.temps = temps.strip()
                        if "|" in temps:
                            eventItem.temps = random.choice(eventItem.temps.split("|"))
                    if not mode is None:
                        eventItem.mode_fr = mode.strip()
                        if "|" in mode:
                            eventItem.mode_fr = random.choice(eventItem.mode_fr.split("|"))
                    eventItem.lvf = classe

                    eventItem.voice = Features.Voice.PASSIVE if "passif" in events[classe][token][
                        "constraints"].keys() else Features.Voice.ACTIVE
                    eventItem.tense = Features.Tense.INDEFINI if temps is None else Features.Tense.PAST if temps in (
                    "imparfait", "passé_simple", "passé_composé", "passé") \
                        else Features.Tense.PRESENT if temps in ("présent") else Features.Tense.FUTURE if temps.startswith("futur") else Features.Tense.INDEFINI

                    for tmpElt in socle:
                        if "sujet" not in events[classe][token].keys():
                            eventItem.person = Features.Person.ZERO
                        else:
                            eventItem.person = Features.Person(tmpElt[1])
                    if temps in ("passé_antérieur", "plus_que_parfait", "futur_antérieur"):
                        eventItem.tense = Features.Tense.PLUPERFECT
                    if events[classe][token]['constraints']['conjugaison']['mode'] in (
                    "participe", "vpa", "vpp", "infinitif"):
                        eventItem.tense = Features.Tense.INDEFINI
                        eventItem.verbForm = Features.VerbForm.INFINITIVE
                        if events[classe][token]['constraints']['conjugaison']['mode'] != 'infinitif':
                            eventItem.verbForm = Features.VerbForm.PARTICIPLE
                            if events[classe][token]['constraints']['conjugaison']['mode'] == "vpa":
                                eventItem.tense = Features.Tense.PRESENT
                                eventItem.aspect = Features.Aspect.PROGRESSIVE
                            else:
                                eventItem.tense = Features.Tense.PAST
                    else:
                        eventItem.verbForm = Features.VerbForm.FINITE
                    eventItem.misc = "mode=" + events[classe][token]["constraints"]["conjugaison"]["informations"][
                        "mode"]  # participe ou infinitif ou ...
                    if eventItem.verbForm != Features.VerbForm.INFINITIVE:
                        eventItem.misc = "temps=" + temps
                    eventItem.misc = "arbre=" + classe
                    a = eventItem.__str__()
                    analyse_conll.addItem(conllObject=eventItem)
            #         item.mood = mood

            if not "arguments dans events" in events.keys():
                events["arguments dans events"] = {}
            # gestion des arguments des évènements
            for classe in events.keys():
                if not take:
                    break
                    log(getframeinfo(currentframe()).lineno)
                for token in events[classe]:
                    if not take:
                        break
                        log(getframeinfo(currentframe()).lineno)
                    if not "event" in events[classe][token].keys():
                        break
                    mode_conj = events[classe][token]["constraints"]["conjugaison"]['mode']
                    nombre = events[classe][token]["constraints"]["conjugaison"]["informations"]["nombre"]
                    référent_event = analyse_conll.getItem(id="var_" + token)

                    if "sujet" in events[classe][token].keys() and "objet" in events[classe][token].keys():
                        if events[classe][token]["objet"] == events[classe][token]["sujet"]:
                            if référent_event.voice == Features.Voice.PASSIVE:
                                référent_event.person = Features.Person.ZERO
                                del events[classe][token]["sujet"]


                    # on traite les objets principaux
                    for item in ("sujet", 'objet', 'compl-prep', 'circonstant'):
                        if not take:
                            break
                            log(getframeinfo(currentframe()).lineno)
                        if item in events[classe][token].keys():
                            #         item.mood = mood
                            misc = ""
                            if len(events[classe][token][item][1]) == 2:
                                data_item = events[classe][token][item][1][1]

                                axe = events[classe][token][item][1][0].replace(" ", "_")
                                # misc="adposition=" + axe
                                value = events[classe][token][item][1][1][2].replace(" ", "_")
                                value += " " + events[classe][token][item][1][1][1].replace(" ", "_")
                                form = events[classe][token][item][1][1][1]
                                lemma = events[classe][token][item][1][1][2]
                            else:
                                data_item = events[classe][token][item][1]
                                axe = events[classe][token][item][0].replace(" ", "_")
                                value = events[classe][token][item][1][2].replace(" ", "_")
                                value += " " + events[classe][token][item][1][1].replace(" ", "_")
                                form = events[classe][token][item][1][1] if events[classe][token][item][1][
                                                                                1] != "j'" else "je"
                                lemma = events[classe][token][item][1][2]

                            if data_item[4] in ('ADV', "ADJ"):
                                data_item[4] = "NC"
                                data_item[3] = "N"
                                grew[getTGIndex(token, grew, talismane)][3] = "N"
                                grew[getTGIndex(token, grew, talismane)][4] = "NC"
                                talismane[getTGIndex(token, grew, talismane)][3] = "N"
                                talismane[getTGIndex(token, grew, talismane)][4] = "NC"
                                stanford[getTGIndex(token, grew, talismane)][3] = "N"
                                stanford[getTGIndex(token, grew, talismane)][4] = "NC"

                            étiquette = "var_" + token + "." + data_item[0]
                            objItem = conllItem(initialID=étiquette, form=item,
                                                lemma="",
                                                partOfSpeechTAG=UPOS.X, head=référent_event, real_form=data_item[1])
                            # if form_évènement != "copule" or item == "sujet":
                            objItem.misc = "as=" + events[classe][token][item][0]
                            objItem.misc = misc

                            ad_pos_item = objItem

                            # on donne une première valeur au nombre de notre variable.
                            objItem.number = Features.Number.PLURAL if item == "sujet" and nombre == 'plural' \
                                else Features.Number.SINGULAR if item == "sujet" and nombre == 'singular' \
                                else Features.Number.INDEFINI


                            if item == "sujet":
                                if référent_event.number == Features.Number.INDEFINI:
                                    référent_event.number = objItem.number
                                elif objItem.number == Features.Number.INDEFINI:
                                    objItem.number = référent_event.number
                                if référent_event.gender not in (Features.Gender.FEMININE, Features.Gender.MASCULINE):
                                    référent_event.gender = objItem.gender
                                elif objItem.gender not in (Features.Gender.FEMININE, Features.Gender.MASCULINE):
                                    objItem.gender = référent_event.gender


                            # on donne une première valeur au nombre de notre variable.
                            objItem.gender = Features.Gender.MASCULINE if item == "sujet" and \
                                                                          data_item[1].lower() in ("il", "ils") else \
                                Features.Gender.FEMININE if item == "sujet" and \
                                                            data_item[1].lower() in (
                                                                "elle") else Features.Gender.INDEFINI

                            objItem.number = getNumber(data_item[0:2])
                            setGenderNumber(objItem, data_item[1].lower(), data_item[2].lower())

                            analyse_conll.addItem(conllObject=objItem)

                            étiquette = data_item[0]
                            référent = objItem

                            if analyse_conll.isInside("var_" + étiquette):
                                objItem = analyse_conll.getItem("var_" + étiquette)
                                objItem.head = référent
                                value = "var_" + étiquette
                            elif analyse_conll.isInside("mod_" + étiquette):
                                objItem = analyse_conll.getItem("mod_" + étiquette)
                                objItem.head = référent
                                value = "mod_" + étiquette
                            else:
                                value = data_item[1]
                                objItem = conllItem(initialID=étiquette, form=value,
                                                    lemma=data_item[2], head=référent, real_form=data_item[1])
                                # if analyse_conll.isInside("var_" + étiquette):
                                #   analyse_conll.getItem("var_" + étiquette).head = objItem

                                # on donne une première valeur au nombre de notre variable.
                            objItem.gender = Features.Gender.MASCULINE if item == "sujet" and \
                                                                          data_item[1].lower() in ("il", "ils") else \
                                Features.Gender.FEMININE if item == "sujet" and \
                                                            data_item[1].lower() in (
                                                                "elle") else Features.Gender.INDEFINI
                            objItem.number = référent.number
                            objItem.universal_pos = getUPOS(data_item)

                            if value == data_item[1]:
                                setGenderNumber(objItem, data_item[1], data_item[2])
                                if objItem.number == Features.Number.PLURAL:
                                    référent.number = objItem.number

                            if analyse_conll.isInside(ID=étiquette):
                                compared = analyse_conll.getItem(étiquette)
                                if objItem.number != compared.number:
                                    if compared.number == Features.Number.INDEFINI:
                                        compared.number = objItem.number
                                if compared.head != objItem.head:
                                    for k in objItem.head:
                                        compared.head = k
                                for adPosition in objItem.adPosition:
                                    compared.adPosition = adPosition
                                compared.head = référent
                                if objItem.universal_pos != compared.universal_pos:
                                    objItem.universal_pos = UPOS.X
                                    take = False
                                objItem = compared

                            else:
                                analyse_conll.addItem(conllObject=objItem)
                            setGenderNumber(objItem, data_item[1], data_item[2])

                            if len(events[classe][token][item][1]) == 2:
                                objItem.adPosition = (axe, ad_pos_item)
                                ad_pos_item.deleteHead(objItem)
                                objItem.deleteHead(ad_pos_item)
                            if référent.number == Features.Number.INDEFINI:
                                référent.number = objItem.number

                    if "modifiers" in events[classe][token].keys():
                        # on va le faire traiter dans la chaine suivante
                        events["arguments dans events"][token] = {'argument': events[classe][token]['event'],
                                                                  "modifiers": events[classe][token]["modifiers"]}

            if len(events["arguments dans events"]) == 0:
                del events["arguments dans events"]
            # AUTRES MODIFIERS

            for classe in events.keys():
                if not take:
                    break
                    log(getframeinfo(currentframe()).lineno)
                if classe == "arguments sur events":
                    for token in events[classe].keys():
                        référent = None
                        if analyse_conll.isInside("var_" + token):
                            référent = analyse_conll.getItem(id="var_" + token)
                        elif analyse_conll.isInside(token):
                            référent = analyse_conll.getItem(id=token)
                            if référent.universal_pos == UPOS.verb:
                                référent.ID = "var_" + référent.ID
                                référent.form = form_évènement
                                référent.lemma = event['event'][2].lower()

                        if référent is None:
                            continue
                            log(getframeinfo(currentframe()).lineno)
                        if "modifiers" in events[classe][token].keys():
                            take = addModifiers(events, classe, token, référent, analyse_conll,
                                                events[classe][token]['argument'][1].lower(),
                                                events[classe][token]['argument'][2].lower())

            # AUTRES MODIFIERS
            for classe in ("arguments dans events", "arguments sur items events", 'arguments sur events', "arguments",
                           'arguments hors events',
                           "modifications tierces"):
                if classe in events.keys():
                    if not take:
                        log(getframeinfo(currentframe()).lineno)
                        break
                    for token in events[classe].keys():
                        argument = events[classe][token]['argument']
                        # on crée l'objet  - théoriquement il devrait être géré ultérieurement dans des énumérations
                        if argument[4] != "CC" and not (
                                analyse_conll.isInside(token) or analyse_conll.isInside("var_" + token)):
                            tmp = conllItem(initialID=argument[0],
                                            form=argument[1],
                                            lemma=argument[2], real_form=argument[1])
                            tmp.universal_pos = getUPOS(argument)
                            setGenderNumber(tmp, tmp.form, tmp.lemma)
                            analyse_conll.addItem(conllObject=tmp)

                        if analyse_conll.isInside(token) or analyse_conll.isInside("var_" + token):
                            if analyse_conll.isInside(token):
                                référent = analyse_conll.getItem(id=token)
                            else:
                                référent = analyse_conll.getItem(id="var_" + token)

                            if "modifiers" in events[classe][token].keys():

                                for modifier_ in events[classe][token]["modifiers"]:
                                    if len(modifier_) == 2:
                                        if len(modifier_[1]) == 10 and isinstance(modifier_[1][1], str):
                                            data_item_ = [modifier_[1]]
                                            axe_ = [modifier_[0].replace(" ", "_")]
                                            étiquette_ = [modifier_[1][0]]
                                            # misc="adposition=" + axe
                                            form_ = [modifier_[1][1]]
                                            lemma_ = [modifier_[1][2]]

                                            for data_item, axe, étiquette, form, lemma in zip(data_item_, axe_,
                                                                                              étiquette_, form_,
                                                                                              lemma_):

                                                if len(data_item) < 10:
                                                    continue
                                                if data_item[4] == "DET":
                                                    if analyse_conll.isInside(token):
                                                        référent = analyse_conll.getItem(id=token)
                                                        take = makeDET(référent, analyse_conll,
                                                                       argument[1].lower(),
                                                                       argument[2].lower(),
                                                                       data_item)
                                                else:

                                                    if analyse_conll.isInside("var_" + étiquette):
                                                        objItem = analyse_conll.getItem(id="var_" + étiquette)
                                                    elif analyse_conll.isInside(étiquette):
                                                        objItem = analyse_conll.getItem(id=étiquette)
                                                    else:
                                                        objItem = conllItem(initialID=étiquette, form=form,
                                                                            lemma=lemma,
                                                                            real_form=form)  # , head=référent)
                                                        objItem.universal_pos = getUPOS(data_item)
                                                        setGenderNumber(objItem, form, lemma)

                                                    if len(modifier_) == 2:

                                                        if axe == "négation":
                                                            référent.polarité = Features.Polarity.NEG
                                                            objItem.head = référent
                                                        else:
                                                            objItem.adPosition = (axe, référent)
                                                    else:
                                                        objItem.head = référent
                                                    analyse_conll.addItem(conllObject=objItem)
                                        else:
                                            data_item_ = []
                                            axe_ = []
                                            étiquette_ = []
                                            form_ = []
                                            lemma_ = []
                                            tab = []
                                            if not isinstance(modifier_[1][0][0], str):
                                                for liste in modifier_[1]:
                                                    for modifier in liste:
                                                        tab.append(modifier)
                                            else:
                                                for modifier in modifier_[1]:
                                                    tab.append(modifier)
                                            for modifier in tab:
                                                data_item_.append(modifier)
                                                axe_.append(modifier_[0].replace(" ", "_"))
                                                étiquette_.append(modifier[0])
                                                # misc="adposition=" + axe
                                                form_.append(modifier[1])
                                                lemma_.append(modifier[2])

                                            for data_item, axe, étiquette, form, lemma in zip(data_item_, axe_,
                                                                                              étiquette_, form_,
                                                                                              lemma_):

                                                if len(data_item) < 10:
                                                    continue
                                                if data_item[4] == "DET":
                                                    if analyse_conll.isInside(token):
                                                        référent = analyse_conll.getItem(id=token)
                                                        take = makeDET(référent, analyse_conll,
                                                                       argument[1].lower(),
                                                                       argument[2].lower(),
                                                                       data_item)
                                                else:
                                                    if analyse_conll.isInside("var_" + étiquette):
                                                        objItem = analyse_conll.getItem(id="var_" + étiquette)
                                                    elif analyse_conll.isInside(étiquette):
                                                        objItem = analyse_conll.getItem(id=étiquette)
                                                    else:

                                                        objItem = conllItem(initialID=étiquette, form=form,
                                                                            lemma=lemma,
                                                                            real_form=form)  # , head=référent)
                                                        objItem.universal_pos = getUPOS(data_item)
                                                        setGenderNumber(objItem, form, lemma)

                                                    if len(modifier_) == 2:

                                                        if axe == "négation":
                                                            référent.polarité = Features.Polarity.NEG
                                                            objItem.head = référent
                                                        else:
                                                            objItem.adPosition = (axe, référent)
                                                    else:
                                                        objItem.head = référent
                                                    analyse_conll.addItem(conllObject=objItem)
                                    else:
                                        data_item_ = [modifier_]
                                        axe_ = [""]
                                        étiquette_ = [modifier_[0]]
                                        form_ = [modifier_[1]]
                                        lemma_ = [modifier_[2]]

                                        for data_item, axe, étiquette, form, lemma in zip(data_item_, axe_, étiquette_,
                                                                                          form_, lemma_):

                                            if len(data_item) < 10:
                                                continue
                                            if data_item[4] == "DET":
                                                if analyse_conll.isInside(token):
                                                    référent = analyse_conll.getItem(id=token)
                                                    take = makeDET(référent, analyse_conll,
                                                                   argument[1].lower(),
                                                                   argument[2].lower(),
                                                                   data_item)
                                            else:
                                                if analyse_conll.isInside("var_" + étiquette):
                                                    objItem = analyse_conll.getItem(id="var_" + étiquette)
                                                elif analyse_conll.isInside(étiquette):
                                                    objItem = analyse_conll.getItem(id=étiquette)
                                                else:
                                                    objItem = conllItem(initialID=étiquette, form=form,
                                                                        lemma=lemma,
                                                                        real_form=form)  # , head=référent)
                                                    objItem.universal_pos = getUPOS(data_item)
                                                    setGenderNumber(objItem, form, lemma)

                                                if len(modifier_) == 2:
                                                    if axe == "négation":
                                                        référent.polarité = Features.Polarity.NEG
                                                        objItem.head = référent
                                                    else:
                                                        objItem.adPosition = (axe, référent)
                                                else:
                                                    objItem.head = référent
                                                analyse_conll.addItem(conllObject=objItem)

                                #    take = addModifiers(events, classe, token, référent, analyse_conll,
                                #                  modifier[1].lower(),
                                #                   modifier[2].lower())
                            if "constraints" in events[classe][token].keys():
                                if not take:
                                    log(getframeinfo(currentframe()).lineno)
                                    break
                                if "relative objet" in events[classe][token]["constraints"].keys():
                                    référent_relative = None

                                    if len(référent.head) == 1:
                                        référent_relative = référent.head[0]
                                    elif len(référent.head) == 0:
                                        if analyse_conll.isInside(token):
                                            référent_relative = conllItem(initialID="grp_" + étiquette,
                                                                          form="groupe",
                                                                          lemma="nominal", real_form="k")
                                            référent_relative.universal_pos = UPOS.X
                                            analyse_conll.addItem(conllObject=référent_relative)
                                            analyse_conll.getItem(token).head = référent_relative
                                    else:
                                        for réf_relative_1 in référent.head:
                                            for réf_relative_2 in référent.head:
                                                if réf_relative_2.ID in réf_relative_1.ID:
                                                    if réf_relative_2.ID in réf_relative_1.ID:
                                                        référent_relative = réf_relative_2
                                        if référent_relative is None:
                                            for réf_relative_1 in référent.head:
                                                if not réf_relative_1.ID.startswith("var_"):
                                                    référent_relative = réf_relative_1
                                        if référent_relative is None:
                                            raise Exception

                                        for relative in events[classe][token]["constraints"]["relative objet"]:
                                            if analyse_conll.isInside("var_" + relative[0]):
                                                eventItem = analyse_conll.getItem("var_" + relative[0])
                                                eventItem.contrainte = (Contrainte.RELATIVE_OBJET, référent_relative)
                                            else:
                                                if not analyse_conll.isInside(relative[0]):
                                                    if relative[4] == "P":
                                                        i = getTGIndex(relative[0], grew, talismane)
                                                        for j in range(i + 1, i + 4):
                                                            if analyse_conll.isInside("var_" + grew[j][0]):
                                                                eventItem = analyse_conll.getItem(
                                                                    "var_" + grew[j][0])
                                                                break
                                                    elif not analyse_conll.isInside(relative[0]):
                                                        objItem = conllItem(initialID=relative[0],
                                                                            form=relative[1],
                                                                            lemma=relative[2], real_form=relative[1])
                                                        # if analyse_conll.isInside("var_" + étiquette):
                                                        #   analyse_conll.getItem("var_" + étiquette).head = objItem

                                                        # on donne une première valeur au nombre de notre variable.
                                                        objItem.universal_pos = getUPOS(data_item)
                                                        setGenderNumber(objItem, relative[1], relative[2])
                                                        analyse_conll.addItem(conllObject=objItem)
                                                        eventItem = analyse_conll.getItem(relative[0])
                                                else:
                                                    eventItem = analyse_conll.getItem(relative[0])

                                                référent_relative.contrainte = (
                                                    Contrainte.COMPARATIF, eventItem)

                                else:
                                    raise Exception

                                if not take:
                                    break
            # AUTRES MODIFIERS
            # gestion des contraintes

            for classe in ("énumération d'arguments",):
                if classe in events.keys():
                    if not take:
                        log(getframeinfo(currentframe()).lineno)
                        break
                    for token in events[classe].keys():
                        if not take:
                            log(getframeinfo(currentframe()).lineno)
                            break
                        if "modifiers" in events[classe][token].keys():
                            modifiers = events[classe][token]["modifiers"]
                            for liste_associée in modifiers:
                                référence = liste_associée.pop(0)
                                if référence[4] == "VIMP":
                                    # on n'a pas géré les impératifs
                                    take = False
                                    log(getframeinfo(currentframe()).lineno)
                                    break
                                if not analyse_conll.isInside(référence[0]):
                                    log(getframeinfo(currentframe()).lineno)
                                    continue
                                item = analyse_conll.getItem(référence[0])
                                référent_énumération = conllItem(
                                    initialID="grp_" + référence[0],
                                    form="et",
                                    lemma="liaison", real_form="k")
                                gender_list = set()
                                for each in item.head:
                                    if each.lemma == "coordination":
                                        référent_énumération = each
                                        break
                                if référent_énumération.lemma == "liaison":
                                    for head in item.head:
                                        for child in head.child:
                                            if child.lemma == "coordination":
                                                référent_énumération = child
                                            break
                                        if référent_énumération.lemma == "coordination":
                                            break

                                if référent_énumération.lemma == "liaison":
                                    for each in list(item.head):
                                        référent_énumération.head = each
                                        item.deleteHead(each)

                                    i_del = []
                                    for i, ad in enumerate(item._adPosition):
                                        adPosition, node_ = ad
                                        référent_énumération.adPosition = ad
                                        i_del_child = []
                                        for iAdp, ad2 in enumerate(node_._child_adPosition):
                                            adP, node = ad2
                                            if adP == adPosition and node == item:
                                                i_del_child.append(iAdp)
                                        for i_c in reversed(i_del_child):
                                            node_._child_adPosition.pop(i_c)
                                        i_del.append(i)
                                    for i in reversed(i_del):
                                        item._adPosition.pop(i)

                                    i_del = []
                                    for i, ad in enumerate(item._contrainte):
                                        adPosition, node_ = ad
                                        référent_énumération.contrainte = ad
                                        i_del_child = []
                                        for iAdp, ad2 in enumerate(node_._child_contrainte):
                                            adP, node = ad2
                                            if adP == adPosition and node == item:
                                                i_del_child.append(iAdp)
                                        for i_c in reversed(i_del_child):
                                            node_._child_contrainte.pop(i_c)
                                        i_del.append(i)
                                    for i in reversed(i_del):
                                        item._contrainte.pop(i)

                                    item.head = référent_énumération
                                    analyse_conll.addItem(référent_énumération)

                                gender_list.add(item.gender)
                                for élément in liste_associée:
                                    # l'item va prendre les même head que celle de celui ratatché
                                    if analyse_conll.isInside(élément[0]):
                                        objItem = analyse_conll.getItem(élément[0])
                                    else:
                                        objItem = conllItem(initialID=élément[0], form=élément[1],
                                                            lemma=élément[2], real_form=élément[1])
                                        objItem.universal_pos = getUPOS(élément)
                                        setGenderNumber(objItem, élément[1], élément[2])
                                        analyse_conll.addItem(conllObject=objItem)
                                        gender_list.add(objItem.gender)
                                    objItem.head = référent_énumération
                                children = référent_énumération.child
                                for child in children:
                                    for child_2 in list(child.child):
                                        if child_2 in children:
                                            child_2.deleteHead(child)

                                référent_énumération.number = Features.Number.PLURAL
                                if Features.Gender.MASCULINE in gender_list:
                                    référent_énumération.gender = Features.Gender.MASCULINE
                                elif len(gender_list) == 1:
                                    référent_énumération.gender = gender_list.pop()

            for classe in ('coordination libre',):
                if not take:
                    log(getframeinfo(currentframe()).lineno)
                    break
                if classe in events.keys():
                    if not take:
                        log(getframeinfo(currentframe()).lineno)
                        break
                    for token in events[classe].keys():
                        if not take:
                            log(getframeinfo(currentframe()).lineno)
                            break
                        if "modifiers" in events[classe][token].keys():
                            modifiers = events[classe][token]["modifiers"]
                            for modifier_info in modifiers:
                                modifier = modifier_info[0]
                                liste_associée = modifier_info[1]
                                référence = liste_associée.pop(0)
                                if référence[4] == "VIMP":
                                    # on n'a pas géré les impératifs
                                    take = False
                                    log(getframeinfo(currentframe()).lineno)
                                    break
                                if not analyse_conll.isInside(référence[0]):
                                    log(getframeinfo(currentframe()).lineno)
                                    continue

                                if analyse_conll.isInside(référence[0]) or analyse_conll.isInside(
                                        "var_" + référence[0]):
                                    if analyse_conll.isInside("var_" + référence[0]):
                                        item = analyse_conll.getItem("var_" + référence[0])
                                    else:
                                        item = analyse_conll.getItem(référence[0])

                                référent_coodination = conllItem(
                                    initialID="grp_" + events[classe][token]['argument'][0],
                                    form=events[classe][token]['argument'][1],
                                    lemma="coordination", real_form="k")

                                référent_coodination.universal_pos = UPOS.X
                                analyse_conll.addItem(conllObject=référent_coodination)
                                for head in list(item.head):
                                    if head != référent_coodination:
                                        gender_list = set()
                                        gender_list.add(item.gender)

                                        item.deleteHead(head)
                                        if events[classe][token]['argument'][1] in ('et', 'ou'):
                                            référent_coodination.head = head

                                if events[classe][token]['argument'][1] in ('et', 'ou'):
                                    item.head = référent_coodination
                                else:
                                    référent_coodination.head = item
                                gender_list = set()
                                for élément in liste_associée:
                                    # todo cd
                                    if analyse_conll.isInside(élément[0]) or analyse_conll.isInside(
                                            "var_" + élément[0]):
                                        if analyse_conll.isInside("var_" + élément[0]):
                                            objItem = analyse_conll.getItem("var_" + élément[0])
                                        else:
                                            objItem = analyse_conll.getItem(élément[0])
                                        objItem.head = référent_coodination
                                    else:
                                        objItem = conllItem(initialID=élément[0], form=élément[1],
                                                            lemma=élément[2], head=référent_coodination,
                                                            real_form=élément[1])
                                        objItem.universal_pos = getUPOS(élément)
                                        setGenderNumber(objItem, élément[1], élément[2])
                                        analyse_conll.addItem(conllObject=objItem)

                                        setGenderNumber(objItem, élément[1], élément[2])
                                        # gender_list.add(objItem.gender)
                                    if "." in référent.ID:
                                        if Features.Gender.INDEFINI in gender_list:
                                            gender_list.remove(Features.Gender.INDEFINI)
                                        if Features.Gender.MASCULINE in gender_list:
                                            référent.gender = Features.Gender.MASCULINE
                                        elif len(gender_list) == 1 and référent.ID.startswith("var_") == False:
                                            référent.gender = gender_list.pop()
                                        elif not référent.ID.startswith("var_"):
                                            référent.gender = Features.Gender.DEFINI

                                        if not référent.ID.startswith("var_"):
                                            référent.number = Features.Number.PLURAL

            for classe in ("coordination d'events",):
                if not take:
                    log(getframeinfo(currentframe()).lineno)
                    break
                if classe in events.keys():
                    if not take == True:
                        log(getframeinfo(currentframe()).lineno)
                        break
                    for token in events[classe].keys():
                        if not take:
                            log(getframeinfo(currentframe()).lineno)
                            break
                        if "modifiers" in events[classe][token].keys():
                            modifiers = events[classe][token]["modifiers"]
                            référent_coodination = conllItem(initialID="grp_" + token,
                                                             form=events[classe][token]['argument'][1],
                                                             lemma="coordination", real_form="k")

                            référent_coodination.universal_pos = UPOS.X
                            analyse_conll.addItem(conllObject=référent_coodination)
                            for modifier_info in copy.deepcopy(modifiers):
                                if len(modifier_info) == 1:
                                    modifier_info = modifier_info[0]
                                coordonnant = modifier_info[0]
                                référence = modifier_info[1]
                                if analyse_conll.isInside("var_" + référence[2]):
                                    référence = analyse_conll.getItem("var_" + référence[2])
                                elif analyse_conll.isInside(référence[2]):
                                    référence = analyse_conll.getItem(référence[2])
                                else:
                                    take = False
                                    break

                                associés = modifier_info[2:]
                                if events[classe][token]['argument'][1] in ('et', 'ou'):
                                    référence.head = référent_coodination
                                else:
                                    référent_coodination.head = référence
                                for associé in associés:
                                    if analyse_conll.isInside("var_" + associé[2]):
                                        associé = analyse_conll.getItem("var_" + associé[2])
                                    elif analyse_conll.isInside("var_" + associé[2]):
                                        associé = analyse_conll.getItem(associé[2])
                                    else:
                                        take = False
                                        break

                                    associé.head = référent_coodination

            for classe in events.keys():
                if not take:
                    break
                for token in events[classe]:
                    if not take:
                        log(getframeinfo(currentframe()).lineno)
                        break
                    if not "event" in events[classe][token].keys() or classe == "arguments":
                        break
                    if "constraints" in events[classe][token].keys():

                        if "comparatif" in events[classe][token]['constraints'].keys():
                            pass

                        for item_contrainte in ("conjugaison", "relative sujet", "relative objet"):
                            if not take:
                                log(getframeinfo(currentframe()).lineno)
                                break
                            if item_contrainte in events[classe][token]['constraints'].keys() and item_contrainte in (
                            "relative sujet", "relative objet"):
                                if not 'sujet' in events[classe][token].keys():
                                    take = False
                                    log(getframeinfo(currentframe()).lineno)
                                    break
                                token_ref = events[classe][token]["sujet"][1][0]
                                if item_contrainte == "relative sujet":
                                    contrainte = Contrainte.RELATIVE_SUJET
                                else:
                                    contrainte = Contrainte.RELATIVE_OBJET


                                def getHead(arg: conllItem, baseEvent, count=0):
                                    if count == 40:
                                        return "", False
                                    else:
                                        count += 1
                                    for head in [n for n in arg.head if n != baseEvent]:
                                        if not head.ID.startswith("var_"):
                                            return getHead(head, baseEvent, count)
                                        else:
                                            return head, True
                                    for _, head in [(a, n) for n in arg.adPosition if n != baseEvent]:
                                        if isinstance(head, tuple):
                                            take = False  # on sort, la relative a été rattachée au modifier du sujet réel...
                                            break
                                        if not head.ID.startswith("var_"):
                                            return getHead(head, baseEvent, count)
                                        else:
                                            return head, True

                                    if conllItem.head == []:
                                        étiquette = "var_" + head.ID
                                        objItem = conllItem(initialID="grp_" + étiquette, form="groupe",
                                                            lemma="nominal",
                                                            partOfSpeechTAG=UPOS.X, head=head, real_form="k")

                                        analyse_conll.addItem(conllObject=objItem)
                                        return objItem, True
                                    return None, True


                                if analyse_conll.isInside(token_ref):
                                    try:
                                        head, take = getHead(analyse_conll.getItem(token_ref), eventItem)
                                        if not take:
                                            break
                                        if head is None:
                                            head = analyse_conll.getItem(token_ref)
                                    except Exception as inst:
                                        take = True
                                        continue
                                    eventItem.contrainte = (contrainte, head)
                                    item_to_del = None
                                    for child_head in head.head:
                                        for event_child in eventItem.head:
                                            if event_child.form == child_head.form:
                                                for child_1 in event_child.head:
                                                    for child_2 in child_head.head:
                                                        if child_1 == child_2:
                                                            item_to_del = (event_child, child_1)
                                    if item_to_del is not None:
                                        item_to_del[1].deleteHead(item_to_del[0])
                                        item_to_del[0].deleteHead(eventItem)
                                        for str, adPosition in item_to_del[0].adPosition:
                                            head.adPosition = str, adPosition
                                        analyse_conll.deleteItem(item_to_del[0])

                            if item_contrainte in events[classe][token][
                                'constraints'].keys() and item_contrainte == "conjugaison":
                                isOk = False
                                if events[classe][token]['constraints']['conjugaison']['mode'] != "conj":
                                    pass
                                if events[classe][token]['constraints']['conjugaison']['mode'] in (
                                        "infinitif", "vpa", "vpp"):
                                    to_treat = True
                                    if events[classe][token]['constraints']['conjugaison']['mode'] == "vpa":
                                        contrainte = Contrainte.VPA
                                        if "sujet" in events[classe][token]:
                                            token_ref = events[classe][token]["sujet"][1][0]
                                        elif "objet" in events[classe][token]:
                                            token_ref = events[classe][token]["objet"][1][0]
                                        else:
                                            token_ref = token
                                    elif events[classe][token]['constraints']['conjugaison']['mode'] == "vpp":
                                        contrainte = Contrainte.VPP
                                        if "sujet" in events[classe][token]:
                                            token_ref = events[classe][token]["sujet"][1][0]
                                        elif "objet" in events[classe][token]:
                                            token_ref = events[classe][token]["objet"][1][0]
                                        else:
                                            token_ref = token
                                    else:
                                        contrainte = Contrainte.INFINITIF
                                        if "sujet" in events[classe][token]:
                                            token_ref = events[classe][token]["sujet"][1][0]
                                        else:
                                            token_ref = token
                                    eventItem = analyse_conll.getItem("var_" + token)

                                    if len(eventItem.adPosition) > 0:
                                        isOk = True

                                    # notre verbe n'est pas relié....
                                    if not isOk and not analyse_conll.isInside(token_ref):
                                        log(getframeinfo(currentframe()).lineno)
                                        continue
                                    elif analyse_conll.isInside(token_ref):
                                        isOk = False
                                        argArgument = analyse_conll.getItem(token_ref)
                                        for argHead in argArgument.head:
                                            for head in [n for n in argHead.head if n != eventItem]:
                                                misc = []
                                                for k in head.misc:
                                                    for l in k.split("&"):
                                                        if l.startswith("mode"):
                                                            l = l.split("=")[1]
                                                            for n in l.split("|"):
                                                                if n not in ("vpa", "vpp", "infinitif", "participe"):
                                                                    misc.append(n)
                                                if len(misc) > 0:
                                                    eventItem.contrainte = (contrainte, head)
                                                    item_to_del = None
                                                    for child_head in head.head:
                                                        for event_child in eventItem.head:
                                                            if event_child.form == child_head.form:
                                                                for child_1 in event_child.head:
                                                                    for child_2 in child_head.head:
                                                                        if child_1 == child_2:
                                                                            item_to_del = (event_child, child_1)
                                                    if item_to_del is not None:
                                                        item_to_del[1].deleteHead(item_to_del[0])
                                                        item_to_del[0].deleteHead(eventItem)
                                                        for str, adPosition in item_to_del[0].adPosition:
                                                            head.adPosition = str, adPosition
                                                        analyse_conll.deleteItem(item_to_del[0])
                                                    isOk = True
                                        if not isOk:

                                            for argHead in argArgument.head:
                                                for head in [n for n in argHead.head if n != eventItem]:
                                                    if conllItem.head == []:
                                                        étiquette = "grp_" + head.ID
                                                        objItem = conllItem(initialID=étiquette,
                                                                            form="groupe",
                                                                            lemma="nominal",
                                                                            partOfSpeechTAG=UPOS.X,
                                                                            head=head, real_form="k")

                                                        analyse_conll.addItem(conllObject=objItem)

                                                        eventItem.contrainte = (contrainte, head)
                                                        item_to_del = None
                                                        for child_head in head.head:
                                                            for event_child in eventItem.head:
                                                                if event_child.form == child_head.form:
                                                                    for child_1 in event_child.head:
                                                                        for child_2 in child_head.head:
                                                                            if child_1 == child_2:
                                                                                item_to_del = (
                                                                                event_child, child_1, child_head)
                                                        if item_to_del is not None:
                                                            child_head.head = eventItem
                                                            item_to_del[1].deleteHead(item_to_del[0])
                                                            item_to_del[0].deleteHead(eventItem)
                                                            for str, adPosition in item_to_del[0].adPosition:
                                                                objItem.adPosition = str, adPosition
                                                            analyse_conll.deleteItem(item_to_del[0])

                                                        isOk = True
                                                        log(getframeinfo(currentframe()).lineno)
                                                        break

            if "arguments" in events.keys():
                for token in events["arguments"].keys():
                    if not take:
                        break
                    if not analyse_conll.isInside(token):
                        if args.debug:
                            log(getframeinfo(currentframe()).lineno)
                            continue
                        else:
                            take = False
                            log(getframeinfo(currentframe()).lineno)
                            break
                    if "modifiers" in events["arguments"][token].keys():
                        if analyse_conll.isInside("var_" + token):
                            référent = analyse_conll.getItem("var_" + token)
                        else:
                            référent = analyse_conll.getItem(token)
                        take = addModifiers(events, "arguments", token, référent,
                                            analyse_conll,
                                            events["arguments"][token]['argument'][1].lower(),
                                            events["arguments"][token]['argument'][2].lower())
        if not take:
            count_not_take += 1
            log(getframeinfo(currentframe()).lineno)
            analyse_conll.destroy()
        else:
            count_take += 1
    progress2.close()
    progress.update()
progress.close()

del data
mega_print = True

if os.path.exists(conll_file):
    exit(0)

if args.log:
    print("fichiers traités", file=flog)
    print("Local time:", time.ctime(time.time()), file=flog)
    flog.flush()

fcon = open(conll_file, 'w')
# We redirect the 'sys.stdout' command towards the descriptor file
oldi_stout = sys.stdout
sys.stdout = fcon
print("# global.columns = ID	FORM	LEMMA	UPOS	FEATS	HEAD	MISC")
# print(stock_analyses.__str__())

printer = stock_analyses.printer
i_print = 0
for conll_str in printer:
    print(conll_str)
    i_print += 1
    if i_print % 1000 == 0:
        fcon.flush()
fcon.flush()
fcon.close()

while not fcon.closed:
    True

if args.log:
    print("conll généré", file=flog)
    print("Local time:", time.ctime(time.time()), file=flog)
    flog.flush()

sys.stdout = oldi_stout
stock_analyses.delLoose()

if args.refresh_dic:
    # sauvegarde des genres et nombres
    def filterDic(_dic, dic):
        for m1 in _dic.keys():
            if not m1 is None:
                if m1 not in dic.keys():
                    dic[m1] = {}
                for m2 in _dic[m1].keys():
                    if len(_dic[m1][m2]) > 1:
                        if Features.Gender.DEFINI in _dic[m1][m2]:
                            _dic[m1][m2].remove(Features.Gender.DEFINI)
                    if m2 in dic[m1] and dic[m1][m2].value in _dic[m1][m2] and isinstance(_dic[m1][m2][0],
                                                                                          Features.Gender):
                        if dic[m1][m2] == Features.Gender.DEFINI:
                            del dic[m1][m2]
                    if len(_dic[m1][m2]) == 1 and (
                            m2 not in dic[m1] or dic[m1][m2] == Features.Gender.DEFINI):
                        dic[m1][m2] = _dic[m1][m2][0].value
                    elif m2 in dic[m1].keys():
                        if (not (_dic[m1][m2][0] == dic[m1][m2] or _dic[m1][m2][0].value == dic[m1][
                            m2])) or len(_dic[m1][m2]) > 1:
                            del dic[m1][m2]
                if len(dic[m1].keys()) == 0:
                    del dic[m1]

        return dic


    dic_gender = filterDic(_dic_gender, dic_gender)
    dic_plural = filterDic(_dic_plural, dic_plural)

    for m1 in dic_gender.keys():
        for m2 in dic_gender[m1].keys():
            if isinstance(dic_gender[m1][m2], Features.Gender):
                dic_gender[m1][m2] = dic_gender[m1][m2].value
    for m1 in dic_plural.keys():
        for m2 in dic_plural[m1].keys():
            if isinstance(dic_plural[m1][m2], Features.Number):
                dic_plural[m1][m2] = dic_plural[m1][m2].value

    fjson = open("dic_gender.json", "w")
    fjson.write(json.dumps(dic_gender, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

    print(len(dic_gender.keys()), file=sys.stderr, end=" formes genrées.\n")

    fjson = open("dic_plural.json", "w")
    fjson.write(json.dumps(dic_plural, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

    print(len(dic_plural.keys()), file=sys.stderr, end=" formes dénombrées.\n")

print("", file=sys.stderr)

fnf = lambda l: "{:,}".format(round(l, 2))
fn = lambda l: "{:,}".format(int(l)) if int(l) == l else fnf(l)
# on loggue les tailles, et écrit les données
all_sizes = 0
progress = tqdm(range(12))
base_i = ("￨" + TRAIT_VIERGE).count("￨")
base_o = ("￨" + shorted_TRAIT_VIERGE).count("￨")
for flow in ("inputs", "outputs", "anonymise_outputs", "anonymise_inputs"):
    for dépôt in ("train", "test", "val"):
        tableau = dépôt + "_" + flow
        print("Size of ", end=tableau + ": ")
        real_tableau = globals()[tableau]
        print(fn(len(real_tableau)))
        if flow == "inputs" and args.with_traits:
            for i in range(len(real_tableau)):
                real_tableau[i] = real_tableau[i].split()
                for j in range(len(real_tableau[i])):
                    if not "￨" in real_tableau[i][j]:
                        real_tableau[i][j] = real_tableau[i][j] + "￨" + TRAIT_VIERGE
                    if not (real_tableau[i][j].count("￨") == base_i):
                        while real_tableau[i][j].count("￨") != base_i:
                            real_tableau[i][j] =real_tableau[i][j][0:real_tableau[i][j].rindex("￨")]
                        print("", file=sys.stderr)
                        print(real_tableau[i][j], file=sys.stderr)
                        print(real_tableau[i][j], file=sys.stderr)
                        print(real_tableau[i][j].count("￨"), file=sys.stderr)
                        print(base_i, file=sys.stderr)
                    assert(real_tableau[i][j].count("￨") == base_i )
                    if base_i == 0:
                        base_i = real_tableau[i][j].count("￨")
                real_tableau[i] = " ".join(real_tableau[i])
        if flow == "outputs" and args.with_traits:
            for i in range(len(real_tableau)):
                real_tableau[i] = real_tableau[i].split()
                for j in range(len(real_tableau[i])):
                    if not "￨" in real_tableau[i][j]:
                        real_tableau[i][j] = real_tableau[i][j] + "￨" + shorted_TRAIT_VIERGE
                    if not (real_tableau[i][j].count("￨") == base_o or base_o ==0):
                        while real_tableau[i][j].count("￨") != base_o:
                            real_tableau[i][j] =real_tableau[i][j][0:real_tableau[i][j].rindex("￨")]
                        print("", file=sys.stderr)
                        print(real_tableau[i][j], file=sys.stderr)
                        print(real_tableau[i][j], file=sys.stderr)
                        print(real_tableau[i][j].count("￨"), file=sys.stderr)
                        print(base_o, file=sys.stderr)
                    assert(real_tableau[i][j].count("￨") == base_o or base_o ==0)
                    if base_o == 0:
                        base_o = real_tableau[i][j].count("￨")
                real_tableau[i] = " ".join(real_tableau[i])

        stats["size_token_corpus"]["Size of " + tableau] = fn(len(real_tableau))
        all_sizes += len(real_tableau) if flow == 'inputs' else 0
        with open(model_dir + '/' + tableau + '.txt', 'w') as ftxt:
            ftxt.writelines("%s\n" % l for l in real_tableau)
        progress.update()
progress.close()

print(count_not_take, file=sys.stderr, end=" non pris.\n")
print(count_take, file=sys.stderr, end=" pris.\n")
print(stock_analyses.size, file=sys.stderr, end=" analyses.\n")
print(sentences_count, file=sys.stderr, end=" sentences.\n")
print(manual_sentences_count, file=sys.stderr, end=" sentences manuelles.\n")

for line in sorted(take_table.keys(), key=lambda x: take_table[x]):
    print("", file=sys.stderr)
    print(take_table[line], file=sys.stderr, end=" line " + line.__str__())
print("", file=sys.stderr)

stats["sentences"] = {'keep': stock_analyses.size, 'from': sentences_count}

if args.debug:
    file = "debug_stats.json"
else:
    file = "stats.json"
fjson = open(file, "w")
fjson.write(json.dumps(stats, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

for key in stats["détails"].keys():
    for type in stats["détails"][key].keys():
        stats["détails"][key][type] = fnf((stats["détails"][key][type] / stock_analyses.size) * 100) + "%"

for key in stats.keys():
    if key not in ("détails", 'sentences', "size_token_corpus"):
        stats[key] = fnf((stats[key] / stock_analyses.size) * 100) + "%"

if args.debug:
    file = "debug_stats_percent.json"
else:
    file = "stats_percent.json"
fjson = open(file, "w")
fjson.write(json.dumps(stats, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True

# zoup maintenant on prepare notre cmd.bash pour openNMT
if not os.path.exists(os.path.join(model_dir, "data")):
    os.makedirs(os.path.join(model_dir, "data"))
if not os.path.exists(os.path.join(model_dir, "logs")):
    os.makedirs(os.path.join(model_dir, "logs"))

for cmd in ("_lua", ""): #

    cmd_bash = """
cd $model_dir/
fn=$(ls -t |grep pt | head -n1)

cd /home/colineem/OpenNMT-py/
pathed="$model_dir/"

python3.7 preprocess.py -train_src ${pathed}train_inputs.txt -train_tgt ${pathed}train_outputs.txt -valid_src ${pathed}val_inputs.txt -valid_tgt ${pathed}val_outputs.txt -save_data ${pathed}data/test -shard_size 25000 --src_seq_length $SRC_SEQ_LENGTH --tgt_seq_length $TGT_SEQ_LENGTH 

python3.7 train.py -encoder_type brnn -data ${pathed}data/test -save_model ${pathed}test-model -train_steps $train_steps -valid_steps $val_steps  -batch_size $batch_size -valid_batch_size $batch_val_size  -world_size 1 -gpu_ranks 0 -save_checkpoint_steps $steps_checkpoint  -max_generator_batches $max_generator_batches -start_decay_steps $steps_checkpoint -decay_steps $steps_checkpoint -log_file ${pathed}logs/log.txt --keep_checkpoint 1 $feats        

cd $model_dir/
fn=$(ls -t |grep pt | head -n1)

cd /home/colineem/OpenNMT-py/

python3.7 translate.py -model ${pathed}${fn} -src ${pathed}test_inputs.txt -tgt ${pathed}test_outputs.txt -output ${pathed}pred.txt -verbose  -batch_size $batch_translate_size -log_file ${pathed}pred.log -gpu 0 --report_bleu 

"""
    if cmd == "_lua":
        cmd_bash = cmd_bash.replace("python3.7", "th")
        cmd_bash = cmd_bash.replace("-py", "")
        cmd_bash = cmd_bash.replace("--report_bleu", "")
        cmd_bash = cmd_bash.replace("-verbose", "")
        cmd_bash = cmd_bash.replace("grep pt", "grep t7")
        cmd_bash = cmd_bash.replace(".py", ".lua")
        cmd_bash = cmd_bash.replace("--report_bleu", "")
        cmd_bash = cmd_bash.replace("-encoder_type brnn -data ${pathed}data/test", "-encoder_type brnn -data ${pathed}data/test-train.t7")
        cmd_bash = cmd_bash.replace("--keep_checkpoint 1", "")
        cmd_bash = cmd_bash.replace("-shard_size 25000", "")
        cmd_bash = cmd_bash.replace("--src_seq_length", "-src_seq_length")
        cmd_bash = cmd_bash.replace("--tgt_seq_length", "-tgt_seq_length")
        cmd_bash = cmd_bash.replace("-world_size 1 -gpu_ranks 0", "-gpuid 1")
        cmd_bash = cmd_bash.replace("-gpu 0", "-gpuid 1")
        cmd_bash = cmd_bash.replace("-max_generator_batches $max_generator_batches -start_decay_steps $steps_checkpoint -decay_steps $steps_checkpoint", "")
        cmd_bash = cmd_bash.replace("-save_checkpoint_steps $steps_checkpoint", "-save_validation_translation_every 1 -save_every_epochs 1   -validation_metric bleu")
        cmd_bash = cmd_bash.replace("-train_steps $train_steps -valid_steps $val_steps", "")
        cmd_bash = cmd_bash.replace("-batch_size $batch_size -valid_batch_size $batch_val_size", "")

    #
    epochs = 10
    batch_size = 96
    batch_translate_size = 256
    batch_val_size = 96
    train_steps = math.ceil(len(train_inputs) / batch_size) * epochs
    steps_checkpoint = math.ceil(len(train_inputs) / batch_size)  # len(train_inputs)
    val_steps = math.ceil(
        len(train_inputs) / batch_size)  # perform validation each...math.ceil(len(val_inputs)/batch_val_size)
    max_generator_batches = max_input
    feats = "--feat_merge concat " if args.with_traits else ""
    if cmd == "_lua":
        feats = "-feat_merge concat"
    cmd_bash = cmd_bash.replace("$model_dir", os.path.join(fullpath, model_dir))
    cmd_bash = cmd_bash.replace("$batch_size", batch_size.__str__())
    cmd_bash = cmd_bash.replace("$SRC_SEQ_LENGTH", max_input.__str__())
    cmd_bash = cmd_bash.replace("$TGT_SEQ_LENGTH", max_output.__str__())
    cmd_bash = cmd_bash.replace("$feats", feats.__str__())
    cmd_bash = cmd_bash.replace("$train_steps", train_steps.__str__())
    cmd_bash = cmd_bash.replace("$batch_val_size", batch_val_size.__str__())
    cmd_bash = cmd_bash.replace("$val_steps", val_steps.__str__())  # perform validation each...
    cmd_bash = cmd_bash.replace("$batch_translate_size", batch_translate_size.__str__())
    cmd_bash = cmd_bash.replace("$steps_checkpoint", steps_checkpoint.__str__())
    cmd_bash = cmd_bash.replace("$max_generator_batches", max_generator_batches.__str__())

    sys.stdout = old_stout

    ftxt = open(model_dir + '/cmd' + cmd + '.bash', 'w')
    ftxt.write(cmd_bash)
    ftxt.close()

    while not ftxt.closed:
        True
    print("fichier cmd :")
    print("bash " + model_dir + '/cmd' + cmd + '.bash')

    ftxt = open(model_dir + '/cmd_emilie' + cmd + '.bash', 'w')
    ftxt.write(cmd_bash.replace("colineem/data", "emilie").replace("colineem", "emilie").replace("emilie/Dropbox",
                                                                                                 "emilie/oldDropbox/Dropbox"))
    ftxt.close()
    while not ftxt.closed:
        True
    print("fichier cmd :")
    print("bash " + model_dir + '/cmd_emilie' + cmd + '.bash')

if args.log:
    print("outputs ok", file=flog)
    print("Local time:", time.ctime(time.time()), file=flog)
    flog.flush()
    flog.close()
    while not flog.closed:
        True

if args.debug:
    snapshot = tracemalloc.take_snapshot()
    display_top(snapshot)

os.kill(os.getpid(), signal.SIGKILL)
sys.exit(0)

# cd ~/OpenNMT-py/
# pathed="/home/colineem/Dropbox/local/git/analyseurs/corpusGenerator/génération/features_debug/buildCoNLL-U/"
# mkdir ${pathed}data
# mkdir ${pathed}logs
# # uncomment to preprocess
# python3 preprocess.py -train_src ${pathed}train_inputs.txt -train_tgt ${pathed}train_outputs.txt -valid_src ${pathed}val_inputs.txt -valid_tgt ${pathed}val_outputs.txt -save_data ${pathed}data/test -max_shard_size 2048000
#
#
# cd ~/OpenNMT-py/
#
# python3 train.py -encoder_type brnn -data ${pathed}data/test -save_model ${pathed}test-model -train_steps 1400000 -valid_steps 140000  -batch_size 96 -valid_batch_size 96  -world_size 1 -gpu_ranks 0 -save_checkpoint_steps 140000  -max_generator_batches 70 -start_decay_steps 700000 -decay_steps 140000 -log_file ${pathed}logs/log.txt
#
# python3 translate.py -model ${pathed}test-model_step_1400000.pt -src ${pathed}test_inputs.txt -output ${pathed}pred.txt -verbose  -batch_size 256 -log_file ${pathed}pred.log -gpu 0
