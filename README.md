Ce projet vise à constituer un corpus d'apprentissage pour la langue française.

# decrypteur #
Permet de passer des analyses grew/talismane/stanford à une analyse globale des phrases sur base du lvf (http://rali.iro.umontreal.ca/LVF+1/documents/PresentationLVF_JF_DLP_DL.pdf, consulté le 3/1/2019) pour repérer évènements, structures des évènements, arguments, structures des arguments.

## structure des fichiers de sortie ##
analyse.py crée un lot de fichiers d'après données issues du preparsing.
#### arbres/simple ####
Les phrases sorties sont par ordre de traitement. 
#### arbres/random ####
Les phrases sorties sont randomisées, rangées en slot de 1/10 des données, dans dix fichiers. Le reliquat des phrases (nombre de phrases non multiple de dix) est rangé de le premier fichier.
### les données sont enregistrées au format json ###
Modèle:
{"[sentence]": 
    {
        titre:"[Nom du livre Gutemberg dont la phrase est tirée]"
        auteur:"[Auteur du livre]"
        source:"[nom du fichier gutemberg d'origine]"
        first_tree:"[exemple d'arbre créé d'après les évènements ex:T1400 souhaiter ( sujet … 'obj', '8', 'obj']) ) ]"
        events:{…}
        analyses:{…}
    }
 }

# preparsing #
Génère les sorties (dossier output) vers des json : Phrases gutemberg analysées, dictionnaire de la forme:
Pour chaque phrase 
 * une entrée sentence --> la phrase
 * talismane -> analyse talismane
 * grew -> analyse grew
 * stanford -> analyse stanford.
 Les données sont transmissibles sur demande. Deux lots: phrases composées de 70 mots ou moins et toutes phrases à l'analyse interparseurs convergeante (cf preparsing/tableaux.pdf pour les explications d'exclusion et preparsing/analyse.py pour le code).  

## détails des fichiers
getBooks.py : se connecte sur gutemberg et récupère les livres en français.
separateur.py : génère un corpus de phrases françaises à partir de livres théoriquement en français au format texte (gère encodage, non accentuation, et type d'enregistrement (fichier binaire ou non)
parse.py : lance les analyses des sorties de trois parseurs classiques : talismane, grew, stanford. 
analyze.py : explore les données de travail,  aligne les analyses des trois parseurs et sort à des fins de redirection vers fichier tex un tableau d'analyses des données traitées.
categoriser.py : génère les données d'analyse touchant aux verbes, permettant l'exclusion des phrases où des verbes socle n'ont pas été correctement codifiés.
builder.py : génère les fichiers de travail fonction des résultats d'analyses (correspondance inter-parseurs et verbes).


## les informations si après sont plus de l'aide au développement, gardé en tant que trace ##
cd preparsing/tools

#### lancement analyse talismane : ####
(cd talismane-distribution-5.1.2-bin/ && java -Xmx1G -Dconfig.file=talismane-fr-5.0.4.conf -jar talismane-core-5.1.2.jar --analyse --sessionId=fr --encoding=UTF8 --inFile=data/frTest.txt --outFile=data/frTest.tal)

###### le fichier généré est à éditer. ######
###### --> prep à remplacer par obj.p ######
###### --> étiquettes première colonne nature mot : juste les initiales sauf P+D et PONCT ######
###### --> 2 dernières colonnes à videer pour des underscores ######
###### enregister sous test.surf.conll ######
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i talismane-distribution-5.1.2-bin/data/test.surf.conll -o test_talismane.ud2

#### conversion talismane ud2 ####
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i POStoSSQ/test.surf.conll -o test_grew.ud2

#### analyse grew : on envoie du texte vers melt
cat stanford-corenlp-full-2018-02-27/test.txt | MElt -T -L > POStoSSQ/test.melt 
grew transform -grs "POStoSSQ/grs/surf_synt_main.grs" -i POStoSSQ/test.melt -o test.surf.conll
##### conversion grew ud2 #####
grew transform -grs SSQ_UD/grs/ssq_to_ud/main.grs -i POStoSSQ/test.surf.conll -o test_grew.ud2

#### analyse stanford ####
( cd stanford-corenlp-full-2018-02-27 && java -mx8g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP -outputFormat conllu -file test.txt  -props StanfordCoreNLP-french.properties)
( cd stanford-corenlp-full-2018-02-27 && java -mx8g -cp '*'  edu.stanford.nlp.pipeline.StanfordCoreNLP   -annotators tokenize,ssplit,pos,lemma,ner,parse,depparse,udfeats  -props StanfordCoreNLP-french.properties -outputFormat conllu -file test.txt)
