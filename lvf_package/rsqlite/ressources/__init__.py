#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = "0.0.184"
__local_name__ = "ressources"
__database__ = "_edict.db"
OLD_DATABASES = ["alles.db", "ressources.db", "dictionnaire.db", "_data.db", "_ressources.db", "edic.db", "_edic.db"]
assert (__database__ not in OLD_DATABASES)

def __load__():
    from tqdm import tqdm
    import tempfile
    import shutil
    import requests
    import os
    import sys
    import zipfile
    import sqlite3
    import unidecode
    import re

    from xml.etree import ElementTree
    if __local_name__ in sys.modules.keys():
        data_dir =  os.path.dirname(sys.modules[__local_name__].__file__)
        data_dir += "/data"
    else:
        data_dir = "./data"
    for database in OLD_DATABASES:
        try:
            if os.path.exists(data_dir+"/"+database):
                os.remove(data_dir+"/"+database)
        except:
            print("Database changed, please on command line (linux) launch 'sudo -H Dem \"a priori\"' or other words ; simple users have not rights on your installation.", file=sys.stderr)
            if not os.path.exists(data_dir + "/" + __database__) or data_dir == "./data":
                exit(1)


    if not os.path.exists(data_dir+"/" + __database__) or data_dir == "./data"  :
        dirpath = tempfile.mkdtemp()
        print("First launch, download data.", file=sys.stderr)
        try:
            # ... do stuff with dirpath
            #print(dirpath)
            if not os.path.exists(data_dir+ "/dubois.xml"):
                url = "http://rali.iro.umontreal.ca/LVF+1/dubois.xml.zip"
                response = requests.get(url, stream=True)

                with open(dirpath+"/dubois.xml.zip", "wb") as handle: #dirpath+"/dubois.xml.zip"
                    for data in tqdm(response.iter_content(), unit="B", unit_scale=True, miniters=1):
                        handle.write(data)

                zip_ref = zipfile.ZipFile(dirpath+"/dubois.xml.zip", 'r')
                zip_ref.extractall(data_dir)
                zip_ref.close()
            print("lvf on the road.", file=sys.stderr)

            if not os.path.exists(data_dir + "/DEM-1_0.xml"):
                url = "http://rali.iro.umontreal.ca/DEM/DEM-1_0.xml.zip"
                response = requests.get(url, stream=True)

                with open(dirpath+"/dem.xml.zip", "wb") as handle: #dirpath+"/dubois.xml.zip"
                    for data in tqdm(response.iter_content(), unit="B", unit_scale=True, miniters=1):
                        handle.write(data)

                zip_ref = zipfile.ZipFile(dirpath+"/dem.xml.zip", 'r')
                zip_ref.extractall(data_dir)
                zip_ref.close()

            print("dem on the road.", file=sys.stderr)

            zip_ref = zipfile.ZipFile(data_dir+"/all.zip", 'r')
            zip_ref.extractall(data_dir)
            os.rename(data_dir+"/all.db", data_dir+"/"+__database__)
            zip_ref.close()
            print("morphalou on the road.", file=sys.stderr)

            print("store lvf data", file = sys.stderr)
            global __lemmes_e
            __lemmes_e = {}
            __h_aspirés_ = (
                "habanera", "hâb", "hâblerie", "hâbleu", "hach", "hachisch", "hachoir", "hachure", "hack", "hackeur_-euse",
                "hacquebute", "haquebute", "hacquebutier", "haquebutier", "hada", "haddock", "hadîth", "hadj", "hadji",
                "HADOPI",
                "haguais", "Haguai", "Hague", "hagard", "ha_ha", "haha", "hahé", "haie", "haïe", "haïk", "haillon", "haine",
                "haine",
                "haineusement", "haïr", "haïssable", "halage", "halbran", "HALDE", "hâle", "halecret", "haler", "hâler",
                "haleter",
                "halètement", "hall", "halle", "hallebarde", "hallebardier", "hallier", "hallstatien", "halo", "haloir",
                "hâloir",
                "halophile", "halot", "halte", "halva", "hamac", "hamada", "hamal", "hammal", "Hambourg", "hamburger",
                "hameau",
                "hammal", "hamal", "hammam", "Hammerfest", "hammerless", "hampe", "hamster", "han", "hanap", "hanche",
                "hanchement",
                "hancher", "hand", "handball", "handball", "handballeu", "handicap", "hangar", "hanneton", "hanse",
                "hanséatique",
                "hant", "happe", "happelourde", "happ", "happening", "happement", "happy-end", "happy_end", "haquebute",
                "hacquebute",
                "haquebutier", "hacquebutier", "haquenée", "haquet", "hara-kiri", "harangue", "haranguer", "harangueu",
                "haras",
                "harassant", "harasser", "harassement", "harc", "harcèlement", "harceleu", "hachich", "Harald", "hard",
                "hardes",
                "hardi", "hardiesse", "hardiment", "hardware", "harem", "hareng", "harengère", "haret", "harfang", "hargne",
                "hargne",
                "hargneusement", "haricot", "haricoter", "haridelle", "harissa", "harka", "harki", "harle", "harlou",
                "harnach",
                "harnais", "harnois", "Harold", "haro", "harp", "Harry", "hart", "Harvard", "hasard", "hasarde", "has-been",
                "haschich",
                "hase", "hast", "hastaire", "haste", "Hastings", "hât", "hauban", "haubergeon", "haubert", "hauss", "haut",
                "hautain",
                "hautai", "hautbois", "haut-de-chausses", "haut-de-forme", "haute-contre", "haute-forme", "hautement",
                "hautesse",
                "hauteur", "hautes-contre", "hautes-formes", "haut-fond", "hautin", "haut-le-cœur", "haut-le-corps",
                "haut-le-pied",
                "haut-parleur", "haut-parleurs", "haut-relief", "hauts-de-chausses", "hauts-de-forme", "hauts-fonds",
                "hauts-reliefs",
                "hauturi", "havage", "havanais", "Havanai", "havane", "hâve", "haveneau", "havenet", "hav", "haveu",
                "havir", "havrais",
                "Havrai", "havre", "havresac", "havre-sac", "havresacs", "havre-sacs", "hayon", "hé", "heaume", "heaumier",
                "hé_bien",
                "heimatlos", "hein", "hélas", "hél", "hèl", "hel", "hem", "hum", "hemloc", "hemlock", "henné", "hennir",
                "hennissant",
                "hennissement", "hennisseu", "Henri", "henry", "Henry", "hep", "héraut", "herchage", "herschage", "herch",
                "hersch",
                "herche", "herscheu", "hère", "hériss", "hermitique", "herniaire", "hernie", "hernie", "héron", "héronni",
                "héros",
                "herschage", "herchage", "hersch", "herch", "hersche", "hercheu", "herse", "herser", "hertz", "hertzien",
                "Hesse",
                "hêtraie", "hêtre", "heu", "heulandite", "heurt", "heurtement", "heurtequin", "heurte", "heurteu",
                "heurtoir", "hi",
                "hiata", "hibou", "hic", "hic_et_nunc", "hickory", "hideur", "hideusement", "hide", "hie", "hiement",
                "hier",
                "hiéracocéphale", "hiérarch", "hiératique", "hiératiquement", "hiératisant", "hiératisé", "hiératisme",
                "hiérochromie",
                "hiérocrate", "hiérocratisme", "hiérodrame", "hiérogamie", "hiérogamique", "hiéroglyphe", "hiéroglyphé",
                "hiéroglyphie",
                "hiéroglyphié", "hiéroglyphique", "hiéroglyphiquement", "hiéroglyphisme", "hiéroglyphite", "hiérogramme",
                "hiérogrammate", "hiérogrammatisme", "hiérographe", "hiéromancie", "hiéromoine", "hiérophanie",
                "hiéroscopie",
                "hiéroscopique", "hi-fi", "highlandai", "Highlander", "Highlands", "high-life", "highlifer", "highlifeur",
                "hi-han",
                "hi_han", "hilaire", "hile", "hiloire", "Hilbert", "Hildegarde", "hindi", "hindî", "hip_hip_hip", "hip-hop",
                "hippie",
                "hipp", "hiragana", "Hiroshima", "hiss", "hit", "hit-parade", "hit-parades", "hittite", "Hittite", "HLM",
                "Hobart",
                "hobb", "hobereau", "hobereautaille", "hoberelle", "hoc", "hoca", "hocco", "hocko", "hoche", "hochement",
                "hochepot",
                "hochequeue", "hoche-queue", "hochequeues", "hoche-queues", "hoch", "hochet", "hockey", "hockeyeu", "hocko",
                "hocco",
                "hodja", "hoffmannesque", "hoffmannien", "hognement", "hogn", "holà", "holding", "hôl", "hold-up",
                "hollandais",
                "Hollandai", "hollandaisement", "hollande", "Hollande", "hollandé", "hollandis", "hollando-belge",
                "hollando-françai",
                "hollando-norvégien", "hollando-saxon", "Hollywood", "hollywoodesque", "hollywoodien", "homard",
                "homarderie",
                "homardier", "home", "home-cinema", "homespun", "hon", "Honduras", "hondurien", "Hondurien", "Hongkong",
                "Hongkongai",
                "hongre", "hongreline", "hongrer", "hongreu", "Hongrie", "hongrois", "Hongrois", "hongroyage", "hongroy",
                "honnir",
                "honnissement", "Honshu", "honte", "honte", "honteusement", "hooligan", "hop", "hoquet", "hoquèt",
                "hoqueton", "horde",
                "horion", "hormis", "hornblende", "hors", "horsain", "hors-bord", "hors-bords", "hors-caste", "hors-castes",
                "hors-d’œuvre", "hors_d’œuvre", "horseguard", "horse-guard", "horseguards", "horse-guards", "horse-pox",
                "hors-jeu",
                "hors-la-loi", "hors-série", "horst", "hors-texte", "hosanna", "hosannah", "hosannière", "hotdog",
                "hot-dog", "hotdogs",
                "hot-dogs", "hotte", "hottée", "hott", "hottentot", "hotteu", "hou", "houblon", "houblonneu",
                "houblonnière", "houdan",
                "Houdan", "houe", "houhou", "houill", "hourdi", "hourdis", "houret", "houri", "hourque", "hourra_ouhurra",
                "hourvari",
                "houseau", "houspill", "houss", "Houston", "houx", "hoyau", "huard", "hublot", "huche", "huchée", "huche",
                "huchet",
                "huchier", "hue", "huée", "huerta", "huehau", "Hugo", "hugolâtre", "hugolâtrie", "hugolien", "hugotique",
                "hugotisme",
                "Hugues", "huguenot", "huitain", "huitaine", "huitante", "huitième", "Hulk", "hulotte", "hulul", "hum",
                "humage",
                "humement", "hum", "Hun", "humot", "hune", "hunier", "hunter", "huppe", "huppé", "huque", "hure", "hurl",
                "huro-iroquois", "Huro-iroquois", "huron", "Huron", "Huronien", "huronien", "hurricane", "husk", "hussard",
                "hussite",
                "hussitisme", "hutin", "hutinet", "hutt")
            global __h_aspirés
            __h_aspirés = set()
            for h in __h_aspirés:
                __h_aspirés.add(h.lower())
            del __h_aspirés_

            def elisionAvailable(lemme: str) -> bool:
                liste_voyelles = ["a", "e", "i", "o", "u"]#, "y"]
                if lemme == "":
                    raise Exception
                global __lemmes_e, __h_aspirés
                if not lemme in __lemmes_e.keys():
                    __lemmes_e[lemme] = False
                    if lemme == "y":
                        __lemmes_e[lemme] = True
                        return __lemmes_e[lemme]
                    if not lemme[0].upper() in ("B", "C", "Ç", "D", "F", "G", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Z", "*", "*", "-"):
                        for h_aspiré in __h_aspirés:
                            if lemme.lower().startswith(h_aspiré):
                                __lemmes_e[lemme] = False
                                return False
                        if lemme[0].lower() == "h":
                            __lemmes_e[lemme] = True
                        elif unidecode.unidecode(lemme[0].lower()) in liste_voyelles:
                            __lemmes_e[lemme] = True
                return __lemmes_e[lemme]



            sql_file = data_dir+"/" + __database__

            # on va récuperer les phrases
            fichier = open(data_dir+ "/dubois.xml", "r")
            phrases_texte = [ligne.strip() for ligne in fichier if "<phrase>" in ligne]
            fichier.close()

            lvf = {}
            lvf_état_fini = {}

            root = ElementTree.parse(data_dir+ "/dubois_parsed.xml").getroot()
            if not data_dir == "./data":
                os.remove(data_dir+ "/dubois.xml")
            if not data_dir == "./data":
                os.remove(data_dir+ "/dubois_parsed.xml")
            clean = re.compile('<.?phrase>')
            domaines = []
            for elt in root:
                mot = elt.attrib['mot']
                print("store " + mot, file=sys.stderr)
                nb = elt.attrib['nb']
                id = elt.attrib['id']

                lvf[mot] = {}
                infos = []

                entrees = elt.findall("entree")
                for entree in entrees:
                    lvf[mot][entree.find("mot").attrib['no']] = {'domaine': entree.find("domaine").attrib["nom"],
                                                                 'constructions': {},
                                                                 'phrases': []}
                    if lvf[mot][entree.find("mot").attrib['no']]['domaine'] not in domaines:
                        domaines.append(lvf[mot][entree.find("mot").attrib['no']]['domaine'])
                    lvf[mot][entree.find("mot").attrib['no']]["conjugaison"] = entree.find("conjugaison").attrib
                    lvf[mot][entree.find("mot").attrib['no']]["classe"] = entree.find("classe").attrib
                    lvf[mot][entree.find("mot").attrib['no']]["classe"]['valeur'] = entree.find("classe").text
                    if entree.find("operateur").find("predicat") is None:
                        lvf[mot][entree.find("mot").attrib['no']]["prédicat"] = ""
                    else:
                        lvf[mot][entree.find("mot").attrib['no']]["prédicat"] = \
                        entree.find("operateur").find("predicat").attrib['desc']
                    lvf[mot][entree.find("mot").attrib['no']]["sens"] = entree.find("sens").text
                    constructions = entree.findall("constructions/scheme")
                    phrases = entree.findall("phrases/phrase")
                    for phrase in phrases:
                        lvf[mot][entree.find("mot").attrib['no']]['phrases'].append(
                            re.sub(clean, '', phrases_texte.pop(0)).strip())

                    for scheme in constructions:
                        lvf[mot][entree.find("mot").attrib['no']]['constructions'][scheme.attrib["origine"]] = scheme.attrib
                        lvf[mot][entree.find("mot").attrib['no']]['constructions'][scheme.attrib["origine"]]["arguments"] = {}
                        __arguments = scheme.findall("argument")
                        for __argument in __arguments:
                            lvf[mot][entree.find("mot").attrib['no']]['constructions'][scheme.attrib["origine"]][
                                "arguments"][__argument.attrib['type']] = __argument.attrib['value']

                        code = scheme.text
                    derivation = entree.find("derivation")
                    lvf[mot][entree.find("mot").attrib['no']]['derivation'] = derivation.attrib

            connexion = sqlite3.connect(sql_file)
            connexion.execute("PRAGMA foreign_keys = 1")  # Récupération d'un curseur
            curseur = connexion.cursor()  # Récupération d'un curseur
            # Exécution unique
            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_domaines (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                domaine TEXT
            )''')

            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_phrases (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                verbe_id INTEGER,
                phrase TEXT
            )''')

            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_natures
            (code            TEXT   PRIMARY KEY,
            value        TEXT)''')

            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_prépositions
            (code            TEXT   PRIMARY KEY,
            value        TEXT)''')

            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_compléments
            (code            TEXT   PRIMARY KEY,
            value        TEXT)''')

            t_dom = {}
            for domaine in domaines:
                __a = curseur.execute('''INSERT INTO lvf_domaines (domaine) VALUES (?);''', (domaine,))
                t_dom[domaine] = __a.lastrowid

            natures = [(1, 'humain',), \
                       (2, 'animal',), \
                       (3, 'chose',), \
                       (4, 'complétive ou chose',), \
                       (5, 'complétive ou inf',), \
                       (7, 'pluriel humain',), \
                       (8, 'pluriel chose',), \
                       (9, 'humain ou chose',)]
            curseur.executemany('INSERT INTO lvf_natures VALUES (?, ?)', natures)

            prépositions = [('a', 'à',), \
                            ('b', 'de',), \
                            ('c', 'avec',), \
                            ('d', 'contre',), \
                            ('e', 'par',), \
                            ('g', 'sur, vers',), \
                            ('i', 'de',), \
                            ('j', 'dans',), \
                            ('k', 'pour',), \
                            ('l', 'auprès',), \
                            ('m', 'devant',), \
                            ('n', 'divers mouvements (ie cheminer le long de)',), \
                            ('q', 'pour',)]
            curseur.executemany('INSERT INTO lvf_prépositions VALUES (?, ?)', prépositions)

            compléments = [(1, 'locatif (où l’on est)',), \
                           (2, 'locatif de destination',), \
                           (3, 'locatif d’origine',), \
                           (4, 'double locatif',), \
                           (5, 'temps',), \
                           (6, 'modalité (manière, mesure, quantité)',), \
                           (7, 'cause',), \
                           (8, 'instrumental, moyen',)]
            curseur.executemany('INSERT INTO lvf_compléments VALUES (?, ?)', compléments)

            # Exécution unique
            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                verbe TEXT,  
                domaine TEXT,  
                construction TEXT,  
                groupe INT,
                sousgroupe TEXT,
                auxiliaire TEXT,
                classe TEXT,
                classe_sem_synt TEXT,
                constr_synt TEXT,
                valeur_sem_synt TEXT,
                prédicat TEXT,
                sens TEXT,
                type TEXT,
                sujet TEXT,
            objet TEXT,
            préposition TEXT,
            circonstant TEXT)''')

            curseur.execute('''CREATE TABLE IF NOT EXISTS lvf_derivation (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                verbe_id TEXT,  
                base TEXT,  
                derivation TEXT,  
                value TEXT)''')


            for key in lvf.keys():

                for item in lvf[key]:
                    verbe = key
                    domaine = t_dom[lvf[key][item]['domaine']]
                    groupe = lvf[key][item]['conjugaison']['groupe']

                    sousgroupe = lvf[key][item]['conjugaison']['sous-groupe'] if 'sous-groupe' in lvf[key][item][
                        'conjugaison'].keys() else ""
                    auxiliaire = lvf[key][item]['conjugaison']['auxiliaire'] if 'auxiliaire' in lvf[key][item][
                        'conjugaison'].keys() else ""
                    classe = lvf[key][item]['classe']['generique']
                    classe_sem_synt = lvf[key][item]['classe']['semantico-syntaxique']
                    constr_synt = lvf[key][item]['classe']['construction-syntaxique']
                    valeur_sem_synt = lvf[key][item]['classe']['valeur']
                    prédicat = lvf[key][item]['prédicat']
                    sens = lvf[key][item]['sens']
                    dérivations = lvf[key][item]['derivation']
                    for construction in lvf[key][item]['constructions'].keys():
                        type_ = lvf[key][item]['constructions'][construction]['type']
                        sujet = lvf[key][item]['constructions'][construction]['arguments']['sujet'] if 'sujet' in \
                                                                                                       lvf[key][item][
                                                                                                           'constructions'][
                                                                                                           construction][
                                                                                                           'arguments'].keys() else ""
                        objet = lvf[key][item]['constructions'][construction]['arguments']['objet'] if 'objet' in \
                                                                                                       lvf[key][item][
                                                                                                           'constructions'][
                                                                                                           construction][
                                                                                                           'arguments'].keys() else ""
                        préposition = lvf[key][item]['constructions'][construction]['arguments'][
                            'préposition'] if 'préposition' in \
                                              lvf[key][item]['constructions'][construction]['arguments'].keys() else ""
                        circonstant = lvf[key][item]['constructions'][construction]['arguments'][
                            'circonstant'] if 'circonstant' in \
                                              lvf[key][item]['constructions'][construction]['arguments'].keys() else ""

                        __a = curseur.execute('''INSERT INTO lvf ( verbe, domaine, construction, groupe, sousgroupe, auxiliaire,
                         classe, classe_sem_synt, constr_synt, valeur_sem_synt, prédicat, sens, type, sujet, objet, préposition, 
                         circonstant) 
                         VALUES (?,  ?, ?,  ?, ?,  ?, ?,  ?, ?,  ?, ?,  ?, ?,  ?, ?,  ?, ?);''',
                                            (verbe, domaine, construction,
                                             groupe, sousgroupe, auxiliaire,
                                             classe, classe_sem_synt,
                                             constr_synt, valeur_sem_synt,
                                             prédicat, sens, type_,
                                             sujet, objet, préposition,
                                             circonstant,))
                        id = __a.lastrowid
                        for dérivation in dérivations.keys():
                            __a = curseur.execute('''INSERT INTO  lvf_derivation (verbe_id, base, derivation, value ) 
                                 VALUES (?,  ?, ?, ?);''', (id, verbe, dérivation, dérivations[dérivation],))
                        for phrase in lvf[key][item]['phrases']:
                            __a = curseur.execute('''INSERT INTO  lvf_phrases (verbe_id, phrase ) 
                                                         VALUES (?,  ?);''',
                                                (id, phrase,))


            del lvf

            root = ElementTree.parse(data_dir + "/DEM-1_0.xml").getroot()
            if not data_dir == "./data":
                os.remove(data_dir + "/DEM-1_0.xml")

            curseur.execute('''CREATE TABLE IF NOT EXISTS dem (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                mot TEXT,  
                mot_init TEXT,  
                number INTEGER,  
                contenu TEXT,  
                domaine TEXT,  
                OP TEXT,  
                OP1 TEXT,  
                sens TEXT,  
                categorie TEXT,  
                type TEXT,  
                genre TEXT,  elision TEXT)''')

            for elt in root:
                entrees = elt.findall("entree")
                for entree in entrees:
                        M = entree.find("M")
                        mot = M.attrib['mot']
                        mot_init = M.attrib['mot-initial'] if "mot-initial" in M.attrib.keys() else ""
                        number = M.attrib['no'] if "no" in M.attrib.keys() else ""
                        CONT = entree.find("CONT").text
                        DOM = entree.find("DOM").attrib['nom']
                        OP = entree.find("OP")
                        if OP is not None:
                            OP = OP.text
                        else:
                            OP = ""
                        SENS = entree.find("SENS")
                        if SENS is not None:
                            SENS = SENS.text
                        else:
                            SENS = ""
                        OP1 = entree.find("OP1")
                        if OP1 is not None:
                            OP1 = OP1.text
                        else:
                            OP1 = ""
                        CA = entree.find("CA")
                        categorie = CA.attrib['categorie'] if "categorie" in CA.attrib.keys() else ""
                        type = CA.attrib['type'] if "type" in CA.attrib.keys() else ""
                        genre = CA.attrib['genre'] if "genre" in CA.attrib.keys() else ""

                        __a_tuple = (mot, mot_init, number, CONT, DOM, OP, OP1, SENS, categorie, type, genre, elisionAvailable(mot).__str__())
                        __a = curseur.execute('''INSERT INTO dem ( mot , mot_init ,  number ,  contenu ,  domaine ,  OP ,  OP1 ,  sens ,  categorie ,  type ,  genre, elision ) 
                         VALUES (? , ? , ? , ? , ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ?, ?  );''',
                                            __a_tuple)

            print("All is treated")

            __a = curseur.execute("""CREATE INDEX wdForm ON word_details(form);""")
            __a = curseur.execute("""CREATE INDEX wdLemma ON word(lemma);""")
            __a = curseur.execute("""CREATE INDEX vForm ON verb_details(form);""")
            __a = curseur.execute("""CREATE INDEX vLemma ON verb(lemma);""")
            __a = curseur.execute("""CREATE INDEX dMot ON dem(mot);""")
            __a = curseur.execute("""CREATE INDEX `lidv` ON `verb_details` ( `verb_id` ASC )""")
            __a = curseur.execute("""CREATE INDEX `lidw` ON `word_details` ( `word_id` ASC )""")
            __a = curseur.execute("""CREATE INDEX wIndexverbe ON lvf(verbe);""")
            __a = curseur.execute("""CREATE INDEX wIndexdomaine ON lvf(domaine);""")
            __a = curseur.execute("""CREATE INDEX wIndexconstruction ON lvf(construction);""")
            __a = curseur.execute("""CREATE INDEX wIndexgroupe ON lvf(groupe);""")
            __a = curseur.execute("""CREATE INDEX wIndexsousgroupe ON lvf(sousgroupe);""")
            __a = curseur.execute("""CREATE INDEX wIndexauxiliaire ON lvf(auxiliaire);""")
            __a = curseur.execute("""CREATE INDEX wIndexclasse ON lvf(classe);""")
            __a = curseur.execute("""CREATE INDEX wIndexclasse_sem_synt ON lvf(classe_sem_synt);""")
            __a = curseur.execute("""CREATE INDEX wIndexconstr_synt ON lvf(constr_synt);""")
            __a = curseur.execute("""CREATE INDEX wIndexvaleur_sem_synt ON lvf(valeur_sem_synt);""")
            __a = curseur.execute("""CREATE INDEX wIndexprédicat ON lvf(prédicat);""")
            __a = curseur.execute("""CREATE INDEX wIndexsens ON lvf(sens);""")
            __a = curseur.execute("""CREATE INDEX wIndextype ON lvf(type);""")
            __a = curseur.execute("""CREATE INDEX wIndexsujet ON lvf(sujet);""")
            __a = curseur.execute("""CREATE INDEX wIndexobet ON lvf(objet);""")
            __a = curseur.execute("""CREATE INDEX wIndexsprep ON lvf(préposition);""")
            __a = curseur.execute("""CREATE INDEX wIndexcirc ON lvf(circonstant);""")

            __a = curseur.execute("""CREATE INDEX verb_details_form_mod ON verb_details (form, mode);""")
            __a = curseur.execute("""CREATE INDEX lvf_struc_aux ON lvf (construction, auxiliaire);""")

            connexion.commit()

            connexion.close()  # Déconnexion
        except Exception as ex:
            print(ex)
            sys.exit(1)
        shutil.rmtree(dirpath)

__load__()

from . import *

if __name__ != '__main__':
    from ressources.ressources import Word
    from ressources.ressources import DistinctWord
    from ressources.ressources import Lemma
    from ressources.ressources import GrammaticalCategory
    from ressources.ressources import Tense
    from ressources.ressources import Mood
    from ressources.ressources import Person
    from ressources.ressources import Number
    from ressources.ressources import Aspect
    from ressources.ressources import Mode
    from ressources.ressources import Temps
    from ressources.ressources import Elision
    from ressources.ressources import Source
    from ressources.ressources import POS
    from ressources.ressources import main
    from ressources.ressources import Information
    from ressources.ressources import Answer
    from ressources.ressources import database
    from ressources.features import *

