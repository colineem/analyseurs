#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Type

__all__ = ['Word', 'DistinctWord', 'Lemma', 'Information', 'POS', 'GrammaticalCategory', "Tense", "Mood", "Person", "Number", "Aspect",
           "Mode", "Temps", "Elision", "Source", "Answer", "database"]
__local_name__ = "ressources"

import sys
import os
import sqlite3
import atexit
import copy
import inspect

from enum import Enum
from collections.abc import Iterable

"""
    Implementation of word recognition .

    Usage:

    >>> 
    info = Information(grammaticalCategory=None, source=None)
    aime = info.get_words_items("aime") #Return Word.Word_Item objects
    

    >>> import ressources
    >>> Word("manger", GrammaticalCategory.commonNoun)
    >>> Word("manger", GrammaticalCategory.verb)
    >>> Word("manger", Source.dem)
    >>> Word("manger", Source.dem, GrammaticalCategory.verb)
    
    Information.get_forms(** lemma to search **) : Generator of dictionnary informing words sharing the same lemma.
    Information.get_rows(** word to search **) : Generator of dictionnary informating a word.
"""


if __name__ == '__main__':
    __database__ = "dictionnaire.db"
else:
    from ressources import __database__
    from ressources import __version__

class Filter(Enum):
    """
    Master class for filtering objects
    """

    def __subclasscheck__(self, cls):
        return hasattr(cls, 'text') and hasattr(cls, 'category')

class Answer(Enum):
    no = 0
    yes = 1
    ambiguous = 2
    not_found = 3
        
        
class POS(Filter):
    """
    Define POS for the Dictionnaire Électronique des Mots
    """
    adjective = 1  #: adjective
    adverb = 2  #: adverb
    conjunction = 3  #: conjunction
    interjection = 4  #: interjection
    nom = 5  #:
    verbe_intransitif = 6  #:
    verbe_pronominal = 7  #
    verbe_transitif = 8  #
    none = 9  # other

    @property
    def text(self):
        if self == POS.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "pos"


class GrammaticalCategory(Filter):
    """
    Grammatical category (morphalou-like categorization)
    """
    commonNoun = 1  #: adjective
    adjective = 2  #: adposition
    functionWord = 3  #: adverb
    adverb = 4  #: auxiliary
    interjection = 5  #:
    onomatopoeia = 6  #: determiner
    verb = 7  # interjection
    none = None

    @property
    def text(self):
        if self == GrammaticalCategory.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "grammaticalCategory"


class Tense(Filter):
    """
    Tenses of verbs
    """
    infinitive = 1
    present = 2
    past = 3
    future = 4
    none = None

    @property
    def text(self):
        if self == Tense.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "tense"


class Mood(Filter):
    """
    Mood of verbs
    """
    indicative = 1
    imperative = 2
    conditional = 3
    subjunctive = 4
    null = 5
    none = None

    @property
    def text(self):
        if self == Mood.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "mood"


class Person(Filter):
    """
    Person
    """
    person1 = 1
    person2 = 2
    person3 = 3
    null = 4
    none = None

    @property
    def text(self):
        if self == Person.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "person"


class Number(Filter):
    """
    Number
    """
    singular = 1
    plural = 2
    null = 3
    none = None

    @property
    def text(self):
        if self == Number.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "number"


class Aspect(Filter):
    """
    Aspect of verbs
    """
    imperfective = 1
    progressive = 2
    perfective = 3
    null = 4
    none = None

    @property
    def text(self):
        if self == Aspect.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "aspect"


class Mode(Filter):
    """
    French names for moods
    """
    infinitif = 1
    indicatif = 2
    participe = 3
    impératif = 4
    conditionnel = 5
    subjonctif = 6
    null = 7
    none = None

    @property
    def text(self):
        if self == Mode.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "mode"


class Temps(Filter):
    présent = 1
    imparfait = 2
    passé = 3
    passé_simple = 4
    futur = 5
    none = None

    @property
    def text(self):
        if self == Temps.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "temps"


class Elision(Filter):
    true = 1
    false = 2
    none = None

    @property
    def text(self):
        if self == Elision.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "elision"


class Source(Filter):
    """
    Source of data
    """
    morphalou = 1
    lvf = 2
    dem = 3
    none = None

    @property
    def text(self):
        if self == Source.none:
            return None
        else:
            return self.name

    @property
    def category(self):
        return "source"

class Word(object):
    """
    Word to describe
        """

    if __local_name__ in sys.modules.keys():
        _database = os.path.dirname(sys.modules[__local_name__].__file__)
        _database += "/data/" + __database__
    else:
        _database = "./data/" +  __database__
    __connVerbs = sqlite3.connect(_database, isolation_level=None)
    __connVerbs.execute("PRAGMA locking_mode = NORMAL")  # Récupération d'un curseur
    __curseurVerbs = __connVerbs.cursor()
    __open = True

    _equivPOS = {"A": POS.adjective, "Adv": POS.adverb, "Conj": POS.conjunction, "Interj": POS.interjection,
                 "N": POS.nom, "Vi": POS.verbe_intransitif, "Vp": POS.verbe_pronominal, "Vt": POS.verbe_transitif}
    _equivGram = {"A": GrammaticalCategory.adjective, "Adv": GrammaticalCategory.adverb,
                  "Conj": GrammaticalCategory.functionWord,
                  "Interj": GrammaticalCategory.interjection, "N": GrammaticalCategory.commonNoun,
                  "Vi": GrammaticalCategory.verb,
                  "Vp": GrammaticalCategory.verb, "Vt": GrammaticalCategory.verb}
    _equivGender = {"": "", "F": "feminine", "M": "masculine", "MF": "all"}

    def _curseur(self):

        if not Word.__open:
            Word.__connVerbs = sqlite3.connect(self._database, isolation_level=None)
            Word.__curseurVerbs = self.__connVerbs.cursor()
            Word.__open = True

        return Word.__curseurVerbs

    def __init__(self, form: str, *screens, verbose: bool = False):
        """Describe a word
    Keyword arguments:
    word -- the word to look for
    verbose -- the imaginary part (default False)
        :return void
        :rtype None
        """
        self._form = form
        self._verbose = verbose
        self._variations = None
        self._grammaticalCategory = GrammaticalCategory.none
        self._tense = Tense.none
        self._mood = Mood.none
        self._person = Person.none
        self._number = Number.none
        self._aspect = Aspect.none
        self._mode = Mode.none
        self._temps = Temps.none
        self._elision = Elision.none
        self._source = Source.none
        self._POS = POS.none

        for screen in screens:
            self.screen = screen

    def __getitem__(self, name):
        return getattr(self, name)

    def __setitem__(self, name, value):
        return setattr(self, name, value)

    def __delitem__(self, name):
        return delattr(self, name)

    def __contains__(self, name):
        return hasattr(self, name)

    @property
    def form(self):
        """Get the word define.
        :return the word defined
        :rtype str
        """
        return self._form

    @form.setter
    def form(self, form: str):
        """Set the word define.
        :param form: the word defined
        :type form: str
        """
        self._variations = None
        self._form = form

    @property
    def screen(self):
        """Get the screens of the object : it's the restrictions on outputs.
        :return the tuple ot active screens
        :rtype array of filters
        """
        return [k for k in (
        self._grammaticalCategory, self._tense, self._mood, self._person, self._number, self._aspect, self._mode,
        self._temps, self._elision, self._source, self._POS)]

    @screen.setter
    def screen(self, screen: Filter = None):
        """Set a screen for the object : it's a restriction on outputs.. None to remove the restriction.
        :param screen: filter
        :type screen: Filter
        :return: void
        :rtype:  None
        """
        assert (isinstance(screen, Filter) or screen is None)
        assert (issubclass(screen.__class__, Enum) or screen is None)
        if screen is not None:
            self["_" + screen.category] = screen

    @property
    def verbose(self):
        '''Verbose mode
        :return: Return the value of the verbose mode
        :rtype: int
        '''
        return self._verbose

    @verbose.setter
    def verbose(self, verbose: bool):
        """Set the verbose mode to the parameter value
        :param verbose:True or False
        :type verbose: bool
        :return:void
        :rtype:None
        """
        self._verbose = verbose

    class Word_Item():

        def __init__(self):
            self._aspect = None
            self._contenu = None
            self._domaine = None
            self._elision = None
            self._form = None
            self._gender = None
            self._grammaticalCategory = None
            self._lemma = None
            self._mode = None
            self._mood = None
            self._number = None
            self._OP = None
            self._OP1 = None
            self._person = None
            self._pos = None
            self._sens = None
            self._source = None
            self._temps = None
            self._tense = None
            self._type_objet = None

        @property
        def aspect(self):
            return self._aspect

        @property
        def contenu(self):
            return self._contenu

        @property
        def domaine(self):
            return self._domaine

        @property
        def elision(self):
            return self._elision

        @property
        def form(self):
            return self._form

        @property
        def gender(self):
            return self._gender

        @property
        def grammaticalCategory(self):
            return self._grammaticalCategory

        @property
        def lemma(self):
            return self._lemma

        @property
        def mode(self):
            return self._mode

        @property
        def mood(self):
            return self._mood

        @property
        def number(self):
            return self._number

        @property
        def OP(self):
            return self._OP

        @property
        def OP1(self):
            return self._OP1

        @property
        def person(self):
            return self._person

        @property
        def pos(self):
            return self._pos

        @property
        def sens(self):
            return self._sens

        @property
        def source(self):
            return self._source

        @property
        def temps(self):
            return self._temps

        @property
        def tense(self):
            return self._tense

        @property
        def type_objet(self):
            return self._type_objet
            
        def properties(self, active=True):
            """
            Get the word properties
            :param active: True only returns the values that are defined (None excluded)
            :return: properties of the word
            """
            return {k[1:]:v for k, v in self.__dict__.items() if v is not None or active == False}

        def __hash__(self):
            return hash((k.__str__() for k in (self._aspect, self._contenu, self._domaine,
                         self._elision, self._form, self._gender,
                         self._grammaticalCategory, self._lemma, self._mode,
                         self._mood, self._number, self._OP, self._OP1,
                         self._person, self._pos, self._sens, self._source,
                         self._temps, self._tense, self._type_objet) if k is not None))

        def __str__(self):
            return " ".join((k.__str__() for k in (self._aspect, self._contenu, self._domaine,
                         self._elision, self._form, self._gender,
                         self._grammaticalCategory, self._lemma, self._mode,
                         self._mood, self._number, self._OP, self._OP1,
                         self._person, self._pos, self._sens, self._source,
                         self._temps, self._tense, self._type_objet) if k is not None))

    def get_rows(self):
        """Restricted lines of variations for the rows
        :return: array of variations limited by screens
        :rtype: array
        """
        rowsWithConstraint = []

        for rw in self.variations:
            take = False
            for screen in (
            self._grammaticalCategory, self._tense, self._mood, self._person, self._number, self._aspect, self._mode,
            self._temps, self._elision, self._source, self._POS):
                take = True if rw[screen.category] == screen.text or screen.text is None else False
                if not take:
                    break
            if take:
                rowsWithConstraint.append(rw)

        return rowsWithConstraint

    def get_words_items(self):
        """
        Object word
        :return: yied Word object
        """
        for rw in self.get_rows():
            word = Word.Word_Item()
            for property in vars(word).keys():
                if  property[1:] in rw.keys():
                    vars(word)[property] = rw[property[1:]]
            yield word

    @property
    def variations(self):
        """
        Build variations if not done
        :return: array of variations
        :rtype: array
        """
        if self._variations is None:
            self._variations = []

            if not Word.__open:
                Word.__connVerbs = sqlite3.connect(self._database, isolation_level=None)
                Word.__curseurVerbs = self.__connVerbs.cursor()
                Word.__open = True

            if self.verbose:
                print(self._form, file=sys.stderr)
            try:

                req = "select * from word_details, word where word.id=word_details.word_id and form=:form"
                Word.__curseurVerbs.execute(req, {'form': self.form})
                rows = Word.__curseurVerbs.fetchall()

                for row in rows:
                    self._variations.append({'form': row[2], 'gender': row[3], 'number': row[4], 'lemma': row[6],
                                             'grammaticalCategory': row[7], 'elision': row[-1] == 'True',
                                             'source': 'morphalou'})

                req = "select * from verb_details, verb where verb.id=verb_details.verb_id and form=:form"
                Word.__curseurVerbs.execute(req, {'form': self.form})
                rows = Word.__curseurVerbs.fetchall()

                for row in rows:
                    if self.verbose:
                        print(row, file=sys.stderr)
                    self._variations.append({'form': row[2], 'tense': row[3], 'mood': row[4], 'person': row[5],
                                             'grammaticalCategory': "verb",
                                             "number": row[6], "aspect": row[7], "gender": row[8], "mode": row[9],
                                             "temps": row[10], 'lemma': row[13],
                                             'elision': row[11] == 'True', 'source': 'lvf'})

                req = """select mot, mot_init, number, contenu, domaine , OP , OP1 ,sens, categorie, type , genre, elision from dem where mot =:mot"""
                Word.__curseurVerbs.execute(req, {'mot': self.form})
                rows = Word.__curseurVerbs.fetchall()

                for row in rows:
                    if self.verbose:
                        print(row, file=sys.stderr)
                    mot = row[0]
                    mot_init = row[1]
                    number = row[2]
                    contenu = row[3]
                    domaine = row[4]
                    OP = row[5]
                    OP1 = row[6]
                    sens = row[7]
                    categorie = row[8]
                    type_objet = row[9]
                    genre = row[10]
                    elision = row[11]
                    lemma = row[0]

                    req = """select distinct lemma from word, word_details where word.id = word_details.word_id and form= :mot"""
                    Word.__curseurVerbs.execute(req, {'mot': self.form})
                    rows = Word.__curseurVerbs.fetchall()
                    if len(rows) == 1:
                        lemma = rows[0][0]

                    self._variations.append({'form': mot,
                                             'grammaticalCategory': Word._equivGram[categorie].name,
                                             'pos': Word._equivPOS[categorie].name,
                                             "contenu": contenu,
                                             "domaine": domaine,
                                             "OP": OP,
                                             "OP1": OP1,
                                             "sens": sens,
                                             "type_objet": type_objet,
                                             "gender": Word._equivGender[genre], 'lemma': lemma,
                                             'elision': elision == 'True', 'source': 'dem'})

                # update keys to have similar arrays
                for i_v in range(len(self._variations)):
                    for key in {'person', 'mood', 'mode', 'grammaticalCategory', 'tense', 'number', 'elision', 'form',
                                'gender', 'aspect', 'source', 'lemma', 'temps', "pos", "contenu", "domaine", "OP",
                                "OP1", "sens", "type_objet"}:
                        if key not in self._variations[i_v].keys():
                            self._variations[i_v][key] = None

            except Exception as ex:
                raise ex
        return self._variations

    def __iter__(self):
        """
        Generator for the word
        :return: one by one, all the variations inside the database
        :rtype: dict
        """
        for variation in self.variations:
            yield variation

class DistinctWord(object):


    _form: str

    def __init__(self, form: str, verb_authorized: bool=True, *screens):
        filtres = [screen for screen in screens]
        self._word = Word(form, *filtres)
        self._verb = verb_authorized
        self.__getInfo()

    def __getInfo(self):
        self._form = set()
        self._gender = set()
        self._grammatical_category = set()
        self._lemma = set()
        self._number = set()
        for variation in self._word.variations:
            if self._verb or (not self._verb and variation['grammaticalCategory'] !="verb"):
                if variation['form'] is not None:
                    self._form.add(variation['form'])
                if variation['gender'] is not None:
                    self._gender.add(variation['gender'])
                if variation['grammaticalCategory'] is not None:
                    self._grammatical_category.add(variation['grammaticalCategory' ])
                if variation['lemma'] is not None:
                    self._lemma.add(variation['lemma' ])
                if variation['number'] is not None:
                    self._number.add(variation['number' ])

    @property
    def form(self):
        r"""Get the form define.
        :return the form defined
        :rtype str
        """
        return self._form

    @property
    def gender(self):
        r"""Get the gender define.
        :return the gender defined
        :rtype str
        """
        return self._gender

    @property
    def grammatical_category(self):
        r"""Get the gender define.
        :return the gender defined
        :rtype str
        """
        return self._grammatical_category

    @property
    def lemma(self):
        r"""Get the gender define.
        :return the gender defined
        :rtype str
        """
        return self._lemma

    @property
    def number(self):
        r"""Get the gender define.
        :return the gender defined
        :rtype str
        """
        return self._number


class Lemma(object):

    __lemmas__ = {}

    def __init__(self, lemma: str, cached:bool=True):
        r"""Describe a lemma
    Keyword arguments:
    lemma -- the lemma to look for
        :return void
        :rtype None
        """
        self._lemma = lemma
        self._cached = cached

    @property
    def lemma(self):
        r"""Get the lemma define.
        :return the lemma defined
        :rtype str
        """
        return self._lemma

    @lemma.setter
    def lemma(self, lemma: str):
        r"""Set the lemma define.
        :param lemma: the lemma defined
        :type lemma: str
        """
        self._lemma = lemma

    @property
    def forms(self):
        r"""Get the forms for the lemma defined.
        :return the forms available
        :rtype array
        """
        req = """
select distinct form from word_details, word where word.id =  word_details.word_id and lemma = :mot
union
select distinct form from verb_details, verb where verb.id =  verb_details.verb_id and lemma = :mot
union 
select distinct mot as form from dem where mot = :mot
        """
        if self._cached and self.lemma in Lemma.__lemmas__.keys():
            return Lemma.__lemmas__[self.lemma]
        k = Word("")
        k._curseur().execute(req, {'mot': self.lemma, 'mot': self.lemma, 'mot': self.lemma})
        rows = k._curseur().fetchall()

        if self._cached:
            Lemma.__lemmas__[self.lemma] = [row[0] for row in rows]
            return Lemma.__lemmas__[self.lemma]
        return [row[0] for row in rows]

    def get_words_object(self):
        for form in self.forms:
            yield Word(form)

class LVF(object):

    def __init__(self):
        self._restrict = {}
        self._curseur = Word("")

    def get_verb(self, infinitive_form: str = None, domaine: str = None, construction: str = None, groupe: str = None,
                 sousgroupe: str = None, auxiliaire: str = None, classe: str = None, classe_sem_synt: str = None,
                 constr_synt: str = None, valeur_sem_synt: str = None, prédicat: str = None, sens: str = None,
                 type: str = None, sujet: str = None, objet: str = None, préposition: str = None,
                 circonstant: str = None):
        """
        Return lvf details about verb domains, structures...
        All parameters are free.
        :param infinitive_form: infinitive form of a verb
        :param domaine: domain of a verb
        :param construction:
        :param groupe:
        :param sousgroupe:
        :param auxiliaire:
        :param classe:
        :param classe_sem_synt:
        :param constr_synt:
        :param valeur_sem_synt:
        :param prédicat:
        :param sens:
        :param type:
        :param sujet:
        :param objet:
        :param préposition:
        :param circonstant:
        :return:
        """
        self._restrict = {}
        names_ = {}

        for variable, value in zip((infinitive_form, domaine, construction, groupe, sousgroupe, auxiliaire, classe,
                          classe_sem_synt, constr_synt, valeur_sem_synt, prédicat, sens, type, sujet, objet,
                          préposition, circonstant), ('infinitive_form', 'domaine', 'construction', 'groupe', 'sousgroupe', 'auxiliaire', 'classe', 'classe_sem_synt', 'constr_synt', 'valeur_sem_synt', 'prédicat', 'sens', 'type', 'sujet', 'objet', 'préposition', 'circonstant')):
            if variable is not None:
                if value == "infinitive_form":
                    names_["verbe"] = variable
                else:
                    names_[value] = variable

        for name in names_.keys():
            if names_[name] is not None:
                self._restrict[name] = names_[name]

        where = ""
        sep = 'where'
        for key in self._restrict.keys():
            where = sep + " " + key + "=:" + key
            sep = " and "
        req = "select id, verbe , domaine, construction, groupe, sousgroupe, auxiliaire, classe, classe_sem_synt, constr_synt, valeur_sem_synt, prédicat, sens, type, sujet, objet, préposition, circonstant from lvf " + where

        k = self._curseur._curseur().execute(req, self._restrict)

        rows = k.fetchall()
        columns = ('uniq_id', 'verbe ', 'domaine', 'construction', 'groupe', 'sousgroupe', 'auxiliaire', 'classe', 'classe_sem_synt', 'constr_synt', 'valeur_sem_synt', 'prédicat', 'sens', 'type', 'sujet', 'objet', 'préposition', 'circonstant')
        results = []
        for row in rows:
            results.append({key:value for key, value in zip(columns, row)})

        return results

    def get_domaine(self, code: str):
        """
        Domain for a code
        :param code of a domain
        :return: string or None if the code doesn't exist.
        """
        req = "select domaine from lvf_domaines where id=:id;"

        k = self._curseur._curseur().execute(req, {"id": code})

        rows = k.fetchall()
        if len(rows) == 0:
            return None
        else:
            return rows[0][0]

    def get_nature(self, code: str):
        """
        Nature for a code
        :param code of a nature
        :return: string or None if the code doesn't exist.
        """
        req = "select value from lvf_natures where code=:code;"

        k = self._curseur._curseur().execute(req, {"code": code})

        rows = k.fetchall()
        if len(rows) == 0:
            return None
        else:
            return rows[0][0]

    def get_preposition(self, code: str):
        """
        Nature for a code
        :param code of a nature
        :return: string or None if the code doesn't exist.
        """
        req = "select value from lvf_prépositions where code=:code;"

        k = self._curseur._curseur().execute(req, {"code": code})

        rows = k.fetchall()
        if len(rows) == 0:
            return None
        else:
            return rows[0][0]

    def get_complement(self, code: str):
        """
        Complement for a code
        :param code of a complement
        :return: string or None if the code doesn't exist.
        """
        req = "select value from lvf_compléments where code=:code;"

        k = self._curseur._curseur().execute(req, {"code": code})

        rows = k.fetchall()
        if len(rows) == 0:
            return None
        else:
            return rows[0][0]

    def get_phrases(self, verbe_id: str):
        """
        Phrase for a verb_id
        :param id of a verb structure
        :return: array of string or None if the code doesn't exist.
        """
        req = "select phrase from lvf_phrases where verbe_id=:id;"

        k = self._curseur._curseur().execute(req, {"id": verbe_id})

        rows = k.fetchall()
        if len(rows) == 0:
            return None
        else:
            return [row[0] for row in rows]


def __exit_handler():
    """
    Close the sqlite connection
    :return:void
    :rtype:None
    """
    try:
        if Word.__open:
            Word.__connVerbs.close()
            Word.__open = False
    except Exception:
        pass


atexit.register(__exit_handler)

database = Word._database[:]

class Information():
    """
    Allow operation on a word
    """

    __cached_words = {}
    __cached_lemmas__ = {}

    def __init__(self, grammaticalCategory=None, source=None):
        self._grammaticalCategory = grammaticalCategory
        self._source = source

    def get_words(self, form: str, tense: Tense = None, mood: Mood = None,
                   person: Person = None, number: Number = None,
                   aspect: Aspect = None,
                   mode: Mode = None, temps: Temps = None,
                   elision: Elision = None, pos: POS = None):
        rows = self.get_rows(form, tense, mood, person, number,
                   aspect, mode, temps, elision, pos)
        lemmas = set()
        for row in rows:
            lemmas.add(row["lemma"])
        for lemma in lemmas:
            for row in self.get_forms(lemma, tense, mood, person, number,
                   aspect, mode, temps, elision, pos):
                yield row

    def get_forms(self, lemma: str, tense: Tense = None, mood: Mood = None,
                   person: Person = None, number: Number = None,
                   aspect: Aspect = None,
                   mode: Mode = None, temps: Temps = None,
                   elision: Elision = None, pos: POS = None):
        """
        Generator of dictionnary informing words sharing the same lemma.
        :param lemma: lemma to define
        :type lemma: str
        :param tense: tense
        :type tense: Tense
        :param mood: mood
        :type mood: Mood
        :param person: person
        :type person: Person
        :param number: number
        :type number: Number
        :param aspect: aspect
        :type aspect: Aspect
        :param mode: mode (french name)
        :type mode: Mode
        :param temps: temps (french name)
        :type temps: Temps
        :param elision: (elision)
        :type elision: Elision
        :param pos: dem pos
        :type pos: POS
        :return: generator of dict
        :rtype: Iterable
        """
        grammaticalCategory = self._grammaticalCategory
        source = self._source

        lemma = Lemma(lemma)
        for name in lemma.forms:
            name = Word(name)
            name.screen = grammaticalCategory
            name.screen = tense
            name.screen = mood
            name.screen = person
            name.screen = number
            name.screen = aspect
            name.screen = mode
            name.screen = temps
            name.screen = elision
            name.screen = source
            name.screen = pos

            for variation in name.get_rows():
                yield {k: v for k, v in variation.items() if variation[k] is not None}

    def get_rows(self, form, lemma=None, tense: Tense = None, mood: Mood = None,
                 person: Person = None, number: Number = None,
                 aspect: Aspect = None,
                 mode: Mode = None, temps: Temps = None,
                 elision: Elision = None, pos: POS = None):
        """
        Generator of dictionnary informing word with differents lemmas
        :param form: word to define
        :type form: str
        :param lemma: lemma restricting the word
        :type lemma: str
        :param tense: tense
        :type tense: Tense
        :param mood: mood
        :type mood: Mood
        :param person: person
        :type person: Person
        :param number: number
        :type number: Number
        :param aspect: aspect
        :type aspect: Aspect
        :param mode: mode (french name)
        :type mode: Mode
        :param temps: temps (french name)
        :type temps: Temps
        :param elision: (elision)
        :type elision: Elision
        :param source: data source, none for all
        :type source: Source
        :param pos: dem pos
        :type pos: POS
        :return: generator of dict
        :rtype: Iterable
        """
        name = Word(form)

        grammaticalCategory = self._grammaticalCategory
        source = self._source
        name.screen = grammaticalCategory
        name.screen = tense
        name.screen = mood
        name.screen = person
        name.screen = number
        name.screen = aspect
        name.screen = mode
        name.screen = temps
        name.screen = elision
        name.screen = source
        name.screen = pos

        for variation in name.get_rows():
            yield {k: v for k, v in variation.items() if
                   variation[k] is not None and (lemma is None or variation['lemma'] == lemma)}

    def get_words_items(self, form: str = None, lemma: str = None, cache: bool = False):
        return self._get_cached_words(form=form, lemma=lemma, cache=cache)

    def _get_cached_words(self, form: str = None, lemma: str = None, cache: bool = True):
        if cache:
            key_1 = "" if form is None else form
            key_2 = "" if lemma is None else lemma
            if self._grammaticalCategory is not None:
                key_1 += self._grammaticalCategory.__str__()
            if self._source is not None:
                key_1 += self._source.__str__()
            if key_1 not in self.__cached_words.keys():
                self.__cached_words[key_1] = {}
            if key_2 not in self.__cached_words[key_1].keys():
                self.__cached_words[key_1][key_2] = None

        if not cache or self.__cached_words[key_1][key_2] is None:
        
            words = set()
            if form is not None:
                words.update([k for k in Word(form=form).get_words_items() if (lemma is None or k.lemma == lemma) and (self._grammaticalCategory is None or k.grammaticalCategory == self._grammaticalCategory) and (self._source is None or k.source == self._source)])
            else:
                lemma = Lemma(lemma)
                for form in lemma.forms:
                    word.update([k for k in Word(form=form).get_words_items() if (self._grammaticalCategory is None or k.grammaticalCategory == self._grammaticalCategory) and (self._source is None or k.source == self._source)])
            if cache:
                self.__cached_words[key_1][key_2] = words
        else:
            words = self.__cached_words[key_1][key_2]
        return words

    def is_noun(self, form: str = None, lemma: str = None, cache: bool = False):
        

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found

        results = set()

        for word in words:
            results.add(word.grammaticalCategory)
            if GrammaticalCategory.commonNoun in results and len(results) > 1:
                return Answer.ambiguous
        if GrammaticalCategory.commonNoun in results:
            return Answer.yes

        return Answer.no

    def is_singular(self, form: str = None, lemma: str = None, cache: bool = True):

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.number)
            if Number.singular in results and len(results) > 1:
                return Answer.ambiguous
        if Number.singular in results:
            return Answer.yes
        return Answer.no

    def is_plural(self, form: str = None, lemma: str = None, cache: bool = True):

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.number)
            if Number.plural in results and len(results) > 1:
                return Answer.ambiguous
        if Number.plural in results:
            return Answer.yes
        return Answer.no

    def is_masculine(self, form: str = None, lemma: str = None, cache: bool = True):

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.gender)
            if "masculine" in results and len(results) > 1:
                return Answer.ambiguous
        if "masculine" in results:
            return Answer.yes
        return Answer.no

    def is_feminine(self, form: str = None, lemma: str = None, cache: bool = True):

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.gender)
            if "feminine" in results and len(results) > 1:
                return Answer.ambiguous
        if "feminine" in results:
            return Answer.yes
        return Answer.no

    def is_mixte(self, form: str = None, lemma: str = None, cache: bool = True):

        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.gender)
            if "all" in results and len(results) > 1:
                return Answer.ambiguous
        if "all" in results:
            return Answer.yes
        return Answer.no

    def get_gender(self, form: str = None, lemma: str = None, cache: bool = True):
        """
        Get a gender
        :param form: form of the word
        :param lemma: lemma of the word
        :param cache: data are cached ? default True
        :return: Answer.not_found if not found, else list of genders
        """
        words = self._get_cached_words(form=form, lemma=lemma, cache=cache)

        if len(words) == 0:
            return Answer.not_found
        results = set()

        for word in words:
            results.add(word.gender)
        return results

def __source():
    for arg in sys.argv[1:]:
        name = Word(arg)

        if sys.argv[0].endswith('Morphalou'):
            print("Please, for verbs use LVF or Dem", file=sys.stderr)
            name.screen = Source.morphalou
        elif sys.argv[0].endswith('LVF'):
            name.screen = Source.lvf
        elif sys.argv[0].endswith('Dem'):
            name.screen = Source.dem
        print("\033[;7m" + arg + "\033[0;0m")  # reversed and reset color
        for variation in name.get_rows():
            print("", end="\t")
            print({k: v for k, v in variation.items() if variation[k] is not None})
        print("\033[0;0m")  # reset color


def __lemma():
    for arg in sys.argv[1:]:
        lemmas = Lemma(arg).forms
        print("\033[;7m" + arg + "\033[0;0m")  # reversed and reset color
        for lemme in lemmas:
            print("", end="\t")
            print(lemme)
        print("\033[0;0m")  # reset color

def __categories():
    _lvf = LVF()
    for arg in sys.argv[1:]:
        details = _lvf.get_verb(infinitive_form=arg)
        print("\033[;7m" + arg + "\033[0;0m")  # reversed and reset color
        for detail in details:
            print("", end="\t")
            print(detail)
        print("")
        print(len(details).__str__() + " entries.")
        print("\033[0;0m")  # reset color

def __get_details(verbs, key):
    _lvf = LVF()
    _info = set()
    for arg in verbs:
        details = _lvf.get_verb(infinitive_form=arg)
        print("\033[;7m" + arg + "\033[0;0m")  # reversed and reset color
        for detail in details:
            if detail[key] not in _info:
                print("", end="\t")
                print(detail[key])
                _info.add(detail[key])
        print("")
        print(len(_info).__str__() +" " + key + " entries.")
        print("\033[0;0m")  # reset color
    return _info

def __constructions():
    key = "construction"
    __get_details(sys.argv[1:], key)

def __groupes():
    key = "groupe"
    __get_details(sys.argv[1:], key)

def __sousgroupes():
    key = "sousgroupe"
    __get_details(sys.argv[1:], key)

def __auxiliaires():
    key = "auxiliaire"
    __get_details(sys.argv[1:], key)

def __types():
    key = "type"
    __get_details(sys.argv[1:], key)

def _get_details(verbs:[], key:str):
    """
    :return a dictionary of lvf values for verbs
    :param verbs: array of string: verb lemmas
    :param key: lvf key among
    :return: set of key data for the lemma
    """
    _lvf = LVF()
    _info = set()
    for arg in verbs:
        details = _lvf.get_verb(infinitive_form=arg)
        for detail in details:
            if detail[key] not in _info:
                _info.add(detail[key])

    return _info

def _constructions(verbs:[]):
    key = "construction"
    return _get_details(verbs, key)

def _groupes(verbs:[]):
    key = "groupe"
    return _get_details(verbs, key)

def _sousgroupes(verbs:[]):
    key = "sousgroupe"
    return _get_details(verbs, key)

def _auxiliaires(verbs:[]):
    key = "auxiliaire"
    return _get_details(verbs, key)

def _types(verbs:[]):
    key = "type"
    return _get_details(verbs, key)

def main():
    for arg in sys.argv[1:]:
        name = Word(arg)
        print("\033[;7m" + arg + "\033[0;0m")  # reversed and reset color
        for variation in name.variations:
            print("", end="\t")
            print({k: v for k, v in variation.items() if variation[k] is not None})
        print("\033[0;0m")  # reset color


if __name__ == '__main__':
    main()
    print("object information:")
    print("""
    info = Information()
    print([k.__str__() for k in info.get_words_items('mangée')])
    a = info.is_feminine(form='mangée')
    print(a)""")

    info = Information()
    aime = info.get_words_items("aime")
    print([k.__str__() for k in info.get_words_items("mangée")])
    a = info.is_feminine(form="mangée")
    print(a)

    print("informations on aime form:")
    print("""
    info = Information(grammaticalCategory=None, source=None)
    aime = info.get_words_items("aime") #Return Word.Word_Item objects
    for word in aime:
        print(word)""")
    info = Information(grammaticalCategory=None, source=None)
    aime = info.get_words_items("aime") #Return Word.Word_Item objects
    for word in aime:
        print(word)

    print("informations on manger form:")
    print("""
    info = Information(grammaticalCategory=None, source=None)
    aime = info.get_words_items("manger") #Return Word.Word_Item objects
    for word in manger:
        print(word)""")
    info = Information(grammaticalCategory=None, source=None)
    manger = info.get_words_items("manger") #Return Word.Word_Item objects
    for word in manger:
        print(word.properties())
