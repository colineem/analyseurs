#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Use information inside analyze to build trees of events and arguments based on lvf 
ref: http://rali.iro.umontreal.ca/LVF+1/documents/PresentationLVF_JF_DLP_DL.pdf
3/1/19
  """
# on les habille non en noir, mais en rouge

start_pass_db_from = 0

import glob
import os
import sys
import json
import re #import our regex module
from xml.etree import ElementTree
from pprint import pprint
from tqdm import tqdm
import copy
import shutil
import random
import math
from inspect import currentframe, getframeinfo

from mosestokenizer import MosesTokenizer
from pattern.fr import singularize, pluralize, conjugate
from pattern.fr import INDICATIVE, IMPERATIVE, CONDITIONAL, SUBJUNCTIVE
from pattern.fr import IMPERFECTIVE, PROGRESSIVE, PERFECTIVE, PRESENT, PAST, FUTURE, INFINITIVE
from pattern.fr import SG, PL

tqdm.monitor_interval = 0  # bug multithreading #https://github.com/tqdm/tqdm/issues/481

tokenize = MosesTokenizer(lang='fr')
ASPECT =[None, IMPERFECTIVE, PROGRESSIVE]

debug = True

if debug:
    count_size_verbe = {}
    start_pass_db_from = 0#54
    start_pass_i_analyse_from = 0# placer à -1 pour désactiver ou zéro #54

    
cpls_intro = set()
SAVE_PART = 700 # nombre de phrases à sauver par partie

arbres_extraits = {}
table_des_verbes = {}
nb_verbes_sentence = []
phrases_perdues = 0
dic_verbes_sentences = {}
table_des_arbres = {}
table_des_types_events = {}

clean = re.compile('<.?phrase>')

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

phrases_perdues_phase_1 = []
phrases_perdues_phase_2 = []
take_table = {}

keys_arbres_extraits = set()
if not debug:
    if os.path.exists("arbres"):
        shutil.rmtree("arbres")
    os.makedirs("arbres/random")
    os.makedirs("arbres/simple")
    os.makedirs("arbres/simple sans analyses")
count_part = 0

def save_part_arbres(phrases_par_fichiers = SAVE_PART):
    global arbres_extraits, keys_arbres_extraits, count_part
    size_arbres = len(arbres_extraits.keys())

    if size_arbres % phrases_par_fichiers == 0:
        local_keys_arbres_extraits = list(arbres_extraits.keys())

        arbres_ = {}
        arbres_2_ = {}

        for key in local_keys_arbres_extraits:
            if key not in keys_arbres_extraits:
                keys_arbres_extraits.add(key)
                arbres_[key] = arbres_extraits[key]
                arbres_2_[key] = copy.deepcopy(arbres_extraits[key])
                del arbres_2_[key]["analyses"]
                

        count_part += 1

        if debug:
            return

        fjson = open("arbres/simple/arbres_extraits_" + count_part.__str__().zfill(4) + ".json", "w")
        fjson.write(json.dumps(arbres_, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
            
        fjson = open("arbres/simple sans analyses/arbres_extraits_" + count_part.__str__().zfill(4) + ".json", "w")
        fjson.write(json.dumps(arbres_2_, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
# exclusions:
# la première liste contient trop de livres, mais excepté les deux premiers, ils contiennent tous " avoit "
exclusions_list = ['41843-0.txt', "42088-0.txt", "10685/10685-8.txt", "10697/10697-8.txt", "10824/10824-8.txt", "12137-8.txt", "12246-8.txt", "12356-8.txt", "12437-8.txt", "12504-8.txt", "12566-8.txt", "12812-8.txt", "13804-8.txt", "13846-8.txt", "13938-8.txt", "13965-8.txt", "14343-8.txt", "14692-8.txt", "14905-8.txt", "15310-0.txt", "15739-0.txt", "15871-8.txt", "15907-8.txt", "16816-8.txt", "17044-0.txt", "17140/17140-8.txt", "17258-8.txt", "17940-8.txt", "17984-8.txt", "18061-8.txt", "18089-8.txt", "18121-8.txt", "18455-8.txt", "18610-8.txt", "18669-8.txt", "18727-8.txt", "18865-8.txt", "18889-8.txt", "19124-8.txt", "19454-8.txt", "19455-8.txt", "19954-8.txt", "19956-8.txt", "19984-8.txt", "20143-8.txt", "20262-0.txt", "20325-8.txt", "20415-8.txt", "20441-8.txt", "20457-8.txt", "20635-8.txt", "20886-0.txt", "21199-8.txt", "21221-8.txt", "21257-8.txt", "22011-8.txt", "22016-8.txt", "22268-8.txt", "22388-8.txt", "22543-8.txt", "22552-8.txt", "22613-8.txt", "22751-8.txt", "22830-8.txt", "23199-0.txt", "23484-0.txt", "23596-0.txt", "23610-0.txt", "23654-8.txt", "23828-8.txt", "24320-8.txt", "24546-0.txt", "24555-8.txt", "24915-8.txt", "25036-8.txt", "25680-8.txt", "25839-8.txt", "26082-0.txt", "26211-0.txt", "26394-8.txt", "26432-8.txt", "26435-8.txt", "26531-8.txt", "26608-8.txt", "26680-8.txt", "26780-0.txt", "26822-8.txt", "2682-8.txt", "27215-0.txt", "27267-0.txt", "27269-8.txt", "27278-8.txt", "27281-8.txt", "27282-8.txt", "27610-8.txt", "27625-8.txt", "27641-8.txt", "27772-8.txt", "27773-8.txt", "27807-8.txt", "27808-8.txt", "27809-8.txt", "27876-0.txt", "27878-8.txt", "28230-8.txt", "28534-0.txt", "28789-0.txt", "29049-8.txt", "29251-8.txt", "29279-8.txt", "29476-0.txt", "29772-0.txt", "30268-8.txt", "30602-8.txt", "30654-8.txt", "30781-8.txt", "30782-8.txt", "30783-8.txt", "30785-8.txt", "30787-8.txt", "30788-8.txt", "30904-0.txt", "31474-0.txt", "31628-8.txt", "31805-8.txt", "32298-8.txt", "32349-8.txt", "32621-8.txt", "32798-8.txt", "33033-8.txt", "33184-0.txt", "33205-0.txt", "33408-8.txt", "33414-8.txt", "33580-0.txt", "33590-8.txt", "33692-8.txt", "33745-0.txt", "33856-8.txt", "33931-8.txt", "34351-0.txt", "34445-8.txt", "34803-0.txt", "34991-8.txt", "35010-8.txt", "35089-0.txt", "35262-8.txt", "35525-8.txt", "35643-0.txt", "35732-0.txt", "35971-8.txt", "36011-8.txt", "36025-0.txt", "36315-0.txt", "36826-0.txt", "36972-0.txt", "37096-8.txt", "37305-8.txt", "37401-0.txt", "37617-8.txt", "37678-8.txt", "37987-8.txt", "38118-0.txt", "38244-8.txt", "38361-8.txt", "39201-8.txt", "39220-0.txt", "39314-8.txt", "39320-0.txt", "39335-8.txt", "39363-0.txt", "39449-0.txt", "39512-0.txt", "39513-0.txt", "39687-0.txt", "39739-0.txt", "39880-8.txt", "40025-0.txt", "40049-0.txt", "40086-8.txt", "40172-0.txt", "40194-8.txt", "40496-8.txt", "40530-0.txt", "40625-8.txt", "40695-0.txt", "40763-8.txt", "40768-8.txt", "40902-8.txt", "41099-0.txt", "41123-8.txt", "41124-8.txt", "41147-0.txt", "41226-0.txt", "41577-8.txt", "41644-0.txt", "41943-0.txt", "41967-8.txt", "41970-8.txt", "41992-8.txt", "42021-8.txt", "42126-0.txt", "42177-0.txt", "42186-8.txt", "42432-0.txt", "42463-8.txt", "42464-8.txt", "42497-0.txt", "42525-0.txt", "42586-0.txt", "42659-8.txt", "42662-8.txt", "42694-8.txt", "42695-0.txt", "42743-0.txt", "42744-8.txt", "43291-0.txt", "43311-8.txt", "43501-0.txt", "43535-0.txt", "43561-0.txt", "43718-8.txt", "43734-8.txt", "43752-8.txt", "43760-8.txt", "43772-8.txt", "43848-0.txt", "43849-8.txt", "44070-0.txt", "44156-0.txt", "44162-8.txt", "44181-0.txt", "44198-8.txt", "44199-8.txt", "44285-0.txt", "44301-8.txt", "44403-0.txt", "44453-0.txt", "44504-0.txt", "44697-0.txt", "44713-0.txt", "44723-0.txt", "44877-0.txt", "44906-0.txt", "44960-0.txt", "45031-0.txt", "45121-0.txt", "45150-0.txt", "45212-8.txt", "45213-8.txt", "45323-0.txt", "45458-0.txt", "45513-8.txt", "45655-0.txt", "45679-8.txt", "45692-0.txt", "45787-0.txt", "45864-0.txt", "46033-0.txt", "46142-0.txt", "46183-0.txt", "46224-0.txt", "46527-0.txt", "46640-0.txt", "46870-0.txt", "46991-0.txt", "47459-0.txt", "47468-8.txt", "47469-8.txt", "48401-8.txt", "48421-8.txt", "48477-8.txt", "48519-8.txt", "48520-8.txt", "48529-0.txt", "48581-8.txt", "48590-0.txt", "48683-0.txt", "48684-0.txt", "48855-0.txt", "49004-0.txt", "4936-0.txt", "49409-8.txt", "49502-8.txt", "49586-0.txt", "49855-0.txt", "49943-0.txt", "50173-0.txt", "50340-0.txt", "50356-0.txt", "50743-0.txt", "51312-0.txt", "51516-0.txt", "51612-0.txt", "51631-0.txt", "52011-0.txt", "52140-0.txt", "52282-0.txt", "52443-0.txt", "52487-0.txt", "52488-0.txt", "52489-0.txt", "52629-0.txt", "52843-0.txt", "52929-0.txt", "53183-0.txt", "53279-0.txt", "53331-0.txt", "53497-0.txt", "53540-0.txt", "53595-0.txt", "53640-0.txt", "53761-0.txt", "54035-0.txt", "54089-0.txt", "54182-0.txt", "54311-0.txt", "54482-0.txt", "54819-0.txt", "55233-0.txt", "55430-8.txt", "55639-0.txt", "55659-0.txt", "56072-0.txt", "56327-0.txt", "56390-0.txt", "56918-8.txt", "56919-8.txt", "57270-0.txt", "7268-8.txt", "7812-8.txt", "8591-8.txt", "9818-8.txt", "9891-8.txt"]
# deux glossaires
exclusions_list = ['41843-0.txt', "42088-0.txt", 'books/27878-8.txt']
save_arbres = True

if os.path.exists('table_des_arbres.json'):
    a = open("table_des_arbres.json", "r")
    table_des_arbres = json.load(a)
    a.close()
    save_arbres = False

if os.path.exists('lvf.json') and os.path.exists("lvf_état_fini.json"):
    a = open('lvf.json', "r")
    lvf = json.load(a)
    a.close()
    a = open("lvf_état_fini.json", "r")
    lvf_état_fini = json.load(a)
    a.close()

else:
    # on va récuperer les phrases
    fichier = open("../preparsing/tools/LVF/dubois.xml", "r")
    phrases_texte = [ligne.strip() for ligne in fichier if "<phrase>" in ligne]
    fichier.close()

    lvf = {}
    lvf_état_fini = {}

    root = ElementTree.parse(os.path.join(fullpath, "../preparsing/tools/LVF/dubois.xml")).getroot()

    for elt in root:
        mot = elt.attrib['mot']
        nb = elt.attrib['nb']
        id = elt.attrib['id']

        lvf[mot] = {}
        lvf_état_fini[mot] = {}
        infos = []

        entrees = elt.findall("entree")
        for entree in entrees:
            lvf[mot][entree.attrib['LVFid']] = {'domaine':entree.find("domaine").attrib["nom"], 'constructions':{}, 'phrases':[]}
            lvf[mot][entree.attrib['LVFid']]["conjugaison"] = entree.find("conjugaison").attrib
            lvf[mot][entree.attrib['LVFid']]["classe"] = entree.find("classe").attrib
            constructions = entree.findall("construction/scheme")
            phrases =  entree.findall("phrases/phrase")
            for phrase in phrases:
                lvf[mot][entree.attrib['LVFid']]['phrases'].append(re.sub(clean, '', phrases_texte.pop(0)).strip())

            for scheme in constructions:
                lvf[mot][entree.attrib['LVFid']]['constructions'][scheme.text] = scheme.attrib
                code = scheme.text

                if scheme.attrib['type'] not in  lvf_état_fini[mot].keys():
                    lvf_état_fini[mot][scheme.attrib['type']] = []
                info = code + "".join([scheme.attrib[t] for t in list(scheme.attrib.keys())])
                if info not in infos:
                    lvf_état_fini[mot][scheme.attrib['type']].append((code,{t:scheme.attrib[t] for t in list(scheme.attrib.keys()) if t != 'type'}))
                    infos.append(info)

    if not debug:
        fjson = open("arbres/lvf_état_fini.json", "w")
        fjson.write(json.dumps(lvf_état_fini, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
        fjson = open("arbres/lvf.json", "w")
        fjson.write(json.dumps(lvf, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True

sentences_dbs = sorted(glob.glob(os.path.join(fullpath, '../preparsing/outputs/results_70/*_analyses.txt')))[start_pass_db_from:]
stats_reductions = {"détail":{}, "final":{}, "initial":{}}
count_take = 0

def log(lineNumber):
    take_table[lineNumber] = take_table[lineNumber]+1 if lineNumber in take_table.keys() else 1
    sys.stdout.flush()

livres = {}
def addTypeEvent(sentence, index_tag, value):# infinitif, vpp, vpa, conj sur key sentence
    # if value not in ('infinitif', 'vpp', 'vpa', 'conj'):
    #     raise Exception
    if sentence not in table_des_types_events.keys():
        table_des_types_events[sentence] = {}
    table_des_types_events[sentence][index_tag] = value

def calcDispo(information_lvf):
    dispos = []
    types = [t for t in information_lvf.keys()]
    for t in types:
        if len(information_lvf[t]) == 0:
            del information_lvf[t]
        else:
            for item in information_lvf[t]:
                dispos.append("-".join([k for k in item[1].values()]))
    return dispos

def filter(information_lvf):
    # je ne suis pas capable de distinguer les circonstants... et ils sont facultatifs.

    tmp_lvf_info = copy.deepcopy(information_lvf)
    information_lvf = {}
    for t in tmp_lvf_info.keys():
        information_lvf[t] = []
        new_dic = {}
        codes = []
        for code, item in tmp_lvf_info[t]:
            codes.append(code.ljust(5))
            for k in item.keys():
                if k not in new_dic.keys():
                    new_dic[k] = set()
                for value in item[k].split(" ou "):
                    new_dic[k].add(value)
        for k in new_dic.keys():
            new_dic[k] = " ou ".join(sorted(list(new_dic[k])))

        nouveau_code = []
        nouveau_code += codes[-1]
        for i in range(len(codes)-1) :
            for j in range(len(codes[0])) :
                if nouveau_code[j] != codes[i][j]:
                    nouveau_code[j] = "Z"
                    break
        nouveau_code = "".join(nouveau_code)

        if [code, item] not in information_lvf[t]:
            information_lvf[t].append([nouveau_code, new_dic])

    if len(set(calcDispo(information_lvf))) == 1:
        tmp = calcDispo(information_lvf)[0]
        for t in tmp_lvf_info.keys():
            for item in tmp_lvf_info[t]:
                value = "-".join([k for k in item[1].values()])
                if tmp == value:
                    return {t:[item]}
        return information_lvf
    elif len(set(calcDispo(information_lvf))) > 1:
        keys = " ou ".join(sorted(information_lvf.keys()))
        dic_synthèse = {keys: []}
        for str_type in information_lvf.keys():
            for choice in information_lvf[str_type]:
                dic_synthèse[keys].append(choice)
        return filter(dic_synthèse)  # Exception
    elif len(set(calcDispo(information_lvf))) == 0 and len(tmp_lvf_info)>0:
        keys = " ou ".join(sorted(tmp_lvf_info.keys()))
        dic_synthèse = {keys: []}
        for str_type in tmp_lvf_info.keys():
            for choice in tmp_lvf_info[str_type]:
                dic_synthèse[keys].append(choice)
        return filter(dic_synthèse)  # Exception

    return information_lvf
#Dunkerque fut le port où, le 21 mai 1717, descendit Pierre Ier, accompagné de sa suite.

def getObjet(item, grew, talismane, sentence):
    # l'item est construite sous la forme :
    # item = (grew[x][1], grew[x][3], grew[x][5], x) avec x l'index de l'objet à remonter
    if grew[item[3]][2] == 'que':
        item = (grew[item[3]][2], grew[item[3]][3], grew[item[3]][5], item[3])

    if grew[item[3]][2] == "ce" and grew[item[3]+1][2] == 'que':
        item = (grew[item[3]+1][2], grew[item[3]+1][3], grew[item[3]+1][5], item[3]+1)

    if grew[item[3]][2].lower() in ("dont", "où", "qui") or item[0] in ("dont", "où", "qui") or ("au" in item[0] and "quel" in item[0]) or grew[item[3]][4].lower() in ("vpr", 'vpp') :
        if sentence == 'Grande corbeille carrée qui se fabrique dans le Jura, et dont on se sert pour emballer.' \
                or sentence == 'Une troupe de comédiens français, dont je faisais partie, donnait des représentations dans cette ville.':
            return True, True, grew[1]
        elif sentence == "Leur luxe surpassait souvent celui des femmes de qualité, dont les maris blâmaient la dépense tout en prodiguant l' or à leurs maîtresses.":
            return True, True, grew[6]
        elif sentence == 'Alors il prit sans hésiter son chapeau, dont il détacha la cocarde, et la remit soigneusement dans sa poche pour la prochaine occasion.':
            return True, True, grew[6]
        elif sentence == "Il m' est témoin encore qu' eux seuls me poussent dans la voie que je vais suivre, et dont rien à présent ne me détournera, aussi vrai que j' espère en la miséricorde divine.":
            return True, True, grew[12]
        elif sentence == "Son plus doux souvenir fut celui d' une eau limpide et froide où il lava son front chaud et fatigué dans un jardin de Gênes.":
            return True, True, grew[8]
        elif sentence == "Le plus doux souvenir qui te resta dans la mémoire fut celui d' une eau limpide et froide où tu lavas ton front chaud et fatigué dans un jardin de Gênes.":
            return True, True, grew[14]
        elif sentence == "Ce dont vous souffrez, c' est de la maladie du scrupule.":
            return True, True, grew[9]
        elif sentence == "Cette perte de vue, sur du blanc dont les tons changent avec le ciel, s' assombrissent sous la tempête, et prennent des reflets roses au soleil oblique du matin ou du couchant, cette vision infinie et immaculée est splendide et poignante.":
            return True, True, grew[1]
        elif talismane[item[3] - 1][3] == "ADJ" and grew[item[3] - 1][3] == "A" and grew[item[3] - 2][3] == "N":
            return True, True, grew[item[3] - 2]
        elif talismane[item[3] - 1][3] == "PONCT" and (grew[item[3] - 2][1] == "ici" or grew[item[3] - 2][3] == "N"):
            return True, True, grew[item[3] - 2]
        elif talismane[item[3] - 1][3] == "PONCT" and grew[item[3] - 2][3] == "A" and grew[item[3] - 3][3] == "N":
            return True, True, grew[item[3] - 3]
        elif sentence in (
        "Si, par les nuits sereines de fin avril ou des premiers jours de mai, elle brille d' un vif éclat précisément à la période de l' année où la végétation entre en mouvement et où le sol ne reçoit pas encore suffisamment la chaleur solaire, qu' arrive-t-il ?", "Je vous expliquerai tout cela et, en relisant vos notes ensemble, nous arrêterons le relatif dont j' ai besoin.", "Seulement il lui trouvait, à part lui, dans le regard un « je ne sais quoi » dont il avait peur.", "Rien de ce doux confortable dont une femme a besoin.", "Une école est une troupe de petits Écrans opaques d' un grain très grossier, qui, n' ayant pas eux-mêmes la puissance de donner des images, prennent celle de l' Écran puissant et pur dont ils font leur chef de file.", "Le lendemain, à sept heures du matin, Boniface Cointet se promenait le long de la prise d' eau qui alimentait sa vaste papeterie, et dont le bruit couvrait celui des paroles.",
        'Çà et là, des chaloupes faisaient des points téméraires.'):
            return False, False, ""
        elif sentence == "Les légendes elles-mêmes sont une source féconde de la littérature canadienne, légendes de ces temps lointains des luttes contre le féroce Iroquois, où le merveilleux se mêle à l' héroïsme.":
            return True, True, grew[12]
        elif sentence == "Presque chaque jour, on y voit figurer dix centimes pour son tabac à priser, dont elle faisait une grande consommation, comme toutes les personnes de son temps.":
            return True, True, grew[12]
        elif grew[item[3] - 1][3] == "N" or talismane[item[3] - 1][3] == "NC" or (grew[item[3] - 1][4] == "PRO"):
            return True, True, grew[item[3] - 1]
        elif grew[item[3]-1][4] == "CC":
            # coordination de relative, on trouve la relative précédente
            for x in sorted(list(range(item[3]-1)), reverse=True):
                if grew[x][2].lower() in ("dont", "où", "qui") or (grew[x][5] == 's=rel' and grew[x][4] == 'PROREL'):
                    item = (grew[x][1], grew[x][3], grew[x][5], x)
                    return getObjet(item, grew, talismane, sentence)
            return False, False, ""
        else:
            # TODO
            return False, False, ""
        if not done:
            return True, True, grew[item[3] - 1]
    elif grew[item[3]][2] == "que":
        # on va avancer et trouver le verbe

        for k in [l+item[3]+1 for l in range(len(grew) - item[3]-1)]:
            evaluation = grew[k][7] == 'obj.cpl' or grew[k][7] == 'mod.rel' or grew[k][7] == 'mod'
            if evaluation and grew[k][4] == "V":
                return True, True, grew[k]
        return False, False, ""
    elif sentence == "On n' en trouvait pas d' autre pour replacer au but ; le jeune prince voit briller une rose sur le sein d' une des jeunes filles qui entourent la barrière, il s' en saisit et court la placer." and \
            grew[item[3]][1] == "en":
        return True, True, grew[30]
    elif sentence == "Le pere ne l' en justifia pas davantage dans son esprit, pour avoir bonne réputation auprès d' elle.":
        return True, True, grew[-2]
    elif sentence == "Il en avertit son fils & lui recommanda d'y prendre garde.":
        # support du "en" hors phrase... # TODO
        return False, False, ""

    else:#if grew[item[3]][2] in ("de", "*du", "*des") or item[0][-2:].lower() in ("de", "du") or item[0].lower() in ("à", "selon", "sans", "par", "pour", 'sans','une_fois',  'derrière', 'outre', 'hors', 'contre', 'avant', 'après', 'depuis', "chez", 'à_cause_de', "dès", "pour", "comme", "avec", "pour","dans","devant", "entre","aux", "au", "sous", "malgré", 'auprès_de', "vers", "sur", 'en-dehors_de',"quant_à", "dans", "en", "parmi", "par", "depuis", "pendant") or grew[item[3]][4].lower() in ("p+d", "clo") or "tandis" in item[0]:
        if grew[item[3]][4].lower() == "clo":
            if item[0] != "en":
                return True, True, grew[item[3]]
            elif sentence == "Un danger grave menace César, qui l' en avertit ?":
                return True, True, grew[3]
            else:
                retour = False
                for k in [l + item[3] + 1 for l in range(len(grew) - item[3] - 1)]:
                    if grew[k][0] == grew[item[3]][6]:
                        verbe = grew[k][0]
                        for l in range( item[3]-1, -1, -1):
                            if grew[l][5] == "s=rel" and grew[item[3]][6] == grew[l][6]:
                                retour = True
                            if retour and grew[l][5] == "suj":
                                return True, True, grew[l]
                            else:
                                break
                return False, False, ""
        if len(grew)-1 <= item[3]:
            if  grew[item[3]][4].startswith("P"):
                for k in range(item[3]+2, len(grew)):
                    if grew[k][7] == 'obj.p' and grew[k][6] != "_" and getTGIndex(grew[k][6]) == item[3]:
                        return True, True, grew[k]
                    if talismane[k][7] == 'prep' and talismane[k][6] != "_" and getTGIndex(talismane[k][6]) == item[3]:
                        return True, True, grew[k]

            if grew[item[3] + 1][4] == "CLO":
                return True, True, grew[item[3] + 1]
            return False, False, ""

        if grew[item[3] + 1][4] == "VINF":
            return True, True, grew[item[3] + 1]
        if grew[item[3] + 1][4] == "PROREL" and grew[item[3] + 1][2] == "qui":
            for k in range( item[3]-1, -1, -1):
                evaluation = 'Celui-ci regrettait bien sa mauvaise action, surtout en voyant le courage de ce jeune homme à qui il avait projeté d\' unir sa fille.' or grew[k][7] != 'obj.p'
                if evaluation and  grew[k][4] in ("NC", "NPP", "PRO", "PROWH"):
                    return True, True, grew[k]
            return False, False, ""
        elif grew[item[3] + 1][4] in ("NC", "NPP", "PRO", "PROWH"):
            return True, True, grew[item[3] + 1]
        elif len(grew)-2 <= item[3]:
            return False, False, ""
        elif grew[item[3] + 1][4] == "ADJ" and grew[item[3] + 2][4] in ("P", "P+D", 'PROREL', "P+PRO"):
            return True, True, grew[item[3] + 1]
        elif (grew[item[3] + 1][5] == "s=refl" or grew[item[3] + 1][2] == "ne") and grew[item[3] + 2][4] == "VINF":
            return True, True, grew[item[3] + 2]
        elif grew[item[3] + 1][4] in ("CLO", "ADV") and grew[item[3] + 2][4] == "VINF":
            return True, True, grew[item[3] + 2]
        elif grew[item[3] + 1][4] in ("ADJWH", "DETWH") and grew[item[3] + 2][4] in ("NC", "NPP"):
            return True, True, grew[item[3] + 2]
        elif sentence == "Il n' avait vu que cette créature, blanche parmi ses voiles blancs, de laquelle émanait une suavité si pénétrante qu' il avait de la peine à croire à son bonheur !":
            return True, True, grew[item[3] + 2]
        elif sentence in (
        "Quelques jours après, je priai mon mari de s' en informer, autant qu' on pouvait le faire cependant sans se compromettre.", "Chevalier se leva et fit mine de s' en aller, pour montrer qu' il avait de l' usage."):
            return False, False, ""
        elif sentence in ("Dieu me préserve de te les répéter et de te nommer les sots qui les inventent !",
                          'Chaque âge et chaque estomac en exigent de différents.',
                          "Je vois votre pensée, et de quoi l' on m' accuse.", "Michel s' avisa de tout cela trop tard pour réparer son imprudence."):
            return False, False, ""
        elif sentence in (
        "Les sots font de leur morale une masse compacte et indivisible, pour qu' elle se mêle le moins possible avec leurs actions, et les laisse libres dans tous les détails.", "Je me plaignais du mien avec une acrimonie due sans doute à la petite colère que me causait le contraste entre la nuance rose du paysage et la face hideuse de la bourgeoise chez qui l' on m' avait installé.", "Les anciens ne se souciaient nullement d' une émancipée dans leur maison.", "Je profitai d' une seconde où mon camarade me tournait le dos et frappait ses galoches contre un tronc d' arbre afin de faire tomber la neige amassée entre le talon et la semelle de bois.", "Nous avons besoin de sous pour acheter du fourbi ; eh bien !", "Je vous expliquerai tout cela et, en relisant vos notes ensemble, nous arrêterons le relatif dont j' ai besoin.", "Il copia son maintien sur celui de de Marsay, le fameux dandy parisien, en tenant d' une main sa canne et son chapeau qu' il ne quitta pas, et il se servit de l' autre pour faire des gestes rares à l' aide desquels il commenta ses phrases."):
            # mauvais parsing
            # TODO
            return False, False, ""
        elif sentence == "Il n' est pas dans la nature de passion plus diaboliquement impatiente que celle d' un homme qui, frissonnant sur l' arête d' un précipice, rêve de s'y jeter.":
            return True, True, grew[31]
        elif grew[-1][1] == "?" and grew[item[3] + 1][4] == "PROREL":
            return True, True, grew[item[3] + 1]
        elif grew[item[3] + 1][4]  in ('P', 'P+D', "P+PRO"):
            x = item[3] + 1
            item = (grew[x][1], grew[x][3], grew[x][5], x)
            return getObjet(item, grew, talismane, sentence)
        elif grew[item[3] + 1][4] == "ADV" and grew[item[3] + 2][4]  in ('P', 'P+D', "P+PRO"):
            x = item[3] + 2
            item = (grew[x][1], grew[x][3], grew[x][5], x)
            return getObjet(item, grew, talismane, sentence)
        if len(grew)-2 <= item[3]:
            if grew[item[3] + 1][4] in ("DET", "ADV") and (grew[item[3] + 2][4] in ("NC", "NPP") or stanford[item[3] + 2][4] == "NOUN"):
                return True, True, grew[item[3] + 2]
            elif grew[item[3] + 1][4] in ("ADJ") and (grew[item[3] + 2][4] in ("NC", "NPP") or stanford[item[3] + 2][4] == "NOUN"):
                return True, True, grew[item[3] + 2]
            elif len ( grew ) - 3 <= item[3] :
                if grew[item[3] + 1][4] == "DET" and grew[item[3] + 2][4] == "ADV" and grew[item[3] + 3][4] == "ADJ" and \
                        grew[item[3] + 4][4] in ("NC", "NPP"):
                    return True, True, grew[item[3] + 4]
                elif grew[item[3] + 1][4] == "ADJ" and grew[item[3] + 2][4] == "DET" and grew[item[3] + 3][4] == "NC":
                    return True, True, grew[item[3] + 3]
                elif grew[item[3] + 1][4] in ("DET", "ADV") and grew[item[3] + 2][4] in ("ADJ", "DET") and grew[item[3] + 3][
                    4] == "NC":
                    return True, True, grew[item[3] + 3]

    for k in range(item[3]+1, len(grew)):
        if grew[k][7] == 'obj.p' and grew[item[3]][0] == grew[k][6]:
            return True, True, grew[k]
    return False, False, ""
    if done:
        infos_verbe["liens"]["de_obj"].remove(item)
    else:
        assert grew[item[3]][1] != "en"
    return False, False, ""

def getSujet(entrée_grew_verbe, grew):
    for i in range(len(grew)):
        if grew[i][7] == 'suj' and entrée_grew_verbe[0] == grew[i][6]:
            return grew[i]
    return False

def findTree(lvf_info_base, ref, orig = []):
    ttake = True
    lvf_info_tmp = []
    lvf_info_base_cp = copy.deepcopy(lvf_info_base)
    bobj = False
    bcirc = False
    bcomp = False
    if "objet" in ref:
        bobj = True
    if "circonstant" in ref:
        bcirc = True
    if "comp" in ref:
        bcomp = True

    def refresh():
        to_del = []
        compl_prep = False
        for info_key in lvf_info_base_cp.keys():
            for i_a, associated_values in enumerate(lvf_info_base_cp[info_key]):
                if not bcomp and not bcirc and "compl-prep" in associated_values[1].keys():
                    to_del.append((info_key, i_a))
                elif not bobj and "objet" in associated_values[1].keys():
                    to_del.append((info_key, i_a))
                elif bobj and not "objet" in associated_values[1].keys():
                    to_del.append((info_key, i_a))
                if "compl-prep" in associated_values[1].keys():
                    compl_prep = True

        to_del.reverse()
        for info_key, i_a in to_del:
            lvf_info_base_cp[info_key].pop(i_a)
            if len(lvf_info_base_cp[info_key]) == 0:
                del lvf_info_base_cp[info_key]
        if len(lvf_info_base_cp.keys()) == 1:
            for key in lvf_info_base_cp.keys():
                if len(lvf_info_base_cp[key]) == 1:
                    return lvf_info_base_cp, compl_prep
                else:
                    return False, compl_prep
        return False, compl_prep

    done, compl_prep = refresh()
    if done:
        return True, done

    if compl_prep:
        done, compl_prep = refresh()
        if done:
            return True, done

    if orig != []:
        if not compl_prep:
            for arbre in orig.keys():
                if "compl-prep" in orig[arbre]:
                    orig[arbre]['circonstant'] = orig[arbre]["compl-prep"]
                    del orig[arbre]['compl-prep']

        for arbre in orig.keys():
            for it_org in orig[arbre].keys():
                to_del = []
                for item_t in list(orig[arbre][it_org]):
                    if len(item_t) == 10 and item_t[3] == "PONCT":
                        to_del.append(item_t)
                    if len(item_t) == 2 and len(item_t[1]) == 10 and item_t[1][3] == "PONCT":
                        to_del.append(item_t)
                    for to_d in to_del:
                        orig[arbre][it_org].remove(to_d)

            for it_org in list(orig[arbre].keys()):

                if len(orig[arbre][it_org]) ==0 :
                    del orig[arbre][it_org]

    done, compl_prep = refresh()
    if done:
        return done

    compl_prep = False

    for info_key in lvf_info_base_cp.keys():
        for i_a, associated_values in enumerate(lvf_info_base_cp[info_key]):
            if not bcomp and not bcirc and "compl-prep" in associated_values[1].keys():
                to_del.append((info_key, i_a))
            elif not bobj and "objet" in associated_values[1].keys():
                to_del.append((info_key, i_a))
            elif bobj and not "objet" in associated_values[1].keys():
                to_del.append((info_key, i_a))
            elif "compl-prep" in associated_values[1].keys():
                compl_prep = True

    if orig != []:

        for arbre in orig.keys():
            for value in "circonstant", "compl-prep":
                if value in orig[arbre]:
                    if len(orig[arbre][value])==0:
                        del orig[arbre][value]
                if value in orig[arbre]:
                    if len(orig[arbre][value][0]) == 2:
                        circonstant = orig[arbre][value][0][0]
                    elif len(orig[arbre][value][0]) == 10:
                        circonstant = orig[arbre][value][0][2].lower()
                        if orig[arbre][value][0][4] in ('P', 'P+D', "P+PRO") or orig[arbre][value][0][3] == "clo":
                            if (("à" in circonstant or "au" in circonstant) and len(circonstant) <= 4 and circonstant not in ("ad", "avec")) or orig[arbre][value][0][3] == "clo":
                                circonstant = "à"
                            elif ("de" in circonstant or "du" in circonstant) and len(circonstant) <= 4:
                                circonstant = "de"
                        x = getTGIndex(orig[arbre][value][0][0])
                        item = (grew[x][1], grew[x][3], grew[x][5], x)
                        done, take, résulat = getObjet(item, grew, talismane, sentence)
                        if not take:
                            return False, {}
                        orig[arbre][value][0] = (circonstant, résulat)
                    else:
                        raise Exception
                    for info_key in lvf_info_base_cp.keys():
                        for i_a, associated_values in enumerate(lvf_info_base_cp[info_key]):
                            if 'compl-prep'  in associated_values[1].keys():
                                if circonstant in associated_values[1]['compl-prep'].split(", "):
                                    orig[arbre]['compl-prep'] = orig[arbre][value]
                                    if value == "circonstant":
                                        del orig[arbre]['circonstant']
                                    return True, {info_key:[associated_values]}
    else:
        possibilités = lvf_info_base_cp

        lvf_info = {}
        for info_key in lvf_info_base.keys():
            for item in lvf_info_base[info_key]:
                if item[0] in possibilités:
                    if info_key not in lvf_info.keys():
                        lvf_info[info_key] = []
                    lvf_info[info_key].append(item)

        lvf_info_tmp = filter(lvf_info)

    if (len(lvf_info_tmp) > 1 or len(lvf_info_tmp)==0):
        if ref in table_des_arbres.keys():
            possibilités_globales = set(k[0] for k in sorted(table_des_arbres[ref].items(), key=lambda kv: kv[1]))
        else:
            possibilités_globales = set()
        possibilités_locales = set()

        for items in lvf_info_base.values():
            for item in items:
                possibilités_locales.add(item[0])

        possibilités = possibilités_locales.intersection(possibilités_globales)
        lvf_info = {}
        for info_key in lvf_info_base.keys():
            for item in lvf_info_base[info_key]:
                if item[0] in possibilités:
                    if info_key not in lvf_info.keys():
                        lvf_info[info_key] = []
                    lvf_info[info_key].append(item)

        lvf_info_tmp = filter(lvf_info)

        if len(possibilités) == 0 and (len(lvf_info_tmp) > 1 or len(lvf_info_tmp)==0):
            ttake = False

    return ttake, lvf_info_tmp

def getTAG(from_index, label, grew, table_labels):
    real_index = getTGIndex(grew[from_index][6])
    for x in sorted(list(range(real_index+1)), reverse=True):
        obj = grew[x]
        if obj[3] == label:
            return obj
    return False

#return l'index d'un label donné
def getTGIndex(label_index):
    if label_index.isdigit() and int(label_index) < len(grew) and grew[int(label_index)-1][0] == label_index:
        return int(label_index)-1
    for i in range(len(grew)):
        if grew[i][0] == label_index:
            return i
    for i in range(len(grew)):
        if talismane[i][0] == label_index:
            return i
    return -1

def getSupportRelative(label_index):
    for k in label_index.split(", "):
        for i in range(len(grew)):
            if "obj.cpl" in  grew[i][7] or "mod.rel" in  grew[i][7]:
                for k_i in grew[i][6].split(", "):
                    if k_i == k:
                        return grew[i]
    indexTGI = getTGIndex(label_index)
    indexTGI = getTGIndex(grew[indexTGI][6])
    if not talismane[indexTGI][7].startswith("aux"):
        if talismane[indexTGI][3].startswith("V"):
            return grew[indexTGI]
    if grew[indexTGI][7] in ("mod.rel", 'mod_cleft'):
        return  grew[indexTGI]
    elif talismane[indexTGI][7] in ('mod_rel', 'sub'):
        return  grew[indexTGI]
    elif talismane[indexTGI][7].startswith("aux"):
        indexTGI =  getTGIndex(talismane[indexTGI][6])
        if talismane[indexTGI][7] in ('mod_rel', 'sub'):
            return grew[indexTGI]
    if talismane[indexTGI][7] == "suj":
        return getSupportRelative(talismane[indexTGI][0])
    return False


def nettoiePandD(data):
    data_dbg = data
    data = copy.deepcopy(data)
    data[3] = "D"
    data[4] = "DET"
    data[7] = "det"
    data[2] = data[2].lower()
    if data[2][-3:] in ("des", "aux") or data[1][-3:].lower() in ('des', 'aux'):
        data[2] = "les"
        data[5] = "n=p"
    elif data[2][-2:] in ('du', 'au') or data[1][-2:].lower() in ('du', 'au'):
        data[2] = "le"
        data[5] = "n=s|g=m"
    else:
        return False
    return data


def setConjugaison(conj_poss, list_conj_real, value, new_value) :
    if conj_poss == value :
        for situation in list_conj_real.keys () :
            for item in list_conj_real[situation] :
                item['temps'] = new_value

progress = tqdm(range(len(sentences_dbs)))
for i_book_file, sentences_file in enumerate(sentences_dbs):

    with open(sentences_file, 'r') as infile:
        analyses_db = json.load(infile)
        progress2 = tqdm(range(len(analyses_db)))
        for i_analyse, analyses in enumerate(analyses_db):
            if 'Title' in analyses['metas'].keys():
                titre = analyses['metas']['Title']
            elif analyses['metas']['source'] == 'books/15579-8.txt':
                titre = "L'affaire Lerouge"
                analyses['metas']['Title'] = "L'affaire Lerouge"
                analyses['metas']['Author'] = "Émile Gaboriau"
            else:
                titre = 'Titre non relevé'
            if 'Author' in analyses['metas'].keys():
                auteur = analyses['metas']['Author']
            elif analyses['metas']['source'] == 'books/35969-0.txt':
                auteur = 'anonyme, traduction Antoine Galland'
            else:
                auteur = 'Auteur non relevé'
            source = analyses['metas']['source']
            origine_texte = source
            titre_texte = titre
            local_file = sentences_file
            sentence = analyses['sentence']
            talismane = analyses['talismane']
            grew = analyses['grew']
            stanford = analyses['stanford']

            tokens_dones = [False] * len(grew)

            take = analyses['metas']['source'].split('/')[-1] not in exclusions_list
            if sentence in (
            "Sertorius répond de très haut, sans habiletés d' avocat et sans précautions d' homme d' affaires.", "Dunkerque fut le port où, le 21 mai 1717, descendit Pierre Ier, accompagné de sa suite.",
            'La foule encombrait les rives du fleuve ; les femmes étaient sans voile et elles se déchiraient le visage de douleur.',
            'Sur votre expérience appuyant ma foiblesse, Peut-être je pourrai d\' une folle tendresse Prévenir les retours jaloux.',
            'Les faibles déplaisirs s\' amusent à parler, Et quiconque se plaint cherche à se consoler.',
            'Çà et là, des chaloupes faisaient des points téméraires.',
            'Les Gouverneurs alors se chargeoient de la fourniture du pain de munition pour leurs Garnisons, & celui-ci ayant remarqué que celui que St.',
            'Malgré la forte brise, ils établissent chacun leurs flèches, téméraires et se défiant dans leur marche de front.',
            "Espère-t-on donc qu' une fille de seize ans ignore l' existence de l' amour ?",
            'Notamment le cléricalisme, dont les injures lui ont refait une virginité.') or "desirer" in sentence or sentence.startswith(
                'Page') or sentence.endswith(" fig.") or sentence.endswith("etc."):
                take = False
                if debug: log(getframeinfo(currentframe()).lineno)

            # on passera ensuite tous les verbes conjugués, place aux infinitifs.
            if debug:
                take = take or "Le Piccinino" == titre
                take = take or "En Russie, elle eut le sort de toutes les cantinières de l' armée : elle perdit chevaux, voitures, lingots, fourrures et son protecteur." == sentence
                take =  i_analyse >= start_pass_i_analyse_from

            if take:

                def getAux(i_v_base, table_aux=[], base = '', i_base_index = '', ):
                    if base == '':
                        i_base_index = i_v_base
                        base = grew[i_v_base][5]
                    for i_aux in sorted(list(range(i_v_base)), reverse=True):
                        if grew[i_aux][7] == 'suj' and grew[-1][2] != "?":
                            return copy.deepcopy ( table_aux )
                        if not grew[i_aux][7].startswith('aux'):
                            if talismane[i_aux][7].startswith('aux'):
                                grew[i_aux][6] = talismane[i_aux][6]
                                grew[i_aux][7] = talismane[i_aux][7].replace("_", ".")
                        grew[i_aux][7] = grew[i_aux][7].replace("_", ".")
                        if grew[i_aux][7].startswith('aux') and (grew[i_aux][6] == "_" or getTGIndex(grew[i_aux][6])==i_v_base):
                            if  grew[i_aux][4] != "VINF" and grew[i_base_index][5] != grew[i_aux][5]:# grew[i_aux][7] not in ('aux.caus', 'aux.mod') :
                                table_aux.append(grew[i_aux])
                            return getAux(i_aux, table_aux, base, i_base_index)
                        if grew[i_aux][7] == 'suj' and not((table_aux==[] and 'm=part' in base) or "?" == grew[-1][1]): #on fera le ménage après.
                            break
                    return copy.deepcopy(table_aux)

                # grew va servir de référence, on raligne les pointeurs des dépendances

                for item in (stanford, talismane):
                    for i_talismane_1 in range(len(item)):
                        if "," in item[i_talismane_1][0]:
                            refs = item[i_talismane_1][0].split(", ")
                            a = sorted(set(refs).difference(set(item[i_talismane_1][6].split(", "))))
                            b = sorted(set(item[i_talismane_1][6].split(", ")).difference(set(refs)))

                            b = b [0]
                            refB = item[i_talismane_1][6].split(", ")

                            for i in (4, 3, 6, 7, 8, 9):
                                if item[i_talismane_1][i] != "_":
                                    index = min(refB.index(b), len(item[i_talismane_1][i].split(", "))-1)
                                    item[i_talismane_1][i] = item[i_talismane_1][i].split(", ")[index]
                            for i_talismane_2 in range(len(item)):
                                if item[i_talismane_2][6] in refs:
                                    item[i_talismane_2][6] = a[0]
                            item[i_talismane_1][0] = a[0]

                for item in (stanford, talismane):
                    for i_grew in range(len(grew)):
                        source = grew[i_grew]
                        if source[0] != item[i_grew][0]:
                            old_ref = item[i_grew][0]
                            item[i_grew][0] = source[0]
                            for i in range(len(item)):
                                for j in (6, 8):
                                    if item[i][j] != "_":
                                        if item[i][j] == old_ref:
                                            item[i][j] = "c"+item[i_grew][0]
                    for i in range(len(item)):
                        for j in (6, 8):
                            if item[i][j] != "_":
                                if "c" in item[i][j]:
                                    item[i][j] = item[i][j].replace("c", "")

                # on teste l'accord suj grew/talismane
                for item_1, item_2 in zip(talismane, grew):
                    if item_1[-1] == "suj" or item_2[7] == "suj":
                        if item_2[7] == 'suj' and item_1[-1]  == "_":
                            item_1[-1] = 'suj'
                            item_1[6] = item_2[6]
                            if item_1[7]  == "_":
                                item_1[7] = "suj"
                        if item_2[7] == '_' and item_1[-1]  == "suj":
                            item_2[7] = 'suj'
                            item_2[6] = item_1[6]
                            item_2[9] = item_1[9]
                        # if not (item_1[-1] == "suj" or item_1[2].lower() in ("dont", "où", "qui")) and item_2[7] == "suj":
                        #     take = False
                        #     if debug: log(getframeinfo(currentframe()).lineno)
                        #     break
                for i in range(len(grew)):
                    if not "obj" in talismane[i][7] :
                        talismane[i][7] = talismane[i][7].replace("_", ".")
                    if not "obj" in grew[i][7] :
                        grew[i][7] = grew[i][7].replace("_", ".")
                    if not grew[i][7].startswith("aux") and talismane[i][7].startswith("aux") and talismane[i][7] != ("aux.caus"):
                        grew[i][7] = talismane[i][7]
                        grew[i][6] = talismane[i][6]

                # on s'occupe d'abord des infinitifs
                if take:
                    table_labels = []
                    verbes = []
                    infinifs = []
                    final_infinitifs = []
                    init_arbres= []
                    final_arbres = []


                    lvf_infos = []
                    pointeurs_verbes = []
                    éléments_liés = []

                    for i in range(len(stanford)):
                        table_labels.append(grew[i][0])

                        if grew[i][4] == ("VINF") and stanford[i][4] == ("VERB") and grew[i][7] not in ('ato', 'aux.caus'):
                            take_verbe = True
                            if grew[i][7] == 'dep.coord':
                                verbe_item = getTAG(i, "V", grew, table_labels)
                                if not verbe_item:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                if verbe_item[7] == "ato":
                                    take_verbe = False

                            if take_verbe:
                                verbes.append({"index": i, "label_index": grew[i][0]})
                                verbe = grew[i][1]
                                if verbe not in lvf_état_fini.keys():
                                    verbe = verbe.replace("î", "i").lower()
                                if verbe not in lvf_état_fini.keys():
                                    verbe = grew[i][2]
                                    if "*" in verbe:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                if verbe not in lvf_état_fini.keys():
                                    verbe = verbe.replace("û", "u").lower()
                                if verbe not in lvf_état_fini.keys():
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                lvf_infos.append(lvf_état_fini[verbe])
                                pointeurs_verbes.append(grew[i][0])
                                éléments_liés.append([])

                    init_arbres_infinitifs = []  # nombre de cadres verbaux possibles pour les verbes à l'infinitif
                    final_arbres_infinitifs = []  # nombre de cadres verbaux conservés pour les verbes à l'infinitif
                    infinifs = []  # index des verbes
                # verbes à l'infinitif
                if take and len(verbes) > 0:
                    # on va constituer les tables d'éléments liés
                    for i in range(len(stanford)):
                        if grew[i][6] in pointeurs_verbes:
                            éléments_liés[pointeurs_verbes.index(grew[i][6])].append(grew[i])

                    arbres_résultants = []
                    for verbe, lvf_info, pointeurs, éléments_lié in zip(verbes, lvf_infos, pointeurs_verbes,
                                                                        éléments_liés):

                        if grew[verbe["index"]][7] == 'dep.coord':
                            verbe_item = getTAG(verbe["index"], "V", grew, table_labels)
                            if not verbe_item:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)
                                break
                            else:
                                élément = copy.deepcopy(getSujet(verbe_item, grew))
                            if not élément:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)
                                break
                            else:
                                élément[6] = verbe["label_index"]
                                éléments_lié.append(élément)


                        elif grew[verbe["index"]][7] in ('obj.p', 'obj', "_", 'p_obj.o', 'ats'):

                            verbe_item = getTAG(verbe["index"], "V", grew, table_labels)
                            if verbe_item:
                                élément = False
                                if verbe_item[4] == 'VINF' and grew[verbe["index"]][4] == 'VINF':
                                    x = verbe["index"]
                                    item = (grew[x][1], grew[x][3], grew[x][5], x)
                                    # print('"'+sentence+'"')
                                    done, take, élément = getObjet(item, grew, talismane, sentence)
                                if not élément:
                                    élément = copy.deepcopy(getSujet(verbe_item, grew))
                                if not élément and verbe_item[4]=="VINF":
                                    x = verbe["index"]
                                    item =  (grew[x][1], grew[x][3], grew[x][5], x)
                                    done, take, élément = getObjet(item, grew, talismane, sentence)
                            #else:
                               # take = False
                               # if debug: log(getframeinfo(currentframe()).lineno)
                            if verbe_item:
                                #if not élément:
                                 #   take = False
                                 #   if debug: log(getframeinfo(currentframe()).lineno)
                                if take and élément:
                                    élément[6] = verbe["label_index"]
                                    éléments_lié.append(élément)

                            if not take:
                                if debug: log(getframeinfo(currentframe()).lineno)
                        else:
                            take = False
                            if debug: log(getframeinfo(currentframe()).lineno)

                        if take:
                            arbre = []
                            minuscules = []
                            refs_minuscules = {}
                            ref_comp_prep = []
                            ref_circonstant = []
                            ref_sujet = []
                            ref_objet = []

                            for ie, élément in enumerate(éléments_lié):
                                arbre.append("sujet" if élément[7] == "suj" else "objet" if élément[7] == "obj" else "")
                                if élément[7] == "suj":
                                    ref_sujet = élément
                                if élément[7] == "obj":
                                    ref_objet = élément
                                if élément[7] in ("a_obj", "de_obj"):
                                    arbre.append("compl-prep")
                                    élément[2] = élément[2].lower()
                                    if (("à" in élément[2] or "au" in élément[2]) and len(élément[2]) <= 4 and élément[2] not in ("ad", "avec")) or élément[3] == "clo":
                                        minuscules.append("a")
                                    elif élément[2] in ("devant"):
                                        minuscules.append("m")
                                    elif ("de" in élément[2] or "du" in élément[2]) and len(élément[2]) <= 4:
                                        minuscules.append("b")
                                    elif élément[2] in ("avec"):
                                        minuscules.append("c")
                                    elif élément[2] in ("contre"):
                                        minuscules.append("d")
                                    elif élément[2] in ("par"):
                                        minuscules.append("e")
                                    elif élément[2] in ("sur", "vers"):
                                        minuscules.append("g")
                                    elif élément[2] in ("dans"):
                                        minuscules.append("j")
                                    elif élément[2] in ("auprès"):
                                        minuscules.append("l")
                                    elif élément[2] in ("s=refl"):
                                        minuscules.append("p")
                                    else:
                                        # TODO
                                        # raise Exception
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    x = grew.index(élément)
                                    item =  (grew[x][1], grew[x][3], grew[x][5], x)
                                    done, take, résultat = getObjet(item, grew, talismane, sentence)
                                    if done:
                                        tokens_dones[item[-1]] = True
                                        éléments_lié[ie] = (élément[2], résultat)
                                    refs_minuscules[minuscules[-1]] = ie

                            ref = "".join(sorted(arbre)) + "".join(sorted(minuscules))

                            if "compl-prep" in ref:
                                if ref in table_des_arbres.keys() :
                                    ie = refs_minuscules[minuscules[-1]]
                                    ref_comp_prep = éléments_lié[ie]
                                else:
                                    arbre.append("circonstant")
                                    if len(minuscules) > 1:
                                        for minuscule in minuscules:
                                            ref = "".join(sorted(arbre)) + minuscule
                                            if ref in table_des_arbres.keys():
                                                ie = refs_minuscules[minuscule]
                                                del refs_minuscules[minuscule]
                                                ref_comp_prep = éléments_lié[ie]
                                                ie = refs_minuscules.pop()
                                                ref_circonstant = éléments_lié[ie]
                                                break
                                    else:
                                        arbre.remove("compl-prep")
                                        refs_minuscules = {}
                                        if ref in table_des_arbres.keys():
                                            ref = "".join(sorted(arbre))
                                            del refs_minuscules[minuscule]
                                            ie = refs_minuscules[minuscules[-1]]
                                            ref_circonstant = éléments_lié[ie]


                            if ref not in table_des_arbres.keys() and not save_arbres:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)
                            if take :
                                take, lvf_info_tmp = findTree(lvf_info, ref)
                                name = False
                                for type in lvf_info_tmp:
                                    tree = lvf_info_tmp[type][0]
                                    name = tree[0]
                                    dic_tree = {k:'' for k in tree[1].keys()}
                                    to_del = []
                                    for elt in dic_tree.keys():
                                        if elt == "sujet":
                                            dic_tree[elt] = (tree[1][elt], ref_sujet)
                                            if ref_sujet == []:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                        elif elt == "objet":
                                            dic_tree[elt] = (tree[1][elt], ref_objet)
                                            if ref_objet == []:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                        elif elt == "compl-prep":
                                            dic_tree[elt] = (tree[1][elt], ref_comp_prep)
                                            if ref_comp_prep == []:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                        elif elt == "circonstant":
                                            if ref_circonstant != []:
                                                dic_tree[elt] = (tree[1][elt], ref_circonstant)
                                            else:
                                                to_del.append(elt)
                                    for item in to_del:
                                        del dic_tree[item]
                                if name:
                                    arbres_résultants.append({name:dic_tree})
                                else:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                    if take:
                        final_infinitifs = copy.deepcopy(arbres_résultants) # <class 'list'>: [{'T1400': {'sujet': ('humain', ['2', 'reine', 'reine|roi', 'N', 'NC', 's=c', '3', 'suj', '_', '_']), 'objet': ('complétive ou chose', ['8', 'fût', 'être', 'V', 'VS', 'm=subj', '5', 'obj.cpl', '_', '_'])}}]

                        for verbe, lvf_info in zip(copy.deepcopy(verbes), copy.deepcopy(lvf_infos)):
                            table_aux = getAux(verbe["index"], [])
                            addTypeEvent(sentence, grew[verbe["index"]][0], ("infinitif", table_aux))
                            infinifs.append(verbe['index'])
                            count = 0
                            for type_possible in lvf_info.keys():
                                count += len(lvf_info[type_possible])
                            init_arbres_infinitifs.append(count)
                            final_arbres_infinitifs.append(1)


                verbes = []
                # place aux verbes conjugués
                # fonction qui sera utile pour remonter les chaînes
                def getIndex(repère, dones=[]):
                    if repère in dones:
                        return []
                    dones.append(repère)
                    for i in range(len(grew)):
                        if grew[i][0] == repère:
                            if grew[i][4] == "V":
                                for j in range (len(grew)):
                                    if talismane[j][0] == grew[i][6] and grew[j][3] == "C":
                                        return getIndex(talismane[j][6], dones)
                                return i
                            if talismane[i][4] == "V":
                                for j in range (len(grew)):
                                    if talismane[j][0] == grew[i][6] and grew[j][3] == "C":
                                        return getIndex(talismane[j][6], dones)
                                return i
                            if talismane[i][6] == "":
                                return getIndex(grew[i][6], dones)
                            else:
                                return getIndex(talismane[i][6], dones)
                    return []

                def getSol(i_sol):

                    if grew[i_sol][4] in ("ADJ", "CLR"):
                        sol_6 = talismane[i_sol][6].lower()  # meilleure ref que Grew pour les adverbes
                        sol_7 = talismane[i_sol][7].lower()
                    else:
                        sol_6 = grew[i_sol][6].lower()
                        sol_7 = grew[i_sol][7].lower()

                        if sol_6 == "_":
                            sol_6 = talismane[i_sol][6].lower()
                        if sol_7 == "_":
                            sol_7 = talismane[i_sol][7].lower()

                    sol_1 = grew[i_sol][1].lower()
                    sol_4 = grew[i_sol][4].lower()
                    sol_5 = grew[i_sol][5].lower()

                    if sol_5 == "_":
                        sol_5 = talismane[i_sol][5].lower()
                    if sol_4 == "_":
                        sol_4 = talismane[i_sol][4].lower()
                    return sol_1, sol_4, sol_5, sol_6, sol_7

                verbes = []
                verbes_type_copule  = []
                verbes_sentence = {} # pour ranger les verbes traités
                init_arbres = []
                final_arbres = []

                finals = []

                if "il y a" in sentence.lower():
                    take = False
                    if debug: log(getframeinfo(currentframe()).lineno)

                if take:
                    for i in range(len(stanford)):

                        # on vire les il y a trop casse-pieds, non analysés au passé, différences de istuation particulières
                        # à gérer (il y a très longtemps // il y a dans chaque cillage des rivalités // il y a du grain...)
                        if (grew[i][7] not in ('aux.tps', 'aux.pass') or (grew[i][7] == 'aux.tps' and grew[i][2] not in ("être", "avoir"))) and  grew[i][4] in  ("V", "VPR", "VPP", "VS") and not "il y a" in sentence.lower():
                            if grew[i][2] != '*il_y_er':
                                verbes.append(i)
                                if "|" in grew[i][2]:
                                    if grew[i][1].lower() == "faut":
                                        grew[i][2] = "falloir"
                                    elif  grew[i][2].lower() == 'étayer|être':
                                        grew[i][2] = "être"
                                    elif  grew[i][2].lower() == 'bruire|bruisser':
                                        grew[i][2] = "bruisser"
                                    elif  grew[i][2].lower() == 'ouvrer|ouvrir':
                                        grew[i][2] = "ouvrir"
                                    elif  grew[i][2].lower() == 'saurer|savoir' and grew[i][1].lower() == "saurait":
                                        grew[i][2] = "savoir"
                                    elif  grew[i][2].lower() == 'frire|friser' and grew[i][1] == 'frisent':
                                        grew[i][2] = "friser"
                                    else:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                elif "*" in grew[i][2]:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                            #recherche de copules
                            # if grew[i][2] in ("être", "apparaître", "demeurer", "devenir", "appeler", "choisir", "considérer", "créer", "désigner", "dire", "élire", "faire", "prendre", "proclamer", "réputer", "tenir", "traiter", "paraître", "passer", "rester", "appeler", "avérer", "sembler", "montrer", "nommer", "trouver", "tomber", "venir", '*il_y_er'):
                            #     indexes = grew[i][0].split(", ")
                            #     verbes_type_copule.append(i)
                            #     for i_grew in range(len(grew)):
                            #         pointeurs = grew[i_grew][6].split(", ")
                            #         for pointeur in pointeurs:
                            #             if pointeur in indexes and ("ats" == grew[i_grew][7] or grew[i][2] =="être"):
                            #                 verbes.remove(i)
                            #                 break
                            #         if i not in verbes:
                            #             break {...
                obj_cpls = {}
                if take:
                    # for verbe_type_copule in verbes_type_copule:
                    #     i_verbe = -1
                    #     if len(verbe_type_copule) == 3:
                    #         verbe = verbe_type_copule[1][2]
                    #         i_verbe = talismane.index(verbe_type_copule[1])
                    #     if len(verbe_type_copule) == 2:
                    #         if verbe_type_copule[0] in talismane:
                    #             verbes_type_copule.remove(verbe_type_copule)
                    #             i_verbe = talismane.index(verbe_type_copule[0])
                    #     if i_verbe > -1 and i_verbe in verbes:
                    #         verbes.remove(i_verbe)
                    lvf_infos = []
                    pointeurs_verbes = []

                    for i_verbe in verbes:
                        pointeurs_verbes.append(grew[i_verbe][0])
                        if grew[i_verbe][2] in lvf_état_fini.keys() and  grew[i_verbe][4] in ("V", "VPR", "VPP", "VS") and talismane[i_verbe][4] in ("V", "VPR", "VPP", "VS"):
                            lvf_infos.append(lvf_état_fini[grew[i_verbe][2]])
                        elif grew[i_verbe][4] in  ("V", "VPR", "VPP", "VS") and talismane[i_verbe][4] in ("V", "VPR", "VPP", "VS") and talismane[i_verbe][2] in lvf_état_fini.keys():
                            lvf_infos.append(lvf_état_fini[talismane[i_verbe][2]])
                        else:
                            take = False
                            if debug: log(getframeinfo(currentframe()).lineno)
                            break

                obj_cpls = {}
                if take and len(verbes)>0:
                    infos_verbes = {}
                    obj_cpls = {}
                    for i_grew in range(len(grew)):

                        indexes = grew[i_grew][0].split(", ")
                        pointeurs = grew[i_grew][6].split(", ")

                        if pointeurs == ['_'] and talismane[i_grew][6].split(", ") != "":
                            potential_pointeurs = talismane[i_grew][6].split(", ")

                            pivot = True
                            if '0' in potential_pointeurs:
                                potential_pointeurs.remove('0')
                            if potential_pointeurs == ['_']  and grew[i_grew][5] == 's=refl':
                                if i_grew < len(grew) -1 and grew[i_grew+1][3] == "V":
                                    potential_pointeurs = [i_grew+2]
                            else:
                                nature_pot = talismane[i_grew][7]
                                pivot = True
                                for k in [l + i_grew + 1 for l in range(len(grew) - i_grew - 1)]:
                                    if talismane[i_grew][6] == grew[k][6] and (nature_pot == grew[k][7] or nature_pot == talismane[k][7]):
                                        pivot = False
                                        break

                            if pivot:
                                pointeurs = potential_pointeurs

                        intersections = set(pointeurs).intersection(set(pointeurs_verbes))

                        if intersections or indexes[0] in pointeurs_verbes:
                            sol_1, sol_4, sol_5, sol_6, sol_7 = getSol(i_grew)

                        if intersections:
                            for intersection in intersections:
                                index = pointeurs_verbes.index(intersection)
                                if index not in infos_verbes:
                                    infos_verbes[index] = {'liens': {}}
                                if sol_7 not in infos_verbes[index]['liens'].keys():
                                    infos_verbes[index]['liens'][sol_7] = []
                                if sol_7 in infos_verbes[index]['liens'] and len(infos_verbes[index]['liens'][sol_7])>0:
                                    if grew[infos_verbes[index]['liens'][sol_7][0][-1]] == "_"  and sol_7 not in ('det', ): # entre les deux je prends grew
                                        infos_verbes[index]['liens'][sol_7] = [(sol_1, sol_4, sol_5, i_grew)]
                                    elif sol_7 == "suj":
                                        diffVerb1 = infos_verbes[index]['liens'][sol_7][0][-1]
                                        diffVerb2 = i_grew
                                        # TODO
                                        # avec des séquences de tags se serait mieux
                                        # rrr comment détecter les énumérations, si je perd le premier tag, ma détection ne fonctionenra plus
                                        seq = []
                                        if diffVerb1<diffVerb2 and not verbes[index]<diffVerb2:
                                            for i_diff in [diffVerb1 + idiff for idiff in range(diffVerb2-diffVerb1)]:
                                                if grew[i_diff][3][0] in ("N", "A", 'D') or grew[i_diff][1] == ","\
                                                        or grew[i_diff][4] == "CC" or (talismane[i_diff][7] == "dep" and "PRO" not in talismane[i_diff][4]):
                                                    seq.append(grew[i_diff][1])

                                        if len(seq) != diffVerb2-diffVerb1:
                                            if sol_1 in ("j", "je", "tu") or ((i_grew + 1 == verbes[index] or (grew[i_grew+1][4] != "PONCT" and i_grew + 2 == verbes[index])) and not (grew[-1][1] == "?" and sol_4 == "cls") and not(sol_4 == "cls" and sol_1 == "t'")  and not (sol_4 == "prorel")):
                                                infos_verbes[index]['liens'][sol_7] = [(sol_1, sol_4, sol_5, i_grew)]
                                            elif grew[diffVerb1][1] in ("j", "je", "tu"):
                                                pass
                                            elif not (sol_4 == "cls" and grew[diffVerb1][4]=="CLS")  and (not (grew[-1][1] == "?" and sol_4 == "cls") and (not grew[-1][1] == "?" and not diffVerb2>verbes[index]) and not (sol_4 == "prorel") and not(sol_4 == "cls" and sol_1 == "t'")):# non décidable
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                break
                                elif  sol_7 not in ('det', ):
                                    infos_verbes[index]['liens'][sol_7].append((sol_1, sol_4, sol_5, i_grew))

                        if indexes[0] in pointeurs_verbes:
                            index = pointeurs_verbes.index(indexes[0])
                            if index not in infos_verbes:
                                infos_verbes[index] = {'liens':{}}
                            cibles_pointées = []
                            if len(pointeurs) > 0:
                                for pointe in range(len(grew)):
                                    if pointe != i_grew:
                                        if len(set(grew[pointe][0].split(", ")).intersection(set(pointeurs))) > 0:
                                            if talismane[pointe][6] == "_":
                                                cibles_pointées.append((grew[pointe], pointe))
                                            elif talismane[pointe][6] not in ("", "0"):
                                                cibles_pointées.append((talismane[pointe], pointe))
                                            else:
                                                cibles_pointées.append((grew[pointe], pointe))
                                            break
                            if len(cibles_pointées) > 1:
                                raise Exception
                            elif len(cibles_pointées) == 1:
                                cibles_pointées = cibles_pointées[0]
                            req_index = indexes[0]
                            for r_inde in range(len(grew)):
                                if grew[r_inde][0] == indexes[0]:
                                    req_index = r_inde
                                    break
                            if req_index == indexes[0]:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)
                                break
                            infos_verbes[index]['socle'] = (sol_1, sol_4, sol_7, sol_5, req_index, cibles_pointées)
                            if sol_4 == ("vpp") and cibles_pointées != []:
                                if cibles_pointées[0][7] == "suj":
                                    infos_verbes[index]["suj"] = [cibles_pointées[0]]
                                    infos_verbes[index]['socle'] = (sol_1, sol_4, sol_7, sol_5, req_index, [])
                            if 'obj.cpl' == sol_7:
                                str_cpl = grew[cibles_pointées[-1]][6]
                                cpl_intro = grew[cibles_pointées[-1]][2] if grew[cibles_pointées[-1]][2] != '*se' else 'si'
                                i_cpl = -1
                                for i_cpl_g in range(len(grew)):
                                    if str_cpl == grew[i_cpl_g][0]:
                                        i_cpl = i_cpl_g
                                        break
                                if i_cpl == -1:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                                sol_1, sol_4, sol_5, sol_6, sol_7 = getSol(i_cpl)
                                if cpl_intro != "que":
                                    if "," in cpl_intro or cpl_intro in ('*quelques-uns', "*quelqu'_un", "*quelqu'_un, que", '*quecunque','*parce', '*parceque', '*peu-à-peu, dès_que',  '*simoniaque', '*tantost', '*tems',  "*tout_d'_abord, lorsque", '*tragi-comique','*très-chaud','*très-fréquemment, lorsque','très-remarquable, si','*très-souvent, que','*voyoucratique','*—', '*Éaque', '*Édith', '*Édouard',  '*Élisabeth', '*Émile', '*Énide', '*Éric', '*Éthel', '*Étienne',  '*Éva', "*aujourd'_hui, alors_que",  "*aujourd'_hui, parce_que",'*avecque',"*d'_abord, parce_que","*d'_ailleurs, en_même_temps_que", '*consequemment', '*Lui-même', '*Properce', '*Serassi', '*Télémaque', '*archi-sourde', '*en_ce_sens, que', '*en_question, chaque_fois_que', 'voir','à_peine, *se',  'à_présent, *se') :
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    if cpl_intro == "*Comme_s'":
                                        cpl_intro= "comme_si"
                                    elif cpl_intro == "*Même_s'":
                                        cpl_intro = "même_si"
                                    elif cpl_intro == "*S'":
                                        cpl_intro = "si"
                                    elif cpl_intro.startswith("*"):
                                        cpl_intro= cpl_intro[1:]
                                    grew[cibles_pointées[-1]][2] = cpl_intro
                                    cpls_intro.add(cpl_intro)
                                    obj_cpls[grew[infos_verbes[index]['socle'][4]][0]] = (cpl_intro, grew[i_cpl])
                                elif not "obj" in  infos_verbes[index]['liens'] and not "ats"  in  infos_verbes[index]['liens']:
                                    infos_verbes[index]['liens']['obj_cp'] = [((sol_1, sol_4, sol_5, i_cpl))]
                                    # TODO rajouter traitement liaison d'évènement

                    for index in infos_verbes.keys():
                        if "obj_cp" in infos_verbes[index]['liens']:
                            if not "obj" in infos_verbes[index]['liens']:
                                infos_verbes[index]['liens']['obj'] = infos_verbes[index]['liens']['obj_cp']
                                if grew[infos_verbes[index]['socle'][4]][0] in obj_cpls.keys():
                                    del obj_cpls[grew[infos_verbes[index]['socle'][4]][0]]
                            del infos_verbes[index]['liens']['obj_cp']
                            # TODO liaision d'évènement ex:quand(event_1, event_2)

                    for i_verbe in infos_verbes.keys():
                        infos_verbe = infos_verbes[i_verbe]
                        if not "obj" in infos_verbe['liens'].keys():
                            for i_grew in range(len(grew)):
                                if getTGIndex(stanford[i_grew][6]) == verbes[i_verbe] and stanford[i_grew][7]=='dobj':
                                    sol_1, sol_4, sol_5, sol_6, sol_7 = getSol(i_grew)
                                    if not (sol_7 in  infos_verbe['liens'].keys()) and sol_7 not in ('det', 'ponct'):
                                        infos_verbe['liens']['obj'] = [((sol_1, sol_4, sol_5, i_grew))]
                                        break

                if take and len(verbes)>0:

                    for i_verbe in verbes:
                        if grew[i_verbe][2] not in verbes_sentence.keys():
                            verbes_sentence[grew[i_verbe][2]] = {"décompte": 0, "vpp": 0, "vpr": 0, "autres": 0,
                                                                 "infinitif": 0, "cadres_initiaux": 0, "pertes": 0,
                                                                 "pertes_collatérales": 0, "cadres_finaux": [],
                                                                 "dispersion_autres": [], "dispersion_vpp": [],
                                                                 "dispersion_vpa": [], 'dispersion_infinitif': []}


                    def isVPP(verbe_info):
                        liste_keys = list(verbe_info["liens"].keys() )
                        for key in liste_keys:
                            if "_" in key:
                                verbe_info["liens"][key.replace("_", ".")] = verbe_info["liens"][key]
                                del verbe_info["liens"][key]
                        retour = verbe_info["socle"][1] == "vpp" and \
                               verbe_info["socle"][2] not in ("root", 'mod.rel') and not 'aux.pass' in verbe_info["liens"].keys() and \
                                 not 'aux.tps' in verbe_info["liens"].keys() and not 'aux_tps' in verbe_info["liens"].keys() and \
                               verbe_info['socle'][2] != 'aux.caus' # and 'm=part|t=past' not in verbe_info["socle"][3]
                        if retour :
                            return retour
                        return  retour

                    def getSuj(index, dones=[]):
                        dones.append(index)
                        if 'suj' not in infos_verbes[index]['liens'].keys():
                            if not isVPP(infos_verbes[index]):
                                if len(infos_verbes[index]['socle'][-1]) == 0:
                                    return False
                                i_référent = infos_verbes[index]['socle'][-1][-1]
                                suj_verbes_else = suj_verbes.difference(set([index]))
                                for index_else in suj_verbes_else:
                                    if infos_verbes[index_else]['socle'][4] == i_référent:
                                        if 'suj' in infos_verbes[index_else]['liens']:
                                            return infos_verbes[index_else]['liens']["suj"]
                                        else:
                                            if index_else in dones:
                                                return False
                                            else:
                                                return getIndex(index_else, dones)
                            else:
                                return 'vpr_or_vpp'
                        else:
                            return infos_verbes[index]['liens']["suj"]
                        return False

                    suj_verbes = set(infos_verbes.keys())
                    for index in suj_verbes:
                        if 'suj' not in infos_verbes[index]['liens'].keys() and not infos_verbes[index]["socle"][1] == "vpr" and not isVPP(infos_verbes[index]):
                            result = getSuj(index)
                            if result != "vpr_or_vpp":
                                if result == False or result == None or result == []:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                else:
                                    infos_verbes[index]['liens']["suj"] = result

                    if take:

                        take = True
                        base_verbes = copy.deepcopy(lvf_infos)

                        for index, i_verbe, lvf_info_base in zip(range(len(verbes)), verbes, lvf_infos):

                            pronominal = False

                            lvf_info = copy.deepcopy(lvf_info_base)
                            infos_verbe = copy.deepcopy(infos_verbes[index])
                            if len(infos_verbe["socle"][5]) > 0:
                                socle_référence_verbe = infos_verbe["socle"][5][0]
                            else:
                                socle_référence_verbe = []

                            bck_lvf_info = ""
                            key_pepa= ""

                            résultats = {}

                            if isVPP(infos_verbe):
                                table_aux = getAux(i_verbe, [])
                                addTypeEvent(sentence, grew[i_verbe][0], ("vpp", table_aux))
                                verbes_sentence[grew[i_verbe][2]]["vpp"] += 1
                                bck_lvf_info = lvf_info
                                lvf_info = {}
                                key_pepa = "pe"
                                lvf_info[key_pepa] = [['PE', {}]]
                                if infos_verbe["socle"][2] == "mod" and socle_référence_verbe!=[]:
                                   # if socle_référence_verbe[4] == "V":
                                    if "suj" in infos_verbe["liens"].keys():
                                        lvf_info[key_pepa][0][1]["sujet"] = []
                                        to_del = []
                                        for item in infos_verbe["liens"]["suj"]:
                                            lvf_info[key_pepa][0][1]["sujet"].append(grew[item[3]])
                                        del infos_verbe["liens"]["suj"]
                                    else:
                                        if infos_verbe["socle"][5][0][4] == "V":
                                            for i_g in [l  for l in range(infos_verbe["socle"][5][1])]:
                                                if (grew[i_g][7] == "suj" and grew[i_g][6] == infos_verbe["socle"][5][0][0]) \
                                                        or (talismane[i_g][7] == "suj" and talismane[i_g][6] == infos_verbe["socle"][5][0][0]):
                                                    # sujet trouvé
                                                    lvf_info[key_pepa][0][0] += "O"
                                                    if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                                        lvf_info[key_pepa][0][1]["sujet"] = []
                                                    lvf_info[key_pepa][0][1]["sujet"].append(grew[i_g])
                                                    break
                                        else :
                                            support = infos_verbe["socle"][5][0][6]
                                            if support not in ("_", ""):
                                                index_à_utiliser = []
                                                for i_g in [l  for l in range(infos_verbe["socle"][5][1])]:
                                                    if grew[i_g][0] == support:


                                                        for index_, tmp_socle in infos_verbes.items():
                                                            tmp_socle = tmp_socle["socle"]
                                                            if i_g == tmp_socle[4]:
                                                                index_à_utiliser.append(index_)
                                                if len(index_à_utiliser) == 1:
                                                    index_à_utiliser = index_à_utiliser[-1]
                                                elif len(index_à_utiliser) > 1:
                                                    index_à_utiliser = index_à_utiliser[-1]
                                                else:
                                                    index_à_utiliser = -1
                                                if index_à_utiliser > -1:
                                                    sujet = getSuj(index_à_utiliser)
                                                    # sujet trouvé
                                                    if sujet and sujet != 'vpr_or_vpp':
                                                        lvf_info[key_pepa][0][0] += "O"
                                                        if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                                        lvf_info[key_pepa][0][1]["sujet"].append(grew[sujet[0][3]])

                                    if "p_obj.agt" in infos_verbe["liens"].keys():


                                        objet = getObjet(infos_verbe["liens"]["p_obj.agt"][0], grew, talismane, stanford)
                                        if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                            if grew[i_verbe - 1][2] == ",":
                                                if len(finals) > 0:
                                                    refVerb = finals[-1][list(finals[-1].keys())[0]]
                                                    if "sujet" in refVerb:
                                                        lvf_info[key_pepa][0][1]["objet"] = refVerb["sujet"]
                                        else:
                                            lvf_info[key_pepa][0][1]["objet"] = lvf_info[key_pepa][0][1]["sujet"]
                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                        lvf_info[key_pepa][0][1]["sujet"].append(objet[2])

                                        #  raise Exception
                                    if socle_référence_verbe[4] != "V":
                                        if len(infos_verbe["socle"][5]) > 0:
                                            lvf_info[key_pepa][0][0] += "1"
                                            if grew[i_verbe - 1 ][2] != "," :
                                                if not "objet" in lvf_info[key_pepa][0][1].keys():
                                                    lvf_info[key_pepa][0][1]["objet"] = []
                                                if grew[i_verbe][6] == "_":
                                                    lvf_info[key_pepa][0][1]["objet"].append(socle_référence_verbe)
                                                else:
                                                    lvf_info[key_pepa][0][1]["objet"].append(grew[getTGIndex(grew[i_verbe][6])])
                                            else:
                                                if not "objet" in lvf_info[key_pepa][0][1].keys():
                                                    lvf_info[key_pepa][0][1]["objet"] = [grew[getTGIndex(grew[i_verbe][6])]]
                                        else:
                                            raise Exception

                                elif len(infos_verbe["liens"].keys()) == 0:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                else:
                                    # TODO
                                   # raise Exception
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                            if infos_verbe["socle"][1].lower() == "vpr":
                                table_aux = getAux(i_verbe, [])
                                addTypeEvent(sentence, grew[i_verbe][0], ("vpa", table_aux))
                                verbes_sentence[grew[i_verbe][2]]["vpr"] += 1
                                bck_lvf_info = lvf_info
                                lvf_info = {}
                                key_pepa = "pa"
                                lvf_info[key_pepa] = [['PA', {}]]
                                if socle_référence_verbe != []:
                                    if socle_référence_verbe[3] == "V":
                                        repère = infos_verbe["socle"][-1][-1]
                                    else:
                                        repère = getIndex(socle_référence_verbe[0])
                                else:
                                    repère = []
                                if repère != [] and socle_référence_verbe[4] in("NC", "N"):
                                    if len(infos_verbe["socle"][5]) > 0:
                                        lvf_info[key_pepa][0][0] += "O"
                                        if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                        lvf_info[key_pepa][0][1]["sujet"].append(socle_référence_verbe)
                                elif repère != [] and socle_référence_verbe[4] in("V"):
                                    for i_g in range(len(grew)):
                                        if grew[i_g][7] == "suj" and grew[i_g][6] == grew[repère][0]:
                                            # sujet trouvé
                                            lvf_info[key_pepa][0][0] += "O"
                                            if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["sujet"] = []
                                            lvf_info[key_pepa][0][1]["sujet"].append(grew[i_g])
                                            break
                                    if not "sujet" in lvf_info[key_pepa][0][1].keys() and grew[repère][3] == "N":
                                        if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                        lvf_info[key_pepa][0][1]["sujet"].append(grew[repère])

                                elif repère == [] or grew[repère][4] == "CC" or grew[repère][3] == "N":
                                    # l'identification du support du PPA est catastrophique dans le parsing
                                    if repère == []:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    elif grew[repère][3] == "N":
                                        if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["sujet"] = []
                                        lvf_info[key_pepa][0][1]["sujet"].append(grew[repère])
                                    else:
                                        raise Exception
                                elif grew[repère][4] in ("P", "P+D", "P+PRO", "V"):

                                    if grew[repère][4] in ('P', 'P+D', "P+PRO"):
                                        repère = getIndex(grew[repère][6])

                                        if talismane[repère][7] == 'mod':
                                            repère = getIndex(talismane[repère][6])


                                    if grew[repère][4] == "V":
                                        for i_g in range(len(grew)):
                                            if grew[i_g][7]=="suj" and grew[i_g][6] == grew[repère][0]:
                                                #sujet trouvé
                                                lvf_info[key_pepa][0][0] += "O"
                                                if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                                    lvf_info[key_pepa][0][1]["sujet"] = []
                                                lvf_info[key_pepa][0][1]["sujet"].append(grew[i_g])
                                                break

                                if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                     dones = []
                                     while repère != []:

                                         if grew[repère][4] == "V":
                                             # le sujet est le dernier actant
                                             for i_g in range(len(grew)):
                                                 if grew[i_g][7] == "suj" and grew[i_g][6] == grew[repère][0]:
                                                     # sujet trouvé
                                                     lvf_info[key_pepa][0][0] += "O"
                                                     if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                                         lvf_info[key_pepa][0][1]["sujet"] = []
                                                     lvf_info[key_pepa][0][1]["sujet"].append(grew[i_g])
                                                     repère = []
                                                     break
                                             if repère in dones:
                                                 repère = []
                                             else:
                                                 dones.append(repère)
                                         else:
                                             repère = getIndex(repère)

                                if not "sujet" in lvf_info[key_pepa][0][1].keys():
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                if "obj" in infos_verbe["liens"].keys():
                                    lvf_info[key_pepa][0][1]["objet"] = []
                                    to_del = []
                                    for item in infos_verbe["liens"]["obj"]:
                                        lvf_info[key_pepa][0][1]["objet"].append(grew[item[3]])
                                    del infos_verbe["liens"]["obj"]

                            if bck_lvf_info != "":
                                if "ponct" in infos_verbe["liens"].keys():
                                    del infos_verbe["liens"]["ponct"]
                                if "coord" in infos_verbe["liens"].keys():
                                    del infos_verbe["liens"]["coord"]

                                if "mod" in infos_verbe["liens"].keys():
                                    to_del = []
                                    for item in infos_verbe["liens"]["mod"]:
                                        if item[1] in ("p", "cs", "nc", "p+d", "clo"):
                                            if "circonstant" not in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["circonstant"] = []
                                            if item[1] != "nc":
                                                done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                if done:
                                                    tokens_dones[item[-1]] = True
                                                    introduction = item[0]
                                                    if (("à" in item[0] or "au" in item[0]) and len(item[0])<=4 and item[0] not in ("ad", "avec")) or item[1] == "clo":
                                                        introduction = "à"
                                                        lvf_info[key_pepa][0][0] += "a"
                                                    elif item[0] in ("devant"):
                                                        lvf_info[key_pepa][0][0] += "m"
                                                    elif ("d'" in item[0] or "de" in item[0] or "du" in item[0]) and len(item[0])<=4:
                                                        introduction = "de"
                                                        lvf_info[key_pepa][0][0] += "b"
                                                    elif item[0] in ("avec"):
                                                        lvf_info[key_pepa][0][0] += "c"
                                                    elif item[0] in ("contre"):
                                                        lvf_info[key_pepa][0][0] += "d"
                                                    elif item[0] in ("par"):
                                                        lvf_info[key_pepa][0][0] += "e"
                                                    elif item[0] in ("sur", "vers"):
                                                        lvf_info[key_pepa][0][0] += "g"
                                                    elif item[0] in ("dans"):
                                                        lvf_info[key_pepa][0][0] += "j"
                                                    elif item[0] in ("auprès"):
                                                        lvf_info[key_pepa][0][0] += "l"
                                                    elif item[2] in ("s=refl"):
                                                        lvf_info[key_pepa][0][0] += "p"
                                                    lvf_info[key_pepa][0][1]["circonstant"] .append((introduction, résultat))
                                                    to_del.append(item)
                                                    done = True
                                                else:
                                                    # TODO
                                                    done = False
                                                    take = False
                                                    if debug: log(getframeinfo(currentframe()).lineno)
                                                    break
                                            else:
                                                lvf_info[key_pepa][0][1]["circonstant"].append(grew[item[3]])
                                                to_del.append(item)
                                        elif item[1] in ("adv", "adj"):
                                            if "modifieur" not in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["modifieur"] = []
                                            lvf_info[key_pepa][0][1]["modifieur"].append(grew[item[3]])
                                            to_del.append(item)
                                        elif item[1] in ("adj"):
                                            to_del.append(item)
                                        elif item[1] in ("vpp"):
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                            break
                                        elif "aff" in infos_verbe["liens"].keys() and item[1]=="pro":
                                            if "sujet" in lvf_info[key_pepa][0][1].keys() and "objet" in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["référent_sujet"] = lvf_info[key_pepa][0][1]["sujet"]
                                                lvf_info[key_pepa][0][1]["sujet"] = []
                                                # le premier obj pro est le sujet, pas un truc détecté de façon secondaire
                                                mov = []
                                                for obj in lvf_info[key_pepa][0][1]["objet"]:
                                                    if obj[3] == "PRO":
                                                        mov.append(obj)
                                                if len(mov) == 0:
                                                    lvf_info[key_pepa][0][1]["sujet"] = lvf_info[key_pepa][0][1]["référent_sujet"]
                                                    del lvf_info[key_pepa][0][1]["référent_sujet"]
                                                elif len(mov) == 1:
                                                    lvf_info[key_pepa][0][1]["sujet"].append(mov[0])
                                                    lvf_info[key_pepa][0][1]["objet"].remove(mov[0])
                                                    lvf_info[key_pepa][0][1]["objet"].append(grew[item[3]])
                                                else:
                                                    print()
                                                    print(sentence)
                                                    print(lvf_info[key_pepa][0][1])                                                    
                                                    raise Exception
                                            elif not "objet" in lvf_info[key_pepa][0][1].keys() :
                                                lvf_info[key_pepa][0][1]["référent_sujet"] = lvf_info[key_pepa][0][1]["sujet"]
                                                lvf_info[key_pepa][0][1]["sujet"] = [grew[item[3]]]
                                            else:
                                                raise Exception
                                            to_del.append(item)
                                        elif item[1]=="pro":
                                            if "objet" in lvf_info[key_pepa][0][1].keys():
                                                if not "circonstant" in lvf_info[key_pepa][0][1].keys():
                                                    lvf_info[key_pepa][0][1]["circonstant"] = lvf_info[key_pepa][0][1]["objet"]
                                                    lvf_info[key_pepa][0][1]["objet"] = [grew[item[3]]]
                                                else:
                                                    lvf_info[key_pepa][0][1]["circonstant"].append(grew[item[3]])
                                            else:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                break
                                                raise Exception
                                            to_del.append(item)
                                        else:
                                            # TODO
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                            break
                                    for item in to_del:
                                        infos_verbe["liens"]["mod"].remove(item)
                                    if len(infos_verbe["liens"]["mod"]) == 0:
                                        del infos_verbe["liens"]["mod"]

                                if 'ats' in infos_verbe['liens'].keys():
                                    lvf_info[key_pepa][0][0] += "C"
                                    if "circonstant" not in lvf_info[key_pepa][0][1].keys():
                                        lvf_info[key_pepa][0][1]["circonstant"] = []
                                    for item in infos_verbe["liens"]['ats']:
                                        lvf_info[key_pepa][0][1]["circonstant"].append(grew[item[3]])
                                    del infos_verbe['liens']['ats']

                                if 'p_obj.o' in infos_verbe['liens'].keys():
                                    lvf_info[key_pepa][0][0] += "C"
                                    if "circonstant" not in lvf_info[key_pepa][0][1].keys():
                                        lvf_info[key_pepa][0][1]["circonstant"] = []
                                    for item in infos_verbe["liens"]['p_obj.o']:
                                        lvf_info[key_pepa][0][1]["circonstant"].append(grew[item[3]])
                                    del infos_verbe['liens']['p_obj.o']

                                if 'a_obj' in infos_verbe['liens'].keys():
                                    if 'compl-prep' not in lvf_info[key_pepa][0][1].keys():
                                        lvf_info[key_pepa][0][1]["compl-prep"] = []
                                    value = item[0]
                                    for item in infos_verbe["liens"]['a_obj']:
                                        if (("à" in item[0] or "au" in item[0]) and len(item[0])<=4 and item[0] not in ("ad", "avec")) or item[1] == "clo":
                                            lvf_info[key_pepa][0][0] += "a"
                                            value = "à"
                                        elif item[0] in ("devant"):
                                            lvf_info[key_pepa][0][0] += "m"
                                        elif ("d'" in item[0] or "de" in item[0] or "du" in item[0]) and len(item[0])<=4:
                                            lvf_info[key_pepa][0][0] += "b"
                                            value = "de"
                                        elif item[0] in ("avec"):
                                            lvf_info[key_pepa][0][0] += "c"
                                        elif item[0] in ("contre"):
                                            lvf_info[key_pepa][0][0] += "d"
                                        elif item[0] in ("par"):
                                            lvf_info[key_pepa][0][0] += "e"
                                        elif item[0] in ("sur", "vers"):
                                            lvf_info[key_pepa][0][0] += "g"
                                        elif item[0] in ("dans"):
                                            lvf_info[key_pepa][0][0] += "j"
                                        elif item[0] in ("auprès"):
                                            lvf_info[key_pepa][0][0] += "l"
                                        elif item[2] in ("s=refl"):
                                            lvf_info[key_pepa][0][0] += "p"
                                        elif not "circonstant" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["circonstant"]=[(value, grew[item[3]])]
                                            value =""
                                        else:
                                            value = ""
                                        if value != "":
                                            lvf_info[key_pepa][0][1]["compl-prep"].append((value, grew[item[3]]))
                                    del infos_verbe['liens']['a_obj']

                                if 'p_obj.agt' in infos_verbe['liens'].keys():
                                    to_del = []
                                    for item in infos_verbe["liens"]['p_obj.agt']:
                                        add = True
                                        value = item[0]
                                        if (("à" in item[0] or "au" in item[0]) and len(item[0])<=4 and item[0] not in ("ad", "avec")) or item[1] == "clo":
                                            lvf_info[key_pepa][0][0] += "a"
                                            value = "à"
                                        elif item[0] in ("devant"):
                                            lvf_info[key_pepa][0][0] += "m"
                                        elif ("d'" in item[0] or "de" in item[0] or "du" in item[0]) and len(item[0])<=4:
                                            lvf_info[key_pepa][0][0] += "b"
                                            value = "de"
                                        elif item[0] == ("avec"):
                                            lvf_info[key_pepa][0][0] += "c"
                                        elif item[0] == ("contre"):
                                            lvf_info[key_pepa][0][0] += "d"
                                        elif item[0] == ("par"):

                                            if "sujet" not in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["sujet"] = []

                                            objet = getObjet(item, grew, talismane, stanford)
                                            tokens_dones[item[-1]] = True
                                            lvf_info[key_pepa][0][1]["sujet"].append(objet[2])

                                            to_del.append(item)
                                            add = False
                                            #lvf_info[key_pepa][0][0] += "e"
                                        elif item[0] in ("sur", "vers"):
                                            lvf_info[key_pepa][0][0] += "g"
                                        elif item[0] == ("dans"):
                                            lvf_info[key_pepa][0][0] += "j"
                                        elif item[0] == ("auprès"):
                                            lvf_info[key_pepa][0][0] += "l"
                                        elif not "circonstant" in lvf_info[key_pepa][0][1].keys():
                                            lvf_info[key_pepa][0][1]["circonstant"]=[(value, grew[item[3]])]
                                            value =""
                                            add = False
                                        else:
                                            value = ""
                                            add = False
                                        if add:
                                            if "compl-prep" not in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["compl-prep"] = []
                                            to_del.append(item)
                                            lvf_info[key_pepa][0][1]["compl-prep"].append((value, grew[item[3]]))

                                    for item in to_del:
                                        infos_verbe["liens"]['p_obj.agt'].remove(item)

                                    if len(infos_verbe["liens"]['p_obj.agt']) == 0:
                                        del infos_verbe["liens"]['p_obj.agt']
                                    else:
                                        if "sujet" in lvf_info[key_pepa][0][1].keys():
                                            del  lvf_info[key_pepa][0][1]["sujet"]
                                        to_del = []
                                        for item in infos_verbe["liens"]['p_obj.agt']:
                                            index = grew[item[3]][0]
                                            if "sujet" not in lvf_info[key_pepa][0][1].keys():
                                                lvf_info[key_pepa][0][1]["sujet"] = []
                                            objet = getObjet(item, grew, talismane, stanford)
                                            tokens_dones[item[-1]] = True
                                            lvf_info[key_pepa][0][1]["sujet"].append(objet[2])
                                            to_del.append(item)

                                        verbes_testés = []

                                        for tmp_verbe in infos_verbes.values() :
                                            if tmp_verbe["socle"] != infos_verbe["socle"]:
                                                verbes_testés.append(tmp_verbe["socle"][4])
                                            else:
                                                break

                                        index_à_utiliser = []
                                        for index_, tmp_socle in infos_verbes.items():
                                            tmp_socle = tmp_socle["socle"]
                                            if tmp_socle[4] in verbes_testés:
                                                index_à_utiliser.append(index_)
                                        if len(index_à_utiliser) == 1:
                                            index_à_utiliser = index_à_utiliser[0]
                                        elif len(index_à_utiliser) > 1:
                                            index_à_utiliser = index_à_utiliser[-1]
                                        else:
                                            index_à_utiliser = -1
                                        if index_à_utiliser > -1:
                                            objet = getSuj(index_à_utiliser)
                                            if objet != 'vpr_or_vpp' and objet:
                                                lvf_info[key_pepa][0][1]["objet"] = [grew[objet[0][3]]]
                                            else:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                break
                                        for item in to_del:
                                            infos_verbe["liens"]['p_obj.agt'].remove(item)

                                        if len(infos_verbe["liens"]['p_obj.agt']) == 0:
                                            del infos_verbe["liens"]['p_obj.agt']

                                if "de_obj" in infos_verbe['liens'].keys():
                                    if "compl-prep" not in lvf_info[key_pepa][0][1].keys():
                                        lvf_info[key_pepa][0][1]["compl-prep"] = []
                                    for item in infos_verbe['liens']["de_obj"]:
                                        lvf_info[key_pepa][0][1]["compl-prep"].append(("de", grew[item[3]]))
                                    del infos_verbe['liens']["de_obj"]
                                    lvf_info[key_pepa][0][0] += "b"

                                if 'aff' in infos_verbe['liens'].keys():
                                    key = list(lvf_info.keys())[0]
                                    lvf_info["pro" + key ] = lvf_info[key]
                                    del lvf_info[key]
                                    del infos_verbe['liens']["aff"]
                                    pronominal = True

                                if len(infos_verbe["liens"]) == 0:
                                    take = True
                                else:
                                    if "det" in infos_verbe["liens"].keys():
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    else:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break

                                if not take:
                                    break
                                else:
                                    résultats = {}
                                    for key in lvf_info.keys():
                                        résultats[lvf_info[key][0][0]] = lvf_info[key][0][1]
                                    if len(résultats.keys()) > 1:
                                        raise Exception
                                    classe_résultat = ""
                                    for classe in résultats.keys():
                                        allowed = list(résultats[classe].keys())
                                        classe_résultat = classe
                                    for type in lvf_info_base.keys():
                                        for solution_class in copy.deepcopy(lvf_info_base[type]):
                                            classe = solution_class[0]
                                            dico_classe = solution_class[1]
                                            existing = list(dico_classe.keys())
                                            sujet_ok = ("sujet" in existing and "sujet" in allowed) or \
                                                       (not "sujet" in existing and not "sujet" in allowed)
                                            objet_ok = ("objet" in existing and "objet" in allowed) or \
                                                       (not "objet" in existing and not "objet" in allowed)
                                            if sujet_ok and objet_ok:
                                                if "compl-prep" in existing and not "compl-prep" in allowed and "circonstant" in allowed:
                                                    if len(résultats[classe_résultat]["circonstant"][0]) == 2:
                                                        if résultats[classe_résultat]["circonstant"][0][0] == dico_classe["compl-prep"]:
                                                            résultats[classe_résultat]["compl-prep"] = résultats[classe_résultat]["circonstant"]
                                                            del résultats[classe_résultat]["circonstant"]
                                                            allowed = list(résultats[classe_résultat].keys())
                                                            break

                                    finals.append(résultats)
                            else:
                                table_aux = getAux(i_verbe, [])
                                addTypeEvent(sentence, grew[i_verbe][0], ("conj", table_aux))
                                verbes_sentence[grew[i_verbe][2]]["autres"] += 1
                                # on va nettoyer les infos verbes pour supprimer ce qui n'est pas utile à cette étape
                                for value in ["ponct", "coord"]:
                                    if value in infos_verbe["liens"].keys():
                                        del infos_verbe["liens"][value]

                                if "mod" in infos_verbe["liens"].keys():
                                    to_del = [item for item in  infos_verbe["liens"]["mod"] if item[1] == "adv"]
                                    for item in to_del:
                                        infos_verbe["liens"]["mod"].remove(item)
                                    if len(infos_verbe["liens"]["mod"]) == 0:
                                        del infos_verbe["liens"]["mod"]

                                if "obj" in infos_verbe['liens'].keys() and 'intransitif' in lvf_info.keys():
                                    del lvf_info['intransitif']

                                pronominal = False
                                pronominal_possible = False

                                for key in infos_verbe['liens'].keys():
                                    for item in infos_verbe['liens'][key]:
                                        if item[2] == "s=refl":
                                            if key == "a_obj":
                                                pronominal_possible = True
                                            else:
                                                pronominal = True
                                        if item[2] == "suj" and item[1] == "adj":
                                            item[1] = "nc"
                                            ref = item[3]
                                            grew[ref][3] = "NC"
                                            grew[ref][4] = "NC"

                                if "aux.tps" in infos_verbe['liens'].keys():
                                    del infos_verbe['liens']['aux.tps']

                                if "aux.pass" in infos_verbe['liens'].keys() and not 'p_obj.agt' in  infos_verbe['liens'].keys():
                                    pronominal_possible = True

                                if not (pronominal or pronominal_possible) and "pronominal" in lvf_info.keys():
                                    del lvf_info["pronominal"]

                                if pronominal:
                                    types = [t for t in lvf_info.keys() if t != "pronominal"]
                                    for t in types:
                                        del lvf_info[t]

                                if len(lvf_info) == 0:
                                    lvf_info = copy.deepcopy(lvf_info_base) # bon, il y a couac


                                complétive_possible = False
                                complétive = False
                                if "mod" in infos_verbe['liens'].keys():
                                    for item in infos_verbe['liens']['mod']:
                                        if item[1] == "cs":
                                            if item[2] == "s=s":
                                                complétive_possible = True
                                            else:
                                                print("b")
                                                pass

                                if 'obj' in infos_verbe['liens'].keys():
                                    for item in infos_verbe['liens']['obj']:
                                        if item[2] == "s=s":
                                            complétive = True

                                compl_prep = False
                                compl_preps = []
                                compl_preps_orig = []

                                if 'de_obj' in infos_verbe['liens'].keys():
                                    compl_prep = True
                                    compl_preps.append("de")
                                    compl_preps_orig.append("")

                                circonstant = False
                                if 'ats' in infos_verbe['liens'].keys():
                                    circonstant = True

                                circonstant_possible = True# TODO
                                if 'p_obj.o' in infos_verbe['liens'].keys():
                                    circonstant_possible = True

                                if 'a_obj' in infos_verbe['liens'].keys():
                                    for item in infos_verbe['liens']['a_obj']:
                                        if item[1].lower() in ("p", "p+d", "clo"):
                                            compl_prep = True
                                            compl_preps.append(item[0] if item[1]!="clo" else "à")
                                            compl_preps_orig.append(item)

                                transitif = False

                                if 'p_obj.agt' in  infos_verbe['liens'].keys() and "aux.pass" in  infos_verbe['liens'].keys():
                                    # le verbe est transitif : le sujet est l'objet sémantique.
                                    transitif = True

                                if 'obj' in infos_verbe['liens'].keys():
                                    for obj in infos_verbe['liens']['obj']:
                                        if obj[2] == 's=refl':
                                            pronominal = True
                                        else:
                                            transitif = True
                                if not transitif and infos_verbe['socle'][2] == 'obj.cpl' and not "ats" in infos_verbe['liens'].keys():
                                    transitif = True

                                if not compl_prep and not circonstant_possible:
                                    #on ne peut pas traiter a priori le cas inverse, des fois que
                                    # des compléments locatifs soient mal rattachés.
                                    if 'transitif indirect' in lvf_info.keys():
                                        del lvf_info['transitif indirect']
                                    for t in lvf_info.keys():
                                        to_del = []
                                        for item in lvf_info[t]:
                                            if "compl-prep" in item[1].keys():
                                                to_del.append(item)
                                        for item in to_del:
                                            lvf_info[t].remove(item)

                                for t in lvf_info.keys():
                                    to_del = []
                                    for item in lvf_info[t]:
                                        if not "compl-prep" in item[1].keys() and compl_prep:
                                            to_del.append(item)
                                    for item in to_del:
                                        lvf_info[t].remove(item)

                                if not transitif:
                                    if 'transitif direct' in lvf_info.keys():
                                        del lvf_info['transitif direct']
                                    for t in lvf_info.keys():
                                        to_del = []
                                        for item in lvf_info[t]:
                                            if "objet" in item[1].keys() and not "circonstant" in item[1].keys():
                                                to_del.append(item)
                                        for item in to_del:
                                            lvf_info[t].remove(item)
                                else:
                                    for t in lvf_info.keys():
                                        to_del = []
                                        for item in lvf_info[t]:
                                            if "objet" not in item[1].keys():
                                                to_del.append(item)
                                        for item in to_del:
                                            lvf_info[t].remove(item)

                                take = True

                                for type_possible in lvf_info.keys():
                                    to_del = []
                                    support = copy.deepcopy(lvf_info[type_possible])
                                    for item in support:
                                        code = item[0]
                                        info_détaillée = item[1]
                                        if 'compl-prep' in info_détaillée.keys():
                                            take = False
                                            for prepr in info_détaillée['compl-prep'].split(", "):
                                                prepr = prepr.lower()
                                                if prepr in compl_preps or (prepr == "à" and ("au" in compl_preps or "aux" in compl_preps) or prepr == 'divers mouvements'):
                                                    take = True
                                                    if prepr != 'divers mouvements' or len(compl_preps) > 0:
                                                        if prepr == "à" and compl_preps.count("aux") == 1:
                                                            ref_prep = "aux"
                                                        elif prepr == "à" and compl_preps.count("au") == 1:
                                                            ref_prep = "au"
                                                        else:
                                                            ref_prep = prepr if len(compl_preps) > 1 else compl_preps[0]
                                                        if compl_preps_orig[compl_preps.index(ref_prep)] != "":
                                                            tokens_dones[compl_preps_orig[compl_preps.index(ref_prep)][-1]] = True
                                            if not take  and item not in to_del:
                                                to_del.append(item)
                                        if circonstant and not 'circonstant' in info_détaillée.keys() and item not in to_del:
                                            to_del.append(item)

                                    for item in to_del:

                                        lvf_info[type_possible].remove(item)

                                types = [t for t in lvf_info.keys()]
                                for t in types:
                                    if len(lvf_info[t])==0:
                                        del lvf_info[t]

                                if len(lvf_info.keys()) > 1 :
                                    if 'intransitif' in lvf_info.keys() and transitif:
                                        del lvf_info["intransitif"]
                                if len(lvf_info.keys()) > 1 :
                                    if 'pronominal' in lvf_info.keys():
                                        if pronominal_possible or pronominal:
                                            types = [t for t in lvf_info.keys() if t != "pronominal"]
                                        else:
                                            types = ["pronominal"]
                                        for t in types:
                                            del lvf_info[t]

                                dispos = calcDispo(lvf_info)
                                if len(dispos) > 1 and (complétive_possible or complétive):
                                    if "complétive" in "".join(dispos):
                                        for t in lvf_info.keys():
                                            to_del = []
                                            for item in lvf_info[t]:
                                                if "complétive" not in "".join([k for k in item[1].values()]):
                                                    to_del.append(item)
                                            for item in to_del:
                                                lvf_info[t].remove(item)



                                if len(dispos) > 1 and not (complétive_possible or complétive):
                                    has_completive = False
                                    has_no_completive = False
                                    for t in lvf_info.keys():
                                        for item in lvf_info[t]:
                                            for key_item in item[1].keys():
                                                if "complétive" in item[1][key_item]:
                                                    has_completive = True
                                                else:
                                                    has_no_completive = True
                                                if has_no_completive and has_completive:
                                                    break

                                    if "complétive" in "".join(dispos) and has_completive and has_no_completive:
                                        for t in lvf_info.keys():
                                            to_del = []
                                            for item in lvf_info[t]:
                                                if "complétive" in "".join([k for k in item[1].values()]):
                                                    to_del.append(item)
                                            for item in to_del:
                                                lvf_info[t].remove(item)
                                nb_items = sum([len(infos_verbe["liens"][k]) for k in infos_verbe["liens"].keys() if
                                                k not in ("aux.pass", "mod", "aux.tps")])
                                for t in lvf_info.keys():
                                    to_del = []
                                    for item in lvf_info[t]:
                                        if len([k for k in item[1] if k != "circonstant"]) > nb_items:
                                            to_del.append(item)

                                    for item in to_del:
                                        lvf_info[t].remove(item)

                                dispos = calcDispo(lvf_info)

                                take = True
                                if len(lvf_info) == 0:
                                    lvf_info = copy.deepcopy(lvf_info_base)
                                    lvf_info = filter(lvf_info)
                                    dispos = calcDispo(lvf_info)
                                if len(lvf_info) == 0:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break
                                if  len(lvf_info) > 0 and len(dispos) > 1:
                                    change = False

                                    # on va prendre les situations les plus standards
                                    has_no_circonstant = False
                                    has_circonstant = False

                                    has_no_compl_prep = False
                                    has_compl_prep = False

                                    simpl_info = [k for k in infos_verbe["liens"].keys() if k not in ("aux.pass", "aux.tps", "aff")]

                                    eqs = (['suj', 'obj'], ['obj'], ['suj'], ['suj', 'p_obj.agt'])
                                    for eq in eqs:
                                        if set(eq) == set(simpl_info):

                                            for t in lvf_info.keys():
                                                for item in lvf_info[t]:
                                                    if "circonstant" in item[1].keys():
                                                        has_circonstant = True
                                                    else:
                                                        has_no_circonstant = True

                                            if has_circonstant and has_no_circonstant:

                                                for t in lvf_info.keys():
                                                    to_del = []
                                                    for item in lvf_info[t]:
                                                        if "circonstant" in item[1].keys():
                                                            to_del.append(item)
                                                    for item in to_del:
                                                        lvf_info[t].remove(item)
                                                        change = True

                                            has_no_compl_prep = False
                                            has_compl_prep = False

                                            for t in lvf_info.keys():
                                                for item in lvf_info[t]:
                                                    if "compl-prep" in item[1].keys():
                                                        has_compl_prep = True
                                                    else:
                                                        has_no_compl_prep = True

                                            if has_no_compl_prep and has_compl_prep:

                                                for t in lvf_info.keys():
                                                    to_del = []
                                                    for item in lvf_info[t]:
                                                        if "compl-prep" in item[1].keys():
                                                            to_del.append(item)
                                                    for item in to_del:
                                                        lvf_info[t].remove(item)
                                                        change = True

                                    dispos = calcDispo(lvf_info)
                                    if len(dispos) > 1:
                                        eqs = (['suj', 'ats'], ['suj', 'mod'], ['suj'])

                                        has_obj = False
                                        has_no_obj = False

                                        for t in lvf_info.keys():
                                            for item in lvf_info[t]:
                                                if "objet" in item[1].keys():
                                                    has_obj = True
                                                else:
                                                    has_no_obj = True
                                                if has_no_obj and has_obj:
                                                    break
                                        if has_obj and has_no_obj:
                                            for eq in eqs:
                                                if set(eq) == set(simpl_info):

                                                    if has_no_obj and has_obj:

                                                        for t in lvf_info.keys():
                                                            to_del = []
                                                            for item in lvf_info[t]:
                                                                if "objet" in item[1].keys():
                                                                    to_del.append(item)
                                                            for item in to_del:
                                                                lvf_info[t].remove(item)
                                                                change = True
                                        if change :
                                            dispos = calcDispo(lvf_info)
                                        if len(dispos) > 1:
                                            old_lvf = copy.deepcopy(lvf_info)
                                            lvf_info_tmp = filter(lvf_info)
                                            if len(lvf_info_tmp.keys()) == 0:
                                                take = False

                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                break
                                            if len(dispos) > len(calcDispo(lvf_info_tmp)):
                                                lvf_info = lvf_info_tmp


                                    # lvf_info = copy.deepcopy(lvf_info_base)

                                # à ce stade, on doit pouvoir départager : on affecte
                                verbe = infos_verbe["socle"]

                                passif = "aux.pass" in infos_verbe.keys()

                                for key in list(infos_verbe["liens"].keys())[:]:
                                    if key == 'aux.tps':
                                        del infos_verbe["liens"][key]

                                if 'pronominal' in lvf_info.keys():
                                    for key in list(infos_verbe["liens"].keys())[:]:
                                        for obj in infos_verbe['liens'][key][:]:
                                            if obj[2] == 's=refl':
                                                infos_verbe['liens'][key].remove(obj)
                                                if len(infos_verbe['liens'][key]) == 0:
                                                    del infos_verbe['liens'][key]
                                
                                objetDeleted = False
                                for titre in lvf_info.keys():
                                    résultats = {}
                                    infos_verbe = copy.deepcopy(infos_verbes[index])
                                    if not take :

                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    take = True
                                    old_modèle = ""
                                    for modèle in copy.deepcopy(lvf_info[titre]):
                                        type = modèle[0]
                                        if old_modèle != "" and take == False:
                                            lvf_info[titre].remove(modèle)
                                            résultats = {type:{}}
                                        else:
                                            if type not in résultats.keys():
                                                résultats[type] = {}
                                            old_modèle = modèle
                                        code = modèle[0]
                                        items = copy.deepcopy(modèle[1])
                                        if "sujet" in items.keys():
                                            done = False
                                            type_local = items["sujet"]
                                            if passif:
                                                items["sujet"] = (type_local, )
                                                raise Exception
                                            else:
                                                if "suj" in infos_verbe["liens"].keys():
                                                    if len(infos_verbe["liens"]["suj"]) > 1:
                                                        for info_tmp in infos_verbe["liens"]["suj"][:]:
                                                            if info_tmp[1] == "p+d":
                                                                infos_verbe["liens"]["suj"].remove(info_tmp)

                                                    assert len(infos_verbe["liens"]["suj"]) == 1
                                                    for item in infos_verbe["liens"]["suj"][:]:
                                                        if grew[item[3]][3] in ("N", 'CL', 'PRO'):
                                                            if grew[item[3]][4] == "PROREL":
                                                                done, find, sujet = getObjet(item, grew,
                                                                     talismane, stanford)
                                                                if not find:
                                                                    take = False
                                                                    if debug: log(getframeinfo(currentframe()).lineno)
                                                                    break
                                                            else:
                                                                sujet = grew[item[3]]
                                                            résultats[type]["sujet"] = (type_local, sujet)
                                                            infos_verbe["liens"]["suj"].remove(item)
                                                            done = True
                                                        elif grew[item[3]][4] == "PROREL" and grew[-1][1] == ".":
                                                            if grew[item[3]-1][3] == "N":
                                                                résultats[type]["sujet"] = (type_local, grew[item[3]-1])
                                                                infos_verbe["liens"]["suj"].remove(item)
                                                                done = True
                                                            elif grew[item[3]-1][3] == ("PONCT") and grew[item[3]-2][3] == ("N"):
                                                                résultats[type]["sujet"] = (type_local, grew[item[3]-2])
                                                                infos_verbe["liens"]["suj"].remove(item)
                                                                done = True
                                                            else:
                                                                pass
                                                                # TODO
                                                        else:
                                                            pass
                                                            # TODO
                                            if done:
                                                del items["sujet"]
                                        if "objet" in items.keys():
                                            done = False
                                            type_local = items["objet"]
                                            if passif:
                                                items["objet"] = (type_local, )
                                                raise Exception
                                            else:
                                                if "obj" in infos_verbe["liens"].keys():
                                                    assert len(infos_verbe["liens"]["obj"]) == 1
                                                    for item in infos_verbe["liens"]["obj"][:]:
                                                        if item[3] == "p":
                                                            done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                            if done:
                                                                tokens_dones[item[-1]] = True
                                                                résultats[type]["objet"] = (type_local, résultat)
                                                                infos_verbe["liens"]["obj"].remove(item)
                                                                done = True
                                                            else:
                                                                # TODO
                                                                done = False
                                                        elif item[1] == "cs" and item[2] == "s=s":
                                                            for k in [l + item[3] + 1 for l in range(len(grew) - item[3] - 1)]:
                                                                 if grew[k][7] == "obj.cpl" and grew[k][3] == "V":
                                                                    résultats[type]["objet"] = (type_local, grew[k])
                                                                    infos_verbe["liens"]["obj"].remove(item)
                                                                    done = True
                                                                    break
                                                        else:
                                                            résultats[type]["objet"] = (type_local, grew[item[3]])
                                                            infos_verbe["liens"]["obj"].remove(item)
                                                            done = True

                                            if done:
                                                del items["objet"]
                                            else:
                                                if 'p_obj.o'  in infos_verbe["liens"].keys():
                                                    assert len(infos_verbe["liens"]["p_obj.o"]) == 1
                                                    for item in infos_verbe["liens"]["p_obj.o"][:]:
                                                        if item[1] == 'vinf' and 'inf' in items["objet"]:
                                                            résultats[type]["objet"] = (type_local, grew[item[3]])
                                                            infos_verbe["liens"]["p_obj.o"].remove(item)
                                                            done = True
                                                if done:
                                                    del items["objet"]
                                        if "compl-prep" in items.keys():
                                            type_local = items["compl-prep"]
                                            if passif:
                                                items["compl-prep"] = (type_local, )
                                                raise Exception
                                            else:
                                                done = False
                                                if "de_obj" in infos_verbe["liens"].keys() and type_local == 'de':
                                                    assert len(infos_verbe["liens"]["de_obj"]) == 1

                                                    for item in infos_verbe["liens"]["de_obj"][:]:
                                                        if item[1].lower().endswith('wh'):
                                                            done = True
                                                            résultat = grew[item[-1]]
                                                        else:
                                                            done, take, résultat = getObjet(item, grew, talismane, sentence)
                                                            if not take:

                                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                                break
                                                        if done:
                                                            tokens_dones[getTGIndex(résultat[0])] = True
                                                            résultats[type]["compl-prep"] = (type_local, résultat)
                                                    if done:
                                                        del items["compl-prep"]

                                                if "a_obj" in infos_verbe["liens"].keys() and type_local == 'à':
                                                    assert len(infos_verbe["liens"]["a_obj"]) == 1
                                                    for item in infos_verbe["liens"]["a_obj"][:]:
                                                        if item[1].lower().endswith('wh'):
                                                            done = True
                                                            résultat = grew[item[-1]]
                                                        else:
                                                            if grew[item[3] + 1 ][3] == "D" and grew[item[3] + 2 ][3] == "A"  and \
                                                                    grew[item[3] + 3][3] == "N":
                                                                résultat = grew[item[3] + 3]
                                                                done = True
                                                            elif grew[item[3] + 1][3] in ("N", "V", "CL"):
                                                                résultat = grew[item[3] + 1]
                                                                done = True
                                                            elif grew[item[3] + 1][3] == "PRO" and grew[item[3] + 2][4] == "CS":
                                                                ref =  grew[item[3] + 2][0]
                                                                start = len(grew[item[3] + 2:])
                                                                for i in range(start):
                                                                    i_ref =item[3] + 2 + i
                                                                    if grew[i_ref][6] == ref and grew[i_ref][3] == "V":
                                                                        résultat = grew[i_ref]
                                                                        done = True
                                                                        break
                                                        if done:
                                                            infos_verbe["liens"]["a_obj"].remove(item)
                                                        else:
                                                            done, take, résultat = getObjet(item, grew, talismane, sentence)
                                                            tokens_dones[item[-1]] = True
                                                            if not take:

                                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                                break
                                                        if done:
                                                            if (len(résultat) == 10 and résultat[4] == "CS"):
                                                                done, take, résultat = getObjet(item, grew, talismane, sentence)
                                                                tokens_dones[item[-1]] = True
                                                                if not take:

                                                                    if debug: log(getframeinfo(currentframe()).lineno)
                                                                    break
                                                            résultats[type]["compl-prep"] = (type_local, résultat)
                                                            break
                                                    if done:
                                                        del items["compl-prep"]
                                                    else:
                                                        # TODO
                                                        done = False
                                                        pass

                                                if not done:
                                                    if sentence == "Un danger grave menace César, qui l' en avertit ?":
                                                        résultats[type]["compl-prep"] = (type_local, grew[1])
                                                        del items["compl-prep"]
                                                        break
                                                    elif sentence in ("La correction qu' il m' en fit fut grande effectivement.", "Le pere ne l' en justifia pas davantage dans son esprit, pour avoir bonne réputation auprès d' elle."):
                                                        done = False
                                                        take = False

                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        # le pointeur du "en" n'est pas dans la phrase
                                                        break
                                                    else:
                                                        # TODO
                                                        done = False
                                                        take = False

                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        pass

                                                # l'objet peut avoir été introduit par une préposition qui le définit
                                                # comme de_obj ou a_obj, et être lui-même rattaché comme objt du verbe.
                                                # En ce cas il faut choisir quelle info est bonne.
                                                # On ne va pas faire comme si la préposition n'existait pas. C'est elle qui gagne.
                                                if "objet" in résultats[type].keys() and "compl-prep" in résultats[type].keys() and résultats[type]["objet"][1] == résultats[type]["compl-prep"][1]:
                                                    del résultats[type]["objet"]
                                                    objetDeleted = True

                                        if len(items) > 0 and take:
                                            for key in list(infos_verbe["liens"].keys()):
                                                if len(infos_verbe["liens"][key]) == 0:
                                                    del infos_verbe["liens"][key]

                                            if take and len(infos_verbe["liens"].keys()) == 0:
                                                for key_tmp in ("objet", "sujet", "compl-prep"):
                                                    if key_tmp in items.keys() :
                                                        take = False
                                                        break
                                                if take == False:
                                                    break
                                            if take and list(items.keys()) == ['circonstant'] :
                                                if len(infos_verbe["liens"].keys()) == 0:
                                                    del items['circonstant']
                                                elif list(infos_verbe["liens"].keys()) == ['mod']:
                                                    if infos_verbe["liens"]['mod'][0][2] == 'm=ind':
                                                        del infos_verbe["liens"]['mod']
                                                        del items["circonstant"]
                                                    elif infos_verbe["liens"]['mod'][0][0] == 'où':
                                                        item = infos_verbe["liens"]['mod'][0]
                                                        done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                        if done:
                                                            tokens_dones[item[-1]] = True
                                                            type_local = items["circonstant"]
                                                            résultats[type]["circonstant"] = (type_local, résultat)
                                                            del infos_verbe["liens"]['mod']
                                                            del items["circonstant"]
                                                        else:
                                                            # TODO
                                                            done = False
                                                            pass
                                                    elif items["circonstant"] in ('locatif (où on est)', 'locatif de destination (où on va)'):
                                                        if infos_verbe["liens"]['mod'][0][1] == "vpp":
                                                            take = False

                                                        elif infos_verbe["liens"]['mod'][0][1] == "p":
                                                            item = infos_verbe["liens"]['mod'][0]
                                                            done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                            if done:
                                                                tokens_dones[item[-1]] = True
                                                                type_local = items["circonstant"]
                                                                résultats[type]["circonstant"] = (type_local, résultat)
                                                                del infos_verbe["liens"]['mod']
                                                                del items["circonstant"]
                                                            else:
                                                                # TODO
                                                                done = False
                                                                pass
                                                        else:

                                                            # TODO
                                                            done = False
                                                            pass
                                                    elif items["circonstant"] in ('cause'):
                                                        if infos_verbe["liens"]['mod'][0][0] == "devant":
                                                            take = False
                                                            break
                                                        else:

                                                            # TODO
                                                            done = False
                                                            pass
                                                    elif items["circonstant"] in ('instrumental, moyen'):
                                                        if infos_verbe["liens"]['mod'][0][1] == "nc":
                                                            del infos_verbe["liens"]['mod']
                                                            del items["circonstant"]
                                                            done = True
                                                        else:

                                                            # TODO
                                                            done = False
                                                            pass
                                                    elif items["circonstant"] in ('modalité') and infos_verbe["liens"]['mod'][0][0] in ("à", "aux") :
                                                        item = infos_verbe["liens"]['mod'][0]
                                                        tokens_dones[item[-1]] = True
                                                        done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                        if done:
                                                            tokens_dones[item[-1]] = True
                                                            type_local = items["circonstant"]
                                                            résultats[type]["circonstant"] = (type_local, résultat)
                                                            del infos_verbe["liens"]['mod']
                                                            del items["circonstant"]
                                                        else:
                                                            # TODO
                                                            done = False
                                                            pass
                                                    else:
                                                        # TODO
                                                        done = False
                                                        pass
                                                elif items["circonstant"] in ('locatif (où on est)', 'temps'):
                                                    if 'p.obj.o' in infos_verbe["liens"].keys():
                                                        item = infos_verbe["liens"]['p.obj.o'][0]
                                                        done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                        if done:
                                                            tokens_dones[item[-1]] = True
                                                            type_local = items["circonstant"]
                                                            résultats[type]["circonstant"] = (type_local, résultat)
                                                            del infos_verbe["liens"]['p.obj.o']
                                                            del items["circonstant"]
                                                        else:
                                                            # TODO
                                                            done = False
                                                            pass
                                                else:
                                                    # TODO
                                                    done = False
                                                    pass
                                            elif "sujet" in items.keys() and take:
                                                if len(infos_verbe["socle"][5])> 0 and infos_verbe["socle"][5][0][3] == "V":

                                                    verbes_testés = []

                                                    for tmp_verbe in infos_verbes.values():
                                                        if tmp_verbe["socle"] != infos_verbe["socle"] and not  isVPP(tmp_verbe):
                                                            verbes_testés.append(tmp_verbe["socle"][4])
                                                        else:
                                                            break

                                                    index_à_utiliser = []
                                                    for index_, tmp_socle in infos_verbes.items():
                                                        tmp_socle = tmp_socle["socle"]
                                                        if tmp_socle[4] in verbes_testés:
                                                            index_à_utiliser.append(index_)
                                                    if len(index_à_utiliser) == 1:
                                                        index_à_utiliser = index_à_utiliser[0]
                                                    elif len(index_à_utiliser) > 1:
                                                        index_à_utiliser = index_à_utiliser[-1]
                                                    else:
                                                        index_à_utiliser = -1

                                                    if index_à_utiliser > -1:
                                                        suj = getSuj(index_à_utiliser)
                                                        if suj:
                                                            résultats[type]["sujet"] = (type_local, grew[suj[0][3]])
                                                            del items["sujet"]
                                                        else:
                                                            take = False
                                                    else:
                                                        take = False
                                                elif sentence == 'Jean-Baptiste lui-même pénétrait mal ses sentiments.':
                                                    résultats[type]["sujet"] = (type_local, grew[0])
                                                    del items["sujet"]
                                                    done = True
                                                elif "suj" in infos_verbe["liens"].keys():
                                                    résultats[type]["sujet"] = (type_local, grew[infos_verbe["liens"]["suj"][0][3]])
                                                    del items["sujet"]
                                                else:
                                                    # TODO
                                                    take = False

                                    if old_modèle != "" and take == False:
                                        lvf_info[titre].remove(modèle)
                                        del résultats[type]
                                        take = True
                                    else:
                                        old_modèle = ''

                                if len(résultats.keys()) == 1:
                                    lvky =  list(lvf_info.keys())[0]
                                    lvky =  lvf_info[lvky][0][1]
                                    if "ats" in infos_verbe["liens"].keys() and "circonstant" in lvky.keys():
                                        circonstant = lvky['circonstant']
                                        for res in résultats.keys():
                                            résultats[res]['circonstant']=(circonstant, grew[infos_verbe["liens"]['ats'][0][-1]])
                                            del lvky['circonstant']
                                    take = True
                                elif len(résultats.keys()) == 0:
                                    take = False
                                    break
                                elif len(résultats.keys()) > 1:
                                    raise Exception
                                if résultats == {}:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                else:
                                    take = True
                                

                                finals.append(résultats)
                                if len(résultats.keys()) != 1:
                                    raise Exception
                                
                                keyOfResult = list(résultats.keys())[0]
                                if objetDeleted and "objet" not in résultats[keyOfResult].keys() and keyOfResult.startswith("T"):
                                    resultat = résultats[keyOfResult]
                                    del résultats[keyOfResult]
                                    résultats[keyOfResult.replace("T", "N")] = resultat

                            if finals[-1] == {}:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)

                            if take:
                                final = finals[-1]
                                for arbre in list(final.keys()):
                                    if arbre.startswith('PA') or arbre.startswith('PE'):

                                        minuscules = "".join("".join(re.findall(r'[a-z]', arbre)))
                                        minuscules = "".join(sorted(minuscules))
                                        ref = "".join(sorted(résultats[arbre].keys())).replace('modifieur', "") + minuscules

                                        if ref not in table_des_arbres.keys() and "circonstant" in ref:
                                            ref = ref.replace('circonstant', "compl-prep")

                                        if ref not in table_des_arbres.keys():

                                            if "sujet" not in final[arbre] and "objet" in final[arbre]:

                                                final[arbre]["sujet"] = final[arbre]["objet"]
                                                del final[arbre]["objet"]

                                            ref = "".join(sorted(résultats[arbre].keys())).replace('modifieur', "") #on est sur du circonstant, du modifieur, on vire la lettre
                                        if ref not in table_des_arbres.keys() and "compl-prep" in ref:

                                            ref += minuscules

                                        if "circonstant" in ref:
                                            has_circonstant = True
                                        else:
                                            has_circonstant = False

                                        if ref not in table_des_arbres.keys():
                                            if "circonstant"+ref in table_des_arbres.keys():
                                                ref= "circonstant"+ref

                                        if take :

                                            take, lvf_info_tmp = findTree(lvf_info_base, ref, final)

                                            if take:
                                                found = False
                                                for info_key in lvf_info_tmp.keys():
                                                    for item in lvf_info_tmp[info_key]:
                                                        if arbre in final.keys():
                                                            final[item[0]] = {}
                                                            for key_arbre in final[arbre].keys():
                                                                if key_arbre in item[1].keys():
                                                                    final[item[0]][key_arbre] = (item[1][key_arbre], final[arbre][key_arbre][0])
                                                            del final[arbre]
                                                            found = True

                            # count_bad = 0
                            # for arbre in finals[-1].keys():
                            #     if (arbre.startswith('PA') or arbre.startswith('PE')):
                            #         count_bad += 1
                            #
                            # if count_bad > 0:
                            #     take = False

                            if take:
                                for arbre in résultats.keys():
                                    infos_arbre = résultats[arbre]
                                    for élément in list(infos_arbre.keys()):
                                        data = infos_arbre[élément]
                                        if len(data) == 2 and data[1] is None:
                                            del info_arbre[élément]
                                        if len(data) == 2:
                                            if len(data[1]) == 2:
                                                data = data[1]
#<class 'tuple'>: ('à', ['6', 'connaissances', 'connaissance', 'N', 'NC', 's=c', '3', 'obj.p', '_', '_'])
#<class 'tuple'>: ('instrumental, moyen ou modalité', ('de', ['22', 'racine', 'racine', 'N', 'NC', 's=c', '19', 'obj.p', '_', '_']))
                                        if (len(data) == 2 and (isinstance(data[1], str) or isinstance(data[1], int))) or \
                                                data[1][5] == 's=rel'or \
                                                data[1][4] == 'P':
                                            if isinstance(data[1], str):
                                                take = False
                                                for l in range(len(grew)):
                                                    if grew[l][0] == data[-1]:
                                                        data = (data[0], grew[l])
                                                        take = True
                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        break
                                            elif isinstance(data[1], int):
                                                résultats[arbre][élément] = (data[0], grew[data[1]])

                                            elif take:
                                                index_courant = -1
                                                if data[1] in grew:
                                                    index_courant = grew.index(data[1])
                                                else:
                                                    index_courant = getTGIndex(data[1][0])
                                                if index_courant > -1:
                                                    item = (data[1][1], data[1][3], data[1][5], index_courant)
                                                    done, find, résultat = getObjet(item, grew, talismane, sentence)
                                                else:
                                                    done = False
                                                    take = False
                                                if done:
                                                    if résultat[7] == "mod":
                                                        for l in range(grew.index(résultat) - 1, -1, -1):
                                                            if grew[l][3] == 'PONCT':
                                                                break
                                                            elif grew[l][0] == résultat[6]:
                                                                résultat = grew[l]
                                                                break
                                                    if data[1][4]  in ('P', 'P+D', "P+PRO"):
                                                        val = data[1][2]
                                                    else:
                                                        val = data[0]
                                                    if élément in ('sujet', 'objet'):
                                                        if val != résultats[arbre][élément][0]:
                                                            résultats[arbre][élément] = [résultats[arbre][élément][0], (val, résultat)]
                                                        else:
                                                            résultats[arbre][élément] = (val, résultat)
                                                    else:
                                                        résultats[arbre][élément] = (val, résultat)
                                                else:
                                                    take = False
                                                    if debug: log(getframeinfo(currentframe()).lineno)

                            if take:

                                final = finals[-1]

                                for arbre in final.keys():

                                    if not (isVPP(infos_verbe) or infos_verbe["socle"][1] == "vpr"):
                                        minuscules = "".join("".join(re.findall(r'[a-z]', arbre)))
                                        minuscules = "".join(sorted(minuscules))
                                        ref = "".join(sorted(résultats[arbre].keys())) + minuscules
                                        # constitution d'outils de jonction;
                                        if save_arbres and not(arbre.startswith('PA') or arbre.startswith('PE')):
                                            if ref not in table_des_arbres.keys():
                                                table_des_arbres[ref] = {}
                                            if arbre not in table_des_arbres[ref].keys():
                                                table_des_arbres[ref][arbre] = 1
                                            else:
                                                table_des_arbres[ref][arbre] += 1
                                            if "circonstant" in ref:
                                                a = list(résultats[arbre].keys())
                                                a.remove("circonstant")
                                                ref2 = "".join(sorted(a)) + minuscules
                                                if ref2 not in table_des_arbres.keys():
                                                    table_des_arbres[ref2] = {}
                                                if arbre not in table_des_arbres[ref2].keys():
                                                    table_des_arbres[ref2][arbre] = 1
                                                else:
                                                    table_des_arbres[ref2][arbre] += 1

                                count = 0
                                for type_possible in lvf_info_base.keys():
                                    count += len(lvf_info_base[type_possible])
                                init_arbres.append(count)
                                verbes_sentence[grew[i_verbe][2]]["cadres_initiaux"] = count

                                # flatten = lambda l: [item for sublist in l for item in sublist]

                                count = 0
                                for arbre in final.keys():
                                    if isVPP(infos_verbe) :
                                        verbes_sentence[grew[i_verbe][2]]["dispersion_vpp"].append(arbre)
                                    elif infos_verbe["socle"][1] == "vpr":
                                        verbes_sentence[grew[i_verbe][2]]["dispersion_vpa"].append(arbre)
                                    else:
                                        verbes_sentence[grew[i_verbe][2]]["dispersion_autres"].append(arbre)
                                    count += 1

                                final_arbres.append(count)
                                verbes_sentence[grew[i_verbe][2]]["cadres_finaux"].append(count)

                            else:
                                count = 0
                                for type_possible in lvf_info_base.keys():
                                    count += len(lvf_info_base[type_possible])
                                init_arbres.append(count)
                                verbes_sentence[grew[i_verbe][2]]["cadres_initiaux"] = count

                                verbes_sentence[grew[i_verbe][2]]["pertes"] += 1

                                break

                if take:
                    verbes_inifitif = [False for _ in init_arbres]
                    verbes_inifitif += [True for _ in init_arbres_infinitifs]
                    init_arbres += init_arbres_infinitifs
                    final_arbres += final_arbres_infinitifs
                    verbes += infinifs
                    finals += final_infinitifs

                    for i_verbe, arbre, init_arbres_infinitif in zip(infinifs, final_infinitifs, init_arbres_infinitifs):
                        assert len(arbre.keys()) == 1
                        if grew[i_verbe][2] not in verbes_sentence.keys():
                            verbes_sentence[grew[i_verbe][2]] = {"décompte": 0, "vpp": 0, "vpr": 0, "autres": 0,
                                                                 "infinitif":0,
                                                                 "cadres_initiaux": 0, "pertes": 0,
                                                                 "pertes_collatérales": 0, "cadres_finaux": [],
                                                                 "dispersion_autres": [], "dispersion_vpp": [],
                                                                 "dispersion_vpa": [], 'dispersion_infinitif': []}

                        verbes_sentence[grew[i_verbe][2]]["dispersion_infinitif"].append(list(arbre.keys())[0])
                        verbes_sentence[grew[i_verbe][2]]["infinitif"] += 1
                        verbes_sentence[grew[i_verbe][2]]["cadres_finaux"].append(1)

                    nb_verbes_sentence.append(len(verbes))

                    verbes_états = []
                    finals_états = []
                    specificateurs = []
                    # for verbe_type_copule in copy.deepcopy(verbes_type_copule):
                    #     if len(verbe_type_copule) == 3:
                    #         verbe = verbe_type_copule[1][2]
                    #         index_copule = talismane.index(verbe_type_copule[1])
                    #         verbes_états.append(index_copule)
                    #         if len(verbe_type_copule[2]) == 2:
                    #             finals_états.append({'définition_état': {'sujet': ('humain ou chose', verbe_type_copule[0]), 'compl-prep': (verbe, verbe_type_copule[2])}})
                    #         else:
                    #             finals_états.append({'association_état': {
                    #                 'sujet': ('humain ou chose', verbe_type_copule[0]),
                    #                 'objet': (verbe, verbe_type_copule[2])}})
                    #         verbes_type_copule.remove(verbe_type_copule)
                    # for verbe_type_copule in copy.deepcopy(verbes_type_copule):
                    #     if len(verbe_type_copule) == 2:
                    #         if verbe_type_copule[0] in talismane:
                    #             verbes_type_copule.remove(verbe_type_copule)
                    #             i_verbe = talismane.index(verbe_type_copule[0])
                    #             if i_verbe in verbes_états:
                    #                 for i_index, i_verbe_store in enumerate(verbes_états):
                    #                     if i_verbe == i_verbe_store:
                    #                         support = 'association_état' if 'association_état' in finals_états[i_index].keys() else 'définition_état'
                    #                         if 'circonstant'  in finals_états[i_index][support]:
                    #                             if finals_états[i_index][support]['circonstant']  != verbe_type_copule[1]:
                    #                                 specificateurs.append((finals_états[i_index][support]['circonstant'], verbe_type_copule[1]))
                    #                         else:
                    #                             finals_états[i_index][support]['circonstant'] = verbe_type_copule[1]
                    #             else:
                    #                 specificateurs.append(verbe_type_copule)
                    #
                    #


                    for init, final in zip(init_arbres, final_arbres):
                        if init not in stats_reductions['détail'].keys():
                            stats_reductions['détail'][init] = {}

                        if final not in stats_reductions['détail'][init].keys():
                            stats_reductions['détail'][init][final] = 1
                        else:
                            stats_reductions['détail'][init][final] += 1

                        if final not in stats_reductions['final'].keys():
                            stats_reductions['final'][final] = 1
                        else:
                            stats_reductions['final'][final] += 1

                        if init not in stats_reductions['initial'].keys():
                            stats_reductions['initial'][init] = 1
                        else:
                            stats_reductions['initial'][init] += 1
                        count_take += 1

                    if len(verbes_états) > 0:
                        verbes += verbes_états
                        finals += finals_états

                    first_tree = ''
                    sep = ''
                    évènements = {}
                    index_events = {}

                    # on réinitialise les tokens dones sur objets prépositionnels (à, dont...),
                    # c'est prêt pour en cas de changement de méthode de traitement
                    tokens_used = copy.deepcopy(tokens_dones)
                    tokens_dones = [False] * len(grew)
                    index_arguments = {}
                    index_modifieurs_arguments = []

                    # vérification des objets
                    if take:
                        for final, i_verbe in zip(finals, verbes):
                            if not take:
                                break
                            for classe in final.keys () :
                                if not take:
                                    break
                                for item in ("sujet", 'objet', 'compl-prep', 'circonstant') :
                                    if not take:
                                        break
                                    if item in final[classe].keys () :
                                        if len ( final[classe][item][1] ) == 2 :
                                            data_item = final[classe][item][1][1]
                                            axe = final[classe][item][1][0].replace ( " ", "_" )  # préposition
                                        else :
                                            data_item = final[classe][item][1]
                                            axe = final[classe][item][0].replace ( " ",  "_" )  # valeur de type chose humain modalité
                                        if len(data_item) > 1:
                                            if data_item[3] == "D" :
                                                index = getTGIndex (data_item[0] )
                                                if talismane[index][3] != "DET":
                                                    if len ( final[classe][item][1] ) == 2 :
                                                        final[classe][item][1][1] = talismane[index]
                                                    else :
                                                        final[classe][item] = (final[classe][item][0], talismane[index])
                                                else:
                                                    for k in [l + index + 1 for l in
                                                              range ( len ( grew ) - index - 1 )] :
                                                        if grew[k][3][0] == "N" :
                                                            if len ( final[classe][item][1] ) == 2 :
                                                                final[classe][item][1][1] = grew[k]
                                                                break
                                                            else :
                                                                final[classe][item] = (final[classe][item][0], grew[k])
                                                                break

                                            if data_item[4] == "CS" or data_item[2] == "que" or data_item[2] == "qui":
                                                x = getTGIndex ( data_item[0] )
                                                if data_item[4] == "CS" or data_item[2] == "qui":
                                                    obj = (grew[x][1], grew[x][3], grew[x][5], x)
                                                    done, take, résultat = getObjet(obj, grew, talismane, sentence)
                                                elif  data_item[2] == "que":
                                                    take = False
                                                    for k in sorted(list(range(x)), reverse=True):
                                                        if grew[k][3][0] == "N"  and grew[k][7] != 'obj.p' and (k == 0 or (k > 0 and grew[k-1][3][0] != "N")):
                                                            résultat = grew[k]
                                                            take = True
                                                            break
                                                if not take:
                                                    break
                                                if len ( final[classe][item][1] ) == 2 :
                                                    final[classe][item][1][1] = résultat
                                                else :
                                                    final[classe][item] = (final[classe][item][0],résultat)
                                if take and  "sujet" in final[classe].keys() and "compl-prep" in final[classe].keys():
                                    if final[classe]["sujet"] == final[classe]["compl-prep"][1]:
                                        print(i_book_file)
                                        print(i_analyse)
                                        print(sentence)
                                        exit()

                                if take and  "sujet" in final[classe].keys() and "objet" in final[classe].keys():
                                    if final[classe]["sujet"] == final[classe]["objet"]:

                                        if len(final[classe]["sujet"][1]) == 2:
                                            ref = final[classe]["sujet"][1][1][0]
                                        else:
                                            ref = final[classe]["sujet"][1][0]
                                        ref = getTGIndex(ref)
                                        for x in sorted(list(range(i_verbe)), reverse=True):
                                            if grew[x][7] == "suj" and grew[x][6] != "_" and getTGIndex (grew[x][6]) == i_verbe:
                                                # des fois il y a plusieurs sujets, et par reconstruction, on a le bon objet (massivement en ce sens en tout cas)
                                                if ref != getTGIndex(grew[x][0]):
                                                    if len ( final[classe]["sujet"][1] ) == 2 :
                                                        final[classe]["sujet"][1][1] = grew[x]
                                                        break
                                                    else :
                                                        final[classe]["sujet"] = (final[classe]["sujet"][0], grew[x])
                                                        break

                                    if final[classe]["sujet"][1] == final[classe]["objet"][1]:
                                        pass
                                if take and  "compl-prep" in final[classe].keys() and "objet" in final[classe].keys():
                                    if final[classe]["compl-prep"][1] == final[classe]["objet"]:

                                        if len(final[classe]["objet"][1]) == 2:
                                            ref = final[classe]["objet"][1][1][0]
                                        else:
                                            ref = final[classe]["objet"][1][0]
                                        ref = getTGIndex(ref)
                                        for x in sorted(list(range(i_verbe)), reverse=True):
                                            if grew[x][7] == "a.obj" and grew[x][6] != "_" and getTGIndex (grew[x][6]) == i_verbe:
                                                # des fois il y a plusieurs sujets, et par reconstruction, on a le bon objet (massivement en ce sens en tout cas)
                                                if ref != getTGIndex(grew[x][0]):
                                                    final[classe]["compl-prep"] = [final[classe]["compl-prep"][0], grew[x]]

                    for arbres, i_verbe in zip(finals, verbes):
                        if not take:
                            break
                        tokens_dones[i_verbe] = True
                        verbe = grew[i_verbe][2]
                        for key_a in arbres.keys():
                            if not take:
                                break
                            if key_a == 'association_état':
                                support = 'définition_état'
                            else:
                                support = verbe
                            clé_socle = key_a
                            clé_socle = clé_socle.strip()
                            if clé_socle not in évènements.keys():
                                évènements[clé_socle] = {}
                            if grew[i_verbe][0] not in évènements[clé_socle]:
                                évènements[clé_socle][grew[i_verbe][0]] = []

                            évènements[clé_socle][grew[i_verbe][0]]={}
                            évènements[clé_socle][grew[i_verbe][0]]['event'] = grew[i_verbe]

                            tokens = grew[i_verbe][0].split(", ")
                            for token in tokens:
                                index_events[token] = (clé_socle, grew[i_verbe][0])

                            liste_keys = list(arbres[key_a].keys())
                            
                            first_tree += sep + clé_socle + ' ' + support +' ('
                            sep = ' '

                            try:
                                for key_b in liste_keys:
                                    if not take:
                                        break
                                    résultat = arbres[key_a][key_b][1]
                                    if isinstance(résultat, str):
                                        indexTGI = getTGIndex(résultat)
                                        résultat = grew[indexTGI]
                                        arbres[key_a][key_b] = (arbres[key_a][key_b][0], résultat)
                                    if len(résultat) == 2:
                                        résultat = résultat[1]
                                        if isinstance(résultat, int):
                                            tokens_dones[résultat] = True
                                            arbres[key_a][key_b] = (arbres[key_a][key_b][0], arbres[key_a][key_b][1][0])
                                        elif len(résultat)>0 :
                                            tokens_dones[getTGIndex(résultat[0])] = True
                                        else:
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                    elif len(résultat)>0 :
                                        tokens_dones[getTGIndex(résultat[0])] = True
                                    else:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                    if len(arbres[key_a][key_b]) == 2:
                                        if arbres[key_a][key_b][1] == grew[i_verbe]:
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                            break
                                    elif arbres[key_a][key_b] == grew[i_verbe]:
                                        take = False
                                        if debug: log(getframeinfo(currentframe()).lineno)
                                        break
                                    évènements[clé_socle][grew[i_verbe][0]][key_b] = arbres[key_a][key_b]
                                    for a in arbres[key_a][key_b][1][0].split(", ") if len(arbres[key_a][key_b][1]) == 10 else arbres[key_a][key_b][1][1][0].split(", ") :
                                        index_arguments[a] =  arbres[key_a][key_b][1] if len(arbres[key_a][key_b][1]) == 10 else arbres[key_a][key_b][1][1]


                                    if liste_keys[-1] == key_b :
                                        sep2 = ' '
                                    else:
                                        sep2 = ' ; '
                                    first_tree += sep + key_b + ' : ' + arbres[key_a][key_b].__str__() + sep2
                                first_tree += ') '
                            except Exception:
                                take = False
                                if debug: log(getframeinfo(currentframe()).lineno)
                                break

                    def add_constraint(tokens_dones, index_modifieurs_arguments,  évènements, clé_socle, clé_event, contrainte, ligne_grew, ):
                        global take
                        if ligne_grew[0] == ",":
                            return

                        if clé_socle in ("arguments sur events", "arguments", "arguments hors events", "arguments sur items events", "modifications tierces", "énumération d'arguments", "coordination d'events", "coordination à objet d'event", "coordination libre", "énumération ou apposition"):
                            argument = clé_event
                            clé_event = clé_event[0]

                            if len(ligne_grew) == 2:
                                if clé_event == ligne_grew[1][0]:
                                    return

                            if not clé_socle in "arguments hors events":
                                tokens_dones[getTGIndex(clé_event)]

                        if len(ligne_grew) == 2:
                            if clé_event == ligne_grew[1][0]:
                                return

                        for each in évènements.keys () :
                            for eachi in évènements[each].keys () :
                                if "modifiers" in évènements[each][eachi].keys () and ligne_grew in \
                                        évènements[each][eachi]["modifiers"] :
                                    return

                        if clé_socle not in évènements.keys():
                            évènements[clé_socle] = {}

                        if clé_socle == 'arguments':
                            # index_modifieurs_arguments.append(clé_event[0])
                            if len(ligne_grew) == 2:
                                index_modifieurs_arguments.append(ligne_grew[1][0])
                            else:
                                index_modifieurs_arguments.append(ligne_grew[0])
                            if len(ligne_grew) == 10:
                                tokens_dones[getTGIndex(ligne_grew[0])] = True

                        if clé_event not in évènements[clé_socle].keys():
                            évènements[clé_socle][clé_event] = {}
                            if clé_socle in ("arguments", "arguments sur events", "arguments hors events", "arguments sur items events", "modifications tierces", "énumération d'arguments", "coordination d'events", "coordination à objet d'event", "coordination libre", "énumération ou apposition"):
                                évènements[clé_socle][clé_event]["argument"] = argument

                        if contrainte:
                            if "constraints" not in évènements[clé_socle][clé_event].keys():
                                évènements[clé_socle][clé_event]["constraints"] = {}
                            if len(contrainte) == 2 and isinstance(contrainte, tuple):
                                if contrainte[0] not in évènements[clé_socle][clé_event]["constraints"].keys():
                                    évènements[clé_socle][clé_event]["constraints"][contrainte[0]] = []
                                évènements[clé_socle][clé_event]["constraints"][contrainte[0]].append(contrainte[1])
                            else:
                                if contrainte not in évènements[clé_socle][clé_event]["constraints"].keys():
                                    évènements[clé_socle][clé_event]["constraints"][contrainte] = []
                                évènements[clé_socle][clé_event]["constraints"][contrainte].append(ligne_grew)

                        else:
                            if "modifiers" not in évènements[clé_socle][clé_event].keys():
                                évènements[clé_socle][clé_event]["modifiers"] = []
                            # todo j'ai une double contrainte sur les modifieurs prépositionnels à gérer
                            if len(ligne_grew) == 2:
                                if len(ligne_grew[1]) == 10:
                                    if ligne_grew[1][4] == "CS":
                                        x = getTGIndex(ligne_grew[1][0])
                                        item = (grew[x][1], grew[x][3], grew[x][5], x)
                                        done, take, résulat = getObjet(item, grew, talismane, sentence)
                                        if take:
                                            ligne_grew = (ligne_grew[0], résulat)
                                        else:
                                            return
                            évènements[clé_socle][clé_event]["modifiers"].append(ligne_grew)
                        if contrainte != 'complément de' and  not clé_socle in "arguments hors events":
                            tokens_dones[i_token] = True

                    for i_verbe in verbes:
                        if not take :
                            break
                        if grew[i_verbe][0] in obj_cpls.keys():
                            clé_socle = index_events[grew[i_verbe][0]][0]
                            clé_event = index_events[grew[i_verbe][0]][1]

                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event,
                                           'complément de', obj_cpls[grew[i_verbe][0]])

                    for i_token, done in enumerate(tokens_dones):
                        if not take :
                            break
                        if not done:
                            if grew[i_token][4] in ("ADJ", "ADV") and grew[i_token][2] != 'ne':
                                tokens = talismane[i_token][6].split(", ") # meilleure ref que Grew pour les adverbes
                            else:
                                # meilleure ref que Grew pour les adverbes
                                if grew[i_token][6] == '_':
                                    tokens = talismane[i_token][6].split(", ")
                                else:
                                    tokens = grew[i_token][6].split(", ")

                            for token in tokens:
                                tgi_index = getTGIndex(token)
                                if tgi_index == -1:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                            if not take:
                                break
                            for token in tokens:
                                if token in index_events.keys() or token in index_arguments.keys():
                                    if token in index_events.keys():
                                        clé_socle = index_events[token][0]
                                        clé_event = index_events[token][1]
                                    else:
                                        clé_socle = "arguments"
                                        clé_event = index_arguments[token]#on reconstruit l'argument

                                    guère_plus_is_negation = False
                                    if grew[i_token][2].lower() in ('guère', 'plus'):
                                        if clé_socle == "arguments":
                                            if "arguments" in évènements.keys():
                                                if "token" in évènements["arguments"].keys():
                                                    if 'modifiers' in évènements["arguments"][token].keys():
                                                        for modifier in évènements["arguments"][token]["modifiers"]:
                                                            if modifier[0] == "négation":
                                                                guère_plus_is_negation = True
                                        else:
                                            if 'constraints' in "négation" in évènements[clé_socle][token].keys():
                                                if "négation" in évènements[clé_socle][token]['constraints'].keys():
                                                    guère_plus_is_negation = True
                                    if (guère_plus_is_negation and grew[i_token][2].lower() in ('guère', 'plus')) or ((i_token == 0 or i_token+1 == len(grew) or (grew[i_token-1][4] != "DET"))  and grew[i_token][2].lower() in ("ne", "sans", 'jamais', "point", 'pas', 'aucun', 'personne', 'rien', 'nullement', 'aucunement', 'aucun', 'aucune', 'ni', "nul", 'nulle')) :

                                        modifié = False
                                        if grew[i_token][4] == "NC": # si c'est modifié par quelque chose, attention
                                            for i_g in range(len(grew)):
                                                if grew[i_g][7] == "mod" and grew[i_token][0] in grew[i_g][6].split(", "):
                                                    modifié = True

                                        if not modifié:
                                            if token in index_events.keys():
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "négation", grew[i_token])
                                            else:
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements,
                                                               clé_socle, clé_event, False, ('négation', grew[i_token]))

                                    if grew[i_token][4] in ("ADJ", "ADV"):
                                        if  (not guère_plus_is_negation and grew[i_token][2].lower() in ('guère', 'plus')) :
                                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, grew[i_token])
                                        elif (grew[i_token][2].lower() not in ('guère', 'plus', "ne", 'jamais', "point", 'pas', 'aucun', 'personne', 'rien', 'nullement', 'aucunement', 'aucun', 'aucune', 'ni', "nul", 'nulle', "sans") ):
                                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, grew[i_token])
                                    elif grew[i_token][7] == "suj" and grew[i_token][5] == "s=rel":
                                        tgi_index  = getTGIndex(grew[i_token][6])
                                        add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "relative sujet", grew[i_token])
                                    elif (grew[i_token][4] == "CS" and grew[i_token][5] == "s=s") or (grew[i_token][4] == "PROREL" and grew[i_token][5] == "s=rel" and grew[i_token][7] == "mod"):
                                        #tgi_index  = getTGIndex(grew[i_token][6])
                                        supRel = getSupportRelative(grew[i_token][0])

                                        if not supRel:
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                            break
                                        add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "relative objet", supRel)
                                    elif grew[i_token][4] == "CS" and grew[i_token][5] == "s=rel":
                                        add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "comparatif",
                                                       grew[i_token])
                                    elif grew[i_token][1] == ".":
                                        pass
                                        # add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "proposition principale", grew[i_token])
                                    elif grew[i_token][7]  == "aux.pass":
                                        add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, "passif", grew[i_token])
                                    elif tokens_used[i_token] == False:
                                        if grew[i_token][4] == "CLO":
                                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, ("à", grew[i_token]))
                                        elif grew[i_token][4] == "DET":
                                            if token in index_events.keys():
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                            else:
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, grew[i_token])
                                        elif grew[i_token][4] in ("P", "P+D", "P+PRO"):
                                            item = (grew[i_token][2], grew[i_token][3], grew[i_token][5], i_token)
                                            done, take, résultat = getObjet(item, grew, talismane, sentence)
                                            if not take:
                                                tgi_index = getTGIndex(grew[i_token][6])
                                                tokens_dones[tgi_index] = True
                                                if tgi_index > -1:
                                                    résultat = grew[tgi_index]
                                                    take = True
                                                    done = True
                                                else:
                                                    take = False
                                            if token in index_events.keys():
                                                pass_constr = résultat != évènements[clé_socle][clé_event]['event']
                                            else:
                                                pass_constr = résultat != index_arguments[token]
                                            if pass_constr:
                                                if résultat == '':
                                                    # type : Après, les soldats s ’ en reviennent avariés, et les filles restent grosses.
                                                    # la prep est sur un event externe, on ne s'embête pas
                                                    # autre situation : erreur d'analyse : il guettait les vagues du plus loin possible.
                                                    # possible non détecté comme nom
                                                    take = False
                                                    if debug: log(getframeinfo(currentframe()).lineno)
                                                    break

                                                value_axe = item[0].lower()
                                                if not (len(item[0].lower()) > 4 or item[0].lower() in ("ad", "avec")):
                                                    if (item[0].lower().startswith("a") or
                                                            item[0].lower().startswith("*a") or
                                                            item[0].lower().endswith("au") or
                                                            item[0].lower().endswith("aux")):
                                                        value_axe = "à"
                                                    elif (item[0].lower().startswith("de") or
                                                          item[0].lower().startswith("*de") or
                                                          item[0].lower().endswith("du") or
                                                          item[0].lower().endswith("de") or
                                                          item[0].lower().endswith("des")):
                                                        value_axe = "de"
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False,
                                                               (value_axe, résultat))
                                                if grew[i_token][4] in ("P+D", "P+PRO")  :
                                                    data = nettoiePandD(grew[i_token])
                                                    if data:
                                                        data[6] = résultat[6]
                                                        tgi_index_arg = getTGIndex(clé_event[0])
                                                        tgi_index_mod = getTGIndex(data[0])
                                                        if tgi_index_arg > -1 and tgi_index_mod > -1 and tgi_index_arg > tgi_index_mod:
                                                           add_constraint(tokens_dones, index_modifieurs_arguments, évènements, "arguments hors events", résultat, False, data)
                                                    else:
                                                        take = False
                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        break

                                            elif grew[i_token][4] in ("P+D", "P+PRO") :
                                                data = nettoiePandD(grew[i_token])
                                                if data:
                                                    tgi_index_arg = getTGIndex(clé_event[0])
                                                    tgi_index_mod = getTGIndex(data[0])
                                                    if tgi_index_arg > -1 and tgi_index_mod > -1 and tgi_index_arg > tgi_index_mod:
                                                       add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, data)
                                                else:
                                                    take = False
                                                    if debug: log(getframeinfo(currentframe()).lineno)
                                                    break
                                        elif not clé_socle.startswith("P") and grew[i_token][5] == "s=refl":
                                            take = False
                                        else:
                                            a= "1"
                                    else:
                                        a = " 1 "
                        else:
                            if grew[i_token][0] in index_events.keys():
                                clé_socle = index_events[grew[i_token][0]][0]
                                clé_event = index_events[grew[i_token][0]][1]
                                conjugaison = table_des_types_events[sentence][clé_event]
                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle,
                                               clé_event, conjugaison, grew[i_token])
                    links_between_tokens = {}
                    for i_token, done in enumerate(tokens_dones):
                        if not take :
                            break
                        if not done:
                            if grew[i_token][4] in ("ADJ", "ADV"):
                                tokens = talismane[i_token][6].split(", ") # meilleure ref que Grew pour les adverbes
                            else:
                                # meilleure ref que Grew pour les adverbes
                                if grew[i_token][6] == '_':
                                    tokens = talismane[i_token][6].split(", ")
                                else:
                                    tokens = grew[i_token][6].split(", ")

                            for token in tokens:
                                tgi_index = getTGIndex(token)
                                if tgi_index == -1:
                                    take = False
                                    if debug: log(getframeinfo(currentframe()).lineno)
                                    break

                            if not take:
                                break


                            for token in tokens:
                                #if not (token in index_events.keys()):
                                #if not (token in index_events.keys()):
                                # on modifie un modifieur
                                    if grew[i_token][7] == "obj.p":
                                        item = False
                                        for i_sam in sorted(list(range(i_token)), reverse=True):
                                            if grew[i_sam][4].startswith("P"):
                                                item = grew[i_sam]
                                                break
                                        if item:
                                            tokens_modifiers = item[6].split(", ")
                                            for token_modifiers in tokens_modifiers:
                                                if token_modifiers in index_events.keys() or token_modifiers in index_arguments.keys():
                                                    if token_modifiers in index_events.keys():
                                                        clé_socle = index_events[token_modifiers][0]
                                                        clé_event = index_events[token_modifiers][1]
                                                    else:
                                                        clé_socle = "arguments"
                                                        clé_event = index_arguments[token_modifiers]#on reconstruit l'argument
                                                        if clé_event in grew:
                                                            tokens_dones[grew.index(clé_event)] = True
                                                        else:
                                                            tgi_index = getTGIndex(clé_event[0])
                                                            if tgi_index > -1:
                                                                tokens_dones[tgi_index] = True
                                                            else:
                                                                take = False
                                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                                break

                                                    value_axe = item[2].lower()
                                                    if not  (len(item[2].lower())>4 or item[1].lower() in ("ad", "avec")):
                                                        if (item[2].lower().startswith("a") or
                                                                item[2].lower().startswith("*a") or
                                                                item[2].lower().endswith("au") or
                                                                item[2].lower().endswith("aux")):
                                                            value_axe = "à"
                                                        elif (item[2].lower().startswith("de") or
                                                                item[2].lower().startswith("*de") or
                                                                item[2].lower().endswith("du") or
                                                                item[2].lower().endswith("de") or
                                                                item[2].lower().endswith("des")):
                                                            value_axe = "de"
                                                    add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False,
                                                               (value_axe, grew[i_token]))
                                                else:
                                                    tgi_index = getTGIndex(token_modifiers)
                                                    if tgi_index > -1:
                                                        tokens_dones[tgi_index] = True
                                                    else:
                                                        take = False
                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        break
                                                    clé_event = grew[tgi_index] #on reconstruit l'argument
                                                    if clé_event[0] in index_modifieurs_arguments or clé_event[0] in index_arguments.keys():
                                                        clé_socle = "arguments sur items events"
                                                    else:
                                                        clé_socle = "arguments hors events"
                                                    if token in index_events.keys():
                                                        clé_socle = "arguments sur events"

                                                    value_axe = item[2].lower()
                                                    if not  (len(item[2].lower())>4 or item[1].lower() in ("ad", "avec")):
                                                        if (item[2].lower().startswith("a") or
                                                                item[2].lower().startswith("*a") or
                                                                item[2].lower().endswith("au") or
                                                                item[2].lower().endswith("aux")):
                                                            value_axe = "à"
                                                        elif (item[2].lower().startswith("de") or
                                                                item[2].lower().startswith("*de") or
                                                                item[2].lower().endswith("du") or
                                                                item[2].lower().endswith("de") or
                                                                item[2].lower().endswith("des")):
                                                            value_axe = "de"

                                                    add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False,
                                                               (value_axe, grew[i_token]))
                                                    tokens_dones[tgi_index] = True
                                                    tokens_dones[i_token] = True

                                                if item[4]  in ("P+D", "P+PRO")  and not token_modifiers in index_events.keys():
                                                    data = nettoiePandD(item)
                                                    if data :
                                                        tgi_index_arg = getTGIndex(grew[i_token][0])
                                                        tgi_index_mod = getTGIndex(data[0])
                                                        if tgi_index_arg > -1 and tgi_index_mod > -1 and tgi_index_arg > tgi_index_mod:
                                                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements, "arguments hors events", grew[tgi_index_arg], False, data)
                                                    else:
                                                        take = False
                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        break
                                                #elif item[4] == "P+D" :
                                                #    take = False
                                                #    if debug: log(getframeinfo(currentframe()).lineno)
                                                #else:
                                                #    take = False
                                                #    if debug: log(getframeinfo(currentframe()).lineno)
                                        else:
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                    elif grew[i_token][4] in ("ADJ", "ADV", "DET") or grew[i_token][4].startswith('N'):
                                        if grew[i_token][6] == '_' :
                                            tokens_modifiers = talismane[i_token][6].split(", ")
                                        else:
                                            tokens_modifiers = grew[i_token][6].split(", ")
                                        for token_modifiers in tokens_modifiers:
                                            tgi_index = getTGIndex(token_modifiers)
                                            if tgi_index == -1:
                                                take = False
                                                if debug: log(getframeinfo(currentframe()).lineno)
                                            else:
                                                clé_event = grew[tgi_index]  # on reconstruit l'argument
                                                if clé_event[0] in index_modifieurs_arguments or clé_event[0] in index_arguments.keys():
                                                    clé_socle = "arguments sur items events"
                                                    tokens_dones[tgi_index] = True
                                                else:
                                                    clé_socle = "arguments hors events"
                                                if token in index_events.keys():
                                                    clé_socle = "arguments sur events"
                                                if grew[tgi_index][3] != 'C':
                                                    add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle, clé_event, False, grew[i_token])



                                    elif grew[i_token][4] in ("P",  "P+D", "P+PRO") :
                                        item = (grew[i_token][2], grew[i_token][3], grew[i_token][5], i_token)
                                        done, take, résultat = getObjet(item, grew, talismane, sentence)
                                        if not take:
                                            take = False
                                            if debug: log(getframeinfo(currentframe()).lineno)
                                            break
                                        else:
                                            links_between_tokens[résultat[0]] = (grew[getTGIndex(grew[i_token][6])], grew[i_token],résultat)
                                        # l'item est construite sous la forme :
                                        # item = (grew[x][1], grew[x][3], grew[x][5], x) #avec x l'index de l'objet à remonter

                    clé_socle = "arguments hors events"
                    if clé_socle in évènements.keys():
                        for clé_event in list(évènements[clé_socle].keys()):
                            if clé_event in links_between_tokens.keys():
                                source, liaison, rattachement = links_between_tokens[clé_event]
                                data = nettoiePandD(liaison)
                                out = False
                                if source[3] == "C":
                                    if source[2] == '*se' and source[4] == "CS":
                                        source[2] == 'si'
                                        out = True
                                if not out:
                                    if data:
                                        tgi_index_arg = getTGIndex(grew[i_token][0])
                                        tgi_index_mod = getTGIndex(data[0])
                                        if  clé_event not in index_events.keys():
                                            if tgi_index_arg > -1 and tgi_index_mod > -1 and tgi_index_arg > tgi_index_mod:
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle,
                                                           rattachement, False, data)
                                    clé_socle = "modifications tierces"

                                    if source[3] == "C" :
                                        pointeC = getTGIndex ( source[6] )
                                        if pointeC > -1:
                                            x = pointeC
                                            item = (grew[x][1], grew[x][3], grew[x][5], x)
                                            circonstant = grew[x][2].lower()
                                            if len(circonstant)<=4 or circonstant == "avec" or grew[x][3] == "clo":
                                                if (("à" in circonstant or "au" in circonstant) and len(circonstant) <= 4) or  grew[x][3] == "clo":
                                                    circonstant = "à"
                                                elif ("de" in circonstant or "du" in circonstant) and len(circonstant) <= 4:
                                                    circonstant = "de"
                                            if grew[x][3] == "clo":
                                                résulat = grew[x]
                                            else:
                                                done, take, résulat = getObjet(item, grew, talismane, sentence)

                                            if take and tokens_dones[getTGIndex(résulat[0])]:
                                                # s'il n'est pas encore traité, le C appartient peut-être à une plus vaste coordination
                                                if len(rattachement) == 10:
                                                    rattachement = [rattachement]
                                                rattachement.insert(0, résulat)

                                    value_axe = liaison[2].lower()
                                    if not (len(liaison[2].lower()) > 4 or liaison[1].lower() in ("ad", "avec")):
                                        if (liaison[2].lower().startswith("a") or
                                                liaison[2].lower().startswith("*a") or
                                                liaison[2].lower().endswith("au") or
                                                liaison[2].lower().endswith("aux")):
                                            value_axe = "à"
                                        elif (liaison[2].lower().startswith("de") or
                                              liaison[2].lower().startswith("*de") or
                                              liaison[2].lower().endswith("du") or
                                              liaison[2].lower().endswith("de") or
                                              liaison[2].lower().endswith("des")):
                                            value_axe = "de"
                                    add_constraint(tokens_dones, index_modifieurs_arguments, évènements, clé_socle,
                                                   source, False, (value_axe, rattachement))
                    # s=c : s=clause
                                                
                    # énumération , coordination
                    if take:
                        for g, t in zip(grew, tokens_dones):
                            if not t and g[3][0] not in ("P", "P+D", "P+PRO", "V", "D") and g[5] != 's=refl':
                                if g[3] == "C":
                                    if g[2] == '*se' and g[4] == "CS":
                                        g[2] == 'si'
                                    index_cible = getTGIndex(g[6])
                                    index_source = getTGIndex(g[0])
                                    if index_cible == -1:
                                        index_cible = getTGIndex(talismane[index_source][6])

                                    label_ref_1 = grew[index_cible][0]
                                    évènement_1 = False
                                    évènement_2 = False
                                    for event_1 in évènements.keys():
                                        if event_1 != "arguments hors events" and label_ref_1 in évènements[event_1].keys():
                                            # si c'est rattaché à un event, c'est qu'on a une coordination d'event
                                            if label_ref_1 in index_events:
                                                label_1 = "event"
                                            else:
                                                label_1 = "argument"
                                            évènement_1 = (label_1, event_1, label_ref_1)
                                            for x in range(len(grew)-index_source-1):
                                                step = x + index_source + 1
                                                index_element_ciblé = getTGIndex(grew[step][6])
                                                if index_element_ciblé == -1:
                                                    index_element_ciblé = getTGIndex(talismane[step][6])

                                                if index_element_ciblé == index_source:
                                                    label_ref_2 = grew[step][0]
                                                    for event_2 in évènements.keys():
                                                        if event_2 != "arguments hors events" and label_ref_2 in évènements[event_2].keys():
                                                            if label_ref_2 in index_events:
                                                                label_2 = "event"
                                                            else:
                                                                label_2 = "argument"
                                                            évènement_2 = (label_2, event_2, label_ref_2)
                                                            break
                                                    if évènement_2:
                                                        break
                                            if évènement_2:
                                                break
                                    if évènement_1 and évènement_2:

                                        clé_socle = "coordination d'events"
                                        add_constraint(tokens_dones, index_modifieurs_arguments, évènements,
                                                       clé_socle, g, False, (g[2], évènement_1, évènement_2))
                                        break
                                    else:
                                        cible = grew[index_cible] #coordination de clauses, d'items
                                        if grew[index_cible][4] in ('P', 'P+D', "P+PRO") or grew[index_cible][3] == "clo":
                                            circonstant = grew[index_cible][2].lower()
                                            if len(circonstant)<=4 or circonstant == "avec" or  grew[index_cible][3] == "clo":
                                                if (("à" in circonstant or "au" in circonstant) and len(circonstant) <= 4) or  grew[index_cible][3] == "clo":
                                                    circonstant = "à"
                                                elif ("de" in circonstant or "du" in circonstant) and len(
                                                        circonstant) <= 4:
                                                    circonstant = "de"
                                            x = getTGIndex(grew[index_cible][0])
                                            item = (grew[x][1], grew[x][3], grew[x][5], x)
                                            done, take, résulat = getObjet(item, grew, talismane, sentence)
                                            if take:
                                                step = getTGIndex(résulat[0])
                                            else:
                                                break
                                            if not take:
                                                break
                                            end = grew[step]
                                            take = True
                                            break
                                        nature_cibles = set(grew[index_cible][3])
                                        enumeration = []
                                        end =  ''
                                        take = False
                                        for x in range(len(grew)-index_source-1):
                                            step = x + index_source + 1
                                            index_element_ciblé = getTGIndex(grew[step][6])
                                            if index_element_ciblé == -1:
                                                index_element_ciblé = getTGIndex(talismane[step][6])
                                            if index_element_ciblé == index_source:
                                                if grew[step][4]  in ("P", "P+D", "P+PRO")  or grew[step][3] == "clo":
                                                    circonstant = grew[step][2]
                                                    if len(circonstant)<=4 or circonstant == "avec"or  grew[index_cible][3] == "clo":
                                                        if (("à" in circonstant or "au" in circonstant) and len(circonstant) <= 4) or grew[step][3] == "clo":
                                                            circonstant = "à"
                                                        elif ("de" in circonstant or "du" in circonstant) and len(circonstant) <= 4:
                                                            circonstant = "de"
                                                        x = getTGIndex(grew[step][0])
                                                    item = (grew[x][1], grew[x][3], grew[x][5], x)
                                                    done, take, résulat = getObjet(item, grew, talismane, sentence)
                                                    if take:
                                                        step = getTGIndex(résulat[0])
                                                    else:
                                                        break
                                                    if not take:
                                                        break
                                                take = True
                                                break

                                        if not take : break
                                        end = grew[step]
                                        nature_cibles.add(grew[step][3])
                                        clé_socle = "coordination à objet d'event"
                                        if not évènement_1:
                                            clé_socle = "coordination libre"
                                        if len(nature_cibles) > 1:
                                            if not évènement_1:
                                                évènement_1 = grew[index_cible]
                                            if grew[step][3] in ("*se", "si"):
                                                take = False
                                                break
                                            add_constraint(tokens_dones, index_modifieurs_arguments, évènements,
                                                           clé_socle, g, False, (g[2], [évènement_1, grew[step]]))
                                        else:
                                            enumeration = []
                                            if not évènement_1:
                                                # sous réserve, mais le modifieurs ne serait pas séparé par une virgule (?) sauf ", à vrai dire"
                                                excluded = []
                                                min_x = -1
                                                for x in sorted(range(index_cible+1), reverse=True):
                                                    if tokens_dones[x] and grew[x][3] in nature_cibles:
                                                        min_x = x
                                                        break
                                                if min_x > -1:
                                                    enumeration.append(grew[min_x])
                                                for x in sorted(range(index_cible+1), reverse=True):
                                                    if grew[x][3] in nature_cibles:
                                                        for event in évènements.keys():
                                                            if ("arguments hors events" not in évènements.keys() or grew[x][0] not in évènements["arguments hors events"].keys()) and grew[x][0] in évènements[event].keys():
                                                                if "modifiers" in évènements[event][grew[x][0]].keys():
                                                                    if len(évènements[event][grew[x][0]]["modifiers"]) == 2:
                                                                        excluded += évènements[event][grew[x][0]]["modifiers"][1]
                                                                    else:
                                                                        excluded += évènements[event][grew[x][0]]["modifiers"]
                                                if enumeration == []:
                                                    enumeration = [grew[index_cible]]
                                                index_cible = getTGIndex(enumeration[0][0])
                                                ponct = False
                                                for i_t in range(step - index_cible - 1):
                                                    if grew[i_t + index_cible + 1][3] in nature_cibles :
                                                        if ponct and grew[i_t + index_cible +1] not in excluded:
                                                            if i_t + index_cible +1 > min_x:
                                                                enumeration.append(grew[i_t + index_cible +1 ])
                                                                ponct = False
                                                    elif grew[i_t + index_cible +1 ][3] == 'PONCT':
                                                        if i_t + index_cible +1 >= min_x:
                                                            ponct = True
                                                enumeration.append(grew[step])
                                                add_constraint(tokens_dones, index_modifieurs_arguments, évènements,
                                                               clé_socle, g, False, (g[2], enumeration))

                                else:
                                    # si ce n'est pas une coordination
                                    a = "1"
                                    pass

                    # on va maintenant chercher des énumérations caractéristiques.
                    if take:
                        tokens_added = []
                        for key in ("arguments", "arguments sur items events", "arguments hors events"):
                            if key in évènements.keys():
                                tokens_treated = []
                                for token in évènements[key].keys():
                                    if token not in tokens_treated:
                                        if "modifiers" in évènements[key][token].keys() and "argument" in évènements[key][token].keys():
                                            argument = évènements[key][token]["argument"]
                                            modifieurs = évènements[key][token]["modifiers"]
                                            natures_argument = [argument[3]]
                                            natures_modifieurs = {}
                                            withPrep = {}
                                            for item in modifieurs:
                                                if len(item) == 2:
                                                    #on va gérer à la plage, à la montagne, mais pas plus
                                                    if item[0] + item[1][3] not in natures_modifieurs.keys():
                                                        natures_argument.append(item[0] + natures_argument[0])
                                                        natures_modifieurs[item[0] + item[1][3]] = []
                                                        withPrep[item[0] + item[1][3]] = True
                                                    natures_modifieurs[item[0] + item[1][3]].append(item)
                                                else:
                                                    if item[3] not in natures_modifieurs.keys():
                                                        natures_modifieurs[item[3]] = []
                                                        withPrep[item[3]] = False
                                                    natures_modifieurs[item[3]].append(item)
                                            for nature_argument in natures_argument:
                                                if nature_argument in natures_modifieurs.keys():
                                                    index_start = getTGIndex(argument[0])
                                                    index_end = getTGIndex(modifieurs[-1][0])
                                                    part = ""

                                                    while index_start < index_end:
                                                        part += grew[index_start][1]
                                                        index_start += 1
                                                    if len(natures_modifieurs[nature_argument]) == part.count(","):
                                                        # on a a priori une énumaration, sinon on ne sait pas : si with prep: TODO
                                                        if not withPrep[nature_argument]:
                                                            tokens_treated = natures_modifieurs[nature_argument]
                                                            énumération = [argument] + natures_modifieurs[nature_argument]
                                                            for i_tmp in range(len(natures_modifieurs[nature_argument])):
                                                                tokens_treated.append(natures_modifieurs[nature_argument][i_tmp][0])
                                                            clé_socle = "énumération d'arguments"
                                                            is_a_coordination = False
                                                            if "coordination libre" in évènements.keys():
                                                                for coord_token in évènements["coordination libre"].keys():
                                                                    for i_coord, coordonnées in enumerate(évènements["coordination libre"][coord_token]["modifiers"]):
                                                                        #on récupère le premier argument de la coordination, son index
                                                                        if set(tokens_dones[index_end + 1:getTGIndex(coordonnées[1][0][0])]) == {False}:
                                                                            #si entre la coordination et notre énumération il n'y a pas d'aarguments, d'évents traités
                                                                            # on a une seule et même liste
                                                                            # coordonnant = grew[getTGIndex(coord_token)]
                                                                            # coordonnés = coordonnées[1]
                                                                            is_a_coordination = (i_coord, coordonnées[0], énumération + coordonnées[1])
                                                                            break
                                                                    if is_a_coordination:
                                                                        if len(natures_modifieurs[nature_argument]) and not is_a_coordination[2][0][0] in tokens_added:
                                                                            tokens_added.append(is_a_coordination[2][0][0])
                                                                            évènements["coordination libre"][coord_token]["modifiers"][is_a_coordination[0]] = (is_a_coordination[1], is_a_coordination[2])
                                                                        break
                                                            if not is_a_coordination:
                                                                if len(natures_modifieurs[nature_argument]) == 1 and not withPrep[nature_argument]:
                                                                    clé_socle = "énumération ou apposition"
                                                                if not énumération[0][0] in tokens_added:
                                                                    tokens_added.append(énumération[0][0])
                                                                    add_constraint(tokens_dones, index_modifieurs_arguments, évènements,clé_socle, argument, False, énumération)

                                for token_treated in tokens_treated:
                                    if "modifiers" in évènements[key][token].keys() and token_treated in évènements[key][token]["modifiers"]:
                                        évènements[key][token]["modifiers"].remove(token_treated)
                                if "modifiers" in évènements[key][token].keys() and len(évènements[key][token]["modifiers"])==0:
                                    del évènements[key][token]["modifiers"]
                                if len(évènements[key][token].keys())== 1: # il ne reste plus que l'argument
                                    del évènements[key][token]
                                if len(évènements[key].keys()) == 0:
                                    del évènements[key]

                    if take:
                        for event_key in évènements.keys():
                            if event_key not in ("arguments sur events", "arguments", "arguments hors events", "arguments sur items events", "modifications tierces", "énumération d'arguments", "coordination d'events", "coordination à objet d'event", "coordination libre", "énumération ou apposition"):
                                for token in évènements[event_key].keys():
                                    assert "constraints" in évènements[event_key][token].keys()
                                    for value in ('infinitif', 'vpp', 'vpa', 'conj'):
                                        if value in évènements[event_key][token]["constraints"]:
                                            bis = évènements[event_key][token]["constraints"][value]
                                            while [] in bis:
                                                bis.remove([])
                                            auxiliaires = []
                                            while len(bis) > 0:
                                                auxiliaire = bis.pop(0)
                                                if len(auxiliaire) == 10:
                                                    auxiliaires.append(auxiliaire)
                                                else:
                                                    for each in auxiliaire:
                                                        bis.append(each)

                                            évènements[event_key][token]["constraints"]["conjugaison"] = {"mode":value, "auxiliaires":auxiliaires}
                                            # un verbe au passif s'accorde avec le sujet
                                            for verbe in  [évènements[event_key][token]["event"]] + auxiliaires:
                                                if value != "vpa" and verbe[4].upper() != "VPP":
                                                    terminaisons_pluriel = ("îmes", "îtes", "ons", "ez", "nt", "âmes", "âtes", "ûmes", "ûtes")
                                                    for terminaison in terminaisons_pluriel:
                                                        if verbe[1].endswith(terminaison):
                                                            évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "pluriel"
                                                            break
                                                    if "nombre" in évènements[event_key][token]["constraints"]["conjugaison"].keys():
                                                        break

                                            if value not in ("vpa", "vpp", 'infinitif'):
                                                if "nombre" not in évènements[event_key][token]["constraints"]["conjugaison"].keys():
                                                    évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "singulier"
                                            del évènements[event_key][token]["constraints"][value]
                                            if not "nombre" in évènements[event_key][token]["constraints"]["conjugaison"].keys():
                                                évènements[event_key][token]["constraints"]["conjugaison"]['nombre'] = "inconnu"

                                            mood = ""
                                            aspect = ""
                                            tense = ""
                                            person = ""
                                            verbe = évènements[event_key][token]['event']
                                            auxiliaires = copy.deepcopy(évènements[event_key][token]["constraints"]["conjugaison"]['auxiliaires'])
                                            is_tps_auxilié = False

                                            for auxi in évènements[event_key][token]["constraints"]["conjugaison"]['auxiliaires']:
                                                if not auxi[2] in ("être", "avoir"):# or (auxi[2] in ("être", "avoir") and auxi[7] == 'aux.caus'):
                                                    if len(auxiliaires) == 1:
                                                        auxiliaires = []
                                                        mode = "vpp"
                                                        value = "vpp"
                                                        verbe = auxi
                                                        is_tps_auxilié = True
                                                        if "sujet" in évènements[event_key][token].keys() and "objet" in évènements[event_key][token].keys():
                                                            del évènements[event_key][token]['sujet']
                                                    elif len(auxiliaires) == 2:
                                                        verbe = auxiliaires.pop(auxiliaires.index(auxi))
                                                        mode = "vpp"
                                                        value = "conj"
                                                    else:
                                                        a = 1

                                            #     "liens"].keys():
                                            #     key_aux = 'aux.tps' if 'aux.tps' in verbe_info[
                                            #         "liens"].keys() else 'aux_tps'
                                            #     for auxi in verbe_info["liens"][key_aux]:
                                            #         if not grew[auxi[-1]][2] in ("être", "avoir"):
                                            #             return True

                                            while len(auxiliaires)>=3:
                                                évènements[event_key][token]["constraints"]["conjugaison"]['auxiliaires'].pop(-1)
                                            if len(auxiliaires)==2:
                                                if auxiliaires[-1][7] == 'aux.tps' and auxiliaires[0][7] == 'aux.tps':
                                                    évènements[event_key][token]["constraints"]["conjugaison"]['auxiliaires'].pop(-1)

                                            if len(auxiliaires)>0:
                                                if auxiliaires[-1][7] == 'aux.pass' and auxiliaires[0][7] == 'aux.tps':
                                                    évènements[event_key][token]["constraints"]["conjugaison"]['auxiliaires'].pop(-1)
                                                for auxi in auxiliaires:
                                                    if auxi[7] in ('aux.tps') and auxi[2] not in ( "être", "avoir"):
                                                        a = 1
                                                if len(auxiliaires) <= 2 :
                                                    verbe = auxiliaires[-1]
                                                    if auxiliaires[-1][7] == 'aux.tps':
                                                        is_tps_auxilié = True

                                                    elif auxiliaires[0][7] in ('aux.pass', 'aux.mod'):
                                                        pass
                                                    else:
                                                        print(sentence)
                                                        print(évènements[event_key][token]['event'])
                                                        print(auxiliaires)
                                                        raise Exception
                                                    #"PAST + PARTICIPLE"
                                                else:
                                                    print(sentence)
                                                    print(verbe)
                                                    print(auxiliaires)
                                                    raise Exception

                                            conjugaisons_possibles = []

                                            if value in ("vpa", "vpp", 'infinitif'):
                                                if value == 'infinitif':
                                                    mood = [None]
                                                    aspect = [None]
                                                    tense = [INFINITIVE]
                                                else:
                                                    mood = [INDICATIVE]
                                                    aspect = [PROGRESSIVE]
                                                    if value == "vpa":
                                                        tense = [PRESENT]
                                                    else:
                                                        tense = [PAST]
                                                number = [None]
                                                person = [None]
                                                if 'sujet' in évènements[event_key][token].keys():
                                                    person = [3] # PERSON = [None, 1, 2, 3]
                                                    sujet = évènements[event_key][token]['sujet'][1]
                                                    if len(sujet) != 10:
                                                        take = False
                                                        if debug: log(getframeinfo(currentframe()).lineno)
                                                        break

                                                    if sujet[2] == "cln":
                                                        if len(sujet[1]) < 3:
                                                            if sujet[1].lower().startswith("t"):
                                                                person = [2]
                                                                number = [SG]
                                                                évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "singulier"
                                                            elif sujet[1].lower().startswith("j"):
                                                                person = [1]
                                                                number = [SG]
                                                                évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "singulier"
                                                        elif sujet[1].lower() == "nous":
                                                            person = [1]
                                                            évènements[event_key][token]["constraints"]["conjugaison"][
                                                                "nombre"] = "pluriel"
                                                            number = [PL]
                                                        elif sujet[1].lower() == "vous":
                                                            person = [2]
                                                            number = [PL]
                                                conjugaisons_possibles.append((tense[0], person[0], mood[0], aspect[0], number[0], 'auxilié' if is_tps_auxilié else ''))

                                            if value == "conj" or is_tps_auxilié:
                                                mood = [INDICATIVE, IMPERATIVE, CONDITIONAL, SUBJUNCTIVE]
                                                aspect = [IMPERFECTIVE, PERFECTIVE]
                                                tense = [PRESENT, PAST, FUTURE]
                                                if évènements[event_key][token]["constraints"]["conjugaison"]['nombre'] == "singulier":
                                                    number = [SG]
                                                elif évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] == "pluriel":
                                                    number = [PL]
                                                else:
                                                    number = [SG, PL]
                                                if not 'sujet' in évènements[event_key][token].keys():
                                                    person = [1, 2, 3]
                                                else:
                                                    person = [3]
                                                if not "sujet" in évènements[event_key][token].keys():
                                                    mood = [IMPERATIVE]
                                                else:
                                                    mood = [INDICATIVE, CONDITIONAL, SUBJUNCTIVE]
                                                    if not "qu" in sentence:
                                                        mood = [INDICATIVE, CONDITIONAL]
                                                    if len(verbe) == 10:
                                                        if len ([k[2] for k in grew[:getTGIndex ( token )] if  k[2].lower() == "que"] ) == 0 :
                                                            mood = [INDICATIVE, CONDITIONAL]
                                                        elif (not verbe[5] == "m=subj") and (len([k[2] for k in grew[:getTGIndex(token)] if k[2].lower() == "que"]) > 0):
                                                            ref_point = grew[-1][6]
                                                            if ref_point == "_":
                                                                ref_point = talismane[-1][6]
                                                            if ref_point == "_":
                                                                ref_point = stanford[-1][6]
                                                            ref_point = getTGIndex(ref_point)
                                                            if len(évènements[event_key][token]['sujet']) == 10:
                                                                comp_point = évènements[event_key][token]['sujet'][0]
                                                            elif len(évènements[event_key][token]['sujet']) == 2 and len(évènements[event_key][token]['sujet'][1]) == 10:
                                                                comp_point = évènements[event_key][token]['sujet'][1][0]
                                                            else:
                                                                take = False
                                                                if debug: log(getframeinfo(currentframe()).lineno)
                                                                break
                                                            if ref_point == getTGIndex(comp_point):
                                                                mood = [INDICATIVE, CONDITIONAL]
                                                                if sentence == 'Plus légers que le matin, nous sautions, nous courions sans fatigue, sans obstacle.':
                                                                    mood = [INDICATIVE]
                                                                    if verbe[1] == "connaissiez" and "seulement, pour comprendre la situation" in sentence:
                                                                        mood = [SUBJUNCTIVE]
                                                                    elif verbe[1] == "cachiez" and "il est nécessaire que vous ne cachiez" in sentence:
                                                                        mood = [SUBJUNCTIVE]
                                                        elif verbe[5] == "m=subj":
                                                            mood = [SUBJUNCTIVE]

                                                    # on a dix phrase indéterminées algorithmiquement, pour conditionnel ou indicatif:
                                                    verbes_indicatif = [("distendaient", "Ils se distendaient démesurément, ils effaçaient le temps et l' espace, ils réduisaient l' univers à une quantité négligeable."),
                                                                        ("sourdait", "Il la trouvait rose, les lèvres en moue ; de la sueur sourdait sur la peau mate."),
                                                                        ("empreignaient", "Ils cherchaient une compensation à leurs maux dans l' exaltation de leurs sentiments, et leurs paroles, leurs joies, leurs jeux s' empreignaient d' une espèce de frénésie."),
                                                                        ("sourdait", "Tous les peuples, dans tous leurs langages, poussaient d' incertaines rumeurs : sur la rive sourdait la colère des Paümotu réclamant on ne savait quels esclaves."),
                                                                        ("appendait", "Sa sagaie à pointe de corne appendait de guinguois à sa taille, il tenait à la main droite l' énorme massue de bois de chêne."),
                                                                        ("sourdait", "Par les verrières rustiques une lumière sourdait, aimable et naïve, et des rayons jonquilles, rubis feu, bleu intense, éclairaient les piliers, les chaises, le dallage."),
                                                                        ("distendait", "Ses yeux pétillaient de désirs ; un sourire lubrique distendait sa bouche."),
                                                                        ("éperdait", "Toute haine s' éperdait."),
                                                                        ("retendait", "La conquête de l' empire grec ébranlait son autorité dans l' Occident plus qu' elle ne retendait dans l' Orient."),
                                                                        ("sourdaient", "Les réflexions sourdaient en lui par bouillonnantes cascades.")]
                                                    verbes_indicatif += [("sautions", "Plus légers que le matin, nous sautions, nous courions sans fatigue, sans obstacle."),
                                                                         ("courions", "Plus légers que le matin, nous sautions, nous courions sans fatigue, sans obstacle."),
                                                                         ("venions", "Nous ne quittions l' étude que pour discourir entre nous des matières que nous venions d' apprendre, et, chose déplorable !"),
                                                                         ("rencontrions", "Tu sais que, pendant les premières années de mon hymen, madame la comtesse voyageait sans cesse, et que nous nous rencontrions fort peu."),
                                                                         ("appelions", "Il avait une maîtresse, une pauvre fille débile, rachitique, à ce point que, nommant l' homme Agricol, nous appelions sa femme la Mayeux, une chétive créature qui s' était abandonnée éperdument à ce grand garçon."),
                                                                         ("descendiez", "Il a fallu que vous descendiez dans la cave, hein ?"),
                                                                         ("recevions", "Il me semble que nous recevions de bien grandes grâces."),
                                                                         ("connaissiez", "Pourquoi cette princesse que vous ne connaissiez pas s' intéresse-t-elle ainsi à vous ?"),
                                                                         ("estimions", "Nous venons de perdre un ami que nous estimions tous pour son amour des lettres."),
                                                                         ("habitions", "Nous fûmes témoins, le 8 août, d' une horrible aventure qui se passa dans la rue même que nous habitions."),
                                                                         ("devenions", "Il me parut que nous devenions d' une grande pâleur ; les rideaux de la fenêtre remuaient ; nous étions sous l' influence de Minuit."),
                                                                         ("découchions", "Nous ne sortions que deux fois par mois, et nous ne découchions qu' au jour de l' an."),
                                                                         ("remontions", "Il devenait évident que nous remontions l' échelle de la vie animale dont l' homme occupe le sommet."),
                                                                         ("parlions", "Le rôle fatal du bey Pintorovitch ne s' explique que par certaines modifications apportées dans le poème à l' histoire véritable dont nous parlions tout à l' heure."),
                                                                         ("touchions", "Je savais que nous touchions au développement de ses vues secrètes."),
                                                                         ("osions", "Nous espérions que le roi aurait passé la frontière, mais nous n' osions calculer l' effet que cet événement causerait dans Paris."),
                                                                         ("voyions", "Ce n' était pas la première fois que nous nous voyions."),
                                                                         ("preniez", "Ce n' est pas la première fois que vous boudez, mais ordinairement vous preniez la peine de motiver votre absence."),
                                                                         ("serviez","Elle trouvait que vous serviez trop largement la pratique."),
                                                                         ("respirions", "Le bon Cadillan éprouvait la même chose : il semblait que nous respirions plus librement."),
                                                                         ("pensiez", "Sans doute vos blessures sont moins graves que vous ne le pensiez ?"),
                                                                         ("donniez", "En me l' annonçant aussitôt que vous l' avez formé, vous me donniez la facilité de le combattre."),
                                                                         ("improvisions", "Le soleil resplendissait gaiement tout alentour, et c' était là que, dans les beaux jours d' été, nous improvisions nos farces."),
                                                                         ("habitions", "Le lecteur sait déjà que nous habitions une des contrées privilégiées du globe."),
                                                                         ("suivions", "Oui, c' était bien légèrement que nous suivions les traces de Jésus !"),
                                                                         ("cherchiez", "Figurez-vous, il m' avait semblé que vous cherchiez une canne."),
                                                                         ("appelions", "Au mois d' août de cette même année, ma femme eut un autre drole, qui fut enregistré sous le nom de Bernard, mais que nous appelions tant qu' il était petit, Berny."),
                                                                         ("voyions", "On piqua droit sur les tentes ; il faisait chaud, et nous avions encore à traverser une longue lisière de sables jaunes que nous voyions briller entre la montagne et nous, rude passage en plein midi, sous un soleil sans nuages."),
                                                                         ("bâtissions", "Figurez-vous que depuis des siècles nous bâtissions nos villes, non pas avec des pierres, de la chaux et des briques, mais au moyen d' une substance malléable, péniblement sécrétée par des organes spéciaux de notre corps."),
                                                                         ("formions", "Aïdine Aga n' avait que peu de sympathie pour les principaux habitants de l' île, et pour son lieutenant commandant de la garnison ; le Saïd et moi, nous formions sa société de prédilection ; il m' entretenait de toutes ses affaires, et, chose plus extraordinaire, il me parlait même de son harem."),
                                                                         ("citions", "Dans cette malheureuse ville de Beaumont, que nous citions déjà plus haut, un convoyeur trouva un moyen plus simple et plus expéditif de s' enrichir."),
                                                                         ("montions", "Chapitre II À mesure que nous montions, la végétation devenait plus rare."),
                                                                         ("occupiez", "Soldats, quel que soit le rang que vous occupiez dans l' armée, la reconnaissance de la nation vous attend."),
                                                                         ("espériez", "Ma recommandation vous a-t-elle procuré le travail que vous espériez ?"),
                                                                         ("voulions", "La clarté de la nuit nous permettait encore de distinguer, sous le vent à nous, le point mobile que nous voulions atteindre."),
                                                                         ("répariez", "Henry, vous avez fait une faute, il faut que vous la répariez."),
                                                                         ("venions", "La pampa, où paissait le troupeau, avait plus d' un mille d' étendue et, comme celles que nous venions de traverser, elle était environnée de forêts."),
                                                                         ("voyions", "Sans doute c' était le même mince et étroit visage que nous voyions Robert et moi."),
                                                                         ("acceptiez", "Vous saviez qu' aucun trafic important ne passerait plus par le trajet dont vous acceptiez cependant d' étudier les possibilités de restauration."),
                                                                         ("enfoncions", "En effet, nous nous acheminâmes sur le liège du bonhomme et je constatai que nous n' enfoncions nullement."),
                                                                         ("connaissions", "Nous demandâmes asile à Ribet, le grand commissionnaire du roulage, que nous connaissions, et nous nous installâmes dans une chambre donnant sur le port."),
                                                                         ("avancions", "Je ne vois pas que nous avancions en besogne."),
                                                                         ("approchions", "Le bruit des cognées frappant sur les troncs d' arbres m' avertit que nous approchions ; cependant on n' apercevait pas la moindre trace des monuments, la forêt vierge nous enveloppait dans l' épaisseur de ses ombres, et nous n' avancions qu' avec difficulté."),
                                                                         ("avancions", "Le bruit des cognées frappant sur les troncs d' arbres m' avertit que nous approchions ; cependant on n' apercevait pas la moindre trace des monuments, la forêt vierge nous enveloppait dans l' épaisseur de ses ombres, et nous n' avancions qu' avec difficulté."),
                                                                         ("rendions", "Par suite d' une erreur commise par l' ordonnance dans leur enregistrement, mes bagages furent expédiés à une station frontière beaucoup plus au nord que celle où nous nous rendions."),
                                                                         ("séjournions", "Telles étaient les transactions diplomatiques que l' on passait à Vienne, tandis que nous séjournions à Gand."),
                                                                         ("essayions", "Il était fixé à un seul de ses montants par une serrure que nous essayions de forcer à l' aide d' une baïonnette."),
                                                                         ("négligiez", "Ce lord Steyne, qui l' année dernière ne daignait pas m' honorer d' un coup d' oeil à la cour, a fini par découvrir qu' il pouvait bien y avoir quelque chose dans Pitt Crawley ; mais cependant c' est le même homme, mes beaux messieurs, que vous négligiez naguère encore, l' occasion seule jusqu' ici avait manqué."),
                                                                         ("résumiez", "Tout ce que vous m' avez dit sur les vivants et sur les morts est bien vrai, et c' est ma foi que vous me résumiez."),
                                                                         ("attendions", "Vous savez que nous attendions leur retour pour cela."),
                                                                         ("vivions", "Je l' appelle voisin parce que sa plantation était proche de la mienne, et que nous vivions très-amicalement."),
                                                                         ("pénétrions", "Je reconnais que nous, les Français, nous pénétrions injustement sur le territoire des autres."),
                                                                         ("serviez", "Ce brave enfant ne pouvait supposer que vous vous serviez de lui pour dépouiller les autres !"),
                                                                         ("touchions", "Tel est l' homme qui, en une heure de conversation et par ce que j' ai vu autour de lui, m'a convaincu que nous touchions à une crise décisive."),
                                                                         ("abandonnions", "Elle fourrait dans ses poches profondes nos flottes de guerre et de commerce, nos comptoirs et nos colonies, que nous abandonnions à leur sort avec gaieté."),
                                                                         ("établissiez", "Elle verrait avec plaisir que vous établissiez à Krems un atelier d' habillement pour reformer l' habillement de vos troupes."),
                                                                         ("teniez", "Je ne voyais pas que vous teniez votre chapeau à la main."),
                                                                         ("venions", "Ces sauvages avaient été nos alliés pendant presque toute la guerre que nous venions de soutenir ; et leurs chefs me reçurent avec les plus grandes acclamations de joie."),
                                                                         ("voyions", "Nous nous disposâmes à débarquer dans une petite anse où la mer était si limpide que nous en voyions le fond à trois brasses de profondeur."),
                                                                         ("vouliez", "Vous m' avez longuement parlé du château des Frênes, auprès d' Auxerre, où vous êtes allé, lors de votre première sortie, chercher des passagers que vous vouliez initier aux charmes de la navigation aérienne."),
                                                                         ("entendiez", "Il me semble pourtant que vous ne vous entendiez pas mal, ces jours derniers."),
                                                                         ("attendions", "Cette nouvelle, que nous attendions tous les jours, fut cependant pour nous comme le coup de la mort."),
                                                                         ("portiez", "Un jour, à la promenade, vous vous aperçûtes soudain que vous portiez à la main une cage en osier."),
                                                                         ("poursuivions", "Dieu, qui voit le fond des âmes, sait que nous poursuivions un noble but."),
                                                                         ("veniez", "Avec quelle reconnaissance elle me parla aussi de la donation que vous veniez de faire à notre hôpital !"),
                                                                         ("connaissiez", "Ne vous est-il pas souvent arrivé de rencontrer par hasard, dans le monde, un homme que vous ne connaissiez pas, et que vous regardiez pourtant avec une curieuse attention, tant sa physionomie vous frappait ?"),
                                                                         ("regardiez", "Ne vous est-il pas souvent arrivé de rencontrer par hasard, dans le monde, un homme que vous ne connaissiez pas, et que vous regardiez pourtant avec une curieuse attention, tant sa physionomie vous frappait ?"),
                                                                         ("osions", "Jacques n' osait dire qu' il n' avait pu la retrouver, et nous n' osions le questionner sur ses recherches."),
                                                                         ("préparions", "On lui a raconté, M. Thiers et autres, que nous préparions une Vendée."),
                                                                         ("admirions", "Ces robes à ramages, ces bonnets de dentelle, expliquent les chansons d' amour merveilleusement braves et pimpantes que nous admirions tout à l' heure."),
                                                                         ("ignorions",  "Nous savions vaguement que la guerre avait recommencé en Europe ; mais nous en ignorions l' issue."),
                                                                         ("tenions", "Moi et madame Canada, plus chauds et bouillants que de vrais père et mère de qui nous tenions la place, nous n' étions pas encore contents."),
                                                                         ("disiez", "Qu' est-ce que vous disiez au colonel ?"),
                                                                         ("dévalions", "Au-dessus de nos têtes, le ciel était devenu, en un clin d' œil, plus ténébreux que les parois noires du couloir où nous dévalions à perdre haleine."),
                                                                         ("consultiez", "Canadiens, demandez à ceux que vous consultiez autrefois avec attention et respect ; demandez aux chefs de votre église qui ont occasion de me connaître."),
                                                                         ("appeliez", "Ce que vous appeliez le beau n' était qu' une déviation de cet idéal dont j' avais la pure notion ; vos couleurs étaient criardes, vos lignes irrégulières, vos monuments grotesques."),
                                                                         ("venions", "Elle roula principalement sur la campagne que nous venions de faire en Autriche."),
                                                                         ("regardions",  "Les injures des petits journaux classiques contre le jeune maître, que nous regardions dès lors et avec raison comme le plus grand poète de France, nous mettaient en des colères féroces."),
                                                                         ("dépassiez", "Vous ne vous connaissiez plus, vous vous emballiez, il fallait à toute force que vous la dépassiez."),
                                                                         ("prenions", "Tout ce qu' on nous servait à table, quand nous prenions nos repas, était plein de sable que le vent y portait."),
                                                                         ("ignorions",  "Maintenant que cette convention a eu lieu d' un commun accord, selon notre traité d' alliance avec la Porte, pendant que nous ignorions cette restriction, je ne conçois pas la possibilité de son infraction."),
                                                                         ("considérions",  "La présence de cette œuvre, que nous considérions comme la meilleure du maître, nous étonnait beaucoup."),
                                                                         ("paraissiez",  "Vous êtes venu me voir aussi un jour que j' étais malade et vous paraissiez très bon."),
                                                                         ("désirions",  "Heureusement, ce que nous désirions voir pouvait se voir au clair de la lune."),
                                                                         ("possédions", "Pourtant, les spéos les plus anciens que nous possédions ne remontent qu' aux premiers règnes de la XVIIIe dynastie."),
                                                                         ("usiez", "Chacun admet que vous en usiez vis-à-vis de ma tante avec la liberté d' un commensal."),
                                                                         ("cueillions",  "La vérité est que nous cueillions des mûres, chacun de notre côté, et que nous nous rencontrâmes par hasard, ce qui arrive souvent."),
                                                                         ("montriez", "Où est l' ardeur que vous montriez tout à l' heure ?"),
                                                                         ("traversions", "Malgré cela, j' ai remarqué que nous traversions un pays presque aussi joli que celui d' hier."),
                                                                         ("tombiez",  "Elle dort bien mal ; elle rêvait tout à l' heure que vous tombiez d' une falaise dans la mer."),
                                                                         ("précédiez", "Non, j' ai trouvé votre bracelet à Courville ; cela m'a indiqué que vous me précédiez sur la route."),
                                                                         ("traitiez", "Vous ne direz pas que vous me traitiez comme votre hôte ou même comme votre obligé."),
                                                                         ("regardiez", "Vous m' avez quittée pendant dix minutes, mais je sentais que vous me regardiez de loin."),
                                                                         ("entendions",  "Je ne dis pas un ami tel que nous l' entendions autrefois."),
                                                                         ("attendiez", "Jaggers a dit que vous l' attendiez dans son cabinet."),
                                                                         ("voulions", "Presque toujours c' était dans un coin du jardin, dans un coin retiré que nous voulions sauvage."),
                                                                         ("voyiez",  "Le jour qui venait de derrière vous était disposé de telle façon que vous la voyiez blanche et qu' elle vous voyait noir."),
                                                                         ("rencontrions",  "Cependant la reconnaissance de Schaunard, toutes les fois que nous nous rencontrions, continuait de s' exhaler en hymnes enthousiastes."),
                                                                         ("courions", "Pourtant, il n'y a encore qu' un an, nous courions, nous luttions ensemble."),
                                                                         ("luttions", "Pourtant, il n'y a encore qu' un an, nous courions, nous luttions ensemble."),
                                                                         ("admiriez", "Ainsi Idoménée est beaucoup plus puissant qu' il ne l' était quand vous admiriez sa magnificence."),
                                                                         ("connaissions", "Les crénelages les plus anciens que nous connaissions en France, construits après les premières croisades, sont ceux qui couronnent les tours et courtines du château de Carcassonne ( fin du XIe siècle ou commencement du XIIe)."),
                                                                         ("rencontrions", "Il était convenu que si nous rencontrions une patrouille, les porteurs diraient qu' ils allaient avec une malade à l' hôpital."),
                                                                         ("manquions", "Nous n' avions qu' une très petite quantité de fer coulé à notre disposition ; nous manquions surtout de bombes et d' obus."),
                                                                         ("chérissiez", "Vous aviez deux soeurs que vous chérissiez tendrement, et le ciel les a ravies à votre affection en les ensevelissant dans le même cloître."),
                                                                         ("occupions", "Nous y restâmes du 27 décembre au 11 janvier 1807, logés dans la maison du bailli, que suivant notre usage nous occupions tout entière, moins deux pièces où il était relégué avec sa famille."),
                                                                         ("viviez", "Je dis donc à votre mère que vous viviez, dans quel pays vous étiez."),
                                                                         ("causiez", "Vous n' avez pas pu ne pas mesurer la douleur que vous me causiez et je pense que vous l' avez voulue en proportion de ma faute !"),
                                                                         ("admirions", "Rossini avait promis, un soir qu' il était sensible, de traduire par un beau duetto ce groupe sublime de Vénus et Adonis que nous admirions à la lueur d' une torche."),
                                                                         ("venions", "Dans la nouvelle direction que nous venions de prendre, nous laissions le Kremlin sur notre gauche."),
                                                                         ("laissions", "Dans la nouvelle direction que nous venions de prendre, nous laissions le Kremlin sur notre gauche."),
                                                                         ("gagnions", "Mes oreilles tintaient, la respiration me manquait, tout tourbillonnait devant moi : j' étais comme un homme ivre, et cependant il me semblait que nous gagnions de l' avance, car les pas des chevaux s' affaiblissaient derrière nous."),
                                                                         ("venions", "Le cheval bai dont Gordon m ’ avait fait présent, était épuisé par le long voyage que nous venions de faire."),
                                                                         ("exposiez", "Vous ne saviez donc pas que vous exposiez votre vie ?"),
                                                                         ("attendions", "Ce n' est pas le dieu ouvrier qui nous était promis et que nous attendions, c' est le gras Jupiter de l' ancien et risible Olympe."),
                                                                         ("vouliez", "Un jour que vous vouliez vous battre avec Combelaine, M. Verdale et Me Roberjot se sont trouvés en présence."),
                                                                         ("jouiez", "Au jeu que vous jouiez, monsieur, on risque sa vie... et vous avez perdu, n' est-ce pas ?")]
                                                    for verbe_conjugué, sentence_comparée in verbes_indicatif:
                                                        if verbe[1] == verbe_conjugué and sentence == sentence_comparée:
                                                            mood = [INDICATIVE]
                                                            break

                                                    verbes_subjonctif = [("connaissiez", "Il faut seulement, pour comprendre la situation, que vous connaissiez les choses dans leurs grandes lignes."),
                                                                         ("cachiez", "Cependant, il est nécessaire que vous ne cachiez aucune des circonstances qui ont marqué ces relations ?"),
                                                                         ("envoyions", "Cela vous étonne-t-il que nous envoyions notre Godelive à la fabrique de dentelles ?"),
                                                                         ("tiriez", "Alors, il faut que vous me tiriez d' affaire, n' importe comment."),
                                                                         ("gardiez", "Il faut que vous gardiez votre beauté."),
                                                                         ("aspiriez", "Je veux bien croire que vous aspiriez à vous élever en servant votre pays..."),
                                                                         ("aimiez", "Ce chien a nom Buffalo ; ce n' est point bien joli, mais il faut que vous l' aimiez tout de même."),
                                                                         ("voyions", "Mon ami, il n' est pas vraisemblable que nous nous voyions jamais, mais il est sûr que nous nous aimerons toujours."),
                                                                         ("occupiez", "En ce cas, je m' étonne que vous ne l' occupiez pas, car votre santé paraît délicate."),
                                                                         ("sentiez", "Je sens l' amertume de mon imprévoyance, mais pourquoi faut-il que vous la sentiez aussi ?"),
                                                                         ("donniez", "Il faut que vous me donniez la force de les vaincre."),
                                                                         ("ignoriez", "Tous mes camarades, même ceux du chantier, le savent, et je m' étonne que vous l' ignoriez."),
                                                                         ("attachiez", "Elle désire, messieurs, que vous vous attachiez à distinguer la question politique de la forme du gouvernement de la France, de la question actuelle de la conclusion d' un armistice."),
                                                                         ("connaissiez", "Se peut-il que vous sachiez quelque chose concernant le malheureux mariage de ce pauvre ami et que vous connaissiez cette femme ?"),
                                                                         ("égarions", "Elles filent le long des haies, elles s' arrêtent à chaque détour, pour veiller à ce que nous ne nous égarions pas."),
                                                                         ("trouvions", "Il est impossible que nous ne trouvions pas une issue !"),
                                                                         ("demeuriez", "Il est préférable que vous demeuriez avec monseigneur."),
                                                                         ("regrettiez", "Je souffre de vous voir ainsi, et j' ai peur que vous ne regrettiez Paris dans les auberges des bords du Rhin !"),
                                                                         ("rentrions", "Mon avis est donc que nous rentrions d' abord chez nous."),
                                                                         ("réprimiez", "Je demande, président, que vous réprimiez les clameurs de ce côté droit, car il est ressuscité parmi nous."),
                                                                         ("éprouviez", "Quelle que soit la contrariété que vous éprouviez, la nature ne perd pas ses droits, et, malgré l' affliction de votre cœur, votre estomac ne serait peut-être pas fâché d' avaler un morceau."),
                                                                         ("aimiez", "Vous êtes bien jolie, et c' est grand dommage que vous aimiez les capucins."),
                                                                         ("réglions", "Il faut que nous réglions bien ce mouvement-là."),
                                                                         ("reprochiez", "Aussi je ne m' étonne pas que vous reprochiez à Camille son Comité de clémence."),
                                                                         ("mettiez", "Il est temps que vous mettiez ordre à tout cela."),
                                                                         ("acceptiez", "Je désire que vous acceptiez les fonctions de secrétaire général."),
                                                                         ("viviez", "Cela me fait vraiment de la peine de penser que vous, ma chère filleule, vous viviez dorénavant dans un monde inférieur..."),
                                                                         ("perdiez", "Je le veux ainsi, je ne souffrirai jamais que vous vous perdiez pour l' amour de moi."),
                                                                         ("connaissions", "En effet, la clef d' arcs ogives la plus ancienne que nous connaissions se voit dans la tribune du porche de Vézelay."),
                                                                         ("obéissiez", "Je vous avais rencontré avec cette demoiselle, je pensais que vous obéissiez à Monseigneur."),
                                                                         ("acceptiez", "En revanche, il faut absolument que vous acceptiez le revenu."),
                                                                         ("parlions", "Démosthène et Cicéron ne parlaient que pour eux, de leurs affaires ou de leur nation : nous parlions pour l' humanité tout entière ; notre affaire était l' affaire de la raison générale, la cause de l' homme et de l' esprit humain."),
                                                                         ("considériez", "Si vous sortez de la paroisse et que vous considériez le canton, vous reverrez le même spectacle."),
                                                                         ("aimiez", "Je ne comprends pas que vous aimiez cette couleur ?"),
                                                                         ("parlions", "Il faut que nous parlions ici des amours de la rose."),
                                                                         ("montions", "Il ne faut pas que nous montions cette fois dans le même bateau."),
                                                                         ("supportiez", "Je m' étonne que vous supportiez une pareille existence."),
                                                                         ("compreniez", "Cette position, je veux vous la dire, car il faut que vous compreniez ma volonté."),
                                                                         ("juriez", "Il faut que vous me juriez par le bon Dieu d' avoir soin de lui."),
                                                                         ("rattrapions", "Il faut, à toute force, que nous la rattrapions, dit le lieutenant d' une voix fébrile."),
                                                                         ("venions", "La science n'a pas encore dit son dernier mot sur les tribus composites qui se disputent les piètres ressources de la portion de l' Asie centrale que nous venions de parcourir."),
                                                                         ("aimiez", "Vous avez dit que vous n' aimiez pas M. Morand, mais vous ne m' avez pas dit que vous en aimiez un autre."),
                                                                         ("aimiez", "Vous avez dit que vous n' aimiez pas M. Morand, mais vous ne m' avez pas dit que vous en aimiez un autre."),
                                                                         ("trouviez", "Il faut, de toute nécessité, que vous trouviez le moyen de nous tirer de cette crise qui menace, au plus haut degré, la paix de l' Europe."),
                                                                         ("craigniez", "Pourquoi faut-il que vous craigniez sa vue ?"),
                                                                         ("éprouviez", "Depuis que j' ai goûté le charme de l' amitié, je désire que vous l' éprouviez à votre tour."),
                                                                         ("rameniez", "Comment se fait-il que vous la rameniez dans un tel état ?"),
                                                                         ("preniez", "Il faut que vous preniez votre parti de ma forme brusque, quelquefois dure."),
                                                                         ("retrouvions", "Il faut, il faut que nous retrouvions cette jeune fille !"),
                                                                         ("retrouvions", "Je m' imaginai que nous nous retrouvions sous un soleil splendide, dans le parterre de Chavanges, sur ce banc où je l' avais tenue dans mes bras."),
                                                                         ("connaissiez", "Il faut que vous connaissiez exactement l' origine et les circonstances de la querelle."),
                                                                         ("veilliez", "Mère, il faut que vous veilliez sur elle."),
                                                                         ("endormiez", "Le temps ne m' attend pas, et j' ai peur que vous ne vous endormiez."),
                                                                         ("laissions", "Il n' est pas même indispensable que nous laissions sommeiller improductivement ces titres dans l' intervalle."),
                                                                         ("envoyiez", "Il est inutile que vous envoyiez des adjoints à Paris."),
                                                                         ("jetiez", "Dès lors comment se fait-il que vous jetiez le poids de votre épée et de votre expérience dans le plateau le plus faible ?"),
                                                                         ("dormions", "Ainsi, mes frères, si Dieu nous aime, croyez qu' il ne permet pas que nous dormions à notre aise dans ce lieu d' exil."),
                                                                         ("preniez", "Un peuple dont le pain est taxé est un peuple esclave, de quelque manière que vous le preniez."),
                                                                         ("disiez", "Quoi que vous en disiez, le climat de Biarritz ne vous est pas indispensable."),
                                                                         ("enfermions", "Il faut que nous enfermions l' eau, afin d' augmenter le marécage des prairies, la force du torrent et les vibrations du pont de bambou."),
                                                                         ("appreniez", "Demain vous boirez un coup à sec, il faut que vous appreniez à tout faire."),
                                                                         ("coupiez", "Vous savez que je ne veux pas que vous les coupiez ; il est juste que j' en aie soin."),
                                                                         ("venions", "Mon avis est que nous venions demain bien accompagnés."),
                                                                         ("marchiez", "Toutefois, général, l' intention de l' Empereur est que vous marchiez sans délai sur Gratz et que vous culbutiez les corps de Giulay et de Chasteler, qui y sont."),
                                                                         ("culbutiez", "Toutefois, général, l' intention de l' Empereur est que vous marchiez sans délai sur Gratz et que vous culbutiez les corps de Giulay et de Chasteler, qui y sont."),
                                                                         ("aimiez", "Je vous ai dit un jour que vous aimiez cet homme."),
                                                                         ("veniez", "Vous nous manquez beaucoup ici, il faudra bien un jour que vous veniez reprendre votre place."),
                                                                         ("suiviez", "Il faut que vous me suiviez chez un magistrat."),
                                                                         ("rayiez", "Je ne crois donc pas mériter ce reproche : il faut que vous rayiez cet article sur le mémoire de mes défauts..."),
                                                                         ("voyions", "Il faut que nous voyions la marquise à l' instant même."),
                                                                         ("donnions", "Vous savez que le gouvernement se propose de faire de nouveaux établissements des ports francs ; il est donc essentiel que nous donnions à toutes les marines des motifs de les préférer, pour leurs relâches et leur ravitaillement, aux autres points de l' Océanie."),
                                                                         ("prenions", "Il faut que nous prenions un chemin qui ne garde pas de traces."),
                                                                         ("périssions", "Je ne crois pas aux miracles, mais je crois à Dieu, qui souffre rarement que nous périssions par nos vertus."),
                                                                         ("mettions", "Il faut que nous mettions aussitôt la police en mouvement !"),
                                                                         ("rendiez", "Mes chères petites, il faut que vous rendiez la fête complète, il faut que vous me tutoyiez ce soir."),
                                                                         ("tutoyiez", "Mes chères petites, il faut que vous rendiez la fête complète, il faut que vous me tutoyiez ce soir."),
                                                                         ("tourniez", "De quelque côté que vous vous tourniez, vous avez tort."),
                                                                         ("connaissions", "Même les formes d' énergie les plus compliquées et les plus parfaites que nous connaissions, la vie psychique des animaux supérieurs, la pensée et la raison humaines, reposent sur des processus matériels, sur des changements dans le neuroplasma des cellules ganglionnaires ; on ne peut pas les concevoir sans cela."),
                                                                         ("mettions", "Nos enfans recevront les soins du Doge et de mes oncles : il faut que nous mettions à la voile avant la nuit."),
                                                                         ("trompiez", "Eh bien, j' ai peur que vous ne vous trompiez."),
                                                                         ("lisiez", "Rassurez-vous, ma lettre sera courte, je désire que vous la lisiez jusqu' au bout."),
                                                                         ("compreniez", "Il faut que vous compreniez mon erreur."),
                                                                         ("voyiez", "Je désire que vous voyiez la chose comme moi."),
                                                                         ("haïssiez", "Il nous importe surtout que vous ne nous haïssiez pas ; il y va de notre gloire et de notre honneur."),
                                                                         ("parliez", "Il faudra que vous me parliez de lui, tante Mélanie."),
                                                                         ("trouviez", "Je n' imagine pas que vous trouviez des charmes bien puissants dans les fonctions de demoiselle d' honneur de la princesse royale ni que vous ayez l' intention d'y passer votre vie."),
                                                                         ("obéissions", "Il faut que nous obéissions aux instincts qu' il nous a donnés, comme le canard obéit à l' instinct impérieux qui l' entraîne vers la rivière."),
                                                                         ("conserviez", "Je demande seulement que vous ne conserviez aucune force dans nos villes de garnison."),
                                                                         ("possédions", "Les nouveaux bassins sont les plus longs et les plus vastes que nous possédions."),
                                                                         ("trouviez", "Le beau temps est venu, et les cerises s' en vont : j' ai peur, Mademoiselle, que si vous ne faites bientôt ici une promenade, vous n'y en trouviez plus."),
                                                                         ("possédiez", "Enfin, il faut que vous possédiez une bonne méthode, c' est à dire une méthode rationnelle de direction."),
                                                                         ("ignoriez", "Mon intention est que vous ignoriez cette mauvaise conduite."),
                                                                         ("nommiez", "Vous vous imaginez que M. le curé souffrira que vous les nommiez d' un nom dangereux ?"),
                                                                         ("appeliez", "De quelque nom que vous l ’ appeliez, il est le complément de notre organisation, et comme la clef de voûte de notre monde intellectuel."),
                                                                         ("battiez", "Monsieur, je ne contrains personne ; mais il faut que vous vous battiez, ou que vous épousiez ma sœur."),
                                                                         ("épousiez", "Monsieur, je ne contrains personne ; mais il faut que vous vous battiez, ou que vous épousiez ma sœur."),
                                                                         ("veniez", "Ne vaut-il pas mieux que vous veniez avec moi chez le roi votre père ?"),
                                                                         ("prépariez", "Je désire donc, mon cher ami, que vous prépariez les voies auprès d' un libraire."),
                                                                         ("répondiez", "Il faut absolument que vous répondiez à mon amour."),
                                                                         ("cherchions", "Il est inutile que nous cherchions à nous déraidir les membres."),
                                                                         ("chantiez", "Mme Gerbier m'a dit que vous chantiez."),
                                                                         ("gagniez", "On sait le petit commerce que vous pratiquez, et que vous n' avez point d' applaudissements que vous ne gagniez à force de sonnets et de révérences."),
                                                                         ("disiez", "Cette description, quoi que vous disiez, est au second plan."),
                                                                         ("connaissiez", "Il est impossible que vous ne connaissiez pas ce tableau ; c' est l' original de la belle gravure de Morghen."),
                                                                         ("veniez", "En deux mots, mon cher Humbert, il faut que vous veniez plus tôt que nous n' étions convenus."),
                                                                         ("quittions", "Elles vinrent m' embrasser tendrement l' une après l' autre, en me disant : Adieu, cher prince, adieu ; il faut que nous vous quittions."),
                                                                         ("dénichions", "Il faut absolument que nous dénichions la bru de la veuve Chupin, et j' ai tout lieu d' espérer que nous trouverons son adresse chez le commissaire de l' arrondissement."),
                                                                         ("aimiez", "Il est impossible que vous ne m' aimiez pas un jour."),
                                                                         ("prononciez", "Il n' est pas possible que vous prononciez la condamnation d' une soeur comme celle que Dieu vous a donnée."),
                                                                         ("preniez", "Il pourra être utile que vous preniez l' initiative d' une introduction de cultivateurs de la Chine ou de l' archipel d' Asie."),
                                                                         ("mêliez", "Il n' est nullement nécessaire que vous vous mêliez de cette affaire."),
                                                                         ("essuyiez", "Ma fille, il faut que vous essuyiez tout ceci."),
                                                                         ("devenions", "La Nature, elle, oubliant que l' imitation peut devenir la forme la plus sincère de l' insulte, se met à répéter cet effet jusqu'à ce que nous en devenions absolument las."),
                                                                         ("croyiez", "Jérôme, que vous croyiez un complice, Jérôme est un ami de Jean, comme moi."),
                                                                         ("preniez", "Tout concourt donc, citoyen préfet, à ce que vous preniez des mesures pour l' aliénation de la maison dont il s' agit."),
                                                                         ("disiez", "Quoi que vous disiez, Paganel, un nid ne peut suffire à un homme, et vous l' apprendrez bientôt à vos dépens."),
                                                                         ("hâtiez", "Tout cela vaut bien la peine que vous hâtiez votre voyage."),
                                                                         ("trouviez", "Sans autre forme de procès, jetez-vous sur eux ; que vous les trouviez aux champs ou dans les bois, n' importe, ils sont à vous."),
                                                                         ("oubliiez", "Quelle que soit mon amitié pour vous, je ne souffrirai jamais que vous oubliiez un moment le respect que vous devez à ma femme, monsieur."),
                                                                         ("preniez", "Il se fait temps d' ailleurs que vous preniez soin de votre santé."),
                                                                         ("épousiez", "Qui que vous épousiez, il voudra même chose."),
                                                                         ("payiez", "Il faut, Madame, que vous les payiez ou que ces Messieurs y demeurent."),
                                                                         ("disiez", "Quoi que vous disiez, son organisation même comprime le développement de votre puissance."),
                                                                         ("considériez", "Il est tout simple que vous ne considériez pas ces choses là d' un point de vue professionnel."),
                                                                         ("tentiez", "Le gouvernement français n' entend pas que vous tentiez aucune entreprise au delà de la sphère que je vous ai marquée."),
                                                                         ("essayions", "Il faudra que nous essayions ça à la maison, quand nous n' aurons pas les Ledragon."),
                                                                         ("supportiez", "Il est bon que vous en supportiez, dès le début, toutes les conséquences."),
                                                                         ("voyiez", "Je serai bien aise que vous les voyiez représenter dans notre langue."),
                                                                         ("conserviez", "Ce qui m' étonne, c' est que vous conserviez un jury criminel."),
                                                                         ("prenions", "Au milieu des grands événements de l' Europe, je n' ai qu' une pensée ; il faudra pourtant que nous prenions une résolution à Paris."),
                                                                         ("voyions", "Quoi que nous voyions à la surface, il existe au fond de la société un sentiment de justice et de bienveillance universelle, une aspiration vers un ordre social qui satisfasse d' une manière plus complète et surtout plus égale les besoins physiques, intellectuels et moraux de tous les hommes."),
                                                                         ("armions", "De notre côté, il faudra que nous armions des gens pour nous défendre."),
                                                                         ("viviez", "Vous ne savez pas ce que j' ai dit : j' ai dit que vous viviez avec un vieux ; mais je n' en croyais rien, mademoiselle Bathilde, parole d' honneur !"),
                                                                         ("priiez", "Elle vous aime bien ; elle a besoin que vous priiez pour elle."),
                                                                         ("assuriez", "Il est juste que vous assuriez de mes attentions Vénus-Newton."),
                                                                         ("mangiez", "Votre saint oncle entend que vous les mangiez pendant votre convalescence."),
                                                                         ("suivions", "On comprendra que nous ne suivions pas le voyageur dans ses chasses."),
                                                                         ("avertissiez", "Pour les autres jours, il faudra que vous nous avertissiez ; car nous avons assez, l' habitude de passer toute la journée dehors et assez loin."),
                                                                         ("veniez", "Il faudra que vous veniez, quelque jour, à son thé."),
                                                                         ("veniez", "Je veux que vous veniez avec moi ce soir."),
                                                                         ("inspiriez", "Je vous ai dit aussi l' effroi que vous m' inspiriez."),
                                                                         ("traitions", "On ne s' étonnera pas que, pour des raisons diverses, nous ne traitions pas ici à fond, au point de vue théorique, les questions soulevées par le socialisme."),
                                                                         ("exigiez", "Il serait bon, pour l' exemple, que vous exigiez de ces messieurs quelques millions."),
                                                                         ("appreniez", "Il faut que vous appreniez aussi sur le violon quelques chansonnettes que vous m' accompagnerez."),
                                                                         ("consentiez", "Il m'a paru que vous aviez quelque amitié pour elle, et je ne fais aucun doute que vous ne consentiez à la garder chez vous jusqu'à mon retour."),
                                                                         ("causions", "Il faut que nous causions seul à seul, moi et le citoyen Arnould."),
                                                                         ("retourniez", "Je demeure en ma première opinion, qu' il faut que vous retourniez vers Béarn."),
                                                                         ("envoyiez", "Il est indispensable que vous y envoyiez un ingénieur constructeur pour la faire terminer."),
                                                                         ("venions", "Je regardai ce côté que nous venions de franchir."),
                                                                         ("permettiez", "Colonel, il faudra que vous me permettiez de faire son portrait."),
                                                                         ("marchions", "Si l' ennemi suit Napoléon, et que nous marchions en avant, elles nous rejoindront plus tard."),
                                                                         ("partagiez", "Il faut que vous partagiez ma joie."),
                                                                         ("aimiez", "Je vous aime assez tous les deux pour consentir à ce que vous vous aimiez."),
                                                                         ("opposiez", "Je puis bien comprendre que vous vous opposiez à ce que l' Art soit traité comme un miroir."),
                                                                         ("réussissions", "On fait bien des choses avec de l' or ; mais l' esprit d' opposition est fort en France, et je crains que nous ne réussissions pas."),
                                                                         ("préfériez", "Je n' ai point avec vous le droit de conseil, il se peut que vous préfériez la démence à la lucidité, l' ombre à la lumière, l' éternelle nuit des dogmes de l' enfer et du célibat à l' éternelle vie du ciel et de l' amour légitime."),
                                                                         ("aidiez", "Il faut donc que vous nous aidiez, en nous disant la vérité."),
                                                                         ("donniez", "Il faut que vous me donniez la faculté d' avancer de deux ou trois grades ceux qui serviront bien."),
                                                                         ("pénétriez", "Il faut que vous pénétriez cette nation."),
                                                                         ("détestiez", "Je croyais que vous détestiez ça, les bougies en plein jour."),
                                                                         ("vouliez", "Peu m' importe la fée ; vous m' avez dit que vous vouliez emmener mademoiselle dans la lune."),
                                                                         ("ouvriez", "Il n' est pas nécessaire que vous m' ouvriez votre coeur, je le connais."),
                                                                         ("parliez", "Assurément l' idée est belle, mais elle est pour le moins aussi incompatible avec la réalité que le « chigavélisme » dont vous parliez tout à l' heure en termes si méprisants."),
                                                                         ("rendiez", "Messeigneurs de France, il faut que vous rendiez vos armes à mes officiers."),
                                                                         ("placiez", "Mon intention est que vous placiez vos troupes sur deux rangs au lieu de trois."),
                                                                         ("examinions", "Il faudra que M Cyrus, Ayrton et moi, nous examinions l' affaire !"),
                                                                         ("formions", "Il est donc parfaitement naturel, parfaitement simple que, mettant à part toute animosité, mettant à part toute vieille querelle, nous formions, M. Barrot et moi, des voeux différents."),
                                                                         ("étudiiez", "Il sera bon que vous étudiiez à fond cette question, car certainement vous aurez bientôt à utiliser ces connaissances."),
                                                                         ("autorisiez", "Nous demandons que vous nous autorisiez à faire faire des visites domiciliaires."),
                                                                         ("viviez", "Mon but à moi, George, c' est que vous viviez avec la meilleure société de l' Angleterre."),
                                                                         ("appreniez", "Il est temps que vous appreniez."),
                                                                         ("connaissions", "Maintenant, messieurs, il importe que nous connaissions un peu mieux les étrangers."),
                                                                         ("repreniez", "Tu vois que de pierres elle a entassées, afin que vous ne la repreniez jamais."),
                                                                         ("aimiez", "Si je croyais que vous m' aimiez... même, simplement, si j' espérais qu' un jour, vous finirez par m' aimer, je vous demanderais d' être ma femme."),
                                                                         ("commettiez", "Il croit que si vous commettiez un nouveau délit, il n' aurait pas, cette fois, le pouvoir d' enrayer l' action judiciaire."),
                                                                         ("rappeliez", "Il faut que vous rappeliez les Bourbons."),
                                                                         ("longions", "Je regardais en silence la haute muraille très accore que nous longions en ce moment, inébranlable base du massif sableux de la côte."),
                                                                         ("embrassiez", "Il faut aussi que vous embrassiez madame."),
                                                                         ("perdiez", "Le monde ne vaut pas la peine que vous perdiez votre santé pour lui."),
                                                                         ("usiez", "Votre nom pour ce choix est plus fort que le mien, Et je n' ose douter que vous n' en usiez bien."),
                                                                         ("estimions", "Crouzillat est une victime du devoir, c' est un homme que nous estimions beaucoup."),
                                                                         ("disiez", "Non, quoi que vous en disiez, c' est un retour impossible."),
                                                                         ("changiez", "Il faut absolument que vous changiez ce papier."),
                                                                         ("mourions", "Il faut mieux que nous mourions en combattant !"),
                                                                         ("voyiez", "Il n'y a aucun inconvénient, aucun inconvénient, maintenant, à ce que vous le voyiez."),
                                                                         ("insultions", "Grande et héroïque nation, ne craignez pas que nous insultions à vos misères !"),
                                                                         ("réprimiez", "Quand vous ne voulez pas réprimer par la force, il faut que vous réprimiez par votre autorité sur les esprits."),
                                                                         ("espériez", "Après ces déclarations, je ne pense pas que vous espériez d' empêcher le cours de mes gazettes."),
                                                                         ("rencontrions", "Il est donc impossible qu' au jour nous ne rencontrions pas quelque barque de pêcheur qui nous recueillera."),
                                                                         ("laissiez", "Il faut, monsieur, que vous me laissiez faire encore un massage, sans quoi l' enflure reviendrait."),
                                                                         ("jouiez", "Il faut donc toujours que vous jouiez un jeu ou un autre ?"),
                                                                         ("connaissiez", "Il n' est pas que vous ne connaissiez la Fillon, capitaine ?"),
                                                                         ("abandonniez", "Il semble que vous nous abandonniez au pillage."),
                                                                         ("aidiez", "Ma belle, il faudra que vous m' aidiez à passer quelques-unes de mes malles en contrebande."),
                                                                         ("craigniez", "Il est exposé au midi, et j' ai entendu dire que vous craigniez la chaleur dans les appartements."),
                                                                         ("annoncions", "Il est bon encore que nous annoncions à tous nos ennemis que nous voulons être continuellement et complètement en mesure contre eux."),
                                                                         ("imitiez", "Votre père avait mangé deux fortunes avant de refaire celle qu' il vous a laissée, vous ne seriez point un Manerville si vous ne l' imitiez pas."),
                                                                         ("méditiez", "Il est impossible que vous méditiez un pareil crime, Sylvie !"),
                                                                         ("habitions", "Il me semble que si nous habitions la même maison, cela serait plus agréable pour tous deux."),
                                                                         ("sortions", "Oui, il est temps que nous sortions."),
                                                                         ("donnions", "Nos lecteurs n' attendent pas sans doute que nous leur en donnions l' explication."),
                                                                         ("sentiez", "Je comprends qu' avec de pareilles idées vous vous sentiez libre."),
                                                                         ("aimiez", "Ce dessein est de vous plaire ; je vous aime, je désire que vous m' aimiez."),
                                                                         ("laissiez", "Seulement, il faudra que vous vous laissiez mesurer, mon ami."),
                                                                         ("vivions", "Quand la volonté de Dieu est que nous vivions, nos parents ne peuvent nous rappeler à eux dans l' autre vie."),
                                                                         ("attendiez", "Il faut que vous l' attendiez un petit instant ; il est chez le photographe avec son frère."),
                                                                         ("mettiez", "Il est temps que vous y mettiez ordre par quelque plainte énergique."),
                                                                         ("passiez", "Il est impossible que vous passiez ainsi la nuit."),
                                                                         ("cherchiez", "Il faut donc que vous cherchiez quelque autre chose : pas de trou possible."),
                                                                         ("sauvions", "Il faut que nous sauvions la cathédrale, mylord."),
                                                                         ("viviez", "Les compagnons ne souffriront pas que vous viviez ainsi tout seul ; vous tomberiez en mélancolie."),
                                                                         ("bouchiez", "Il faut que vous vous bouchiez les oreilles contre ces petits mots faux."),
                                                                         ("veniez", "Il faut donc que vous nous veniez en aide."),
                                                                         ("écrasions", "Cela décupla notre courage, car il était visible que nous les écrasions sous notre nombre."),
                                                                         ("regardions", "Vous le voyez, que nous regardions le sol ou les hommes qui l' habitent, un grand et fécond établissement territorial dans la régence d' Alger paraît impossible."),
                                                                         ("aimiez", "Vous ne m' aimez encore que par une galanterie qui est de costume chez les hommes ; mais si vous m' aimiez bien, vous tomberiez dans un désespoir affreux."),
                                                                         ("goûtiez", "Il faut que vous le goûtiez bon gré mal gré."),
                                                                         ("combattions", "Il faut que nous combattions pour qu' une pareille chose devienne impossible dans notre existence et dans l' existence de nos enfants et petits-enfants."),
                                                                         ("lanciez", "Il faut que vous lanciez cet hiver un roman chez Charpentier."),
                                                                         ("gagnions", "Il faut pourtant que nous gagnions notre vie."),
                                                                         ("donnions", "Pour cela il faut que nous donnions une cause absolument inattaquable à notre prêt."),
                                                                         ("grandissiez", "Le public lui-même s' étonne que vous grandissiez en maturité dans la science de la vie."),
                                                                         ("gardiez", "Il faut même que vous gardiez le plus profond secret à cet égard."),
                                                                         ("condamniez", "Il est impossible que vous condamniez des ministres responsables, des ministres qui répondent de tous leurs agents, à subir des agents sur lesquels ils n' auraient pas une action indépendante."),
                                                                         ("écriviez", "Quoi que vous écriviez, évitez la bassesse : Le style le moins noble a pourtant sa noblesse."),
                                                                         ("sauvions", "Il se peut que nous sauvions la vie, nous ne sauverons pas la raison..."),
                                                                         ("considérions", "Si nous voulons l' affranchissement de ceux qui ne sont pas libres, cela ne veut pas dire que nous considérions une certaine répartition des biens comme une chose essentielle en soi, une certaine hiérarchie des droits de jouissance comme une chose désirable, une certaine formule utilitaire comme décisive."),
                                                                         ("dévorions", "Il n'y a point de paix possible avec la Vendée ; l' épée est tirée, il faut que nous dévorions le chancre ou qu' il nous dévore."),
                                                                         ("connaissiez", "Comment se fait-il, monsieur, que vous connaissiez cet homme ?"),
                                                                         ("rentrions", "Impossible que nous rentrions à Londres ensemble, toi et moi : jamais nous ne pourrions expliquer l' absence de l' oncle !"),
                                                                         ("rangiez", "Il est temps que vous vous rangiez."),
                                                                         ("mettiez", "Je veux, mademoiselle, que vous mettiez immédiatement votre châle et votre chapeau et que vous vous prépariez à m' accompagner."),
                                                                         ("prépariez", "Je veux, mademoiselle, que vous mettiez immédiatement votre châle et votre chapeau et que vous vous prépariez à m' accompagner."),
                                                                         ("songiez", "Il faut que vous songiez à établir la paix et le bonheur sur la terre, et ce sera un bonheur pour vous."),
                                                                         ("retrouvions", "Quand tu auras cette lettre, huit jours bientôt seront passés et il s' en faudra d' une semaine que nous nous retrouvions."),
                                                                         ("haïssiez", "Il est notoire dans le pays que vous le haïssiez."),
                                                                         ("aimiez", "Je comprends à la rigueur que vous n' aimiez pas le gouvernement."),
                                                                         ("logiez", "Ainsi, ne m' a-t-on pas dit également que vous logiez chez ce Bourras ?"),
                                                                         ("accommodiez", "Nous continuons notre chapelle ; il fait chaud ; les soirées et les matinées sont très-belles dans ces bois et devant cette porte ; mon appartement est frais ; j' ai bien peur que vous ne vous accommodiez pas si bien de vos chaleurs de Provence."),
                                                                         ("craigniez", "Pourquoi faut-il que vous craigniez sa vue !"),
                                                                         ("connaissiez", "Il est fâcheux que vous ne connaissiez pas M. Lebègue, car c' est un digne homme et il aurait pu vous être utile."),
                                                                         ("entendiez", "Je viens pour autre chose, qu' il faut que vous entendiez."),
                                                                         ("demandiez", "Que sont les conseils d' un ami auprès de ceux que vous demandiez à Dieu même ?"),
                                                                         ("occupions", "Il faudra que nous nous occupions d' un parti convenable pour lui."),
                                                                         ("passiez", "Je ne suppose pas que, malgré sa passion pour le tabac, Camille vous préfère un cigare, et que, malgré votre admiration pour les femmes auteurs, vous passiez quatre heures à lire des romans femelles."),
                                                                         ("différiez", "Ce qui m' importe surtout, c' est que vous ne différiez en rien l' établissement du Code Napoléon."),
                                                                         ("adressiez", "Nous ne faisons pas ce genre d' affaires... et je m' étonne que vous ne vous adressiez pas aux personnes avec lesquelles vous traitez d' ordinaire."),
                                                                         ("vengions", "Si ces monstres ont tué Philippe, ne faut-il pas que nous vengions sa mort ?"),
                                                                         ("accueilliez", "Nous verrons souvent Lugarto ; je l' aime beaucoup ; je désire que vous l' accueilliez avec bienveillance."),
                                                                         ("forciez", "Je crains qu' outre le mariage à l' autel, vous ne me forciez à accepter toutes les cérémonies d' un mariage du monde."), ]
                                                    # pareil, traitement manuel sur les subjonctifs_|_indicatifsfor valeur_test in verbes_subjonctif:
                                                    for verbe_conjugué, sentence_comparée in verbes_subjonctif:
                                                        if verbe[1] == verbe_conjugué and sentence == sentence_comparée:
                                                                mood = [SUBJUNCTIVE]
                                                                break

                                                if value =="conj" or "sujet" in évènements[event_key][token].keys():
                                                    sujet = évènements[event_key][token]['sujet'][1]
                                                    if len(évènements[event_key][token]['sujet']) == 10:
                                                        sujet = évènements[event_key][token]['sujet']
                                                    elif len(évènements[event_key][token]['sujet']) == 2 and len(évènements[event_key][token]['sujet'][1]) == 10:
                                                        sujet = évènements[event_key][token]['sujet'][1]
                                                    else:
                                                        take = False
                                                        break
                                                    if sujet[2] == "cln":
                                                        if len(sujet[1]) < 3:
                                                            if sujet[1].lower().startswith("t"):
                                                                person = [2]
                                                                évènements[event_key][token]["constraints"][
                                                                    "conjugaison"]["nombre"] = "singulier"
                                                            elif sujet[1].lower().startswith("j"):
                                                                person = [1]
                                                                évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "singulier"
                                                        elif sujet[1].lower() == "nous":
                                                            person = [1]
                                                            évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "pluriel"
                                                            number = [PL]
                                                        elif sujet[1].lower() == "vous":
                                                            person = [2]
                                                            évènements[event_key][token]["constraints"]["conjugaison"]["nombre"] = "pluriel"
                                                            number = [PL]

                                                    assert mood != ""
                                                    assert aspect != ""
                                                    assert tense != ""
                                                    assert person != ""

                                                    for temps in tense:
                                                        for personne in person:
                                                            for mode in mood:
                                                                for forme in aspect:
                                                                    for nombre in number:
                                                                        résultat = conjugate(verbe[2], tense=temps, person=personne, mood=mode, aspect=forme, number=nombre)
                                                                        if résultat == verbe[1]:
                                                                            if not (mode == SUBJUNCTIVE and len(conjugaisons_possibles) > 0):
                                                                                # on fait confiance en cas de doute au tag m=subj :
                                                                                # sa présence implique que la liste ne contienne pas d'indicatif
                                                                                conjugaisons_possibles.append((temps, personne, mode, forme, nombre, 'auxilié' if is_tps_auxilié and value == 'conj' else ''))

                                            if len(conjugaisons_possibles) == 0:
                                                # dans 90% des cas le verbe est un verbe du premier groupe:
                                                # on laisse tomber l'impératif dans notre cas de figure
                                                if verbe[2].endswith("er"):
                                                    if verbe[4] in ('VPP', 'VPA'):
                                                        mood = [INDICATIVE]
                                                        aspect = [PROGRESSIVE]
                                                        if  verbe[4] == "VPA":
                                                            tense = [PRESENT]
                                                        else:
                                                            tense = [PAST]
                                                        number = [None]
                                                        person = [None]  # PERSON = [None, 1, 2, 3]
                                                        conjugaisons_possibles.append((tense[0], person[0], mood[0], aspect[0], number[0], 'auxilié' if is_tps_auxilié and value == 'conj' else ''))
                                                    else:
                                                        mode = ""
                                                        forme = ""
                                                        temps = ""
                                                        nombre = ""
                                                        personne = person[0]
                                                        if verbe[2] == "aller":
                                                            nombre = PL
                                                            if verbe[1].lower() in ("vais", "vas", "va", "allons", "allez", "vont"):
                                                                mode = INDICATIVE
                                                                temps = PRESENT
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("vais", "vas", "va"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("allais", "allais", "allait", "allions", "alliez", "allaient"):
                                                                mode = INDICATIVE
                                                                temps = PAST
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("allais", "allais", "allait"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("allai", "allas", "alla", "allâmes", "allâtes", "allèrent"):
                                                                mode = INDICATIVE
                                                                temps = PAST
                                                                forme = PERFECTIVE
                                                                if verbe[1].lower() in ("allai", "allas", "alla"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("irai", "iras", "ira", "irons", "irez", "iront"):
                                                                mode = INDICATIVE
                                                                temps = FUTURE
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("irai", "iras", "ira"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("irais", "irais", "irait", "irions", "iriez", "iraient"):
                                                                temps = PRESENT
                                                                mode = CONDITIONAL
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("irais", "irais", "irait"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("aille", "ailles", "aille", "allions", "allasse", "allasses"):
                                                                temps = PRESENT
                                                                mode = SUBJUNCTIVE
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("aille", "ailles", "aille"):
                                                                    nombre = SG
                                                            if verbe[1].lower() in ("allât", "allassions", "allassiez", "allassent", "alliez", "aillent"):
                                                                temps = PAST
                                                                mode = INDICATIVE
                                                                forme = IMPERFECTIVE
                                                                if verbe[1].lower() in ("allât", "allassions", "allassiez"):
                                                                    nombre = SG

                                                        else:
                                                            if verbe[1][-2:] in ("ez", "ns", "nt"):
                                                                nombre = PL
                                                            else:
                                                                nombre = SG
                                                            if verbe[1].endswith("erai") or verbe[1].endswith("eras") or \
                                                                    verbe[1].endswith("era") or verbe[1].endswith("erons") or \
                                                                    verbe[1].endswith("erez") or verbe[1].endswith("eront"):
                                                                temps = FUTURE
                                                                forme = IMPERFECTIVE
                                                                mode = INDICATIVE
                                                            elif  verbe[1].endswith("erais") or verbe[1].endswith("erais") or \
                                                                    verbe[1].endswith("erait") or verbe[1].endswith("erions") or verbe[1].endswith(
                                                                    "eriez") or verbe[1].endswith("eraient"):
                                                                temps= PRESENT
                                                                forme = IMPERFECTIVE
                                                                mode = CONDITIONAL
                                                            elif verbe[1].endswith("asse") or verbe[1].endswith("asses") or \
                                                                verbe[1].endswith("ât") or verbe[1].endswith("assions") or \
                                                                    verbe[1].endswith("assiez") or verbe[1].endswith("assent"):
                                                                forme = IMPERFECTIVE
                                                                temps = PAST
                                                                mode = SUBJUNCTIVE
                                                            elif verbe[1].endswith("ai") or verbe[1].endswith("as") or verbe[1].endswith(
                                                                    "a") or verbe[1].endswith("âmes") or verbe[1].endswith(
                                                                    "âtes") or verbe[1].endswith("èrent"):
                                                                forme = PERFECTIVE
                                                                temps = PAST
                                                                mode = INDICATIVE
                                                                if  verbe[1].endswith("âmes") or verbe[1].endswith("âtes"):
                                                                    nombre = PL
                                                            elif  verbe[1].endswith("ions") or verbe[1].endswith("iez"):
                                                                if verbe[5] == "m=subj":
                                                                    temps = PRESENT
                                                                    forme = IMPERFECTIVE
                                                                    mode = SUBJUNCTIVE
                                                                else:
                                                                    forme = IMPERFECTIVE
                                                                    temps = PAST
                                                                    mode = INDICATIVE
                                                            elif verbe[1].endswith("ais") or verbe[1].endswith("ais") or \
                                                                    verbe[1].endswith("ait") or verbe[1].endswith("aient"):
                                                                forme = IMPERFECTIVE
                                                                temps = PAST
                                                                mode = INDICATIVE
                                                            elif verbe[1].endswith("e") or verbe[1].endswith("es") or \
                                                                    verbe[1].endswith("e") or verbe[1].endswith("ons") or \
                                                                    verbe[1].endswith("ez") or verbe[1].endswith("ent"):
                                                                temps = PRESENT
                                                                forme = IMPERFECTIVE
                                                                mode = INDICATIVE

                                                                if verbe[1].endswith("e") or verbe[1].endswith("es") or \
                                                                        verbe[1].endswith("e") or verbe[1].endswith("ent"):
                                                                    if verbe[5] == "m=subj":
                                                                        temps = PRESENT
                                                                        forme = IMPERFECTIVE
                                                                        mode = SUBJUNCTIVE
                                                            if mode == '':
                                                                print((verbe, sentence))
                                                                print((temps, forme, mode))
                                                                print((is_tps_auxilié))
                                                        assert mode != ""
                                                        assert forme != ""
                                                        assert temps != ""
                                                        assert nombre != ""
                                                        conjugaisons_possibles.append((temps, personne, mode, forme, nombre, 'auxilié' if is_tps_auxilié and value == 'conj' else ''))

                                            if len(conjugaisons_possibles) == 0:
                                                take = False
                                            else:
                                                liste_temps = []
                                                liste_modes = []
                                                liste_personnes = []
                                                liste_nombres = []
                                                for information in conjugaisons_possibles:
                                                    take_info = True
                                                    temps = information[0]
                                                    person = information[1]
                                                    mode = information[2]
                                                    forme = information[3]
                                                    nombre = information[4]
                                                    if nombre is not None:
                                                        liste_nombres.append(nombre)
                                                    if person is not None:
                                                        liste_personnes.append(person)

                                                    if value == "conj":
                                                        if mode == INDICATIVE and temps == PRESENT and forme == IMPERFECTIVE:
                                                            temps = "présent"
                                                            mode = "indicatif"
                                                        elif mode == INDICATIVE and temps == PAST and forme == IMPERFECTIVE:
                                                            temps = "imparfait"
                                                            mode = "indicatif"
                                                        elif mode == INDICATIVE and temps == PAST and forme == PERFECTIVE:
                                                            temps = "passé_simple"
                                                            mode = "indicatif"
                                                        elif mode == INDICATIVE and temps == FUTURE and forme == IMPERFECTIVE:
                                                            temps = "futur"
                                                            mode = "indicatif"
                                                        elif mode == CONDITIONAL and temps == PRESENT and forme == IMPERFECTIVE:
                                                            temps = "présent"
                                                            mode = "conditionnel"
                                                        elif mode == SUBJUNCTIVE and temps == PRESENT and forme == IMPERFECTIVE:
                                                            temps = "présent"
                                                            mode = "subjonctif"
                                                        elif mode == SUBJUNCTIVE and temps == PAST and forme == IMPERFECTIVE:
                                                            temps = "imparfait"
                                                            mode = "subjonctif"
                                                        elif mode == IMPERATIVE and temps == PRESENT:
                                                            temps = "présent"
                                                            mode = "impératif"
                                                        else:
                                                            take_info = False

                                                        if take and is_tps_auxilié and len(auxiliaires)>0:
                                                            if temps == "présent" and mode == "indicatif":
                                                                temps = "passé_composé"
                                                                mode = "indicatif"
                                                            elif temps == "imparfait" and mode == "indicatif":
                                                                temps = "plus_que_parfait"
                                                                mode = "indicatif"
                                                            elif temps == "passé_simple" and mode == "indicatif":
                                                                temps = "passé_antérieur"
                                                                mode = "indicatif"
                                                            elif temps == "futur" and mode == "indicatif":
                                                                temps = "futur_antérieur"
                                                                mode = "indicatif"
                                                            elif temps == "présent" and mode == "subjonctif":
                                                                temps = "passé"
                                                                mode = "subjonctif"
                                                            elif temps == "imparfait" and mode == "subjonctif":
                                                                temps = "plus_que_parfait"
                                                                mode = "subjonctif"
                                                            elif temps == "présent" and mode == "conditionnel":
                                                                temps = "passé"
                                                                mode = "conditionnel"
                                                            elif temps == "présent" and mode == "impératif":
                                                                temps = "passé"
                                                                mode = "impératif"
                                                            else:
                                                                take_info = False

                                                    elif value == 'infinitif':
                                                        mode = "infinitif"
                                                        temps = ""
                                                    else:
                                                        mode = "participe"
                                                        if value == "vpa":
                                                            temps = "présent"
                                                        else:
                                                            temps = "passé"
                                                    if take_info:
                                                        if temps != "":
                                                            liste_temps.append(temps)
                                                        if mode != '':
                                                            liste_modes.append(mode)

                                                if len(liste_modes) == 0:
                                                    take = False
                                                else:

                                                    temps = " | ".join(sorted(set(liste_temps))) if len(liste_temps) > 0 else None
                                                    mode = " | ".join(sorted(set(liste_modes))) if len(liste_modes) > 0 else None
                                                    personne = " | ".join([k.__str__() for k in sorted(set(liste_personnes))])  if len(liste_personnes) > 0 else None
                                                    nombre = " | ".join([k.__str__() for k in sorted(set(liste_nombres))]) if len(liste_nombres) > 0 else None
                                                    
                                                assert mode != ""
                                                assert personne != ""

                                                antérieur_composé = [("frappé", "Ici, tout le monde se dit frappé de la ressemblance entre Mme de Lazareff et moi !"),
                                                                     ("abandonné", "Quant au comte de Wellingerode, on le dit entièrement abandonné des médecins."),
                                                                     ("criblé", "On le dit criblé de dettes, ruiné."),
                                                                     ("désespéré", "Émile s' enfuit désespéré."),
                                                                     ("est",  "Lorsqu' elles sont triangulaires, les deux ralingues qui partent de l' angle supérieur sont les ralingues de chute ; celle qui les réunit est la ralingue de bordure."),
                                                                     ("fait", "Gaspard avilit fait le coup !"),
                                                                     ("chargé", "Sur quoi Proudhon somme ce propriétaire, qui se dit chargé de pouvoirs du sol, de montrer sa procuration.")]
                                                for verbe_conjugué, sentence_comparée in antérieur_composé :
                                                    if verbe[1] == verbe_conjugué and sentence_comparée == sentence :
                                                        temps = "présent"
                                                        mode = "indicatif"
                                                if take:
                                                    évènements[event_key][token]["constraints"]["conjugaison"]["informations"] = {"socle":conjugaisons_possibles,
                                                                                                                                  "temps":temps,
                                                                                                                                  "mode":mode,
                                                                                                                                  "personne":personne,
                                                                                                                                  "nombre":nombre,}
                                                    if debug:
                                                        if value not in count_size_verbe.keys():
                                                            count_size_verbe[value] = {}
                                                        if len(conjugaisons_possibles) not in count_size_verbe[value].keys():
                                                            count_size_verbe[value][len(conjugaisons_possibles)] = 0
                                                        count_size_verbe[value][len(conjugaisons_possibles)] += 1

                        
                    if take:
                        flat_tokenize = []
                        for item in grew:
                            flat_tokenize.append(item[1].replace(" ", "_"))
                               
                        arbres_extraits[sentence] = {'titre':titre_texte, 'auteur':auteur, 'source':origine_texte,
                                                     'local_file':local_file,
                                                     "fichier":"arbres_extraits_" + (count_part+1).__str__().zfill(4) + ".json", "book number":i_book_file,                                                     #"'first_tree': first_tree,
                                                     "analyse number":i_analyse,
                                                     'events': évènements,
                                                     "flat tokenized":" ".join(flat_tokenize),
                                                     'analyses':
                                                         {'grew':grew, 'talismane':talismane, 'stanford':stanford, 'tokens_dones':tokens_dones}} #, 'dépendances':{'grew':grew, 'talismane':talismane, 'stanford':stanford}, 'infos brutes':finals
                        save_part_arbres()
                    else:
                        phrases_perdues += 1
                        phrases_perdues_phase_1.append(sentence)
                else:
                    phrases_perdues += 1
                    phrases_perdues_phase_2.append(sentence)

                for verbe in verbes_sentence.keys():
                    if verbe not in table_des_verbes.keys():
                        table_des_verbes[verbe] = {"pertes_collatérales":0, "décompte":0, "pertes":0, "dispersion_autres":[], "dispersion_vpp":[], "dispersion_vpa":[], "dispersion_infinitif":[],"vpp":0, "vpr":0, "autres":0, 'infinitif':0}
                    table_des_verbes[verbe]["décompte"] += 1
                    for key_v in verbes_sentence[verbe].keys():
                        if key_v.startswith("dispersion"):
                            table_des_verbes[verbe][key_v]+= verbes_sentence[verbe][key_v]
                        elif not take and key_v in ("vpp", "vpr", "autres", "infinitif"):
                            pass
                        elif key_v == "décompte":
                            pass
                        elif key_v not in table_des_verbes[verbe].keys() and key_v != "pertes" :
                            table_des_verbes[verbe][key_v] = verbes_sentence[verbe][key_v]
                        elif key_v == "cadres_initiaux" and  verbes_sentence[verbe][key_v] > table_des_verbes[verbe][key_v]:
                            table_des_verbes[verbe][key_v] = verbes_sentence[verbe][key_v]
                        elif key_v not in ("cadres_initiaux", "pertes"):
                            table_des_verbes[verbe][key_v] += verbes_sentence[verbe][key_v]
                        elif not take and key_v == "pertes":
                            if grew[i_verbe][2] == verbe:
                                table_des_verbes[verbe][key_v] += 1
                            else:
                                table_des_verbes[verbe]["pertes_collatérales"] += 1

            progress2.update()
            if debug and False:
                if len(arbres_extraits.keys()) > SAVE_PART:
                    break
        progress2.close()
    progress.update()
    if debug and False:
        break
progress.close()

if debug:
    print(len(phrases_perdues_phase_1))
    random.shuffle(phrases_perdues_phase_1)
    print(phrases_perdues_phase_1[0:3])
    print(len(phrases_perdues_phase_2))
    random.shuffle(phrases_perdues_phase_2)
    print(phrases_perdues_phase_2[0:3])
    pprint(take_table)

if debug:
    print()
    print("count_size_verbe (conjugaisons possibles)")
    pprint(count_size_verbe)
    print()
    
if debug: pprint({"cpls_intro":cpls_intro})

save_part_arbres(len(arbres_extraits.keys()) - SAVE_PART * int(len(arbres_extraits.keys())/SAVE_PART))

#for sentence in arbres_extraits.keys():
#    del arbres_extraits[sentence]['first_tree']

if not debug:
    fjson = open("table_des_arbres.json", "w")
    fjson.write(json.dumps(table_des_arbres, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

table_des_verbes_bck = {}
nb_verbes_sentence = 0
for sentence in arbres_extraits.keys():
    count_verbes = 0
    if "events" in arbres_extraits[sentence].keys():
        events = arbres_extraits[sentence]["events"]
        for cadre in events.keys():
            for item in events[cadre].keys():
                if "event" in events[cadre][item].keys():
                    verbe = events[cadre][item]["event"][2]
                    if verbe not in table_des_verbes_bck.keys():
                        table_des_verbes_bck[verbe] = {'pertes_collatérales':table_des_verbes[verbe]['pertes_collatérales'],
                                                       'pertes':table_des_verbes[verbe]['pertes'],
                                                       'cadres_finaux':set(),
                                                       'dispersion_autres': [],
                                                       'dispersion_vpp': [],
                                                       'dispersion_vpa': [],
                                                       'dispersion_infinitif': [],
                                                       'vpp': 0,
                                                       'vpr': 0,
                                                       'autres': 0,
                                                       'infinitif': 0,
                                                       'cadres_initiaux': table_des_verbes[verbe]['cadres_initiaux'],
                                                       'décompte': 0,
                                                      }
                    if "constraints" in events[cadre][item].keys():
                        count_verbes += 1
                        conjugaison = "autres"
                        if "infinitif" == events[cadre][item]["constraints"]["conjugaison"]["mode"]:
                            table_des_verbes_bck[verbe]['dispersion_infinitif'].append(cadre.replace("_", " "))
                            table_des_verbes_bck[verbe]['infinitif'] += 1
                        elif "vpa" == events[cadre][item]["constraints"]["conjugaison"]["mode"]:
                            table_des_verbes_bck[verbe]['dispersion_vpa'].append(cadre.replace("_", " "))
                            table_des_verbes_bck[verbe]['vpr'] += 1
                        elif "vpp" == events[cadre][item]["constraints"]["conjugaison"]["mode"]:
                            table_des_verbes_bck[verbe]['dispersion_vpp'].append(cadre.replace("_", " "))
                            table_des_verbes_bck[verbe]['vpp'] += 1
                        else:
                            table_des_verbes_bck[verbe]['dispersion_autres'].append(cadre.replace("_", " "))
                            table_des_verbes_bck[verbe]['autres'] += 1
                        table_des_verbes_bck[verbe]['décompte'] += 1
                        table_des_verbes_bck[verbe]['cadres_finaux'].add(cadre.replace("_", " "))
    if count_verbes not in dic_verbes_sentences.keys():
        dic_verbes_sentences[count_verbes] = 0
    dic_verbes_sentences[count_verbes] += 1
    nb_verbes_sentence += count_verbes

infos_générales = {"ph_conserv":len(arbres_extraits.keys()),
                   "ph_perdues":phrases_perdues,
                   "v_p_phrases":nb_verbes_sentence/len(arbres_extraits.keys()),
                   "dic_rep":dic_verbes_sentences
                   }
print()
print(infos_générales["ph_conserv"], end=" phrases conservées.\n")
print(infos_générales["ph_perdues"], end=" phrases perdues.\n")
print(infos_générales["v_p_phrases"], end=" verbes par phrase.\n")

pprint(infos_générales["dic_rep"])


if not debug:
    fjson = open("arbres/infos_générales.json", "w")
    fjson.write(json.dumps(infos_générales, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()

for verbe in table_des_verbes_bck.keys():
    table_des_verbes_bck[verbe]["cadres_finaux"] = len(table_des_verbes_bck[verbe]["cadres_finaux"])

if not debug:
    fjson = open("arbres/table_des_verbes.json", "w")
    fjson.write(json.dumps(table_des_verbes_bck, indent=2, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True

del table_des_verbes_bck


size_arbres = len(arbres_extraits.keys())

size_parties = math.floor(size_arbres/10)
reliquat = size_arbres - (size_parties*10)
count_part = 0

local_keys_arbres_extraits = list(arbres_extraits.keys())
progress = tqdm(range(10))

while len(local_keys_arbres_extraits) > 0:

    # on génère un petit fichier test
    if not os.path.exists("arbres/fichier_test.json"):
        arbres_ = {}
        sample = random.sample(local_keys_arbres_extraits, 100)

        for key in sample:
            info_arbre = copy.deepcopy(arbres_extraits[key])
            del info_arbre["analyses"]
#            info_arbre = reduce_analyse(info_arbre)
            arbres_[key] = info_arbre

        if not debug:
            fjson = open("arbres/fichier_test.json", "w")
            fjson.write(json.dumps(arbres_, indent=2, ensure_ascii=False))
            fjson.flush()
            fjson.close()
            while not fjson.closed:
                True

    sample = random.sample(local_keys_arbres_extraits, size_parties + reliquat)
    reliquat = 0
    arbres_ = {}

    for key in sample:
        local_keys_arbres_extraits.remove(key)
        arbres_[key] = arbres_extraits[key]

    if not debug:
        fjson = open("arbres/random/arbres_extraits_" + count_part.__str__() + ".json", "w")
        count_part += 1
        fjson.write(json.dumps(arbres_, indent=2, ensure_ascii=False))
        fjson.flush()
        fjson.close()
        while not fjson.closed:
            True
    progress.update()
progress.close()

#cela va être fort gros, mais utise pour les calculs
#for sentence in arbres_extraits.keys():
#    del arbres_extraits[sentence]['analyses']
if not debug:
    fjson = open("arbres/arbres_extraits.json", "w")
    fjson.write(json.dumps(arbres_extraits, ensure_ascii=False))
    fjson.flush()
    fjson.close()
    while not fjson.closed:
        True


print("len(phrases_perdues_phase_1)")
print(len(phrases_perdues_phase_1))
print("len(phrases_perdues_phase_2)")
print(len(phrases_perdues_phase_2))

exit()
