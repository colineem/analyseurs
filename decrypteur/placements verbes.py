#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Pdf maker for verbs informations.
"""
        
import json
import os, sys
import nltk

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

attributions = json.load(open('arbres/table_des_verbes.json', "r"))
infos_générales = json.load(open('arbres/infos_générales.json', "r"))

formate = lambda l: '{:,}'.format(l).replace(",", " ")
formatef = lambda l: "%8.2f" % l
tprint = lambda l: print(l, file=tableau)
cprint = lambda l: print("~\\\\`" + l.__str__() + "'~\\\\", file=tableau)

if not os.path.exists("documentation"):
    os.mkdir("documentation")

tableau = open('documentation/tableau.tex', "w")

sorted_by_value = sorted(attributions.items(), key=lambda kv: kv[1]["pertes"]/kv[1]["décompte"], reverse=True)
sorted_by_value = sorted(attributions.items(), key=lambda kv: kv[1]["décompte"], reverse=True)

def makeTable(items, caption, type, entetes = []):
    table = ""
    if len(items) == 0:
        return "~\\\\no data~\\\\"
    cline = "\hline"
    if type == "Verbes":
        ligne_1 = ['décompte', 'vpp', 'vpr', 'infinitif', 'autres', 'pertes', 'pertes_collatérales',  'cadres_initiaux', 'cadres_finaux', 'dispersion']
        entête_1 = ['décompte', 'vpp', 'vpr', 'infinitif', 'autres', '\multicolumn{2}{c|}{pertes}', '\multicolumn{3}{c|}{cadres}']
        entête_2 = ['', '', '', '','', 'directes', 'collatérales',  'initiaux', 'finaux', 'dispersion']
        cline = "\cline{7-11}"
    elif type == "Arbres":
        ligne_1 = ['verbes', 'conjugués', 'vpp', 'vpa', 'infinitif']
        entête_1 = ['verbes',  'conjugués', 'vpp', 'vpa', 'infinitif']
        entête_2 = []
    elif len(entetes) > 0:
        ligne_1 = entetes
        entête_1 = entetes
        entête_2 = []
    else:
        ligne_1 = items[0][1].keys()
        entête_1 = items[0][1].keys()
        entête_2 = []

    table += "\\begin{longtable}{|l|" + "l|"*len(ligne_1) + "} \n\t"
    table += """
\caption{""" + caption + """ ("""+ '{:,}'.format(len(items)).replace(",", " ") + " " + type.lower()
    table += """)}\\\\
\hline """
    table += """
 """ + type[0:-1] + " & " + " & " .join(entête_1).replace("_", ' ') + "\\\\" + cline + " \n\t" + """ """
    if len(entête_2)> 0:
        table += """
""" + "  & " + " & " .join(entête_2).replace("_", ' ') + "\\\\" + " \hline\n\t"

    table += """
\endfirsthead
\multicolumn{""" + (len(ligne_1)+1).__str__() + """}{c}%
{\\tablename\ \\thetable\ -- \\textit{Continuée de la page précédente}} \\\\
\hline"""
    table += """
 """ + type[0:-1] + " & " + " & " .join(entête_1).replace("_", ' ') + "\\\\" + cline + " \n\t" + """ """
    if len(entête_2)> 0:
        table += """
""" + "  & " + " & " .join(entête_2).replace("_", ' ') + "\\\\" + " \hline\n\t"

    table += """
\endhead
\hline \multicolumn{""" +(len(ligne_1) +1).__str__() + """}{r}{\\textit{suite de '""" + caption + """' page suivante}} \\\\
\endfoot
\hline
\endlastfoot
"""


    for ligne in items:
        table += ligne[0]
        values_dic = ligne[1]
        for value in ligne_1:
            if value  == "cadres_finaux":
                table += " & %8.2f" % values_dic[value]
            elif value == "dispersion":
                dispersion = set(values_dic['dispersion_autres'])
                dispersion.update(set(values_dic['dispersion_vpp']))
                dispersion.update(set(values_dic['dispersion_vpa']))
                dispersion.update(set(values_dic['dispersion_infinitif']))
                table += " & %8d" % len(dispersion)
            elif value == 'verbes':
                table += "&" + ", ".join(values_dic[value][0:6])
                if len(values_dic[value]) > 10:
                    table+=", \ldots{}"
            elif type == "Verbes":
                table +=  " & " + '{:,}'.format(values_dic[value]).replace(",", " ")
            else:
                if value == 'conjugués':
                    value = 'autres'
                table +=  " & " + values_dic[value].__str__()
        table += "\\\\" + " \hline\n\t"


    table += "\end{longtable} \t\t"

    return table

print("""\documentclass{article}
\\usepackage[utf8]{inputenc}
\\usepackage{longtable}
\\usepackage{geometry}
\\usepackage{hyperref}
\hypersetup{backref,
pdfpagemode=UseOutlines,
colorlinks=true}
\geometry{hmargin=1cm,vmargin=1.5cm}
\\title{Retour sur verbes}
\\author{Émilie Colin}
\date{\\today}
\\begin{document}
\\flushright{\\today}
\\\\
\\flushleft
""", file=tableau)
print()
print(infos_générales["ph_conserv"], end=" phrases conservées.\n")
print(infos_générales["ph_perdues"], end=" phrases perdues.\n")
print(infos_générales["v_p_phrases"], end=" verbes par phrase.\n")
print()
data = infos_générales["dic_rep"]


print("\\tableofcontents", file=tableau)
print(file=tableau)
print("\\vspace{1cm}",file=tableau)
print("\\noindent",file=tableau)
print(formate(infos_générales["ph_conserv"]) + " phrases conservées.\\\\",file=tableau)
print(formate(infos_générales["ph_perdues"]) + " phrases perdues.\\\\",file=tableau)
print(formatef(infos_générales["v_p_phrases"]) + " verbes par phrase.\\\\",file=tableau)

print(formate(len(sorted_by_value)) + """ verbes différents mais des pertes.\\\\
""" + '{:,}'.format(len([x for x in attributions.items() if x[1]["pertes"]+x[1]["pertes_collatérales"]!=x[1]["décompte"]])).replace(",", " ") + """ verbes sur phrases conservées.\\\\""", file=tableau)
print("""
\\begin{longtable}{|c|""" + "c|"*(len(data.keys())-1) + """}
\caption{Résumé des distributions en terme de verbes par phrase}\\\\
\hline"""+ " & ".join(["\\textbf{" + x.__str__() + "}" for x in sorted([int(x) for x in data.keys()])]) + """ \\\\\hline
\endfirsthead""", file=tableau)
print("""
\multicolumn{"""+(len(data.keys())-1).__str__()+"""}{c}%
{\\tablename\ \\thetable\ -- \\textit{Continué de la page précédente}} \\\\
\hline"""+ " & ".join(["\\textbf{" + x.__str__() + "}" for x in sorted([int(x) for x in data.keys()])]) + """ \\\\\hline
\endhead""", file=tableau)
print("""
\hline \multicolumn{"""+(len(data.keys())-1).__str__()+"""}{r}{\\textit{Continued on next page}} 
\endfoot
\hline
\endlastfoot
""" + " & ".join(["" + '{:,}'.format(data[x.__str__()]).replace(",", " ")  for x in sorted([int(x) for x in data.keys()])]) + """
\end{longtable}
""", file=tableau)
print('\section{Données avec perte intégrale et sans perte intégrale}', file=tableau)

data_usable = sorted([x for x in attributions.items() if x[1]["pertes"]+x[1]["pertes_collatérales"]!=x[1]["décompte"]], key=lambda kv: kv[1]["décompte"], reverse=True)

print('\subsection{' + '{:,}'.format(len(data_usable)).replace(",", " ") + ' verbes sans perte intégrale}', file=tableau)

print(makeTable(data_usable, "Données sans perte intégrale", "Verbes"), file =tableau)

data_unusability = sorted([x for x in attributions.items() if x[1]["pertes"]+x[1]["pertes_collatérales"]==x[1]["décompte"]], key=lambda kv: kv[1]["décompte"], reverse=True)
print('\subsection{' + '{:,}'.format(len(data_unusability)).replace(",", " ") + ' verbes avec perte intégrale}', file=tableau)
print(makeTable(data_unusability, "avec perte intégrale", "Verbes"), file=tableau)

trees_else = []
trees_vpa = []
trees_vpp = []
trees_inf = []

dic_tree_else = {}
dic_tree__vpa = {}
dic_tree__vpp = {}
dic_tree__inf = {}

for dispersion, dic_tree in zip(("dispersion_autres", "dispersion_vpa", "dispersion_vpp", "dispersion_infinitif"), (dic_tree_else, dic_tree__vpa, dic_tree__vpp, dic_tree__inf)):

    for x in data_usable:
        verbe = x[0]
        fdist1 = nltk.FreqDist(x[1][dispersion])

        for arbre, compte in sorted(fdist1.items()):
            if arbre not in dic_tree.keys():
                dic_tree[arbre] = [[], 0, []]
            dic_tree[arbre][0].append(verbe)
            dic_tree[arbre][1] += compte

for tree, dic_tree in zip((trees_else, trees_vpa, trees_vpp, trees_inf), (dic_tree_else, dic_tree__vpa, dic_tree__vpp, dic_tree__inf)):
    for arbre in dic_tree.keys():
        # tree.append((arbre, {'verbes':dic_tree[arbre][0], 'compte': dic_tree[arbre][1],
        #                     'autres':'X' if arbre in dic_tree_else.keys() else '',
        #                     'infinitif':'X' if arbre in dic_tree__inf.keys() else '',
        #                     'vpp':'X' if arbre in dic_tree__vpp.keys() else '',
        #                     'vpa':'X' if arbre in dic_tree__vpa.keys() else ''}))
        tree.append((arbre, {'verbes':dic_tree[arbre][0], #'compte': dic_tree[arbre][1],
                             'autres': dic_tree_else[arbre][1] if arbre in dic_tree_else.keys() else 0,
                             'infinitif': dic_tree__inf[arbre][1] if arbre in dic_tree__inf.keys() else 0,
                             'vpp': dic_tree__vpp[arbre][1] if arbre in dic_tree__vpp.keys() else 0,
                             'vpa':dic_tree__vpa[arbre][1]  if arbre in dic_tree__vpa.keys() else 0}))


print("\section{Arbres}", file=tableau)

# informations générales
tprint("\\subsection{Informations générales}")


print("source des informations générales: \\url{http://rali.iro.umontreal.ca/LVF+1/documents/PresentationLVF_JF_DLP_DL.pdf}", file=tableau)

tprint("page 10: ``l’appartenance aux types traditionnels est notée par une lettre majuscule''")

types_trads = [("A", {"type":"intransitif", "construction":"Sujet + Circonstant"}),
               ("N", {"type": "transitif direct", "construction": "Sujet + Complément Prépositionnel"}),
               ("T", {"type": "transitif", "construction": "Sujet + Objet direct + CompPrep + Circonstant"}),
               ("P", {"type": "pronominal", "construction": "Sujet + Objet direct + CompPrep + Circonstant"}),]
print(makeTable(types_trads, "appartenance aux types traditionnels", "Codes"), file =tableau)

tprint("page 11: ``Notation pour la nature du sujet et des compléments. Sujet = 1er caractère après A, N, T, P. Objet = 2è caractère après T et P''")


types_trads = [("1", {"type":"humain", "code":"qn"}),
               ("2", {"type": "animal", "code": "an(imal)"}),
               ("3", {"type":"chose", "code":"qc"}),
               ("4", {"type":"complétive ou chose", "code":"Q, Inf, qc"}),
               ("5", {"type":"complétive ou inf", "code":"Q/D+Inf"}),
               ("$7^6$", {"type":"pluriel humain", "code":"qn+pl"}),
               ("8", {"type":"pluriel chose", "code":"qc+pl"}),
               ("9", {"type":"humain ou chose", "code":"qn,qc"}),
               ]
print(makeTable(types_trads, "Codage de la nature du sujet et des compléments", "Codes"), file =tableau)
cprint('Exemple : croire 01 [T1400] On croit que tu dis la vérité')
tprint("~\\\\")
tprint("page 11: ``Notation pour les compléments prépositionnels et les circonstants (2è  caractère pour N et A, 3è et 4è caractère pour T et P)''")

tprint("``Une lettre minuscule code la préposition''")

types_trads = [("a", {"préposition":"à"}),
               ("b", {"préposition": "de"}),
               ("c", {"préposition": "avec"}),
               ("d", {"préposition": "contre"}),
               ("e", {"préposition": "par"}),
               ("g", {"préposition": "sur, vers"}),
               ("i", {"préposition": "de"}),
               ("j", {"préposition": "dans"}),
               ("k", {"préposition": "pour"}),
               ("l", {"préposition": "auprès"}),
               ("m", {"préposition": "devant"}),
               ("n", {"préposition": "divers mvts (cheminer le long de)"}),
               ("q", {"préposition": "pour"}),]

print(makeTable(types_trads, "Codage de la nature du sujet et des compléments", "Codes"), file =tableau)
cprint('Exemple :  avertir 01  [T11b0]  On avertit Pierre de mon arrivée')
tprint("~\\\\")
tprint("``Un chiffre code le type de complément'")

types_trads = [("1", {"type de complément":"locatif (où l’on est)", "code":"bivouaquer qp"}),
               ("2", {"type de complément": "locatif de destination", "code":"accourir qp"}),
               ("3", {"type de complément": "locatif d’origine", "code":"décamper de qp"}),
               ("4", {"type de complément": "double locatif", "code":"conduire de qp à qp"}),
               ("5", {"type de complément": "temps", "code":"durer deux heures, persévérer longtemps"}),
               ("6", {"type de complément": "modalité (manière, mesure, quantité)", "code":"aller bien, chausser du 37, manger bcp"}),
               ("7", {"type de complément": "cause", "code":"mourir d’un cancer"}),
               ("8", {"type de complément": "instrumental, moyen", "code":"montrer par un geste, blesser avec une arme"})]

print(makeTable(types_trads, "Codage du type de complément", "Codes"), file =tableau)
cprint('Exemple: conduire 04 [T3140] Ces empreintes conduisent l\'enquêteur au voleur')
tprint("~\\\\")

print('\subsection{' + '{:,}'.format(len(trees_else)).replace(",", " ") + ' arbres pour verbes conjugués.}', file=tableau)
print(makeTable(trees_else, "Arbres des verbes conjugués", "Arbres"), file =tableau)

print('\subsection{' + '{:,}'.format(len(trees_vpa)).replace(",", " ") + ' arbres pour verbes participes présents.}', file=tableau)
print(makeTable(trees_vpa, "Arbres des verbes au participe présent", "Arbres"), file =tableau)
print('\subsection{' + '{:,}'.format(len(trees_vpp)).replace(",", " ") + ' arbres pour verbes participes passés.}', file=tableau)
print(makeTable(trees_vpp, "Arbres des verbes au participe passé", "Arbres"), file =tableau)

print('\subsection{' + '{:,}'.format(len(trees_inf)).replace(",", " ") + ' arbres pour verbes à l\'infinitif.}', file=tableau)
print(makeTable(trees_inf, "Arbres des verbes à l'infinitif", "Arbres"), file =tableau)


print("""
\\end{document}
""", file=tableau)

tableau.close()

import subprocess
subprocess.run("cd documentation && pdflatex tableau.tex  && pdflatex tableau.tex  && pdflatex tableau.tex ", shell=True, check=True)