#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" TODO """
#Dropbox/local/git/analyseurs/decrypteur
import os
import json
import sys
import glob
import subprocess
from statistics import mean, median
pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

debug = False

arbres_extraits = {}

# chargement des données
def loadArbres():
    global debug
    if not debug:
        fichier = 'arbres/arbres_extraits.json'
        fichier2 = 'arbres_extraits.json'
    else:
        fichier = 'arbres/random/arbres_extraits_0.json'
        fichier2 = 'arbres/simple/arbres_extraits_0001.json'
    if os.path.exists(fichier):
        print("chargement " + fichier)
        a = open(fichier, "r")
        arbres_extraits = json.load(a)
        a.close()
        print("arbres chargés")
        return arbres_extraits
    elif os.path.exists(fichier2):
        print("chargement " + fichier)
        a = open(fichier2, "r")
        arbres_extraits = json.load(a)
        a.close()
        print("arbres chargés")
        return arbres_extraits
    print("arbres non available", file=sys.stderr)
    exit()

def loadInfos():
    if os.path.exists('arbres/infos_générales.json'):
        print("chargement infos_générales")
        a = open('arbres/infos_générales.json', "r")
        infos_générales = json.load(a)
        a.close()
        print("infos_générales chargés")
        return infos_générales
    elif os.path.exists('infos_générales.json'):
        print("chargement infos_générales")
        a = open('infos_générales.json', "r")
        infos_générales = json.load(a)
        a.close()
        print("infos_générales chargés")
        return infos_générales
    return {}

def loadSuccinctsLVF():
    if os.path.exists('lvf.json') and os.path.exists("lvf_état_fini.json"):
        print("lvf chargé")
        a = open('lvf.json', "r")
        lvf = json.load(a)
        a.close()
        a = open("lvf_état_fini.json", "r")
        lvf_état_fini = json.load(a)
        a.close()
        print("arbres chargés")
        return lvf, lvf_état_fini
    return {}, {}


if not os.path.exists("documentation"):
    os.mkdir("documentation")

if debug:
    tableau = open('documentation/documentationDebug.tex', "w")
else:
    tableau = open('documentation/documentation.tex', "w")

#fonctions de bases
tprint = lambda l: print(l, file=tableau)
fnf = lambda l: "{:,}".format(round(l, 2))
fn = lambda l: "{:,}".format(l) if int(l) == l else fnf(l)

def compil_():
    while not tableau.closed:
        tableau.close()
    if debug:
        subprocess.run("cd documentation && pdflatex documentationDebug.tex && pdflatex documentationDebug.tex && pdflatex documentationDebug.tex ", shell=True, check=True)
    else:
        subprocess.run("cd documentation && pdflatex documentation.tex && pdflatex documentation.tex && pdflatex documentation.tex ", shell=True, check=True)
    exit ()


def getEntete():
    return """\documentclass{article}
\\usepackage[utf8]{inputenc}
\\usepackage{longtable}
\\usepackage{geometry}
\\usepackage{fancyvrb}
\\usepackage{amsmath}
\\usepackage{hyperref}
\\usepackage{amsfonts}
\\usepackage{longtable}
\\usepackage{tabu}
\\usepackage{amssymb}
\\usepackage{graphicx}
\\usepackage{lmodern}
\\usepackage{standalone}
\\usepackage{listingsutf8}
\\usepackage{tikz}
\\usepackage{pgfplots}
\\usepackage[toc,page]{appendix}
\\hypersetup{backref,
pdfpagemode=UseOutlines,
colorlinks=true}
\geometry{hmargin=1cm,vmargin=1.5cm}
\\title{Retour sur verbes}
\\author{Émilie Colin}
\date{\\today}
\DefineVerbatimEnvironment{VRB}{Verbatim}{%
  formatcom={\\baselineskip=-\maxdimen\lineskip=1pt}
}
\\begin{document}
\\flushright{\\today}
\\\\
\\flushleft
"""

def getEnd():
    return """
\\end{document}
"""
def addComment(texte:str, rcBef=True, rcAfter=True):
    assert isinstance(texte, str)
    if rcBef:
        var1 = "\n"
    else:
        var1= ""
    if rcAfter: texte = texte +"\n"
    tableau.write(var1+"%s" % "% " + texte)
def addVisibleComment(texte:str):
    assert isinstance(texte, str)
    addComment("***************************", True, False)
    addComment("", True, True)
    addComment(texte, False, False)
    addComment("", True, False)
    addComment("***************************", True, True)
def addSection(titre:str):
    assert isinstance(titre, str)
    tprint("\n")
    tprint("\section{%s}" % titre)
    tprint("\n")
def addSubsection(titre:str):
    assert isinstance(titre, str)
    tprint("\n")
    tprint("\t\subsection{%s}" % titre)
    tprint("\n")
def addParagraph(texte:str, escaping = True):
    if isinstance(texte, int) or isinstance(texte, float):
        texte = "{:,}".format(texte)

    assert isinstance(texte, list) or isinstance(texte, str)
    if isinstance(texte, list):
        addList(texte)
        return
    tprint("\n")
    if escaping:
        for char in ("\\", "#", "$", "%", "&", "~", "_", "^", "{", "}"):
            texte = texte.replace(char, "\\"+char)
    tprint("%s" % texte)
    tprint("\n")
def addList(liste:list, title="", enumerate=False):
    assert isinstance(liste, list)
    tprint("\n")
    if title != "":
        tprint("~\\\\~\n\\noindent %s" % title)
    if enumerate:
        tprint("\\begin{enumerate}\n")
    else:
        tprint("\\begin{itemize}\n")
    for texte in liste:
        for char in ("#", "$", "%", "&", "~", "_", "^", ):
            texte = texte.replace(char, "\\"+char)
        tprint("\item %s" % texte)
    if enumerate:
        tprint("\end{enumerate}")
    else:
        tprint("\end{itemize}")
    tprint("\n")
def addVerbatim(texte:str, split=True):
    assert isinstance(texte, str)
    liste = [""]
    if split:
        texte = texte.split()
        sep = ""
        for elt in texte:
            if len(liste[-1]) + len(elt)>108:
                liste.append("")
                sep = ""
            liste[-1] += sep + elt
            sep = " "
    else:
        liste = texte.split('\n')
    tprint("\n")
    tprint("\\begin{VRB}\n")
    for texte in liste:
        tprint("%s\n" % texte)
    tprint("\end{VRB}")
    tprint("\n")
pathname = os.path.dirname(sys.argv[0])

sentences_dbs = sorted(glob.glob(os.path.join(fullpath, '../preparsing/outputs/results_70/*_analyses.txt')))

book_dbs = glob.glob("../preparsing/books/*/*.txt")
book_dbs += glob.glob("../preparsing/books/*.txt")
for i in range(len((book_dbs))):
    book_dbs[i] = book_dbs[i][book_dbs[i].rindex("/") + 1:]
assert len(book_dbs) == len(set(book_dbs))

tprint(getEntete())
addVisibleComment("Ce texte est généré automatiquement depuis ../documentation.py")

addSection("Free eBooks - Project Gutenberg")
addSubsection("Creation process")
addParagraph("Download " + fn(len(book_dbs)) + " french books from Gutenberg API." )

parseurs = ["grew, version 0.48.0, \href{http://grew.fr/}{http://grew.fr/},",
            "talismane, version 5.1.2, \href{http://redac.univ-tlse2.fr/applications/talismane.html}{http://redac.univ-tlse2.fr/applications/talismane.html},",
            "stanford, version 2018-02-27, \href{https://nlp.stanford.edu/software/lex-parser.shtml}{https://nlp.stanford.edu/software/lex-parser.shtml}."]
addList(parseurs, "Parse with three parsers :")
addParagraph("Their bibtex are :")
addVerbatim("""
@inproceedings{guillaume2012grew,
  title={Grew: un outil de réécriture de graphes For le TAL (Grew: a Graph Rewriting Tool for NLP)
  [in French]},
  author={Guillaume, Bruno and Bonfante, Guillame and Masson, Paul and Morey, Mathieu and Perrier, Guy},
  booktitle={Proceedings of the Joint Conference JEP-TALN-RECITAL 2012, volume 5: Software Demonstrations},
  pages={1--2},
  year={2012}
}
""", split = False)
addVerbatim("""
@phdthesis{Urieli2013Talismane,
  title={Robust French syntax analysis: reconciling statistical methods and 
         linguistic knowledge in the Talismane toolkit},
  author={Urieli, Assaf},
  year={2013},
  school={Université de Toulouse II le Mirail}
}
""", split=False)

addVerbatim("""
@inproceedings{nivre2016universal,
  title={Universal Dependencies v1: A Multilingual Treebank Collection.},
  author={Nivre, Joakim and De Marneffe, Marie-Catherine and Ginter, Filip and Goldberg, Yoav and Hajic, 
  Jan and Manning, Christopher D and McDonald, Ryan T and Petrov, Slav and Pyysalo, Sampo and Silveira, 
  Natalia and others},
  booktitle={LREC},
  year={2016}
}


""", split=False)

addSubsection("Pretreatments")
addParagraph("The " + fn(len(book_dbs)) + " gave around " + fn(3806889) + " sentence, and after filtering a little bit more than " + fn(1700000) + ".")
addParagraph("Filtering means avoid anomalies inside dependencies (exclusions of  minority non-correspondences inter-parsers).")
addParagraph("Filtering also means avoid sentences in others languages than french, and sentences without verb from lvf, and sentences composed of less 70 tokens.")
addParagraph("We use ``Les verbes français'' from Jean Dubois and Françoise Dubois-Charlier , version LVF+1. The bibtex are :", False)

addVerbatim("""
@inproceedings{dubois1997synonymie,
  title={Synonymie syntaxique et classification des verbes français},
  author={Dubois, Jean and Dubois-Charlier, Françoise},
  journal={Langages},
  pages={51--71},
  year={1997},
  publisher={JSTOR}
}
""", split=False)
addComment('see builder.py and tableau.pdf in preparsing')
addParagraph("To border anomalies, we align data, and only kept dependencies (dep) with strong consensus between parser, ie a dependencie used by grew like \\textit{a\_obj } has been found by the stanford parser as \\textit{iobj}, \\textit{case}, \\textit{dobj},\\textit{mark}\ldots ", False)
addParagraph("Align data means to use common tokens after the tokenization opered by each parser as pivots, and group splitted items when needed.")
addParagraph('We decide to kept the values commonly together, not in the 5 worst percentile (see p.\pageref{dep}).', False)
addParagraph('We assume the same process for the parts of speech (pos) tags, beeing here a little bit less strict because of the data. The 4 worst percentile were exclude (see p.\pageref{pos}).', False)
addParagraph('For the both process (dep and pos), we start from grew and see if '
             'there is really troubles, keeping partial analyses, too common in grew. '
             'It\'s the choice of keeping a lot of errors, but have more coherent matter to see later.'
             'It\'s a prefiltering.' , False)
#

addSubsection("Treatments")
addParagraph("""
An LVF verb description is - for the used kind of description, composed by :
\\begin{itemize}    
    \item always one and only one subject,
    \item one and only one or zero object, 
    \item one and only one or zero complement introduced by a preposition, 
    \item one and only one or zero adverbial phrase, 
\\end{itemize}
This description is codified : we know if the verb is pronominal, direct or indirect transitive or not transitive. 
The label of the description and annotations in the LVF base give us  semantics information (as T1400 : transitive (T) with a human subject (1) followed by a subordinate proposition or a thing (4)).

Sentence after sentence, we have found in the tokens verbes referenced by the LVF, excluding auxiliaries and load the lvf description.

We used the dependencies tags in the respect of this idea : one of the class should correspond... and we filter. Fast all the dep tags and pos tags are useful. If 'obj' is used, that's our object (or subject for a passive voice). We trust the parser, eliminating classes not conform, linking preposition with their dependence. The tag 's=refl' allows us to detect pronominal forms.
An incoherence break the treatment and we loose the sentence. 
At the end, if two descriptions have the same signature (for example one waits a human for subject, one an animal), we aggregate the information and alter the label, replacing the merged part of it by a ``Z''.

The classified verbs can be seen in appendices, p.\pageref{verbs} (in french).

""", False)

addSection("Statistics")
arbres_extraits = loadArbres()
infos_générales = loadInfos()

tokens = {}
books_used = {}
tokens_count = []
classes_lvf = {'Z':{}, 'pures':{}}
classes_lvf_lem = {}
sentences_lvf = {}
sentences_lvf_conj = {}
sentences_lvf_by_mode = {}
lvf_lem = {"conj":set(), "vpp":set(), "vpa":set(), "infinitif":set()}
for sentence in arbres_extraits.keys():
    s_infos = arbres_extraits[sentence]

    titre = s_infos["titre"] + s_infos["auteur"]
    if titre not in books_used:
        books_used[titre] = 1
    else:
        books_used[titre] += 1
    tokens_count.append(len(s_infos["analyses"]['grew']))
    for token in s_infos["analyses"]['grew']:
        if token[2] not in tokens.keys():
            tokens[token[2]] = 0
        tokens[token[2]] += 1
    events = s_infos["events"]
    for classe in events.keys():
        for token in events[classe]:
            if not "event" in events[classe][token].keys():
                break
            event = events[classe][token]["event"]
            conjugaison = events[classe][token]["constraints"]["conjugaison"]
            if (not classe in classes_lvf['pures'].keys() and not 'Z' in classe) or\
                    (not classe in classes_lvf['Z'].keys() and 'Z' in classe):
                if 'Z' in classe:
                    classes_lvf["Z"][classe] = {}
                else:
                    classes_lvf["pures"][classe] = {}
            if 'Z' in classe:
                classe_lvf = classes_lvf["Z"][classe]
            else:
                classe_lvf = classes_lvf["pures"][classe]
            if event[2] not in classe_lvf:
                classe_lvf[event[2]] = 1
            else:
                classe_lvf[event[2]] += 1
            if classe not in sentences_lvf.keys():
                sentences_lvf[classe] = 0
            sentences_lvf[classe] += 1
            if "conj" == conjugaison['mode']:
                if not classe in sentences_lvf_conj.keys():
                    sentences_lvf_conj[classe] = 0
                sentences_lvf_conj[classe] += 1
            if not classe in sentences_lvf_by_mode.keys():
                sentences_lvf_by_mode[classe] = {"conj":0, "vpp":0, "vpa":0, "infinitif":0, 'all':0}
            sentences_lvf_by_mode[classe][conjugaison['mode']] += 1
            lvf_lem[conjugaison['mode']].add(event[2])
            sentences_lvf_by_mode[classe]["all"] += 1

sentences_lvf_stats = []
sentences_lvf_stats_pure = []
for classe in sentences_lvf.keys():
    sentences_lvf_stats.append(sentences_lvf[classe])
    if not "Z" in classe:
        sentences_lvf_stats_pure.append(sentences_lvf[classe])
sentences_lvf_conj_stats = []
for classe in sentences_lvf_conj.keys():
    sentences_lvf_conj_stats.append(sentences_lvf_conj[classe])

all_lem = lvf_lem['conj'].intersection(lvf_lem['vpa']).intersection(lvf_lem['vpp']).intersection(lvf_lem['infinitif'])

sentences_lvf_mode_conj_stats = []
sentences_lvf_mode_conj_stats_conj = []
sentences_lvf_mode_conj_stats_vpa = []
sentences_lvf_mode_conj_stats_vpp = []
sentences_lvf_mode_conj_stats_inf = []
all_classes = set()
for classe in sentences_lvf_by_mode.keys():
    if sum([1 for m, k in sentences_lvf_by_mode[classe].items() if k > 0 and m != "all"]) == 4:
        all_classes.add(classe)
        sentences_lvf_mode_conj_stats.append(sentences_lvf_by_mode[classe]['all'])
        sentences_lvf_mode_conj_stats_conj.append(sentences_lvf_by_mode[classe]['conj'])
        sentences_lvf_mode_conj_stats_vpa.append(sentences_lvf_by_mode[classe]['vpa'])
        sentences_lvf_mode_conj_stats_vpp.append(sentences_lvf_by_mode[classe]['vpp'])
        sentences_lvf_mode_conj_stats_inf.append(sentences_lvf_by_mode[classe]['infinitif'])

pureVerbs = [val for sublist in [classes_lvf['pures'][k].keys() for k in classes_lvf['pures']] for val in sublist]
ZVerbs = [val for sublist in [classes_lvf['Z'][k].keys() for k in classes_lvf['Z']] for val in sublist]
evt_verbaux_pure = sum([sum(classes_lvf['pures'][k].values()) for k in classes_lvf['pures']])
evt_verbaux_Z = sum([sum(classes_lvf['Z'][k].values()) for k in classes_lvf['Z']])
allLems = pureVerbs + ZVerbs
#evt_verbaux_p = mean([mean(classes_lvf['pures'][k].values()) for k in classes_lvf['pures']])
#evt_verbaux_Z = mean([mean(classes_lvf['Z'][k].values()) for k in classes_lvf['Z']])

sentences_lvf_by_mode_only_with_allVerbs = {}
distinct_sentences = set()
lemmas = {}
lvf_lem = {"conj":set(), "vpp":set(), "vpa":set(), "infinitif":set()}
for sentence in arbres_extraits.keys():
    s_infos = arbres_extraits[sentence]
    events = s_infos["events"]
    for classe in events.keys():
        take = True
        verbs = []
        conjugaisons = []
        for token in events[classe]:
            if not "event" in events[classe][token].keys():
                # on exclut les phrases où il y aurait des verbes moins communs
                take = False
                break
            event = events[classe][token]["event"]
            verbs.append(event[2])
            conjugaisons.append(events[classe][token]["constraints"]["conjugaison"])
            if not event[2] in all_lem:
                take = False
                break
        if take:
            for verb in verbs:
                if verb not in lemmas:
                    lemmas[verb] = 0
                lemmas[verb] += 1
            for conjugaison in conjugaisons:
                if not classe in sentences_lvf_by_mode_only_with_allVerbs.keys():
                    sentences_lvf_by_mode_only_with_allVerbs[classe] = {"conj":0, "vpp":0, "vpa":0, "infinitif":0, 'all':0}
                sentences_lvf_by_mode_only_with_allVerbs[classe][conjugaison['mode']] += 1
                sentences_lvf_by_mode_only_with_allVerbs[classe]["all"] += 1
                distinct_sentences.add(sentence)

sentences_lvf_mode_conj_stats_only_with_allVerbs = []
sentences_lvf_mode_conj_stats_conj_only_with_allVerbs = []
sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs = []
sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs = []
sentences_lvf_mode_conj_stats_inf_only_with_allVerbs = []
for classe in sentences_lvf_by_mode_only_with_allVerbs.keys():
    if sum([1 for m, k in sentences_lvf_by_mode_only_with_allVerbs[classe].items() if k > 0 and m != "all"]) == 4:
        sentences_lvf_mode_conj_stats_only_with_allVerbs.append(sentences_lvf_by_mode_only_with_allVerbs[classe]['all'])
        sentences_lvf_mode_conj_stats_conj_only_with_allVerbs.append(sentences_lvf_by_mode_only_with_allVerbs[classe]['conj'])
        sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs.append(sentences_lvf_by_mode_only_with_allVerbs[classe]['vpa'])
        sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs.append(sentences_lvf_by_mode_only_with_allVerbs[classe]['vpp'])
        sentences_lvf_mode_conj_stats_inf_only_with_allVerbs.append(sentences_lvf_by_mode_only_with_allVerbs[classe]['infinitif'])
if False:
    tprint("""
    \\begin{center}
    \\begin{tikzpicture}
    \\begin{axis}[
    xlabel={\# sentences},
    ylabel={\# LVF classes},
    title={Sentences per LVF classes},
    ]
    \\addplot coordinates {
    """)
    for count in sorted(sentences_lvf_stats.keys()):
        x = sentences_lvf_stats[count]
        y = count
        tprint(""" (""" + x.__str__() + """,""" + y.__str__() +""") """)
    tprint("""
    };
    \end{axis}
    \end{tikzpicture}
    \end{center}
    """)
addParagraph("Note: find the LVF class could not be possible, if the same signature is share between several classes. A real LVF class is called 'pure', an LVF class created after desambiguization is called a 'Z class'")
stats = []
stats.append(fn(len(books_used)) + '/' + fn(len(book_dbs))+ " books used,")
stats.append(fn((infos_générales["ph_perdues"]+infos_générales["ph_conserv"])) + " sentences parsed,")
stats.append(fn(len(arbres_extraits.keys())) + " sentences after labelling (= experimental data),")
stats.append(fn(len(tokens.keys())) + " tokens,")
stats.append("Tokens frequencies : min " + fn(min(tokens.values())) + ", max " + fn(max(tokens.values())) + ", median " + fnf(median(tokens.values())) + ", avg " + fnf(mean(tokens.values())) + "." )
stats.append("Tokens per sentence : min " + fn(min(tokens_count)) + ", max " + fn(max(tokens_count)) + ", median " + fnf(median(tokens_count)) + ", avg " + fnf(mean(tokens_count)) + "." )
stats.append(fnf(infos_générales["v_p_phrases"]) + " verbs per sentence.")
stats.append(fn(len(classes_lvf['Z'].keys())+len(classes_lvf['pures'].keys())) +
             " LVF classes detected or formed, with " + fn(len(classes_lvf['pures'].keys())) + " pures :\\begin{itemize} \item" +
              fn(len(set(pureVerbs))) + " distincts lemmas for the pure classes " +
             "\item " + fn(len(set(ZVerbs))) + " distincts lemmas from a Z class. " +
             "\item " + fn(len(set(allLems))) + " distincts lemmas from all. " +
             "\item " +fn(len(set(pureVerbs).difference(set(ZVerbs)))) +  " verbal lemmas are in a pure verbs class but not in a Z class" +
             "\item " +fn(len(set(ZVerbs).difference(set(pureVerbs)))) + " verbal lemmas are in a Z class but not in a pure verbs class " +
             "\item " +fn(len(set(ZVerbs).intersection(set(pureVerbs)))) + " verbal lemmas are both in a Z class and in a pure verbs class \end{itemize}")
stats.append("Sentences per classes : min " + fn(min(sentences_lvf_stats)) + ", max " + fn(max(sentences_lvf_stats)) + ", median " + fnf(median(sentences_lvf_stats))+ ", avg " + fnf(mean(sentences_lvf_stats)) + "." )
stats.append("Sentences per pure classes : min " + fn(min(sentences_lvf_stats_pure)) + ", max " + fn(max(sentences_lvf_stats_pure)) + ", median " + fnf(median(sentences_lvf_stats_pure))+ ", avg " + fnf(mean(sentences_lvf_stats_pure)) + "." )
stats.append(fn(len(sentences_lvf_conj.keys())) + " classes for conjugated verbs." )
stats.append("Sentences per classes of conjugated verbs : min " + fn(min(sentences_lvf_conj_stats)) + ", max " + fn(max(sentences_lvf_conj_stats)) + ", median " + fnf(median(sentences_lvf_conj_stats))+ ", avg " + fnf(mean(sentences_lvf_conj_stats)) + "." )
stats.append(fn(len(sentences_lvf_by_mode.keys())) + " classes present in all modes for verbs (vpp, vpa, conj, infinitif)." )
stats.append("Sentences per classes of verbs of all modes (each verb can be only in one mode, each classe is realized in all mode) : \\\\min " + fn(min(sentences_lvf_mode_conj_stats)) + ", max " + fn(max(sentences_lvf_mode_conj_stats)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats)) + " with :\\begin{itemize} \item" +
             " sentences for conjugated : min " + fn(min(sentences_lvf_mode_conj_stats_conj)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_conj)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_conj))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_conj))  +
             "\item sentences for vpp : min " + fn(min(sentences_lvf_mode_conj_stats_vpp)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_vpp)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_vpp))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_vpp))  +
             "\item sentences for vpa : min " + fn(min(sentences_lvf_mode_conj_stats_vpa)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_vpa)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_vpa))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_vpa))  +
             "\item sentences for infinitive : min " + fn(min(sentences_lvf_mode_conj_stats_inf)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_inf)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_inf))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_inf))
             + " \end{itemize}")
stats.append('For the ' + fn(len(lemmas.keys())) + " distincts lemmas, there is  min " + fn(min(lemmas.values())) + ", max " + fn(max(lemmas.values())) + ", median " + fnf(median(lemmas.values()))+ ", avg " + fnf(mean(lemmas.values())) + " sentences." )
stats.append(fn(len(all_lem)) + " verbs presents in all modes (vpp, vpa, conj, infinitif),")
stats.append(fn(sum(sentences_lvf_mode_conj_stats_only_with_allVerbs)) + " sentences (" + fn(len(distinct_sentences)) + " distinct) for verbs presents in all modes (vpp, vpa, conj, infinitif),")
stats.append("Sentences per classes of verbs of all modes (with each verb and each classe is realized in all modes) :\\\\ min " +
             fn(min(sentences_lvf_mode_conj_stats_only_with_allVerbs)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_only_with_allVerbs)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_only_with_allVerbs))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_only_with_allVerbs)) + " with :\\begin{itemize} \item" +
             " sentences for conjugated : min " + fn(min(sentences_lvf_mode_conj_stats_conj_only_with_allVerbs)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_conj_only_with_allVerbs)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_conj_only_with_allVerbs))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_conj_only_with_allVerbs))  +
             "\item sentences for vpp : min " + fn(min(sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_vpp_only_with_allVerbs))  +
             "\item sentences for vpa : min " + fn(min(sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_vpa_only_with_allVerbs))  +
             "\item sentences for infinitive : min " + fn(min(sentences_lvf_mode_conj_stats_inf_only_with_allVerbs)) + ", max " + fn(max(sentences_lvf_mode_conj_stats_inf_only_with_allVerbs)) + ", median " + fnf(median(sentences_lvf_mode_conj_stats_inf_only_with_allVerbs))+ ", avg " + fnf(mean(sentences_lvf_mode_conj_stats_inf_only_with_allVerbs))
             + " \end{itemize}")

addList(stats, "About sentences after preparsing.")

addParagraph("""\\newpage\\begin{appendices}""", False)
addSubsection("Verbal events")
addParagraph("""\label{verbs}""", False)
# addParagraph("\input{tableau.tex}", False)
TAGS = ('''
\lstset{%
  breaklines=true,
  basicstyle=\\normalfont\\ttfamily,
  columns=flexible,
  frame=single,
  inputencoding=utf8/latin1,xleftmargin=0pt,breakindent=0pt,resetmargins=true
}
\section{Part Of Speech}\label{pos}
\subsection{grew}
\\begin{lstlisting}
['ADJ', 'ADJWH', 'ADV', 'ADVWH', 'CC', 'CLO', 'CLR', 'CLS', 'CS', 'DET', 'DETWH', 'ET', 'I', 'NC', 'NPP', 'P', 'P+D', 'P+PRO', 'PONCT', 'PREF', 'PRO', 'PROREL', 'PROWH', 'V', 'VIMP', 'VINF', 'VPP', 'VPR', 'VS']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{part of speech grew}\\\\\
\hline
grew & stanford & talismane\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{part of speech for grew continued on next page}} \\\\\
    \hline
grew & stanford & talismane\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{part of speech for grew continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
ADJ & ('NOUN', 610900, '10.23 \%') & ('ADJ', 5290926, '88.58 \%') \\\\\
~ & ('ADJ', 4328779, '72.48 \%') & \\textit{others (\#67)} : 11.42\% \\\\\
~ & \\textit{others (\#48)} : 17.3\% & ~ \\\\\
\hline

ADJWH & ('DET', 9563, '49.31 \%') & ('DETWH', 19333, '100.0 \%') \\\\\
~ & ('ADJ', 3981, '20.53 \%') & ~ \\\\\
~ & ('PRON', 4850, '25.01 \%') & ~ \\\\\
~ & \\textit{others (\#1)} : 5.16\% & ~ \\\\\
\hline

ADV & ('ADV', 3709950, '67.71 \%') & ('ADV', 4900380, '89.42 \%') \\\\\
~ & ('PART', 1117110, '20.39 \%') & \\textit{others (\#78)} : 10.58\% \\\\\
~ & \\textit{others (\#86)} : 11.91\% & ~ \\\\\
\hline

ADVWH & ('SCONJ', 6318, '12.83 \%') & ('ADVWH', 40549, '82.45 \%') \\\\\
~ & ('ADV', 41266, '83.83 \%') & \\textit{others (\#6)} : 17.55\% \\\\\
~ & \\textit{others (\#1)} : 3.33\% & ~ \\\\\
\hline

CC & ('CONJ', 2875114, '95.86 \%') & ('CC', 2848424, '94.97 \%') \\\\\
~ & \\textit{others (\#16)} : 4.14\% & \\textit{others (\#15)} : 5.03\% \\\\\
\hline

CLO & ('PRON', 1738962, '82.66 \%') & ('CLO', 1981711, '94.2 \%') \\\\\
~ & \\textit{others (\#22)} : 17.34\% & \\textit{others (\#24)} : 5.8\% \\\\\
\hline

CLR & ('PRON', 1106063, '95.82 \%') & ('CLR', 1095236, '94.89 \%') \\\\\
~ & \\textit{others (\#6)} : 4.18\% & \\textit{others (\#9)} : 5.11\% \\\\\
\hline

CLS & ('PRON', 4113819, '99.06 \%') & ('CLS', 3944196, '94.98 \%') \\\\\
~ & \\textit{others (\#22)} : 0.94\% & \\textit{others (\#28)} : 5.02\% \\\\\
\hline

CS & ('SCONJ', 1166451, '65.32 \%') & ('CS', 1415280, '79.25 \%') \\\\\
~ & ('PRON', 185071, '10.36 \%') & \\textit{others (\#46)} : 20.75\% \\\\\
~ & \\textit{others (\#37)} : 24.32\% & ~ \\\\\
\hline

DET & ('DET', 12113481, '92.29 \%') & ('DET', 12697515, '96.75 \%') \\\\\
~ & \\textit{others (\#49)} : 7.71\% & \\textit{others (\#43)} : 3.25\% \\\\\
\hline

DETWH & ('DET', 10420, '66.53 \%') & ('DETWH', 15631, '100.0 \%') \\\\\
~ & ('PRON', 4762, '30.4 \%') & ~ \\\\\
~ & \\textit{others (\#1)} : 3.06\% & ~ \\\\\
\hline

ET & ('PROPN', 36618, '21.2 \%') & ('NPP', 64747, '37.64 \%') \\\\\
~ & ('ADJ', 18029, '10.44 \%') & ('ADV', 21736, '12.64 \%') \\\\\
~ & ('NOUN', 41109, '23.8 \%') & ('NC', 43945, '25.55 \%') \\\\\
~ & ('VERB', 34351, '19.89 \%') & \\textit{others (\#35)} : 24.17\% \\\\\
~ & \\textit{others (\#27)} : 24.67\% & ~ \\\\\
\hline

I & ('VERB', 2364, '16.31 \%') & ('ADV', 5206, '36.15 \%') \\\\\
~ & ('INTJ', 2601, '17.94 \%') & ('I', 2642, '18.34 \%') \\\\\
~ & ('NOUN', 4941, '34.08 \%') & ('NC', 3400, '23.61 \%') \\\\\
~ & \\textit{others (\#8)} : 31.67\% & \\textit{others (\#3)} : 21.91\% \\\\\
\hline

NC & ('NOUN', 16052513, '93.61 \%') & ('NC', 16427922, '95.8 \%') \\\\\
~ & \\textit{others (\#60)} : 6.39\% & \\textit{others (\#78)} : 4.2\% \\\\\
\hline

NPP & ('PROPN', 2350277, '90.65 \%') & ('NPP', 2353765, '90.8 \%') \\\\\
~ & \\textit{others (\#42)} : 9.35\% & \\textit{others (\#65)} : 9.2\% \\\\\
\hline

P & ('ADP', 10812816, '94.48 \%') & ('P', 11072600, '96.75 \%') \\\\\
~ & \\textit{others (\#64)} : 5.52\% & \\textit{others (\#78)} : 3.25\% \\\\\
\hline

P+D & ('ADP, DET', 1098087, '62.0 \%') & ('P+D', 1659688, '93.7 \%') \\\\\
~ & ('ADP', 481125, '27.17 \%') & \\textit{others (\#33)} : 6.3\% \\\\\
~ & \\textit{others (\#33)} : 10.83\% & ~ \\\\\
\hline

P+PRO & ('NOUN', 10219, '35.38 \%') & ('P+PRO', 28505, '99.5 \%') \\\\\
~ & ('ADJ', 14003, '48.48 \%') & \\textit{others (\#1)} : 0.5\% \\\\\
~ & \\textit{others (\#6)} : 16.13\% & ~ \\\\\
\hline

PONCT & ('PUNCT', 11922461, '99.99 \%') & ('PONCT', 11922630, '99.99 \%') \\\\\
~ & \\textit{others (\#9)} : 0.01\% & \\textit{others (\#7)} : 0.01\% \\\\\
\hline

PREF & ('PRON', 965, '63.11 \%') & ('PRO, PONCT, ADJ', 338, '33.3 \%') \\\\\
~ & ('NOUN', 287, '18.77 \%') & ('VPP', 234, '23.05 \%') \\\\\
~ & \\textit{others (\#2)} : 18.12\% & ('PRO, PONCT, ADV', 248, '24.43 \%') \\\\\
~ & ~ & ('ADV, PONCT, ADJ', 103, '10.15 \%') \\\\\
~ & ~ & \\textit{others (\#1)} : 9.06\% \\\\\
\hline

PRO & ('PRON', 1109516, '83.72 \%') & ('PRO', 1083967, '81.82 \%') \\\\\
~ & \\textit{others (\#31)} : 16.28\% & \\textit{others (\#47)} : 18.18\% \\\\\
\hline

PROREL & ('PRON', 1536373, '90.07 \%') & ('PROREL', 1638349, '96.04 \%') \\\\\
~ & \\textit{others (\#9)} : 9.93\% & \\textit{others (\#14)} : 3.96\% \\\\\
\hline

PROWH & ('PROPN', 4467, '13.61 \%') & ('PROWH', 19451, '59.6 \%') \\\\\
~ & ('PRON', 26731, '81.45 \%') & ('NPP', 5142, '15.76 \%') \\\\\
~ & \\textit{others (\#3)} : 4.94\% & ('PROREL', 6337, '19.42 \%') \\\\\
~ & ~ & \\textit{others (\#5)} : 5.23\% \\\\\
\hline

V & ('VERB', 6207106, '72.1 \%') & ('V', 8359997, '97.11 \%') \\\\\
~ & ('AUX', 2069809, '24.04 \%') & \\textit{others (\#22)} : 2.89\% \\\\\
~ & \\textit{others (\#18)} : 3.86\% & ~ \\\\\
\hline

VIMP & ('VERB', 53542, '86.1 \%') & ('VIMP', 19556, '31.33 \%') \\\\\
~ & \\textit{others (\#5)} : 13.9\% & ('V', 37142, '59.51 \%') \\\\\
~ & ~ & \\textit{others (\#5)} : 9.15\% \\\\\
\hline

VINF & ('VERB', 2626947, '90.92 \%') & ('VINF', 2843456, '98.43 \%') \\\\\
~ & \\textit{others (\#15)} : 9.08\% & \\textit{others (\#19)} : 1.57\% \\\\\
\hline

VPP & ('VERB', 2337720, '84.04 \%') & ('VPP', 2636522, '94.79 \%') \\\\\
~ & \\textit{others (\#24)} : 15.96\% & \\textit{others (\#25)} : 5.21\% \\\\\
\hline

VPR & ('VERB', 443284, '82.56 \%') & ('VPR', 490623, '91.46 \%') \\\\\
~ & \\textit{others (\#7)} : 17.44\% & \\textit{others (\#9)} : 8.54\% \\\\\
\hline

VS & ('AUX', 96899, '35.05 \%') & ('VS', 220570, '79.83 \%') \\\\\
~ & ('VERB', 151863, '54.94 \%') & \\textit{others (\#8)} : 20.17\% \\\\\
~ & \\textit{others (\#8)} : 10.01\% & ~ \\\\\
\hline

\end{longtabu}
Grew offers 29 different labels, excluding grouping.
\paragraph{Stanford dispersion} : 
For 29 different POS : 47 occurrences (average 1.62).
\paragraph{Talismane stanford by grew} : average : 88.85\%.
\paragraph{Talismane dispersion} : 
For 29 different POS : 39 occurrences (average 1.34).
\paragraph{Talismane cover by grew} : average : 91.79\%.
\subsection{stanford}
\\begin{lstlisting}
['ADJ', 'ADP', 'ADV', 'AUX', 'CONJ', 'DET', 'INTJ', 'NOUN', 'NUM', 'PART', 'PRON', 'PROPN', 'PUNCT', 'SCONJ', 'SYM', 'VERB', 'X']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{part of speech stanford}\\\\\
\hline
stanford & grew & talismane\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{part of speech for stanford continued on next page}} \\\\\
    \hline
stanford & grew & talismane\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{part of speech for stanford continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
ADJ & ('ADJ', 4328779, '86.52 \%') & ('ADJ', 4238665, '84.73 \%') \\\\\
~ & \\textit{others (\#25)} : 13.48\% & \\textit{others (\#35)} : 15.27\% \\\\\
\hline

ADP & ('P', 10812816, '92.38 \%') & ('P', 10770814, '92.02 \%') \\\\\
~ & \\textit{others (\#22)} : 7.62\% & \\textit{others (\#33)} : 7.98\% \\\\\
\hline

ADV & ('ADV', 3709950, '86.2 \%') & ('ADV', 3613502, '83.98 \%') \\\\\
~ & \\textit{others (\#25)} : 13.8\% & \\textit{others (\#45)} : 16.02\% \\\\\
\hline

AUX & ('V', 2069809, '81.45 \%') & ('V', 2060195, '81.07 \%') \\\\\
~ & \\textit{others (\#20)} : 18.55\% & \\textit{others (\#17)} : 18.93\% \\\\\
\hline

CONJ & ('CC', 2875114, '99.57 \%') & ('CC', 2794215, '96.77 \%') \\\\\
~ & \\textit{others (\#6)} : 0.43\% & \\textit{others (\#9)} : 3.23\% \\\\\
\hline

DET & ('DET', 12113481, '92.87 \%') & ('DET', 12103641, '92.8 \%') \\\\\
~ & \\textit{others (\#18)} : 7.13\% & \\textit{others (\#16)} : 7.2\% \\\\\
\hline

INTJ & ('ADV', 19917, '59.41 \%') & ('ADV', 23138, '69.17 \%') \\\\\
~ & ('V', 4779, '14.26 \%') & ('V', 6080, '18.18 \%') \\\\\
~ & \\textit{others (\#7)} : 26.33\% & \\textit{others (\#4)} : 12.66\% \\\\\
\hline

NOUN & ('NC', 16052513, '91.66 \%') & ('NC', 15957229, '91.12 \%') \\\\\
~ & \\textit{others (\#27)} : 8.34\% & \\textit{others (\#64)} : 8.88\% \\\\\
\hline

NUM & ('DET', 406584, '56.29 \%') & ('DET', 293555, '40.64 \%') \\\\\
~ & ('ADJ', 205106, '28.4 \%') & ('ADJ', 242990, '33.64 \%') \\\\\
~ & \\textit{others (\#11)} : 15.31\% & ('NC', 118059, '16.34 \%') \\\\\
~ & ~ & \\textit{others (\#16)} : 9.39\% \\\\\
\hline

PART & ('ADV', 1117110, '96.79 \%') & ('ADV', 1115705, '96.68 \%') \\\\\
~ & \\textit{others (\#9)} : 3.21\% & \\textit{others (\#8)} : 3.32\% \\\\\
\hline

PRON & ('CLS', 4113819, '40.1 \%') & ('CLS', 3961941, '38.62 \%') \\\\\
~ & ('CLO', 1738962, '16.95 \%') & ('CLO', 1741823, '16.98 \%') \\\\\
~ & ('PROREL', 1536373, '14.98 \%') & ('PROREL', 1569837, '15.3 \%') \\\\\
~ & ('CLR', 1106063, '10.78 \%') & ('PRO', 1083607, '10.56 \%') \\\\\
~ & ('PRO', 1109516, '10.82 \%') & ('CLR', 1100619, '10.73 \%') \\\\\
~ & \\textit{others (\#28)} : 6.37\% & \\textit{others (\#44)} : 7.8\% \\\\\
\hline

PROPN & ('NPP', 2350277, '78.01 \%') & ('NPP', 2340959, '77.72 \%') \\\\\
~ & ('NC', 392804, '13.04 \%') & ('NC', 403507, '13.4 \%') \\\\\
~ & \\textit{others (\#32)} : 8.95\% & \\textit{others (\#50)} : 8.88\% \\\\\
\hline

PUNCT & ('PONCT', 11922461, '99.92 \%') & ('PONCT', 11938407, '99.99 \%') \\\\\
~ & \\textit{others (\#15)} : 0.08\% & \\textit{others (\#7)} : 0.01\% \\\\\
\hline

SCONJ & ('CS', 1166451, '78.72 \%') & ('CS', 1132564, '76.43 \%') \\\\\
~ & \\textit{others (\#19)} : 21.28\% & \\textit{others (\#21)} : 23.57\% \\\\\
\hline

SYM & ('DET', 11817, '16.79 \%') & ('DET', 11876, '16.89 \%') \\\\\
~ & ('NC', 8499, '12.07 \%') & ('PONCT', 52074, '74.05 \%') \\\\\
~ & ('CC', 46525, '66.1 \%') & \\textit{others (\#5)} : 9.06\% \\\\\
~ & \\textit{others (\#8)} : 5.04\% & ~ \\\\\
\hline

VERB & ('V', 6207106, '49.95 \%') & ('V', 6161972, '49.59 \%') \\\\\
~ & ('VINF', 2626947, '21.14 \%') & ('VINF', 2610916, '21.01 \%') \\\\\
~ & ('VPP', 2337720, '18.81 \%') & ('VPP', 2362326, '19.01 \%') \\\\\
~ & \\textit{others (\#21)} : 10.09\% & \\textit{others (\#22)} : 10.38\% \\\\\
\hline

X & ('NC', 26354, '38.09 \%') & ('NC', 23497, '34.05 \%') \\\\\
~ & ('V', 7028, '10.16 \%') & ('V', 7075, '10.25 \%') \\\\\
~ & ('NPP', 8497, '12.28 \%') & ('NPP', 17691, '25.64 \%') \\\\\
~ & ('ET', 8860, '12.8 \%') & \\textit{others (\#17)} : 30.05\% \\\\\
~ & \\textit{others (\#19)} : 26.67\% & ~ \\\\\
\hline

\end{longtabu}
stanford offers 17 different labels, excluding grouping.
\paragraph{Grew dispersion} : 
For 17 different POS : 31 occurrences (average 1.82).
\paragraph{Stanford cover by grew} : average : 88.66\%.
\paragraph{Talismane dispersion} : 
For 17 different POS : 30 occurrences (average 1.76).
\paragraph{Talismane cover by stanford} : average : 88.67\%.
\subsection{talismane}
\\begin{lstlisting}
['ADJ', 'ADV', 'ADVWH', 'CC', 'CLO', 'CLR', 'CLS', 'CS', 'DET', 'DETWH', 'ET', 'I', 'NC', 'NPP', 'P', 'P+D', 'P+PRO', 'PONCT', 'PRO', 'PROREL', 'PROWH', 'V', 'VIMP', 'VINF', 'VPP', 'VPR', 'VS']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{part of speech talismane}\\\\\
\hline
talismane & grew & stanford\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{part of speech for talismane continued on next page}} \\\\\
    \hline
talismane & grew & stanford\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{part of speech for talismane continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
ADJ & ('ADJ', 5290926, '90.17 \%') & ('NOUN', 605094, '10.31 \%') \\\\\
~ & \\textit{others (\#25)} : 9.83\% & ('ADJ', 4238665, '72.23 \%') \\\\\
~ & ~ & \\textit{others (\#19)} : 17.45\% \\\\\
\hline

ADV & ('ADV', 4900380, '93.71 \%') & ('ADV', 3613502, '69.1 \%') \\\\\
~ & \\textit{others (\#24)} : 6.29\% & ('PART', 1115705, '21.34 \%') \\\\\
~ & ~ & \\textit{others (\#23)} : 9.56\% \\\\\
\hline

ADVWH & ('ADVWH', 40549, '76.72 \%') & ('SCONJ', 6027, '11.39 \%') \\\\\
~ & ('ADV', 6449, '12.2 \%') & ('ADV', 42124, '79.61 \%') \\\\\
~ & \\textit{others (\#3)} : 11.07\% & \\textit{others (\#1)} : 9.0\% \\\\\
\hline

CC & ('CC', 2848424, '98.6 \%') & ('CONJ', 2794215, '96.72 \%') \\\\\
~ & \\textit{others (\#7)} : 1.4\% & \\textit{others (\#9)} : 3.28\% \\\\\
\hline

CLO & ('CLO', 1981711, '95.09 \%') & ('PRON', 1741823, '83.58 \%') \\\\\
~ & \\textit{others (\#8)} : 4.91\% & \\textit{others (\#7)} : 16.42\% \\\\\
\hline

CLR & ('CLR', 1095236, '95.96 \%') & ('PRON', 1100619, '96.43 \%') \\\\\
~ & \\textit{others (\#3)} : 4.04\% & \\textit{others (\#2)} : 3.57\% \\\\\
\hline

CLS & ('CLS', 3944196, '99.29 \%') & ('PRON', 3961941, '99.73 \%') \\\\\
~ & \\textit{others (\#5)} : 0.71\% & \\textit{others (\#5)} : 0.27\% \\\\\
\hline

CS & ('CS', 1415280, '84.54 \%') & ('SCONJ', 1132564, '67.65 \%') \\\\\
~ & \\textit{others (\#7)} : 15.46\% & \\textit{others (\#13)} : 32.35\% \\\\\
\hline

DET & ('DET', 12697515, '98.44 \%') & ('DET', 12103641, '93.84 \%') \\\\\
~ & \\textit{others (\#12)} : 1.56\% & \\textit{others (\#16)} : 6.16\% \\\\\
\hline

DETWH & ('ADJWH', 19333, '38.28 \%') & ('DET', 33415, '66.07 \%') \\\\\
~ & ('DET', 15443, '30.57 \%') & ('ADJ', 6276, '12.41 \%') \\\\\
~ & ('DETWH', 15631, '30.95 \%') & ('PRON', 9935, '19.64 \%') \\\\\
~ & \\textit{others (\#1)} : 0.2\% & \\textit{others (\#1)} : 1.88\% \\\\\
\hline

ET & ('V', 3875, '19.05 \%') & ('VERB', 8262, '40.59 \%') \\\\\
~ & ('ET', 9979, '49.05 \%') & ('PROPN', 2084, '10.24 \%') \\\\\
~ & \\textit{others (\#11)} : 31.9\% & ('NOUN', 3961, '19.46 \%') \\\\\
~ & ~ & ('ADJ', 2052, '10.08 \%') \\\\\
~ & ~ & \\textit{others (\#7)} : 19.62\% \\\\\
\hline

I & ('NC', 1202, '18.98 \%') & ('NOUN', 3275, '52.34 \%') \\\\\
~ & ('I', 2642, '41.72 \%') & ('PROPN', 980, '15.66 \%') \\\\\
~ & ('NPP', 692, '10.93 \%') & ('VERB', 1288, '20.58 \%') \\\\\
~ & ('VINF', 859, '13.57 \%') & \\textit{others (\#3)} : 11.41\% \\\\\
~ & \\textit{others (\#5)} : 14.8\% & ~ \\\\\
\hline

NC & ('NC', 16427922, '94.89 \%') & ('NOUN', 15957229, '92.18 \%') \\\\\
~ & \\textit{others (\#26)} : 5.11\% & \\textit{others (\#24)} : 7.82\% \\\\\
\hline

NPP & ('NPP', 2353765, '83.76 \%') & ('PROPN', 2340959, '83.35 \%') \\\\\
~ & \\textit{others (\#24)} : 16.24\% & \\textit{others (\#20)} : 16.65\% \\\\\
\hline

P & ('P', 11072600, '99.3 \%') & ('ADP', 10770814, '96.6 \%') \\\\\
~ & \\textit{others (\#14)} : 0.7\% & \\textit{others (\#22)} : 3.4\% \\\\\
\hline

P+D & ('P+D', 1659688, '97.33 \%') & ('ADP, DET', 1094200, '64.17 \%') \\\\\
~ & \\textit{others (\#2)} : 2.67\% & ('ADP', 477493, '28.0 \%') \\\\\
~ & ~ & \\textit{others (\#11)} : 7.83\% \\\\\
\hline

P+PRO & ('P+PRO', 28505, '100.0 \%') & ('NOUN', 10159, '35.63 \%') \\\\\
~ & ~ & ('ADJ', 13773, '48.3 \%') \\\\\
~ & ~ & \\textit{others (\#6)} : 16.07\% \\\\\
\hline

PONCT & ('PONCT', 11922630, '99.49 \%') & ('PUNCT', 11938407, '99.56 \%') \\\\\
~ & \\textit{others (\#16)} : 0.51\% & \\textit{others (\#4)} : 0.44\% \\\\\
\hline

PRO & ('PRO', 1083967, '89.28 \%') & ('PRON', 1083607, '89.23 \%') \\\\\
~ & \\textit{others (\#10)} : 10.72\% & \\textit{others (\#7)} : 10.77\% \\\\\
\hline

PROREL & ('PROREL', 1638349, '92.16 \%') & ('PRON', 1569837, '88.29 \%') \\\\\
~ & \\textit{others (\#5)} : 7.84\% & \\textit{others (\#5)} : 11.71\% \\\\\
\hline

PROWH & ('PROWH', 19451, '57.94 \%') & ('PRON', 30019, '89.5 \%') \\\\\
~ & ('ADV', 5920, '17.64 \%') & \\textit{others (\#3)} : 10.5\% \\\\\
~ & ('PROREL', 4936, '14.7 \%') & ~ \\\\\
~ & \\textit{others (\#2)} : 9.72\% & ~ \\\\\
\hline

V & ('V', 8359997, '97.52 \%') & ('VERB', 6161972, '71.88 \%') \\\\\
~ & \\textit{others (\#22)} : 2.48\% & ('AUX', 2060195, '24.03 \%') \\\\\
~ & ~ & \\textit{others (\#13)} : 4.09\% \\\\\
\hline

VIMP & ('VIMP', 19556, '61.64 \%') & ('VERB', 27759, '87.8 \%') \\\\\
~ & ('V', 9034, '28.47 \%') & \\textit{others (\#4)} : 12.2\% \\\\\
~ & \\textit{others (\#8)} : 9.89\% & ~ \\\\\
\hline

VINF & ('VINF', 2843456, '99.06 \%') & ('VERB', 2610916, '90.95 \%') \\\\\
~ & \\textit{others (\#13)} : 0.94\% & \\textit{others (\#12)} : 9.05\% \\\\\
\hline

VPP & ('VPP', 2636522, '92.71 \%') & ('VERB', 2362326, '83.07 \%') \\\\\
~ & \\textit{others (\#16)} : 7.29\% & \\textit{others (\#12)} : 16.93\% \\\\\
\hline

VPR & ('VPR', 490623, '95.51 \%') & ('VERB', 426525, '83.01 \%') \\\\\
~ & \\textit{others (\#9)} : 4.49\% & \\textit{others (\#7)} : 16.99\% \\\\\
\hline

VS & ('VS', 220570, '90.86 \%') & ('AUX', 93223, '38.34 \%') \\\\\
~ & \\textit{others (\#8)} : 9.14\% & ('VERB', 125026, '51.42 \%') \\\\\
~ & ~ & \\textit{others (\#8)} : 10.24\% \\\\\
\hline

\end{longtabu}
talismane offers 27 different labels, excluding grouping.
\paragraph{Grew dispersion} : 
For 27 different POS : 37 occurrences (average 1.37).
\paragraph{Grew cover by talismane} : average : 92.97\%.
\paragraph{Stanford dispersion} : 
For 27 different POS : 41 occurrences (average 1.52).
\paragraph{Stanford cover by talismane} : average : 89.42\%.
\section{Dependencies}\label{dep}

\subsection{grew}
\\begin{lstlisting}
['_', 'a_obj', 'aff', 'arg', 'arg.comp', 'ato', 'ats', 'aux.caus', 'aux.mod', 'aux.pass', 'aux.tps', 'coord', 'de_obj', 'dep', 'dep.coord', 'dep.coord|dep.coord', 'dep.coord|mod.cleft', 'dep.coord|mod.inc', 'dep.coord|mod.rel', 'dep.coord|obj', 'det', 'dis', 'mod', 'mod.app', 'mod.cleft', 'mod.inc', 'mod.rel', 'mod.voc', 'mod|suj', 'mod|suj|suj', 'mwe', 'obj', 'obj.cpl', 'obj.p', 'obj|suj', 'p_obj.agt', 'p_obj.o', 'ponct', 'ponct|coord', 'ponct|ponct', 'suj', 'suj|dep.coord', 'suj|mod', 'suj|obj', 'suj|suj', 'suj|suj|ats', 'suj|suj|mod', 'suj|suj|obj', 'suj|suj|suj', 'suj|suj|suj|suj']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{dependencies grew}\\\\\
\hline
grew & stanford & talismane\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{dependencies for grew continued on next page}} \\\\\
    \hline
grew & stanford & talismane\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{dependencies for grew continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
\_ & ('root', 2244499, '17.03 \%') & ('mod', 3560384, '26.99 \%') \\\\\
~ & ('punct', 1604506, '12.17 \%') & ('root', 3012491, '22.83 \%') \\\\\
~ & \\textit{others (\#205)} : 70.8\% & ('ponct', 1653035, '12.53 \%') \\\\\
~ & ~ & \\textit{others (\#144)} : 37.65\% \\\\\
\hline

a\_obj & ('iobj', 452241, '37.74 \%') & ('mod', 482456, '40.24 \%') \\\\\
~ & ('case', 274238, '22.88 \%') & ('aff', 250149, '20.86 \%') \\\\\
~ & ('dobj', 186978, '15.6 \%') & ('a\_obj', 183767, '15.33 \%') \\\\\
~ & ('mark', 122089, '10.19 \%') & ('obj', 182510, '15.22 \%') \\\\\
~ & \\textit{others (\#25)} : 13.59\% & \\textit{others (\#16)} : 8.34\% \\\\\
\hline

aff & ('dobj', 364374, '95.15 \%') & ('aff', 347292, '90.65 \%') \\\\\
~ & \\textit{others (\#17)} : 4.85\% & \\textit{others (\#10)} : 9.35\% \\\\\
\hline

arg & ('case', 8594, '92.73 \%') & ('mod', 4361, '47.07 \%') \\\\\
~ & \\textit{others (\#1)} : 7.27\% & ('dep', 4246, '45.83 \%') \\\\\
~ & ~ & \\textit{others (\#2)} : 7.09\% \\\\\
\hline

ato & ('xcomp', 69070, '39.17 \%') & ('mod', 79775, '45.16 \%') \\\\\
~ & ('amod', 38051, '21.58 \%') & ('obj', 43269, '24.49 \%') \\\\\
~ & \\textit{others (\#25)} : 39.26\% & ('ats', 21392, '12.11 \%') \\\\\
~ & ~ & \\textit{others (\#15)} : 18.24\% \\\\\
\hline

ats & ('root', 283580, '26.38 \%') & ('obj', 397600, '36.93 \%') \\\\\
~ & ('conj', 141210, '13.13 \%') & ('ats', 392331, '36.44 \%') \\\\\
~ & \\textit{others (\#47)} : 60.49\% & ('mod', 149734, '13.91 \%') \\\\\
~ & ~ & \\textit{others (\#33)} : 12.72\% \\\\\
\hline

aux.caus & ('root', 38514, '23.18 \%') & ('aux\_caus', 46958, '28.28 \%') \\\\\
~ & ('conj', 34578, '20.81 \%') & ('root', 30556, '18.4 \%') \\\\\
~ & ('acl', 28308, '17.04 \%') & ('\_', 17421, '10.49 \%') \\\\\
~ & ('acl:relcl', 21781, '13.11 \%') & \\textit{others (\#8)} : 42.84\% \\\\\
~ & ('advcl', 17374, '10.46 \%') & ~ \\\\\
~ & \\textit{others (\#11)} : 15.39\% & ~ \\\\\
\hline

aux.mod & ('aux', 354, '100.0 \%') & ('obj', 199, '59.76 \%') \\\\\
~ & ~ & ('prep', 134, '40.24 \%') \\\\\
\hline

aux.pass & ('auxpass', 249245, '54.84 \%') & ('aux\_pass', 271656, '59.74 \%') \\\\\
~ & ('aux', 111314, '24.49 \%') & ('aux\_tps', 129228, '28.42 \%') \\\\\
~ & ('cop', 74279, '16.34 \%') & \\textit{others (\#13)} : 11.84\% \\\\\
~ & \\textit{others (\#16)} : 4.33\% & ~ \\\\\
\hline

aux.tps & ('aux', 1044761, '90.83 \%') & ('aux\_tps', 989890, '86.05 \%') \\\\\
~ & \\textit{others (\#24)} : 9.17\% & \\textit{others (\#16)} : 13.95\% \\\\\
\hline

coord & ('cc', 1929989, '95.19 \%') & ('coord', 1883988, '92.91 \%') \\\\\
~ & \\textit{others (\#19)} : 4.81\% & \\textit{others (\#12)} : 7.09\% \\\\\
\hline

de\_obj & ('case', 317634, '53.44 \%') & ('mod', 482050, '81.09 \%') \\\\\
~ & ('mark', 115590, '19.45 \%') & \\textit{others (\#18)} : 18.91\% \\\\\
~ & \\textit{others (\#23)} : 27.11\% & ~ \\\\\
\hline

dep & ('case', 4095017, '77.86 \%') & ('mod', 2793113, '53.09 \%') \\\\\
~ & \\textit{others (\#79)} : 22.14\% & ('dep', 2132987, '40.55 \%') \\\\\
~ & ~ & \\textit{others (\#53)} : 6.36\% \\\\\
\hline

dep.coord & ('conj', 1662197, '58.86 \%') & ('dep\_coord', 2181378, '77.22 \%') \\\\\
~ & ('case', 390086, '13.81 \%') & \\textit{others (\#54)} : 22.78\% \\\\\
~ & \\textit{others (\#61)} : 27.33\% & ~ \\\\\
\hline

dep.coord|mod.inc & ('conj', 232, '100.0 \%') & ('sub', 242, '100.0 \%') \\\\\
\hline

dep.coord|mod.rel & ('conj', 2034, '66.64 \%') & ('dep\_coord', 2825, '92.35 \%') \\\\\
~ & ('acl:relcl', 405, '13.27 \%') & \\textit{others (\#2)} : 7.65\% \\\\\
~ & ('aux', 375, '12.29 \%') & ~ \\\\\
~ & \\textit{others (\#1)} : 7.8\% & ~ \\\\\
\hline

det & ('det', 9936121, '77.26 \%') & ('det', 12474849, '96.99 \%') \\\\\
~ & ('nmod:poss', 2105792, '16.37 \%') & \\textit{others (\#49)} : 3.01\% \\\\\
~ & \\textit{others (\#73)} : 6.37\% & ~ \\\\\
\hline

dis & ('mark', 4521, '89.28 \%') & ('mod', 4531, '87.78 \%') \\\\\
~ & \\textit{others (\#2)} : 10.72\% & \\textit{others (\#4)} : 12.22\% \\\\\
\hline

mod & ('amod', 3343009, '18.67 \%') & ('mod', 14938931, '83.35 \%') \\\\\
~ & ('case', 2960586, '16.53 \%') & \\textit{others (\#155)} : 16.65\% \\\\\
~ & ('advmod', 2795595, '15.61 \%') & ~ \\\\\
~ & \\textit{others (\#279)} : 49.18\% & ~ \\\\\
\hline

mod.app & ('appos', 43366, '40.25 \%') & ('mod', 95621, '88.16 \%') \\\\\
~ & ('conj', 38892, '36.1 \%') & \\textit{others (\#15)} : 11.84\% \\\\\
~ & \\textit{others (\#22)} : 23.65\% & ~ \\\\\
\hline

mod.cleft & ('acl:relcl', 22884, '73.82 \%') & ('mod\_rel', 23233, '75.16 \%') \\\\\
~ & \\textit{others (\#13)} : 26.18\% & ('mod', 4717, '15.26 \%') \\\\\
~ & ~ & \\textit{others (\#6)} : 9.58\% \\\\\
\hline

mod.inc & ('conj', 20280, '44.37 \%') & ('mod', 35356, '77.06 \%') \\\\\
~ & ('acl', 5713, '12.5 \%') & \\textit{others (\#10)} : 22.94\% \\\\\
~ & \\textit{others (\#16)} : 43.13\% & ~ \\\\\
\hline

mod.rel & ('acl:relcl', 848259, '67.55 \%') & ('mod\_rel', 855698, '68.15 \%') \\\\\
~ & \\textit{others (\#29)} : 32.45\% & ('mod', 179724, '14.31 \%') \\\\\
~ & ~ & \\textit{others (\#19)} : 17.53\% \\\\\
\hline

mod.voc & ('nsubj', 627, '35.67 \%') & ('mod', 933, '52.86 \%') \\\\\
~ & ('nmod', 570, '32.42 \%') & ('suj', 597, '33.82 \%') \\\\\
~ & ('root', 222, '12.63 \%') & \\textit{others (\#2)} : 13.31\% \\\\\
~ & ('appos', 197, '11.21 \%') & ~ \\\\\
~ & \\textit{others (\#1)} : 8.08\% & ~ \\\\\
\hline

mwe & ('nmod', 573, '34.79 \%') & ('mod', 782, '39.3 \%') \\\\\
~ & ('case', 532, '32.3 \%') & ('prep', 648, '32.56 \%') \\\\\
~ & ('mark', 420, '25.5 \%') & ('obj', 291, '14.62 \%') \\\\\
~ & \\textit{others (\#1)} : 7.41\% & \\textit{others (\#2)} : 13.52\% \\\\\
\hline

obj & ('dobj', 3716155, '64.65 \%') & ('obj', 4332951, '75.36 \%') \\\\\
~ & \\textit{others (\#63)} : 35.35\% & \\textit{others (\#48)} : 24.64\% \\\\\
\hline

obj.cpl & ('ccomp', 333941, '21.64 \%') & ('sub', 1076650, '69.77 \%') \\\\\
~ & ('advcl', 332949, '21.57 \%') & \\textit{others (\#22)} : 30.23\% \\\\\
~ & ('aux', 169678, '10.99 \%') & ~ \\\\\
~ & ('conj', 156945, '10.17 \%') & ~ \\\\\
~ & \\textit{others (\#28)} : 35.62\% & ~ \\\\\
\hline

obj.p & ('nmod', 9792461, '75.07 \%') & ('prep', 12286066, '94.17 \%') \\\\\
~ & \\textit{others (\#62)} : 24.93\% & \\textit{others (\#53)} : 5.83\% \\\\\
\hline

p\_obj.agt & ('case', 173265, '98.47 \%') & ('mod', 168941, '95.96 \%') \\\\\
~ & \\textit{others (\#4)} : 1.53\% & \\textit{others (\#8)} : 4.04\% \\\\\
\hline

p\_obj.o & ('case', 545700, '67.94 \%') & ('mod', 637033, '79.3 \%') \\\\\
~ & \\textit{others (\#36)} : 32.06\% & ('obj', 113207, '14.09 \%') \\\\\
~ & ~ & \\textit{others (\#25)} : 6.6\% \\\\\
\hline

ponct & ('punct', 10226010, '99.98 \%') & ('ponct', 10224626, '99.97 \%') \\\\\
~ & \\textit{others (\#8)} : 0.02\% & \\textit{others (\#8)} : 0.03\% \\\\\
\hline

ponct|ponct & ('punct', 10471, '100.0 \%') & ('ponct', 10472, '100.0 \%') \\\\\
\hline

suj & ('nsubj', 6521773, '83.06 \%') & ('suj', 6692626, '85.23 \%') \\\\\
~ & \\textit{others (\#66)} : 16.94\% & \\textit{others (\#60)} : 14.77\% \\\\\
\hline

suj|suj & ('nsubj', 330, '100.0 \%') & ('suj', 303, '100.0 \%') \\\\\
\hline

\end{longtabu}
grew offers 50 different labels, excluding grouping.
\paragraph{Stanford dispersion} : 
For 34 different DEP : 63 occurences (average : 1.85).
\paragraph{Talismane stanford by grew} : average : 80.35\%.
\paragraph{Talismane dispersion} : 
For 36 different DEP : 57 occurences (average : 1.58).
\paragraph{Talismane cover by grew} : average : 88.01\%.
\\newpage

\subsection{stanford}
\\begin{lstlisting}
['acl', 'acl:relcl', 'advcl', 'advmod', 'amod', 'appos', 'aux', 'auxpass', 'case', 'cc', 'ccomp', 'compound', 'conj', 'cop', 'csubj', 'dep', 'det', 'discourse', 'dislocated', 'dobj', 'expl', 'goeswith', 'iobj', 'mark', 'mwe', 'name', 'neg', 'nmod', 'nmod:poss', 'nsubj', 'nsubjpass', 'nummod', 'parataxis', 'punct', 'reparandum', 'root', 'vocative', 'xcomp']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{dependencies stanford}\\\\\
\hline
stanford & grew & talismane\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{dependencies for stanford continued on next page}} \\\\\
    \hline
stanford & grew & talismane\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{dependencies for stanford continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
acl & ('obj.p', 998256, '42.46 \%') & ('prep', 991761, '42.19 \%') \\\\\
~ & ('mod', 651102, '27.7 \%') & ('mod', 819798, '34.87 \%') \\\\\
~ & ('\_', 395917, '16.84 \%') & \\textit{others (\#22)} : 22.94\% \\\\\
~ & \\textit{others (\#17)} : 13.0\% & ~ \\\\\
\hline

acl:relcl & ('mod.rel', 848259, '53.46 \%') & ('mod\_rel', 816069, '51.44 \%') \\\\\
~ & ('\_', 226706, '14.29 \%') & ('mod', 255191, '16.09 \%') \\\\\
~ & \\textit{others (\#18)} : 32.26\% & \\textit{others (\#22)} : 32.47\% \\\\\
\hline

advcl & ('obj.p', 435620, '36.92 \%') & ('prep', 433152, '36.72 \%') \\\\\
~ & ('obj.cpl', 332949, '28.22 \%') & ('sub', 301933, '25.6 \%') \\\\\
~ & ('\_', 155016, '13.14 \%') & ('mod', 155484, '13.18 \%') \\\\\
~ & \\textit{others (\#16)} : 21.72\% & \\textit{others (\#17)} : 24.49\% \\\\\
\hline

advmod & ('mod', 2795595, '79.41 \%') & ('mod', 3053098, '86.76 \%') \\\\\
~ & ('\_', 494973, '14.06 \%') & \\textit{others (\#46)} : 13.24\% \\\\\
~ & \\textit{others (\#17)} : 6.53\% & ~ \\\\\
\hline

amod & ('mod', 3343009, '85.05 \%') & ('mod', 3521139, '89.59 \%') \\\\\
~ & \\textit{others (\#19)} : 14.95\% & \\textit{others (\#31)} : 10.41\% \\\\\
\hline

appos & ('mod', 478211, '44.59 \%') & ('mod', 614908, '57.37 \%') \\\\\
~ & ('\_', 204551, '19.07 \%') & ('obj', 157172, '14.66 \%') \\\\\
~ & \\textit{others (\#25)} : 36.34\% & \\textit{others (\#31)} : 27.97\% \\\\\
\hline

aux & ('aux.tps', 1044761, '49.63 \%') & ('aux\_tps', 1035061, '49.17 \%') \\\\\
~ & ('\_', 445293, '21.15 \%') & ('root', 296943, '14.11 \%') \\\\\
~ & \\textit{others (\#19)} : 29.22\% & \\textit{others (\#19)} : 36.72\% \\\\\
\hline

auxpass & ('aux.pass', 249245, '67.3 \%') & ('aux\_pass', 201005, '54.3 \%') \\\\\
~ & ('\_', 57301, '15.47 \%') & ('aux\_tps', 53268, '14.39 \%') \\\\\
~ & \\textit{others (\#12)} : 17.22\% & ('root', 37320, '10.08 \%') \\\\\
~ & ~ & \\textit{others (\#12)} : 21.23\% \\\\\
\hline

case & ('dep', 4095017, '41.09 \%') & ('mod', 6952370, '69.76 \%') \\\\\
~ & ('mod', 2960586, '29.7 \%') & ('dep', 2170532, '21.78 \%') \\\\\
~ & \\textit{others (\#23)} : 29.21\% & \\textit{others (\#33)} : 8.46\% \\\\\
\hline

cc & ('coord', 1929989, '64.61 \%') & ('coord', 2805329, '93.91 \%') \\\\\
~ & ('\_', 958310, '32.08 \%') & \\textit{others (\#18)} : 6.09\% \\\\\
~ & \\textit{others (\#13)} : 3.31\% & ~ \\\\\
\hline

ccomp & ('obj.cpl', 333941, '45.33 \%') & ('sub', 308455, '41.9 \%') \\\\\
~ & ('\_', 102850, '13.96 \%') & ('mod', 96653, '13.13 \%') \\\\\
~ & \\textit{others (\#17)} : 40.71\% & ('obj', 95150, '12.92 \%') \\\\\
~ & ~ & \\textit{others (\#19)} : 32.05\% \\\\\
\hline

compound & ('mod', 53841, '43.05 \%') & ('mod', 64412, '51.55 \%') \\\\\
~ & ('\_', 16189, '12.95 \%') & ('prep', 15290, '12.24 \%') \\\\\
~ & ('obj.p', 15969, '12.77 \%') & \\textit{others (\#23)} : 36.22\% \\\\\
~ & \\textit{others (\#15)} : 31.23\% & ~ \\\\\
\hline

conj & ('dep.coord', 1662197, '34.78 \%') & ('dep\_coord', 1528465, '31.98 \%') \\\\\
~ & ('\_', 817364, '17.1 \%') & ('mod', 1139939, '23.85 \%') \\\\\
~ & ('obj.p', 777150, '16.26 \%') & ('prep', 785106, '16.43 \%') \\\\\
~ & ('mod', 763442, '15.97 \%') & \\textit{others (\#41)} : 27.73\% \\\\\
~ & \\textit{others (\#27)} : 15.89\% & ~ \\\\\
\hline

cop & ('\_', 528398, '46.76 \%') & ('root', 375370, '33.22 \%') \\\\\
~ & ('obj.cpl', 147141, '13.02 \%') & ('mod', 225538, '19.96 \%') \\\\\
~ & ('mod', 127373, '11.27 \%') & ('sub', 150922, '13.36 \%') \\\\\
~ & \\textit{others (\#17)} : 28.95\% & \\textit{others (\#16)} : 33.46\% \\\\\
\hline

csubj & ('\_', 3100, '39.33 \%') & ('mod', 2130, '26.87 \%') \\\\\
~ & ('obj.cpl', 1333, '16.91 \%') & ('root', 1640, '20.69 \%') \\\\\
~ & ('mod', 1254, '15.91 \%') & ('sub', 1166, '14.71 \%') \\\\\
~ & \\textit{others (\#7)} : 27.86\% & \\textit{others (\#7)} : 37.73\% \\\\\
\hline

dep & ('\_', 18353, '43.13 \%') & ('mod', 14124, '33.18 \%') \\\\\
~ & ('mod', 7306, '17.17 \%') & ('suj', 7291, '17.13 \%') \\\\\
~ & ('suj', 6651, '15.63 \%') & ('\_', 5974, '14.03 \%') \\\\\
~ & \\textit{others (\#11)} : 24.08\% & ('obj', 5211, '12.24 \%') \\\\\
~ & ~ & \\textit{others (\#12)} : 23.42\% \\\\\
\hline

det & ('det', 9936121, '90.16 \%') & ('det', 9967966, '90.45 \%') \\\\\
~ & \\textit{others (\#17)} : 9.84\% & \\textit{others (\#23)} : 9.55\% \\\\\
\hline

discourse & ('mod', 6680, '70.42 \%') & ('mod', 8389, '86.6 \%') \\\\\
~ & ('\_', 2806, '29.58 \%') & \\textit{others (\#4)} : 13.4\% \\\\\
\hline

dobj & ('obj', 3716155, '57.67 \%') & ('obj', 3816524, '59.23 \%') \\\\\
~ & ('\_', 949458, '14.73 \%') & ('aff', 1149851, '17.85 \%') \\\\\
~ & \\textit{others (\#24)} : 27.6\% & \\textit{others (\#40)} : 22.92\% \\\\\
\hline

expl & ('mod', 90458, '59.15 \%') & ('aff', 65481, '42.92 \%') \\\\\
~ & ('det', 28532, '18.66 \%') & ('det', 29426, '19.29 \%') \\\\\
~ & \\textit{others (\#11)} : 22.2\% & ('obj', 16772, '10.99 \%') \\\\\
~ & ~ & ('mod', 15473, '10.14 \%') \\\\\
~ & ~ & \\textit{others (\#10)} : 16.65\% \\\\\
\hline

goeswith & ('mod', 101, '100.0 \%') & ('mod', 100, '100.0 \%') \\\\\
\hline

iobj & ('a\_obj', 452241, '47.72 \%') & ('obj', 265071, '27.98 \%') \\\\\
~ & ('\_', 147999, '15.62 \%') & ('aff', 244371, '25.79 \%') \\\\\
~ & ('obj', 124271, '13.11 \%') & ('a\_obj', 193273, '20.4 \%') \\\\\
~ & \\textit{others (\#12)} : 23.56\% & ('\_', 107705, '11.37 \%') \\\\\
~ & ~ & \\textit{others (\#14)} : 14.46\% \\\\\
\hline

mark & ('mod', 1194620, '40.98 \%') & ('mod', 1971403, '67.64 \%') \\\\\
~ & ('\_', 615501, '21.12 \%') & ('obj', 382471, '13.12 \%') \\\\\
~ & ('obj', 392553, '13.47 \%') & \\textit{others (\#24)} : 19.24\% \\\\\
~ & \\textit{others (\#19)} : 24.44\% & ~ \\\\\
\hline

mwe & ('\_', 117837, '37.12 \%') & ('mod', 178898, '56.45 \%') \\\\\
~ & ('mod', 87783, '27.65 \%') & \\textit{others (\#22)} : 43.55\% \\\\\
~ & ('dep', 35597, '11.21 \%') & ~ \\\\\
~ & \\textit{others (\#16)} : 24.01\% & ~ \\\\\
\hline

name & ('mod', 158113, '65.74 \%') & ('mod', 190586, '79.34 \%') \\\\\
~ & ('\_', 29286, '12.18 \%') & \\textit{others (\#16)} : 20.66\% \\\\\
~ & \\textit{others (\#14)} : 22.08\% & ~ \\\\\
\hline

neg & ('mod', 1633515, '93.52 \%') & ('mod', 1617548, '92.63 \%') \\\\\
~ & \\textit{others (\#11)} : 6.48\% & \\textit{others (\#14)} : 7.37\% \\\\\
\hline

nmod & ('obj.p', 9792461, '86.93 \%') & ('prep', 9681112, '85.96 \%') \\\\\
~ & \\textit{others (\#30)} : 13.07\% & \\textit{others (\#52)} : 14.04\% \\\\\
\hline

nmod:poss & ('det', 2105792, '98.14 \%') & ('det', 2110127, '98.35 \%') \\\\\
~ & \\textit{others (\#8)} : 1.86\% & \\textit{others (\#9)} : 1.65\% \\\\\
\hline

nsubj & ('suj', 6521773, '86.4 \%') & ('suj', 6221400, '82.44 \%') \\\\\
~ & \\textit{others (\#28)} : 13.6\% & \\textit{others (\#49)} : 17.56\% \\\\\
\hline

nsubjpass & ('suj', 264675, '84.89 \%') & ('suj', 252952, '81.19 \%') \\\\\
~ & \\textit{others (\#13)} : 15.11\% & \\textit{others (\#21)} : 18.81\% \\\\\
\hline

nummod & ('det', 311268, '57.23 \%') & ('det', 262848, '48.31 \%') \\\\\
~ & ('mod', 174804, '32.14 \%') & ('mod', 240559, '44.21 \%') \\\\\
~ & \\textit{others (\#20)} : 10.63\% & \\textit{others (\#14)} : 7.48\% \\\\\
\hline

parataxis & ('mod', 172406, '36.0 \%') & ('mod', 248856, '52.01 \%') \\\\\
~ & ('\_', 165402, '34.54 \%') & ('obj', 61844, '12.93 \%') \\\\\
~ & \\textit{others (\#17)} : 29.46\% & \\textit{others (\#18)} : 35.06\% \\\\\
\hline

punct & ('ponct', 10226010, '85.62 \%') & ('ponct', 11935299, '99.87 \%') \\\\\
~ & ('\_', 1604506, '13.43 \%') & \\textit{others (\#15)} : 0.13\% \\\\\
~ & \\textit{others (\#19)} : 0.95\% & ~ \\\\\
\hline

root & ('\_', 2244499, '65.34 \%') & ('root', 1955105, '56.93 \%') \\\\\
~ & \\textit{others (\#21)} : 34.66\% & \\textit{others (\#31)} : 43.07\% \\\\\
\hline

xcomp & ('\_', 166737, '22.75 \%') & ('obj', 350155, '47.79 \%') \\\\\
~ & ('obj', 135002, '18.42 \%') & ('mod', 125159, '17.08 \%') \\\\\
~ & ('mod', 103618, '14.14 \%') & \\textit{others (\#22)} : 35.13\% \\\\\
~ & \\textit{others (\#15)} : 44.69\% & ~ \\\\\
\hline

\end{longtabu}
stanford offers 38 different labels, excluding grouping.
\paragraph{Grew dispersion} : 
For 35 different DEP : 73 occurences (average : 2.09).
\paragraph{Stanford cover by grew} : average : 80.09\%.
\paragraph{Talismane dispersion} : 
For 35 different DEP : 67 occurences (average : 1.91).
\paragraph{Talismane cover by stanford} : average : 78.99\%.
\\newpage

\subsection{talismane}
\\begin{lstlisting}
['_', 'a_obj', 'aff', 'arg', 'ato', 'ats', 'aux_caus', 'aux_pass', 'aux_tps', 'comp', 'coord', 'de_obj', 'dep', 'dep_coord', 'det', 'mod', 'mod_rel', 'obj', 'p_obj', 'ponct', 'prep', 'root', 'sub', 'suj']
\end{lstlisting}
\\begin{longtabu}{|X|c|c|}
\caption{dependencies talismane}\\\\\
\hline
talismane & grew & stanford\\\\\
\hline
\endfirsthead
\multicolumn{3}{c}{\\tablename\ \\thetable\ -- \\textit{dependencies for talismane continued on next page}} \\\\\
    \hline
talismane & grew & stanford\\\\\
\hline
\hline
    \endhead
    \hline \multicolumn{3}{r}{\\tablename\ \\thetable\ -- \\textit{dependencies for talismane continued on next page}} \\\\\
    \endfoot
    \hline
    \endlastfoot
\_ & ('\_', 813617, '34.42 \%') & ('nsubj', 369584, '15.65 \%') \\\\\
~ & ('mod', 416976, '17.64 \%') & ('dobj', 249568, '10.57 \%') \\\\\
~ & ('suj', 318889, '13.49 \%') & ('conj', 247114, '10.46 \%') \\\\\
~ & \\textit{others (\#29)} : 34.45\% & \\textit{others (\#56)} : 63.31\% \\\\\
\hline

a\_obj & ('a\_obj', 183767, '69.92 \%') & ('iobj', 193273, '73.62 \%') \\\\\
~ & ('obj', 27001, '10.27 \%') & \\textit{others (\#15)} : 26.38\% \\\\\
~ & \\textit{others (\#10)} : 19.81\% & ~ \\\\\
\hline

aff & ('obj', 486763, '29.96 \%') & ('dobj', 1149851, '70.77 \%') \\\\\
~ & ('aff', 347292, '21.38 \%') & ('iobj', 244371, '15.04 \%') \\\\\
~ & ('\_', 321239, '19.77 \%') & \\textit{others (\#29)} : 14.19\% \\\\\
~ & ('a\_obj', 250149, '15.4 \%') & ~ \\\\\
~ & \\textit{others (\#13)} : 13.49\% & ~ \\\\\
\hline

arg & ('\_', 505, '44.14 \%') & ('acl:relcl', 218, '25.38 \%') \\\\\
~ & ('mod', 283, '24.74 \%') & ('acl', 188, '21.89 \%') \\\\\
~ & ('obj.cpl', 260, '22.73 \%') & ('conj', 172, '20.02 \%') \\\\\
~ & \\textit{others (\#1)} : 8.39\% & ('xcomp', 151, '17.58 \%') \\\\\
~ & ~ & ('advcl', 130, '15.13 \%') \\\\\
\hline

ato & ('\_', 10527, '30.47 \%') & ('xcomp', 6080, '17.78 \%') \\\\\
~ & ('ato', 7143, '20.67 \%') & ('nsubj', 5853, '17.12 \%') \\\\\
~ & ('mod', 6593, '19.08 \%') & ('nmod', 3492, '10.21 \%') \\\\\
~ & \\textit{others (\#12)} : 29.77\% & \\textit{others (\#19)} : 54.88\% \\\\\
\hline

ats & ('ats', 392331, '65.53 \%') & ('root', 167044, '27.9 \%') \\\\\
~ & ('mod', 97261, '16.24 \%') & ('conj', 85196, '14.23 \%') \\\\\
~ & \\textit{others (\#14)} : 18.23\% & ('amod', 61135, '10.21 \%') \\\\\
~ & ~ & \\textit{others (\#26)} : 47.65\% \\\\\
\hline

aux\_caus & ('aux.caus', 46958, '58.06 \%') & ('acl', 28859, '35.75 \%') \\\\\
~ & ('obj.p', 17556, '21.71 \%') & ('advcl', 12260, '15.19 \%') \\\\\
~ & \\textit{others (\#11)} : 20.23\% & ('conj', 11175, '13.84 \%') \\\\\
~ & ~ & ('aux', 8639, '10.7 \%') \\\\\
~ & ~ & \\textit{others (\#16)} : 24.51\% \\\\\
\hline

aux\_pass & ('aux.pass', 271656, '68.72 \%') & ('auxpass', 201005, '50.84 \%') \\\\\
~ & ('aux.tps', 53514, '13.54 \%') & ('aux', 105121, '26.59 \%') \\\\\
~ & \\textit{others (\#12)} : 17.74\% & ('cop', 49758, '12.59 \%') \\\\\
~ & ~ & \\textit{others (\#19)} : 9.98\% \\\\\
\hline

aux\_tps & ('aux.tps', 989890, '79.93 \%') & ('aux', 1035061, '83.57 \%') \\\\\
~ & ('aux.pass', 129228, '10.44 \%') & \\textit{others (\#26)} : 16.43\% \\\\\
~ & \\textit{others (\#11)} : 9.63\% & ~ \\\\\
\hline

comp & ('\_', 570, '37.9 \%') & ('nsubj', 573, '54.16 \%') \\\\\
~ & ('suj', 431, '28.66 \%') & ('conj', 242, '22.87 \%') \\\\\
~ & ('dep.coord', 205, '13.63 \%') & ('acl:relcl', 152, '14.37 \%') \\\\\
~ & ('mod', 183, '12.17 \%') & \\textit{others (\#1)} : 8.6\% \\\\\
~ & \\textit{others (\#1)} : 7.65\% & ~ \\\\\
\hline

coord & ('coord', 1883988, '64.84 \%') & ('cc', 2805329, '96.55 \%') \\\\\
~ & ('\_', 898610, '30.93 \%') & \\textit{others (\#28)} : 3.45\% \\\\\
~ & \\textit{others (\#15)} : 4.23\% & ~ \\\\\
\hline

de\_obj & ('mod', 10513, '33.07 \%') & ('mark', 10637, '33.7 \%') \\\\\
~ & ('de\_obj', 9335, '29.37 \%') & ('case', 7094, '22.47 \%') \\\\\
~ & ('\_', 5562, '17.5 \%') & ('iobj', 6963, '22.06 \%') \\\\\
~ & \\textit{others (\#7)} : 20.06\% & \\textit{others (\#8)} : 21.77\% \\\\\
\hline

dep & ('dep', 2132987, '75.61 \%') & ('case', 2170532, '76.96 \%') \\\\\
~ & ('mod', 315970, '11.2 \%') & ('case, det', 374044, '13.26 \%') \\\\\
~ & \\textit{others (\#23)} : 13.19\% & \\textit{others (\#41)} : 9.78\% \\\\\
\hline

dep\_coord & ('dep.coord', 2181378, '80.56 \%') & ('conj', 1528465, '56.46 \%') \\\\\
~ & \\textit{others (\#25)} : 19.44\% & ('case', 440817, '16.28 \%') \\\\\
~ & ~ & \\textit{others (\#40)} : 27.26\% \\\\\
\hline

det & ('det', 12474849, '97.43 \%') & ('det', 9967966, '77.85 \%') \\\\\
~ & \\textit{others (\#17)} : 2.57\% & ('nmod:poss', 2110127, '16.48 \%') \\\\\
~ & ~ & \\textit{others (\#33)} : 5.67\% \\\\\
\hline

mod & ('mod', 14938931, '58.94 \%') & ('case', 6952370, '27.44 \%') \\\\\
~ & ('\_', 3560384, '14.05 \%') & ('amod', 3521139, '13.9 \%') \\\\\
~ & ('dep', 2793113, '11.02 \%') & ('advmod', 3053098, '12.05 \%') \\\\\
~ & \\textit{others (\#56)} : 16.0\% & \\textit{others (\#139)} : 46.62\% \\\\\
\hline

mod\_rel & ('mod.rel', 855698, '67.01 \%') & ('acl:relcl', 816069, '63.91 \%') \\\\\
~ & ('\_', 190694, '14.93 \%') & \\textit{others (\#30)} : 36.09\% \\\\\
~ & \\textit{others (\#17)} : 18.05\% & ~ \\\\\
\hline

obj & ('obj', 4332951, '60.97 \%') & ('dobj', 3816524, '53.71 \%') \\\\\
~ & ('\_', 873277, '12.29 \%') & \\textit{others (\#39)} : 46.29\% \\\\\
~ & \\textit{others (\#27)} : 26.75\% & ~ \\\\\
\hline

p\_obj & ('mod', 7216, '44.51 \%') & ('case', 12900, '79.96 \%') \\\\\
~ & ('p\_obj.agt', 3250, '20.05 \%') & \\textit{others (\#6)} : 20.04\% \\\\\
~ & ('p\_obj.o', 2152, '13.27 \%') & ~ \\\\\
~ & \\textit{others (\#5)} : 22.17\% & ~ \\\\\
\hline

ponct & ('ponct', 10224626, '84.74 \%') & ('punct', 11935299, '98.86 \%') \\\\\
~ & ('\_', 1653035, '13.7 \%') & \\textit{others (\#31)} : 1.14\% \\\\\
~ & \\textit{others (\#24)} : 1.56\% & ~ \\\\\
\hline

prep & ('obj.p', 12286066, '95.11 \%') & ('nmod', 9681112, '74.95 \%') \\\\\
~ & \\textit{others (\#28)} : 4.89\% & \\textit{others (\#40)} : 25.05\% \\\\\
\hline

root & ('\_', 3012491, '89.08 \%') & ('root', 1955105, '57.82 \%') \\\\\
~ & \\textit{others (\#21)} : 10.92\% & ('cop', 375370, '11.1 \%') \\\\\
~ & ~ & \\textit{others (\#32)} : 31.08\% \\\\\
\hline

sub & ('obj.cpl', 1076650, '70.36 \%') & ('ccomp', 308455, '20.16 \%') \\\\\
~ & ('\_', 209770, '13.71 \%') & ('advcl', 301933, '19.73 \%') \\\\\
~ & \\textit{others (\#21)} : 15.93\% & ('conj', 185869, '12.15 \%') \\\\\
~ & ~ & ('aux', 154942, '10.13 \%') \\\\\
~ & ~ & \\textit{others (\#29)} : 37.84\% \\\\\
\hline

suj & ('suj', 6692626, '88.29 \%') & ('nsubj', 6221400, '82.07 \%') \\\\\
~ & \\textit{others (\#25)} : 11.71\% & \\textit{others (\#37)} : 17.93\% \\\\\
\hline

\end{longtabu}
talismane offers 24 different labels, excluding grouping.
\paragraph{Grew dispersion} : 
For 24 different DEP : 53 occurences (average : 2.21).
\paragraph{Grew cover by talismane} : average : 84.71\%.
\paragraph{Stanford dispersion} : 
For 24 different DEP : 53 occurences (average : 2.21).
\paragraph{Stanford cover by talismane} : average : 75.17\%.
\end{appendices}''')
addParagraph(TAGS, False)
tprint(getEnd())

tableau.close()
compil_()

# • Label verbs with LVF class *** say how ***
# • Label arguments and modifiers *** say which args and mods and how
# labeling is done ***
#
# Statistics
# • # Books used
# • # Sentences parsed
# • # Sentences after labelling (= experimental data)
# • # Distinct verb lemmas in experimental data
# • # tokens / sentence: min, max, median, avg
# • # LVF classes
# • Distribution # verb/sentence
# • Distribution # sentence/verb classs
