
import copy
import json
import os
import random
import sys

pathname = os.path.dirname(sys.argv[0])
fullpath = os.path.abspath(pathname)

debug = True
# chargement des données
def loadArbres():
    global debug
    if not debug:
        fichier = 'arbres/arbres_extraits.json'
        fichier2 = 'arbres/random/arbres_extraits_0.json'
    else:
        fichier = 'arbres/random/arbres_extraits_0.json'
        fichier2 = 'arbres/simple/arbres_extraits_0001.json'
    if os.path.exists(fichier):
        print("chargement " + fichier)
        a = open(fichier, "r")
        arbres_extraits = json.load(a)
        a.close()
        print("arbres chargés")
        return arbres_extraits
    elif os.path.exists(fichier2):
        print("chargement " + fichier2)
        a = open(fichier2, "r")
        arbres_extraits = json.load(a)
        a.close()
        print("arbres chargés")
        return arbres_extraits
    return {}

arbres_extraits = loadArbres()

local_keys_arbres_extraits = list(arbres_extraits.keys())


# on génère un petit fichier test
arbres_ = {}
arbres = {}
sample = random.sample(local_keys_arbres_extraits, 100)

def reduce_analyse(item):
    if isinstance(item, str) or isinstance(item, int):
        return item
    if isinstance(item, list) and ((not len(item) == 10 or (len(item) == 10 and not isinstance(item[-1], str))) and len(item)>0):
        if isinstance(item[0], str) and len(item) == 2:
            return [item[0], reduce_analyse(item[1])]
        if isinstance(item[0], str) :
            return [item[0], [reduce_analyse(k) for k in item[1:]]]
        for i in range(len(item)):
            item[i] = reduce_analyse(item[i])
        return item
    elif isinstance(item, list) and len(item) == 10 and  isinstance(item[-1], str):
        return item[1] + ";" + item[2] if item[1] != item[2] else item[1]
    elif isinstance(item, dict):
        to_del = []
        for k in item.keys():
            if item[k] is None:
                to_del.append(item[k])
            else:
                item[k] = reduce_analyse(item[k])
                if not isinstance(item[k], int):
                    if len(item[k]) == 0:
                        to_del.append(k)
                    if item[k] == [[]]:
                        item[k] = "-"
        for k_ in set(to_del):
            del item[k]
        return item
    elif isinstance(item, tuple):
        if isinstance(item[0], str) and len(item) == 2:
            if isinstance(item[1], list) :
                retour = []
                for each in item[1]:
                    if len(each)>2:
                        if each[0] in ('event', 'argument'):
                            retour.append(each)
                        else:
                            retour.append(reduce_analyse(each))
                    else:
                        retour.append(reduce_analyse(each))
            else:
                retour = reduce_analyse(item[1])
            return {item[0]:retour}
            #return item[0], reduce_analyse(item[1])
    else:
        return item


for key in sample:
    arbres[key] = arbres_extraits[key]
    info_arbre = copy.deepcopy(arbres_extraits[key])
    del info_arbre["analyses"]
    info_arbre = reduce_analyse(info_arbre)
    arbres_[key] = info_arbre

fjson = open("arbres/fichier_test_simplifié.json", "w")
fjson.write(json.dumps(arbres_, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
fjson = open("arbres/fichier_test.json", "w")
fjson.write(json.dumps(arbres, indent=2, ensure_ascii=False))
fjson.flush()
fjson.close()
while not fjson.closed:
    True
exit()