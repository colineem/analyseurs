import glob
import sys
from tqdm import tqdm
from pprint import pprint
import subprocess

import json
fichiers = sorted(glob.glob('arbres/random/*.json'))
if len(fichiers) != 10:
    fichiers = sorted(glob.glob('arbres/simple/arbres_extraits_*.json'))
table_conjugaison = {}
progress = tqdm(range(len(fichiers)))
dones = set()

temps_ambigus = []

for i_f, fichier in enumerate(fichiers):
    a = open(fichier, "r")
    data = json.load(a)
    a.close()

    # on va y aller lentement, mais c'est juste pour que le code soit plus lisible
    # d'abord on comptabilise pour vérifier nos bornes
    progress2 = tqdm(range(len(data.keys())))
    for sentence in data.keys():
        s_infos = data[sentence]
        events = s_infos["events"]
        temps_all = []
        temps_ambigu = []
        for classe in events.keys () :
            for token in events[classe] :
                if not "event" in events[classe][token].keys () :
                    break
                event = events[classe][token]
                verbe_conjugué = event["event"][1]
                temps = events[classe][token]["constraints"]["conjugaison"]["informations"]["temps"].replace("_", " ") \
                    if events[classe][token]["constraints"]["conjugaison"]["informations"]["temps"] is not None else ''
                if "|" in temps:
                    temps_ambigu.append(verbe_conjugué + " avec "  + temps)
                temps_all.append(temps)
                mode = events[classe][token]["constraints"]["conjugaison"]["informations"]["mode"].replace("_", " ")\
                    if events[classe][token]["constraints"]["conjugaison"]["informations"]["mode"] is not None else ''
                nombre = events[classe][token]["constraints"]["conjugaison"]["informations"]["nombre"].replace("_", " ") \
                    if events[classe][token]["constraints"]["conjugaison"]["informations"]["nombre"] is not None else ''
                mode_conj = events[classe][token]["constraints"]["conjugaison"]['mode'].replace("_", " ") \
                    if events[classe][token]["constraints"]["conjugaison"]["informations"]["mode"] is not None else ''
                passif = " (passif)" if "passif" in events[classe][token]["constraints"].keys() else ''
                if mode not in table_conjugaison.keys():
                    table_conjugaison[mode] = {}
                if temps not in table_conjugaison[mode].keys():
                    table_conjugaison[mode][temps] = {'compte':0, 'exemples':["", "", "", "", "", ""]}
                table_conjugaison[mode][temps]['compte'] += 1
                if passif == '':
                    if table_conjugaison[mode][temps]['exemples'][0] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][0] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]
                    elif table_conjugaison[mode][temps]['exemples'][1] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][1] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]
                    elif table_conjugaison[mode][temps]['exemples'][2] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][2] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]
                else:
                    if table_conjugaison[mode][temps]['exemples'][3] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][3] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]
                    elif table_conjugaison[mode][temps]['exemples'][4] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][4] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]
                    elif table_conjugaison[mode][temps]['exemples'][5] == "" and sentence not in dones:
                        dones.add ( sentence )
                        table_conjugaison[mode][temps]['exemples'][5] = [event["event"][1] + " / " + event["event"][2] + passif, sentence.replace("_", " ").replace("&", "\&")]

        if len(temps_ambigu)>0:
            temps_ambigus.append({"temps ambigu":temps_ambigu, "temps présent"+"s" if len(temps_all)>0 else "":temps_all, "sentence":sentence})
        progress2.update()
    progress2.close()
    progress.update()
progress.close()

progress2.clear()
progress.clear()

for _ in range(100000):
    True



ftex = open ("documentation/conjugaison.tex", 'w' )
# We redirect the 'sys.stdout' command towards the descriptor file
old_stout = sys.stdout
sys.stdout = ftex

fnf = lambda l: "{:,}".format(round(l, 2))
fn = lambda l: "{:,}".format(int(l)) if int(l) == l else fnf(l)

print("""
\\documentclass[10pt,a4paper]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[french]{babel}
\\usepackage[T1]{fontenc}
\\usepackage{amsmath}
\\usepackage{amsfonts}
\\usepackage{amssymb}
\\usepackage{graphicx}
\\usepackage{lmodern}
\\usepackage{longtable}
\\usepackage[left=1cm,right=1cm,top=1.5cm,bottom=1.5cm]{geometry}
\\author{Emilie Colin}
\\usepackage{hyperref}
\hypersetup{backref,
colorlinks=true}
\\begin{document}""")


stemps = set()

modes = ['indicatif',
 'indicatif | subjonctif',
 'subjonctif', 'conditionnel',
 'conditionnel | indicatif',
 'participe',
 'infinitif']
stemps = ['présent',
 'passé simple | présent',
 'passé simple',
 'imparfait',
 'imparfait | présent',
 'passé composé',
 'passé antérieur | passé composé',
 'passé antérieur',
 'passé',
 'passé | plus que parfait',
 'plus que parfait',''
 'futur',
 'futur antérieur',
          ''
 ]
appened = []
for mode in  table_conjugaison.keys():
    if mode not in modes:
        modes.append(mode)
        appened.append(mode)
    for temps in table_conjugaison[mode].keys():
        if temps not in stemps:
            stemps.append(temps)
            appened.append(temps)

print("\section{Vue d'ensemble}")
if len(appened)>0:
    pprint(appened)
    pprint("... ajoutés dynamiquement.")

compte = 0
table_modes = {}
for mode in modes:
    if mode in table_conjugaison.keys():
        table_modes[mode] = 0
        for temps in stemps:
            if temps in table_conjugaison[mode].keys():
                compte += table_conjugaison[mode][temps]['compte']
                table_modes[mode] += table_conjugaison[mode][temps]['compte']

print("Nous avons un total de " + fn(compte) + " verbes gérés.")

for mode in modes:
    if mode in table_conjugaison.keys():
        print("\subsection*{"+mode[0].upper() + mode[1:]+", for a total of " + fn(table_modes[mode])+ " verbs~}")
        for temps in stemps:
            if temps in table_conjugaison[mode].keys():
                compte = table_conjugaison[mode][temps]['compte']
                print("\paragraph{" + (temps if temps != ""  else "-") + ":~}" + fn(compte)  + " (voir p. \pageref{" + temps.replace(" ", "") + mode.replace(" ", "") + "})")


print("\section{Exemples}")
for mode in modes:
    if mode in table_conjugaison.keys():
        print("""\subsection{""" + mode[0].upper() + mode[1:] +' (' + fn(table_modes[mode]) +')' + """}
        
    \\begin{longtable}{|p{3.8cm}|p{14.5cm}|}\hline
    """)
        for temps in stemps:
            if temps in table_conjugaison[mode].keys():
                compte = table_conjugaison[mode][temps]['compte']
                exemples = table_conjugaison[mode][temps]['exemples']
                print("\multicolumn{2}{c|}{" + temps, end = ", compte: ")
                print(fn(compte) + "\label{" + temps.replace(" ", "") + mode.replace(" ", "") + "}", end = "} \\\\ \hline   \n ")
                for ie, exemple in enumerate(exemples):
                   # texte = ''
                    if exemples[ie] != '':
                       # for i in range(len(exemples[ie][1])):
                         #   if i % 65 == 0:
                        #        texte += "\\newline{}" + exemples[ie][1][i].strip()
                          #  else:
                           #     texte += exemples[ie][1][i]
                     #   exemples[ie][1] = texte
                        print(exemples[ie][0], end = " & ")
                        print(exemples[ie][1], end = "  \\\\ \hline \n ")

        print("""
          \end{longtable} 
          \paragraph{}~\\\\~ 
        """)

print('\end{document}')


sys.stdout = old_stout
ftex.close()

while not ftex.closed:
    True

subprocess.run("cd documentation && pdflatex conjugaison.tex && pdflatex conjugaison.tex && pdflatex conjugaison.tex ", shell=True, check=True)

pprint(temps_ambigus)
exit()